<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Campaign Management</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<style type="text/css">

#search-button {
  display: inline-block;
  width: 34px;
  height: 32px;
  position: relative;
  left: -41px;
  top: 9px;
  background: url(images/search.jpg) no-repeat right;
}
</style>

<script>
$(document).ready(function() {
renderlistpage();

});

function renderlistpage()
{
	$("#listrows").empty();
	 $("#listrowsPage").empty();
	$.ajax({
		type: "get",
		url: "displayCampaigns.htm",
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}

 function rendernextlistpage()
{
	 $("#listrows").empty();
	 $("#listrowsPage").empty();
	pagenum =pagenum+1;
	$.ajax({
		type: "get",
		url: "displayCampaigns.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}

function renderprevlistpage()
{
	pagenum =pagenum-1;
	$.ajax({
		type: "get",
		url: "displayCampaigns.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}


var pagenum = 1; 

function renderlisttable(objData, pagenumber, totalpages)
{
	$("#listrows").empty();
	 $("#listrowsPage").empty();
	pagenum = pagenumber;
	if(objData != null && objData.length > 0)
	{
		$("#listrows").append('<tr><th width="100">Campaign Name</th><th width="100">List Name</th><th width="100">Status</th><th width="100">Subject</th><th width="100">Smtp Server</th><th width="100">Start Time</th><th width="100">End Time</th><th width="140">Actions</th></tr>');
		
		var cont =1;
		for(var count =0; count < objData.length; count++)
		{
			var row = objData[count];
			renderRowInListTable(row, cont);
			cont++;
				
		}
		$("#listrowsPage").append('<tr align:right>');
		
		
		if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$("#listrowsPage").append('<td align:right><input type="submit"  style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"><input type="submit"   style="float: right; margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$("#listrowsPage").append('<td align:right><input type="submit"   style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$("#listrowsPage").append('<td align:right><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
			
		}
		$("#listrowsPage").append('</tr>');
		
		
	}
	else
	{
		$("#listrows").append('<tr><td style="color:red">No list found.</td></tr>');
	}
	
}

function renderlisttable1(objData)
{
	if(objData != null && objData.length > 0)
	{
		$("#listrows").empty();
		 $("#listrowsPage").empty();
		$("#listrows").append('<tr><th width="100">Campaign Name</th><th width="100">List Name</th><th width="100">Status</th><th width="100">Subject</th><th width="100">Smtp Server</th><th width="100">Created On</th><th width="100">Start Time</th><th width="100">End Time</th><th width="140">Actions</th></tr>');
		var cont =1;
		for(var count =0; count < objData.length; count++)
		{
			var row = objData[count];
			renderRowInListTable(row, cont);
			cont++;
				
		}
	}
	else
	{
		$("#listrows").append('<tr><td style="color:red">No list found.</td></tr>');
	}
	
}

function renderRowInListTable(row, cont)
{
	
	var campname=row.campaignName;
	var listname = row.listName;
	if(listname == null)
		listname ="";
	var status = row.status;
	if(status == null)
		status ="";
	var subject=row.subject;
	var smtpServerName=row.smtpServer;
	var createdDate = row.createdDate;
	var campaignStartDateTime = row.campaignStartDateTime;
	if(campaignStartDateTime == null)
		campaignStartDateTime="";
	var campaignEndDateTime = row.campaignEndDateTime;
	if(campaignEndDateTime == null)
		campaignEndDateTime="";
	
	var action="Start Action";
	var editaction="Edit Action ";
	var deleteaction="Delete Action";
	var stopaction="Stop Action";
	 $("#listrows").append('<tr><td width="70">'+campname+'</td><td width="70">'+listname+'</td><td width="60">'+status+'</td><td width="100">'+subject+'</td><td width="70">'+smtpServerName+'</td><td width="120">'+campaignStartDateTime+'</td><td width="120">'+campaignEndDateTime+'</td><td width="170" align="center">&nbsp;&nbsp;&nbsp;<a href="#"><img src="images/start.png" class="tooltip" title="start" onclick="checkStart(\''+status+'\',\''+campname+'\');"/></a>&nbsp;&nbsp;&nbsp;<a href="#"><img src="images/stop.png" class="tooltip" title="stop" onclick="checkStop(\''+status+'\',\''+campname+'\');" /></a>&nbsp;&nbsp;&nbsp;<a href="#"><img src="images/edit.png" class="tooltip" title="edit" onclick="checkEdit(\''+status+'\',\''+campname+'\');"/></a>&nbsp;&nbsp;&nbsp;<a href="displaycopycampaign.htm?campaignname='+campname+'"><img src="images/copy.jpg" class="tooltip" title="copy"/></a>&nbsp;&nbsp;&nbsp;<a href="campaignReports.htm?campaignname='+campname+'"><img src="images/report.png" title="report"  alt="report" /></a>&nbsp;&nbsp&nbsp;;<img src="images/delete.png" class="tooltip" title="delete" onclick="checkDelete(\''+status+'\',\''+campname+'\');" /></td></tr>');

	   
	}			
	function checkStart(status,campaignName)
	{
		
		if(status =="running" ||  status =="cancelled")
		{
			alert("campaign status is " + status + " can not start the campaign");
			return false;
		}  
		else
		{
			window.location.href="campaignconfig.htm?campaignName="+campaignName;
					
			
		} 
		
	}
	function checkEdit(status,campaignName)
	{
		
		if(status =="running" ||  status =="cancelled")
		{
			alert("campaign status is " + status + "can not update the campaign");
			return false;
		}else
		{
			/* $.ajax({
				type: "get",
				url: "displayeditcampaign.htm?campaignName="+campaignName,
				dataType: "json",
				success: function( objData )
				{
					response.redirect = "editcampaign.jsp";	
				},
				error: function(data){
					
					alert( "An error occurred" +data[0] );
					//window.location = "campaignManagement.jsp";
				}
			}); */
			window.location.href="displayeditcampaign.htm?campaignName="+campaignName;
			
			
			
		}
		
	}
	function checkStop(status,campaignName)
	{
		
		if(!(status =="running"))
		{
			alert("campaign status is " + status + "can not stop it ");
			return false;
		}else
		{
			if(confirm("campaign status is " + status +", are you sure you want to stop this campaign" + campaignName))
			{
			$.ajax({
				type: "get",
				url: "stopcampaign.htm?campaignName="+campaignName,
				dataType: "json",
				success: function( objData )
				{
					window.location = "campaignManagement.jsp";	
				},
				error: function(data){
					
					//alert( "An error occurred" +data[0] );
					window.location = "campaignManagement.jsp";
				}
			});
			
			}
			
			
		}
		
	}
	function checkDelete(status,campaignName)
	{
			
		if(status =="running")
		{
			alert("campaign status is " + status + "can not delete this campaign ");
			return false;
		}else
		{
			if(confirm("campaign status is " + status +", are you sure you want to delete this campaign" + campaignname))
			{
			$.ajax({
				type: "get",
				url: "deletecampaign.htm?campaignName="+campaignname,
				dataType: "json",
				success: function( objData )
				{
					window.location = "campaignManagement.jsp";	
				},
				error: function(data){
					
					//alert( "An error occurred" +data[0] );
					window.location = "campaignManagement.jsp";
				}
			});
			
			}
			
			
		}
		
	}
			
			/* function */
			
	function stopCampaign(campaignname)
	{
		alert("hi stopt");
		if(confirm("are you sure you want to stop this campaign" + campaignname))
		{
		$.ajax({
			type: "get",
			url: "stopcampaign.htm?campaignName="+campaignname,
			dataType: "json",
			success: function( objData )
			{
				window.location = "campaignManagement.jsp";	
			},
			error: function(data){
				
				//alert( "An error occurred" +data[0] );
				window.location = "campaignManagement.jsp";
			}
		});
		
		}
	
	}


function deleteRow(id)
{
	alert("hi delee");
	if(confirm("are you sure you want to delete this list: " + id))
	{
	$.ajax({
		type: "get",
		url: "deletecampaign.htm?campaignName="+id.toString(),
		data: id,
		dataType: "json",
		success: function( objData )
		{
			window.location = "campaignManagement.jsp";	
		},
		error: function(data){
			
			//alert( "An error occurred" +data[0] );
			window.location = "campaignManagement.jsp";
		}
	});
	}
	return false;
	
}
function refresh() {
	window.location = "campaignManagement.jsp";
}


	
</script>
<script type="text/javascript">
function createNewCampaign()
{
	window.location="createCampaign.jsp";
}

function searchCampaign()
{
	var name = document.getElementById("searchcampaign");
	var campaignname = name.value.trim();
	if(campaignname == "")
		{
		alert("Please provide Campaign Name");
		}
		
		$.ajax({
			type: "get",
			url: "searchcampaign.htm?campaignname="+campaignname,
			dataType: "json",
			success: function( objData )
			{
				renderlisttable(objData.result, objData.pagenumber, objData.totalpages);
				//window.location = "campaignManagement.jsp";	
			},
			error: function(data){
				
				//alert( "An error occurred" +data[0] );
				window.location = "campaignManagement.jsp";
			}
		});
		
	
	return false;


}
function validateForm(name)
{

	if(isalphaNum(name, "Please enter valid name (A-Z, a-z, 0-9)"))
	{
		return true;
	}
	return false;
	
}
</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    <li><a href="showemailconfig.htm"> Email Configuration</a></li>
    </ul>
</div>
 <a class="right-nav" href="showsmtpManagement.htm"> SMTP Configuration</a>
</div>
</div>
</div>
	<div id="content">
		<div id="loading-popup-box" style="display: none;"></div>
		<div id="content-main">
			<div class="contents">
			<div class="create_form">
			<input type="text" id="searchcampaign" name="searchcampaign" placeholder="Search Campaign" size="20" ><span id="search-button" onclick="searchCampaign();"></span>
		    <a href="createCampaign.jsp" id="buttonclass">Create New Campaign</a>
			</div>
			<table id="listrows" class="tblewidth">		
				</table>
				<table id="listrowsPage"></table>
			</div>
		</div>
	</div>

	<div id="footer">
		<div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div>
	</div>
</body>
</html>