<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit Subscriber</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script>
$(document).ready(function(){
	getattributesdata();
	
});
</script>
<script>
function getattributesdata()
{
	var email ="${email}";
	var listName ="${listName}";	
	$.ajax({
		type: "get",
		url: "getsubscriber.htm?listName=" + listName+"&email=" + email,
		dataType: "json",
		success: function( objData )
		{
			populatecustattr(objData.custAttr);
			populatestdattr(objData.stdAttr);
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
	}
function populatecustattr(objData)
{
	var fieldsData="";
	var count=Object.keys(objData).length;
	alert(count);
	for(var i in objData)
		if(i == "email" || i == "emailId")
		{
			//fieldsData = fieldsData+'<tr><td>'+'<label>'+i+'</label></td><td>'+ '<input type="text" readonly="readonly" '+' name="'+i+'"'+ 'value="'+objData[i]+'"'+'size="28"'+'id="#'+i+'"/>'+'</tr></td>';
		}
		else
		{
			fieldsData = fieldsData+'<tr><td align="center">'+'<label>'+i+':</label></td><td align="center">'+ '<input type="text" '+' name="'+i+'"'+ 'value="'+objData[i]+'"'+'size="28"'+'id="#'+i+'"/>'+'</tr></td>';
		}
	$("#custattributes").html(fieldsData);
}

function Cancel()
{
	
}

function populatestdattr(objData)
{
	var fieldsData="";
	var vale = objData["email"];
	var emt  = "email";
	fieldsData = fieldsData+'<tr><td align="center">'+'<label>'+emt+':</label></td><td align="center">'+ '<input type="text" readonly="readonly" '+' name="'+emt+'"'+ 'value="'+vale+'"'+'size="28"'+'id="#'+i+'"/>'+'</tr></td>';
	for(var i in objData)
	{
		if(i == "email" || i == "emailId")
		{
			//fieldsData = fieldsData+'<tr><td>'+'<label>'+i+'</label></td><td>'+ '<input type="text" readonly="readonly" '+' name="'+i+'"'+ 'value="'+objData[i]+'"'+'size="28"'+'id="#'+i+'"/>'+'</tr></td>';
		}
		else
		{
			if(objData[i] == null || objData[i].length <= 0)
			{
				vale = "";
			}else
				vale = objData[i];
			fieldsData = fieldsData+'<tr><td align="center">'+'<label>'+i+':</label></td><td align="center">'+ '<input type="text" '+' name="'+i+'"'+ 'value="'+vale+'"'+'size="28"'+'id="#'+i+'"/>'+'</tr></td>';
		}
	}
	$("#stdattributes").html(fieldsData);
	
}
</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<form name = "subscriberform" action="editsubscriber.htm" method="post">
	<div class="contents">
	     <ul class="breadcrumb2" style="margin-top:-10px;margin-left:5px;margin-bottom:10px;">
	   <li><a href="listManagement.jsp" class="bredcom">Lists</a></li>
	   <li><a href="subsribers.htm?listName=<%=request.getAttribute("listName")%>" class="bredcom" id=""><%=request.getAttribute("listName") %></a></li>
	 <li><a href="#" class="bredcom" id="edit">${email}</a> 
	   </ul>
    <table width="100%">
    <tr>
    	<td width="50%" valign="top">
    		<table id="stdattributes" ></table>	
    		</td>
    	<td width="50%" valign="top">
    		<table id="custattributes" ></table>
    	</td>
    </tr>	
 	
   </table>
   <div style="float: right;margin-top:-30px;">
   <table >
    	<tr ><td ><input type="hidden" name="listName" value=<%=request.getAttribute("listName") %>>
    	<td><input type="submit" id="buttonclass" value="Update">
    
    	</table>
    	  </div>
   </div>
</form>
</div>
</div>
<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>
</body></html>