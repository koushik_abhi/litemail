<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create List</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script>
var listname= "";
$(document).ready(function() {

$("#rowfile").css("display","");
$("#rowftp").css("display","none");
$('input[name=datasource]:radio').click(function()
{
	
	if($(this).val()=="fileupload")
	{
		$("#rowfile").css("display","");
		$("#rowftp").css("display","none");
	}
	else if($(this).val()=="ftpupload")
	{
		$("#rowfile").css("display","none");
		$("#rowftp").css("display","");
	}
});


			
});
function validateForm(name)
{
	/*if(name == null && name =="")
	{
		alert("Please enter valid list name");
		elem.focus();
		return false; 
	}*/
	if(isalphaNum(name, "Please enter valid name (A-Z, a-z, 0-9)"))
	{
		return true;
	}
	return false;
	
}



function importRecords()
{
	var listname = document.getElementById("listname");
	var values = listname.value.trim();
	if(values =="")
	{
		alert("please enter listname");
		return false;
	}
		
	if(validateForm(values))
	{
		var actionurl ="";
		var type = $("input:radio[name='datasource']:checked").val();
		if(type == "fileupload")
		{	
		    document.uploadlistform.enctype = "multipart/form-data";
			actionurl = "uploadlist.htm";
			document.uploadlistform.action = actionurl;
			
			document.uploadlistform.submit();
			return true;
			
		}
		if(type == "ftpupload")
		{
			actionurl = "uploadlistftp.htm";
			document.uploadlistform.action = actionurl;
			document.uploadlistform.submit();
			return true;
		}
		
	}
	return false;
	
}


function cancel()
{
	
	window.location="listManagement.jsp";
}

</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
<!--  <div class="workflow"><span><img src="./images/createlist-red.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>        
 --> <div class="contents">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<!-- upload list -->
	<form name = "uploadlistform" action="uploadlist.htm" method="post">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="create_list">
  <tbody>
 <tr>
    <td width="130"><label>List Name</label></td>
    <td><input type="text" id = "listname"  name="listname" size="30"></td>
  </tr>
  <tr>
   <td><label>Description<br></label></td>
   <td><textarea id="description"  name="description" rows="3" cols="22"></textarea></td>
  </tr>
  <tr>
  <td height="40">&nbsp;</td>
    <td><label><input type="radio" name="datasource" checked="checked" value="fileupload"> File Upload</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio" name="datasource"  value="ftpupload"> FTP / SFTP</label>
</td>
  </tr>
   <tr id="rowfile" style="display: none;">
    <td height="55"><label>Upload File: </label></td>
    <td><input type="file" id="datafile" name="datafile"></td>
  </tr>
  <tr id="rowftp" style="display: none;">
    <td colspan="2"><table border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="130"><label>FTP Hostname: </label></td>
        <td><input type="text" name="ftpserver" size="30"></td>
      </tr>
      <tr>
        <td><label>FTP User: </label></td>
        <td><input type="text" name="ftplogin" size="30"></td>
      </tr>
      <tr>
        <td><label>FTP Password: </label></td>
        <td><input type="password" name="ftppassword"  ></td>
      </tr>
      <tr>
        <td><label>File Location: </label></td>
        <td><input type="text" name="ftppath" size="30"></td>
      </tr>
       <tr>
    <td><input type="hidden" name="check" id="check" value="true"></td>
    <td colspan="2">&nbsp;</td>
  </tr>
    </tbody></table></td>
  </tr>
    
   
<tr>
<td colspan="3" align="right"> &nbsp;<input type="submit" name="submit" value="Save" onclick=" return importRecords();" id="submit"></td>
<td ><input type="button"  id="buttonclass"  value="Cancel" onclick="cancel();"></td>
</tr>
</tbody>
</table>
</form>
</div>
  </div>
</div>

<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>