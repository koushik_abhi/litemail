<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<%@ taglib uri="/WEB-INF/ckeditor.tld" prefix="ckeditor" %>
<%@page import="com.ckeditor.CKEditorConfig"%>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create Campaign</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>
<script src="./ckeditor/ckeditor.js"></script>
<script>
var listname= "";
$(document).ready(function() {
	$("#rowfile").css("display","none");
	$("#editor").css("display","");
//renderlistpage();
var err = "${errorMessage}";


//alert(JSON.stringify(jspn));
if(err != null && err.length > 1 )
{
	<%
	
	String str = (String)request.getAttribute("campaignObj");
	
	%>
	var jspn = <%= str%>;
	renderCampaignData(jspn);
}else
{
  renderlistpage();
  loadEditor();
}

$('input[name=datasource]:radio').click(function()
{
	
	 if($(this).val()=="fileupload")
	{
		$("#rowfile").css("display","");
		$("#editor").css("display","none");
	}
	else if($(this).val()=="pastehtml")
	{
		
		$("#rowfile").css("display","none");
	
		$("#editor").css("display","");
		loadEditor();
	
	}
});


			
});


function renderCampaignData(result)
{
	$("#listname").empty();
	var camp=result.campaignName;
	$("#campaignname").val(camp);
	$("#newcampaignname").val(camp);
	var listName=result.listName;
	$("#listname").append(result.listName);
	var listData="<option value="+listName+" selected>"+listName+"</option>";
    $(listData).appendTo('#listname');
	$("#description").val(result.description);
	$("#subject").val(result.subject);
	$("#sendername").val(result.fromName);
	$("#senderemail").val(result.fromAddress);
	$("#replyname").val(result.replyName);
	$("#replyemail").val(result.replyTo);
	var smtpName=result.smtpServer;
	var smptpServer="<option value="+smtpName+" selected>"+smtpName+"</option>";
	$(smptpServer).appendTo('#smtpserver');
	$("#opentracking").val(result.enableLinkTracking);
	$("#linktracking").val(result.enableOpenTracking);
	
	
	var htmlFileName=result.htmlFileName;
	var campMessage=result.campaignMessage;   
	
	var attachementFileName=result.attachmentFileName; 
	
	if(htmlFileName != null && htmlFileName.length > 0)
		{
		$("#hidemailbody").show();
		$("#filename").text(htmlFileName);
		$("#viewmore").html(campMessage);
		}
	else
		{
		//alert("hi");
		$("#campaignMessage").val(campMessage);
		loadEditor();
		//$("#campaignMessage").val(campMessage);
		//window.setTimeout( 200 );
		$("#showmailbody").show();
		 $("#editor").show();
		
		
		}
	if(attachementFileName != null && attachementFileName.length > 0)
		{
		$("#attachementfile").text(attachementFileName);
		}
	
	if($("#linktracking").val()=="on")
		{
		$("#linktracking").attr("checked",true);
		}
	if($("#opentracking").val()=="on")
	{
	$("#opentracking").attr("checked",true);
	}
	
	renderlistpage();
	
}

function renderlistpage()
{
	$.ajax({
		type: "get",
		url: "getlistsmtpnames.htm",
		dataType: "json",
		success: function( objData )
		{
			populateSmtpServers(objData.smptpServers);
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}
function populateSmtpServers(result)
{
	if(result != null && result.length > 0)
	{	
			$.each(result, function(i, value) {
 	           $('#smtpserver').append($('<option>').text(value).attr('value', value));
 	          
 	        });		
		}
}


function validateForm(name)
{
if(name == null && name =="")
	{
		alert("Please enter valid list name");
		elem.focus();
		return false; 
	}
	if(isalphaNum(name, "Please enter valid name (A-Z, a-z, 0-9)"))
	{
		return true;
	}
	return false;
	
}



function validate()
{
	
	var campaignName_val = $("#campaignname").val();
	var senderName=$("#sendername").val();
	var senderEmail=$("#senderemail").val();
	var replyEmail=$("#replyemail").val();
	var replyName=$("#replyname").val();
	var subjectval=$("#subject").val();
	var listName=$("#listname").val();
	var smtpserver=$("#smtpserver").val();
	var campaignMessage=$("#campaignMessage").val();
	var datafile=$("#datafile").val();
	var Exp =  /^[a-zA-Z-0-9_ ]+$/;
	var emailExp= /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	
	if(campaignName_val == null || campaignName_val =="")
	{
		alert("Please enter campaign name");
		return false; 
	}
	
	if(!campaignName_val.match(Exp)){
	    alert("Please enter valid campaign name (A-Z, a-z, 0-9, _)");
	return false;
	}
	
     if(listName == "" || listName == null)
 		 {
 		 alert("please select listname");
 		 return false;
 		 }
 	 if(subjectval == "" || subjectval == null)
 		 {
 		 alert("please enter subject");
 		 return false;
 		 }
 
 	 if(senderName == null && senderName =="")
 	 {
 		 alert("please enter sender name");
 		 return false;
 	 }
 	if(!isAlphabet(sendername, "Please enter valid sender name (A-Z, a-z)"))
	{
		return false;
	}
 	 
 	if(senderEmail == null || senderEmail == "")
 	 {
 	  alert("please enter Email ");
 	  return false;
 	 } 
 	
 	if(!senderEmail.match(emailExp))
	{
			  alert("Please enter valid valid email");
		return false;
	} 
 	if(replyName != ""){
 	 	if(!isAlphabet(replyname, "Please enter valid reply name (A-Z, a-z)"))
 		{
 			return false;
 		}
 	 	}
	if(replyEmail != ""){
	 	if( !replyEmail.match(emailExp))
		{
				  alert("Please enter valid reply email");
			return false;
		}  
	}
	 	
 	 
 	 if(smtpserver == null || smtpserver == "")
 		 {
 		 alert("Please select valid smtpserver");
 		 return false;
 		 }
 	 /* alert(campaignMessage);
 	 if(campaignMessage == null || datafile == null)
 		 {
 		 alert("please provide mail body");
 		 } */
	 else{
		document.createcampaigform.submit();
	}  
	
}

function loadEditor()
{
	// $("#campaignMessage").val(message);
	 CKEDITOR.replace('campaignMessage', {
			uiColor: '#CFD1CF',
			allowedContent: true,
			height:'350px',
			toolbar: [
				{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Preview', 'Print', '-', 'Templates' ] },
				{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
				{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	               { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	               { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	                    '/',
	              { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	              { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
	              { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
	              { name: 'others', items: [ '-' ] },
	              { name: 'about', items: [ 'About' ] },
			   { name: 'ownerDocument', items: [ 'Text' ] }
			]
		});
}


 function cancel()
{
	
	window.location="dashboard.jsp";
} 
 
 function previewcampaign() {
	
}
 function savecampaign() {
	
}

</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content" >
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
        <div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="150"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign-red.jpg" width="115" height="150"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="115" height="150"></span></span></div>  
        <div class="contents">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<!-- upload list -->
	<form name="createcampaignform" action="createcampaignwf.htm" method="post" enctype="multipart/form-data">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="create_list" style="padding-left: 80px">
  <tbody>
 <tr>
    <td ><label>Campaign Name <font style="color:red;">*</font>:</label></td>
    <td><input type="text" id = "campaignname"  name="campaignname" size="30"></td>
  </tr>
   <tr>
    <td><label>List Name <font style="color:red;">*</font>:</label></td>
    <td style="font-size: 15px"><%= request.getAttribute("listname")%> </td>
    <td>
     <input type="hidden" id="listname" name="listname" size="30" readonly="readonly" value="<%= request.getAttribute("listname")%> "> 
    </td>
  </tr>
  <tr>
    <td ><label>Description :</label></td>
    <td><textarea rows="2" cols="22" name="description" id="description"></textarea></td>
  </tr>
  <tr>
   <td><label>Subject <font style="color:red;">*</font>:<br></label></td>
   <td><input type="text" id="subject"  name="subject"  size="30"></td>
  </tr>
  <tr>
  <td> <label>Enable Tracking For :</label></td>
    <td><input type="checkbox" name="opentracking" checked="checked">  Opens &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="linktracking" checked="checked">  Links  
</td>
  </tr>
  
  <tr>
  <td><label>Sender <font style="color:red;">*</font>:</label></td>
  <td><input type="text" id = "sendername"  name="sendername" size="30" placeholder="Name">
  <input type="text" id = "senderemail"  name="senderemail" size="30" placeholder="Email"></td>
  </tr>
   <tr>
  <td><label>Reply :</label></td>
  <td><input type="text" id = "replyname"  name="replyname" size="30" placeholder="Name">
  <input type="text" id = "replyemail"  name="replyemail" size="30" placeholder="Email"></td>
  </tr>
  <tr>
   <td ><label>Smtp Server <font style="color:red;">*</font>:</label></td>
    <td><select name="smtpserver" id="smtpserver" style="width: 272px;">
    <option value="" id="">-----Select Smtp Server-----</option></select>
    </td>
  </tr>
 <tr>
   <td><label>Mail Body <font style="color:red;">*</font>:</label></td> <td><label><input type="radio" name="datasource" checked="checked" value="pastehtml"> Paste Html Content</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio"  name="datasource"  value="fileupload">Upload Zip(Mail Body)</label>
</td>
  </tr>
    <tr id="rowfile" style="display: none;">
    <td ><label>Upload Zip File : </label></td>
    <td><input type="file" id="datafile" name="datafile"></td>
  </tr>
  </table>
  <table style="padding-left: 60px" width="100%">
  <tr id="editor">

		<td>&nbsp;</td>	
			<td><textarea  id="campaignMessage" name="campaignMessage" ></textarea></td>
		
		
  </tr>
  </table>
<table style="padding-left: 80px" width="100%">
   <!-- <tr>
    <td width="90"><label>Attachment : </label></td>
    <td style="padding-left: 88px"><input type="file" id="attachement" name="attachement"></td>
  </tr> -->
<tr >
<td><input type="hidden" name="flag" id="flag" value="true"></td>
<td align="right">
<input type="submit"  name="submit" value="Save"  id="submit" onclick="return validate();">
<input type="button"  id="buttonclass"  value="Cancel" onclick="cancel();"></td>
</tr>
</tbody>

</table>
</form>
</div>
  </div>
</div>

<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>