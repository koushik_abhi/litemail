<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<%@ taglib uri="/WEB-INF/ckeditor.tld" prefix="ckeditor" %>
<%@page import="com.ckeditor.CKEditorConfig"%>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Schedule Sendout</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="./css/jquery-ui.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="./js/jquery-1.9.1.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/jquery-ui.js" charset="utf-8"></script>

<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="js/jquery-ui-timepicker-addon.css" />

<style>
.btnsmall {
    background-color: #BD2828;
    border: 1px solid #D83526;
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 1px 0 0 #F29C93 inset;
    color: #FFFFFF;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-size: 13px;
    font-weight: bold;
    margin: 3px;
    padding: 3px 15px;
    text-decoration: none;
    text-shadow: 1px 1px 0 #B23E35;
}

.ui-dialog-title{
  font-size:17px;
  font-family:Tomoha;   
}

</style>
<script>

$(document).ready(function(){
$("input.schedulepopup").click(function(){
$("#schedulecampaign").dialog({
         position: 'center',
         width: 350,
         height: 170,
         title: 'Schedule Campaign',
         modal: true
 });

});

$('.date').each(function(){
    $(this).datetimepicker({
    	controlType: 'select',
    	dateFormat:'dd-mm-yy',
    	timeFormat: 'HH:mm'
    });
    $(".ui-datepicker").css("font-size", "11.5px");
   
});
});
</script>
<script>

function startNow()
{
	actionurl = "startcampaign.htm";
	document.startcampaign.action = actionurl;
	document.startcampaign.submit();
	
}

function testMail()
{
 	var email = document.getElementById("testemail").value;
	if(email == "")
	{
	alert("Please provide EmailId");
	return flase;
	} 
	actionurl="sendpreviewtest.htm";
	document.startcampaign.action = actionurl;
	document.startcampaign.submit();
	
}

function scheduleCampaign()
{

	actionurl = "schedulelater.htm";
	document.schedulelater.action = actionurl;
	document.schedulelater.submit();
	
}



function previouspage()
{
window.location="campaignManagement.jsp";	
}


function closeWin() {
	$("#schedulecampaign").dialog('close');
}

</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="loginscreen.jsp">Logout</a></div>
<div id="navigation">
 <a href="dashboard.jsp" class="left-nav">Dashboard</a>
 <a href="attributeManagement.jsp"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="resetpasswordscreen.jsp">Change Password</a></li>
    <li><a href="userProfile.jsp"> User Profile</a></li>
    <li><a href="emailconfig.jsp"> Email Configuration</a></li>
    </ul>
</div>
 <a class="right-nav" href="smtpManagement.jsp">SMTP Configuration</a>
</div>
</div>
</div>
	<div id="content" >
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
        <div class="workflow"><span><img src="./images/list-active.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/define-fields.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/maping.jpg" width="86" height="127"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/enhance.jpg" width="101" height="127"></span></span></div>
        <div class="contents">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<!-- upload list -->
	<form  name="startcampaign" method="get">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="create_list">
  <tbody>
  <tr>
    <td><label>Campaign Name :</label></td>
    <td id="campaignname"></td>
   <td style="margin-left: 250px"><label>List :</label></td>
   <td id="listnname"></td>
  </tr> 
  <tr>
  <td><label>Schedule Time :</label></td>
  <td><input type="text" name="scheduledatedisply" id="scheduledatedisply" readonly="readonly" style="width: 260px;"></td> 
  
    <td ><label>Description :</label></td>
    <td align="left"><textarea rows="2" cols="22" name="description" id="description"></textarea></td>
  </tr>
  <tr>
  <td><label>Preview Message :</label></td>
  </tr>
  </tbody>
  </table>
  
  <table>
<tr>
<td><textarea rows="18" cols="80" name="previewmessage"></textarea></td>
</tr>
  </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" >
<tr>
<td>&nbsp;</td>
<td ><input size="20" type="text" name="testemail" id="testemail" placeholder="Test Email Id"><input type=button id="buttonclass" value="Send Preview Test" onclick="return testMail()">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input type=button id="buttonclass" value="Back" onclick="previouspage();">
<input type="button"  id="buttonclass" name="startnow" value="Start Now" onclick="  startNow();" >
<input type="button"  id="buttonclass" class="schedulepopup" value="Schedule Later" ></td>
</tr>
</table>
</form><label><strong>Note:</strong> Multiple emails should be separated with comma (",") </label>
</div></div></div>
<table>
<tr>
   <td >
    <div id="schedulecampaign"  style="display: none;" >
<form name="schedulelater" method="get" >
    <table align="center" width="330" >
  <tr>
  <td><strong>Date :</strong></td><td><input type="text" class="date" name="scheduledate" size="21" id="scheduledate" style="height: 10"/></td>
   </tr>
   <tr>
   <td></td>
  <td><input type="button"  class="btnsmall" value="Save" onclick = "scheduleCampaign();"> 
   <input type="button"  class="btnsmall" value="Cancel" onclick="closeWin()"> </td></tr>
  </table>

</form>
</div>
    </td>
</tr>
</table>
<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>
</body></html>