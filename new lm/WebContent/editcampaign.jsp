<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<%@ taglib uri="/WEB-INF/ckeditor.tld" prefix="ckeditor" %>
<%@page import="com.ckeditor.CKEditorConfig"%>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit Campaign</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="./css/jquery-ui.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="./js/jquery-1.9.1.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/jquery-ui.js" charset="utf-8"></script>
<script src="./ckeditor/ckeditor.js"></script>

<script>
var listname= "";
$(document).ready(function() {
$("#rowfile").css("display","none");
$("#editor").css("display","none");
getCampaignInfo();
$('input[name=datasource]:radio').click(function()
		{
			
			 if($(this).val()=="fileupload")
			{
				$("#rowfile").css("display","");
				$("#editor").css("display","none");
			}
			else if($(this).val()=="pastehtml")
			{
				$("#rowfile").css("display","none");
				$("#editor").css("display","");
			}
		});

$("#remove").click(function (){
	
	$("#hidemailbody").css("display","none");
	//$("#campaignMessage").val(campMessage);
	loadEditor();
	//window.setTimeout( 200 );
	$("#showmailbody").show();
	$("#editor").show();
	
	$( "#html" ).prop( "checked", true );
	  $( "#file" ).prop( "checked", false ); 
	
});
$("a#previewmessage").click(function(){
	$("#previewpopup").dialog({
	         position: 'center',
	         width: 720,
	         height: 500,
	         title: 'Campaign Preview',
	         modal: true
	 });
	 $(".ui-dialog-titlebar").hide(); 
	});

			
});

function getListSmtpServer()
{
	$.ajax({
		type: "get",
		url: "getlistsmtpnames.htm",
		dataType: "json",
		success: function( objData )
		{
			
			populateLists(objData.lists);
			populateSmtpServers(objData.smptpServers);
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}

function getCampaignInfo()
{
	var campName= "${campaignname}";
	$.ajax({
		type: "get",
		url: "getcampaigndata.htm?campaignname="+campName,
		dataType: "json",
		success: function( objData )
		{
			renderCampaignData(objData.result);	
			 
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
	}
function renderCampaignData(result)
{
	$("#listname").empty();
	var camp=result.campaignName;
	$("#campaignname").val(camp);
	var listName=result.listName;
	$("#listname").append(result.listName);
	var listData="<option value="+listName+" selected>"+listName+"</option>";
    $(listData).appendTo('#listname');
	$("#description").val(result.description);
	$("#subject").val(result.subject);
	$("#sendername").val(result.fromName);
	$("#senderemail").val(result.fromAddress);
	$("#replyname").val(result.replyName);
	$("#replyemail").val(result.replyTo);
	var smtpName=result.smtpServer;
	var smptpServer="<option value="+smtpName+" selected>"+smtpName+"</option>";
	$(smptpServer).appendTo('#smtpserver');
	$("#opentracking").val(result.enableLinkTracking);
	$("#linktracking").val(result.enableOpenTracking);
	
	
		var htmlFileName=result.htmlFileName;
	var campMessage=result.campaignMessage;   
	
	var attachementFileName=result.attachmentFileName; 
	
	if(htmlFileName != null && htmlFileName.length > 0)
		{
		$("#hidemailbody").show();
		$("#filename").text(htmlFileName);
		$( "#html" ).prop( "checked", false );
		  $( "#file" ).prop( "checked", true ); 
		$("#viewmore").html(campMessage);
		}
	else
		{
		//alert("hi");
		loadEditor(campMessage);
		//$("#campaignMessage").val(campMessage);
		//window.setTimeout( 200 );
		$("#showmailbody").show();
		 $("#editor").show();
		
		
		}
	if(attachementFileName != null && attachementFileName.length > 0)
		{
		$("#attachementfile").text(attachementFileName);
		}
	
	if($("#linktracking").val()=="on")
		{
		$("#linktracking").attr("checked",true);
		}
	if($("#opentracking").val()=="on")
	{
	$("#opentracking").attr("checked",true);
	}
	
	getListSmtpServer();
	
}

	



function renderlistpage()
{
	$.ajax({
		type: "get",
		url: "getlistsmtpnames.htm",
		dataType: "json",
		success: function( objData )
		{
			populateLists(objData.lists);
			populateSmtpServers(objData.smptpServers);
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}

function populateLists(result)
{
	if(result != null && result.length > 0)
	{	
			$.each(result, function(i, value) {
 	          
 	          var existlist =$("select#listname option:contains('" + value + "')").text();
				if(existlist ==value )
					{
					return ;
					}
				else
					{
					 $('#listname').append($('<option>').text(value).attr('value', value));
					}
 	        });		
		}
}
function populateSmtpServers(result)
{
	if(result != null && result.length > 0)
	{	
			$.each(result, function(i, value) {
				var existserver =$("select#smtpserver option:contains('" + value + "')").text();
				if(existserver ==value )
					{
					return ;
					}
				else
					{
 	           $('#smtpserver').append($('<option>').text(value).attr('value', value));
					}
 	          
 	        });		
		}
}




function validate()
{
	
	var campaignName_val = $("#campaignname").val();
	var senderName=$("#sendername").val();
	var senderEmail=$("#senderemail").val();
	var replyEmail=$("#replyemail").val();
	var subjectval=$("#subject").val();
	var listName=$("#listname").val();
	var replyName=$("#replyname").val();
	var smtpserver=$("#smtpserver").val();
	var campaignMessage=$("#campaignMessage").val();
	var datafile=$("#datafile").val();
	var Exp =  /^[a-zA-Z-0-9_ ]+$/;
	var emailExp= /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
	
	if(campaignName_val == null || campaignName_val =="")
	{
		alert("Please enter campaign name");
		return false; 
	}
	
	if(!campaignName_val.match(Exp)){
	    alert("Please enter valid campaign name (A-Z, a-z, 0-9, _)");
	return false;
	}
	
     if(listName == "" || listName == null)
 		 {
 		 alert("please select listname");
 		 return false;
 		 }
 	 if(subjectval == "" || subjectval == null)
 		 {
 		 alert("please enter subject");
 		 return false;
 		 }
 
 	 if(senderName == null && senderName =="")
 	 {
 		 alert("please enter sender name");
 		 return false;
 	 }
 	if(!isAlphabet(sendername, "Please enter valid sender name (A-Z, a-z)"))
	{
		return false;
	}
 	 
 	if(senderEmail == null || senderEmail == "")
 	 {
 	  alert("please enter Email ");
 	  return false;
 	 } 
 	
 	if(!senderEmail.match(emailExp))
	{
			  alert("Please enter valid valid email");
		return false;
	} 
 	if(replyName != ""){
 	 	if(!isAlphabet(replyname, "Please enter valid reply name (A-Z, a-z)"))
 		{
 			return false;
 		}
 	 	}
	if(replyEmail != ""){
	 	if( !replyEmail.match(emailExp))
		{
				  alert("Please enter valid reply email");
			return false;
		}  
	}
	 	
 	 if(smtpserver == null || smtpserver == "")
 		 {
 		 alert("Please select valid smtpserver");
 		 return false;
 		 }
 	 /* alert(campaignMessage);
 	 if(campaignMessage == null || datafile == null)
 		 {
 		 alert("please provide mail body");
 		 } */
	 else{
		document.createcampaigform.submit();
	}  
	
}


 function cancel()
{
	
	window.location="campaignManagement.jsp";
} 
 function closepopup()
 {
 	$("#previewpopup").dialog('close');
 } 
 
 function loadEditor(message)
 {
	 $("#campaignMessage").val(message);
	 CKEDITOR.replace('campaignMessage', {
			uiColor: '#CFD1CF',
			allowedContent: true,
			height:'350px',
			toolbar: [
				{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Preview', 'Print', '-', 'Templates' ] },
				{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
				{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
	               { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	               { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
	                    '/',
	              { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
	              { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
	              { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
	              { name: 'others', items: [ '-' ] },
	              { name: 'about', items: [ 'About' ] },
			   { name: 'ownerDocument', items: [ 'Text' ] }
			]
		});
 }


</script>

		
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content" >
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
        <div class="contents">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<!-- upload list -->
	<form  name="editcampaignform" action="editcampaign.htm" method="post" enctype="multipart/form-data">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="create_list" style="padding-left: 80px">
  <tbody>
 <tr>
    <td ><label>Campaign Name <font style="color:red;">*</font>:</label></td>
     <td><input type="text" id = "campaignname"  name="campaignname" size="30" readonly="readonly"></td>
  </tr>
   <tr >
    <td ><label>List Name <font style="color:red;">*</font>:</label></td>
    <td><select name="listname" id="listname" style="width: 272px;">
    <option value="" id="">-----Select List Name-----</option></select>
    </td>
  </tr>
  <tr>
    <td ><label>Description :</label></td>
    <td><textarea rows="2" cols="22" name="description" id="description"></textarea></td>
  </tr>
  <tr>
   <td><label>Subject <font style="color:red;">*</font>:<br></label></td>
   <td><input type="text" id="subject"  name="subject"  size="30"></td>
  </tr>
  <tr>
  <td> <label>Enable Tracking :</label></td>
    <td><input type="checkbox" name="opentracking" id="opentracking">  Opens &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="linktracking" id="linktracking">  Links  
</td>
  </tr>
  
  <tr>
  <td><label>Sender <font style="color:red;">*</font>:</label></td>
  <td><input type="text" id = "sendername"  name="sendername" size="30" placeholder="Name">
  <input type="text" id = "senderemail"  name="senderemail" size="30" placeholder="Email"></td>
  </tr>
   <tr>
  <td><label>Reply :</label></td>
  <td><input type="text" id = "replyname"  name="replyname" size="30" placeholder="Name">
  <input type="text" id = "replyemail"  name="replyemail" size="30" placeholder="Email"></td>
  </tr>
    <tr>
   <td ><label>Smtp Server <font style="color:red;">*</font>:</label></td>
    <td><select name="smtpserver" id="smtpserver" style="width: 272px;">
    <option value="" id="">-----Select Smtp Server-----</option></select>
    </td>
  </tr>
  <tr id="hidemailbody" style="display: none">
   								<td><label>Mail Body :(zip file only)</label></td> 
								<td><label id="filename"></label>&nbsp;&nbsp;&nbsp;<a href="#" id="previewmessage" style="cursor: pointer;text-decoration: underline;"><strong>Mail Preview</strong></a>&nbsp;&nbsp;&nbsp; <label id="remove" style="cursor: pointer;text-decoration: underline;">remove</label></td>
  							</tr>
  
 <tr style="display: none;" id="showmailbody">
   <td><label>Mail Body <font style="color:red;">*</font>:</label></td> <td><label><input type="radio" id="html" name="datasource"  value="pastehtml" checked="checked"> Paste Html Content</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio" id="file"  name="datasource"  value="fileupload" >Upload Zip(Mail Body)</label>
</td>
  </tr>
    <tr id="rowfile" style="display: none;">
    <td ><label>Upload Zip File : </label></td>
    <td><input type="file" id="datafile" name="datafile"></td>
  </tr>
  </table>
  <table style="padding-left: 60px" width="100%">
  <tr id="editor">

		<td>&nbsp;</td>	
			<td><textarea  id="campaignMessage" name="campaignMessage" ></textarea></td>
		
  </tr>
  </table>
<table style="padding-left: 80px" width="100%">
   <!-- <tr>
    <td width="90"><label>Attachment : </label></td>
    <td style="padding-left: 69px"><input type="file" id="attachement" name="attachement"></td>
  </tr> -->
<tr style="padding-left: 43px">
<td><input type="hidden" name="flag" id="flag" value="true"></td>
<td align="right">
<input type="submit"  name="submit" value="Save"  id="submit" onclick="return validate();">
<input type="button"  id="buttonclass"  value="Cancel" onclick="cancel();"></td>
</tr>
</tbody>

</table>
								<div id="previewpopup" style="display: none;">
   						  		<h1 style="font-size:15px;float: right;color:#000;width:30px; cursor: pointer;margin-right: -10px  title="Close" onclick="closepopup();">&nbsp;&nbsp;X</h1>
   						 		<div id="viewmore" style="background-color: white;"></div>
   						 		</div>
 
</form>
</div>
  </div>
</div>

<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>