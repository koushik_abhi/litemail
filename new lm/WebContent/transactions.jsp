<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><title>Transactions</title>

<meta http-equiv="Content-type" content="text/html; charset=UTF-8"></head>

<link href='uitrans/jquery-ui-1.8.4.custom.css' rel='stylesheet' type='text/css'/>
<script type="text/javascript" src="uitrans/g1.js" ></script>


<link rel="stylesheet" type="text/css" href="css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="uitrans/style1.css" media="all">


<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<link href="uitrans/main.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="uitrans/validationEngine.css" type="text/css">

<!-- Dynamic Behaviour -->
<script src="uitrans/jquery-1.8.2.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>

<!--
<script type="text/javascript" 
  src="js/jquery.validate.js">
</script>	
-->
<script type="text/javascript" src="js/jquery-ui.js"></script>
<link rel="stylesheet" href="uitrans/jquery-ui.css" />


<script src="uitrans/validator-en.js" type="text/javascript" charset="utf-8"></script>
<script src="uitrans/validator.js" type="text/javascript" charset="utf-8"></script>


<STYLE TYPE="text/css" media="all">
		.ui-autocomplete { 
			position: absolute; 
			cursor: default; 
			height: 150px; 
			width:200px;
			overflow-y: scroll; 
			overflow-x: hidden;
			text-align:left;
		}
	.ui-datepicker{
	 width:17.5em;
	}
	</STYLE>




<script type="text/javascript">
var pagenum = 1;
var pagenum1 =1;
  $(document).ready(function() {
   
	  $(".tabLink").each(function(){
      $(this).click(function(){
        tabeId = $(this).attr('id');
        if(tabeId =="cont-1")
        {
        	       	
        	$("#listrowsPage").empty();
        	$("#listrowstransPage").empty();
       	   	$("#listrows").empty();
       	 	$("#listrowstrans").empty();   		
        	getAvailablePartners();
        	document.getElementById("error").innerHTML="";
        
        }
        else
        {	
        	
        	$("#listrowsPage").empty();
        	$("#listrowstransPage").empty();
       	   	$("#listrows").empty();
       	 	$("#listrowstrans").empty();    
        	 getAvailablePartnersTrans();
        	 document.getElementById("error").innerHTML="";
        	 
        }
        $(".tabLink").removeClass("activeLink");
        $(this).addClass("activeLink");
        $(".tabcontent").addClass("hide");
        $("#"+tabeId+"-1").removeClass("hide");   
        return false;	  
      });
    }); 
	  
      $("#closebtn").click(function () {
        $("#dlg").hide('800', "swing", function () { $("#bkg").fadeOut("500"); });
      });
      $(".opn").click(function () {
        if (document.getElementById('bkg').style.visibility == 'hidden') {
          document.getElementById('bkg').style.visibility = '';
          $("#bkg").hide();
        }
        if (document.getElementById('dlg').style.visibility == 'hidden') {
          document.getElementById('dlg').style.visibility = '';
          $("#dlg").hide();
        }
        $("#bkg").fadeIn(500, "linear", function () { $("#dlg").show(800, "swing"); });
      });    
	getAvailablePartners();
	
  });
  
  function getAvailablePartners()
  {
	
	  $.ajax({
  		type: "get",
  		url: "getAllPartnerNames.htm",
  		dataType: "json",
  		success: function( objData )
  		{
  			
  			pupulatePartners(objData);
  			
  		},
  		error: function(){
  			alert( "An error " );
  		}
  	});
  				
  }
  function getAvailablePartnersTrans()
  {
	
	  $.ajax({
  		type: "get",
  		url: "getAllPartnerNames.htm",
  		dataType: "json",
  		success: function( objData )
  		{
  			
  			pupulatePartnersTrans(objData);
  			
  		},
  		error: function(){
  			alert( "An error occurred tred" );
  		}
  	});
  				
  }

  function getAvailableUsers(partner)
  {
  	if(partner =="All")
  		return;
  	$.ajax({
  		type: "get",
  		url: "getAllUserNames.htm?partner="+partner,
  		dataType: "json",
  		success: function( objData )
  		{
  			
  			pupulateUsers(objData);
  						
  		},
  		error: function(){
  			alert( "An error occurred 111" );
  		}
  	});
  				
  }
  function getAvailableUsersTrans(partner)
  {
  	if(partner =="All")
  		return;
  	$.ajax({
  		type: "get",
  		url: "getAllUserNames.htm?partner="+partner,
  		dataType: "json",
  		success: function( objData )
  		{
  			pupulateUsersTrans(objData);
  						
  		},
  		error: function(){
  			alert( "An error occurred tr" );
  		}
  	});
  				
  }
  function getAvailableUsersPop(partner)
  {
  	
  	$.ajax({
  		type: "get",
  		url: "getAllUserNames.htm?partner="+partner,
  		dataType: "json",
  		success: function( objData )
  		{
  			pupulateUsersPop(objData);
  						
  		},
  		error: function(){
  			alert( "An error occurred " );
  		}
  	});
  				
  }
  function getUserBalance(user)
  {
  	
  	$.ajax({
  		type: "get",
  		url: "getuserbalance.htm?user="+user,
  		dataType: "json",
  		success: function( objData )
  		{
  			var obj = objData[0];
  			document.getElementById("tobalance").value   = obj.balance;
  						
  		},
  		error: function(){
  			alert( "An error occurred " );
  		}
  	});
  				
  }

  function pupulateUsersPop(users)
  {
  	$("input#touser").autocomplete({
  	    minLength: 0,
  	    source:users,
  	    autoFocus: true,
  	    scroll: true,
  	    multiselect:true,
  	    select: function(event, ui) {
	    	
	    	getUserBalance(ui.item.value);
        }
  	}).focus(function() {
  	    $(this).autocomplete("search", "");
  	}).live("blur", function(event) {
  	    var autocomplete = $(this).data("autocomplete");
  	    var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i");
  	    autocomplete.widget().children(".ui-menu-item").each(function() {
  	    	
  	        //Check if each autocomplete item is a case-insensitive match on the input
  	        var item = $(this).data("item.autocomplete");
  	        if (matcher.test(item.label || item.value || item)) {
  	            //There was a match, lets stop checking
  	            autocomplete.selectedItem = item;
  	            return;
  	        }
  	    });
  	    //if there was a match trigger the select event on that match
  	    if (autocomplete.selectedItem) {
  	        autocomplete._trigger("select", event, {
  	            item: autocomplete.selectedItem
  	            
  	        });
  	    //there was no match, clear the input
  	    } else {
  	        $(this).val('');
  	    }
  	});
  	
  	
  }
  
  function pupulateUsers(users)
  {
  	$("input#user").autocomplete({
  	    minLength: 0,
  	    source:users,
  	    autoFocus: true,
  	    scroll: true,
  	    multiselect:true,
  	    
  	}).focus(function() {
  	    $(this).autocomplete("search", "");
  	}).live("blur", function(event) {
  	    var autocomplete = $(this).data("autocomplete");
  	    var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i");
  	    autocomplete.widget().children(".ui-menu-item").each(function() {
  	    	
  	        //Check if each autocomplete item is a case-insensitive match on the input
  	        var item = $(this).data("item.autocomplete");
  	        if (matcher.test(item.label || item.value || item)) {
  	            //There was a match, lets stop checking
  	            autocomplete.selectedItem = item;
  	            return;
  	        }
  	    });
  	    //if there was a match trigger the select event on that match
  	    if (autocomplete.selectedItem) {
  	        autocomplete._trigger("select", event, {
  	            item: autocomplete.selectedItem
  	            
  	        });
  	    //there was no match, clear the input
  	    } else {
  	        $(this).val('');
  	    }
  	});
  	
  	
  }
 
  function pupulateUsersTrans(users)
  {
  	$("input#usertrans").autocomplete({
  	    minLength: 0,
  	    source:users,
  	    autoFocus: true,
  	    scroll: true,
  	    multiselect:true,
  	    
  	}).focus(function() {
  	    $(this).autocomplete("search", "");
  	}).live("blur", function(event) {
  	    var autocomplete = $(this).data("autocomplete");
  	    var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i");
  	    autocomplete.widget().children(".ui-menu-item").each(function() {
  	    	
  	        //Check if each autocomplete item is a case-insensitive match on the input
  	        var item = $(this).data("item.autocomplete");
  	        if (matcher.test(item.label || item.value || item)) {
  	            //There was a match, lets stop checking
  	            autocomplete.selectedItem = item;
  	            return;
  	        }
  	    });
  	    //if there was a match trigger the select event on that match
  	    if (autocomplete.selectedItem) {
  	        autocomplete._trigger("select", event, {
  	            item: autocomplete.selectedItem
  	            
  	        });
  	    //there was no match, clear the input
  	    } else {
  	        $(this).val('');
  	    }
  	});
  	
  	
  }

  function pupulatePartners(partners)
  {
	
	$("input#partner").autocomplete({
  	    minLength: 0,
  	    maxLength: 40,
  	    source:partners,
  	    autoFocus: true,
  	    scroll: true,
  	    multiselect:true,
  	    select: function(event, ui) {
  	    	
  	    	getAvailableUsers(ui.item.value);
        }
  	}).focus(function() {
  		  $(this).autocomplete("search", "");
  	}).live("blur", function(event) {
  	    var autocomplete = $(this).data("autocomplete");
  	    var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i");
  	    autocomplete.widget().children(".ui-menu-item").each(function() {
  	    	
  	        //Check if each autocomplete item is a case-insensitive match on the input
  	        var item = $(this).data("item.autocomplete");
  	        if (matcher.test(item.label || item.value || item)) {
  	            //There was a match, lets stop checking
  	            autocomplete.selectedItem = item;
  	            return;
  	        }
  	    });
  	    //if there was a match trigger the select event on that match
  	    if (autocomplete.selectedItem) {
  	        autocomplete._trigger("select", event, {
  	            item: autocomplete.selectedItem
  	           
  	        });
  	    //there was no match, clear the input
  	    } else {
  	        $(this).val('');
  	    }
  	    
  	});	
  	
  }
  
  function pupulatePartnersTrans(partners)
  {
	
	$("input#partnertrans").autocomplete({
  	    minLength: 0,
  	    maxLength: 40,
  	    source:partners,
  	    autoFocus: true,
  	    scroll: true,
  	    multiselect:true,
  	    select: function(event, ui) {
  	    	getAvailableUsersTrans(ui.item.value);
          }
  	}).focus(function() {
  		  $(this).autocomplete("search", "");
  	}).live("blur", function(event) {
  	    var autocomplete = $(this).data("autocomplete");
  	    var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex($(this).val()) + "$", "i");
  	    autocomplete.widget().children(".ui-menu-item").each(function() {
  	    	
  	        //Check if each autocomplete item is a case-insensitive match on the input
  	        var item = $(this).data("item.autocomplete");
  	        if (matcher.test(item.label || item.value || item)) {
  	            //There was a match, lets stop checking
  	            autocomplete.selectedItem = item;
  	            return;
  	        }
  	    });
  	    //if there was a match trigger the select event on that match
  	    if (autocomplete.selectedItem) {
  	        autocomplete._trigger("select", event, {
  	            item: autocomplete.selectedItem
  	           
  	        });
  	    //there was no match, clear the input
  	    } else {
  	        $(this).val('');
  	    }
  	    
  	});	
  	
  }
  function Search()
  {
	  
  	var partner = document.getElementById("partner").value;
  	var user = document.getElementById("user").value;
  	//var fromdate = document.getElementById("fromdate").value;
  	//var todate = document.getElementById("todate").value;
  	searchRecords(partner, user);

  }
  
  function ExportUser()
  {
  	var partner = document.getElementById("partner").value;
  	if(partner == null || partner =="")
	{
		partner = "All";
	}
  	var user = document.getElementById("user").value;
  	$.ajax({
  		type: "get",
  		url: "getalluserpartnercreditsexport.htm?partner="+partner+"&user="+user,
  		dataType: "json",
  		success: function( objData )
  		{
  			//sucessfully download data
  		},
  		error: function(data){
  			alert("no data found");
  			
  		}
  	});
  	
  }
  
  function ExportTrans()
  {
  	var partner = document.getElementById("partnertrans").value;
  	if(partner == null || partner =="")
	{
		partner = "All";
	}
  	var user = document.getElementById("usertrans").value;
  	var a = document.getElementById("transexport");
  	var fromdate = document.getElementById("fromdate").value;
  	var todate = document.getElementById("todate").value;
  	a.href = "getalltransactionsexport.htm?partner="+partner+"&user="+user+"&fromdate="+fromdate+"&todate="+todate;
  	
  }
  
  function SearchTrans()
  {
  	var partner = document.getElementById("partnertrans").value;
  	var user = document.getElementById("usertrans").value;
  	var fromdate = document.getElementById("fromdate").value;
  	var todate = document.getElementById("todate").value;
  	if(fromdate != "" && (todate == null || todate ==""))
  	{
  		alert("please select fromdate and todate ");
  		return;
  	}
  	else if(todate != "" && (fromdate == null || fromdate ==""))
  	{
  		alert("please select fromdate and todate ");
  		return;
  	}else if(todate != "" && fromdate != "")
  	{
	  	if ($.datepicker.parseDate('yy-mm-dd', fromdate) > $.datepicker.parseDate('yy-mm-dd', todate)) {
	
	  	   alert('please select valid date range, from date :' + $.datepicker.parseDate('yy-mm-dd', fromdate) + 'is later than todate :' + $.datepicker.parseDate('yy-mm-dd', todate));
	       return;
	
	 	}
  	}
  	searchTransRecords(partner, user, fromdate, todate);
  }
  
  function searchRecords(partner, user)
  {
	if(partner == null || partner =="")
	{
		partner = "All";
	}
	removeTable("listrows");
	$("#listrowsPage").empty();
	
	pagenum = 1;
  	$.ajax({
  		type: "get",
  		url: "getalluserpartnercredits.htm?pagenumber="+pagenum+"&partner="+partner+"&user="+user,
  		dataType: "json",
  		success: function( objData )
  		{
  			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
  		},
  		error: function(data){
  			alert("no data found" +data);
  			
  		}
  	});
  	
  }
  
  function rendernextlistpage()
  {
  	$("#listrows").empty();
  	$("#listrowsPage").empty();
  	
  	var partner = document.getElementById("partner").value;
  	var user = document.getElementById("user").value;
  	
  	pagenum =pagenum+1;
  	$.ajax({
  		type: "get",
  		url: "getalluserpartnercredits.htm?pagenumber="+pagenum+"&partner="+partner+"&user="+user,
  		dataType: "json",
  		success: function( objData )
  		{
  			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
  		},
  		error: function(data){
  			
  			alert("data " +data);
  		}
  	});
  	
  }

  function renderprevlistpage()
  {
  	$("#listrows").empty();
	$("#listrowsPage").empty();
	
  	var partner = document.getElementById("partner").value;
  	var user = document.getElementById("user").value;
  	pagenum =pagenum-1;
  	$.ajax({
  		type: "get",
  		url: "getalluserpartnercredits.htm?pagenumber="+pagenum+"&partner="+partner+"&user="+user,
  		dataType: "json",
  		success: function( objData )
  		{
  			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
  		},
  		error: function(data){
  			
  			alert("data " +data);
  		}
  	});
  	
  }

  function rendernextlistpage1()
  {
  	$("#listrowstrans").empty();
	$("#listrowstransPage").empty();
	
	pagenum1 =pagenum1+1;
  	var partner = document.getElementById("partnertrans").value;
  	var user = document.getElementById("usertrans").value;
  	var fromdate = document.getElementById("fromdate").value;
  	var todate = document.getElementById("todate").value;
  	
  
  	$.ajax({
  		type: "get",
  		url: "getalltransactions.htm?pagenumber="+pagenum1+"&partner="+partner+"&user="+user+"&fromdate="+fromdate+"&todate="+todate,
  		dataType: "json",
  		success: function( objData )
  		{
  			renderlisttranstable(objData.result, objData.pagenumber, objData.totalpages);	
  		},
  		error: function(data){
  			
  			alert("data " +data);
  		}
  	});
  	
  }

  function renderprevlistpage1()
  {
  	$("#listrowstrans").empty();
  	$("#listrowstransPage").empty();
  	
  	pagenum1 =pagenum1-1;
  	var partner = document.getElementById("partnertrans").value;
  	var user = document.getElementById("usertrans").value;
  	var fromdate = document.getElementById("fromdate").value;
  	var todate = document.getElementById("todate").value;
  	
  	
  	$.ajax({
  		type: "get",
  		url: "getalltransactions.htm?pagenumber="+pagenum1+"&partner="+partner+"&user="+user+"&fromdate="+fromdate+"&todate="+todate,
  		dataType: "json",
  		success: function( objData )
  		{
  			renderlisttranstable(objData.result, objData.pagenumber, objData.totalpages);	
  		},
  		error: function(data){
  			
  			alert("data " +data);
  		}
  	});
  	
  }



  
  function removeTransTable(id)
  {
      var tbl = document.getElementById(id);
     
     // if(tbl)
      {
    	  $('#listrowstrans').empty();
    	  $("#listrowstransPage").empty();
    	     	      		
    			   
      }
  }
  function removeTable(id)
  {
      var tbl = document.getElementById(id);
     
      //if(tbl)
      {
    	  $('#listrows').empty();
    	  $("#listrowsPage").empty();
    	 
    	      		
    			   
      }
  }
  
  function searchTransRecords(partner, user, fromdate, todate)
  {
	if(partner == null || partner =="")
	{
		partner = "All";
	}
	removeTransTable("listrowstrans");
	$("#listrowstransPage").empty();
	$("#listrowsPage").empty();
	pagenum1 = 1;
	$.ajax({
  		type: "get",
  		url: "getalltransactions.htm?pagenumber="+pagenum1+"&partner="+partner+"&user="+user+"&fromdate="+fromdate+"&todate="+todate,
  		dataType: "json",
  		success: function( objData )
  		{
  			renderlisttranstable(objData.result, objData.pagenumber, objData.totalpages);	
  		},
  		error: function(data){
  			alert("no data found" +data);
  			
  		} 
  	});
  	
  }

  function renderlisttable(objData, pagenumber, totalpages)
  {
	pagenum = pagenumber;
	$("#listrowstransPage").empty();
	$("#listrowsPage").empty();
	$("#listrows").append('<table class="courses_table1">');
  	if(objData != null && objData.length > 0)
  	{ 
  		//$('#balancecrd').remove();
  		//var list = $('#courses_table');
  		//alert("hello"+list);
  		//if(list != null  )
  		//	list.remove();
  		
  		$("#listrows").append('<tr style="background-color: rgb(238, 238, 238);" id="User Name"><th width="100"><strong>User Name</strong></th><th width="100"><strong>Partner Name</strong></th><th width="100"><strong>Role</strong></th><th width="100"><strong>Credits</strong></th><th width="100"><strong>Debits</strong></th><th width="100"><strong>Balance</strong></th><th width="100"><strong>Updated Time</strong></th><th width="100"><strong>Created By</strong></th></tr>');
  		var cont = 1;
  		for(var count =0; count < objData.length; count++)
  		{
  			var row = objData[count];
  			renderBalanceRowInListTable(row, cont);
  			cont++;
  		}
  		var page = pagenumber +' out of '+ totalpages; 
  		$("#listrowsPage").append('<tr align:right>');
		if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$("#listrowsPage").append('<td align:center >'+page+'</td><td align:right><input type="submit"  style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"><input type="submit"   style="float: right; margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$("#listrowsPage").append('<td align:center >'+page+'</td><td align:right><input type="submit"   style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$("#listrowsPage").append('<td align:center >'+page+'</td><td align:right><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
			
		}
		$("#listrowsPage").append('</tr>');
  		
  		

  	}
  	else
  	{
  		$("#listrows").append('<tr><td style="color:red">No Records found.</td></tr>');
  	}
  	$("#listrows").append('</table>');
  }
  
  function renderlisttranstable(objData, pagenumber, totalpages)
  {
	  $("#listrowstransPage").empty();
		$("#listrowsPage").empty();
	pagenum1 = pagenumber;
	$("#listrowstrans").append('<table class="courses_table1">');
  	if(objData != null && objData.length > 0)
  	{
  		
  		$("#listrowstrans").append('<tr style="background-color: rgb(238, 238, 238);" id="Transaction Id"><th width="100"><strong>Transaction Id</strong></th><th width="100"><strong>Partner Name</strong></th><th width="100"><strong>User Name</strong></th><th width="100"><strong>Trans Type</strong></th><th width="100"><strong>List Name</strong></th><th width="100"><strong># Email</strong></th><th width="100"><strong># Sent Email</strong></th><th width="100"><strong>Credits Used</strong></th><th width="100"><strong>Trans Time</strong></th></tr>');
  		var cont = 1;
  		for(var count =0; count < objData.length; count++)
  		{
  			var row = objData[count];
  			renderTransRowInListTable(row, cont);
  			cont++;	
  		}
  		$("#listrowstransPage").append('<tr align:right>');
  		var page = 'Page No ' +pagenumber +' Out of '+ totalpages +' Pages'; 
		if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$("#listrowstransPage").append('<td align:right><font color=red>'+page+'</font><input type="submit"  style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage1()"><input type="submit"   style="float: right; margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage1()"></td>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$("#listrowstransPage").append('<td align:right><font color=red>'+page+'</font><input type="submit"   style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage1()"></td>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$("#listrowstransPage").append('<td align:right><font color=red>'+page+'</font><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage1()"></td>');
			
		}
		$("#listrowstransPage").append('</tr>');
  		
  		
  	}
  	else
  	{
  		$("#listrowstrans").append('<tr><td style="color:red">No Records found.</td></tr>');
  	}
  	$("#listrowstrans").append("</table>");
  	
  }
  
  
  function renderBalanceRowInListTable(row, cont)
  {
  	var userId = row.userId;
  	var partnerName = row.partnerName;
  	var role = row.role;
  	var credits = row.credits;
  	var debits = row.debits;
  	var balance = row.balance;
	var updatedTime = row.updatedTime;
	//updatedTime =  mobileNo.split(" ").join("");
  	var createdBy = row.createdBy;
  	var transfer = "transfer";																																																																					
  	var strarray = partnerName+"_split_"+userId +"_split_"+balance;
	var iseven = cont%2;
  	if(iseven == 0)
  	{
  		$("#listrows").append('<tr style="text-align:left; background-color:#FDEEB0" id="'+userId+'"><td width="100">'+userId+'</td><td width="100">'+partnerName+'</a></td><td width="100">'+role+'</a></td><td width="100">'+credits+'</a></td><td width="100">'+debits+'</a></td><td width="100">'+balance+'</a></td><td	width="255">'+updatedTime+'</a></td><td width="100">'+createdBy+'</a></td></tr>');
  	}else
  	{
  		$("#listrows").append('<tr style="text-align:left;  background-color:#F4F3F1" id="'+userId+'"><td width="100">'+userId+'</td><td width="100">'+partnerName+'</a></td><td width="100">'+role+'</a></td><td width="100">'+credits+'</a></td><td width="100">'+debits+'</a></td><td width="100">'+balance+'</a></td><td width="255">'+updatedTime+'</a></td><td width="100">'+createdBy+'</a></td></tr>');
  	
  	}
  }
  
  function popupDialog(stringarry)
  {
	
	  $('#credit_form').data("id",stringarry).dialog({
			centered: true, 
			movable:true,
			width:'550',
			height:'400',
			title:'Transfer Credits',
			open: function(event, ui) {
				
				 var $led = $("#credit_form");
				 var stringarry = $led.data('id');
				 var dir = stringarry.split("_split_");
				 var partner = dir[0];
				 document.getElementById("fromuser").value = dir[1];
				 document.getElementById("frombalance").value = dir[2];
				 getAvailableUsersPop(partner);				
  		}
		});
	  
  }
  function renderTransRowInListTable(row, cont)
  {
	
  	var transId = row.transId;
  	var partnerName = row.partnerName;
  	var userId = row.userId;
  	var transType = row.transType;
  	var listName = row.listName;
  	var actualNoOfRecords = row.actualNoOfRecords;
	var actualNoOfFields = row.actualNoOfFields;
  	var debits = row.debits;
  	var createdTime = row.createdTime.trim();
    //if(transId != null && transId != 'undefined')
    var iseven = cont%2;
  	if(iseven == 0)
  	{
  		$("#listrowstrans").append('<tr style="text-align:left; background-color:#FDEEB0" id="'+transId+'"><td width="100">'+transId+'</td><td width="100">'+partnerName+'</a></td><td width="100">'+userId+'</a></td><td width="100">'+transType+'</a></td><td width="100">'+listName+'</a></td><td width="100">'+actualNoOfRecords+'</a></td><td width="100">'+actualNoOfFields+'</a></td><td width="100">'+debits+'</a></td><td width="300">'+createdTime+'</a></td></tr>');
  	}else
  	{
  		$("#listrowstrans").append('<tr style="text-align:left; background-color:#F4F3F1" id="'+transId+'"><td width="100">'+transId+'</td><td width="100">'+partnerName+'</a></td><td width="100">'+userId+'</a></td><td width="100">'+transType+'</a></td><td width="100">'+listName+'</a></td><td width="100">'+actualNoOfRecords+'</a></td><td width="100">'+actualNoOfFields+'</a></td><td width="100">'+debits+'</a></td><td width="300">'+createdTime+'</a></td></tr>');
  	  		
  	}
  }

  function closeWin()
  {
  	$("#credit_form").dialog('close');
  	
  }
  
  function validateCredits()
  {
  	
  	var fromuser = document.getElementById("fromuser").value ;
  	var touser = document.getElementById("touser").value ;
  	if(touser == null || touser.length <=0)
  	{
  		alert("to user should not be empty");
  	}
  	if(fromuser == touser)
  	{
  		alert("transfer credits across different users");
  		return false;
  	}
  	
  	var cr = document.getElementById("credits").value ;
  	if(cr == null || cr.length <=0)
  	{
  		alert("credits  should not be empty");
  	}
  	
  	if(cr !=null && cr.indexOf("-") > 0)
  	{
  		alert("transfer credits should be possitive number");
  		return false;
  	}
  	
  	
  	var frombal = document.getElementById("frombalance").value ;
  	
  	/*if(frombal < lit)
  	{
  		alert(" from balance is lesser than the credits to be transfered ");
  		return false;
  	}*/
  	document.credit_form.submit();
  	$("#credit_form").dialog('close');
  		
  }
</script>





	<script type="text/javascript">
/*function to load a list*/
function loadlist(selobj,url,nameattr)
{
    $(selobj).empty();
    $.getJSON(url,{},function(data)
    {
        $.each(data, function(i,obj)
        {
        	alert(obj);
            $(selobj).append($('<option></option>').val(obj[nameattr]).html(obj[nameattr]));
        });
    });
}

//$(function()
//{ 
 //  loadlist($('select#partner').get(0), 'getAllPartnerNames.htm','partnername');

//});
</script>
<script type="text/javascript">
function loaduserlist(selobj,url,nameattr)
{
	$(selobj).empty();
    $.getJSON(url,{},function(data)
    {
        $.each(data, function(i,obj)
        {	alert("obj");
            $(selobj).append($('<option></option>').val(obj[nameattr]).html(obj[nameattr]));
        });
    });
}
//etAllUserNames.htm?partner="+partner,
//$(function()
//{ 
 //  loaduserlist($('select#user').get(0), 'getAllUserNames.htm?partner='+partner,'name');

//});
</script>
<script>
$(document).ready(function(){		  
	$('.date').each(function(){
	    $(this).datepicker({
	    	dateFormat: 'yy-mm-dd',
			  changeMonth: true,
			  changeYear: true,
			  showAnim: 'slide'
	    });
	});
		});
</script>
<body>
<div id="wrapper">
	<div id="header"><div id="logo"> <a href="#"><img src="images/kenscio-logo.jpg"  border="0" width="70" /></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showtransactions.htm"> Transactions</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    <li><a href="showemailconfig.htm"> Email Configuration</a></li>
    </ul>
</div>
 <a class="right-nav" href="showsmtpManagement.htm"> SMTP Configuration</a>
</div>
</div>
</div>
	<div id="content">
    <div class="contents">
     <div id ="error" align="center" style="color:red">${errorMessage}</div><br>
    <div id="loading-popup-box" style="display: none;"></div>
		
  <div class="tab-box"> 
    <a href="javascript:;" class="tabLink activeLink" id="cont-1">Balance Transfer</a> 
    <a href="javascript:;" class="tabLink " id="cont-2">Transactions</a> 
   
 </div>
<div class="tabcontent" id="cont-1-1">

 <div style="height:12px;"></div> 

 <label>Partner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="partner" name="partner" value="" size="22"></label>&nbsp;
 <label>User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="user" name="user" value="" size="22"></label>&nbsp;&nbsp;&nbsp;
 <input type="submit" name="submit" id="Search" onclick="Search()" value="Search">&nbsp;

<div id="listrows">
  <table id = "courses_table" class="courses_table" cellspacing="1">
        </table>
 </div>
 
<div class="blockbkg" id="bkg" style="visibility: hidden;">
    <div class="cont" id="dlg" style="visibility: hidden;">
      <div class="closebtn" title="Close" id="closebtn"><img src="uitrans/images/cancel.png" alt="Close" title="Close"></div>
<strong>Change Credits</strong><br/><br/>
<div id="credit_form" >
<fieldset>
<form id = "credit_form" name="credit_form" method="get" action="transfercreditui.htm">
  <table align="center">
   <tr>
   <td>	From User</td>
   <td><td><input type="text" value="" readonly name="fromuser" size="50" id="fromuser"/></td>
   </tr>
   <tr>
   <td>	Current Balance</td>
   <td><td><input type="text" value="" readonly name="frombalance" size="50" id="frombalance"/></td>
   </tr>
    <tr>
   <td>	To User</td>
   <td><td><input type="text" value="" name="touser" size="50" id="touser"/></td>
   </tr>
    <tr>
   <td>	Current Balance</td>
   <td><td><input type="text" value="" readonly name="tobalance" size="50" id="tobalance"/></td>
   </tr>
   <tr>
   <td>	Credits To Transfer:</td>
   <td><td><input type="text" value="" name="credits" size="50" id="credits"/></td>
   </tr>
   <tr>
   <td><td><input type="hidden" value="" name="partner" size="50" id="partner"/></td>
   </tr>
  </table>
<br><br>
</form>
<div align="center">
<input class="btnsmall" type="submit" onClick="return validateCredits()" value="Save">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btnsmall" type="submit" onClick="closeWin()" value="Cancel">
 
  </div>
</fieldset>
</div>

<!-- <form action="addCreditChange.htm" method="get" id="credit_form">
<label>Partner Name</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="clone-name" type="text" name="partnername" value="" readonly><br/>
<label>User Name</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="clone-name" type="text" name="username" value="" readonly><br/>
<label>Current Balance:</label> &nbsp;&nbsp;&nbsp;<input id="clone-name" type="text" name="balance" value="" readonly><br/>
<label>Credits(+/-)</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id="clone-name" type="text" name="credit"><br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="btnsmall" type="submit" value="Save">
<input class="btnsmall" type="reset" value=Cancel></form> -->

</div> 
</div>
</div>

  <div class="tabcontent hide" id="cont-2-1">

 <div style="height:12px;"></div> 
 <label>Partner&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="partnertrans" name="partnertrans" value="" size="22"></label>&nbsp;&nbsp;
 <label>User&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="usertrans" name="usertrans" value="" size="22"></label>&nbsp;&nbsp;<br/>
 <label>FromDate&nbsp;&nbsp;<input type="text" class="date" name="fromdate" id="fromdate"value="" size="22"/></label>
<label>ToDate&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" class="date" name="todate" id="todate" value="" size="22"/></label>
<input type="submit" name="submit" id="Search" onclick="SearchTrans()" value="Search">&nbsp;&nbsp;&nbsp;<a id = "transexport" name = "transexport" href="getalltransactionsexport.htm"><input type="submit" name="submittrans" id="submittrans" onclick="ExportTrans()" value="Export"></a><br />

<div id="listrowstrans" >
<table id ="courses_table1" class="courses_table1" cellspacing="1">
        </table>
 </div>
 <table id="listrowsPage" align="right" cellspacing="1" style="font-weight:bold;width:950px; padding:1px;">
</table> 
<table id="listrowstransPage" align="right" cellspacing="1" style="font-weight:bold;width:950px; padding:1px;">
</table><br><br><br><br><br><br>
 
 
 
</div>

</div>
	

</div>
	</div>

	<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing</div></div>


<div id="calendar_div"></div></body></html>
