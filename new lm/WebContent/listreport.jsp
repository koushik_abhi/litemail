<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lite Mail</title>
<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
 <!-- <script type="text/javascript" src="http://www.google.com/jsapi"></script> -->
 <script type="text/javascript" src="js/jsapi.js"></script>
  <script type="text/javascript" src="js/jquery.js"></script>
   <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
    </script>
    <script>
    function drawVisualizations1(attribute, listname, type)
    {
    
    	$.ajax({
            type: "GET",
            url:"listsummarygraph.htm?listName="+listname+"&attribute="+attribute,
            data:{
            	
            	listName:listname,
            	attribute:attribute
            	
            },
            dataType: "json",
            success: function(result)
            {
                drawGraph1(result.data, result.number,attribute, listname, type);
                
            },
            error: function(x, e)
            {
            	alert("error");
            }
         });
    	
    	
    }
    function drawVisualizations2(attribute, listname, type)
    {
    	
    	$.ajax({
            type: "GET",
            url:"listsummarygraph.htm?listName="+listname+"&attribute="+attribute,
            data:{
            	
            	listName:listname,
            	attribute:attribute
            	
            },
            dataType: "json",
            success: function(result)
            {
                drawGraph2(result.data,result.number,attribute, listname, type);
                
            },
            error: function(x, e)
            {
            	alert("error");
            	// alert(x.readyState + " "+ x.status +" "+ e.msg);   
            }
         });
    	
    	
    }
    function drawGraph1(datas, numbers, attribute,listname, type ) {
        // Create and populate the data table.
        
        
        var data = new google.visualization.DataTable();
  		data.addColumn('string', attribute);
  		data.addColumn('number', 'Count');
  		data.addRows(datas.length);
  		for(var i = 0; i < datas.length; i++){
  		  data.setCell(i,0,datas[i]);
  		  data.setCell(i,1,numbers[i]);
  		}
  		
        var options = {
                 title:attribute+"  Distribution",
                 pointSize:4,
                 width:800, height:370,
                 backgroundColor: {fill:'f6f6f6'},
                 hAxis: {title: attribute},
                 vAxis: {title: "Count"}
              };
        // Create and draw the visualization.
           
        if(type =="pie")
        {
        	
        	new google.visualization.PieChart(document.getElementById('chart_div1')).
            	draw(data,options);
        }else if(type =="line")
        {
        	
        	 new google.visualization.LineChart(document.getElementById('chart_div1')).draw(data, options);
          
        	
        }
        else if(type =="column")
        {
        	
        	 new google.visualization.ColumnChart(document.getElementById('chart_div1')).draw(data, options);
        	
        	
        }else
        {
        	
        	 var chart = new google.visualization.ColumnChart(document.getElementById('chart_div1'));
             chart.draw(data, options);
        }
    }
    
    function drawGraph2(datas, numbers, attribute,listname, type ) {
        // Create and populate the data table.
        
       
       
        var data = new google.visualization.DataTable();
  		data.addColumn('string', attribute);
  		data.addColumn('number', 'Count');
  		data.addRows(datas.length);
  		for(var i = 0; i < datas.length; i++){
  		  data.setCell(i,0,datas[i]);
  		  data.setCell(i,1,numbers[i]);
  		}
  		       
        var options = {
                 title:attribute+"  Distribution",
                 width:800, height:370,
                 pointSize:4,
                 backgroundColor: {fill:'f6f6f6'},
                 hAxis: {title: attribute},
                 vAxis: {title: "Count"}
              };
        // Create and draw the visualization.
        if(type =="pie")
        {
        	new google.visualization.PieChart(document.getElementById('chart_div2')).
            	draw(data,options);
        }else if(type =="line")
        {
        	 var chart = new google.visualization.LineChart(document.getElementById('chart_div2'));
             chart.draw(data, options);
        	
        }
        else if(type =="column")
        {
        	 var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
             chart.draw(data, options);
        	
        }else
        {
        	 var chart = new google.visualization.ColumnChart(document.getElementById('chart_div2'));
             chart.draw(data, options);
        }
    }
    
    $(document).ready(function(){
    	
     $("span#act_attrib_pie").click(function(){
    	 type1 ="pie";
    	 $(this).addClass('selected').siblings().removeClass('selected');
    	 $("#charts").text(type1 +" chart");
    	 drawVisualizations1(field1, listname, "pie");
    	
     });
     $("span#act_attrib_bar").click(function(){
    	 type1 ="column";
    	 $(this).addClass('selected').siblings().removeClass('selected');
    	 $("#charts").text(type1 +" chart");
    	 drawVisualizations1(field1, listname, "column");
    	 
     });
     $("span#act_attrib_line").click(function(){
    	 type1 ="line";
    	 $(this).addClass('selected').siblings().removeClass('selected');
    	 $("#charts").text(type1 +" chart");
    	 drawVisualizations1(field1, listname, "line");
    	 
     });
   
    
    });
    </script>
  
    <script>
    var type1 = "pie";
    var type2 = "pie";
    $(document).ready(function()
    		{
    	  $("#act_attrib_bar_1").click(function(){
    	    	 $(this).addClass('selected').siblings().removeClass('selected');
    	    	 type2 ="column";
    	    	 $("#charts").text(type2 +" chart");
    	    	 drawVisualizations2(field2, listname, "column");
    	    	 
    	     });
    	  $("span#act_attrib_pie_1").click(function(){
    		  	 type2 ="pie";
    	    	 $(this).addClass('selected').siblings().removeClass('selected');
    	    	 $("#charts").text(type2 +" chart");
    	    	 drawVisualizations2(field2, listname, "pie");
    	    	
    	     });
    	  $("span#act_attrib_line_1").click(function(){
    		  type2 ="line";
 	    	 $(this).addClass('selected').siblings().removeClass('selected');
 	    	 $("#charts").text(type2 +" chart");
 	    	drawVisualizations2(field2, listname, "line");
 	    	
 	     });
    		});
    </script>
    <script>
    var listname= "";
    var field1= "";
    var field2= "";
    var listid="";
    $(document).ready(function() {
 
    listname ="${listName}";
    //listname= "test1";
    listid="${listId}";
    if(listname != null && listname != "")
    {
    	renderData(listname);
    	showSelecFields(listname,listid);
    }
   
    });
    function renderData(listname)
    {
    	$.ajax({
            type: "GET",
            url:"getlistdetails.htm?listname="+listname,
            data:{
            	
            	listname:listname
            	
            },
            dataType: "json",
            success: function(result)
            {
                populateData(result);
                
            },
            error: function(x, e)
            {
                 //alert(x.readyState + " "+ x.status +" "+ e.msg);   
            }
         });
    	
    	
    }
    function populateData(result)
    {
    	if(result != null && result.length > 0)
    	{
    		var le = result[0];
    		$("#listname").append( le.listName);
    		$("#status").append(le.status);
    		
    	}
    }
    </script>
    <script>
    function showSelecFields(listname,listid)
    {
    	$.ajax({
    		type: "get",
    		url: "getAttributesForListName.htm?listName="+listname+"&listId="+listid,
    		dataType: "json",
    		success: function( objData )
    		{
    			
    			var select = "Select Field";
    			$('#listfield1').append($('<option>').text(select).attr('value', select));
    			$('#listfield2').append($('<option>').text(select).attr('value', select));
    			$.each(objData.result, function(i, value) {
    	           $('#listfield1').append($('<option>').text(value).attr('value', value));
    	           $('#listfield2').append($('<option>').text(value).attr('value', value));
    	        });
    			
    			
    		},
    		error: function(){
    			//alert( "An error occurred" );
    		}
    	});
    		
    	
    }
   
    </script>
    <script>
    $(document).ready(function(){
    	$("select#listfield1").change(function(){
    		var fields=$("#listfield1 option:selected").text();
    		field1 = fields;
    		if(field1 != "select")
    			drawVisualizations1(field1, listname, type1);
    		else
    		{
    			alert("please select a field for analysis");
    			            	
    		}
    	});
    });
    
    $(document).ready(function(){
    	$("select#listfield2").change(function(){
    		var fields=$("#listfield2 option:selected").text();
    		field2 = fields;
    		if(field2 != "select")
    			drawVisualizations2(field2, listname, type2);
    		else
    			alert("please select a field for analysis");
    		//	drawVisualizationsSummary(listname, "pie");
    	});
    });
    </script>
    
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
        <div class="contentsgc">
        <ul class="breadcrumb2" style="margin-top:-10px;margin-left:5px;margin-bottom:10px;">
            
	   <li><a href="listManagement.jsp" class="bredcom">Lists</a></li>
	   <li><a href="#" class="bredcom"><%=request.getAttribute("listName") %></a></li>
	   <li><a href="#" class="bredcom" id="charts">Charts</a>
	   </ul>
	    <div align="left" style="width:1050px; height:50px;border:1px solid #cccccc;border-radius:2px;margin-left:5px;">
	    <table style="width:1100px;margin-top:10px"><tr><td>
	    <label id="listname" style="margin-left:10px;margin-top:0px;">List Name : </label></td>
	    <td align="right"><label id="status" style="float:right;margin-right:100px;">Status : </label><br></td></tr></table>
	    </div>
    <div style="border:1px solid #cccccc;border-radius:2px; margin-left:5px;width:1050px;margin-top:5px;" align="center">
    <div class="chartblock" align="left" style="margin-top:5px;margin-left:5px;">
	<select id="listfield1" name="listfield1" size=1 style="margin-top:5px;margin-left:5px;">
	</select>
        <div class="charticons" style="float:right;border:1px solid #ccc;border-radius:5px;margin-top:5px;margin-right:10px;">
		<span id="act_attrib_pie" class="selected"><img src="images/chart-pie.jpg" title="Pie Chart" alt="Pie Chart"></span>
		<span id="act_attrib_bar" ><img src="images/chart-column-1.jpg" title="Bar Chart" alt="Bar Chart"></span>
		<span id="act_attrib_line" ><img src="images/chart-line-1.jpg" title="Line Chart" alt="Line Chart"></span>
	    </div>
	    </div>
		 <div id="chart_div1">
		 </div>		 
		 </div>
		 <div style="border:1px solid #cccccc;border-radius:2px; margin-left:5px;width:1050px;margin-top:5px;" align="center">
    <div class="chartblock" align="left" style="margin-top:5px;margin-left:5px;">
	<select id="listfield2" name="listfield2" size=1 style="margin-top:5px;margin-left:5px;">
	</select>
        <div class="charticons" style="float:right;border:1px solid #ccc;border-radius:5px;margin-top:5px;margin-right:10px;">
		<span id="act_attrib_pie_1" class="selected"><img src="images/chart-pie.jpg" title="Pie Chart" alt="Pie Chart"></span>
		<span id="act_attrib_bar_1" ><img src="images/chart-column-1.jpg" title="Bar Chart" alt="Bar Chart"></span>
		<span id="act_attrib_line_1" ><img src="images/chart-line-1.jpg" title="Line Chart" alt="Line Chart"></span>
	    </div>
	    </div>
		 <div id="chart_div2">
		 </div>		 
		 </div>
	     </div>
	     </div>
</div>

<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>
</body>
</html>