<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SMTP Details</title>
<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){
	getSmtp_Detals();
});

function getSmtp_Detals()
{
	
	$.ajax({
		type: "get",
		url: "smtpDisplayConfigs.htm",
		dataType: "json",
		success: function( objData )
		{
			renderListData(objData.result);
		},
		error: function(data){
			window.location = "smtpManagement.jsp";
		}
	});
	return false;	
	}
	function renderListData(objData)
	{
		if(objData != null && objData.length > 0)
		{
			$("#smtpdetails").append('<tr><th width="100">SMTP Server Name</th><th width="100">SMTP Server Ip</th><th width="100">Partner</th><th width="100">Created By</th><th width="100">Created On</th><th width="50">Actions</th></tr>');
			var cont =1;
			for(var count =0; count < objData.length; count++)
			{
				var row = objData[count];
				renderRowInListTable(row, cont);
				cont++;
					
			}
		}
		else
		{
			$("#smtpdetails").append('<tr><td style="color:red">No list found.</td></tr>');
		}
		
	}
	function renderRowInListTable(row, cont)
	{
		var smtpid=row.smtpServerId;
		var servername=row.smtpServerName;
		var serverip=row.smtpServer;
		var createdby=row.smtpUser;
		var createdDate=row.createdDate;
		var partner=row.partner;
			$("#smtpdetails").append('<tr><td width="100">'+servername+'</td><td width="100">'+serverip+'</td><td width="100">'+partner+'</td><td width="100">'+createdby+'</td><td width="100">'+createdDate+'</td><td width="50" align="center"><a href="editsmtp.htm?smtpid='+smtpid+'"><img src="images/edit.png" class="tooltip" title="edit"  /></a>&nbsp;&nbsp;<img src="images/delete.png" class="tooltip" title="delete" onclick="deleteRow(\''+servername+'\');" /></td></tr>');
	}
	function deleteRow(servername)
	{
		if(confirm("are you sure you want to delete this record" ))
		{
		$.ajax({
			type: "get",
			url: "deleteSmtp.htm?servername="+servername,
			dataType: "json",
			success: function( objData )
			{
				window.location = "smtpManagement.jsp";	
			},
			error: function(data){
				
				//alert( "An error occurred" +data[0] );
				window.location = "smtpManagement.jsp";
			}
		});
		}
		return false;
		
	}
	
	</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showusers.htm" class="left-nav" >Users </a>
 <a href="showpartners.htm" >Partners </a>
  <a href="showcredits.htm"> Credits</a>
  <a href="showsmtpManagement.htm" id="selected"> SMTP Configuration</a>
  <a href="showemailconfig.htm"> Notification Settings</a>
 <a href="showdashboard.htm" class="right-nav"> Dashboard </a>
</div>
</div>
</div>
	<div id="content">
		<div id="loading-popup-box" style="display: none;"></div>
		<div id="content-main">
<!-- 					<div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>
 -->
			<div class="contents">
			<div class="create_form">
			
		   <!-- div style="padding-left: 50px"> <a href="addsmtpserver.jsp" id="buttonclass" >Add Smtp Server</a></div> -->
			</div>
			<table style="font-weight:normal;width:945px;padding-left: 750px">
			<tr><td><a href="addsmtpserver.jsp" id="buttonclass" >Add New Smtp Server</a></td></tr>
			</table>
			<table id="smtpdetails" class="tblewidth" class="tblewidth"  style="font-weight:normal;width:800px;margin-left: 130px">
			</table>
			</div>
		</div>
	</div>

	<div id="footer">
		<div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div>
	</div>
</body>
</html>