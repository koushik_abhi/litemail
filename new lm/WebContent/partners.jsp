<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Partner</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="uitrans/jquery-ui.css" />

<style type="text/css">

#search-button {
  display: inline-block;
  width: 34px;
  height: 32px;
  position: relative;
  left: -41px;
  top: 9px;
  background: url(images/search.jpg) no-repeat right;
}

	.ui-autocomplete { 
			position: absolute; 
			cursor: default; 
			height: 150px; 
			width:200px;
			overflow-y: scroll; 
			overflow-x: hidden;
			text-align:left;
		}

	
	
</style>

<script>
$(document).ready(function() {

	renderpartnerpage();
//getAvailableLists();
});

function renderpartnerpage()
{
	$("#listrows").empty();
	$("#listrowsPage").empty();
	$.ajax({
		type: "get",
		url: "partnermanagement.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}

function rendernextlistpage()
{
	$("#listrows").empty();
	$("#listrowsPage").empty();
	pagenum =pagenum+1;
	$.ajax({
		type: "get",
		url: "partnermanagement.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
		
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}

function renderprevlistpage()
{
	$("#listrows").empty();
	$("#listrowsPage").empty();
	pagenum =pagenum-1;
	$.ajax({
		type: "get",
		url: "partnermanagement.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}


var pagenum = 1;

function renderlisttable(objData, pagenumber, totalpages)
{
	
	pagenum = pagenumber;
	if(objData != null && objData.length > 0)
	{
		$("#listrows").append('<tr><th width="100">Name</th><th width="100">Type</th><th width="100">Status</th><th width="100">Email-Id</th><th width="100">Mobile Number</th><th width="100">Address</th><th width="100">URL</th><th width="100">Created By</th><th width="100">Created Time</th><th width="120">Actions</th></tr>');
		var cont =1;
		for(var count =0; count < objData.length; count++)
		{
			var row = objData[count];
			renderRowInListTable(row, cont);
			cont++;
				
		}
		 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
			{
				$("#listrowsPage").append('<tr style="background-color: #F6F6F6;"><td width="50%" align="right"><label><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label></td><td width="50%" align="right"><input type="submit"  id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()">&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td></tr>');
				
			}else if(totalpages > 1 && pagenumber == 1)
			{
				$("#listrowsPage").append('<tr style="background-color:#F6F6F6;"><td width="100%" align="center"><label><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label></td><td><input type="submit"   style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td></tr>');
				
			}else if (totalpages == pagenumber  && totalpages > 1)
			{
				$("#listrowsPage").append('<tr style="background-color: #F6F6F6;"><td width="100%" align="center"><label><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label></td><td align:right><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td></tr>');
				
			}
		
	}
	else
	{
		$("#listrows").append('<tr><td style="color:red">No list found.</td></tr>');
	}
	
}
function submitExportTo()
{
	listname ="${listname}";
	var url = "listfieldsexport.htm?listname="+listname;
	document.exportto.action =  url;
	document.exportto.submit();
	$("#exportto").dialog('close');
				
}
function renderRowInListTable(row, cont)
{
	var id=row.pid;
	var name=row.name;
	var type = row.type;
	var status = row.status;
	var emailId=row.emailId;
	var mobileNumber=row.mobileNumber;
	var	address =row.address;
	var url = row.businessUrl;
	var createdBy=row.createdBy;
	var createdTime =row.createdDate;
		$("#listrows").append('<tr ><td width="100" >'+name+'</td><td width="100" >'+type+'</td><td width="100" >'+status+'</td><td width="100" >'+emailId+'</td></td><td width="100" >'+mobileNumber+'</td><td width="100" >'+address+'</td><td width="100" >'+url+'</td><td width="100" >'+createdBy+'</td><td width="150" >'+createdTime+'</td><td width="120" align="center">&nbsp;&nbsp;<a href="editpartner.htm?partnerid='+id+'"><img src="images/edit.png" class="tooltip" title="edit" /></a>&nbsp;&nbsp;<a><img src="images/delete.png" class="tooltip" title="delete" onclick="deleteRow(\''+name+'\');" /></a></td></tr>');
}


function deleteRow(id)
{
	if(confirm("are you sure you want to delete this partner" ))
	{
	$.ajax({
		type: "get",
		url: "deletepartner.htm?partnername="+id,
		data: id,
		dataType: "json",
		success: function( objData )
		{
			window.location = "partners.jsp";	
		},
		error: function(data){
			
			//alert( "An error occurred" +data[0] );
			window.location = "partners.jsp";
		}
	});
	}
	return false;
	
}




function validateForm(name)
{
	
	if(isalphaNum(name, "Please enter valid name (A-Z, a-z, 0-9)"))
	{
		return true;
	}
	return false;
	
}

</script>
</head>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showusers.htm" class="left-nav" >Users </a>
 <a href="showpartners.htm" id="selected">Partners </a>
  <a href="showcredits.htm"> Credits</a>
  <a href="showsmtpManagement.htm"> SMTP Configuration</a>
     <a href="showemailconfig.htm"> Notification Settings</a>
 <a href="showdashboard.htm" class="right-nav"> Dashboard </a>
</div>
</div>
</div>
	<div id="content">
		<div id="loading-popup-box" style="display: none;"></div>
		<div id="content-main">
<!-- 				<div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="86" height="127"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>
 -->
			<div class="contents">
			<div class="create_form">
		    <a href="showaddpartner.htm" id="buttonclass">Add New Partner</a>
			</div>
			<table id="listrows" class="tblewidth">		
				</table>
				<table id="listrowsPage" class="tblewidth">	</table>
			</div>
		</div>
	</div>

	<div id="footer">
		<div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div>
	</div>
</body>
</html>