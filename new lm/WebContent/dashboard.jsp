<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Dashboard</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<style type="text/css">
#welcome a:hover{font-weight: bold}
</style>
</head>
<body>
<div id="wrapper">
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank""><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav" id="selected"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
     <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
        <div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="150"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="115" height="150"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="115" height="150"></span></span></div>
        <div class="contents">
		<!-- welcome div-->
		<div id="welcome" align="center">
		 <div id ="error" align="center" class="forgotpassword" style="color:red">${errorMessage}</div> 
		<table style="margin-top:14px;" cellspacing="5">
		<tr><td><h1><label>Welcome to Lite Mail</label></h1></td></tr>
		<tr></tr>
       <tr>
      <td><label><a href="createlist_wf.jsp" id="list">Create New List</a></label>&nbsp;<label>(*.csv)</label></td></tr>
      	<tr>
      <td><label><a href="showlist.htm" id="list">List Management</a></label>&nbsp;</td></tr>
     <tr><td><label><a href="showcampaign.htm">Campaign Management</a></label></td></tr>
     <tr><td><label><a href="administrator.htm">Administration</a></label></td></tr>
   
    </table>
	</div>
</div>
  </div>
</div>
</div>
<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>