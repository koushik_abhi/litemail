<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Credits</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="./css/main.css" />
<link rel="stylesheet" href="uitrans/jquery-ui.css" />
<script  type="text/JavaScript" src="./js/jquery-1.9.1.js" charset="utf-8"></script>
   <script type="text/JavaScript" src="./js/jquery-ui.js" charset="utf-8"></script>
   <link rel="stylesheet" type="text/css" href="./css/smoothness/jquery-ui-smoothness.css" media="all">


<style type="text/css">
.closepopup
{
color:#000;
text-align: center;
cursor: pointer;
right: 0;
top: 0;
position: absolute;
margin-right:10px;
font-weight: bold;
}
</style>
<script>
$(document).ready(function() {

renderlistpage();
});

function renderlistpage()
{
	$("#attribrows").empty();
	$("#attribrowsPage").empty();
	$.ajax({
		type: "get",
		url: "creditmanagement.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("no data found");
		}
	});
	
}

function rendernextlistpage()
{
	$("#creditrow").empty();
	$("#creditrowpage").empty();
	pagenum =pagenum+1;
	$.ajax({
		type: "get",
		url: "creditmanagement.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
		
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("no data found");
		}
	});
	
}

function renderprevlistpage()
{
	$("#creditrow").empty();
	$("#creditrowpage").empty();
	pagenum =pagenum-1;
	$.ajax({
		type: "get",
		url: "creditmanagement.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("no data found");
		}
	});
	
}


var pagenum = 1;

function renderlisttable(objData, pagenumber, totalpages)
{
	
	pagenum = pagenumber;
	if(objData != null && objData.length > 0)
	{
		$("#creditrow").append('<tr style="background-color: rgb(238, 238, 238);"><th width="100">User Name</th><th width="100">Partner Name</th><th width="100">Credits</th><th width="100">Debits</th><th width="100">Balance</th><th width="180">Trans Type</th><th width="100">Update By</th><th width="150">Updated Time</th><th width="120"><strong>Actions</th></tr>');
		var cont =1;
		for(var count =0; count < objData.length; count++)
		{
			var row = objData[count];
			renderRowInListTable(row, cont);
			cont++;
				
		}
		 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
			{
				$("#creditrowpage").append('<tr style="background-color: #F6F6F6;"><td width="50%" align="right"><label> Page  '+pagenum+' Of '+totalpages+'</label></td><td width="50%" align="right"><input type="submit"  id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()">&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td></tr>');
				
			}else if(totalpages > 1 && pagenumber == 1)
			{
				$("#creditrowpage").append('<tr style="background-color:#F6F6F6;"><td width="100%" align="center"><label> Page  '+pagenum+' Of '+totalpages+'</label></td><td><input type="submit"   style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td></tr>');
				
			}else if (totalpages == pagenumber  && totalpages > 1)
			{
				$("#creditrowpage").append('<tr style="background-color: #F6F6F6;"><td width="100%" align="center"><label> Page  '+pagenum+' Of '+totalpages+'</label></td><td align:right><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td></tr>');
				
			}
		
	}
	else
	{
		$("#creditrowpage").append('<tr><td style="color:red">No Records found.</td></tr>');
	}
	
}

function renderRowInListTable(row, cont)
{	
	var userName=row.username;
	var partnerName = row.partnername;
	var credits = row.credits;
	var	debits =row.debits;
	var balance = row.balance;
	var transType=row.transType;
	var updatedBy=row.updatedBy;
	var updatedTime=row.updatedDate;
		$("#creditrow").append('<tr style="text-align:left;"><td width="100" >'+userName+'</td><td width="120" align="center"  >'+partnerName+'</td><td width="120" align="center" >'+credits+'</td><td width="100" align="center">'+debits+'</a></td><td width="100" align="center">'+balance+'</a></td><td width="150" align="center">'+transType+'</a></td><td width="120" align="center">'+updatedBy+'</a></td><td width="180" align="center">'+updatedTime+'</a></td><td width="150" align="center"><a href="#" style="color:red;" onclick="openpopup(\''+userName+'\')">AssignCredits</a></td></tr>');
}


function openpopup(userName)

{
	$("#uname").text(userName);
	$("#assigncreditid").dialog({
        position: 'center',
        width: 345,
        height: 190,
        title: 'Assign Credit',
        modal: true
});
	$(".ui-dialog-titlebar").hide();
	
	}



function assignCredit()
{
	
	
	var name=$("#uname").text();
	var credits=$("#value").val();
	if(credits == null || credits == "")
	{
		alert("please enter valid credits");
		return false;
	}else
	{
		if(isNaN(credits))
		{
			alert("please enter numbers only for credits to add");
			return false;
		}
		
	}
	
	$.ajax({
        type : 'get',
        url: "assigncredits.htm?username="+name+"&creditvalue="+credits,
        dataType: "html",
			 success: function(  )
			{
				 	
					window.location="credits.jsp";
        }
     });
	
}



function IsNumeric(input){
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
}

	function closeWin() {
		$("#assigncreditid").dialog('close');
	}

</script>
</head>
<body>
<div id="wrapper">
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="images/kenscio-logo.jpg"  border="0" width="70" /></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showusers.htm" class="left-nav" >Users </a>
 <a href="showpartners.htm" >Partners </a>
  <a  href="showcredits.htm" id="selected"> Credits</a>
  <a href="showsmtpManagement.htm"> SMTP Configuration</a>
   <a href="showemailconfig.htm"> Notification Settings</a>
 <a href="showdashboard.htm" class="right-nav"> Dashboard </a>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
<!-- 		<div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="86" height="127"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>
 -->        <div class="contents">
         <div align="center" style="color:red">${errorMessage}</div><br>
      
<table id="creditrow" class="tblewidth"  >
</table>
<table id="creditrowpage" align="right" cellspacing="1" style="font-weight:bold;width:950px; padding:1px;">
</table><br><br><br><br><br><br>
<div id="assigncreditid" style="display: none;" >
<form name="assigncreditform" method="get" action="./assigncredits.htm">
<span class="closepopup" onclick="closeWin();" title="Close">X</span>
    <table align="center" width="320" >
  <tr>
  <td >User Name:</td><td id="uname"></td>
  
   </tr>
    <tr >
  <td>Credits(+/-):</td><td><input type="text"  style="font-size: 12px "name="value" size="21" id="value"/></td>
   </tr>
   <tr>
   <td>&nbsp;</td>
  <td style="padding-top:10px;font-size:12px;"><input type="button" class="btnsmall"  value="Submit" onclick = "return assignCredit()"> 
   <input type="button" class="btnsmall" id="close" value="Cancel" onclick="closeWin()"> </td></tr>
  </table>

</form>
</div>

</div>
</div>
</div>
</div>


<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>