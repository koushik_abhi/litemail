<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Campaign Status</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="./css/smoothness/jquery-ui-smoothness.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="./js/jquery-1.9.1.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/jquery-ui.js" charset="utf-8"></script>

<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="js/jquery-ui-timepicker-addon.css" />

<style type="text/css">
#tblewidth tr:nth-child(2n) {
    background-color: #F4F3F1;
}
#tblewidth tr:nth-child(2n+1) {
    background-color: #FDEEB0;
}
#tblewidth input[type="submit"]{
	color:#fff; padding:4px 10px; border-radius:8px; 
	border:none; cursor:pointer; font-weight:bold; background-color:#cc0000
	}
td, th {
    padding: 2px;
}

input{font-size:15px; font-weight:normal;}
td a{
text-decoration:underline ;
color:#000;
}
.alignView
{
float:right;
margin-left:50px;
}
.alignLeft
{
float:left;
}

.datepicker
{
background: none repeat scroll 0 0 #FBFBFB;
border: 1px solid #E5E5E5;
box-shadow: 1px 1px 2px rgba(200, 200, 200, 0.2) inset;
color: #555555;font-size: 18px;
font-weight: normal;
line-height: 1;margin-bottom: 16px;
margin-right: 6px;
margin-top: 4px;outline: 0 none;padding: 6px;
}

</style>
<script type="text/javascript">
var pageNumber=1;
var recids=[];
$(document).ready(function(){
	var url='<%=session.getAttribute("viewUrl") %>';
	var type='<%=session.getAttribute("type") %>';
	get_Report_Data(url,type); 
	
	$('.datepicker').each(function() {
		$(this).datepicker({
			dateFormat : 'dd-mm-yy'
		});
		$(".ui-datepicker").css("font-size", "13.5px");

	});
});

function get_Report_Data(url1,type) {
	var campaignId='<%=session.getAttribute("campaignId") %>';
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	$("#campaignstatus_header").text(type);
	if(type.match(/Failed/g))
		{
		getMailedFailedData(url1,type);
		return false;
		}
	if(type.match(/SentSoFar/g))
	{
	getMailedSentSoFarData(url1,type);
	return false;
	}
	if(type.match(/Bounce/g))
	{
		
       getBouncedMailListData(url1,type);
       return false;
	}
	if(type.match(/Spam/g))
	{
       getSpamMailListData(url1,type);
       return false;
	}
	if(type.match(/Unsubscribe/g))
	{
       getUnsubscribeMailListData(url1,type);
       return false;
	}
	else{
		var fromdate = document.getElementById("fromdate").value;
		var todate = document.getElementById("todate").value;
		var uId='<%=session.getAttribute("uId") %>';
		var recId=0;
	$.ajax({
		type : "get",
		url : "searchMailList.htm?campaignId=" + campaignId +"&uId="+uId+ "&fromdate="
				+ fromdate + "&todate=" + todate + "&url=" + url1+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			if(type.match(/Opens/g))
				{
				renderTotalOpensData(objData.results,objData.pagenumber, objData.totalpages,url1);
				}
			if(type.match(/Clicks/g))
				{
				
				renderTotalClicksData(objData.results,objData.pagenumber, objData.totalpages,url1);
				}
			/* if(type.match(/Unsubscribe/g))
			{
				
				renderUnsubscribeData(objData.results,objData.pagenumber, objData.totalpages,url1);
			} */
			if(type.match(/Click Through Email/g))
			{
			
		        renderClicksThroughEmailData(objData.results,objData.pagenumber, objData.totalpages,url1);
			}

		},
		error : function(data) {

			alert("error " + data);
		}
	});
	}
	return false;
}

function renderUnsubscribeData(objData,pagenumber, totalpages,url1)
{
	pagenum = pagenumber;
	if(objData !=null && objData.length > 0)
  {
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:250px;">Host</th><th  style="border-right:1px solid #fff;width:250px;">Device</th><th  style="border-right:1px solid #fff;width:250px;">Browser</th><th  style="border-right:1px solid #fff;width:250px;">Location</th><th  style="border-right:1px solid #fff;width:250px;">Created Time</th><th  style="border-right:1px solid #fff;width:200px;">Unsubscibed Time</th></tr>');
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderUnsubscribeDataTable(row, cont);
		cont++;
			
	}	
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevUnsubscribelistpage('+url1+')">&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextUnsubscribelistpage('+url1+')"></td></tr>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"    id ="next" name = "next" value="Next" onclick="rendernextUnsubscribelistpage('+url1+')"></td></tr>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="previous" name = "previous" value="Previous" onclick="renderprevUnsubscribelistpage('+url1+')"></td></tr>');
			
		}
}
else
{
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:250px;">Host</th><th  style="border-right:1px solid #fff;width:250px;">Device</th><th  style="border-right:1px solid #fff;width:250px;">Browser</th><th  style="border-right:1px solid #fff;width:250px;">Location</th><th  style="border-right:1px solid #fff;width:250px;">Created Time</th><th  style="border-right:1px solid #fff;width:200px;">Unsubscribed Time</th></tr>');
   $(".campaignstatus_all").append('<tr><td style="color:red;" align="center"><b>No list found.</b></td></tr>');
}
}

function renderUnsubscribeDataTable(row,cont)
{
	
	var email = row.reciepientEmail;
	var name = row.reciepientName;
	var host = row.unsubscribeIP;
	var device=row.device;
	var browser=row.browser;
	var location=row.unsubscribeLocation;
	var createdDate=row.createdDate;
	var unsubDate = row.unsubscribeDate;
	if(email == null)
	{
		email="";
	}
	if(name == null)
	{
		name="";
	}
	if(unsubDate == null)
	{
		unsubDate="";
	}
	if(host == null)
	{
		host="";
	}
	if(device == null)
	{
		device="";
	}
	if(browser == null)
		{
		browser="";
		}
	if(location == null)
	{
		location="";
	}
	if(createdDate == null)
	{
		createdDate="";
	}
	$(".campaignstatus_all").append('<tr><td width="100" >'+email+' </td><td width="100" >'+name+' </td><td width="100" >'+host+' </td><td width="100" >'+device+' </td><td width="100" >'+browser+'</td><td width="100" >'+location+' </td><td width="100" >'+createdDate+' </td><td width="100" >'+unsubDate+' </td></tr>');
}

function getMailedFailedData(url,type)
{
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
	$.ajax({
		type : "get",
		url : url + "?campaignId=" + campaignId + "&fromdate=" + fromdate
				+ "&todate=" + todate+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {

			renderMailSentListData(objData.result,objData.pagenumber, objData.totalpages);

		},
		error : function(objData) {

			alert("error1 " + objData);
		}
	});
	return false;
}
function renderTotalOpensData(objData,pagenumber, totalpages,url1)
{
	var maxRecId = Math.max.apply(Math, recids);
	var minRecId=Math.min.apply(Math, recids);
	pagenum = pagenumber;
	if( objData.items != null &&  objData.items.length > 0)
	{
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:200px;">Open Time</th><th  style="border-right:1px solid #fff;width:250px;">Host</th><th  style="border-right:1px solid #fff;width:250px;">Device</th><th  style="border-right:1px solid #fff;width:250px;">Location</th></tr>');
	var cont =1;
	for(var count =0; count < objData.items.length; count++)
	{
		var row = objData.items[count];
		renderTotalOpensDataTable(row, cont);
		cont++;
			
	}	
	$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"></td><td class="alignView"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage(\''+url1+'\',\''+minRecId+'\')">&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextlistpage(\''+url1+'\',\''+maxRecId+'\')"></td></tr>');
	 /* if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage(\''+url1+'\',\''+minRecId+'\')">&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextlistpage(\''+url1+'\',\''+maxRecId+'\')"></td></tr>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"    id ="next" name = "next" value="Next" onclick="rendernextlistpage(\''+url1+'\',\''+maxRecId+'\')"></td></tr>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage(\''+url1+'\',\''+minRecId+'\')"></td></tr>');
	} */
	}
	else
		{
		$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:200px;">Open Time</th><th  style="border-right:1px solid #fff;width:250px;">Host</th><th  style="border-right:1px solid #fff;width:250px;">Device</th><th  style="border-right:1px solid #fff;width:250px;">Location</th></tr>');
		$(".campaignstatus_all").append('<tr><td style="color:red;" align="center"><b>No list found.</b></td></tr>');
		}
}

function renderTotalOpensDataTable(row,cont)
{
	
	var email = row.reciepientEmail;
	if(email == null)
		{
		email="";
		}
	var name = row.reciepientName;
	if(name == null)
		{
		name="";
		}
	var openDate = row.openDate;
	if(openDate == null)
		{
		openDate="";
		}
	var host = row.openIP;
	if(host == null)
		{
		host ="";
		}
	var location =row.openLocation;
	if(location == null)
		{
		location="";
		}
	var device=row.device;
	recids.push(row.messageOpenId);
	if(device == null)
		{
		device =="";
		}
	
		$(".campaignstatus_all").append('<tr><td width="100" >'+email+' </td><td width="100" >'+name+' </td><td width="100" >'+openDate+' </td><td width="100" >'+host+' </td><td width="100" >'+device+' </td><td width="100" >'+location+' </td></tr>');
		
}

function renderBouncedMailListData(objData,pagenumber, totalpages)
{
	
	pagenum = pagenumber;
	if(objData!= null && objData.length > 0)
	{
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:200px;">Bounce Time</th><th  style="border-right:1px solid #fff;width:250px;">Bounce Type</th><th  style="border-right:1px solid #fff;width:250px;">Domain Name</th><th  style="border-right:1px solid #fff;width:200px;">Code</th><th  style="border-right:1px solid #fff;width:200px;">Reason</th></tr>');
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		
		var row = objData[count];
		renderBouncedMailListDataTable(row, cont);
		cont++;
			
	}	
	
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
	{
		$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevBouncelistpage()">&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextBouncelistpage()"></td></tr>');
		
	}else if(totalpages > 1 && pagenumber == 1)
	{
		$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"    id ="next" name = "next" value="Next" onclick="rendernextBouncelistpage()"></td></tr>');
		
	}else if (totalpages == pagenumber  && totalpages > 1)
	{
		$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="previous" name = "previous" value="Previous" onclick="renderprevBouncelistpage()"></td></tr>');
		
	}

}
else{
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:200px;">Bounce Time</th><th  style="border-right:1px solid #fff;width:250px;">Bounce Type</th><th  style="border-right:1px solid #fff;width:250px;">Domain Name</th><th  style="border-right:1px solid #fff;width:200px;">Code</th><th  style="border-right:1px solid #fff;width:200px;">Reason</th></tr>');
	$(".campaignstatus_all").append('<tr><td style="color:red;" align="center"><b>No list found.</b></td></tr>');
}
}
function renderBouncedMailListDataTable(row,cont)
{
	var email = row.reciepientEmail;
	var name = row.reciepientName;
	var bouncetime = row.bounceTime;
	var bounceType = row.bounceType;
	var domainName = row.domainName;
	var error_code=row.bounceCode;
	var error_msg =row.reasonForBounce;
	if(email == null)
		{
		email =="";
		}
	if(name == null)
	{
		name =="";
	}
	if(bounceType == null)
	{
		bounceType =="";
	}
	if(domainName == null)
	{
		domainName =="";
	}
	if(bouncetime == null)
	{
		bouncetime =="";
	}
	
	var	error_msgshort=null;
	if(error_msg.length > 20)
		{
		error_msgshort=error_msg.substring(0,20).concat("...");
				}
	 else{
		error_msgshort=error_msg;
	} 

		$(".campaignstatus_all").append('<tr><td width="100" >'+email+' </td><td width="100" >'+name+' </td><td width="100" >'+bouncetime+' </td><td width="100" >'+bounceType+' </td><td width="100" >'+domainName+' </td><td width="100" >'+error_code+' </td><td width="100" title="'+error_msg+'">'+error_msgshort+' </td></tr>');
}

function renderSpamMailListData(objData,pagenumber, totalpages)
{
	
	pagenum = pagenumber;
	if(objData!= null && objData.length > 0)
	{
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:200px;">Click Time</th><th  style="border-right:1px solid #fff;width:250px;">Domain Name</th><th  style="border-right:1px solid #fff;width:200px;">Code</th><th  style="border-right:1px solid #fff;width:200px;">Reason</th></tr>');
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		
		var row = objData[count];
		renderSpamMailListDataTable(row, cont);
		cont++;
			
	}	
	
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;" ><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevSpamlistpage()">&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextSpamlistpage()"></td></tr>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"    id ="next" name = "next" value="Next" onclick="rendernextSpamlistpage()"></td></tr>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="previous" name = "previous" value="Previous" onclick="renderprevSpamlistpage()"></td></tr>');
			
		}
}
		else{
			$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:200px;">Click Time</th><th  style="border-right:1px solid #fff;width:250px;">Domain Name</th><th  style="border-right:1px solid #fff;width:200px;">Code</th><th  style="border-right:1px solid #fff;width:200px;">Reason</th></tr>');
			$(".campaignstatus_all").append('<tr><td style="color:red;" align="center"><b>No list found.</b></td></tr>');
		}
		
}
function renderSpamMailListDataTable(row,cont)
{
	var email = row.reciepientEmail;
	var name = row.reciepientName;
	var clickTime = row.clickDate;
	var domainName = row.domainName;
	var error_code=row.spamCode;
	var error_msg =row.otherReason;
	if(email == null)
		{
		email =="";
		}
	if(name == null)
	{
		name =="";
	}
	
	if(domainName == null)
	{
		domainName =="";
	}
	if(clickTime == null)
	{
		clickTime =="";
	}
		$(".campaignstatus_all").append('<tr><td width="100" >'+email+' </td><td width="100" >'+name+' </td><td width="100" >'+clickTime+' </td><td width="100" >'+domainName+' </td><td width="100" >'+error_code+' </td><td width="100" >'+error_msg+' </td></tr>');
}

function renderTotalClicksData(objData,pagenumber, totalpages,url1)
{
	var maxRecId = Math.max.apply(Math, recids);
	var minRecId=Math.min.apply(Math, recids);
	pagenum = pagenumber;
	if(objData.items !=null && objData.items.length > 0)
  {
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:200px;">Click Time</th><th  style="border-right:1px solid #fff;width:250px;">Host</th><th  style="border-right:1px solid #fff;width:250px;">Device</th><th  style="border-right:1px solid #fff;width:250px;">Location</th><th  style="border-right:1px solid #fff;width:250px;">Redirect Url</th></tr>');
	var cont =1;
	for(var count =0; count < objData.items.length; count++)
	{
		var row = objData.items[count];
		renderTotalClicksDataTable(row, cont);
		cont++;
			
	}	
	$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"></td><td class="alignView"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage(\''+url1+'\',\''+minRecId+'\')">&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextlistpage(\''+url1+'\',\''+maxRecId+'\')"></td></tr>');
	/*  if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage(\''+url1+'\',\''+minRecId+'\')">&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextlistpage(\''+url1+'\',\''+maxRecId+'\')"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage('+url1+')"></td></tr>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"    id ="next" name = "next" value="Next" onclick="rendernextlistpage(\''+url1+'\',\''+maxRecId+'\')"></td></tr>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="70%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage(\''+url1+'\',\''+minRecId+'\')"></td></tr>');
			
	} */
	}
else
{
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:200px;">Click Time</th><th  style="border-right:1px solid #fff;width:250px;">Host</th><th  style="border-right:1px solid #fff;width:250px;">Device</th><th  style="border-right:1px solid #fff;width:250px;">Location</th><th  style="border-right:1px solid #fff;width:250px;">Redirect Url</th></tr>');
   $(".campaignstatus_all").append('<tr><td style="color:red;" align="center"><b>No list found.</b></td></tr>');
}
}
function renderTotalClicksDataTable(row,cont)
{
	var email = row.reciepientEmail;
	var name = row.reciepientName;
	var openDate = row.clickDate;
	var host = row.clickIP;
	var device=row.device;
	var location=row.clickLocation;
	var linkurl=row.linkUrl;
	if(email == null)
	{
		email="";
	}
	if(name == null)
	{
		name="";
	}
	if(openDate == null)
	{
		openDate="";
	}
	if(host == null)
	{
		host="";
	}
	if(device == null)
	{
		device="";
	}
	if(location == null)
	{
		location="";
	}
	recids.push(row.messageOpenId);
	if(linkurl == null)
	{
		linkurl="";
	}
		$(".campaignstatus_all").append('<tr><td width="100" >'+email+' </td><td width="100" >'+name+' </td><td width="100" >'+openDate+' </td><td width="100" >'+host+' </td><td width="100" >'+device+' </td><td width="100" >'+location+' </td><td width="100" >'+linkurl+' </td></tr>');
		
}
///failed data popup data

function renderMailSentListData(objData,pagenumber, totalpages){
	pagenum = pagenumber;
	if(objData!= null && objData.length > 0)
	{
	$(".campaignstatus_all").append('<tr><th width="250">Email</th><th width="250">Name</th></tr>');
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderMailSentListDataTable(row, cont);
		cont++;
			
	}
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="100%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextmailFaillistpage()"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevmailFaillistpage()"></td></tr>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="100%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"    id ="next" name = "next" value="Next" onclick="rendernextmailFaillistpage()"></td></tr>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="100%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="previous" name = "previous" value="Previous" onclick="renderprevmailFaillistpage()"></td></tr>');
			
		}
	}
	else
		{
		$(".campaignstatus_all").append('<tr><th width="250">Email</th><th width="250">Name</th></tr>');
		 $(".campaignstatus_all").append('<tr><td style="color:red;" align="center"><b>No list found.</b></td></tr>');
		}
}
function renderMailSentListDataTable(row,cont)
{
	    var email = row.email;
	    var name = row.name;
		$(".campaignstatus_all").append('<tr><td>'+email+' </td><td>'+name+' </td></tr>');
		
}


function renderMailSentSoFarListData(objData,pagenumber, totalpages){
	pagenum = pagenumber;
	if(objData!= null && objData.length > 0)
	{
	$(".campaignstatus_all").append('<tr><th width="250">Email</th><th width="250">Name</th></tr>');
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderMailSentSoFarListDataTable(row, cont);
		cont++;
			
	}
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="100%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextmailSentSoFarpage()"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevmailSentSoFarpage()"></td></tr>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="100%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"    id ="next" name = "next" value="Next" onclick="rendernextmailSentSoFarpage()"></td></tr>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="100%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="previous" name = "previous" value="Previous" onclick="renderprevmailSentSoFarpage()"></td></tr>');
			
		}
	}
	else
		{
		$(".campaignstatus_all").append('<tr><th width="250">Email</th><th width="250">Name</th></tr>');
		 $(".campaignstatus_all").append('<tr><td style="color:red;" align="center"><b>No list found.</b></td></tr>');
		}
}
function renderMailSentSoFarListDataTable(row,cont)
{
	    var email = row.email;
	    var name = row.name;
		$(".campaignstatus_all").append('<tr><td align="center">'+email+' </td><td align="center">'+name+' </td></tr>');
		
}

function renderClicksThroughEmailData(objData,pagenumber, totalpages,url1)
{
	pagenum = pagenumber;
	if(objData.items !=null && objData.items.length > 0)
  {
		$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:250px;">Url</th></tr>');
	var cont =1;
	for(var count =0; count < objData.items.length; count++)
	{
		var row = objData.items[count];
		renderClicksthroughDataTable(row, cont);
		cont++;
			
	}	
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="100%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextlistpage('+url1+')"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage('+url1+')"></td></tr>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="100%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"    id ="next" name = "next" value="Next" onclick="rendernextlistpage('+url1+')"></td></tr>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$(".campaignstatus_all_page").append('<tr style="background-color: #fff;"><td align="center" width="100%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td class="alignView"><input type="submit"   id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage('+url1+')"></td></tr>');
			
		}
}
else
{
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:250px;">Url</th></tr>');
   $(".campaignstatus_all").append('<tr><td style="color:red;" align="center"><b>No list found.</b></td></tr>');
}
}
function renderClicksthroughDataTable(row,cont)
{
	
	var email = row.reciepientEmail;
	var name = row.reciepientName;
	var linkurl=row.linkUrl;
	if(email == null)
	{
		email="";
	}
	if(name == null)
	{
		name="";
	}
	
	if(linkurl == null)
	{
		linkurl="";
	}
		$(".campaignstatus_all").append('<tr><td width="100" >'+email+' </td><td width="100" >'+name+' </td><td width="100" >'+linkurl+' </td></tr>');
		
}
//close failed data popup//

//For download All.
function get_Report_Download(url,type) {

	if(url.match(/getmailFailedDownload/g) || url.match(/exportBounces/g) || url.match(/Spams/g) || url.match(/Unsubscribe/g)|| url.match(/SentSoFar/g)){
		get_Report_MailDownload(url,type);
		return false;
		}
	else{
	var campaignId='<%=session.getAttribute("campaignId") %>';
	window.location.href = "campaignReportsDownload.htm?campaignId="
			+ campaignId + "&url=" + url;
		}
	return false;
}
//download For Failed Data
function get_Report_MailDownload(url,type){
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
	
	window.location.href = url + "?campaignId=" + campaignId+"&bounceType="+type+"&fromdate="+fromdate+"&todate="+todate;
		
	return false;
}
//Search For All

function searchFilterData(url,headingname) {

		var fromdate = document.getElementById("fromdate").value;
		var todate = document.getElementById("todate").value;

		if (fromdate != "" && (todate == null || todate == "")) {
			alert("please select fromdate and todate ");
			return false;
		} else if (todate != "" && (fromdate == null || fromdate == "")) {
			alert("please select fromdate and todate ");
			return false;
		} else if (todate != "" && fromdate != "") {
			if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
					.parseDate('dd-mm-yy', todate)) {

				alert('please select valid date range, from date  :' + fromdate
						+ 'is later than todate :' + todate);
				return false;

			}
		}

		get_Report_Data(url, headingname,fromdate, todate);

	}
	//For campaign Bounces
	
   function getBouncedMailListData(url,type){

		var fromdate = document.getElementById("fromdate").value;
		var todate = document.getElementById("todate").value;
		var campaignId='<%=session.getAttribute("campaignId")%>';
		if (todate != "" && fromdate != "") {
			if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
					.parseDate('dd-mm-yy', todate)) {

				alert('please select valid date range, from date  :' + fromdate
						+ 'is later than todate :' + todate);
				return false;

			}
		}

		$.ajax({
			type : "get",
			url : "bounceList.htm?campaignId=" + campaignId + "&fromdate="
					+ fromdate + "&todate=" + todate + "&url=" + url+ "&type=" + type+"&pageNumber="+pageNumber,
			dataType : "json",
			success : function(objData) {
				renderBouncedMailListData(objData.result, objData.pagenumber,
						objData.totalpages);

			},
			error : function(data) {

				alert("error " + data);
			}
		});
	}
	
   function renderprevBouncelistpage(){
		
		$(".campaignstatus_all").empty();
		$(".campaignstatus_all_page").empty();
		var type='<%=session.getAttribute("type") %>';
		var fromdate = document.getElementById("fromdate").value;
		var todate = document.getElementById("todate").value;
		var campaignId='<%=session.getAttribute("campaignId")%>';
		if (todate != "" && fromdate != "") {
			if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
					.parseDate('dd-mm-yy', todate)) {

				alert('please select valid date range, from date  :' + fromdate
						+ 'is later than todate :' + todate);
				return false;

			}
		}
	   pageNumber = pageNumber - 1;
		$.ajax({
			type : "get",
			url : "bounceList.htm?campaignId=" + campaignId + "&fromdate="
					+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
			dataType : "json",
			success : function(objData) {
				renderBouncedMailListData(objData.result, objData.pagenumber,
						objData.totalpages);

			},
			error : function(data) {

				alert("error " + data);
			}
		});
	}
	function rendernextBouncelistpage(){
		$(".campaignstatus_all").empty();
		$(".campaignstatus_all_page").empty();
		
		var type='<%=session.getAttribute("type") %>';
		var fromdate = document.getElementById("fromdate").value;
		var todate = document.getElementById("todate").value;
		var campaignId='<%=session.getAttribute("campaignId")%>';
		if (todate != "" && fromdate != "") {
			if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
					.parseDate('dd-mm-yy', todate)) {

				alert('please select valid date range, from date  :' + fromdate
						+ 'is later than todate :' + todate);
				return false;

			}
		}
	pageNumber = pageNumber + 1;
		$.ajax({
			type : "get",
			url : "bounceList.htm?campaignId=" + campaignId + "&fromdate="
					+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
			dataType : "json",
			success : function(objData) {
				renderBouncedMailListData(objData.result, objData.pagenumber,
						objData.totalpages);

			},
			error : function(data) {

				alert("error " + data);
			}
		});
	}
function renderprevlistpage(url1,recId1){
	
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	var type='<%=session.getAttribute("type") %>';
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	var recId=recId1-20;
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
   pageNumber = pageNumber - 1;
	$.ajax({
		type : "get",
		url : "searchMailList.htm?campaignId=" + campaignId + "&fromdate="
		+ fromdate + "&todate=" + todate + "&type="+type+"&url=" + url1+"&pageNumber="+pageNumber+"&recId="+recId,
		dataType : "json",
		success : function(objData) {
			renderBouncedMailListData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}
function rendernextlistpage(url1,recId1){
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	
	var type='<%=session.getAttribute("type") %>';
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	var recId=recId1+1;
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
pageNumber = pageNumber + 1;
	$.ajax({
		type : "get",
		url : "searchMailList.htm?campaignId=" + campaignId + "&fromdate="
		+ fromdate + "&todate=" + todate + "&type="+type+"&url=" + url1+"&pageNumber="+pageNumber+"&recId="+recId,
		dataType : "json",
		success : function(objData) {
			renderBouncedMailListData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}
function getSpamMailListData(url,type){
	
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}

	$.ajax({
		type : "get",
		url : "spamList.htm?campaignId=" + campaignId + "&fromdate="
		+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			renderSpamMailListData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}
function renderprevSpamlistpage(){
	
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	var type='<%=session.getAttribute("type") %>';
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
   pageNumber = pageNumber - 1;
	$.ajax({
		type : "get",
		url : "spamList.htm?campaignId=" + campaignId + "&fromdate="
				+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			renderSpamMailListData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}
function rendernextSpamlistpage(){
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	
	var type='<%=session.getAttribute("type") %>';
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
pageNumber = pageNumber + 1;
	$.ajax({
		type : "get",
		url : "spamList.htm?campaignId=" + campaignId + "&fromdate="
		+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			renderSpamMailListData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}


function getUnsubscribeMailListData(url,type){
	
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}

	$.ajax({
		type : "get",
		url : "unsubscribeList.htm?campaignId=" + campaignId + "&fromdate="
		+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			renderUnsubscribeData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}
function renderprevUnsubscribelistpage(){
	
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	var type='<%=session.getAttribute("type") %>';
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
   pageNumber = pageNumber - 1;
	$.ajax({
		type : "get",
		url : "unsubscribeList.htm?campaignId=" + campaignId + "&fromdate="
				+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			renderUnsubscribeData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}
function rendernextUnsubscribelistpage(){
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	
	var type='<%=session.getAttribute("type") %>';
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
pageNumber = pageNumber + 1;
	$.ajax({
		type : "get",
		url : "unsubscribeList.htm?campaignId=" + campaignId + "&fromdate="
		+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			renderUnsubscribeData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}

function renderprevmailFaillistpage(){
	
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	var type='<%=session.getAttribute("type") %>';
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
   pageNumber = pageNumber - 1;
	$.ajax({
		type : "get",
		url : "getMailFailData.htm?campaignId=" + campaignId + "&fromdate="
				+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			renderMailSentListData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}
function rendernextmailFaillistpage(){
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	
	var type='<%=session.getAttribute("type") %>';
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
pageNumber = pageNumber + 1;
	$.ajax({
		type : "get",
		url : "getMailFailData.htm?campaignId=" + campaignId + "&fromdate="
				+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			renderMailSentListData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}

function renderprevmailSentSoFarpage(){
	
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	var type='<%=session.getAttribute("type") %>';
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
   pageNumber = pageNumber - 1;
	$.ajax({
		type : "get",
		url : "getMailSentSoFar.htm?campaignId=" + campaignId + "&fromdate="
				+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			renderMailSentSoFarListData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}
function rendernextmailSentSoFarpage(){
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	
	var type='<%=session.getAttribute("type") %>';
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
pageNumber = pageNumber + 1;
	$.ajax({
		type : "get",
		url : "getMailSentSoFar.htm?campaignId=" + campaignId + "&fromdate="
				+ fromdate + "&todate=" + todate + "&type=" + type+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {
			renderMailSentSoFarListData(objData.result, objData.pagenumber,
					objData.totalpages);

		},
		error : function(data) {

			alert("error " + data);
		}
	});
}

function getMailedSentSoFarData(url,type)
{
	var fromdate = document.getElementById("fromdate").value;
	var todate = document.getElementById("todate").value;
	var campaignId='<%=session.getAttribute("campaignId")%>';
	if (todate != "" && fromdate != "") {
		if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker
				.parseDate('dd-mm-yy', todate)) {

			alert('please select valid date range, from date  :' + fromdate
					+ 'is later than todate :' + todate);
			return false;

		}
	}
	$.ajax({
		type : "get",
		url : url + "?campaignId=" + campaignId + "&fromdate=" + fromdate
				+ "&todate=" + todate+"&pageNumber="+pageNumber,
		dataType : "json",
		success : function(objData) {

			renderMailSentSoFarListData(objData.result,objData.pagenumber, objData.totalpages);

		},
		error : function(objData) {

			alert("error1 " + objData);
		}
	});
	return false;
}

</script>
</head>
<body>
<div style="margin-left:05px;margin-top:10px"><label style="color: #666;font-size: 14px;font-weight: bold;" id="campaignstatus_header"></label></div>
<table id="tbl" style="margin-top:10px;margin-bottom:05px;">
<tr>
<td><label style="color: #666;font-size: 14px;">From:</label>
<input type="text" name="fromdate" id="fromdate" class="datepicker" onclick="populateDate()" size="25"/></td>
<td><label style="color: #666;font-size: 14px;">To:</label><input type="text" name="todate" id="todate" class="datepicker"  size="25"/></td>
<td style="background-color:#fff;" align="center"><input type="button" id="buttonclass" value="Search" onclick="searchFilterData('<%=session.getAttribute("viewUrl") %>','<%=session.getAttribute("type") %>')"></td>
<td style="background-color:#fff;padding-left:-40px;"><input type="button" id="buttonclass" value="Export" onclick="get_Report_Download('<%=session.getAttribute("exportUrl") %>','<%=session.getAttribute("type") %>')" ></td>
</tr>
</table>
<table class="campaignstatus_all"  id="tblewidth" width="100%">
</table>
 <table class="campaignstatus_all_page"  id="tblewidth" style="width:100%;"></table>
</body>
</html>