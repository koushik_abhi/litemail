<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Campaign Status</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="./css/smoothness/jquery-ui-smoothness.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="./js/jquery-1.9.1.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/jquery-ui.js" charset="utf-8"></script>

<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="js/jquery-ui-timepicker-addon.css" />

<style type="text/css">

td, th {
    padding: 2px;
}

input{font-size:15px; font-weight:normal;}
td a{
text-decoration:underline ;
color:#000;
}
.alignView
{
float:right;
margin-left:50px;
}
.alignLeft
{
float:left;
}
.datepicker
{
background: none repeat scroll 0 0 #FBFBFB;
border: 1px solid #E5E5E5;
box-shadow: 1px 1px 2px rgba(200, 200, 200, 0.2) inset;
color: #555555;font-size: 18px;
font-weight: normal;
line-height: 1;margin-bottom: 16px;
margin-right: 6px;
margin-top: 4px;outline: 0 none;padding: 6px;
}

</style>
<script>
var pagenum = 1;
var campaignId="";

$(document).ready(function() {
	
	campaignId ="${campaignId}";
	//alert(campaignId);
	$(".advAnalytics").click(function(){
		window.location.href="showAdvAnalytics.htm?campaignId="+campaignId;
		});
	rendercampaignstatusinfo();
	renderClickThroughStatus();
	
	});

function rendercampaignstatusinfo()
{  
	campaignId ="${campaignId}";
	$("#statusrows").empty();
	$("#statusvalues").empty();
	$.ajax({
		type: "get",
		url: "displaycampaignreport.htm?campaignId="+campaignId,
		dataType: "json",
		success: function( objData )
		{
			rendermailsentlist(objData.report,objData.counts);
		//	rendermailBouncelist(objData.counts);
			
		},
		error: function(data){
			
			//alert("error " +data);
		}
	});
	
}
function renderClickThroughStatus()
{  
	campaignId ="${campaignId}";
	$("#statusrows").empty();
	$("#statusvalues").empty();
	$.ajax({
		type: "get",
		url: "getClickThroughLinkCounts.htm?campaignId="+campaignId,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData);
		
		},
		error: function(data){
			
			//alert("error " +data);
		}
	});
	
}
function renderlisttable(objData){
	   
	$(".clickThroughStatus").append('<tr><th width="5%"><strong>Position</strong></th><th width="20%"><strong>URL</strong></th><th width="15%"><strong>Total No of Clicks</strong></th><th width="15%"><strong>Clicked Emails</strong></th></tr>');

		var cont =1;
		for(var count =0; count < objData.length; count++)
		{
			var row = objData[count];
			renderRowInListTable(row, cont);
			cont++;
				
		}
	    	
}		

function renderRowInListTable(row, cont)
{
	var campaignId=row.campaignId;
	var urlname = row.position;
	var url = row.url;
	var uid=row.position;
	var totalclicks = row.noOfClicks;
	if(totalclicks == null ||  totalclicks =="")
		totalclicks = 0;
	var clickedemails = "";
		$(".clickThroughStatus").append('<tr><td width="5%" >'+urlname+' </td><td  width="20%">'+url+'</td><td  width="15%" align="center" >'+totalclicks+'</td><td  width="15%" align="center" >'+clickedemails+'&nbsp;&nbsp;<a href="#" id="clickedEmailsView" onclick="getReportPopUp(\'ViewMailURLClickApi.htm&uId='+uid+'\',\'Click Through Email List\',\'downloadAllClicksForCampaign.htm\')"> View</a></td></tr>');
		
}



function rendermailsentlist(maildata,counts)
{
	campaignStartDateTime = maildata.campaignStartDateTime;
	var status = maildata.status;
	if(status =="scheduled")
	{
		campaignStartDateTime = maildata.scheduleTime;
	}
	
	$("#campaignname").append(maildata.campaignName);
	$("#campstatus").append(status);
	$("#startdatetime").append(campaignStartDateTime);
	$("#enddatetime").append(maildata.campaignEndDateTime);
	$("#emailtosend").append(maildata.noOfMails);
	$("#emailsent").append(maildata.mailsSent);
	$("#emailsfailed").append(maildata.mailsFailed);
	$("#showcaseclicks").append(maildata.noOfshowcaseClicks); 
    $("#totalopens").append(counts.totalopens);
	$("#totalclicks").append(counts.totalclicks);
	$("#uniqueopens").append(counts.uniqueopens);
	$("#uniqueclicks").append(counts.uniqueclicks); 
	
	$("#hardbounce").append(counts.hardbounce);
	$("#softbounce").append(counts.softbounce);
	$("#generalbounce").append(counts.generalbounce);
	$("#spamcomplaint").append(counts.spamcount);
	
	$("#transientbounce").append(counts.transientbounce);
	$("#totalmailbounced").append(counts.totalbounce);
	$("#mailblock").append(0);
	$("#unsubscribe").append(counts.unsubscribe);
	$("#autoreply").append(0);
	$("#others").append(0);
	
	//For Mailed Open Graph
	var mailSent = maildata.mailsSent;
	var totalOpens = counts.totalopens;
	
	if (totalOpens == "undefined" || totalOpens == null) {
			totalOpens = 0;
		}
		if (mailSent == "undefined" || mailSent == null) {
			mailSent = 0;
		}

		var imageUrl = "./getMailOpendStatus.htm?totalopens=" + totalOpens
				+ "&mailsent=" + mailSent;
		$("#mailsentgraph").attr('src', imageUrl);

	}
function rendermailBouncelist(objData)
{
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderRowInBounceList(row, cont);
		cont++;
			
	}
}
function renderRowInBounceList(row,cont)
{
	$("#hardbounce").append(row.hardBounce);
	$("#softbounce").append(row.softBounce);
	$("#generalbounce").append(row.generalBounce);
	$("#spamcomplaint").append(row.spamCount);
	
}

	function getReportPopUp(url, headingname, url2) {
		alert(headingname);
		 var campaignId = "${campaignId}";	
		 $.ajax({
				type : "get",
				url : "urlrender.htm?campaignId=" + campaignId + "&url=" + url+"&url2="+url2+"&type="+headingname,
				dataType : "html",
				success : function(objData) {
					window.open("reportpopup.jsp","",'width=800,height=500,scrollbars=yes');
				},
				error : function(data) {

					//alert("data " + data);
				}
			});
			return false;
		}
	
	function cancel() {
		window.location = "campaignManagement.jsp";
	}
	function refresh()
	{
		var campaignId ="${campaignId}";
		var campaignname = "${campaignname}";
		window.location.href = "campaignreports.htm?campaignname="
				+ campaignname + "&campaignId="+campaignId;
				
	}

	
</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content"  >
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
    <div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="86" height="127"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports-red.jpg" width="101" height="127"></span></span></div>         
 <div class="contents">
      <div align="center" style="color:red">${errorMessage}</div><br>
        <div style="float:right;margin-top:-15px;">
		<input type="button" name="submit" id="buttonclass" value="Advanced Analytics" class="advAnalytics">&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" name="submit" id="buttonclass" value="Refresh" onclick="refresh();" id="submit" >&nbsp;&nbsp;&nbsp;&nbsp;<input type=button id="buttonclass" value="Cancel" onclick="cancel();"></div>
        <div style="margin-top:20px;border:solid 0px #eee;height:40px;">
        <table width="80%">
        <tr>
        <td width="50%" style="padding-bottom: 5px;padding-top:10px;padding-left:85px;"><label  id="campaignname" ><b>Campaign Name :</b>&nbsp; &nbsp; </label></td>
        <td width="50%" style="padding-bottom: 5px;padding-top:10px;padding-left:103px;"><label   id="campstatus"><b>Campaign Status :</b>&nbsp; &nbsp;  </label></td>
        </tr></table>
        </div>
        <table width="80%">		
        		<tr>
        			<td width="50%" style="padding-bottom: 10px;">
        						
        			 <div style="border:solid 0px #eee;margin-left:80px;">
        				<table>
        						
        						<tr><td><label ><strong>Mail Sent Data</strong></label></td></tr>
        						 <tr>
    								<td><label >Start Date/Time :</label></td>
   								    <td><span class="alignLeft"><label id="startdatetime" style="color:#000;"></label></span></td>
  								</tr>
 							    <tr>
   									 <td><label >End Date/ Time :</label></td>
  									  <td><span class="alignLeft"><label id="enddatetime" style="color:#000;"></label></span></td>
  								</tr>
 								 <tr>
  									 <td><label >No of Emails to Send :</label></td>
  									 <td><span class="alignLeft"><label id="emailtosend" style="color:#000;"></label></span></td>
							     </tr>
  								 <tr>
  									 <td><label >No of Emails Sent So Far :</label></td>
  									 <td>
  									 <span class="alignLeft"><label id="emailsent" style="color:#000;"></label></span>								
  									</td>
 								 </tr>
  								 <tr>
 							 		 <td><label >No of Emails Failed :</label></td>
   									 <td >
   									<span class="alignLeft"> <label id="emailsfailed" style="color:#000;"></label></span>
   									 <span class="alignView">
  									  <a href="#" id="mailfailstatus" onclick="getReportPopUp('getMailFailData.htm','Email Failed List','getmailFailedDownload.htm')"> View</a></span>
   									 </td>
 								 </tr>
        				</table>
        				</div>
        			</td>
       
        			<td width="50%" style="padding-left:20px;padding-bottom: 10px;">
        			 <div style="border:solid 0px #eee;margin-left:80px;">
        				<table >
        				<tr>
   							<td><label ><strong> Mail Open Data</strong></label></td>
  						</tr>
  						<tr></tr>
   						<tr>
   							<td><label >Total Opens :</label></td>
   							<td><span class="alignLeft"><label id="totalopens" style="color:#000;"></label></span>
   							<span class="alignView">
   							<a href="#" id="totalopensView" onclick="getReportPopUp('ViewMailOpenApi.htm','Total Opens List','downloadAllOpensForCampaign.htm')">View</a></span>
   							
   							</td>
  						</tr>
   						<tr>
  							 <td><label >Total Clicks :</label></td>
  							 <td><span class="alignLeft"><label  id="totalclicks" style="color:#000;"></label></span>
  							<span class="alignView">
  							 <a href="#" id="totalclicksView" onclick="getReportPopUp('ViewMailClickApi.htm','Total Clicks List','downloadAllClicksForCampaign.htm')">View</a></span>
  							</td>
  						</tr>
  					    <tr>
   							<td><label >Unique Opens :</label></td>
   							<td><span class="alignLeft"><label id="uniqueopens" style="color:#000;"></label></span>
   							<span class="alignView">
   							<a href="#" id="uniqueopensView" onclick="getReportPopUp('ViewMailUniqueOpenApi.htm','Unique Opens List','downloadAllUniqueOpensForCampaign.htm')">View</a></span>
   							  							</td>
  						</tr>
     					<tr>
   							<td><label>Unique Clicks :</label></td>
   							<td><span class="alignLeft"><label id="uniqueclicks" style="color:#000;"></label></span>
   							<span class="alignView">
   							<a href="#" id="uniqueclicksView" onclick="getReportPopUp('ViewMailUniqueClickApi.htm','Unique Clicks List','downloadAllUniqueClicksForCampaign.htm')">View</a>
   							</span>
   			            </td>
  						</tr>
  						<tr><td>&nbsp;</td></tr>
        				</table>
        				</div>
        		</td>
        		</tr>
        </table>	
        
        <table style="margin-top:10px;margin-bottom:20px;margin-left:80px;">	
        		<tr >
        			<td ><input name="imageField" type="image" src="" width="400" height="250" border="0" alt="Pie chart" id="mailsentgraph">
        			
        			</td>
        		</tr>
        		<tr><td></td></tr>
        </table>
          <span style="margin-left:85px;"> <label><strong>Click through status </strong></label></span>      
                  <table width="50%"  cellpadding="3" cellspacing="1" border="0" id="tbl" style="margin-top:10px;margin-left:85px;" class="clickThroughStatus"> </table>
                   <table style="margin-top:10px;margin-bottom:10px;margin-left:80px;"><tr><td id="showcaseclicks"><label ><strong>Total No Of Showcase Clicks :</strong> </label></td></tr></table>
         <table style="margin-top:20px;margin-bottom:20px;margin-left:80px;" width="100%">   
				<tr>            
                   <td ><input name="imageField" type="image" src="./getClickthroughStatus.htm?campaignId=<%= request.getAttribute("campaignId")%>" width="400" height="250" border="0" alt="Bar graph"></td>
                </tr>  
        </table>
        
        <table width="80%" style="padding-bottom: 10px;">
   				<!-- <tr>
   					<td  width="250" ><label ><strong> Mail Bounced Data</strong></label></td>
  				</tr> -->
   				<tr>
   					<td width="40%" style="padding-bottom: 10px;">
   					 <div style="border:solid 0px #eee;margin-left:80px;">
   						<table>
   						<tr>
   					<td ><label ><strong> Mail Bounced Data</strong></label></td>
  				      </tr>
   								<tr>
   									<td ><label>Hard Bounce :</label></td>
   									<td><span class="alignLeft"><label id="hardbounce" style="color:#000;"></label></span>
   									<span class="alignView">
   									<a href="#" id="hardbounceView" onclick="getReportPopUp('ViewMailHardBounceApi.htm','HardBounce ','exportBounces.htm')">View</a></span>
   									
   									</td>
  								</tr>
   								<tr>
   									<td><label>Soft Bounce :</label></td>
   									<td><span class="alignLeft"><label id="softbounce" style="color:#000;"></label></span>
   									<span class="alignView">
   									<a href="#" id="softbounceView" onclick="getReportPopUp('ViewMailSoftBounceApi.htm','SoftBounce','exportBounces.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
   								<tr>
   									<td><label>General Bounce :</label></td>
   									<td><span class="alignLeft"><label id="generalbounce" style="color:#000;"></label></span>
   									<span class="alignView">
   									<a href="#" id="generalbounceView" onclick="getReportPopUp('ViewMailGeneralBounceApi.htm','GeneralBounce ','exportBounces.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
     							<tr>
   									<td><label>Transient Bounce :</label></td>
   									<td><span class="alignLeft"><label id="transientbounce" style="color:#000;"></label></span>
   									<span class="alignView">
   									<a href="#" id="transientbounceView" onclick="getReportPopUp('ViewMailTransientBounceApi.htm','TransientBounce','exportBounces.htm')">View</a></span>
   									
   									</td>
  								</tr>
	  							
  								<tr>
   									<td><label>Total Number of Mails Bounced :</label></td>
   									<td><span class="alignLeft"><label id="totalmailbounced" style="color:#000;"></label></span>
   									<span class="alignView">
   									<a href="#" id="totalmailbouncedView" onclick="getReportPopUp('ViewMailBounceApi.htm','totalmailBounce','exportBounces.htm')">View</a></span>
   									
   									</td>
  								</tr>
  								<tr>
   									<td><label>Blocked Mails :</label></td>
   									<td><span class="alignLeft"><label id="mailblock" style="color:#000;"></label></span>
   									<span class="alignView">
   									<a href="#" id="mailblockView" onclick="getReportPopUp('ViewMailBounceApi.htm','mailblock','exportBounces.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>			
  								<tr>
   									<td><label>Spam Complaint :</label></td>
   									<td><span class="alignLeft"><label id="spamcomplaint" style="color:#000;"></label></span>
   									<span class="alignView">
   									<a href="#" id="spamcomplaintView" onclick="getReportPopUp('ViewMailAllSpamsApi.htm','Spam Complaint List','exportSpams.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
  								<tr>(Note:bounces and spams are shown at domain level )</tr>
  						</table>
  						</div>
  						</td>
  					<td width="40%" style="padding-left:20px;padding-bottom: 10px;" >
  					 <div style="border:solid 0px #eee;margin-left:80px;">
  						<table>
  						<tr><td>&nbsp;</td></tr>
  						<tr><td></td></tr>
  						<tr>
   									<td><label>Unsubscribe :</label></td>
   									<td><span class="alignLeft"><label id="unsubscribe" style="color:#000;"></label></span>
   									<span class="alignView">
   									<a href="#" id="unsubscribeView" onclick="getReportPopUp('ViewMailUnsubscribeApi.htm','Unsubscribe List','downloadAllUnsubscribeForCampaign.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
  						
    							<tr>
   									<td><label>Auto Reply :</label></td>
   									<td><span class="alignLeft"><label id="autoreply" style="color:#000;"></label></span>
   									<span class="alignView">
   									<a href="#" id="autoreplyView" onclick="getReportPopUp('ViewMailBounceApi.htm','AutoReply Bounce List','exportBounces.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
    							<tr>
   									<td><label>Others :</label></td>
   									<td><span class="alignLeft"><label id="others" style="color:#000;" class="campaignCount"></label></span>
   									<span class="alignView">
   									<a href="#" id="othersView" onclick="getReportPopUp('ViewMailBounceApi.htm','Others Bounce List','exportBounces.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
  								<tr><td>&nbsp;</td></tr>
  									<tr><td>&nbsp;</td></tr>
  									<tr><td>&nbsp;</td></tr>
  									<tr><td>&nbsp;</td></tr>
  						</table>   
  						</div> 
  					</td>	  
        	</tr>
        	</table>
</div>
</div>
</div>

<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>