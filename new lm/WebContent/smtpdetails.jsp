<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SMTP Details</title>
<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){
	getSmtp_Detals();
});

function getSmtp_Detals()
{
	
	$.ajax({
		type: "get",
		url: "smtpDisplayConfigs.htm",
		dataType: "json",
		success: function( objData )
		{
			renderListData(objData.result);
		},
		error: function(data){
			window.location = "smtpdetails.jsp";
		}
	});
	return false;	
	}
	function renderListData(objData)
	{
		if(objData != null && objData.length > 0)
		{
			$("#smtpdetails").append('<tr><th width="100">SMTP Server Name</th><th width="100">SMTP Server Ip</th><th width="100">Created By</th><th width="100">Created On</th><th width="50">Actions</th></tr>');
			var cont =1;
			for(var count =0; count < objData.length; count++)
			{
				var row = objData[count];
				renderRowInListTable(row, cont);
				cont++;
					
			}
		}
		else
		{
			$("#smtpdetails").append('<tr><td style="color:red">No list found.</td></tr>');
		}
		
	}
	function renderRowInListTable(row, cont)
	{
		var servername=row.smtpServer;
		var serverip=row.smtpServerIp;
		var createdby=row.smtpUser;
			$("#smtpdetails").append('<tr><td width="100">'+servername+'</td><td width="100">'+serverip+'</td><td width="100">'+createdby+'</td><td width="100">'+""+'</td><td width="50" align="center"><a ><img src="images/edit.png" class="tooltip" title="edit" onclick="editRow(\''+"SMTP_ID"+'\');" /></a>&nbsp;&nbsp;<img src="images/delete.png" class="tooltip" title="delete" onclick="deleteRow(\''+"SMTP_ID"+'\');" /></td></tr>');
	}
	function deleteRow(id)
	{
		if(confirm("are you sure you want to delete this record" ))
		{
		$.ajax({
			type: "get",
			url: "deleteSmtp.htm?listrecs="+id.toString(),
			data: id,
			dataType: "json",
			success: function( objData )
			{
				window.location = "smtpdetails.jsp";	
			},
			error: function(data){
				
				//alert( "An error occurred" +data[0] );
				window.location = "smtpdetails.jsp";
			}
		});
		}
		return false;
		
	}
	function editRow(id)
	{
		$.ajax({
			type: "get",
			url: "editSmtp.htm?listrecs="+id.toString(),
			data: id,
			dataType: "html",
			success: function( objData )
			{
				window.location = "smtpdetails.jsp";	
			},
			error: function(data){
				
				//alert( "An error occurred" +data[0] );
				window.location = "smtpdetails.jsp";
			}
		});
		return false;
		
	}
	</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="loginscreen.jsp">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
		<div id="loading-popup-box" style="display: none;"></div>
		<div id="content-main">
			<div class="workflow">
				<span><img src="./images/list-active.jpg" width="115"
					height="127"></span><span class="arrow"><img
					src="./images/arrow.jpg" width="46" height="127"></span><span><img
					src="./images/define-fields.jpg" width="132" height="127"></span><span
					class="arrow"><img src="./images/arrow.jpg" width="46"
					height="127"></span><span><img src="./images/maping.jpg"
					width="86" height="127"><span class="arrow"><img
						src="./images/arrow.jpg" width="46" height="127"></span><span><img
						src="./images/enhance.jpg" width="101" height="127"></span></span>
			</div>
			<div class="contents">
			<div class="create_form">
		    <a href="addsmtpserver.jsp" id="buttonclass">Add Smtp Server</a>
			</div>
			<table id="smtpdetails" class="tblewidth">
			</table>
			</div>
		</div>
	</div>

	<div id="footer">
		<div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div>
	</div>
</body>
</html>