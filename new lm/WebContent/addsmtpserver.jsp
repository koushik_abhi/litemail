<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SMTP Configuration</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){
	 getPartners();
});
</script>
<script>
function getPartners()
{

	$.ajax({
		type: "get",
		url: "getAllPartners.htm",
		dataType: "json",
		success: function( objData )
		{
			populatePartners(objData);
					
			
		},
		error: function(){
			alert( "An error occurred" );
		}
	});
		
		
}
function populatePartners(objData)
{
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderPartners(row, cont);
		cont++;
			
	}
}
function renderPartners(row,cont)
{
	var partnerName = row.name;
	 var partner_data="<option value="+partnerName+">"+partnerName+"</option>";
     $(partner_data).appendTo('#partner'); 
}
function validate()
{
	var smtpServerName=$("#smtpservername").val();
	var smtpServerIp=$("#smtpserverip").val();
	var smtpUserName=$("#smtpusername").val();
	var smtpPassword=$("#smtppassword").val();
	var apiKey=$("#apikey").val();
	
	if(smtpServerName =="" || smtpServerName == null)
		{
		alert("Please provide SMTP Server Name");
		smtpform.smtpservername.focus();
		return false;
		}
	if(smtpServerIp =="" || smtpServerIp == null)
	{
	alert("Please provide SMTP Server Ip");
	smtpform.smtpserverip.focus();
	return false;
	}
		
	if(smtpUserName =="" || smtpUserName == null)
	{
	alert("Please provide SMTP User Name");
	smtpform.smtpusername.focus();
	return false;
	}
	if(smtpPassword =="" || smtpPassword == null)
	{
	alert("Please provide SMTP Password ");
	smtpform.smtppassword.focus();
	return false;
	}
	if(apiKey =="" || apiKey == null)
	{
	alert("Please provide API Key ");
	smtpform.apikey.focus();
	return false;
	}
	
	
	/* return true;  */
}

function fnValidateIPAddress(ipaddr) {
    //Remember, this function will validate only Class C IP.
    //change to other IP Classes as you need
    ipaddr = ipaddr.replace( /\s/g, ""); 
    var re = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/; //regex. check for digits and in
                                          //all 4 quadrants of the IP
    if (re.test(ipaddr)) {
        //split into units with dots "."
        var parts = ipaddr.split(".");
        //if the first unit/quadrant of the IP is zero
        if (parseInt(parseFloat(parts[0])) == 0) {
            return false;
        }
        //if the fourth unit/quadrant of the IP is zero
        if (parseInt(parseFloat(parts[3])) == 0) {
            return false;
        }
        //if any part is greater than 255
        for (var i=0; i<parts.length; i++) {
            if (parseInt(parseFloat(parts[i])) > 255){
                return false;
            }
        }
        return true;
    } else {
        return false;
    }
}
function cancel()
{
	
	window.location="smtpManagement.jsp";
}

</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showusers.htm" class="left-nav" >Users </a>
 <a href="showpartners.htm">Partners </a>
  <a href="showcredits.htm"> Credits</a>
  <a href="showsmtpManagement.htm" id="selected"> SMTP Configuration</a>
  <a href="showemailconfig.htm"> Notification Settings</a>
 <a href="showdashboard.htm" class="right-nav"> Dashboard </a>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
	     <div class="contents">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<form action="addsmtp.htm" method="post" name="smtpform">
    <table    width="100%" border="0" cellspacing="0" cellpadding="0" id="create_list">
  <tbody >
 <tr >
    <td width="150"><label>SMTP Server Name : <font style="color:red;">*</font></label></td>
    <td><input type="text" id = "smtpservername"  name="smtpservername" size="30" value=""></td>
  </tr>
  <tr>
   <td><label>SMTP Server Ip: <font style="color:red;">*</font></label></td>
   <td><input type="text" id = "smtpserverip"  name="smtpserverip" size="30" value=""></td>
  </tr>
  <tr>
   <td><label>SMTP User Name : <font style="color:red;">*</font></label></td>
   <td><input type="text" id = "smtpusername"  name="smtpusername" size="30" value=""></td>
  </tr>
  <tr>
  <td><label>SMTP Password : <font style="color:red;">*</font></label></td>
  <td><input type="password" id = "smtppassword"  name="smtppassword" size="30" value="" style="width: 284px"></td>
  </tr>
   <tr>
  <td><label>API Key : <font style="color:red;">*</font></label></td>
  <td><input type="text" id = "apikey"  name="apikey" size="30" value="" ></td>
  </tr>
   <tr>
   <td><label>Partner Name<font style="color:red;">*</font>:<br></label></td>
   <td><select name="partner" id="partner"  style="width: 288px;">
    <option>--Select Partner --</option>
    </select>
    </td>
    </tr>
<tr>
<td></td>
<td  align="center">
<input type="submit" name="submit" value="Validate and Save" onclick=" return validate();" id="submit">&nbsp;
 <input type="button"  id="buttonclass"  value="Cancel" onclick="cancel();"></td>
</tr>
</tbody>
</table>
</form>
</div>
  </div>
</div>

<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>