<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List Management</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="uitrans/jquery-ui.css" />

<style type="text/css">

#search-button {
  display: inline-block;
  width: 34px;
  height: 32px;
  position: relative;
  left: -41px;
  top: 9px;
  background: url(images/search.jpg) no-repeat right;
}

	.ui-autocomplete { 
			position: absolute; 
			cursor: default; 
			height: 150px; 
			width:200px;
			overflow-y: scroll; 
			overflow-x: hidden;
			text-align:left;
		}

	
	
</style>

<script>
var pagenum = 1;
$(document).ready(function() {

renderlistpage();
//getAvailableLists();
});

function renderlistpage()
{
	$("#listrows").empty();
	$("#listrowsPage").empty();
	$.ajax({
		type: "get",
		url: "displylists.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}

function rendernextlistpage()
{
	$("#listrows").empty();
	$("#listrowsPage").empty();
	pagenum =pagenum+1;
	$.ajax({
		type: "get",
		url: "displylists.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
		
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}

function renderprevlistpage()
{
	$("#listrows").empty();
	$("#listrowsPage").empty();
	pagenum =pagenum-1;
	$.ajax({
		type: "get",
		url: "displylists.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}
function renderlisttable(objData, pagenumber, totalpages)
{
	
	pagenum = pagenumber;
	if(objData != null && objData.length > 0)
	{
		$("#listrows").append('<tr><th width="100">List Name</th><th width="100">Description</th><th width="100">Status</th><th width="100">Total Records</th><th width="120">Created On</th><th width="120">Last Updated On</th><th width="120">Actions</th></tr>');
		var cont =1;
		for(var count =0; count < objData.length; count++)
		{
			var row = objData[count];
			renderRowInListTable(row, cont);
			cont++;
				
		}
		if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$("#listrowsPage").append('<tr style="background-color: #F6F6F6;"><td align="right" width="50%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td align="right" width="50%"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()">&nbsp;&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td></tr>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$("#listrowsPage").append('<tr style="background-color: #F6F6F6;"><td align="right" width="50%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td align="right" width="50%"><input type="submit"    id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td></tr>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$("#listrowsPage").append('<tr style="background-color: #F6F6F6;"><td align="right" width="50%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td align="right" width="50%"><input type="submit"   id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td></tr>');
			
	}
		
		
	}
	else
	{
		$("#listrows").append('<tr><td style="color:red">No list found.</td></tr>');
	}
	
}
function submitExportTo()
{
	listname ="${listname}";
	var url = "listfieldsexport.htm?listname="+listname;
	document.exportto.action =  url;
	document.exportto.submit();
	$("#exportto").dialog('close');
				
}
function renderRowInListTable(row, cont)
{
	var listId=row.listId;
	var listname = row.listName;
	var description = row.listDescription;
	
	if(description != null && description.length> 15)
	{
		description = description.substring(0,18);
		description =description+" ...";
	}
	var status=row.status;
	var	records =row.totalRecords;
	var createdon = row.createdDate;
	var updateOn = row.updatedDate;
	
	if(description == null || description == "undefined")
		{
		description="";
		}
	if(updateOn == null || updateOn == "undefined")
		{
		updateOn="";
		}
	if(records == null || records == "undefined")
		{
		records ="";
		}
	if(createdon == null || createdon == "undefined")
	{
		createdon ="";
	}
		if (status == 'Uploading') {
		  $("#listrows").append('<tr id="'+listname+'"><td width="100" >'+listname+'</td><td width="100" >'+description+'</td><td width="100" >'+status+'</td><td width="100" >'+records+'</td></td><td width="100" >'+createdon+'</td><td width="100" >'+updateOn+'</td><td width="120" align="center">&nbsp;&nbsp;<a href="downloadlist.htm?listname='+listname+'"><img src="images/download1.jpg" class="tooltip" title="download" /></a>&nbsp;&nbsp;<a href="#"><img src="images/import.jpg" class="tooltip" title="upload" onclick="errorMessage();"/></a>&nbsp;&nbsp;<a href="subsribers.htm?listName='+listname+'"><img src="images/subscribers.png" class="tooltip" title="subsribers edit" /></a>&nbsp;&nbsp;<a href="showlistreport.htm?listname='+listname+'&list_id='+listId+'"><img src="images/report.png" title="report"  alt="report" /></a>&nbsp;&nbsp;<a><img src="images/delete.png" class="tooltip" title="delete" onclick="errorMessage();"/></a></td></tr>');
		 }
		 else{
		  $("#listrows").append('<tr id="'+listname+'"><td width="100" >'+listname+'</td><td width="100" >'+description+'</td><td width="100" >'+status+'</td><td width="100" >'+records+'</td></td><td width="100" >'+createdon+'</td><td width="100" >'+updateOn+'</td><td width="120" align="center">&nbsp;&nbsp;<a href="downloadlist.htm?listname='+listname+'"><img src="images/download1.jpg" class="tooltip" title="download" /></a>&nbsp;&nbsp;<a href="uploadrecords.htm?listname='+listname+'"><img src="images/import.jpg" class="tooltip" title="upload" /></a>&nbsp;&nbsp;<a href="subsribers.htm?listName='+listname+'"><img src="images/subscribers.png" class="tooltip" title="subsribers edit" /></a>&nbsp;&nbsp;<a href="showlistreport.htm?listname='+listname+'&listId='+listId+'"><img src="images/report.png" title="report"  alt="report" /></a>&nbsp;&nbsp;<a><img src="images/delete.png" class="tooltip" title="delete" onclick="deleteRow(\''+listId+'\');" /></a></td></tr>');

		 }
}

function errorMessage()
{
 alert("List Uploading in Progress!");
}
function deleteSelectedRows(ids)
{
	//alert("delete list "+ids.toString());
	$.ajax({
		type: "get",
		url: "deletelist.htm?listrecs="+ids,
		data: ids,
		dataType: "json",
		success: function( objData )
		{
			window.location = "listManagement.jsp";	
		},
		error: function(data){
			
			alert( "An error occurred" +data[0] );
			//window.location = "listManagement.jsp";
		}
	});
	
}
function deleteRow(id)
{
	if(confirm("are you sure you want to delete this list" ))
	{
	$.ajax({
		type: "get",
		url: "deletelist.htm?listrecs="+id.toString(),
		data: id,
		dataType: "json",
		success: function( objData )
		{
			window.location = "listManagement.jsp";	
		},
		error: function(data){
			
			alert("can not delete list as it is used in campaign ");
			window.location = "listManagement.jsp";
		}
	});
	}
	return false;
	
}


function refresh()
{
	window.location="listManagement.jsp";
}
</script>
<script type="text/javascript">
function uploadnewList()
{
	window.location="createlist.jsp";
}

function searchList()
{
	var name = document.getElementById("searchlist");
	var listname = name.value.trim();
	
	if(listname == "")
	{
		$("#listrows").empty();
		$("#listrowsPage").empty();
		renderlistpage();
		return;
	}
	
	
		$("#listrows").empty();
		$("#listrowsPage").empty();
		$.ajax({
			type: "get",
			url: "searchlist.htm?listname="+listname+"&pagenumber="+pagenum,
			dataType: "json",
			success: function( objData )
			{
				
				renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
			},
			error: function(data){
				
				window.location = "listManagement.jsp";
			}
		});
		
	}

 function renderSearchListData(row)
{
	 $("#listrows").empty();
	var listId=row.listId;
	var listname = row.listName;
	var description = row.listDescription;
	
	if(description != null && description.length> 15)
	{
		description = description.substring(0,18);
		description =description+" ...";
	}
	var status=row.status;
	var	records =row.records;
	var updateOn = row.updatedDate;
	$("#listrows").append('<tr><th width="100">List Name</th><th width="100">Description</th><th width="100">Status</th><th width="100">Total Records</th><th width="120">Created On</th><th width="120">Last Updated On</th><th width="120">Actions</th></tr>');
	 $("#listrows").append('<tr id="'+listname+'"><td width="100" >'+listname+'</td><td width="100" >'+description+'</td><td width="100" >'+status+'</td><td width="100" >'+records+'</td></td><td width="100" >'+""+'</td><td width="100" >'+updateOn+'</td><td width="120" align="center">&nbsp;&nbsp;<a href="downloadlist.htm?listname='+listname+'"><img src="images/download1.jpg" class="tooltip" title="download" /></a>&nbsp;&nbsp;<a href="uploadrecords.htm?listname='+listname+'"><img src="images/edit.png" class="tooltip" title="edit" /></a>&nbsp;&nbsp;<a href="showListReport.htm?listname='+listname+'"><img src="images/report.png" title="report"  alt="report" /></a>&nbsp;&nbsp;<a><img src="images/refresh.jpg" class="tooltip" title="refresh" onclick="refresh()" /></a>&nbsp;&nbsp;<a><img src="images/delete.png" class="tooltip" title="delete" onclick="deleteRow(\''+listId+'\');" /></a></td></tr>');
	} 

function validateForm(name)
{
	
	if(isalphaNum(name, "Please enter valid name (A-Z, a-z, 0-9)"))
	{
		return true;
	}
	return false;
	
}

</script>
</head>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
		<div id="loading-popup-box" style="display: none;"></div>
		<div id="content-main">
<!-- 			  <div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>
 -->			<div class="contents">
			<div class="create_form">
			<input type="text" id="searchlist" name="searchlist" placeholder="Search List" size="20" ><span id="search-button" onclick="searchList();"></span>
		    <a href="createlist.jsp" id="buttonclass">Create New List</a>
		   <a href="#" id="buttonclass" onclick="refresh()"> Refresh</a>
			</div>
			<table id="listrows" class="tblewidth">		
				</table>
				<table id="listrowsPage" class="tblewidth"></table>
			</div>
		</div>
	</div>

	<div id="footer">
		<div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div>
	</div>
</body>
</html>