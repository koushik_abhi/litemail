<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Campaign Status</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="./css/smoothness/jquery-ui-smoothness.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="./js/jquery-1.9.1.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/jquery-ui.js" charset="utf-8"></script>

<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="js/jquery-ui-timepicker-addon.css" />

<style type="text/css">

td, th {
    padding: 2px;
}

input{font-size:15px; font-weight:normal;}
td a{
text-decoration:underline ;
color:#000;
}

</style>
<script>
var pagenum = 1;
var campaignId="";
$(document).ready(function() {
	campaignId ="${campaignId}";
	rendercampaignstatusinfo();
	//total opens
	/* $("#totalopensView").click(function(){
		getMailOpendStatus();
		$("#campaignstatus").dialog({
	         position: 'center',
	         width: 800,
	         height:500,
	         modal: true
	 });
		$(".ui-dialog-titlebar").hide();
	}); */
	
	});
	
/* function getMailOpendStatus()
{
    campaignId ="${campaignId}";
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	$.ajax({
		type: "get",
		url: "getTotalOpensMailList.htm?campaignId="+campaignId+"&pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
		renderTotalOpensMailsList(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}
function renderTotalOpensMailsList(objData,pagenumber, totalpages)
{
	pagenum = pagenumber;
	$(".campaignstatus_all").append('<tr></tr><tr style="background-color:#fff;"><td><strong>Total Open List</strong></td></tr><tr style="background-color:#fff;"><td style="padding-bottom:10px;"></td></tr>');
	$(".campaignstatus_all").append('<tr><td>From:<input type="text" name="fromdate" id="fromdate" class="datepicker" onclick="populateDate()"/></td><td>To:<input type="text" name="todate" id="todate" class="datepicker" onclick="populateDate()"/></td><td style="background-color:#fff;" align="center"><input type="button" id="buttonclass" value="Search" onclick="searchFilter();"></td><td style="background-color:#fff;padding-left:-40px;"><input type="button" id="buttonclass" value="Export" onclick="Export();"></td></tr><tr style="background-color:#fff;"><td style="padding-bottom:10px;"></td></tr>');
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:200px;">OpenTime</th><th  style="border-right:1px solid #fff;width:250px;">Host</th></tr>');
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderTotalOpensDataTable(row, cont);
		cont++;
			
	}	
	
	$(".campaignstatus_all_page").append('<tr align:right>');
	
	
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"  style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"><input type="submit"   style="float: right; margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
		
	}else if(totalpages > 1 && pagenumber == 1)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"   style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td>');
		
	}else if (totalpages == pagenumber  && totalpages > 1)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
		
	}
	$(".campaignstatus_all_page").append('</tr>');
}
function renderTotalOpensDataTable(row,cont)
{
	var email = row.fromAddress;
	var name = row.fromName;
	var host = row.clientName;
		$(".campaignstatus_all").append('<tr><td width="100" >'+email+' </td><td width="100" >'+name+' </td><td width="100" >'+""+' </td><td width="100" >'+host+' </td></tr>');
} */
function renderTotalClicksData(objData,url,headingname,url2,pagenumber, totalpages)
{
	pagenum = pagenumber;
	$(".campaignstatus_all").append('<tr></tr><tr style="background-color:#fff;"><td><strong>'+headingname+'</strong></td></tr><tr style="background-color:#fff;"><td style="padding-bottom:10px;"></td></tr>');
	$(".campaignstatus_all").append('<tr></tr><tr><td>From:<input type="text" name="fromdate" id="fromdate" class="datepicker" onclick="populateDate()"/></td><td>To:<input type="text" name="todate" id="todate" class="datepicker" onclick="populateDate()"/></td><td style="background-color:#fff;" align="center"><input type="button" id="buttonclass" value="Search" onclick="searchFilterClicks(\''+url+'\',\''+headingname+'\',\''+url2+'\')"></td><td style="background-color:#fff;padding-left:-40px;"><input type="button" id="buttonclass" value="Export" onclick="get_Report_Download(\''+url2+'\')"></td></tr><tr style="background-color:#fff;"><td style="padding-bottom:10px;"></td></tr>');

	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Link Url</th><th  style="border-right:1px solid #fff;width:200px;">OpenTime</th><th  style="border-right:1px solid #fff;width:250px;">Host</th></tr>');
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderTotalClicksDataTable(row, cont);
		cont++;
			
	}	
	$(".campaignstatus_all_page").append('<tr align:right>');
	
	
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"  style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"><input type="submit"   style="float: right; margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
		
	}else if(totalpages > 1 && pagenumber == 1)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"   style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td>');
		
	}else if (totalpages == pagenumber  && totalpages > 1)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
		
	}
	$(".campaignstatus_all_page").append('</tr>');
}
function renderTotalClicksDataTable(row,cont)
{
	var email = row.fromAddress;
	var name = row.fromName;
	var host = row.clientName;
		$(".campaignstatus_all").append('<tr><td width="100" >'+email+' </td><td width="100" >'+name+' </td><td width="100" >'+""+' </td><td width="100" >'+host+' </td></tr>');
		
}

function renderUniqueOpensMailListData(objData,url,headingname,url2,pagenumber, totalpages){
	pagenum = pagenumber;
	$(".campaignstatus_all").append('<tr style="background-color:#fff;"><td><strong>'+headingname+'</strong></td></tr><tr style="background-color:#fff;"><td style="padding-bottom:10px;"></td></tr>');
	$(".campaignstatus_all").append('<tr></tr><tr><td>From:<input type="text" name="fromdate" id="fromdate" class="datepicker" onclick="populateDate()"/></td><td>To:<input type="text" name="todate" id="todate" class="datepicker" onclick="populateDate()"/></td><td style="background-color:#fff;" align="center"><input type="button" id="buttonclass" value="Search" onclick="searchFilter(\''+url+'\',\''+headingname+'\',\''+url2+'\')"></td><td style="background-color:#fff;padding-left:-40px;"><input type="button" id="buttonclass" value="Export" onclick="get_Report_Download(\''+url2+'\')"></td></tr><tr style="background-color:#fff;"><td style="padding-bottom:10px;"></td></tr>');

	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderUniqueOpensMailListTable(row, cont);
		cont++;
			
	}
	$(".campaignstatus_all_page").append('<tr align:right>');
	
	
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"  style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"><input type="submit"   style="float: right; margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
		
	}else if(totalpages > 1 && pagenumber == 1)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"   style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td>');
		
	}else if (totalpages == pagenumber  && totalpages > 1)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
		
	}
	$(".campaignstatus_all_page").append('</tr>');
}
function renderUniqueOpensMailListTable(row,cont)
{
	var email = row.fromAddress;

		$(".campaignstatus_all").append('<tr><td>'+email+' </td>');
		
}

function renderBouncedMailListData(objData,url,headingname,url2,pagenumber, totalpages)
{
	pagenum = pagenumber;
	$(".campaignstatus_all").append('<tr></tr><tr style="background-color:#fff;"><td><strong>'+headingname+'</strong></td></tr><tr style="background-color:#fff;"><td style="padding-bottom:10px;"></td></tr>');
	$(".campaignstatus_all").append('<tr></tr><tr><td>From:<input type="text" name="fromdate" id="fromdate" class="datepicker" onclick="populateDate()"/></td><td>To:<input type="text" name="todate" id="todate" class="datepicker" onclick="populateDate()"/></td><td style="background-color:#fff;" align="center"><input type="button" id="buttonclass" value="Search" onclick="searchFilterBounces(\''+url+'\',\''+headingname+'\',\''+url2+'\')"></td><td style="background-color:#fff;padding-left:-40px;"><input type="button" id="buttonclass" value="Export" onclick="get_Report_Download(\''+url2+'\')"></td></tr><tr style="background-color:#fff;"><td style="padding-bottom:10px;"></td></tr>');
	$(".campaignstatus_all").append('<tr colspan="4"><th style="border-right:1px solid #fff;width:250px;">Email<th  style="border-right:1px solid #fff;width:200px;">Name</th><th  style="border-right:1px solid #fff;width:200px;">OpenTime</th><th  style="border-right:1px solid #fff;width:250px;">Host</th></tr>');
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderBouncedMailListDataTable(row, cont);
		cont++;
			
	}	
	
	$(".campaignstatus_all_page").append('<tr align:right>');
	
	
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"  style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"><input type="submit"   style="float: right; margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
		
	}else if(totalpages > 1 && pagenumber == 1)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"   style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td>');
		
	}else if (totalpages == pagenumber  && totalpages > 1)
	{
		$(".campaignstatus_all_page").append('<td align:right><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
		
	}
	$(".campaignstatus_all_page").append('</tr>');
}
function renderBouncedMailListDataTable(row,cont)
{
	var email = row.fromAddress;
	var name = row.fromName;
	var host = row.clientName;
		$(".campaignstatus_all").append('<tr><td width="100" >'+email+' </td><td width="100" >'+name+' </td><td width="100" >'+""+' </td><td width="100" >'+host+' </td></tr>');
}
function rendercampaignstatusinfo()
{  
	campaignId ="${campaignId}";
	$("#statusrows").empty();
	$("#statusvalues").empty();
	$.ajax({
		type: "get",
		url: "displaycampaignreport.htm?campaignId="+campaignId,
		dataType: "json",
		success: function( objData )
		{
			rendermailsentlist(objData.report);
			/* renderbounclist(objData.bouncelists);
			rendermailsentlist(objData.mailsentlist);
			rendermailopenlist(objData.mailopendata); */
			
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}
function renderlisttable(objData){
	var clikDto = objData.clickDTO;
	 rendermailsentlist(objData);
		$("#statusrows").append('<tr><th width="80"><strong>URL Name</strong></th><th width="140"><strong>URL</strong></th><th width="100"><strong>Total No of Clicks</strong></th><th width="100"><strong>Clicked Emails</strong></th></tr>');
		var cont =1;
		for(var count =0; count < clikDto.length; count++)
		{
			var row = clikDto[count];
			renderRowInListTable(row, cont);
			cont++;
				
		}
	/* 	$("#showcaseclicks").append(objData.noOfshowcaseClicks);  */
}		

function renderRowInListTable(row, cont)
{
	var campaignId="";
	var urlname = row.urlName;
	var url = row.url;
	var	totalclicks =row.noOfClicks;
	var clickedemails = row.noOfemails;
		$("#statusrows").append('<tr><td width="70" >'+urlname+' </td><td  width="50">'+url+'</td><td  align="center" >'+totalclicks+'</td><td  align="center" >'+clickedemails+'&nbsp;&nbsp;<a href="#" id="clickedEmailsView" onclick="clickedEmailsView('+campaignId+')">View</a>&nbsp;&nbsp;<a href="#" id="clickedEmailsDownload" onclick="clickedEmailsDownload('+campaignId+')">Download</a></td></tr>');
		
}


function rendermailsentlist(maildata)
{
	$("#startdatetime").append(maildata.campaignStartDateTime);
	$("#enddatetime").append(maildata.campaignEndDateTime);
	$("#emailtosend").append(maildata.noOfMails);
	$("#emailsent").append(maildata.mailsSent);
	$("#emailsfailed").append(maildata.mailsFailed);
	$("#campstatus").append(maildata.status);
	
	$("#totalopens").append(maildata.totalOpens);
	$("#totalclicks").append(maildata.totalClicks);
	$("#uniqueopens").append(maildata.uniqueOpens);
	$("#uniqueclicks").append(maildata.uniqueClicks);
	
	$("#showcaseclicks").append(maildata.noOfshowcaseClicks);
	$("#hardbounce").append(maildata.hardBounceCount);
	$("#softbounce").append(maildata.softBounceCount);
	$("#generalbounce").append(maildata.generalBounceCount);
	$("#transientbounce").append(maildata.transientBounceCount);
	$("#mailblock").append(maildata.mailBlockCount);
	$("#autoreply").append(maildata.replyCount);
	$("#others").append(maildata.otherCount);
	$("#unsubscribe").append(maildata.unSubscribeCount);
	$("#spamcomplaint").append(maildata.spamCount);
	$("#totalmailbounced").append(maildata.totalBounceCount);
}	


function refresh()
{
	 campaignname = "${campaignname}";
    window.location.href ="campaignReports.htm?campaignname="+campaignname+"&workflow=tbl";
	
}
function closepopup()
{
	$("#campaignstatus").dialog('close');
}
function get_Report_Data(url,headingname,url2,fromdate,todate)
{
	var campaignId ="${campaignId}";
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	$.ajax({
		type: "get",
		url: url+"?campaignId="+campaignId+"&fromdate="+fromdate+"&todate="+todate,
		dataType: "json",
		success: function( objData )
		{
			 
				 renderUniqueOpensMailListData(objData.result,url,headingname,url2,objData.pagenumber, objData.totalpages);
				 $("#campaignstatus").dialog({
			         position: 'center',
			         width: 800,
			         height:500,
			         modal: true
			 });
				$(".ui-dialog-titlebar").hide();
			
				
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	return false;
}
function get_Report_ClicksData(url,headingname,url2,fromdate,todate)
{
	 var campaignId ="${campaignId}";
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	$.ajax({
		type: "get",
		url: url+"?campaignId="+campaignId+"&fromdate="+fromdate+"&todate="+todate,
		dataType: "json",
		success: function( objData )
		{
			renderTotalClicksData(objData.result,url,headingname,url2,objData.pagenumber, objData.totalpages);
			 $("#campaignstatus").dialog({
		         position: 'center',
		         width: 800,
		         height:500,
		         modal: true
		 });
			$(".ui-dialog-titlebar").hide();
				
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	return false;
}
function get_Report_BounceData(url,headingname,url2,fromdate,todate)
{
	 var campaignId ="${campaignId}";
	$(".campaignstatus_all").empty();
	$(".campaignstatus_all_page").empty();
	$.ajax({
		type: "get",
		url: url+"?campaignId="+campaignId+"&fromdate="+fromdate+"&todate="+todate,
		dataType: "json",
		success: function( objData )
		{
				 renderBouncedMailListData(objData.result,url,headingname,url2,objData.pagenumber, objData.totalpages);
				 $("#campaignstatus").dialog({
			         position: 'center',
			         width: 800,
			         height:500,
			         modal: true
			 });
				$(".ui-dialog-titlebar").hide();
				
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	return false;
}
function clickedEmailsView(campaignId)
{
	//click through status
		var url="getclickedEmails.htm?campaignId="+campaignId;
		var headingname="Click Through Email List ";
		get_Report_Data(url,headingname);
		$("#campaignstatus").dialog({
	         position: 'center',
	         width: 800,
	         height:500,
	         modal: true
	 });
		$(".ui-dialog-titlebar").hide();
}
function clickedEmailsDownload(campaignId)
{
	//click through status
		var url="getclickedEmailDownload.htm?campaignId="+campaignId;
		get_Report_Download(url);
}
function get_Report_Download(url)
{
	 var campaignId ="${campaignId}";
	$.ajax({
		type: "get",
		url: url+"?campaignId="+campaignId,
		dataType: "json",
		success: function( objData )
		{
	     /* window.location("campaignreport.jsp"); */
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	return false;
}
function cancel()
{
	window.location = "campaignManagement.jsp";
	}

	function populateDate()
	{
		$('.datepicker').each(function(){
		    $(this).datetimepicker({
		    	controlType: 'select',
		    	dateFormat:'dd-mm-yy',
		    	timeFormat: 'HH:mm'
		    });
		    $(".ui-datepicker").css("font-size", "11.5px");
		   
		});	
	}


function searchFilter(url,headingname,url2)
{
	
      var fromdate = document.getElementById("fromdate").value;
        var todate = document.getElementById("todate").value;
        
        if(fromdate == null || fromdate =="" || todate == null || todate ==""  )
        	{
        	  alert("please select fromdate and todate ");
              return false;
        	}
        if(fromdate != "" && (todate == null || todate ==""))
        {
                alert("please select fromdate and todate ");
                return false;
        }
        else if(todate != "" && (fromdate == null || fromdate ==""))
        {
                alert("please select fromdate and todate ");
                return false;
        }else if(todate != "" && fromdate != "")
        {
                if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker.parseDate('dd-mm-yy', todate)) {
      
                   alert('please select valid date range, from date  :' + fromdate + 'is later than todate :' + todate);
                   return false;
      
               }
        }
        
       
        get_Report_Data(url,headingname,url2,fromdate, todate); 
        
}
function searchFilterClicks(url,headingname,url2)
{
	
      var fromdate = document.getElementById("fromdate").value;
        var todate = document.getElementById("todate").value;
        
        if(fromdate == null || fromdate =="" || todate == null || todate ==""  )
        	{
        	  alert("please select fromdate and todate ");
              return false;
        	}
        if(fromdate != "" && (todate == null || todate ==""))
        {
                alert("please select fromdate and todate ");
                return false;
        }
        else if(todate != "" && (fromdate == null || fromdate ==""))
        {
                alert("please select fromdate and todate ");
                return false;
        }else if(todate != "" && fromdate != "")
        {
                if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker.parseDate('dd-mm-yy', todate)) {
      
                   alert('please select valid date range, from date  :' + fromdate + 'is later than todate :' + todate);
                   return false;
      
               }
        }
        
       
        get_Report_ClicksData(url,headingname,url2,fromdate, todate); 
        
}
function searchFilterBounces(url,headingname,url2)
{
	
      var fromdate = document.getElementById("fromdate").value;
        var todate = document.getElementById("todate").value;
        
        if(fromdate == null || fromdate =="" || todate == null || todate ==""  )
        	{
        	  alert("please select fromdate and todate ");
              return false;
        	}
        if(fromdate != "" && (todate == null || todate ==""))
        {
                alert("please select fromdate and todate ");
                return false;
        }
        else if(todate != "" && (fromdate == null || fromdate ==""))
        {
                alert("please select fromdate and todate ");
                return false;
        }else if(todate != "" && fromdate != "")
        {
                if ($.datepicker.parseDate('dd-mm-yy', fromdate) > $.datepicker.parseDate('dd-mm-yy', todate)) {
      
                   alert('please select valid date range, from date  :' + fromdate + 'is later than todate :' + todate);
                   return false;
      
               }
        }
        
       
        get_Report_BounceData(url,headingname,url2,fromdate, todate); 
        
}

</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    <li><a href="showemailconfig.htm"> Email Configuration</a></li>
    </ul>
</div>
 <a class="right-nav" href="showsmtpManagement.htm"> SMTP Configuration</a>
</div>
</div>
</div>
	<div id="content"  >
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
<!--        <div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports-red.jpg" width="101" height="127"></span></span></div>   -->       
 <div class="contents">
      <div align="center" style="color:red">${errorMessage}</div><br>
        <div style="float:right;margin-top:-15px;"><input type="button" name="submit" id="buttonclass" value="Refresh details" onclick="refresh();" id="submit" >&nbsp;&nbsp;&nbsp;&nbsp;<input type=button id="buttonclass" value="Cancel" onclick="cancel();"></div>
        <div style="margin-top:20px;border:solid 1px #eee;height:40px;">
        <table width="80%">
        <tr>
        <td width="40%" style="padding-bottom: 10px;padding-top:10px;padding-left:30px;"><label  id="campaignname" >Campaign Name :&nbsp; &nbsp; ${campaignname}</label></td>
        <td width="50%" style="padding-bottom: 10px;padding-top:10px;padding-left:30px;"><label   id="campstatus">Campaign Status :&nbsp; &nbsp;  </label></td>
        </tr></table>
        </div>
        <table width="80%">		
        		<tr>
        			<td width="40%" style="padding-bottom: 10px;">
        						
        			 <div style="border:solid 0px #eee;margin-left:30px;margin-top:10px;">
        				<table>
        						
        						<tr><td><label ><strong>Mail Sent Data</strong></label></td></tr>
        						 <tr>
    								<td><label >Start Date/Time :</label></td>
   								    <td><label id="startdatetime" style="color:#000;"></label></td>
  								</tr>
 							    <tr>
   									 <td><label >End Date/ Time :</label></td>
  									  <td><label id="enddatetime" style="color:#000;"></label></td>
  								</tr>
 								 <tr>
  									 <td><label >No of emails to send :</label></td>
  									 <td><label id="emailtosend" style="color:#000;"></label></td>
							     </tr>
  								 <tr>
  									 <td><label >No of emails sent so far :</label></td>
  									 <td>
  									 <label id="emailsent" style="color:#000;"></label>
  									 <span style="margin-left:25px;">
  									  <a href="#" id="mailsentstatus" onclick="get_Report_Data('getMailSentData.htm','Email Sent List','getmailSentDownload.htm')"> View</a></span>
  									 </td>
 								 </tr>
  								 <tr>
 							 		 <td><label >No of emails failed :</label></td>
   									 <td >
   									 <label id="emailsfailed" style="color:#000;"></label>
   									 <span style="margin-left:25px;">
  									  <a href="#" id="mailfailstatus" onclick="get_Report_Data('getMailFailData.htm','Email Failed List','')"> View</a></span>
   									 </td>
 								 </tr>
        				</table>
        				</div>
        			</td>
       
        			<td width="50%" style="padding-left:5px;padding-bottom: 10px;">
        			 <div style="border:solid 0px #eee;margin-left:30px;margin-top:10px;">
        				<table >
        				<tr>
   							<td><label ><strong> Mail Open Data</strong></label></td>
  						</tr>
  						<tr></tr>
   						<tr>
   							<td><label >Total Opens :</label></td>
   							<td><label id="totalopens" style="color:#000;"></label>
   							<span style="margin-left:25px;">
   							<a href="#" id="totalopensView" onclick="get_Report_ClicksData('getTotalOpensMailList.htm','Total Opens List','gettotalOpensDownload.htm')">View</a></span>
   							
   							</td>
  						</tr>
   						<tr>
  							 <td><label >Total Clicks :</label></td>
  							 <td><label  id="totalclicks" style="color:#000;"></label>
  							 <span style="margin-left:25px;">
  							 <a href="#" id="totalclicksView" onclick="get_Report_ClicksData('getTotalClicksMailList.htm','Total Clicks List','gettotalClicksDownload.htm')">View</a></span>
  							</td>
  						</tr>
  					    <tr>
   							<td><label >Unique Opens :</label></td>
   							<td><label id="uniqueopens" style="color:#000;"></label>
   							<span style="margin-left:25px;">
   							<a href="#" id="uniqueopensView" onclick="get_Report_Data('getTotalClicksMailList.htm','Unique Opens List','getuniqueOpensDownload.htm')">View</a></span>
   							  							</td>
  						</tr>
     					<tr>
   							<td><label>Unique Clicks :</label></td>
   							<td><label id="uniqueclicks" style="color:#000;"></label>
   							<span style="margin-left:25px;">
   							<a href="#" id="uniqueclicksView" onclick="get_Report_Data('getTotalClicksMailList.htm','Unique Clicks List','getuniqueClicksDownload.htm')">View</a>
   							</span>
   							   							</td>
  						</tr>
  						<tr><td>&nbsp;</td></tr>
        				</table>
        				</div>
        		</td>
        		</tr>
        </table>	
        
        <table style="margin-top:10px;margin-bottom:20px;margin-left:30px;">	
        		<tr >
        			<td ><input name="imageField" type="image" src="./getMailOpendStatus.htm?campaignId=<%= request.getAttribute("campaignId") %>" width="400" height="250" border="0" alt="Pie chart"></td>
        		</tr>
        		<tr><td></td></tr>
        </table>
          <span style="margin-left:35px;"> <label><strong>Click through status </strong></label></span>      
                  <table width="100%"  cellpadding="3" cellspacing="1" border="0" id="statusrows" style="margin-top:10px;margin-left:30px;"> </table>
                   <table style="margin-top:10px;margin-bottom:10px;margin-left:30px;"><tr><td id="showcaseclicks"><label ><strong>Total No Of Showcase Clicks :</strong> </label></td></tr></table>
         <table style="margin-top:20px;margin-bottom:20px;margin-left:30px;" width="100%">   
				<tr>            
                   <td ><input name="imageField" type="image" src="./getClickthroughStatus.htm?campaignId=<%= request.getAttribute("campaignId")%>" width="400" height="250" border="0" alt="Bar graph"></td>
                </tr>  
        </table>
        
        <table width="80%" style="padding-bottom: 10px;">
   				<!-- <tr>
   					<td  width="250" ><label ><strong> Mail Bounced Data</strong></label></td>
  				</tr> -->
   				<tr>
   					<td width="40%" style="padding-bottom: 10px;">
   					 <div style="border:solid 0px #eee;margin-left:30px;">
   						<table>
   						<tr>
   					<td ><label ><strong> Mail Bounced Data</strong></label></td>
  				      </tr>
   								<tr>
   									<td ><label>Hard Bounce :</label></td>
   									<td><label id="hardbounce" style="color:#000;"></label>
   									<span style="margin-left:25px;">
   									<a href="#" id="hardbounceView" onclick="get_Report_BounceData('getHardBounced.htm','Hard Bounced List','gethardBounceDownload.htm')">View</a></span>
   									
   									</td>
  								</tr>
   								<tr>
   									<td><label>Soft Bounce :</label></td>
   									<td><label id="softbounce" style="color:#000;"></label>
   									<span style="margin-left:25px;">
   									<a href="#" id="softbounceView" onclick="get_Report_BounceData('getSoftBounced.htm','Soft Bounced List','getsoftBounceDownload.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
   								<tr>
   									<td><label>General Bounce :</label></td>
   									<td><label id="generalbounce" style="color:#000;"></label>
   									<span style="margin-left:25px;">
   									<a href="#" id="generalbounceView" onclick="get_Report_BounceData('getGeneralBounced.htm','General Bounced List','getgeneralBounceDownload.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
     							<tr>
   									<td><label>Transient Bounce :</label></td>
   									<td><label id="transientbounce" style="color:#000;"></label>
   									<span style="margin-left:25px;">
   									<a href="#" id="transientbounceView" onclick="get_Report_BounceData('getTransientBounced.htm','Transient Bounced List','gettransientBounceDownload.htm')">View</a></span>
   									
   									</td>
  								</tr>
	  							
  								<tr>
   									<td><label>Total Number of Mail Bounced :</label></td>
   									<td><label id="totalmailbounced" style="color:#000;"></label>
   									<span style="margin-left:25px;">
   									<a href="#" id="totalmailbouncedView" onclick="get_Report_BounceData('getTotalMailBounced.htm','Total Mail Bounced List','gettotalmailBounceDownload.htm')">View</a></span>
   									
   									</td>
  								</tr>
  								<tr>
   									<td><label>Mail Block :</label></td>
   									<td><label id="mailblock" style="color:#000;"></label>
   									<span style="margin-left:25px;">
   									<a href="#" id="mailblockView" onclick="get_Report_BounceData('getMailBlockBounced.htm','Mail Blocked List','getmailblockBounceDownload.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>			
  								<tr>
   									<td><label>Spam Complaint :</label></td>
   									<td><label id="spamcomplaint" style="color:#000;"></label>
   									<span style="margin-left:25px;">
   									<a href="#" id="spamcomplaintView" onclick="get_Report_BounceData('getSpamComplaint.htm','Spam Complaint List','getspamComplaintBounceDownload.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
  						</table>
  						</div>
  						</td>
  					<td width="50%" style="padding-left:5px;padding-bottom: 10px;" >
  					 <div style="border:solid 0px #eee;margin-left:30px;">
  						<table>
  						<tr><td>&nbsp;</td></tr>
  						<tr><td></td></tr>
  						<tr>
   									<td><label>Unsubscribe :</label></td>
   									<td><label id="unsubscribe" style="color:#000;"></label>
   									<span style="margin-left:25px;">
   									<a href="#" id="unsubscribeView" onclick="get_Report_BounceData('getUnsubscribeMailBounced.htm','Unsubscribed Bounced List','getunsubscribeBounceDownload.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
  						
    							<tr>
   									<td><label>Auto Reply :</label></td>
   									<td><label id="autoreply" style="color:#000;"></label>
   									<span style="margin-left:25px;">
   									<a href="#" id="autoreplyView" onclick="get_Report_BounceData('getAutoReplyMailBounced.htm','AutoReply Bounced List','getautoReplyBounceDownload.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
    							<tr>
   									<td><label>Others :</label></td>
   									<td><label id="others" style="color:#000;"></label>
   									<span style="margin-left:25px;">
   									<a href="#" id="othersView" onclick="get_Report_BounceData('getOthersBounced.htm','Others Bounced List','getOthersBounceDownload.htm')">View</a>
   									</span>
   									
   									</td>
  								</tr>
  								<tr><td>&nbsp;</td></tr>
  									<tr><td>&nbsp;</td></tr>
  									<tr><td>&nbsp;</td></tr>
  									<tr><td>&nbsp;</td></tr>
  						</table>   
  						</div> 
  					</td>	  
        	</tr>
        	</table>
</div>
</div>
</div>
<div id="campaignstatus" style="display:none;">
<h1 style="font-size:15px;float: right;color:#000;width:30px; cursor: pointer;margin-right: -10px;" title="Close" onclick="closepopup()">&nbsp;&nbsp;X</h1>
<table class="campaignstatus_all" id="tbl" >
</table>
<table class="campaignstatus_all_page" id="tbl"></table>

</div>
<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>