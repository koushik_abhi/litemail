<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Advanced Analytics</title>
<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="./css/smoothness/jquery-ui-smoothness.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="./js/jquery-1.9.1.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/jquery-ui.js" charset="utf-8"></script>

<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="js/jquery-ui-timepicker-addon.css" />
<script type="text/javascript" src="js/highcharts.js"></script>
<script type="text/javascript" src="js/exporting.js"></script>
<script type="text/javascript" src="graphthemes/skies.js"></script>
<style type="text/css">
tr{
    border: 1px solid #eee;
}
.contents {
    background-color: #f6f6f6;
    border-radius: 12px;
    margin: 20px auto;
    padding: 20px;
    width: 1145px;
}
</style>
<script>
var campaignId="";
var period="";
var type="pie";
$(document).ready(function(){
	 campaignId ="${campaignId}"; 
	period  = $("select#period option:selected").val();
	get_periodData();
	get_analyticData(campaignId,period,type);
	$("#act_attrib_bar").click(function(){
		$(this).addClass('selected').siblings().removeClass('selected');
		 type="column";
		 get_analyticData(campaignId,period,type);
	});
	$("#act_attrib_pie").click(function(){
		 $(this).addClass('selected').siblings().removeClass('selected');
		 type="pie";
		 get_analyticData(campaignId,period,type);
	});
	$("#act_attrib_line").click(function(){
		 $(this).addClass('selected').siblings().removeClass('selected');
		 type="line";
		 get_analyticData(campaignId,period,type);
	});
	
	$("select#period").change(function(){
		period=$(this).val();
		campaignId ="${campaignId}";
		type=$("span.selected").attr("type");
		get_analyticData(campaignId,period,type); 
		
	});
	
 });

function get_periodData(){
	 
	 $.ajax({
	   type:"GET",
	   url:"timeperiod.htm?campaignId="+campaignId,
	   dataType: "json",
		success: function( objData )
		{
		$.each(objData.result, function(index, value) {
		     $('select#period').append($('<option value="'+value+'">'+value+' </option>'));
		});
		}
		 
	 });
	 
}

 function get_analyticData(campaignId,period,type){	
	 
	 var periodof=$('select#period option:selected').text();
	 $.ajax({
	   type:"GET",
	   url:"advAnalytics.htm?campaignId="+campaignId+"&period="+period,
	   dataType: "json",
		success: function( objData )
		{
		showGraph(type,objData.results);
		renderPerformanceData(objData.results,periodof); 
		}	 
	 });
	 
 }
 function renderPerformanceData(objData,periodof){
	    $(".distributionDataTime").empty();
		$(".distributionDataTime").append('<th width="20%"><strong>All Metrics</strong></th>');
		  var jsonarray=objData;
	      for(var key in jsonarray) {
	    $(".distributionDataTime").append('<th width="25%"><strong>'+key+'</strong></th>');
	        }
		renderRowPerformanceTable(objData,periodof);
	}		

	function renderRowPerformanceTable(row,periodof)
	{
		$(".distributionData").empty();
		$(".distributionData").append('<td   align="center" >'+periodof+'</td>');
		var totalopens = row.totalopens;
		var totalclicks = row.totalclicks;
		var uniqueopens = row.uniqueopens;
		var uniqueclicks = row.uniqueclicks;
		var hardbounce = row.hardbounce;
		var softbounce = row.softbounce;
		var generalbounce = row.generalbounce;
		var transientbounce = row.transientbounce;
		var totalbounce = row.totalbounce;
		var spam = row.spam;
		var unsubscribe = row.unsubscribe;
		var mailblock = row.mailblock;
	    $(".distributionData").append('<td   align="center" >'+totalopens+'</td><td   align="center" >'+totalclicks+'</td><td   align="center" >'+uniqueopens+'</td><td   align="center" >'+uniqueclicks+'</td><td   align="center" >'+hardbounce+'</td><td   align="center" >'+softbounce+'</td><td   align="center" >'+generalbounce+' </td><td   align="center" >'+transientbounce+' </td><td   align="center" >'+totalbounce+' </td><td   align="center" >'+mailblock+' </td><td   align="center" >'+spam+' </td><td   align="center" >'+unsubscribe+' </td>');
    
	}
 
  function showGraph(type,data){
	  
	  if(type=="column" || type=="line")
		  {
		  showGraphColumnLine(type,data);
		  return false;
		  }
	  else{
	  
	    var jsonarray=data;
		 var chart=new Highcharts.Chart({
			  chart:{
				  renderTo:"chart_div1",
					height:420,
				    width:900,
				    type:type,
				    
			  },
			  title:{
				     text:"LiteMail Report",
			   }, 
			tooltip: {
				pointFormat: '<span style="color:{series.color}">{point.name}</span>: <b>{point.percentage:.1f}%</b><br/>',
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor || '#FFFFFF'),
                shadow: true
            },
		    plotOptions: {
		        pie: {
		            allowPointSelect: true,
		            cursor: 'pointer',
		            dataLabels: {
		                enabled: true,
		                color: '#000000',
		                connectorColor: '#000000',
		                formatter: function() {
	                        return Math.round(this.percentage*100)/100 + ' %'; 
		                },
		 
		   
		            }
		        }
		    }
		 });
      var keys=[];
      var categories=[];
      var series=[];
      var data=[];
     
      for(var key in jsonarray) {
          keys.push(key);
          categories.push(key);
          data.push(jsonarray[key]);
          series.push([key,jsonarray[key]]);
        }
      var seriesPie={
             name:keys,
         	 data:series,
         	 type:type,
         	 size:"70%",
           	 center:["50%","40%"],
         	 showInLegend:true
          };
       chart.addSeries(seriesPie); 
       chart.redraw(); 
	  }
 } 
  function showGraphColumnLine(type,data){
	     var jsonarray=data;
	     var dataSum = 0;
		 var chart=new Highcharts.Chart({
			  chart:{
				  renderTo:"chart_div1",
					height:420,
				    width:900,
				    type:type,
				    
			  },
			  title:{
				     text:"LiteMail Report",
			   }, 
			   tooltip: {
				   formatter: function() {
	                	 var pcnt = (this.y / dataSum) * 100;
	                     return Highcharts.numberFormat(pcnt) + '%';
	                },
	            },
	            xAxis: {
	                type: 'category',
	                labels: {
	                    rotation: -45,
	                    style: {
	                        fontSize: '13px',
	                        fontFamily: 'Verdana, sans-serif'
	                    }
	                }
	            },
	            plotOptions: {
			        series: {
			            allowPointSelect: true,
			            cursor: 'pointer',
			            dataLabels: {
			                enabled: true,
			                color: '#000000',
			                connectorColor: '#000000',
			                formatter: function() {
			                	 var pcnt = (this.y / dataSum) * 100;
			                     return Highcharts.numberFormat(pcnt) + '%';
			                },
			   
			            }
			        }
			    }
		 });
    var keys=[];
    var categories=[];
    var series=[];
    var data=[];
   
    for(var key in jsonarray) {
        keys.push(key);
        categories.push(key);
        data.push(jsonarray[key]);
        series.push([key,jsonarray[key]]);
      }
    
	  metrics = $('select#period option:selected').text();
	  keys=metrics;
	  chart.xAxis[0].setCategories(categories);
	  for (var i=0;i < data.length;i++) {
	    dataSum += data[i];
	     }
	  
    var seriesPie={
           name:keys,
       	 data:series,
       	 type:type,
       	 size:"70%",
         	 center:["50%","40%"],
       	 showInLegend:true
        };
     chart.addSeries(seriesPie); 
     chart.redraw(); 
} 
  function backToAnalytics() {
		var campaignId ="${campaignId}";
		var campaignname = "${campaignname}";
		window.location.href = "campaignreports.htm?campaignname="
				+ campaignname + "&workflow=tbl&campaignId="+campaignId;

	}
</script>
</head>
<body>
<div id="header"><div id="logo"><a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
		<div id="loading-popup-box" style="display: none;"></div>
		<div id="content-main">
			<div class="contents">
				<div align="center" style="color: red">${errorMessage}</div>
				<div style="margin-left:5px;width:1050px;margin-top:5px;margin-bottom: 15px;">
				<ul class="breadcrumb2" style="margin-top:-10px;margin-left:2px;margin-bottom:10px;">
	   			<li><a href="#" class="bredcom" onclick="backToAnalytics()">campaignreport</a></li>
	   			<li><a href="#" class="bredcom">advancedanalytics</a></li>
				</ul>
				<label><b>Time Analysis</b></label>
				</div> 
				<div style="margin-left:5px;width:1050px;margin-top:5px;margin-bottom: 15px;">		
				<select id="period" name="period">
			 	<option value="All">All Days</option>
				</select>
				<div>
				<label><b>Graph Distribution:</b></label>
				</div>
				</div>
				
	<div style="border:1px solid #cccccc;border-radius:2px; margin-left:5px;width:1050px;min-height:400px; margin-top:5px;" align="center" >
    <div class="chartblock" align="left" style="margin-top:5px;margin-left:5px;">
        <div class="charticons" style="float:right;border:1px solid #ccc;border-radius:5px;margin-top:5px;margin-right:10px;">
		<span id="act_attrib_pie" class="selected" type="pie"><img src="images/chart-pie.jpg" title="Pie Chart" alt="Pie Chart"></span>
		<span id="act_attrib_bar" type="column"><img src="images/chart-column-1.jpg" title="Bar Chart" alt="Bar Chart"></span>
		<span id="act_attrib_line" type="line"><img src="images/chart-line-1.jpg" title="Line Chart" alt="Line Chart"></span>
	    </div>
	  </div>
		 <div id="chart_div1" style="width:750px;height:450px;margin-left:-250px">
		 </div>		 
		 </div>
			
		<div id="tableDistribution" style="margin-left:5px;width:1050px;margin-bottom: 20px;">
		<div style="margin-bottom: 20px;margin-top: 20px;">
		<label><b>Table Distribution:</b></label>
		</div>
		<table style="border: 1px solid #eee;" width="100%" >
		  <tr class="distributionDataTime" style="padding-left:5px;"></tr>
		  <tr class="distributionData" style="padding-left:5px;"></tr>
		</table>
		</div>
		</div>
		</div>
	</div>
</body>
</html>