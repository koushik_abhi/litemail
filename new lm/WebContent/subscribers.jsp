<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Subscribers</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){
	getAllSubscribers();
	
});
</script>
<script>
var pagenum = 1;
function getAllSubscribers()
{
	$("#headers").empty();
	$("#headersrowpage").empty();
	var listName ="${listName}";	
	$.ajax({
		type: "get",
		url: "getallsubsribers.htm?listName=" + listName+"&pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			appendDataToForm(objData.values,objData.headers,objData.pagenumber, objData.totalpages) ;
		},
		error: function(){
			alert( "An error occurred" );
		}
	});
	
	}
	
function renderprevlistpage()
{
	$("#headers").empty();
	$("#headersrowpage").empty();
	pagenum =pagenum-1;
	var listName ="${listName}";
	$.ajax({
		type: "get",
		url: "getallsubsribers.htm?listName=" + listName+"&pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			appendDataToForm(objData.values,objData.headers,objData.pagenumber, objData.totalpages);		
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}
function rendernextlistpage()
{
	$("#headers").empty();
	$("#headersrowpage").empty();
	pagenum =pagenum+1;
	var listName ="${listName}";
	$.ajax({
		type: "get",
		url: "getallsubsribers.htm?listName=" + listName+"&pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			appendDataToForm(objData.values,objData.headers,objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}
	

function appendDataToForm(values,headers,pagenumber, totalpages)
{
	
	pagenum = pagenumber;
	if(headers != null && headers.length > 0)
	{
	var cont =1;
	var email=headers[0];
	var firstName=headers[1];
	var age=headers[2];
	var gender=headers[3];
	var city=headers[4];
	
	
	$("#headers").append('<tr><th width="100">'+email+'</th><th width="100">'+firstName+'</th><th width="100">'+age+'</th><th width="100">'+gender+'</th><th width="120">'+city+'</th><th width="120">Actions</th></tr>');
	for(var count =0; count < values.length; count++)
	{
		var row = values[count];
		renderValues(row, cont);
		cont++;
			
	}
	//$("#headersrowpage").append('<tr align:right>');
	
	 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
			{
				$("#headersrowpage").append('<tr style="background-color: #F6F6F6;"><td width="70%" align="right"><label><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label></td><td width="50%" align="right"><input type="submit"  id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()">&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td></tr>');
				
			}else if(totalpages > 1 && pagenumber == 1)
			{
				$("#headersrowpage").append('<tr style="background-color:#F6F6F6;"><td width="90%" align="center"><label><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label></td><td><input type="submit"   style="float: left" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td></tr>');
				
			}else if (totalpages == pagenumber  && totalpages > 1)
			{
				$("#headersrowpage").append('<tr style="background-color: #F6F6F6;"><td width="70%" align="center"><label><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label></td><td align:right><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td></tr>');
				
			}
	
	 
	
	
}
else
{
	$("#headers").append('<tr><td style="color:red">No attributes found.</td></tr>');
}
	
}



function renderValues(row,cont)
{
	
	var email=row[0];
	var firstName=row[1];
	if(firstName == "undefined" || firstName == null)
	{
	firstName="";
	}
	var age=row[2];
	if(age == "undefined" || age == null)
	{
		age="";
	}
	var gender=row[3];
	if(gender == "undefined" || gender == null)
	{
		gender="";
	}
	var city=row[4];
	if(city == "undefined" || city== null)
	{
		city="";
	}
	$("tr#headers").append('<tr><td width="100" >'+email+'</td><td width="100" >'+firstName+'</td><td width="100" >'+age+'</td><td width="100" >'+gender+'</td></td><td width="100" >'+city+'</td><td align="center"><a><img src="images/edit.png" class="tooltip" title="edit" onclick="edit(\''+email+'\')" /></a>&nbsp;&nbsp;<a><img src="images/delete.png" class="tooltip" title="delete" onclick="deleterow(\''+email+'\');"/></a></td></tr>');


}

function deleterow(email)
{
	var listName ="${listName}";
		window.location.href= "deletesubscriber.htm?email=" + email+"&listName="+listName;
		
	}
	
function edit(email)
{
	var listName ="${listName}";

		window.location.href= "showeditsubscriber.htm?email=" + email+"&listName="+listName;
		
	}

function cancel()
{
	
	window.location="dashboard.jsp";
}
</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
	    <div class="contents">
	     <ul class="breadcrumb2" style="margin-top:-10px;margin-left:5px;margin-bottom:10px;">
	   <li><a href="listManagement.jsp" class="bredcom">Lists</a></li>
	   <li><a href="#" class="bredcom"><%=request.getAttribute("listName") %></a></li>
	  <!--  <li><a href="#" class="bredcom" id="charts">Charts</a> -->
	   </ul>
	    
        <div align="center" style="color:red">${errorMessage}</div><br>
	<form name = "userprofileform" action="updateprofile.htm" method="post">

	<table >
		<tr id="headers" class="tblewidth">
		
		</tr>
</table>
<table id="headersrowpage">
</table>
</form>
</div>
  </div>
</div>

<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>