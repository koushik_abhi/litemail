           $(document).ready(function() {
          
               $("#plist_form").validationEngine('attach');
               popUpCal.init();
          
          
              // Catpture click events
                   $("#list_management").click(function(){
                  get_profiles_list();
                  return false;
               });
          
                   
          
          
               $("#l1").click( function(){
                          $("#cust_attr").hide(1000);
                          $("#std_attr").show(1000);
                  });
             
                   $("#l2").click( function(){
                          $("#cust_attr").show(1000);
                          $("#std_attr").hide(1000);
                  });
                   
                $("#widgetField").click( function(){
                          alert('This will be enabled shortly');
                        
                  });
                                       
               $("#fupg_header1").show();
               $("#fupg_header2").show();
                  
                  show_bulb(0);
                   
                  $("#date_format").css({'display': 'none'});
              if ($("#attribute-type").val() == 'DATE') {
                             $("#date_format").css({'display': 'table-row'});
                  }
          
          
                  capture_clicks();
          
                   $("#logo").click( function() {
                           var url = "/GC/Home/get_form";    
                           window.location.href = url;
                   });
                   
               $("#calendar1").click( function() {
                  //alert('rajsht');
                  });
                   
          });
          
          
           function capture_clicks() {
          
                  $("#plist_form").validationEngine('attach');
                  
                  var list_type= $('input[name="list_type_id"]:checked').val();
                  if (list_type == 1)
                  {
                      $("#plist-name").css({'display': 'block'});
                      $("#plist-name").attr( 'disabled', false );
                      $("#alist-name").css({'display': 'none'});
                  }
                  else if (list_type == 2)
                  {
                      $("#alist-name").css({'display': 'block'});
                      $("#alist-name").attr( 'disabled', false );
                      $("#plist-name").css({'display': 'none'});
                  }
                  
                   $("#cb_open").mousedown(function(){ 
				$(".open_div").css("background",'none repeat scroll 0 0 #BD2828');
				return false;
				});
                   
                $("#attributes_graph").change( function () {
                      
                     $attr = $("#graph_attribute-name option:selected").val();
                     $plist_id = $("#plist_id").val();
                     
                     get_graph("/GC/Profile/get_subscriber_graph?plist_id="+ $plist_id + "\&attribute=" + $attr );
                  return false;
                      
                });

				$("#mgraph_name1").change( function () {
                      
                     $attr1 = $("#mgraph_name1 option:selected").val();
                     $attr2 = $("#mgraph_name2 option:selected").val();

					 if ($attr1 == -1 || $attr2 == -1)
					 {
						 alert('Please select both attributes'); 
					 }
					 else
					{
                     $plist_id = $("#plist_id").val();
                     
                     get_combo("/GC/Profile/get_subscriber_combo?plist_id="+ $plist_id + "\&attr1=" + $attr1+ "\&attr2=" + $attr2 );
                  return false;
					}
                      
                });

				$("#mgraph_name2").change( function () {
                      
					 $attr1 = $("#mgraph_name1 option:selected").val();
                     $attr2 = $("#mgraph_name2 option:selected").val();

					 if ($attr1 == -1 || $attr2 == -1)
					 {
						 alert('Please select both attributes'); 
					 }
					 else
					{
                     $plist_id = $("#plist_id").val();
                     
                     get_combo("/GC/Profile/get_subscriber_combo?plist_id="+ $plist_id + "\&attr1=" + $attr1+ "\&attr2=" + $attr2 );
					}
                  return false;
                      
                });

				$("#line_graph_name1").change( function () {
                      
                     $attr1 = $("#line_graph_name1 option:selected").val();
                     $attr2 = $("#line_graph_name2 option:selected").val();

					 if ($attr1 == -1 || $attr2 == -1)
					 {
						 alert('Please select both attributes'); 
					 }
					 else
					{
                     $plist_id = $("#plist_id").val();
                     
                     get_linegraph("/GC/Profile/get_subscriber_combo?plist_id="+ $plist_id + "\&attr1=" + $attr1+ "\&attr2=" + $attr2 );
                  return false;
					}
                      
                });

				$("#line_graph_name2").change( function () {
                      
					 $attr1 = $("#line_graph_name1 option:selected").val();
                     $attr2 = $("#line_graph_name2 option:selected").val();

					 if ($attr1 == -1 || $attr2 == -1)
					 {
						 alert('Please select both attributes'); 
					 }
					 else
					{
                     $plist_id = $("#plist_id").val();
                     
                     get_linegraph("/GC/Profile/get_subscriber_combo?plist_id="+ $plist_id + "\&attr1=" + $attr1+ "\&attr2=" + $attr2 );
					}
                  return false;
                      
                });

                $("#cb_close").mousedown(function(){
                        $(".close_div").css("background",'none repeat scroll 0 0 #BD2828');
                                return false;
                        });
        
                $("#cb_open").mouseleave(function(){
                        $(".open_div").css("background",'');
                        return false;
                        });
        
                $("#cb_close").mouseleave(function(){
                        $(".close_div").css("background",'');
                        return false;
                        });
                
                $(".pl_name").hover(function(){ 
                                  $(".open_div").css("background",'none repeat scroll 0 0 #BD2828');
                                  return false;
                                  },
                                  function(){ 
                                  $(".open_div").css('background-color',''); 
                                  return false;
                                  });
                
                  $(".sspan").hover(function(){ 
                                  $(".close_div").css("background",'none repeat scroll 0 0 #BD2828');
                                  return false;
                                  },
                                  function(){ 
                                  $(".close_div").css('background-color',''); 
                                  return false;
                                  });
                  $(".delete_rule").hover(function(){ 
                                  $(".close_div").css("background",'none repeat scroll 0 0 #BD2828');
                                  return false;
                                  },
                                  function(){ 
                                  $(".close_div").css('background-color',''); 
                                  return false;
                                  });
                  
                  $("#blist-name").css({'display': 'none'});
                  $("#sslist-name").css({'display': 'none'});
                  $("#agf-id").attr("disabled",true);
                  $("#attribute-grpby").attr("disabled",true);
                  
                  $("#operator").change( function () {
                      
                      if ($("#operator option:selected").val() == 'EXISTS' || $("#operator option:selected").val() == 'NOT_EXISTS' )
                      {
                                 disable_chk('attribute-name',1);
                                 disable_chk('attr_value',1);
                                 disable_chk('aggr_chk',1);
                                 disable_chk('attribute-grpby',1);
                                 
                      }
                      else{
                                 disable_chk('attribute-name',2);
                                 disable_chk('attr_value',2);
                                 disable_chk('aggr_chk',2);
                                 disable_chk('attribute-grpby',2);     
                      }
                      
                      });
                  
                  //$(".delete_rule").click( function (){
                  //   alert($(this).attr("id")); 
                  //});
                          
                  $("#aggr_chk").click( function (){
                     var pl_id = get_list_id();
                     if (pl_id == -1)
                     {
                          $("#aggr_chk").attr('checked', false);
                          alert('Please select profile list');      
                     }
                     else if($("#aggr_chk").attr("checked")==true)
                     {
                         
                         $("#attribute-grpby").attr("disabled",false);
                         
                         $("#attribute-name").attr("disabled",false);
                         $("#operator").attr("disabled",false);
                         $("#attr_value").attr("disabled",false);               
                         $("#agf-id").attr("disabled",false);
                     }
                     else
                     {
                         $("#aggr_chk").attr('checked', false);
                         $("#attribute-name").attr("disabled",false);
                         $("#operator").attr("disabled",false);
                         $("#attr_value").attr("disabled",false);
                         $("#attribute-grpby").attr("disabled",true);
                         $("#agf-id").attr("disabled",true);
                     }
                  });
          
                   //implemented for count
                  $("#agf-id").change(function(){
                     if ($("#agf-id option:selected").val() == 'COUNT' )
                     {
                              
                        $("#attribute-name").attr("disabled",true);
                     }
                     else
                     {
                        $("#attribute-name").attr("disabled",false);        
                     }
                  });
                  
               $("#attribute-name").change(function(){
          
                  if ($("#attribute-name option:selected").attr("typevalue") == 'DATE' )
                  { 
                     $("#attr_value").calendar();
                  }
               else
                  {
                          $("#attr_value").remove();
                          var str = '<input type="text" name="attr_value" id="attr_value" required="" title="Please fill out Field Name" min_length="2"/>';
                          $("#attrval").html(str);
                  } 
               });
               $("#create_plist").click(function(){
                          get_data("/GC/Profile/get_create_plist_form?list_type_id="+ parseInt($(this).attr('list_type_id')));
                  return false;
               });
          
               $("#create_folder").click(function(){
                          get_data("/GC/Profile/get_create_folder_form");
                  return false;
               });
          
               $("#create_attribute").click(function(){
                  get_data("/GC/Profile/get_create_attribute_form?plist_id="+ $(this).attr('plist_id') + "&list_type_id=" + parseInt($(this).attr('list_type_id')) );
                          $("#date_format").css({'display': 'none'});
                  if ($("#attribute-type").val() == 'DATE') {
                             $("#date_format").css({'display': 'table-row'});
                      }
          
                  return false;
               });
          
               $('.get_custom_attributes').click(function(){
                  get_data("/GC/Profile/get_custom_attributes?plist_id="+ $(this).attr('value') + "\&list_type_id=" + parseInt($(this).attr('list_type_id')) );
                  return false;
               });
               
               $('.get_subscriber_reports').click(function(){
                  get_reports("/GC/Profile/get_subscriber_reports?plist_id="+ $(this).attr('value') + "\&list_type_id=" + parseInt($(this).attr('list_type_id')) );
                  return false;
               });
          
               $('.update_plist').click(function(){
                  get_data("/GC/Profile/update_plist_form?plist_id="+ $(this).attr('value') + "\&list_type_id=" + parseInt($(this).attr('list_type_id')) );
                  return false;
               });
          
          
               $("#save_plist").click(function(){
                  post_data("/GC/Profile/add_profile_list");
                  return false;
               });
          
               $("#update_plist").click(function(){
                  post_data("/GC/Profile/update_profile_list");
                  return false;
               });
          
          
               $("#save_folder").click(function(){
                  post_data("/GC/Profile/add_folder");
                  return false;
               });
          
          
               $("#cancel_plist").click(function(){
                  get_data($(this).attr('value'));
                  return false;
               });
               
                 $("#cancel_fileupload").click(function(){
                  get_data("/GC/Profile/get_subscriber_data?plist_id="+ $(this).attr('value') + "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                  return false;
               });
               $("#cancel_ftp_fileupload").click(function(){
                  get_data("/GC/Profile/get_subscriber_data?plist_id="+ $(this).attr('value') + "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                  return false;
               });
               $("#cancel_act_fileupload").click(function(){
                   get_data("/GC/Profile/get_activity_data?plist_id="+ $(this).attr('value') + "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                  return false;
               });
               $("#cancel_act_ftp_fileupload").click(function(){
                   get_data("/GC/Profile/get_activity_data?plist_id="+ $(this).attr('value') + "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                  return false;
               });
          
               $("#save_attribute").click(function(){
                          $("#plist_form").validationEngine();
          
                          if ($(".formErrorContent").length > 0) {
                                  return false;
                          }
          
                  post_data("/GC/Profile/add_custom_attribute");
          
                  $("#plist_form").validationEngine('attach');
          
                  return false;
               });
          
               $("#cancel_attribute").click(function(){
                  get_data("/GC/Profile/get_custom_attributes?plist_id="+ $(this).attr('value') + "&list_type_id=" + parseInt($(this).attr('list_type_id'))); 
                  return false;
               });
            
               $(".get_profile_data").click(function(){
                  get_data("/GC/Profile/get_subscriber_data?plist_id="+ $(this).attr('value') + "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                  return false;
               });     
             
               $(".view_sublist_recs").click(function(){
                  get_data("/GC/Profile/get_sublist_rec_data?sublist_id="+ $(this).attr('value') + "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                  return false;
               });
          
               $(".get_activity_data").click(function(){
                  get_data("/GC/Profile/get_activity_data?plist_id="+ $(this).attr('value') + "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                  return false;
               }); 
          
               $(".get_segments").click(function(){
                  get_data("/GC/Profile/get_segments?folder_id="+ $(this).attr('folder_id') );
                  return false;
               }); 
             
	        $("#clear_segment").click(function(){
                  get_data("/GC/Profile/get_segments?folder_id="+ $(this).attr('folder_id') );
                  return false;
               });                
          
                   $(".get_sublists").click(function(){
                  get_data("/GC/Profile/get_sublists?folder_id="+ $(this).attr('folder_id') );
                  return false;
               }); 
          
                   $("#create_segments").click(function(){
                  get_data("/GC/Profile/create_segments?folder_id=" + $(this).attr('folder_id'));
                  return false;
               });
                   
               $("#aggregate-id").click( function() {
          
                     if ($('input[name="aggregate"]:checked').val() == "on" )
                     {
                        toggle_display('plist-name', 'alist-name', 'attribute-name', 'operator', 'attr_value', 1);          
                     }
                     else
                     {
                        toggle_display('plist-name', 'alist-name', 'attribute-name', 'operator', 'attr_value', 2);         
                     }
               });
          
               $("#upload_profiles").click(function(){
                 get_data("/GC/Profile/upload_profiles_form?plist_id="+ $(this).attr('value') + "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                  return false;
               });
          
               $("#save_fileupload").click(function(){
                //  $('#plist_form').attr("action", "/GC/Profile/upload_file");
                   $('#plist_form').submit(); 
                  return false;
               });
          
                   $("#cancel_update_sub").click(function() {
          
                          var page_count = parseInt($(this).attr('page_count'));
                  var page_start = parseInt($(this).attr('page_start')) - page_count;
                          if (page_start < 0)
                          {
                     page_start = 0;
                          }
                          var page_end = parseInt($(this).attr('page_end')) - page_count;
                          var profile_id = parseInt($(this).attr('value'));
                          var plist_id = parseInt($(this).attr('plist_id'));
                          var list_type_id =  parseInt($(this).attr('list_type_id'));
                          var url = $(this).attr('href')
          
                  get_data(url + "?plist_id=" + plist_id  + 
                                  "&page_start=" + page_start + 
                                  "&page_end=" + page_end + 
                                  "&list_type_id=" + list_type_id + 
                                  "&profile_id=" + profile_id );
                  return false;
              
                   });
          
                   $("#page_prev, #page_prev_only").click(function(){
                          var page_count = parseInt($(this).attr('page_count'));
                  var page_start = parseInt($(this).attr('page_start')) - page_count;
                          if (page_start < 0)
                          {
                     page_start = 0;
                          }
                          var page_end = parseInt($(this).attr('page_end')) - page_count;
                          var plist_id = parseInt($(this).attr('value'));
                          var url = $(this).attr('href');
						  var search_val = $("#search-profile").attr('value');
						  if (search_val == null || search_val.length == 0)
							  search_val = $("#act-search-profile").attr('value');                  
  						  if (search_val == null || search_val.length == 0)
							 search_val = '';
          
                  get_data(url + "?plist_id=" + plist_id  + "&page_start=" + page_start + "&page_end=" + page_end + "&list_type_id=" + parseInt($(this).attr('list_type_id')) + "&needed_attribute=" + $("select option:selected").val() + "&sublist_id="+parseInt($(this).attr('sublist_id')) + "&search_val="+search_val);
                  return false;
              
               });
                   $("#page_next, #page_next_only").click(function(){
                          var page_count = parseInt($(this).attr('page_count'));
                  var page_start = parseInt($(this).attr('page_start')) +  page_count;
                          if (page_start < 0)
                          {
                     page_start = 0;
                          }
                          var page_end =  parseInt($(this).attr('page_end')) + page_count;
                          var url = $(this).attr('href');
                          var plist_id = parseInt($(this).attr('value'));
						  var search_val = $("#search-profile").attr('value');
						  if (search_val == null || search_val.length == 0)
							  search_val = $("#act-search-profile").attr('value');                  
   						  if (search_val == null || search_val.length == 0)
							 search_val = '';

                  get_data(url + "?plist_id=" + plist_id  + "&page_start=" + page_start + "&page_end=" + page_end + "&list_type_id=" + parseInt($(this).attr('list_type_id')) + "&needed_attribute=" + $("select option:selected").val() + "&sublist_id="+parseInt($(this).attr('sublist_id')) + "&search_val="+search_val);
                  return false;
              
               });

                $(".bread_crumb_action").click(function(){
                          var page_count = parseInt($(this).attr('page_count'));
                  var page_start = parseInt($(this).attr('page_start'));
                          if (page_start < 0)
                          {
                     page_start = 0;
                          }
                          var page_end =  parseInt($(this).attr('page_end'));
                          var url = $(this).attr('href');
                          var plist_id = parseInt($(this).attr('value'));
          
                  get_data(url + "?plist_id=" + plist_id  + "&page_start=" + page_start + "&page_end=" + page_end + "&list_type_id=" + parseInt($(this).attr('list_type_id')) + "&needed_attribute=" + $("select option:selected").val());
                  return false;
              
               });

             
               $("#export_profiles").click(function(){
                  window.open("/GC/Profile/export_profiles_page?plist_id="+ $(this).attr('value') + "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                  return false;
                   });
          
               $("#export_sublist").click(function(){
                  window.open("/GC/Profile/export_sublist_page?sublist_id="+ $(this).attr('value'));
                  return false;
                   });
          
               $("#export").click(function(){
                  post_data("/GC/Profile/export_subscribers_data");
                  return false;
               });
          
               $("#save_ftp_fileupload").click(function(){
                  post_data("/GC/Profile/ftp_upload");
                  return false;
               });
          
                   $("#search-button").click(function(){
                get_data("/GC/Profile/get_subscriber_data?search_val=" + 
                            $("#search-profile").attr('value') + 
                            "&plist_id=" + parseInt($(this).attr('plist_id')) + 
                            "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                           return false;
                   });
               
                           $("#act-search-button").click(function(){
                get_data("/GC/Profile/get_activity_data?search_val=" + 
                            $("#act-search-profile").attr('value') + 
                            "&plist_id=" + parseInt($(this).attr('plist_id')) + 
                            "&list_type_id=" + parseInt($(this).attr('list_type_id')));
                           return false;
                   });
          
               $("#needed-attribute").change(function(){
                 get_data("/GC/Profile/get_activity_data?search_val=" + 
                            $("#act-search-profile").attr('value') + 
                            "&plist_id=" + parseInt($(this).attr('plist_id')) + 
                            "&list_type_id=" + parseInt($(this).attr('list_type_id')) +
                             "&needed_attribute=" + $("select option:selected").val());
                           return false;
                   });

			   $("#needed-sub-attribute").change(function(){
                 get_data("/GC/Profile/get_subscriber_data?search_val=" + 
                            $("#search-profile").attr('value') + 
                            "&plist_id=" + parseInt($(this).attr('plist_id')) + 
                            "&list_type_id=" + parseInt($(this).attr('list_type_id')) +
                             "&needed_attribute=" + $("select option:selected").val());
                           return false;
                   });

          
               $("#date_format").css({'display': 'none'});
              if ($("#attribute-type").val() == 'DATE') {
                             $("#date_format").css({'display': 'table-row'});
                  }
          
                   $("#attribute-type").change(function(){
                     $("#default-value").val("");
                     $("#plist_form").validationEngine('attach');
                     $("#default-value").removeClass();
                     var fname;
          
                 if($("#attribute-type").val() == 'DATE') {
                             $("#date_format").css({'display': 'table-row'});
                     fname = $("#format-name option:selected").text();
                 $("#default-value").addClass("validate[custom[" + fname + "]] text-input");
                     } else {
                             if ($("#attribute-type").val() == 'NUMERIC')
                             {
                                          //$("#default-value").val(0);
                          fname = "numeric";
                 $("#default-value").addClass("validate[custom[" + fname + "]] text-input");
                             }
                             if ($("#attribute-type").val() == 'DECIMAL')
                             {
                                      fname = "decimal";
                                          //$("#default-value").val("0.0");
                 $("#default-value").addClass("validate[custom[" + fname + "]] text-input");
                             }
                             else {
                                             $("#plist_form").validationEngine('hideAll');
                             }
                             $("#date_format").css({'display': 'none'});
                     }
          
                     return false;
                   });
          
                   $("#format-name").change(function(){
                            $("#plist_form").validationEngine('attach');
                    var fname = $("#format-name option:selected").text();
                    $("#default-value").removeClass().addClass("validate[custom[" + fname + "]] text-input");
                   });
          
                   $("#plist-name").change(function(){ 
                     if ($("#plist-name").attr('onFocus') != 'this.initialSelect = this.selectedIndex;')
                     {
                            get_segment_attributes("/GC/Profile/get_segment_attributes?plist_id=" + $("#plist-name option:selected").val());
                     }
                     return false;
                   });
          
                   $("#alist-name").change(function(){
                     if ($("#alist-name").attr('onFocus') != 'this.initialSelect = this.selectedIndex;')
                     {
                                get_segment_attributes("/GC/Profile/get_segment_attributes?plist_id=" + $("#alist-name option:selected").val());
                     }
                     return false;
                   });
          
          
                  $("#std_attr").hide(1000);
                  $("#upl_widget2").hide(1000);
          
                      $("#l1").click( function(){
                             toggle_fields('#std_attr', '#cust_attr','#l1','#l2');
                  });
          
                  $("#l2").click( function(){
                          toggle_fields('#cust_attr','#std_attr','#l2','#l1');
                  });
          
                      $("#upl_legend1").click( function(){
                          toggle_fields('#upl_widget1','#upl_widget2','#upl_legend1','#upl_legend2');
                  });
          
                  $("#upl_legend2").click( function(){
                          toggle_fields('#upl_widget2','#upl_widget1','#upl_legend2','#upl_legend1');
                  });
                  
                  //$("#aggr_legend1").click( function(){
                  //        toggle_fields('#aggr_funcs','#gen','#aggr_legend2','#aggr_legend1');
                  //});
                  //
                  //$("#aggr_legend2").click( function(){
                  //   if ($("#aggr_ul > li").length <= 0)
                  //   {
                  //        toggle_fields('#gen','#aggr_funcs','#aggr_legend1','#aggr_legend2');
                  //   }
                  //   
                  //});
          
                          $(".fileupg_h").change(function(){
                                  if ($('input[name="header"]:checked').val() == "N")
                                  {
                                          $("#fupg_header1").show();
                                          $("#fupg_header2").show();
                                  }
          
                                  if ($('input[name="header"]:checked').val() == "Y")
                                  {
                                          $("#fupg_header1").hide();
                                          $("#fupg_header2").hide();
                                  }
          
                          });
          
                          $("#delimiter_span").hide();
                          $(".id_delimiter").change(function(){
                                  if ($('input[name="delimiter"]:checked').val() == "others")
                                  {
                                          $("#delimiter_span").show();
                                  }
                                  else
                                  {
                                       $("#delimiter_span").hide();
                                  }
          
                          });
          
                          $(".delete_attribute").click( function(){
                                  var attr_obj = {'plist_id': $(this).attr("plist_id"), 'value': $(this).attr("value"), 'list_type_id' :  $(this).attr("list_type_id") };
                              set_overlay('#edit_form', '#overlay','#boxclose, #boxcancel', attr_obj );
							   set_overlay1('#edit_form', '#overlay','#boxclose, #boxcancel', attr_obj );
                  });
          
                        $(".delete_segment").click( function(){
                                  var attr_obj = {'segment_id': $(this).attr("value"),'folder_id': $(this).attr("folder_id"),'page_start': $(this).attr("page_start"),'page_end': $(this).attr("page_end") };
                              set_overlay('#edit_form', '#overlay','#boxclose, #boxcancel', attr_obj );
                  });
                           $(".delete_folder").click( function(){
                                 alert("feature disabled temporarly");
                                 //if ($(this).attr("count") == 0)
                                 //{
                                 // var attr_obj = {'folder_id': $(this).attr("value") };
                                 // set_overlay('#edit_form', '#overlay','#boxclose, #boxcancel', attr_obj );
                                 //}
                                 //else
                                 //{
                                 //     //alert('Cannot delete folder unless all segments in the folder are deleted');
                                 //     alert("feature disabled temporarly");
                                 //}
                  });
                           
                          $(".delete_profile_list").click( function(){
                                  var attr_obj = {'value': $(this).attr("value"),'list_type_id' :  $(this).attr("list_type_id")};
                              set_overlay('#edit_form', '#overlay','#boxclose, #boxcancel', attr_obj );
                  });
				  $(".delete_profile_list1").click( function(){
                                  var attr_obj = {'value': $(this).attr("value"),'list_type_id' :  $(this).attr("list_type_id")};
                              set_overlay('#edit_form1', '#overlay1','#boxclose1, #boxcancel1', attr_obj );
                  });
                          $(".delete_record").click( function(){
                                  var attr_obj = {'plist_id': $(this).attr("plist_id"), 'value': $(this).attr("value"), 'list_type_id' :  $(this).attr("list_type_id") };
                              set_overlay('#edit_form', '#overlay','#boxclose, #boxcancel', attr_obj );
                  });
                          $(".delete_act_record").click( function(){
                                  var attr_obj = {'plist_id': $(this).attr("plist_id"), 'value': $(this).attr("value"), 'list_type_id' :  $(this).attr("list_type_id") };
                              set_overlay('#edit_form', '#overlay','#boxclose, #boxcancel', attr_obj );
                  });
                          $(".clone_plist").click( function(){
                                  var attr_obj = {'value': $(this).attr("value"),'list_type_id' :  $(this).attr("list_type_id")};
                                  //$("#clone_form legend").html("Clone List of " + $(this).attr("plist_name"));
                              set_overlay2('#clone_form', '#overlay2','#boxclose2, #boxcancel2', attr_obj );
                                  $("#hidden_plist_id").val($(this).attr("value"));
                                  $("#hidden_list_type_id").val($(this).attr("list_type_id"));
                  });
          
          
                          $("#do_action2").click(function() {
                                  if ( $(this).attr("action") == 'clone_plist' ) {
          
                                          check_clone_list('&plist_id=' + $(this).attr("value") +
                                                          '&list_type_id=' + $(this).attr("list_type_id") + 
                                                          '&clone_plist_name='+ $("#clone-name").val() +
                                                      '&description=' + $("#clone-desc").val()); 
                                  };
                          });
                                  
                          $("#do_action").click(function() {
          
                                  if ( $(this).attr("action") == 'create_sublist' )
                                  {
          check_sublist('&plist_id=' + $(this).attr("plist_id") + 
                  '&list_type_id=' + $(this).attr("list_type_id") + 
                  '&sub_folder_id=' + $("#sub-folder-id option:selected").val() +
                  '&sublist_name=' + $("#sublist-name").val() + 
                  '&sublist_desc=' + $("#sublist-desc").val());
                                  }
                                  else {
          
                                          var fun_map = {
                                                  'del_attr' : 
                                          '/GC/Profile/delete_attribute?'+ '&plist_id=' + $(this).attr("plist_id") + '&attribute_id=' + $(this).attr("value") + '&list_type_id=' + $(this).attr("list_type_id"),
                                                  'del_plist' :
                                          '/GC/Profile/delete_plist?'+ '&plist_id=' + $(this).attr("value") + '&list_type_id=' + $(this).attr("list_type_id"),
                              'del_record' :
                         '/GC/Profile/delete_record?'+ '&plist_id=' + $(this).attr("plist_id")  + '&profile_id=' + $(this).attr("value") + '&list_type_id=' + $(this).attr("list_type_id"),
                              'del_act_record' :
                         '/GC/Profile/delete_act_record?'+ '&plist_id=' + $(this).attr("plist_id")  + '&profile_id=' + $(this).attr("value") + '&list_type_id=' + $(this).attr("list_type_id"),
                             'del_segment' :
                         '/GC/Profile/delete_segment?'+ '&segment_id=' + $(this).attr("segment_id") + '&folder_id=' + $(this).attr("folder_id") + '&page_start=' + $(this).attr("page_start") + '&page_end=' + $(this).attr("page_end"),
                              'del_folder' :
                         '/GC/Profile/delete_folder?'+ '&folder_id=' + $(this).attr("folder_id")
          
                                          };
          
                  //            alert($(this).attr("action"));
                  //            alert(fun_map[$(this).attr("action")]);
          
                                          get_data(fun_map[$(this).attr("action")]);
                                  }
          
          
                                  return false;
                          });
          
                          $("#create_sub_list").click( function(){
          
                                  var query = get_query();
                          
                                  if ( query.indexOf('(') > query.indexOf(')') )
                                  {
                                          alert('Invalid Query: #0001'); 
                                  }
                                  $("#id_query").val(query);
          
                                  var attr_obj = {'plist_id': $("#plist-name option:selected").val(), 'list_type_id' : $('input[name="list_type_id"]:checked').val()  };
                              set_overlay('#edit_form', '#overlay','#boxclose, #boxcancel', attr_obj );
                  });
                          
                          $("#add_aggr").click( function(){
                                
                                var attr_name = 'aggr';
                                var $cnt = '<li id="'+attr_name+'" >';          
                                $cnt = $cnt+'<div id="'+attr_name+'_groupby">Group By: </div>';
                                $cnt = $cnt+'<div id="groupby" class="groupby">'+$("#groupby-name option:selected").val()+'</div>';
                                $cnt = $cnt+'</li>';
                                
                                $cnt = $cnt+'<li id="aggr1" >';         
                                $cnt = $cnt+'<div id="agf_id" class="cont1">'+$("#agf-id option:selected").val()+'</div>';
                                $cnt = $cnt+'<div id="agf_oper" class="cont2">'+$("#operator_count option:selected").val()+'</div>';
                                $cnt = $cnt+'<div id="agf_val" class="cont3" id="cont3">'+$("#aggr_value").val()+'</div>';
                                
                                $cnt = $cnt+'<div class="delete_aggr" title="delete rule">REMOVE</div>';
                                $cnt = $cnt+'</li>';
                                
                                toggle_fields('#gen','#aggr_funcs','#aggr_legend1','#aggr_legend2');
                                $("#aggr_legend2").attr("title","Cannot add more than one aggregate function");
                                $("#top_aggr").show();
                                $("#top_aggr ul").append($cnt);
                                
                                $(".delete_aggr").click( function() { 
                                                                   //alert('delete');
                                           $("#top_aggr ul").empty();
                                           $("#top_aggr").hide();
                                           $("#aggr_legend2").attr("title","Please add one aggregate function");
                                });
                                
                                });
                          
                          
                          
                          $("#add_segment").click( function() {
                                
                                var list_type= $('input[name="list_type_id"]:checked').val();
                                var pl_name = get_pl (list_type, 'name');
                                var pl_id = get_pl (list_type, 'id');
                                                                 
                                if (pl_id == -1)
                                {
                                   alert('Please select Profile list');
                                }
                                else if (($("#attribute-grpby option:selected").val() == -1) && ($("#attribute-grpby").attr("disabled")==false) && ($("#aggr_chk").attr("checked")==true))
                                           {
                                               alert("Please select group by");       
                                           }
                                 else if (($("#agf-id option:selected").val() == -1) && ($("#agf-id").attr("disabled")==false) && ($("#aggr_chk").attr("checked")==true))
                                 {
                                     alert("Please Select Aggregate Function");       
                                 }
                                else{
                                           
                                 var i = $("#add_segment").attr("total_rules") || 0;
                                  var j = $("#id_counter").val();
                                  i = Math.max(parseInt(i), parseInt(j)+1);
                                  j = Math.max(parseInt(i), parseInt(j)+1);
                                  i++;
                                  j++;
                                  $("#id_counter").val(j);
                                  $("#add_segment").attr("total_rules", i);
                                  var check_list = $("input[name='list_type_id']:checked").val();
                                  
                                  // for tools margin top
                                  //var tch = $("#top_container").css('height');
                                  //var ttop = -36 - parseInt(tch);
                                  //$("#tools").css({'margin-top':ttop})
          
                                  // Making Plist disabled
//                                  $("#plist-name").attr( 'readonly', true );
//                                  $("#plist-name").attr( 'title', 'You can not modify the plist in the middle of segment creation' );
          
                                  
                                  var attr_name = 'li_'+i;
                                  var cons = i;
                                  var attr_val = $("#attr_value").val();
                                  var attr_operator = $("#operator").val();
                                  draggables('#cb_open');
                                  draggables('#cb_close');
          
                                  toggle_disable('alist-name', 'plist-name','blist-name','sslist-name','all');
          
                                  //draggables('#cb_and');
                                  //draggables('#cb_or');
                                  //alert(  $('input[name="aggregate"]:checked').val() );  +pl_name+' : '       
                                  var $cnt = '<li id="'+attr_name+'" >';            
                                  $cnt = $cnt+'<div id="'+attr_name+'_div1" class="open_div" >&nbsp;</div>';
                                  $cnt = $cnt+'<div id="pln_'+attr_name+'" style="display:none;" >'+pl_id+'</div>';
                                  $cnt = $cnt+'<div id="pl_'+attr_name+'" >'+pl_name+':</div>';
                                  
                                  if ($("#operator option:selected").val() == 'EXISTS' || $("#operator option:selected").val() == 'NOT_EXISTS' )
                                  {
                                      $cnt = $cnt+'<div class="cont2" id="exists">'+attr_operator+'</div>';
                                      $(".cont2").attr('style', 'width: 65px !important');
                                      $(".cont2").attr('id', 'exists');
                                  }
                                  else if (($("#aggr_chk").attr("checked")==true) && ($("#aggr_chk").attr("disabled")==false))
                                  {
                                      // For group by
                                      var aggr = $("#agf-id option:selected").val();
                                      if ($("#attribute-grpby").attr("disabled")==false)
                                      {
                                        $cnt = $cnt+'<div id="'+attr_name+'_groupby">Group By: </div>';
                                        $cnt = $cnt+'<div id="groupby" class="groupby">'+$("#attribute-grpby option:selected").val()+' </div>';       
                                      }
                                      
                                      if ($("#agf-id option:selected").val() == 'COUNT')
                                      {
                                            $cnt = $cnt+'<div class="acont1">'+aggr+'</div>';
                                      }
                                      else{
                                            $cnt = $cnt+'<div class="acont1">'+aggr+'('+$("#attribute-name option:selected").val()+')'+'</div>';
                                      }
                                      
                                      $cnt = $cnt+'<div class="acont2">'+attr_operator+'</div>';
                                      var attr_short_val = attr_val;
                                      if (attr_val.length > 19)
                                      {
                                          attr_short_val = attr_val.substring(0,13)+'...';
                                       }
          
                                       $cnt = $cnt+'<div class="acont3" id="cont3" title="'+attr_val+'">'+attr_short_val+'</div>';      
                                  }
                                  else{
                                  $cnt = $cnt+'<div class="cont1">'+$("#attribute-name option:selected").val()+'</div>';
                                  $cnt = $cnt+'<div class="cont2">'+attr_operator+'</div>';
                                  var attr_short_val = attr_val;
                                  if (attr_val.length > 19)
                                  {
                                          attr_short_val = attr_val.substring(0,13)+'...';
                                  }
          
                                  $cnt = $cnt+'<div class="cont3" id="cont3" title="'+attr_val+'">'+attr_short_val+'</div>';
                                  }
                                  
                                  $cnt = $cnt+'<div class="close_div" id="'+attr_name+'_div2"  >&nbsp;</div>&nbsp&nbsp;';
                                  $cnt = $cnt+'<span id="select_span'+i+'">';
                                  $cnt = $cnt+'<select name="'+attr_name+'_select" id="'+attr_name+'_select" >';
                                  $cnt = $cnt+'<option value="AND">AND</option>';
                                  $cnt = $cnt+'<option value="OR">OR</option>';
                                  $cnt = $cnt+'<option value="UNION">UNION</option>';
                                  //$cnt = $cnt+'<option value="INTERSECTION">INTERSECTION</option>';
                                  $cnt = $cnt+'</select>';
                                  $cnt = $cnt+'</span>';
                                  $cnt = $cnt+'<div class="delete_rule" id="del_'+i+'" title="delete rule">REMOVE</div>';
                                  $cnt = $cnt+'<div id="lty_'+attr_name+'" style="display:none;" >'+check_list+'</div>';
                                  $cnt = $cnt+'</li>';
                                  
                                  $("#top_container ul").sortable({
                                                  update: function() {
                                                          re_calculate_conjunctions(); 
                                                  }
                                  });
                                  $("#topc_ul").append($cnt);
                                  draggables('#topc_ul');
                                  refresh_screen('agf-id', 'dd');
                                  refresh_screen('attribute-grpby', 'dd');
                                  refresh_screen('attribute-name', 'dd');
                                  refresh_screen('operator', 'dd');
								  //refresh_screen('plist-name', 'dd');
								  refresh_screen('alist-name', 'dd');
                                  refresh_screen('attr_value', 't');
                                  disable_chk('attribute-name', 2);
                                  toggle_check('aggr_chk',1)
								  
                                  disable_chk('attribute-name',2);
                                  disable_chk('attr_value',2);
                                  disable_chk('aggr_chk',2);
                                  disable_chk('attribute-grpby',2);
                                  rem_date();
                                 
                                 if (list_type ==  1)
                                 {
                                   toggle_disp('plist-name', 1);
								   toggle_disp('alist-name', 2);         
                                 }
                                 else if (list_type == 2)
                                 {
                                   toggle_disp('plist-name', 2);
								   toggle_disp('alist-name', 1);
                                 }
                                 
                                 $("#date_format").css({'display': 'none'});                                  
                                 
                                  if (i > 1)
                                  {
                                          var prev_select = 'select_span'+(i-1);
                                          $("#"+prev_select).css({'display':'block'});
                                          re_calculate_conjunctions();
                                  }
          
                                  var open_div = attr_name+'_div1';
                                  drops('pl_'+attr_name,open_div,1);
                                  drops('pln_'+attr_name,open_div,1);
                                 // drops(attr_name,open_div,1);
                                 
                                  $("#"+open_div).droppable({ 
                                  drop: function( event, ui ) {
                                    $(".open_div").css("background",'none repeat scroll 0 0 #BD2828');
                                          if(ui.draggable.text()== '(' )
                                          {
                                                  $("#"+open_div).append( ui.draggable.text() );
                                          }
                                          
                                 }
                                  }).sortable({
                                                  items: "li:not(#)",
                                                  sort: function() {
                                                  $( this ).removeClass( "ui-state-default" );
                                                  }
                                  });
                                  
                                  var close_div = attr_name+'_div2';
                                  
                                  drops('select_span'+i,close_div,2);
                                  //drops('cont3',close_div,2);
                                  //drops_class('.acont3',close_div,2);
                                  drops(attr_name,close_div,2);
                                 
                                  $("#"+close_div).droppable({ 
                                  drop: function( event, ui ) {
                                    $(".close_div").css("background",'none repeat scroll 0 0 #BD2828');
                                          if( ui.draggable.text() == ')' )
                                          {
                                                  $("#"+close_div).html( ui.draggable.text() );
                                          }
                                 }
                                  }).sortable();
          
                                  var cond_div = attr_name+'_cond';
                                  $("#"+cond_div).droppable({ 
                                 drop: function( event, ui ) { 
                                          if( ui.draggable.text() == ' AND ' ||  ui.draggable.text() == ' OR ' ||  ui.draggable.text() == ' UNION ' ||  ui.draggable.text() == ' INTERSECTION ')
                                          {
                                                  $("#"+cond_div).html( ui.draggable.text() );
                                          }
                      }
                                  }).sortable();
          
                          $("#"+open_div).dblclick(function (){
                                  alert("Removing  '(' ");
                                  $(this).html('');
                                  return false;
                          });
          
                          $("#"+close_div).dblclick(function (){
                                  alert("Removing  ')' ");
                                  $(this).html('');
                                  return false;
                          });
          
                           $("#"+open_div).hover(function(){ 
                                  $(".open_div").css('background-color','#CEDAE3'); 
                                  return false;
                                  },
                                  function(){ 
                                  $(".open_div").css('background-color',''); 
                                  return false;
                                  });
                            $("#"+close_div).hover(function(){ 
                                  $(".close_div").css("background",'none repeat scroll 0 0 #BD2828');
                                  return false;
                                  },
                                  function(){ 
                                  $(".close_div").css('background-color',''); 
                                  return false;
                                  });
                           
                          $("#cb_open").mousedown(function(){ 
                                  $(".open_div").css("background",'none repeat scroll 0 0 #BD2828');
                                  return false;
                                  });
          
                          $("#cb_close").mousedown(function(){ 
                                  $(".close_div").css("background",'none repeat scroll 0 0 #BD2828');
                                          return false;
                                  });
          
          
                          $("#cb_open").mouseleave(function(){ 
                                  $(".open_div").css('background-color',''); 
                                  return false;
                                  });
          
                          $("#cb_close").mouseleave(function(){ 
                                  $(".close_div").css('background-color',''); 
                                  return false;
                                  });
                      
                          $("#del_"+i).click( function() { 
                                  var need_del = $(this).attr("id");
                                  
                                  var remove_li = $("#"+need_del).parent().attr("id");
                                  $("#"+remove_li).remove();    
                                  var ind = 'select_span'+((need_del.split("_"))[1] - 1);
                                  none_select(ind);
                                  re_calculate_conjunctions();
                                  //var i = $("#add_segment").attr("total_rules");
                                  //i--;
                                  var i = $("#topc_ul > li").length;
                                  $("#add_segment").attr("total_rules", i);
                                  if (i <= 0)
                                  {
                                          if ($("input[name='list_type_id']:checked").val() == 1)
                                          {
                                                  $("#plist-name").attr( 'disabled', false );
                                                  toggle_disable('plist-name', 'alist-name','blist-name','sslist-name', 0);
                                                  $(".id_plist").attr('checked',true);
                                                  $("#plist-name").attr( 'title', '');
                                          }
                                          else
                                          {
                                                  $("#alist-name").attr( 'disabled', false );
                                                  toggle_disable('alist-name', 'plist-name','blist-name','sslist-name', 0);
                                                  $(".id_alist").attr('checked',true);
                                                  $("#alist-name").attr( 'title', '');
                                          }
                                          
                                          $("#add_segment").attr("total_rules", 0);
                                  }
                                  return false;
                          });
          
                  $("#top_container ul > li").mouseleave( function() {re_calculate_conjunctions(); return false;});
                  re_calculate_conjunctions();
                  
                  }
                  
                  });
          
                  
                  $(".id_alist").click( function () {
                          //if ($("#topc_ul > li").length <= 0 )
                          //{
                                 toggle_disable('alist-name', 'plist-name','blist-name','sslist-name', 0);
                          //}
                  });
          
                  $(".id_plist").click( function () {
                          //if ($("#topc_ul > li").length <= 0 )
                          //{
                                  toggle_disable('plist-name', 'alist-name','blist-name','sslist-name', 0);
                          //}
                          
                  });
          
                  $("#validate").click(function() {
                          
                          var query = get_query();
                          
                          //alert('Rajesh'+query);
          
                          if ( query.indexOf('(') > query.indexOf(')') )
                          {
                                  alert('Invalid Query: #0001'); 
                          }
                          
                  $("#id_query").val(query);
          
                  validate_data("/GC/Profile/validate_segment");
          
                          return false;
                  });
          
                  $("#create_rule").click(function() {
                          
                          var query = get_query();
                          
                          if ( query.indexOf('(') > query.indexOf(')') )
                          {
                                  alert('Invalid Query: #0001'); 
                          }
                          $("#id_query").val(query);
          
                     post_data("/GC/Profile/save_segment");
          
                          return false;
                  });
          
                  
          
                  $(".edit_segment").click( function() {
                          get_edit_segment_form("/GC/Profile/get_edit_segment_form?segment_id=" + parseInt($(this).attr('segment_id')) + "&rand=" + Math.random() );
                          
              });
          
              $(".edit_subscriber").click( function() {
                  get_data("/GC/Profile/edit_subscriber_data_form?plist_id=" + 
                          parseInt($(this).attr('plist_id')) + 
                          "&profile_id=" + parseInt($(this).attr('value')) + 
                          "&list_type_id=" + parseInt($(this).attr('list_type_id')) +
                          "&page_start=" + parseInt($(this).attr('page_start')) + 
                          "&page_end=" + parseInt($(this).attr('page_end')) +
                          "&page_count=" + parseInt($(this).attr('page_count')));
              });
          
                  $("#update_subscriber").click( function() {
                  
                          $("#plist_form").validationEngine();
          
                          if ($(".formErrorContent").length > 0) {
                                  return false;
                          }
          
                          post_data("/GC/Profile/update_subscriber");
              });
                  
          
           };
          
          function get_list_id()
          {
                var list_type= $('input[name="list_type_id"]:checked').val();
                var pl_name = get_pl (list_type, 'name');
                var pl_id = get_pl (list_type, 'id');
                
                return pl_id;
          }
          
          function refresh_screen(widget1, ftype)
          {
                     if (ftype == 'dd')
                     {
                         $("#"+widget1+" option:first-child").attr("selected", "selected");       
                     }
                     else
                     {
                           $("#"+widget1).val("");     
                     }
          }
           function toggle_class(widget)
           {
                   var menu_nav=["list_management","act_list_management","segment_folders", "sublist_folders", "dashboard","reports1","reports2","reports3","reports4",];
          
                  for (i=0; i < menu_nav.length; i++)
                  {
                          if (widget == menu_nav[i])
                          {
                                  $("#"+widget).addClass("li_selected");
                          }
                          else
                          {
                                  $("#"+menu_nav[i]).removeClass("li_selected");
                          }
                  }
                   
                           
          
           }
           
           function disable_chk (widget1, option)
           {
                     if (option == 1)
                     {
                                $("#"+widget1).attr('disabled', true);
                     }
                     else
                     {
                                $("#"+widget1).attr('disabled', false);
                     }
           }
           
           function toggle_check(widget1, option)
           {
                     //'agf-id'
                     if (option == 1)
                     {
                      $("#"+widget1).attr('checked', false);
                     }
                     else
                     {
                      $("#"+widget1).attr('checked', true);
                     }
                    
           }

		   function toggle_disp(widget, option)
		   {
			   if (option == 1)
			   {
				   $("#"+widget).removeAttr("onChange");
				   $("#"+widget).removeAttr("onFocus");
                   $("#"+widget).attr("disabled",false);
                   $("#"+widget).css({'display': 'block'});
			   }
			   else
			   {
				   $("#"+widget).attr("disabled",true);
                   $("#"+widget).css({'display': 'none'});
			   }
		   }
           
           function get_pl(ltype,option)
          {
                     var ret;
                     if (ltype == 1)
                     {
                         ret = ((option == 'name') ? $("#plist-name option:selected").text() : $("#plist-name option:selected").val());       
                     }
                     else if (ltype == 2)
                     {
                          ret = ((option == 'name') ? $("#alist-name option:selected").text() : $("#alist-name option:selected").val());             
                     }
                     else if (ltype == 3)
                     {
                          ret = ((option == 'name') ? $("#blist-name option:selected").text() : $("#blist-name option:selected").val());             
                     }
                     else if (ltype == 4)
                     {
                          ret = ((option == 'name') ? $("#sslist-name option:selected").text() : $("#sslist-name option:selected").val());             
                     }
                     return ret;
          }
           
           function rem_date()
           {
               $("#attr_value").remove();
               var str = '<input type="text" name="attr_value" id="attr_value" required="" title="Please fill out Field Name" min_length="2"/>';
               $("#attrval").html(str);       
           }
           function toggle_display (widget1, widget2,widget3,widget4,widget5, option)
           {
                     //alert(option);
                   if (option == 1)
                    {
                       $("#"+widget1).attr( 'disabled', true );
                       $("#"+widget2).attr( 'disabled', true );
                       $("#"+widget3).attr( 'disabled', true );
                       $("#"+widget4).attr( 'disabled', true );
                       $("#"+widget5).attr( 'disabled', true );
                       
                       $("#aggr_funcs").show(); 
                    }
                    else
                    {
                       $("#"+widget1).attr( 'disabled', false );
                       $("#"+widget2).attr( 'disabled', false );
                       $("#"+widget3).attr( 'disabled', false );
                       $("#"+widget4).attr( 'disabled', false );
                       $("#"+widget5).attr( 'disabled', false );
                       
                       $("#aggr_funcs").hide();         
                    }
           }
          
          function show_bulb(lock)
          {
                  if (lock == 1)
                  {
                          var widget = '<div id="bulb"></div>';
                          if ( $("#bulb").length == 0)
                          {
                                  $("body").append(widget);
                          }
                  }
                  else
                  {
                          if ( $("#bulb").length > 0)
                          {
                                  $("#bulb").css('display','none');
                          }
                  }
          
                  
          }
          
          function toggle_disable(widget1,widget2,widget3,widget4, option)
          {
                  if(option == 'all')
                  {
                          $("#"+widget1).attr("onFocus",'this.initialSelect = this.selectedIndex;');
                          $("#"+widget1).attr("onChange",'this.selectedIndex=this.initialSelect;');
                          $("#"+widget2).attr("onFocus",'this.initialSelect = this.selectedIndex;');
                          $("#"+widget2).attr("onChange",'this.selectedIndex=this.initialSelect;');
                          $("#"+widget3).attr("onFocus",'this.initialSelect = this.selectedIndex;');
                          $("#"+widget3).attr("onChange",'this.selectedIndex=this.initialSelect;');
                          $("#"+widget4).attr("onFocus",'this.initialSelect = this.selectedIndex;');
                          $("#"+widget4).attr("onChange",'this.selectedIndex=this.initialSelect;');
                  }
                  else
                  {
                          $("#"+widget1).removeAttr("onChange");
                          $("#"+widget1).removeAttr("onFocus");
                          $("#"+widget1).removeAttr("onfocus");
                         
                          $("#"+widget1).attr("disabled",false);
                          $("#"+widget2).attr("disabled",true);
                          $("#"+widget3).attr("disabled",true);
                          $("#"+widget4).attr("disabled",true);
                          
                          $("#"+widget1).css({'display': 'block'});
                          $("#"+widget2).css({'display': 'none'});
                          $("#"+widget3).css({'display': 'none'});
                          $("#"+widget4).css({'display': 'none'});
                  }
                  
          }
          function validate_data(url) {
                 var serializedData = $('#plist_form').serialize();
                 $.ajax({
                      'type': 'POST',
                      'url': url,
                       enctype: 'multipart/form-data',
                      'data':serializedData, 
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $('#validation_result').html(response);
                            // $("tr:even").css("background-color", "#eeeeee");
                            // capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                              } catch (e) {}
          
                          }
                  });
              return false;
          
           }
           
           function parse_groupby(sql)
           {
                   //alert(sql);
                   $("#top_aggr").show();
                     var abc = sql.trim();
                     var attr_name = 'aggr';
                     abc = abc.replace(/PLIST\((.*?)\)/, "$1");
                     abc = abc.replace(/GROUPBY\((.*?)\)/, "$1");
                     abc = abc.replace(/OPER\((.*?)\)/, "$1");
                     abc = abc.replace(/VAL\((.*?)\)/, "$1");
                     
                     var ab = abc.split(" ");
                     
                     var str = '<li id="'+attr_name+'" >';
                     str = str+'<div id="groupby" class="groupby" style="display:none">'+ab[0]+'</div>';
                     str = str+'<div id="'+attr_name+'_groupby">Group By: </div>';
                     str = str+'<div id="groupby" class="groupby">'+ab[1]+'</div>';
                     //str = str+'</li>';
                     //
                     //str = str+'<li id="aggr1" >';          
                     str = str+'<div id="agf_id" class="cont1">'+ab[2]+'</div>';
                     str = str+'<div id="agf_oper" class="cont2">'+ab[4]+'</div>';
                     str = str+'<div id="agf_val" class="cont3" id="cont3">'+ab[5]+'</div>';
                     
                     str = str+'<div class="delete_aggr" title="delete rule">REMOVE</div>';
                     str = str+'</li>';
                     
                     disable_chk('aggr_chk', 1);       
                     $("#aggr_ul").append(str);         
                     
                     
                     $(".delete_aggr").click( function() { 
                                                        //alert('delete');
                                $("#aggr_chk").attr('checked', false);
                                $("#aggr_ul").empty();
                                disable_chk('aggr_chk', 2);
                                disable_chk('agf-id', 1);
                                disable_chk('attribute-grpby', 1);
                                refresh_screen('attribute-grpby', 'dd');
                                refresh_screen('agf-id', 'dd');
                                disable_chk('attribute-name', 2); 
                     });
           }
          
          
          
            function re_calculate_conjunctions() { 
                                   var topc_cont = $("#topc_ul").html();
                                   var li_count =  ($("#topc_ul > li").length);
                                   //alert('li count1'+$("#top_container ul > li").length);
                                   //alert('li count2'+li_count);
                                   var html_topc = "";
                                   var j = 1;
                                  
                                  $("#top_container ul > li").each(function(index) {
                                          var counter = index+1;
                                //alert('li count3'+counter);
                                          if( counter < li_count)
                                          {
                                                  //$(this).children("div:first").html(j);
                                                  
                                                  var prev_select = $(this).children("span:first").attr("id");
                                                  $("#"+prev_select).css({'display':'block'});
                                                  var aid = prev_select.substr(11);
                                                  //alert('html:    '+aid);
                                                  if ( $("#"+prev_select).html() == '&nbsp;')
                                                  {
                                                          //$("#"+prev_select).css({'display':'block'});
                                                          var attrname = 'li_'+aid; 
                                                          var $html_cont = '<select name="'+attrname+'_select" id="'+attrname+'_select" >';
                                                                  $html_cont = $html_cont+'<option value="AND">AND</option>';
                                                                  $html_cont = $html_cont+'<option value="OR">OR</option>';
                                                                  $html_cont = $html_cont+'<option value="UNION">UNION</option>';
                                                                  //$html_cont = $html_cont+'<option value="INTERSECTION">INTERSECTION</option>';
                                                                  $html_cont = $html_cont+'</select>';
                                                                  $("#"+prev_select).html($html_cont);
                                                  }
                                                  else
                                                  {
                                                          $("#"+prev_select).css({'display':'block'});
                                                  }
                                          }
                                          else
                                          {
                                                  //$(this).children("div:first").html(j);
                                                  var prev_select = $(this).children("span:first").attr("id");
                                                  //$("#"+prev_select).css({'display':'none'});
                                                  $("#"+prev_select).html('&nbsp;');
                                          }
          
                                          j++;
                                          
                                          });
                                  
                                   $("#top_container ul > li:nth-child(odd)").css("background", "#eeeeee");
                                   $("#top_container ul > li:nth-child(even)").css("background", "#FFFFFF");
            }
          
          
          
            function draggables(widget)
            {
                   $(widget).draggable({
                      appendTo: "body",
                      helper: "clone",
                                  revert: true
                                  
                          });
            }
          
            function draggable_event(widget)
            {
                          $(widget).draggable({
                     
                  });
            }
          
            function none_select(widget)
            {
                          //$("#"+widget).css({'display':'none'});
                          $("#"+widget).html('&nbsp;');
            }
            function set_overlay(widget1, widget2, widget3, attr_str)
            {
                    
                     $(widget2).fadeIn('fast',function(){
                   $(widget1).animate({'top':'160px'},500);
                  });
          
                          $(widget3).click(function(){
                              $(widget1).animate({'top':'-600px'},500,function(){
                                  $(widget2).fadeOut('fast');
                              });
                          });
          
                  for ( x in attr_str)
                  {
                                  $("#do_action"). attr(x, attr_str[x]);
                  }
            }
          
            function set_overlay2(widget1, widget2, widget3, attr_str)
            {
                     $("#tbl_form tr:even").css("background", "#FFFFFF");
                     
                     $(widget2).fadeIn('fast',function(){
                   $(widget1).animate({'top':'160px'},500);
                  });
          
                          $(widget3).click(function(){
                              $(widget1).animate({'top':'-600px'},500,function(){
                                  $(widget2).fadeOut('fast');
                              });
                          });
          
                  for ( x in attr_str)
                  {
                                  $("#do_action2"). attr(x, attr_str[x]);
                  }
            }
          
          
            function toggle_fields(widget1, widget2,legend1,legend2 ) {
          
                 txt_legend1 = $(legend1).html();
                 txt_legend2 = $(legend2).html(); 
                 if ($(widget1).css('display') == 'none')
                          {
                                  $(legend1).html(txt_legend1.replace(/\+/g, '-'));
                                  $(legend2).html(txt_legend2.replace(/\-/g, '+'));
                                  $(widget2).hide(1000);
                                  $(widget1).show(1000);
                          }
                          else
                          {
                                  $(legend1).html(txt_legend1.replace(/\-/g, '+'));
                                  $(legend2).html(txt_legend2.replace(/\+/g, '-'));
                                  $(widget2).show(1000);
                                  $(widget1).hide(1000);
                          }
          
                  
            }
          
            function post_data(url) { 
                 var serializedData = $('#plist_form').serialize();
                 $.ajax({
                      'type': 'POST',
                      'url': url,
                       enctype: 'multipart/form-data',
                      'data':serializedData, 
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $('#content-main').html(response);
                            $("tr:even").css("background-color", "#eeeeee");
                            capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
          
                          }
                  });
              return false;
          
           }
          
           function get_edit_segment_form(url) {
                 $.ajax({
                      'type': 'GET',
                      'url': url,
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $('#content-main').html(response);
                            $("tr:even").css("background-color", "#eeeeee");
                                            //return text from ajax
                                       var ret = $("#id_query").val();
                                        var str_query = parse_query(ret);
                          capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
          
                          }
                  });
              return false;
          
           }
           
           function get_reports(url) {  
                 $.ajax({
                      'type': 'GET',
                      'url': url,
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $('#content-main').html(response);
                            $("tr:even").css("background-color", "#eeeeee");
                                            //return text from ajax
                                       var ret = $("#id_subscibers_areachart_val").val();
                                        var str_query = show_report(ret);
                          capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
          
                          }
                  });
              return false;
          
           }
           
           
           function get_graph(url) {   alert(url);
                 $.ajax({
                      'type': 'GET',
                      'url': url,
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            alert('Response '+response);
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $("#id_subscibers_piechart_val").val(response);
                            var ret = $("#id_subscibers_piechart_val").val();
                            alert('Ret '+ret);
                            var str_query = show_graph(ret);
                        //  capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
          
                          }
                  });
              return false;
          
           }

		   function get_combo(url) {   alert(url);
                 $.ajax({
                      'type': 'GET',
                      'url': url,
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            alert('Response '+response);
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $("#id_subsciberscombochart").val(response);
                            var ret = $("#id_subsciberscombochart").val();
                            alert('Ret '+ret);
                            var str_query = show_combo(ret);
                        //  capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
          
                          }
                  });
              return false;
          
           }

		    function get_linegraph(url) {   alert(url);
                 $.ajax({
                      'type': 'GET',
                      'url': url,
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            alert('Response '+response);
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $("#id_subsciberslinechart").val(response);
                            var ret = $("#id_subsciberslinechart").val();
                            alert('Ret '+ret);
                            var str_query = show_linegraph(ret);
                        //  capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
          
                          }
                  });
              return false;
          
           }
           
          
          
            function get_segment_attributes(url) {
                 $.ajax({
                      'type': 'GET',
                      'url': url,
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $('#attribute-name').html(response);
                            var resp = '<option value="-1">--Select--</option>'+response;
                            
                            $('#attribute-grpby').html(resp);
                                            $('#groupby-name').html(response);
                            //capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
          
                          }
                  });
              return false;
          
           }
          
           function check_clone_list(qs) {
                     var url = '/GC/Profile/check_clone_list?' + qs;
                 $.ajax({
                      'type': 'GET',
                      'url': url,
                      'dataType': 'json',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            // $('#content-main').html(response);
                            //$("tr:even").css("background-color", "#eeeeee");
                            // capture_clicks();
          
                            if (response.status == 1)
                            {
                                  post_data('/GC/Profile/clone_list');
                            }
                                            else {
                               $("#clone-list-error").html(response.error);
                                            }
          
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
          
                          }
                  });
              return false;
           }
          
           function check_sublist(qs) {
                     var url = '/GC/Profile/check_sublist?' + qs;
                 $.ajax({
                      'type': 'GET',
                      'url': url,
                      'dataType': 'json',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            // $('#content-main').html(response);
                            //$("tr:even").css("background-color", "#eeeeee");
                            // capture_clicks();
          
                            if (response == 1)
                            {
                                                  post_data('/GC/Profile/create_sublist');
                            }
                                            else {
                               $("#sublist-error").html("Name already exists");
                                            }
          
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
          
                          }
                  });
              return false;
           }
          
           function get_data(url) {
            $.ajax({
                      'type': 'GET',
                      'url': url,
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $('#content-main').html(response);
                            if (!(url.match(/_form/)))
                            {
                                $("tr:even").css("background-color", "#eeeeee");
                            }
                            capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                
                              } catch (e) {}
          
                          }
                  });
              return false;
          
           }
           
           function session_expriy (){
                       var str =
                                  '<div class="overlay" id="overlay" style="display:none;"></div> ' +
                                 '<div class="modal" id="expiry_form"> ' + 
                                '<a class="boxclose" id="boxclose"></a>' +
                                '<fieldset> ' +
                                '<legend>Session Epired</legend> ' +
                                
                                'Please <a href="/GC/Home/get_form">login</a> again. ' +
                                '</p> </fieldlist></div>';
                                $('#loading-popup-box').css({'display': 'none'});
                                $('#content-main').css({'display': 'block'});
                                $('#content-main').html(str);
                                $("tr:even").css("background-color", "#eeeeee");
                                capture_clicks();
                                var attr_obj = {'a': 'a'};
                                set_overlay('#expiry_form', '#overlay','#boxclose, #boxcancel', attr_obj );
           }
           function get_profiles_list() {
          
                $.ajax({
                      'type': 'GET',
                      'url': '/GC/Profile/get_profiles', 
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },  
                      'success':
                          function(response) {
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $('#content-main').html(response);
                            $("tr:even").css("background-color", "#eeeeee");
                            capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
                             
                          }
                  });
          
              toggle_class('list_management');
          
              return false;
          
          
           }
          
           function get_create_plist_form() {
                    $.ajax({
                      'type': 'GET',
                      'url': '/GC/Profile/get_create_plist_form',
                      'dataType': 'html',
                      'beforeSend':
                          function() {
                              $('#loading-popup-box').css({'display': 'block'});
                              $('#content-main').css({'display': 'none'});
                          },
                      'success':
                          function(response) {
                            $('#loading-popup-box').css({'display': 'none'});
                            $('#content-main').css({'display': 'block'});
                            $('#content-main').html(response);
                            //$("tr:even").css("background-color", "#eeeeee");
                            capture_clicks();
                            },
                      'error':
                          function(xmlhttp, error_msg, exception) {
                              try {
                                  console.error(error_msg);
                                  if (xmlhttp.status == '403')
                                    session_expriy ();
                                  
                              } catch (e) {}
          
                          }
                  });
              return false;
          
           }
          
           function add_custom_attributes() {
          
           }

		  function parse_qs() {

			var nvpair = {};
			var qs = window.location.search.replace('?', '');
			var pairs = qs.split('&');
			$.each(pairs, function(i, v){
			var pair = v.split('=');
			nvpair[pair[0]] = pair[1];
			});

			return nvpair;
		  }

          

