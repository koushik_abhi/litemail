<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<!-- saved from url=(0056)http://api.kenscio.com/hosting/kenscio/dspnew/index.html -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Reset Password</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script type="text/javascript">
function cancel()
{
	
	window.location="dashboard.jsp";
}

function validation()
{
	
	var oldpass=$("#oldpassword").val();
	var newpass = $("#newpassword").val();
    var repeat=$("#repeatpassword").val();
    alert(oldpass);
    alert(newpass);
	if(oldpass == null || oldpass == "")
	{
		alert("please enter old password");
		
		return false;
	}
	if(newpass == null || newpass == "")
	{
		alert("please enter new password");
	
		return false;
	}
	if(repeat == null || repeat=="")
	{
		alert("Please enter password again");
		return false;
	}
	
	if(newpass != repeat)
	{
		alert("newpassword and repeat not matching")
		return false;
	}
	
	document.resetform.submit();
		//return true;
	}

</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav" id="selected">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
<!--         <div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>
 -->        <div id="login_form">
         <div id ="error" class="forgotpassword" style="color:red">${errorMessage}</div> 
        <form name = "resetform" action="resetpassword.htm" method="post">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody><tr>
    <td colspan="2" ><label>Current Password: <br><input type="password" name="oldpassword" id ="oldpassword"></label></td>
  </tr>
  <tr>
    <td colspan="2" ><label>New Password: <br>
    <input type="password" name="newpassword" id = "newpassword"></label>
</td>
  </tr>
  <tr>
      <td colspan="2" ><label>Re-enter New Password: <br>
    <input type="password" name="repeatpassword" id = "repeatpassword"></label></td></tr>
  <tr>
    <td><input type="submit" name="submit" id ="submit"  value="Change password" onclick="return validation();"></td>
   <td><input type="button" id="buttonclass" value="Cancel" onclick="cancel();"></td></tr>
  
        </tbody></table>
</form>
        </div>
       
      </div>
  </div>
</div>
	<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>