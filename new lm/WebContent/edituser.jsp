<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit User</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script>
$(document).ready(function(){
	populateUserData();
	 $("select#usertype").change(function(){
		 if($("select#usertype option:selected").val() == "credits")
			 {
		 $(".assigncredits").show();
			 }
		 else
			 {
			 $(".assigncredits").hide();
			 }
	 });
});
</script>
<script>
function getRoles()
{

	$.ajax({
		type: "get",
		url: "getAllRoles.htm",
		dataType: "json",
		success: function( objData )
		{
			 populateRoles(objData); 
					
		},
		error: function(){
			alert( "An error occurred" );
		}
	});
		
		
}
function getPartners()
{

	$.ajax({
		type: "get",
		url: "getAllPartners.htm",
		dataType: "json",
		success: function( objData )
		{
			populatePartners(objData);
					
			
		},
		error: function(){
			alert( "An error occurred" );
		}
	});
		
		
}
function populateRoles(objData)
{
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderRoles(row, cont);
		cont++;
			
	}
}
function renderRoles(row,cont)
{
	var roleName = row.roleName;
	var existRole =$("select#role option:contains('" + roleName + "')").text();
	if(existRole == roleName)
		{
		return false;
		}
	else
		{
	 var role_data="<option value="+roleName+">"+roleName+"</option>";
    $(role_data).appendTo('#role'); 
		}
}

function populatePartners(objData)
{
	var cont =1;
	for(var count =0; count < objData.length; count++)
	{
		var row = objData[count];
		renderPartners(row, cont);
		cont++;
			
	}
}
function renderPartners(row,cont)
{
	var partnerName = row.name;
	var existPartner =$("select#pname option:contains('" + partnerName + "')").val();
	if(existPartner == partnerName)
		{
		return false;
		}
	else
		{
	 var partner_data="<option value="+partnerName+">"+partnerName+"</option>";
     $(partner_data).appendTo('#pname'); 
		}
}
function populateUserData()
{
	loginId ="${loginId}";
$.ajax({
	type: "get",
	url: "getUser.htm?loginId="+loginId,
	dataType: "json",
	success: function( objData )
	{
		appendDataToForm(objData.result,objData.credits);
	},
	error: function(data){
		
		//alert( "An error occurred" +data[0] );
		window.location="users.jsp";
	}
});

}
function appendDataToForm(objData,credits)
{
	$("#role").empty();
	$("#pname").empty();
	
	var userId       = objData.loginId;
	var username     = objData.userName;
	var password     = objData.password;
	var userType     = objData.userType;
	var userRole     = objData.userRole;
	var email        = objData.emailId;
	var mobileNumber = objData.mobileNumber;
	var phoneNumber  = objData.phoneNumber;
	var description  = objData.description;
	var parnterName  = objData.partnerName;
  	var costpermail=objData.costPerMail;
	$("#userid").val(userId);
	$("#name").val(username);
	$("#password").val(password);
	 var role_data="<option value="+userRole+" selected>"+userRole+"</option>";
     $(role_data).appendTo('#role');
     if(userType== "credits")
    	 {
    	 $(".assigncredits").show();
    	 }
 	$("select#usertype").val(userType)
 	.find("option[value=" + userType +"]").attr('selected', true);
    
	$("#email").val(email);
	$("#mobilenumber").val(mobileNumber);
	$("#phonenumber").val(phoneNumber);
	$("#description").val(description);
	var partner_data="<option value="+parnterName+" selected>"+parnterName+"</option>";
     $(partner_data).appendTo('#pname'); 
     getRoles();
	 getPartners();
	 $("#costpermail").val(costpermail);
	 $("#credits").val(credits);
}
function cancel()
{
	
	window.location="users.jsp";
}
function validation()
{
	var userid=$("#userid").val();
	var password=$("#password").val();
	var role = $("#role").val();
    var emailid=$("#email").val();
	var mobilenumber= $("#mobilenumber").val();
	var phonenumber= $("#phonenumber").val();
	var pname= $("#pname").val();
	var credits=$("#credits").val();
	var costpermail=$("#costpermail").val();
	if(userid == null || userid == "")
		{
		alert("user id is required!");
		document.userform.userid.focus();
		return false;
		}
	if(password == null || password == "")
	{
	alert("please provide password");
	return false;
	}
	if(role == null || role == "")
	{
	alert("role is required!");
	document.userform.role.focus();
	return false;
	}
	if(emailid == null || emailid=="")
	{
	alert("please provide valid Email");
	return false;
	}

	if (!emailValidator(email, "please provide valid email")) {
			return false;
		}
		if (mobilenumber == null || mobilenumber == "") {
			alert("mobile number is required");
			userform.mobilenumber.focus();
			return false;
		}

		if (isNaN(mobilenumber)) {
			 alert("mobile number must be numeric value");
			 userform.mobilenumber.focus();
			 userform.mobilenumber.value = "";
			return false;
		}

		
		if (pname == null || pname == "") {
			alert("partner Name is required");
		    userform.pname.focus();
			return false;
		}
		if (isNaN(costpermail)) {
			alert("cost per mail must be numeric value");
			userform.costpermail.focus();
			userform.costpermail.value = "";
			return false;
		}
		if(credits != ""){
			if(!credits.match(/^[0-9]+$/)){
				alert("credits should be must be numeric value");
				return false;
			}}
		document.userprofileform.submit();
		//return true;
	}

</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="users.jsp" class="left-nav" id="selected">Users </a>
 <a href="partners.jsp" >Partners </a>
  <a  href="credits.jsp"> Credits</a>
  <a href="showsmtpManagement.htm"> SMTP Configuration</a>
   <a href="showemailconfig.htm"> Notification Settings</a>
 <a href="dashboard.jsp" class="right-nav"> Dashboard </a>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
<!-- 		<div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>
 -->        <div class="contents">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<!-- upload list -->
	<form name = "userform" action="updateuser.htm" method="post">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="create_list">
  <tbody>
 <tr>
    <td width="130"><label>User Id<font style="color:red;">*</font> :</label></td>
    <td><input type="text" id = "userid"  name="userid" size="30" readonly></td>
  
   <td><label>User Name :<br></label></td>
 	<td><input type="text" id = "name"  name="name" size="30" readonly></td>
  </tr>
 <tr>
	<td><label>Password <font style="color:red;">*</font>:<br></label></td>
   <td><input type="password" id = "password"  name="password" size="30" style="width: 62%"></td>
 
    <td><label>User Type <font style="color:red;">*</font> :<br></label></td>
 	 <td><select name="type" id="usertype"  style="width: 272px;">
     <option value="licensed">Licensed</option>
     <option value="credits">Credits</option> 
    </select>
    </td>
  </tr>
<tr>
	<td><label>Role <font style="color:red;">*</font>:<br></label></td>
 	 <td><select name="role" id="role"  style="width: 272px;">
    <option value="" id="">   -------Select Role------</option></select>
    </td>
    
   <td><label>Email Id <font style="color:red;">*</font>:<br></label></td>
   <td><input type="text" id = "email"  name="email" size="30"></td>
  
   
  </tr>
  <tr>
   <td><label>MobileNumber <font style="color:red;">*</font>:<br></label></td>
   <td><input type="text" id = "mobilenumber"  name="mobilenumber" size="30"></td>
  
   <td><label>PhoneNumber :<br></label></td>
   <td><input type="text" id = "phonenumber"  name="phonenumber" size="30"></td>
  </tr>
  <tr>
    <td><label>Description :<br></label></td>
   <td><input type="text" id = "description"  name="description" size="30"></td>
   
   <td><label>Partner Name <font style="color:red;">*</font>:<br></label></td>
   <td><select name="pname" id="pname"  style="width: 272px;">
    <option value="" id="">   -------Select Partner------</option></select>
    </td>
    
  </tr>
  <tr>
  <td><label>Cost/Mail :</label></td><td><input type="text" id="costpermail" name="costpermail" size="30"></td>
  
   <td class="assigncredits" style="display: none;"><label>Credits :</label>
   </td><td class="assigncredits" style="display: none;" >
   <input type="text" id = "credits"  name="credits" size="30" readonly="readonly"></td>
  </tr>
  <tr ><td>&nbsp;</td>
   <td colspan="3" align="right" style="padding-left: 200px"> &nbsp;<input type="submit" name="submit" value="Save" onclick=" return validation();" id="submit">
	<input type="button"  id="buttonclass"  value="Cancel" onclick="cancel();"></td>
  </tr>
  <tr>
 <!--  <td><label>Cost/Mail :</label></td><td><input type="text" id="costpermail" name="costpermail" size="30"></td>
    <td><input type="hidden" name="fromdashboard" id="fromdashboard" value="false"></td>
  <td colspan="3" align="right" style="padding-left: 200px"> &nbsp;<input type="submit" name="submit" value="Save" onclick=" return validation();" id="submit">
	<input type="button"  id="buttonclass"  value="Cancel" onclick="cancel();"></td>
  </tr> -->
</tbody>
</table>
</form>
</div>
</div>
</div>
<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>
</body></html>