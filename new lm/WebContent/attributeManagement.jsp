<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List Management</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/jquery-ui.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="./css/main.css" />
<link rel="stylesheet" href="uitrans/jquery-ui.css" />
<script  type="text/JavaScript" src="./js/jquery-1.9.1.js" charset="utf-8"></script>
   <script type="text/JavaScript" src="./js/jquery-ui.js" charset="utf-8"></script>


<script>
var pagenum = 1;
$(document).ready(function() {
renderlistpage();
$("input#addattribute").click(function(){
	$("#createnewattrid").dialog({
	         position: 'center',
	         width: 345,
	         height: 190,
	         title: 'Add Attribute',
	         modal: true
	 });

	});
$("select#attrtype").change(function(){
	var selectdvalues=$("#attrtype").val();
	
 	if((selectdvalues == "String") ||(selectdvalues == "Numeric") || (selectdvalues == "Date")) 
		{
		 $("#attrvalue").show();
		 $("#boolvalue").hide();
		}
	else{
		
		 $("#attrvalue").hide();
		 $("#boolvalue").show();
	 
	}  
 });
});

function renderlistpage()
{
	$("#attribrows").empty();
	$("#attribrowsPage").empty();
	$.ajax({
		type: "get",
		url: "displyattributes.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			var result = objData.result;
			if(result != null && result.length > 0)
			{
			
				renderlisttable(result, objData.pagenumber, objData.totalpages);
			}
			var result1 = objData.result1;
			if(result1 != null && result1.length > 0)
			{
				renderstdlisttable(result1, objData.pagenumber, objData.totalpages);
			}
				
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}

function rendernextlistpage()
{
	$("#attribrows").empty();
	$("#attribrowsPage").empty();
	pagenum =pagenum + 1;
	$.ajax({
		type: "get",
		url: "displyattributes.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}

function renderprevlistpage()
{
	$("#attribrows").empty();
	$("#attribrowsPage").empty();
	pagenum =pagenum-1;
	$.ajax({
		type: "get",
		url: "displyattributes.htm?pagenumber="+pagenum,
		dataType: "json",
		success: function( objData )
		{
			renderlisttable(objData.result, objData.pagenumber, objData.totalpages);	
		},
		error: function(data){
			
			alert("data " +data);
		}
	});
	
}



function renderstdlisttable(objData, pagenumber, totalpages)
{

	pagenum = pagenumber;
	if(objData != null && objData.length > 0)
	{
		$("#stdattribrows").append('<tr style="background-color: rgb(238, 238, 238);" id="Attribute Name"><th width="100">Attribute Name</th><th width="100">Attribute Type</th><th width="100">Attribute Initial Value</th><th width="100">Standard Attribute</th><th width="100">Created On</th></tr>');
		var cont =1;
		for(var count =0; count < objData.length; count++)
		{
			var row = objData[count];
			renderstdRowInListTable(row, cont);
			cont++;
				
		}
		$("#stdattribrowsPage").append('<tr align:right>');
		
		 if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$("#stdattribrowsPage").append('<td align:right><input type="submit"  style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"><input type="submit"   style="float: right; margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$("#stdattribrowsPage").append('<td align:right><input type="submit"   style="float: right" id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$("#stdattribrowsPage").append('<td align:right><input type="submit"  style="float: right" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td>');
			
		}
		$("#stdattribrowsPage").append('</tr>');
		
		
	}
	else
	{
		$("#stdattribrows").append('<tr><td style="color:red">No Standard Attribute found.</td></tr>');
	}
	
}

function renderlisttable(objData, pagenumber, totalpages)
{
	pagenum = pagenumber;
	if(objData != null && objData.length > 0)
	{
		$("#attribrows").append('<tr style="background-color: rgb(238, 238, 238);" id="Attribute Name"><th width="100">Attribute Name</th><th width="100">Attribute Type</th><th width="100">Attribute Initial Value</th><th width="100">Standard Attribute</th><th width="100">Created On</th><th width="100">Actions</th></tr>');
		var cont =1;
		for(var count =0; count < objData.length; count++)
		{
			var row = objData[count];
			renderRowInListTable(row, cont);
			cont++;
				
		}
		$("#attribrowsPage").append('<tr align:right>');
		
		if(totalpages > 1 && pagenumber > 1 && totalpages != pagenumber)
		{
			$("#attribrowsPage").append('<tr><td align="right" width="50%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td align="right" width="50%"><input type="submit"  margin-right: 10px" id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()">&nbsp;&nbsp;<input type="submit"   id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td></tr>');
			
		}else if(totalpages > 1 && pagenumber == 1)
		{
			$("#attribrowsPage").append('<tr><td align="right" width="50%" ><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td align="right" width="50%" style="padding-right:120px;"><input type="submit"    id ="next" name = "next" value="Next" onclick="rendernextlistpage()"></td></tr>');
			
		}else if (totalpages == pagenumber  && totalpages > 1)
		{
			$("#attribrowsPage").append('<tr><td align="right" width="50%"><label ><strong> Page  '+pagenum+' Of '+totalpages+'</strong></label> </td><td align="right" width="50%" style="padding-right:120px;"><input type="submit"   id ="previous" name = "previous" value="Previous" onclick="renderprevlistpage()"></td></tr>');
			
	}
	}
	else
	{
		$("#attribrows").append('<tr><td style="color:red">No Custom Attributes found.</td></tr>');
	}
	
}

function renderstdRowInListTable(row, cont)
{
	var attributeId=row.attrId;
	var attributeName = row.attrName;
	var type = row.attrType;
	var	value =row.attrInitialValue;
	if(value == null)
		value ="";
	var account = row.accountId;
	var std = "Yes"
	if(account != null)
	{
		std = "No"
	}
	var createdOn = row.createdDate;
	$("#stdattribrows").append('<tr style="text-align:left;" id="'+attributeName+'"><td width="100" >'+attributeName+'</td><td width="120" align="center"  >'+type+'</td><td width="120" >'+value+'</td><td width="120" align="center">'+std+'</a></td><td width="120" align="center">'+createdOn+'</a></td></tr>');
}

function renderRowInListTable(row, cont)
{
	var attributeId=row.attrId;
	var attributeName = row.attrName;
	var type = row.attrType;
	var	value =row.attrInitialValue;
	if(value == null)
		value ="";
	var account = row.accountId;
	var std = "Yes"
	if(account != null)
	{
		std = "No"
	}
	var createdOn = row.createdDate;
	$("#attribrows").append('<tr style="text-align:left;" id="'+attributeId+'"><td width="100" >'+attributeName+'</td><td width="120" align="center"  >'+type+'</td><td width="120" >'+value+'</td><td width="120" align="center">'+std+'</a></td><td width="120" align="center">'+createdOn+'</a></td><td align="center" >&nbsp;&nbsp;<img src="images/delete.png" class="tooltip" title="delete" onclick="deleteRow(\''+attributeId+'\');" /></td></tr>');
}


function deleteRow(id)
{
	if(confirm("are you sure you want to delete this Attribute"))
	{
	
	$.ajax({
		type: "get",
		url: "deleteattribute.htm?attributeid="+id,
		data: id,
		dataType: "json",
		success: function( objData )
		{
			window.location = "attributeManagement.jsp";	
		},
		error: function(data){
			
			//alert( "An error occurred" +data[0] );
			window.location = "attributeManagement.jsp";
		}
	});
	}
	return false;
	
}



</script>
<script type="text/javascript">
function submitNewAttr()
{
	var name= document.createnewattr.attrname.value;
	var value=document.getElementById("value").value;
	
	if(name=="" || name==null)
	{
		alert("Please provide Attribute name");
		return false;
	}
	

		
	if (!validName(name)) {
			alert('Please provide a valid Attribute name');
			return false;

		}
	
		
	if (document.getElementById("attrtype").value == "String" && value != "") {
			if (!validName(value)) {
				alert('Please provide string value');
				return false;
			}
		}

		
	if (document.getElementById("attrtype").value == "Date" && value != "") {
			if (!validDate(value)) {
				return false;
			}
		}

	
	if (document.getElementById("attrtype").value == "Numeric" && value != "") {
			if (!validNumeric(value)) {
				return false;
			}

		}

		else {
			document.createnewattr.submit();
		}

	}

	function validDate(value) {
		if (/^([0-9]{2})-([0-9]{2})-([0-9]{4})$/.test(value) == false) {
			alert("Please provide date in DD-MM-YYYY format");
			return false;
		} 
		
		else {
			document.createnewattr.submit();

		}
	}
	function validNumeric(value) {
		if (/^[\d.]+$/.test(value) == false) {
			alert("Please provide numeric value");
			return false;
		} else {
			document.createnewattr.submit();

		}
	}

	function validName(name) {
		if (name != null && name != "") {

			if (/^[a-zA-Z0-9_]*$/.test(name) == false) {
				return false;
			} else {
				return true;
			}
		}
	}

	function closeWin() {
		$("#createnewattrid").dialog('close');
	}

</script>
</head>
<body>
<div id="wrapper">
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="images/kenscio-logo.jpg"  border="0" width="70" /></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm" id="selected"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
<!--         <div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>
 -->        <div class="contents">
         <div align="center" style="color:red">${errorMessage}</div><br>
        <table cellspacing="1" id="DefineAttributes" style="font-weight:normal;width:800px;margin-left: 130px">
         <tr>
        <td align="right"><input type="submit" id="addattribute" name="addattribute" value="Add New Attribute"></td>
        </tr></table>

<table id="attribrows" class="tblewidth"  style="font-weight:normal;width:800px;margin-left: 130px">
</table>
<table id="attribrowsPage" align="right" cellspacing="1" style="font-weight:bold;width:950px; padding:1px;">
</table><br><br><br><br><br><br>

<table id="stdattribrows" class="tblewidth"  style="font-weight:normal;width:800px;margin-left: 130px">
</table>
<!-- <table id="stdattribrowsPage" align="right" cellspacing="1" style="font-weight:bold;width:950px; padding:1px;">
</table><br><br><br><br><br><br> -->

<div id="createnewattrid" style="display: none;" >
<form name="createnewattr" method="get" action="./addattribute.htm">
    <table align="center" width="320" >
  <tr>
  <td >Name:</td><td><input type="text" style="font-size: 12px" name="attrname" size="21" id="attrname"/></td>
   </tr>
   
   <tr>
   <td>Type:</td>
   <td>
   <select name = "attrtype" id="attrtype" style="width:168px; font-size:12px;">
   <option value="String">String</option><option value="Numeric">Numeric</option><option value="Date" id="Date">Date(dd-mm-yyyy HH:MM:SS)</option><option value="Boolean">Boolean</option></select></td>
   </tr>
   
    <tr id="attrvalue">
  <td>Value:</td><td><input type="text"  style="font-size: 12px "name="value" size="21" id="value"/></td>
   </tr>
   
   <tr id="boolvalue" style="display: none" >
  <td>Value:</td>
  <td><select name = "boolvalue" id="bootype" style="width:168px; font-size:12px;">
   <option value="False">False</option><option value="True">True</option><option value="">Null</option></select></td>
   </tr>
   
   <tr>
   <td>&nbsp;</td>
  <td style="padding-top:10px;font-size:12px;"><input type="button" class="btnsmall" id="newattribute" value="Submit" onclick = "submitNewAttr()"> 
   <input type="button" class="btnsmall" id="close" value="Cancel" onclick="closeWin()"> </td></tr>
  </table>

</form>
</div>

</div>
</div>
</div>
</div>


<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>