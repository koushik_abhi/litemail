<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<%@ taglib uri="/WEB-INF/ckeditor.tld" prefix="ckeditor" %>
<%@page import="com.ckeditor.CKEditorConfig"%>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Schedule Sendout</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="./css/smoothness/jquery-ui-smoothness.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="./js/jquery-1.9.1.js" charset="utf-8"></script>
<script type="text/javascript" src="./js/jquery-ui.js" charset="utf-8"></script>

<script type="text/javascript" src="js/jquery-ui-timepicker-addon.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="js/jquery-ui-timepicker-addon.css" />

<style>
.btnsmall {
    background-color: #BD2828;
    border: 1px solid #D83526;
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 1px 0 0 #F29C93 inset;
    color: #FFFFFF;
    cursor: pointer;
    display: inline-block;
    font-family: arial;
    font-size: 13px;
    font-weight: bold;
    margin: 3px;
    padding: 3px 15px;
    text-decoration: none;
    text-shadow: 1px 1px 0 #B23E35;
}

.ui-dialog-title{
  font-size:17px;
  font-family:Tomoha;   
}
td a
{
color:#000;
text-align: center;
text-decoration: underline;
cursor: pointer;
}
.closepopup
{
color:#000;
text-align: center;
cursor: pointer;
right: 0;
top: 0;
position: absolute;
margin-right:10px;
font-weight: bold;
}
 .ui-datepicker {
    width: 190px;
} 
.rightalign{
padding-left: 80px;
}
</style>
<script>

$(document).ready(function(){
	
	var campName ="${campaignName}";	
		getSendoutData(campName);
		
	$("a#previewmessage").click(function(){
		$("#previewpopup").dialog({
		         position: 'center',
		         width: 720,
		         height: 500,
		         title: 'Campaign Preview',
		         modal: true
		 });
		$(".ui-dialog-titlebar").hide();
		});

	$('#scheduledatedisply').each(function(){
	    $(this).datetimepicker({
	    	controlType: 'select',
	    	dateFormat:'dd-mm-yy',
	    	timeFormat: 'HH:mm'
	    });
	    $(".ui-datepicker").css("font-size", "11.5px");
	   
	});
	
	 $('.schedulepopup').on('click',function(e){
		 	var campaignName ="${campaignName}";	
			//var campaignName=$("#campaignName").val();
			var listName=$("#listName").val();
			var scheduleDate=$("#scheduledatedisply").val();
			var mailsPerMinute=$("#mailstosend").val();
			
			 var numericExpression = /^[0-9]+$/;
			 if(mailsPerMinute != ""){
				if(!mailsPerMinute.match(numericExpression)){
					alert("speed should be in integer");
					return false;
				}}
				var creditsAvl =parseInt( $("#creditsavl").text());
				var creditsReq = parseInt($("#creditsrq").text());
				
			if(creditsReq > creditsAvl){
			alert("no enough credits available to start campaign");
			}
			else{
			if(validateDate(scheduleDate))
				{
				 	if(!confirm("Are you sure you want to  schdule the campaign " + campaignName))
					  {
					  return false;
					  }
	        $.ajax({
	           type : 'get',
	           url: "schedulelater.htm?campaignName="+campaignName+"&listName="+listName+"&scheduleDate="+scheduleDate+"&mailsperminute="+mailsPerMinute,
	           dataType: "json",
				success: function(response)
				{
					alert("sucessfully scheduled campaign ");
					$("#schedulecampaign").dialog('close');
					$("#scheduledatedisply").val(response.result);
					window.location = "campaignManagement.jsp";
	           },
	           error : function(data) {

					alert("error");
				}
	        });
			}
			}		 
	    });
	 
	 function validateDate(scheduleDate)
	 {
		 var date =scheduleDate.substring(0,2);
		var month=scheduleDate.substring(3,5);
		var year =scheduleDate.substring(6,10);
		var hour =scheduleDate.substring(11,13);
		var mins = scheduleDate.substring(14,16);
		var inputDate = new Date(year, month - 1,date,hour,mins);
		var todayDate = new Date();
		
		if (inputDate < todayDate) {
			
			alert("ScheduleDate should be greater than current Date!");
			return false;
		} 
		return true;
	 }
	 
	 $(".previewcampaign").on('click',function(e){
			var campaignName ="${campaignName}";	
		// var campName=$("#campaignName").val();
		 mailPreview(campaignName);
	 });
	});


function startNow()
{

	var speed = $("#mailstosend").val();
		var numericExpression = /^[0-9]+$/;
		if (speed != "") {
			if (!speed.match(numericExpression)) {
				alert("speed should be in integer");
				return false;
			}
		}

		
		var creditsAvl =parseInt( $("#creditsavl").text());
		var creditsReq = parseInt($("#creditsrq").text());
		
	if(creditsReq > creditsAvl){
	alert("no enough credits available to start campaign");
	}
	else{
	

		var scheduleDate = $("#scheduledatedisply").val();
		if (scheduleDate == "") {
			if (confirm("do you really want to start campaing now")) {
				var deletecamp = "no";
				actionurl = "startcampaign.htm";
				document.startcampaign.action = actionurl;
				document.startcampaign.submit();
			}
		} else {
			if (confirm("campaign is scheduled do you really want to start now,it will delete the existing schedule")) {
				var deletecamp = "yes";
				actionurl = "startcampaign.htm";
				document.startcampaign.action = actionurl;
				document.startcampaign.submit();
			}

		}

	}
}

	function testMail() {
		var campaignName ="${campaignName}";	
		//var campaignName = $("#campaignName").val();
		var listName = $("#listName").val();
		var emailsTest = $("#testemailid").val();

		if (emailsTest == "") {
			alert("Please provide valid emailId's for test campaign to sendout");
		}
		window.location.href = "sendtestcampaign.htm?campaignName="
				+ campaignName + "&listName=" + listName + "&emailsTest="
				+ emailsTest;

	}

	function viewrecords() {
		$("#showrecords").dialog({
			position : 'center',
			width : 400,
			height : 350,
			title : 'List Records',
			resizable: false,
			modal : true
		});
		$(".ui-dialog-titlebar").hide();
	}

	function cancel() {

		window.location = "campaignManagement.jsp";

	}

	function getSendoutData(campaignName) {
		var campName = campaignName;
		$.ajax({
			type : "get",
			url : "getsendoutdata.htm?campaignname=" + campName,
			dataType : "json",
			success : function(objData) {

				renderCampaignData(objData.campdata);
				renderListData(objData.listrecords, objData.domaincount);
				renderCredits(objData.creditsavl, objData.creditsreq);
			},
			error : function(data) {

				alert("data " + data);
			}
		});

	}
	function renderCredits(avlCredits, reqCredits) {
		if (avlCredits == null && reqCredits == null) {
			$("#licensed").show();
			$("#creditsavl").empty();
			$("#creditsrq").empty();
			
		} else {
			$("#cravl").show();
			$("#crreq").show();
			$("#creditsrq").text(reqCredits);
			$("#creditsavl").text(avlCredits);
			
		}
	}

	function renderCampaignData(result) {
		
		$("#smtpserver").empty();
	//	$("#campaignname").text(result.campaignName);
		$("#listName").val(result.listName);
		$("#mailstosend").val(result.campaignSpeed);
		$("#listNamelabel").text(result.listName);
	
		$("#subject").text(result.subject);
		var smptpServer = "<option value="+result.smtpServer+" selected>"
				+ result.smtpServer + "</option>";
		$(smptpServer).appendTo('#smtpserver');
		$("#previewsmall").html(result.campaignMessage);
		$("#viewmore").html(result.campaignMessage);
		var scheduleTime = result.scheduleTime;
		//alert(scheduleTime);
		//alert(result.campaignSpeed);
		if (scheduleTime != null) {
			$("#scheduledatedisply").val(scheduleTime);
		}
		getListSmtpServer();
	}

	function renderListData(listrecords, domaincount) {
		$("#noofrecords").text(listrecords);
		renderDomainData(domaincount);
	}

	function renderDomainData(objData) {
		$("#recordsdata").empty();
		$("#recordsdata").append('<tr  style="background-color:#777;" align="center"><td  width="330" style="color:#fff"><Strong>Domain Name</strong></td><td width="290" style="color:#fff"><Strong>No of Records</Strong></td></tr>');
		$.each(objData, function(k, v) {
			
			renderDomain(k, v);

		});

	}
	function renderDomain(k,v)
	{
		 $("#recordsrow").empty();
		var domainName = k;
		 var domainCount=v;
		 
		 $("#recordsdata").append('<tr><td width="100" align="left">'+domainName+'</td><td width="100" align="center">'+domainCount+'</td></tr>');
	}

	function getListSmtpServer() {
		$.ajax({
			type : "get",
			url : "getlistsmtpnames.htm",
			dataType : "json",
			success : function(objData) {

				populateSmtpServers(objData.smptpServers);
			},
			error : function(data) {

				alert("data " + data);
			}
		});

	}

	function populateSmtpServers(result) {
		var existSmtp = $('#smtpserver').text();
		if (result != null && result.length > 0) {
			$.each(result, function(i, value) {
				if (existSmtp == value) {
				} else {
					var smtp_data = $('<option>').text(value).attr('value',
							value);
					$(smtp_data).appendTo('#smtpserver');
				}

			});
		}

	}

	function closeWin() {
		$("#schedulecampaign").dialog('close');
	}
	function closepopup() {
		$("#previewpopup").dialog('close');
	}
	function closepopup1() {
		$("#showrecords").dialog('close');
	}
</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content" >
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
<!--            <div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout-red.jpg" width="86" height="127"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>
 -->        <div class="contents">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<form  name="startcampaign" method="post">
	<table cellpadding="15" cellspacing="15" class="rightalign">
	<tr>
	<td width="250"><label>Campaign Name :</label><label id = "campaignName" style="margin-left:5px;"><%= request.getAttribute("campaignName") %></label>
	<input type="hidden"  name="campaignName" id="campaignName" value=<%= request.getAttribute("campaignName") %> >
	</td>
	<%-- <td ><label id = "campaignname"><%= request.getAttribute("campaignName") %></label><input type="text" id = "campaignname"  name="campaignname" size="30" value="<%= request.getAttribute("campaignName") %>" readonly></td> --%>
	<td width="350"><label >Total No of Records :</label>
	<label id="noofrecords"></label>
<!-- 						<input type="text"  id="noofrecords"  name="noofrecords" size="10" value="" readonly >
 -->						<label onclick="viewrecords();"><a href="#" style="cursor: pointer;text-decoration: underline;color:#000;">View</a></label></td>					
	</tr>
	<tr>
	<td width="250"><label>List Name :</label><label id="listNamelabel" style="margin-left:5px;"></label> 
	<input type="hidden" id="listName"  name="listName" size="30" value="" readonly></td>
	<td id="licensed" style="display: none"><label>User Type : Licensed</label></td>
	<td style="display: none" id="cravl"><label>Credits Available :</label>
	<label id="creditsavl"></label>
	</td>	
	</tr>	
	<tr>
	<td width="350"><label>Subject :</label><label id="subject" style="margin-left:5px;"></label> </td>
	<!-- <input type="text" id="subject" name="subject" size="30" value="" readonly></td>	 -->		
	<td width="350" style="display: none"  id="crreq"><label>Credits Required :</label>
	<label id="creditsrq"></label>
	</td>
	</tr>	
	<tr>
	<td width="350"><label>Smtp Server :</label>
	 <select name="smtpserver" id="smtpserver" style="width: 165px;height:35px;margin-left:77px;">
	 <option>--Select Smtp Server--</option>
	 </select>
	 </td>
	<td width="400"><label>Campaign Speed (Minimum of 1000/hr) :</label>
	<input type="text" name="mailstosend" id="mailstosend" size="10"  value="" style="height:20px;"></td>
	
	 <tr>
						<td width="480"><label>Schedule Date And Time :</label>
						<input type="text"  id="scheduledatedisply"  name="scheduledatedisply" size="16" value="">
						<input type="button" id="buttonclass" class="schedulepopup" value="Schedule Later" title="Schedule" style="padding:2px 4px;">
						</td>
						
					</tr>
	 
	
	</table>
	<table class="rightalign">
	<tr>
	<td style="float:left;margin-left:10px;"><label>Message Body :</label></td>
	<td style="float:left;margin-left:630px;"><label><a href="#" id="previewmessage" style="cursor: pointer; text-decoration: underline;">View More</a></label></td>
	</tr>
	<tr>
   		<td >
 <div id="previewsmall" style="height:250px;width:810px;margin-left:10px;position:relative;max-height:100%;overflow:auto;border:5px solid #eee;background-color: #fff;"></div>
      </td>   	
 </tr>
   <tr>
   <td colspan="4">
   <table >
   <tr>
   <td  style="padding-top:20px;"><input size="30" type="text" name="testemailid" id="testemailid" placeholder="Test Email Id" ></td>
   <td ><input type="button" id="buttonclass" value="Send Preview Test"  class=" sendtestmail" onclick="return testMail();"></td>
   
						<td style="padding-left: 300px;"><input type="button"  id="buttonclass" name="startnow" value="Start Now" onclick="return startNow();"></td>
						<td align="right"><input type=button id="buttonclass" value="Cancel" onclick="cancel();"></td>
   </tr>
   </table>
	</td>	
	</tr>			
   	<tr style="display:none;">
   						<td>
   						 <div id="previewpopup" style="display: none;"><span style="font-size: 18px">Message Body</span>
   						   <span class="closepopup" onclick="closepopup();" title="Close">X</span>
   						 <div id="viewmore" style="background-color: white;"></div>
   						 </div></td>
   					</tr>
	</table>
	</form>
	<label class="rightalign"><strong>&nbsp;&nbsp;&nbsp;&nbsp;Note:</strong> Multiple emails should be separated with comma (",") </label>
			<div id="showrecords"  style="display: none;overflow: auto; " ><span style="font-size: 18px">Domain Counts</span>
			<span class="closepopup" onclick="closepopup1();" title="Close">X</span>
    		<table  id="recordsrow" style="margin-top:20px;">
 			 </table>
 			 <table   id="recordsdata" class="tblewidth"></table>

	</div>
	</div>
   </div>
</div>
<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>