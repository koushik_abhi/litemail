<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Data Service Platform</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script>
var listname= "";
$(document).ready(function() {

$("#create_list tr.imp1").hide();

listname ="${listname}";	
if(listname != null && listname != "")
{
	renderData(listname);
}

$("#rowfile").css("display","");
$("#rowftp").css("display","none");
$("#rowsftp").css("display","none");
$('input[name=datasource]:radio').click(function()
{
	
	if($(this).val()=="emailids")
	{
		$("#rowemail").css("display","");
		$("#rowfile").css("display","none");
		$("#rowftp").css("display","none");
	}
	else if($(this).val()=="fileupload")
	{
		//$("#rowemail").css("display","none");
		$("#rowfile").css("display","");
		$("#rowftp").css("display","none");
		$("#rowsftp").css("display","none");
	}
	else if($(this).val()=="ftpupload")
	{
		//$("#rowemail").css("display","none");
		$("#rowfile").css("display","none");
		$("#rowsftp").css("display","none");
		$("#rowftp").css("display","");
	}
	else if($(this).val()=="sftpupload")
	{
		$("#rowfile").css("display","none");
		$("#rowftp").css("display","none");
		$("#rowsftp").css("display","");
				
	}
});
$('input[id=defineSkip]:submit').click(function(){
	$("#welcome").css("display","");
	$("#").css("display","none");
	$(".defAttr").css("display","none");
});
//renderData();
			
});
function validateForm(name)
{
	/*if(name == null && name =="")
	{
		alert("Please enter valid list name");
		elem.focus();
		return false; 
	}*/
	if(isalphaNum(name, "Please enter valid name (A-Z, a-z, 0-9)"))
	{
		return true;
	}
	return false;
	
}

function renderData(listname)
{
	$.ajax({
        type: "GET",
        url:"getlistdetails.htm?listname="+listname,
        data:{
        	
        	listname:listname
        	
        },
        dataType: "json",
        success: function(result)
        {
            populateData(result);
             
        },
        error: function(x, e)
        {
             //alert(x.readyState + " "+ x.status +" "+ e.msg);   
        }
     });
	
	
}
function populateData(result)
{
	if(result != null && result.length > 0)
	{
		result = result[0];
		document.getElementById("listname").value= result.listName;
		document.getElementById("description").value = result.listDescription;
	}
	
}
function importRecords()
{
	var listname = document.getElementById("listname");
	var values = listname.value.trim();
	if(values =="")
	{
		alert("please enter listname");
		return false;
	}
		
	if(validateForm(values))
	{
	    
		var actionurl ="";
		var type = $("input:radio[name='datasource']:checked").val();
		
		if(type == "fileupload")
		{	
			
		    document.uploadlistform.enctype = "multipart/form-data";
			actionurl = "uploadlist.htm";
			document.uploadlistform.action = actionurl;
			
			document.uploadlistform.submit();
			return true;
			
			
		}else if(type == "emailids")
		{	
			
			actionurl = "addemailrecordemails.htm";
			document.uploadlistform.action = actionurl;
			
			document.uploadlistform.submit();
			return true;
		
		}else
		if(type == "ftpupload")
		{
			actionurl = "uploadlistftp.htm";
			document.uploadlistform.action = actionurl;
			document.uploadlistform.submit();
			return true;
			
			
		}else if(type  == "sftpupload")
		{
			var sftpkey = document.getElementById("sftpfile").value;
			if(sftpkey != null && sftpkey != "" && sftpkey.length > 1)
			{
		    	document.uploadlistform.enctype = "multipart/form-data";
			}
			actionurl = "uploadlistftp.htm";
			document.uploadlistform.action = actionurl;
			document.uploadlistform.submit();
			return true;
			
		}
		return false;
		
		
		
	}else
	{
		return false;
	}
	
	
}

function cancel()
{
	
	window.location="listManagement.jsp";
}

function updates(){
	 if ((document.getElementById('chk1').checked))
	 {  $("#create_list tr.imp1").hide();
	} 
	 if ((document.getElementById('chk2').checked))
	 {  $("#create_list tr.imp1").hide();
	 alert("Are you sure,you want to delete");
	} 
	 if ((document.getElementById('chk3').checked))
	 {  $("#create_list tr.imp1").hide();
	 
	} 
	 if ((document.getElementById('chk4').checked))
	 {  $("#create_list tr.imp1").hide();
	} 
			
 }

</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showdashboard.htm" class="left-nav"> Dashboard </a>
 <a href="showattributeManagement.htm"> Attributes </a>
<div id="menu2" class="menu">
  <a href="#"  class="right-nav">Account Management</a>
   <ul class="menulist" id="menulist2">
    <li><a href="showresetpasswordscreen.htm">Change Password</a></li>
    <li><a href="showuserProfile.htm"> User Profile</a></li>
    </ul>
</div>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
         <div class="contents">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<!-- upload list -->
	<form name = "uploadlistform" action="uploadlist.htm" method="post">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="create_list">
  <tbody>
  <tr>
    <td width="130"><label>List Name</label></td>
    <td><input type="text" id = "listname"  readonly name="listname" size="30"></td>
  </tr>
  <tr>
   <td><label>Description<br></label></td>
   <td><textarea id="description"  name="description" rows="5" cols="40"></textarea></td>
  </tr>
  <tr>
  <td height="40">&nbsp;</td>
   <td><label><input type="radio" name="datasource" checked="checked" value="fileupload"> File Upload</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio" name="datasource"  value="ftpupload"> FTP </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio" name="datasource"  value="sftpupload"> SFTP</label>
</td>
  </tr>
   <tr id="rowfile" style="display: none;">
    <td height="55"><label>Upload File: </label></td>
    <td><input type="file" id="datafile" name="datafile"></td>
  </tr>
  <tr id="rowftp" style="display: none;">
    <td colspan="2"><table border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="130"><label>FTP Hostname: </label></td>
        <td><input type="text" name="ftpserver"></td>
      </tr>
      <tr>
        <td><label>FTP User: </label></td>
        <td><input type="text" name="ftplogin"></td>
      </tr>
      <tr>
        <td><label>FTP Password: </label></td>
        <td><input type="password" id ="ftppassword" name="ftppassword"></td>
      </tr>
      <tr>
        <td><label>File Location: </label></td>
        <td><input type="text" name="ftppath"></td>
      </tr>      
          
    </tbody></table></td>
  </tr>
  <tr id="rowsftp" style="display: none;">
    <td colspan="2"><table border="0" cellspacing="0" cellpadding="0">
      <tbody><tr>
        <td width="130"><label>FTP Hostname: </label></td>
        <td><input type="text" name="ftpserver1"></td>
      </tr>
      <tr>
        <td><label>FTP User: </label></td>
        <td><input type="text" name="ftplogin1"></td>
      </tr>
      <tr>
        <td><label>FTP Password: </label></td>
        <td><input type="password" name="ftppassword1"></td>
      </tr>
      <tr>
        <td><label>File Location: </label></td>
        <td><input type="text" name="ftppath1"></td>
        <td><input type="hidden" name="sftp" id="sftp" value="true"></td>
      </tr>  
       
      <tr>
    
    </tbody></table></td>
  </tr>
    
    <tr>
    <td height="40"><label>Sync Mode</label></td>
    <td> <label><input type="radio" name="syncmode"  checked value="add" onClick="updates();" id="chk3" /> 
    Add</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio" name="syncmode" value="update" id="chk4" onClick="updates();"  /> Update</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label><input type="radio" name="syncmode"  value="addupdate"  id="chk1" onClick="updates();"  /> Add and Update</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <label><input type="radio" name="syncmode" value="delete" onClick="updates();" id="chk2"/> Delete</label> </td>
 </tr>
  <tr class="imp1">
  <td height="40">&nbsp;</td>
  <td><label><input type="radio" name="mode" checked value="overwrite" checked = 'checked' /> Overwrite</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio" name="mode" value="keep-existing"  /> Keep Existing </label></td>
 
  </tr>
  
  
    <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
<tr>
<td colspan="3" align="right"><input type="submit" name="submit" value="Save" onclick=" return importRecords();" id="submit"></td>
<td ><input type="button"  id="buttonclass"  value="Cancel" onclick="cancel();"></td>
</tr>
</tbody>
</table>
</form>
</div>
  </div>
</div>

<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>