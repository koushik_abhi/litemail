<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Partner</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>

<script>

function cancel()
{
	
	window.location="partners.jsp";
}
function submitNewPartner()
{
	
	var name = $("#name").val();
	var address = $("#address").val();
	var emailId = $("#emailid").val();
	var mobilenumber = $("#mobilenumber").val();
	 if(name == null || name =="")
	{
		alert("please enter valid partner ");
		userpartnerform.name.focus();
		return false;
	}
	if(!validatePartner(name))
     {
		return false;
     }
	if(emailId == null || emailId =="")
	{
		alert("please enter valid Email ");
		userpartnerform.emailid.focus();
		return false;
	} 
	if (!emailValidator(emailid, "Please enter Valid Email!")) {
		return false;
	}
	if(address ==null || address == "")
		{
		alert("Please Provide Address");
		userpartnerform.address.focus();
		return false;
		}
	if (mobilenumber == null || mobilenumber == "") {
		alert("Mobile Number is required!");
		userpartnerform.mobilenumber.focus();
		return false;
	}

	if (isNaN(mobilenumber)) {
		 alert("Mobile number must be Numeric value!");
		 userpartnerform.mobilenumber.focus();
		 userpartnerform.mobilenumber.value = "";
		return false;
	}

	if (mobilenumber.length<10 || mobilenumber.length>10) {
		 alert("Mobile number must be 10 Digits Only!");
		 userpartnerform.mobilenumber.focus();
		 userpartnerform.mobilenumber.value = "";
		return false;
	}
	else
		{
		document.userpartnerform.submit();
		}
	
}
	

</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="showusers.htm" class="left-nav" >Users </a>
 <a href="showpartners.htm" id="selected">Partners </a>
  <a href="showcredits.htm"> Credits</a>
  <a href="showsmtpManagement.htm"> SMTP Configuration</a>
   <a href="showemailconfig.htm"> Notification Settings</a>
 <a href="showdashboard.htm" class="right-nav"> Dashboard </a>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
<!--         <div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>
 -->        <div class="contents">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<!-- upload list -->
	<form name = "userpartnerform" action="addpartner.htm" method="get">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
 <tr>
    <td width="130"><label>Name <font style="color:red;">*</font> :</label></td>
    <td><input type="text" id = "name"  name="name" size="30" value=""></td>
  
   <td><label>Type :<br></label></td>
 	<td><input type="text" id = "type"  name="type" size="30" value=""></td>
  </tr>
 <tr>
   <td><label>Status :<br></label></td>
   <td><input type="text" id = "status"  name="status" size="30" value=""></td>
 
   <td><label>Description :<br></label></td>
   <td><input type="text" id = "description"  name="description" size="30" value=""></td>
  </tr>
<tr>
   <td><label>Comment :<br></label></td>
   <td><input type="text" id = "comment"  name="comment" size="30" value=""></td>
  
     <td><label>Email Id <font style="color:red;">*</font> :<br></label></td>
   <td><input type="text" id = "emailid"  name="emailid" size="30" value=""></td>
   
   
  </tr>
  <tr>
  <td><label>Address <font style="color:red;">*</font>:<br></label></td>
   <td><input type="text" id = "address"  name="address" size="30" value=""></td>
  
  <td><label>Business URL :<br></label></td>
   <td><input type="text" id = "url"  name="url" size="30" value=""></td>
   
  </tr>
   <tr>
   <td><label>Business Nature :<br></label></td>
   <td><input type="text" id = "bnature"  name="bnature" size="30" value=""></td>
   
   <td><label>Mobile Number <font style="color:red;">*</font> :<br></label></td>
   <td><input type="text" id = "mobilenumber"  name="mobilenumber" size="30" value=""></td>
</tr>	
   <tr>
   <td></td>
   <td colspan="3" align="right" style="padding-left: 200px"> &nbsp;<input type="submit" name="submit" value="Save" onclick=" return submitNewPartner();" id="submit">
	<input type="button"  id="buttonclass"  value="Cancel" onclick="cancel();"></td>
   <tr>
   </tr>
</tbody>
</table>
</form>
</div>
  </div>
</div>

<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>