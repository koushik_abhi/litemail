<!DOCTYPE html PUBLIC "-//W3C//DTD HTML Strict//EN">
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>User Profile</title>

<!-- Styling -->
<link rel="stylesheet" type="text/css" href="./css/style.css" media="all">
<!-- Dynamic Behaviour -->
<script src="./js/jquery.js" type="text/javascript" charset="utf-8"></script>
<script src="./js/validation.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){
	
	getEmailUserConfigDetails();
});
</script>
<script>
function getEmailUserConfigDetails()
{
	
	$.ajax({
		type: "get",
		url: "getEamilUserConfig.htm?userId=",
		dataType: "json",
		success: function( objData )
		{
			appendDataToForm(objData.result) ;
		},
		error: function(){
			alert( "An error occurred" );
		}
	});
	
}
function appendDataToForm(objData)
{
	
	var smtphost        = objData.host;
	var smtpport        = objData.port;
	var password        = objData.password;
	var to              = objData.to;
	var cc              = objData.cc;
	var smtpuser        = objData.username;
	var bcc             = objData.bcc;
	var subject         = objData.subject;
	var description     = objData.description;
	var from            = objData.fromAddress;
	var body            = objData.body;
  
	$("#smtphost").val(smtphost);
	$("#to").val(to);
	$("#smtpport").val(smtpport);
	$("#cc").val(cc);
	$("#smtpuser").val(smtpuser);
	$("#bcc").val(bcc);
	$("#password").val(password);
	$("#subject").val(subject);
	$("#from").val(from);
	$("#body").text(body);
	$("#description").text(description);
}

function cancel()
{
	
	window.location="dashboard.jsp";
}


function validate()
{
	var smtphost=$("#smtphost").val();
	var to_val = $("#to").val();
    var smtpport=$("#smtpport").val();
	var smtpuser= $("#smtpuser").val();
	var password= $("#password").val();
	var from_val= $("#from").val();
	
	if(smtphost == null || smtphost == "")
		{
		alert("Smtp Host is required!");
		document.emailconfig.smtphost.focus();
		return false;
		}
	if(smtpport == null || smtpport == "")
	{
	alert("Smtp Port is required!");
	document.emailconfig.smtpport.focus();
	return false;
	}
	if(smtpuser == null || smtpuser == "")
	{
	alert("Username is required!");
	document.emailconfig.smtpuser.focus();
	return false;
	}
	if(password == null || password == "")
	{
	alert("Password is required!");
	document.emailconfig.password.focus();
	return false;
	}
	
	if(to_val == null || to_val =="")
	{
	alert("To Address is Required!");
	return false;
	}
	if(from_val == null || from_val =="")
	{
	alert("From Address is Required!");
	return false;
	}
	
	
}

function validateupdate()
{
	document.emailconfig.action="updateemailconfig.htm";
	document.emailconfig.submit();
	
}
</script>
</head>
<body>
<div id="header"><div id="logo"> <a href="http://www.kenscio.com" target="_blank"><img src="./images/kenscio-logo.jpg" border="0" width="70"></a>
</div>
<div class="logo_text">Lite Mail</div>
<div class="header_right"><div class="welcome">Welcome <strong><%=session.getAttribute("login") %></strong>, <a href="logout.htm">Logout</a></div>
<div id="navigation">
 <a href="users.jsp" class="left-nav" >Users </a>
 <a href="partners.jsp" >Partners </a>
  <a  href="credits.jsp"> Credits</a>
  <a href="showsmtpManagement.htm"> SMTP Configuration</a>
   <a href="showemailconfig.htm" id="selected"> Notification Settings</a>
 <a href="dashboard.jsp" class="right-nav"> Dashboard </a>
</div>
</div>
</div>
	<div id="content">
	  <div id="loading-popup-box" style="display:none;"></div>
		<div id="content-main">
<!-- 		<div class="workflow"><span><img src="./images/createlist.jpg" width="115" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/campaign.jpg" width="132" height="127"></span><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/sendout.jpg" width="115" height="150"><span class="arrow"><img src="./images/arrow.jpg" width="46" height="127"></span><span><img src="./images/reports.jpg" width="101" height="127"></span></span></div>
 -->        <div class="contents">
        <div align="center" style="color:red">${errorMessage}</div><br>
	<!-- upload list -->
	<form name = "emailconfig" action="emailconfig.htm" method="post">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" id="create_list">
  <tbody>
 <tr>
    <td width="130"><label>SMTP Host:<font style="color:red;">*</font></label></td>
    <td><input type="text" id = "smtphost"  name="smtphost" size="30" value=""></td>
  
  <td><label>To :<font style="color:red;">*</font></label></td>
   <td><input type="text" id = "to"  name="to" size="30" value=""></td>
  </tr>
 <tr>
	<td><label>SMTP Port :<font style="color:red;">*</font></label></td>
   <td><input type="text" id = "smtpport"  name="smtpport" size="30" value=""></td>
 
  <td><label>Cc :<br></label></td>
 	<td><input type="text" id = "cc"  name="cc" size="30" value=""></td>
  </tr>
<tr>
	<td><label>User Name :<font style="color:red;">*</font></label></td>
 	 <td><input type="text" id = "smtpuser"  name="smtpuser" size="30" value=""></td>
    
    <td><label>Bcc :<br></label></td>
   <td><input type="text" id = "bcc"  name="bcc" size="30" value=""></td>
  </tr>
  <tr>
   <td><label>Password :<font style="color:red;">*</font></label></td>
   <td><input type="password" id = "password"  name="password" size="30" value="" style="width: 267px"></td>
  
  <td><label>Subject :<br></label></td>
   <td><input type="text" id = "subject"  name="subject" size="30" value=""></td>
  </tr>
  <tr>
   <td><label>Message Body :<br></label></td>
   <td><textarea name="body" rows="2" cols="22" style="resize: none;" id="body"></textarea></td>
   
   <td><label>From Address :<font style="color:red;">*</font></label></td>
   <td><input type="text" id = "from"  name="from" size="30" value=""></td>
   
  
  </tr>
  <tr>
  <td><label>Description:<br></label></td>
   <td><textarea name="description" rows="2" cols="22" style="resize: none;" id="description"></textarea></td>
  <td colspan="3" align="right" style="padding-left: 200px"> &nbsp;<input type="submit" name="submit" value="Save" onclick=" return validate();" id="submit">
<input type="button"  id="buttonclass"  value="Cancel" onclick="cancel();"></td>

  </tr>
  <tr><td></td>
  <td><lable><span style="margin-left:-30px;"><label>Note:all email ids are seperated with ',' (comma) only</span></label></td></tr>
</tbody>
</table>
</form>

</div>
  </div>
</div>

<div id="footer"><div id="id-footer">� 2013 Kenscio Digital Marketing Pvt. Ltd.</div></div>


</body></html>