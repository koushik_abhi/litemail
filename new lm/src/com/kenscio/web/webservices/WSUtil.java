package com.kenscio.web.webservices;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.kenscio.litemail.workflow.common.ApplicationProperty;

public class WSUtil {
	
	private static final String IMAGE_FOLDER=ApplicationProperty.getProperty("stampit.image.dir");
		
	public WSUtil(){}
	
	public static String  saveStampImage(String accountID,String campaignID,String imageName,InputStream in)
	{
      String folder=IMAGE_FOLDER+File.separator+accountID+File.separator+campaignID;
      return saveToFile(folder,imageName==null?"stampImage.gif":imageName,in);
     
	}
	public static String saveInlineImage(String accountID,String campaignID,String imageName,InputStream in)
	{
	 String folder=IMAGE_FOLDER+File.separator+accountID+File.separator+campaignID+File.separator+"inline";
	 return saveToFile(folder,imageName,in);		
	
	}
	public static void addFileToZip(ZipOutputStream out,String entryName,String file)throws Exception
	{
		ZipEntry entry=new ZipEntry(entryName);
		out.putNextEntry(entry);
		byte[]bytes=new byte[4096];
		int read;
		FileInputStream in=new FileInputStream(file);
		while((read=in.read(bytes,0,4096))!=-1)
			out.write(bytes,0,read);
		out.closeEntry();
		in.close();
	}
	public static void deleteFile(String filePath)
	{ 
	  try{
		if(filePath==null)return;
		File file=new File(filePath);
		if(file.exists())file.delete();
	  }catch(Exception e){
		  e.printStackTrace();
	  }
	}
	public static String saveToFile(String folderPath,String fileName,InputStream in)
	{
	  File folder=new File(folderPath);
	  folder.mkdirs();
	  String filePath=folderPath+File.separatorChar+fileName;
	  try {
		    File file=new File(filePath);
		    if(file.exists())file.delete();
		    file.createNewFile();
		    FileOutputStream out=new FileOutputStream(file);
			byte bytes[]=new byte[4096];
			int read=0;
			while((read=in.read(bytes,0,4096))>0)
			out.write(bytes,0,read);
			out.close();
			in.close();
		} catch (Exception e) {
		   e.printStackTrace();
		   return null;		
		}
	  return filePath;
	}
}
