package com.kenscio.web.webservices.data;

public class HtmlLink {
  private int idx;
  private String url;
  private String name;
  private String cid;
  private String urlId;
public HtmlLink(int idx,String url,String name,String cid){
  this.idx=idx;
  this.url=url;
  this.name=name;
  this.cid=cid;
}
public int getIdx() {
	return idx;
}
public void setIdx(int idx) {
	this.idx = idx;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public String getCid() {
	return cid;
}
public void setCid(String cid) {
	this.cid = cid;
}
public String getUrlId() {
	return urlId;
}
public void setUrlId(String urlId) {
	this.urlId = urlId;
}
}
