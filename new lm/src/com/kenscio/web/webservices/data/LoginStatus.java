package com.kenscio.web.webservices.data;

import com.kenscio.litemail.workflow.domain.AccountEntity;

public class LoginStatus {

	private AccountEntity account;
	private int status;
	public AccountEntity getAccount() {
		return account;
	}
	public void setAccount(AccountEntity account) {
		this.account = account;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
