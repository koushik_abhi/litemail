package com.kenscio.web.webservices.data;

public class ResourceData {
  private String resourceName;
  private String resourcePath;
  public ResourceData(String resourcePath,String resourceName)
  {
	this.resourcePath=resourcePath;
	if(resourcePath!=null&&resourceName==null){
	  int idx=resourcePath.lastIndexOf('\\'); 
	  if(idx==-1)idx=resourcePath.lastIndexOf('/');
	  if(idx!=-1)
		 resourceName=resourcePath.substring(idx+1);
	  else
		 resourcePath=resourceName;
	}else
	 this.resourceName=resourceName;
  }
 public String getResourceName() {
	return resourceName;
}
public void setResourceName(String resourceName) {
	this.resourceName = resourceName;
}
public String getResourcePath() {
	return resourcePath;
}
public void setResourcePath(String resourcePath) {
	this.resourcePath = resourcePath;
}
}
