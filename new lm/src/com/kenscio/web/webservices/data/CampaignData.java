package com.kenscio.web.webservices.data;

import java.util.ArrayList;

public class CampaignData {
   
   private String body;
   private ResourceData stampImageData;
   private ArrayList<ResourceData>resources;
   private ArrayList<HtmlLink>linksList;
   private String resourceCid;
   private long accountId;
   private long campaignId;
   private String zipFile;
   private String username; 
   private String bodyCharset;
   public CampaignData()
   {
	 resources=new ArrayList<ResourceData>();   
   }
      
 
 public String getBodyCharset() {
	return bodyCharset;
}


public void setBodyCharset(String bodyCharset) {
	this.bodyCharset = bodyCharset;
}


public String getUsername() {
	return username;
}


public void setUsername(String username) {
	this.username = username;
}


public long getAccountId() {
	return accountId;
}

public void setAccountId(long accountId) {
	this.accountId = accountId;
}

public long getCampaignId() {
	return campaignId;
}

public void setCampaignId(long campaignId) {
	this.campaignId = campaignId;
}

public String getBody() {
	return body;
 }
 
public String getResourceCid() {
	return resourceCid;
}

public void setResourceCid(String resourceCid) {
	this.resourceCid = resourceCid;
}

public void setBody(String body) {
	this.body = body;
}
public ArrayList<ResourceData> getResources() {
	return resources;
}

public ArrayList<HtmlLink> getLinksList() {
	return linksList;
}

public void setLinksList(ArrayList<HtmlLink> linksList) {
	this.linksList = linksList;
}
public void addResource(ResourceData resource)
{
  resources.add(resource);	
}
public ResourceData getStampImageData() {
	return stampImageData;
}
public void setStampImageData(ResourceData stampImageData) {
	this.stampImageData = stampImageData;
}
}