package com.kenscio.web.webservices;

public class RupeemailWSConstants {
	
   //general codes same for all operations
	public static final int SUCCESS=0;
	public static final int ERROR=1;
	public static final int LOGIN_FAILED =2;
   //create campaign...
	public static final int INSUFFICIENT_STAMP_AMOUNT=3;
    public static final int FROM_ADDRESS_ALREADY_REGISTERED=4;
    public static final int NO_ACCOUNT_FOUND=5;
    public static final int ACCOUNT_CREATION_FAILED=6;
    //generate stamp
    public static final int NO_CAMPAIGN_FOUND=7;
    public static final int STAMP_GENERATION_ERROR=8;
    //criteria check
    public static final int INSUFFICIENT_AMOUNT_IN_WALLET=9;
    //status
    public static final int NO_STATUS_AVAILABLE=10;
    public static final int IO_ERROR=11;
    //login
    public static final int INVALID_PASSWORD=12;
   //request codes
    public static final int REQ_CREATE_CAMPAIGN=1;
    public static final int REQ_ACCOUNT_BALANCE=2;
    public static final int REQ_GENERATE_STAMP=3;
    public static final int REQ_CAMPAIGN_STATUS=4; 
    public static final int REQ_CHECK_CRITERIA=5;
    public static final int REQ_DOWNLOAD_CAMPAIGN_STATUS=6;
    public static final int REQ_LOGIN=7;
    public static final int REQ_DOWNLOAD_ACCOUNT_STATUS=8;
    //mail types...
    public static final int STAMPED_MAIL=1;
    public static final int ORDINARY_MAIL=2;
	
}
