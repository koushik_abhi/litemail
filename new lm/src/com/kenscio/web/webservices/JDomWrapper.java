package com.kenscio.web.webservices;

import java.io.IOException;
import java.io.StringReader;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
/**
 * 
 * @author Biju 
 *
 */
public class JDomWrapper {

   public static Document parse(String xml){
	try {
		SAXBuilder builder=new SAXBuilder();
		return builder.build(new StringReader(xml));
	} catch (JDOMException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return null; 
   }
   public static int getIntValue(Element parent,String name)
   { 
	 if(parent==null||name==null)return -1;
	 String value=parent.getChildText(name);
	 if(value!=null){
	   try{
	       return Integer.parseInt(value);
	    }catch(NumberFormatException e){
	     return -1;
	   }
	 }
	 return -1;
   } 
   public static int getIntValue(Element parent,String name,int defaultValue){
     int value=getIntValue(parent,name);
     return value==-1?defaultValue:value;
   }
   public static long getLongValue(Element parent,String name)
   { 
	 if(parent==null||name==null)return -1;
	 String value=parent.getChildText(name);
	 if(value!=null){
	   try{
	       return Long.parseLong(value);
	    }catch(NumberFormatException e){
	     return -1;
	   }
	 }
	 return -1;
   } 
   public static long getLongValue(Element parent,String name,long defaultValue){
	   long value=getLongValue(parent,name);
	   return value==-1?defaultValue:value;
   }
   public static Element getRootElement(String xml){
	 Document doc=parse(xml);
	 if(doc==null)return null;
	 return doc.getRootElement(); 
   }
   public static String getStringValue(Element parent,String name){
	   if(parent==null||name==null)return "";
	   String value=parent.getChildText(name);
	   if(value==null)value="";
	   return value;
   }
   public static String getStringValue(Element parent,String name,String defaultValue){
	   String value=getStringValue(parent,name);
	   return value.equals("")?defaultValue:value;
   }
 }
