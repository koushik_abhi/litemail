package com.kenscio.mta.ecircle.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.codec.binary.Base64;

import com.kenscio.litemail.workflow.domain.EmailData;

public class EcircleListCreator {
	
	private String filePath;
	private static final String START="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"+
	"<userdata xmlns=\"http://webservices.ecircle-ag.com/ecm\">\n"+
    "<list>\n";
	private static final String END="</list>\n</userdata>\n";
	public EcircleListCreator(String filePath){
		this.filePath=filePath;
	}
	private void writeStart(RandomAccessFile file)throws IOException{
	   file.writeBytes(START);
	}
	
	private void writeEnd(RandomAccessFile file)throws IOException{
		file.writeBytes(END);
	}
	
	private RandomAccessFile open(){
		try {
			RandomAccessFile file=new RandomAccessFile(filePath,"rw");
			if(file.length()>=(START.getBytes().length+END.getBytes().length))
			  file.seek(file.length()-END.getBytes().length);
			else
			  file.seek(file.length());
			return file;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
		return null;
	}
	private String urlEncode(String src){
		
		try {
            return URLEncoder.encode(src,"utf-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }

	}
	private String getReplacementValues(EmailData data){
		 String replacements =null;
         if(data.getReplace1()!=null){
             replacements = data.getReplace1();
         }
         if(data.getReplace2()!=null){
             replacements +="%#%"+data.getReplace2();
         }
         if(data.getReplace3()!=null){
             replacements +="%#%"+data.getReplace3();
         }
         if(replacements!=null)
        	return  new String(Base64.encodeBase64(replacements.getBytes()));
         return null;
		
	}
	public boolean write(EmailData[]data){
	    RandomAccessFile file=open();
	    if(file==null||data==null)return false;
	    StringBuilder builder=new StringBuilder();
	    String repValues=null;
	    for(int i=0;i<data.length;i++){		            
	    	builder.append("<user>\n");
	    	builder.append("  <email>");
	    	builder.append(data[i].getEmailId());
	    	builder.append("</email>\n");
	    	builder.append("  <firstname>");
	    	builder.append(data[i].getName());
	    	builder.append("</firstname>\n");
	    	builder.append("  <campaignattr name=\"code\">");
	    	builder.append(data[i].getClientCode());
	    	builder.append("</campaignattr>\n");
	    	builder.append(" <campaignattr name=\"stamp\">");
	    	builder.append(urlEncode(data[i].getStamp()));
	    	builder.append("</campaignattr>\n");	
	    	repValues=getReplacementValues(data[i]);
	    	if(repValues!=null){
	    	   builder.append(" <campaignattr name=\"replace\">");
		       builder.append(urlEncode(repValues));
		       builder.append("</campaignattr>\n");	
	    	}else{
	    	   builder.append(" <campaignattr name=\"replace\"/>");
			}
	    	builder.append("</user>\n");
	    }
		try {
			if(file.length()<8)
			  writeStart(file);				
			file.writeBytes(builder.toString());
			writeEnd(file);
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
    public void finish(){
     RandomAccessFile file=open();	
     try {
		if(file!=null)writeEnd(file);
		 file.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   }
   
}
