package com.kenscio.mta.ecircle.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;

import javax.activation.DataHandler;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ecircle_ag.webservices.ecm.AddressType;
import com.ecircle_ag.webservices.ecm.ContentType;
import com.ecircle_ag.webservices.ecm.Control;
import com.ecircle_ag.webservices.ecm.EmailReplyToHandlingType;
import com.ecircle_ag.webservices.ecm.ExtendedEmailAddressType;
import com.ecircle_ag.webservices.ecm.HttpAddressType;
import com.ecircle_ag.webservices.ecm.SendDateType;
import com.ecircle_ag.webservices.ecm.UriReference;
import com.ecircle_ag.webservices.ecm.Control.GroupDefinition;
import com.ecircle_ag.webservices.ecm.Control.MemberList;
import com.ecircle_ag.webservices.ecm.Control.Message;
import com.ecircle_ag.webservices.ecm.GroupType.EmailChannel;
import com.ecircle_ag.webservices.ecm.GroupType.IoControl;
import com.ecircle_ag.webservices.ecm.GroupType.MsgFooter;
import com.ecircle_ag.webservices.ecm.GroupType.MsgHeader;

public class EcircleMessageBuilder {
	private static final String CONTROL_NAMESPACE = "http://webservices.ecircle-ag.com/ecm";

	public static final int VARS_XML = 1;

	public static final int VARS_CSV = 2;

	private Control control;

	private String username;

	private String password;

	private String realm;

	private String messageId = "new";

	private String messageHtml;

	private String messageText;

	private String requestId;

	private String groupId = "new";

	private String groupName;

	private String successReportAddressHttp;

	private String failureReportAddressHttp;

	private String successReportAddressEmail;

	private String failureReportAddressEmail;

	private String successReportRecipientName;

	private String failureReportRecipientName;

	private String preferredChannel = "email";

	private String isoCountry = "IN";

	private String isoLanguage = "en";

	private String groupDescription = "";

	private String htmlContentEncoding = "utf-8";

	private String textContentEncoding = "utf-8";

	private String addMode;

	private String syncMode;

	private String recipientUri = "vars.xml.gz";

	private String varsFileURL;

	private String domain = "www.rupeemail.in";

	private int varsFileType = VARS_XML;

	private String varsFileEncoding = "iso-8859-1";

	private boolean isVarsGzipped = true;
	
	private String subject="";
	
	private String fromName;
	
	private String replyToName;
	
	private String fromAddress;
	
	private String replyToAddress;
	
	private String groupEmailAddress;
    private Integer port;
    
	public EcircleMessageBuilder() {

	}

	/**
	 * 
	 * @param requestId
	 *            optional.serves as reference in reports and logfiles
	 * @param groupId
	 *            "new" for a new group.
	 * @return
	 */
    
	private Control createControl(String requestId, String groupId) {
		if (control == null)
			control = new Control();
		control.setRequestId(requestId);
		control.setGroupId(groupId);
		return control;
	}

	/**
	 * 
	 * @param preferredChannel
	 * @param name
	 * @param desc
	 * @param isoCountry
	 * @param isoLanguage
	 * @return
	 */
	private GroupDefinition createGroupDefinition(Control parent) {
		
		GroupDefinition groupDefinition = new GroupDefinition();
		groupDefinition.setPreferredChannel(preferredChannel);
		groupDefinition.setName(groupName);
		// groupDefinition.setDescription()
		groupDefinition.setIsocountry(isoCountry);
		groupDefinition.setIsolanguage(isoLanguage);
		EmailChannel em=new EmailChannel();
		em.setEmail(groupEmailAddress);
		//From
		ExtendedEmailAddressType fromAddr=new ExtendedEmailAddressType();
		fromAddr.setName(fromName);
		fromAddr.setEmail(fromAddress);
		EmailChannel.FromHandling from=new EmailChannel.FromHandling();
		from.setSpecial(fromAddr);
		em.setFromHandling(from);
		//Reply to
        EmailReplyToHandlingType replyTo=new EmailReplyToHandlingType();
        ExtendedEmailAddressType replyToAddr=new ExtendedEmailAddressType();
        replyToAddr.setName(replyToName);
        replyToAddr.setEmail(replyToAddress);
        replyTo.setSpecial(replyToAddr);
        em.setReplyToHandling(replyTo);        
	    groupDefinition.setEmailChannel(em);
		control.setGroupDefinition(groupDefinition);
     	return groupDefinition;
	}

	private MsgHeader createMsgHeader(GroupDefinition parent, String text,
			String html) {
		MsgHeader header = new MsgHeader();
		header.setText(text);
		header.setHtml(html);
		parent.setMsgHeader(header);
		return header;
	}

	private MsgFooter createMsgFooter(GroupDefinition parent, String text,
			String html) {
		MsgFooter footer = new MsgFooter();
		footer.setText(text);
		footer.setHtml(html);
		parent.setMsgFooter(footer);
		return null;
	}

	private MemberList createMemberList(Control parent) {
		MemberList memberList = new MemberList();
		parent.setMemberList(memberList);
		memberList.setAddMode(addMode);
		memberList.setSyncMode(syncMode);
		UriReference uriReference = new UriReference();
		uriReference.setGzipCompressed( isVarsGzipped);
		uriReference.setValue(recipientUri);
		memberList.setUriReference(uriReference);
		return memberList;
	}

	/**
	 * 
	 * @param sendingSpeed
	 *            speed per hour eg.10000
	 * @param bounceCount
	 *            number of bounces before the recipient is considered a bouncer
	 * @param bounceHours
	 *            hours within which bounces are to occur
	 * @return
	 */
	private IoControl createIOControl(GroupDefinition parent,
			long sendingSpeed, int bounceCount, int bounceHours) {
		IoControl ioControl = new IoControl();
		parent.setIoControl(ioControl);
		return ioControl;
	}

	private Message createMessage(Control parent, String messageId,
			SendDateType sendDate, ContentType content) {
		Message message = new Message();
		message.setMessageId(messageId);
		message.setSendDate(sendDate);
		message.setContent(content);
	
		parent.getMessage().add(message);
		return message;
	}

	private AddressType createHttpAddressType(String hostname,Integer port,String path) {
		AddressType address = new AddressType();
		HttpAddressType httpAddress = new HttpAddressType();
		httpAddress.setHostname(hostname);
		httpAddress.setPath(path);
		httpAddress.setPort(port);
		address.setHttpAddress(httpAddress);
		return address;
	}

	private AddressType createEmailAddressType(String name,String email) {
		AddressType address = new AddressType();
		ExtendedEmailAddressType emailAddress = new ExtendedEmailAddressType();
		emailAddress.setEmail(email);
		emailAddress.setName(name);
		address.setEmailAddress(emailAddress);
		return address;
	}

	private AddressType addSuccessReportAddressHttp(Control control,
			String hostname,Integer port, String path) {
		AddressType address = createHttpAddressType(hostname,port, path);
		control.getSuccessReportAddress().add(address);
		return address;
	}

	private AddressType addFailureReportAddressHttp(Control control,
			String hostname,Integer port, String path) {
		AddressType address = createHttpAddressType(hostname,port, path);
		control.getFailureReportAddress().add(address);
		return address;
	}

	private AddressType addSuccessReportAddressEmail(Control control,
			String name, String email) {
		AddressType address = createEmailAddressType(name, email);
		control.getSuccessReportAddress().add(address);
		return address;
	}

	private AddressType addFailureReportAddressEmail(Control control,
			String name, String email) {
		AddressType address = createEmailAddressType(name, email);
		control.getFailureReportAddress().add(address);
		return address;
	}

	private Document createControlDocument() {
		Control control = createControl(requestId, groupId);
		if (groupId.equals("new"))
			createGroupDefinition(control);
		// IoControl con=createIOControl(groupDefinition,1000,2,72);
		createMemberList(control);
		ContentType content = new ContentType();
		ContentType.Html html = null;
		ContentType.Text text = null;
		if (messageHtml != null) {
			html = new ContentType.Html();
			html.setTargetContentEncoding(htmlContentEncoding);
			html.setValue(messageHtml);
			content.setHtml(html);
		}
		if (messageText != null) {
			text = new ContentType.Text();
			text.setTargetContentEncoding(textContentEncoding);
			text.setValue(messageText);
			content.setText(text);
		}
	    ContentType.Subject subject=new ContentType.Subject();
        subject.setValue(this.subject);
        content.setSubject(subject);
        SendDateType sendDate = new SendDateType();
		sendDate.setImmediately("0");
		createMessage(control, messageId, sendDate, content);
		if (successReportAddressHttp != null&&!successReportAddressHttp.trim().equals(""))
			addSuccessReportAddressHttp(control, domain,port,
					successReportAddressHttp);
		if (failureReportAddressHttp != null&&!failureReportAddressHttp.trim().equals(""))
			addFailureReportAddressHttp(control, domain,port,
					failureReportAddressHttp);
		if (successReportAddressEmail != null&&!successReportAddressEmail.trim().equals(""))
			addSuccessReportAddressEmail(control, successReportRecipientName,
					successReportAddressEmail);
		if (failureReportAddressEmail != null&&!failureReportAddressEmail.trim().equals(""))
			addFailureReportAddressEmail(control, failureReportRecipientName,
					failureReportAddressEmail);
		String xml = toXml(control);
		return buildDocument(xml);
	}

	public SOAPMessage createSOAPMessage() throws Exception {
		MessageFactory msgFactory = MessageFactory.newInstance();// SOAPConstants.DYNAMIC_SOAP_PROTOCOL
		SOAPMessage message = msgFactory.createMessage();
		message.getMimeHeaders().addHeader("SOAPAction", "");
		message.getMimeHeaders().addHeader("Content-Type",
				"Multipart/Related;type=\"text/xml\";");
		SOAPPart part = message.getSOAPPart();
		SOAPEnvelope envelope = part.getEnvelope();
		envelope.getHeader().detachNode();
		SOAPFactory soapFactory = SOAPFactory.newInstance();
		SOAPHeader header = envelope.addHeader();
		Name name = soapFactory.createName("authenticate", "",
				"http://webservices.ecircle-ag.com/ws");
		SOAPHeaderElement authData = header.addHeaderElement(name);
		SOAPElement realm = authData.addChildElement("realm");
		realm.addTextNode(this.realm);// http://aghreni.cust-mta.com
		SOAPElement email = authData.addChildElement("email");
		email.addTextNode(username);// manjukg@aghreni.com
		SOAPElement password = authData.addChildElement("password");
		password.addTextNode(this.password);
		// body
		envelope.getBody().addDocument(createControlDocument());
		// attachment...
		URL url = new URL(varsFileURL);
		DataHandler handler = new DataHandler(url);
		AttachmentPart vars = message.createAttachmentPart(handler);
		vars.setContentType(varsFileType == VARS_XML ? "text/xml" : "text/csv"
				+ varsFileEncoding);
		if(isVarsGzipped)
		  vars.addMimeHeader("content-transfer-encoding","binary");
		vars.setContentId(recipientUri);
		message.addAttachmentPart(vars);
		return message;
	   }
		private String toXml(Object model) {
		String xml = "";
		try {
			/*
			 * JAXBContext jaxbContext =
			 * JAXBContext.newInstance("com.ecircle_ag.webservices.ecm");
			 */
			JAXBContext jaxbContext = JAXBContext
					.newInstance(new Class[] { com.ecircle_ag.webservices.ecm.ObjectFactory.class });
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
					new Boolean(true));
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			marshaller.marshal(model, out);
			xml = out.toString();
			// System.out.println(xml);

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml;
	}

	private Document buildDocument(String xml) {

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = docFactory.newDocumentBuilder();
			// build new document with method call and add content document to
			// it
			Document rootDoc = builder.newDocument();
			// root element that defines soap method to call and
			// ServiceNamespace
			Element elem = rootDoc.createElement("postGroupRequest");
			elem.setAttribute("xmlns", "http://webservices.ecircle-ag.com/ws");
			rootDoc.appendChild(elem);

			// build document from xml and add to root document
			Document contentDoc = builder.parse(new InputSource(
					new StringReader(xml)));

			Node replacingNode = rootDoc.importNode(contentDoc
					.getDocumentElement(), true);
			elem.appendChild(replacingNode);
			return rootDoc;
		} catch (DOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;

	}

	public String getFailureReportAddressHttp() {
		return failureReportAddressHttp;
	}

	public void setFailureReportAddressHttp(String failureReportAddressHttp) {
		this.failureReportAddressHttp = failureReportAddressHttp;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getMessageHtml() {
		return messageHtml;
	}

	public void setMessageHtml(String messageHtml) {
		this.messageHtml = messageHtml;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRealm() {
		return realm;
	}

	public void setRealm(String realm) {
		this.realm = realm;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getSuccessReportAddressHttp() {
		return successReportAddressHttp;
	}

	public void setSuccessReportAddressHttp(String successReportAddressHttp) {
		this.successReportAddressHttp = successReportAddressHttp;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIsoCountry() {
		return isoCountry;
	}

	public void setIsoCountry(String isoCountry) {
		this.isoCountry = isoCountry;
	}

	public String getIsoLanguage() {
		return isoLanguage;
	}

	public void setIsoLanguage(String isoLanguage) {
		this.isoLanguage = isoLanguage;
	}

	public String getPreferredChannel() {
		return preferredChannel;
	}

	public void setPreferredChannel(String preferredChannel) {
		this.preferredChannel = preferredChannel;
	}

	public String getGroupDescription() {
		return groupDescription;
	}

	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	public String getAddMode() {
		return addMode;
	}

	public void setAddMode(String addMode) {
		this.addMode = addMode;
	}

	public String getSyncMode() {
		return syncMode;
	}

	public void setSyncMode(String syncMode) {
		this.syncMode = syncMode;
	}

	public boolean isVarsGzipped() {
		return isVarsGzipped;
	}

	public void setVarsGzipped(boolean isVarsGzipped) {
		this.isVarsGzipped = isVarsGzipped;
	}

	public String getRecipientUri() {
		return recipientUri;
	}

	public void setRecipientUri(String recipientUri) {
		this.recipientUri = recipientUri;
	}

	public String getVarsFileURL() {
		return varsFileURL;
	}

	public void setVarsFileURL(String varsFileURL) {
		this.varsFileURL = varsFileURL;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getHtmlContentEncoding() {
		return htmlContentEncoding;
	}

	public void setHtmlContentEncoding(String htmlContentEncoding) {
		this.htmlContentEncoding = htmlContentEncoding;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMessageText() {
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getTextContentEncoding() {
		return textContentEncoding;
	}

	public void setTextContentEncoding(String textContentEncoding) {
		this.textContentEncoding = textContentEncoding;
	}

	public String getFailureReportAddressEmail() {
		return failureReportAddressEmail;
	}

	public void setFailureReportAddressEmail(String failureReportAddressEmail) {
		this.failureReportAddressEmail = failureReportAddressEmail;
	}

	public String getSuccessReportAddressEmail() {
		return successReportAddressEmail;
	}

	public void setSuccessReportAddressEmail(String successReportAddressEmail) {
		this.successReportAddressEmail = successReportAddressEmail;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFailureReportRecipientName() {
		return failureReportRecipientName;
	}

	public void setFailureReportRecipientName(String failureReportRecipientName) {
		this.failureReportRecipientName = failureReportRecipientName;
	}

	public String getSuccessReportRecipientName() {
		return successReportRecipientName;
	}

	public void setSuccessReportRecipientName(String successReportRecipientName) {
		this.successReportRecipientName = successReportRecipientName;
	}

	public String getGroupEmailAddress() {
		return groupEmailAddress;
	}

	public void setGroupEmailAddress(String groupEmailAddress) {
		this.groupEmailAddress = groupEmailAddress;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getFromName() {
		return fromName;
	}

	public void setFromName(String fromName) {
		this.fromName = fromName;
	}

	public String getReplyToAddress() {
		return replyToAddress;
	}

	public void setReplyToAddress(String replyToAddress) {
		this.replyToAddress = replyToAddress;
	}

	public String getReplyToName() {
		return replyToName;
	}

	public void setReplyToName(String replyToName) {
		this.replyToName = replyToName;
	}
   
}
