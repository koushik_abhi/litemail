package com.kenscio.mta.ecircle.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Element;

import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.web.webservices.JDomWrapper;

public class EcircleStatusServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		BufferedReader br=new BufferedReader(req.getReader());
		StringBuilder builder=new StringBuilder();
		String str;
		while((str=br.readLine())!=null){
			builder.append(str);
			if(builder.length()>10000){
			  sendError(resp,"too large");
			  return;
			}
		}
		try {
			if(processDocument(builder.toString()))
			  sendOK(resp);
			else sendError(resp,"invalid document");			    
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void sendOK(HttpServletResponse resp)throws IOException{
		resp.setStatus(200);
		PrintWriter out=resp.getWriter();
		out.print("OK");
		out.close();
	}
	private void sendError(HttpServletResponse resp,String msg)throws IOException{
		resp.sendError(500,msg);
	}
	private boolean processDocument(String xml){
		Element root=JDomWrapper.getRootElement(xml);
		if(root==null)return false;
		if(!root.getName().equals("control-result"))return false;
	    String requestId=root.getAttributeValue("request-id");
	    String processingResult=root.getAttributeValue("processing-result");//FAILURE,COMPLETE,PARTIAL
	    //TODO processing for partial...
	    IEcircleService ecircleService=(IEcircleService)AppContext.getFromApplicationContext("com.hcitek.rupeemail.mta.ecircle.service.IEcircleService");
	    if(processingResult.equalsIgnoreCase("COMPLETE"))
	      return ecircleService.updateSentList(new Long(requestId),null);//TODO changes for batch update
	    else if(processingResult.equalsIgnoreCase("FAILURE")||processingResult.equalsIgnoreCase("PARTIAL")){
	      return ecircleService.updateFailedList(new Long(requestId),null);
	    }
	    return false;
	}
    
  }
		

