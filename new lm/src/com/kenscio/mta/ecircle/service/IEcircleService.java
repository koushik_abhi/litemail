package com.kenscio.mta.ecircle.service;


import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.exception.MTAException;

public interface IEcircleService {
   public void finish();
   public boolean send(Long campaignId,EmailData []mailIds);
   public boolean createEnvelope(Campaign campaign);
   public void upload(Long campaignId,String campaignName,String subject)throws MTAException;
   public boolean updateSentList(Long campaignId,String batchFilePath);
   public boolean updateFailedList(Long campaignId,String batchFilePath);
}
