package com.kenscio.mta.ecircle.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.net.URL;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.StAXOMBuilder;

import com.hcitek.oiv.web.webservices.client.response.data.StampData;
import com.kenscio.litemail.exception.MTAException;
import com.kenscio.litemail.workflow.common.FileUtil;
import com.kenscio.litemail.workflow.common.LoggerUtil;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.common.TemplateEngine;
import com.kenscio.litemail.workflow.dao.CampaignDao;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.service.CampaignService;

import com.sun.xml.messaging.saaj.util.JAXMStreamSource;

public class EcircleService  {
	
	
	public boolean sendMessage(SOAPMessage message) {
		SOAPConnection connection=null; 
		try { 
			SOAPConnectionFactory connectionFactory = SOAPConnectionFactory
					.newInstance();
			 connection = connectionFactory.createConnection();

			SOAPMessage result = connection.call(message, new URL(
					ApplicationProperty
							.getProperty("campaign.mta.ecircle.endpoint")));
		
			if(result!=null){
			 SOAPPart soapPart =result.getSOAPPart();
			 JAXMStreamSource source = (JAXMStreamSource)soapPart.getContent();
			 InputStream is = source.getInputStream();
			 byte[] toRead = new byte[is.available()];
			 is.read(toRead);
			 String str=new String(toRead);
			 if(str.contains("submitted for processing"))return true;
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(connection!=null)connection.close();
			} catch (SOAPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	public void finish() {

	}

	public boolean createEnvelope(Campaign campaign) {
		String serverName = ApplicationProperty
				.getProperty("current.server.full.path");
		Map values = new HashMap();
		values.put("server", serverName);
		values.put("postalAddress", campaign.getPostalAddress());
		values.put("subject", campaign.getSubject());
		values.put("accId", campaign.getAccountId());
		values.put("campaignId", campaign.getCampaignId());
		values.put("imgName", campaign.getImageFileName());
		values.put("stampImg", campaign.getImageRadiobutton());
		values.put("category", campaign.getMailContentType());
		values.put("fromEmail", campaign.getFromAddress());
		values.put("fromName", campaign.getFromName());
		//values.put("stampValue", ConverterUtil.getAmountInDollarFormat(campaign
				//.getStampValue().getAmount().doubleValue()));
		String envelope = TemplateEngine.getInstnace().getEmailBody(
				"ecircleGreenEnvelope.htm", values);
				/*String envelope;
		if(!campaign.getUnSealedFlag()){	        
			envelope = templateEngine.getEmailBody(
					"ecircleGreenEnvelope.htm", values);
        }else{
        	values.put("mailBody",getCompleteMailBody(campaign.getCampaignMessage()));
        	envelope = templateEngine.getEmailBody(
					"ecircleUnsealedEnvelope.htm", values);
        }*/
		String filePath = getEnvelopeFilePath(campaign.getCampaignId());
		return saveToFile(filePath, envelope);
	}
	
	private String getCompleteMailBody(String compelteMailBody,EmailData data,String toName) {   
        if(!"".equals(toName) || toName!=null){
             compelteMailBody = compelteMailBody.replaceAll("%NAME%",toName);
         }else{
            compelteMailBody = compelteMailBody.replaceAll("%NAME%","");
         }
         compelteMailBody = compelteMailBody.replaceAll("%EMAIL%",data.getEmailId());

         if(data.getReplace1()!=null){
            compelteMailBody = compelteMailBody.replaceAll("%REPLACE1%",data.getReplace1());
         }
         if(data.getReplace2()!=null){
            compelteMailBody = compelteMailBody.replaceAll("%REPLACE2%",data.getReplace2());
         }
         if(data.getReplace3()!=null){
            compelteMailBody = compelteMailBody.replaceAll("%REPLACE3%",data.getReplace3());
         }

         String recipientUrl = ApplicationProperty.getProperty("stampit.clickthrough.recipient.link");
         compelteMailBody = compelteMailBody.replace(recipientUrl,("&email="+data.getEmailId()));

         String recipientName = ApplicationProperty.getProperty("stampit.clickthrough.name.link");
         compelteMailBody = compelteMailBody.replace(recipientName,("&name="+toName));

         return compelteMailBody;
       }

	public void upload(Long campaignId, String campaignName, String subject)
			throws MTAException { 
		// find the batch to upload...
		try {
			SOAPMessage message = prepareMessage(campaignId, campaignName,
					subject, getListFilePath(campaignId));
			if(!sendMessage(message)){
		       updateFailedList(campaignId,null);
		       throw new Exception("error uploading to ecircle");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			updateFailedList(campaignId,null);
			LoggerUtil.doLog("Failed to upload campaign-"+campaignName+":Id-"+campaignId+" to ecircle");
			throw new MTAException("error uploading to ecircle");
		}

	}

	private boolean saveToFile(String filePath, String contents) {

		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(filePath));
			out.print(contents);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
		}
		return false;
	}

	public SOAPMessage prepareMessage(Long campaignId, String campaignName,
			String subject, String listFile) throws Exception {
		EcircleMessageBuilder builder = new EcircleMessageBuilder();
		builder.setAddMode("add_no_message");
		builder.setSyncMode("add_update_mode");
		String groupEmail = ApplicationProperty
				.getProperty("campaign.mta.ecircle.group.email");
		Object vals[] = { campaignId+"_"+System.currentTimeMillis()/100000};
		MessageFormat format = new MessageFormat(groupEmail);
		groupEmail = format.format(vals);
		builder.setGroupEmailAddress(groupEmail);
		builder.setDomain(ApplicationProperty
				.getProperty("campaign.mta.ecircle.client.domain"));
		builder
				.setSuccessReportAddressHttp(ApplicationProperty
						.getProperty("campaign.mta.ecircle.client.success.report.address"));
		builder
				.setFailureReportAddressHttp(ApplicationProperty
						.getProperty("campaign.mta.ecircle.client.failure.report.address"));
		builder
				.setSuccessReportAddressEmail(ApplicationProperty
						.getProperty("campaign.mta.ecircle.client.success.report.email.address"));
		builder
				.setFailureReportAddressEmail(ApplicationProperty
						.getProperty("campaign.mta.ecircle.client.failure.report.email.address"));
		builder
				.setSuccessReportRecipientName(ApplicationProperty
						.getProperty("campaign.mta.ecircle.client.success.report.email.name"));
		builder
				.setFailureReportRecipientName(ApplicationProperty
						.getProperty("campaign.mta.ecircle.client.failure.report.email.name"));
		builder.setGroupId("new");
		builder.setGroupName(campaignName);
		builder.setMessageHtml(getEnvelope(campaignId));
		// builder.setGroupDescription("r");
		String tempPath = ApplicationProperty.getProperty("stampit.tempdir");
		tempPath += "/vars" + System.currentTimeMillis() + ".gz";
		if (gzipFile(listFile, tempPath))
			builder.setVarsFileURL("file:///" + tempPath);
		else {
			builder.setVarsGzipped(false);
			builder.setVarsFileURL("file:///" + listFile);
		}
		builder.setRequestId(String.valueOf(campaignId));// TO DO ..add
		// batch...
		builder.setSubject(subject);
		builder.setRealm(ApplicationProperty
				.getProperty("campaign.mta.ecircle.realm"));
		builder.setUsername(ApplicationProperty
				.getProperty("campaign.mta.ecircle.username"));
		builder.setPassword(ApplicationProperty
				.getProperty("campaign.mta.ecircle.password"));
		builder.setFromName(ApplicationProperty.getProperty("campaign.mta.ecircle.message.from.name"));
		builder.setFromAddress(ApplicationProperty.getProperty("campaign.mta.ecircle.message.from.address"));
		builder.setReplyToName(ApplicationProperty.getProperty("campaign.mta.ecircle.message.replyto.name"));
		builder.setReplyToAddress(ApplicationProperty.getProperty("campaign.mta.ecircle.message.replyto.address"));
		String port=ApplicationProperty.getProperty("campaign.mta.ecircle.client.port");
		if(port!=null&&!port.equals(""))
		builder.setPort(new Integer(port));
		return builder.createSOAPMessage();
	}

	private boolean gzipFile(final String sourcePath, final String destPath) {

		GZIPOutputStream out = null;
		FileInputStream in = null;
		try {
			in = new FileInputStream(sourcePath);
			out = new GZIPOutputStream(new FileOutputStream(destPath));
			byte bytes[] = new byte[2048];
			int len = 0;
			while ((len = in.read(bytes)) > 0)
				out.write(bytes, 0, len);
			out.flush();
			out.finish();
			return true;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	private String getEcircleFolder(Long campaignId) {
		String eCircleFolder = ApplicationProperty
				.getProperty("stampit.csv.home")
				+ "/ecircle/" + campaignId;
		File file = new File(eCircleFolder);
		if (!file.exists())
			if (FileUtil.createDirectory(eCircleFolder))
				return eCircleFolder;
			else
				return null;
		return eCircleFolder;
	}

	private String getListFilePath(Long campaignId) {
		String folder = getEcircleFolder(campaignId);
		if (folder == null)
			return null;
		return folder + "/" + "ecList.xml";
	}

	private String getEnvelopeFilePath(Long campaignId) {
		String folder = getEcircleFolder(campaignId);
		if (folder == null)
			return null;
		return folder + "/" + "envelope.htm";
	}

	private String getEnvelope(Long campaignId) {
		String envelopePath = getEnvelopeFilePath(campaignId);
		if (envelopePath != null) {
			String str = null;
			StringBuilder contents = new StringBuilder();
			BufferedReader br;
			try {
				br = new BufferedReader(new FileReader(envelopePath));
				while ((str = br.readLine()) != null) {
					contents.append(str);
					contents.append("\n");
				}
				return contents.toString();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	
	
	public boolean send(Long campaignId, EmailData[] mailIds) {
		if (mailIds == null || mailIds.length == 0)
			return false;
	   EcircleListCreator creator = new EcircleListCreator(
				getListFilePath(campaignId));
		creator.write(mailIds);
		return true;
	}

	public boolean updateFailedList(Long campaignId, String batchFilePath) {
		String failedFileName = ApplicationProperty
				.getProperty("failed.mails.filename.prefix")
				+ campaignId;
		//EmailListService emailListService = new EmailListService();
		String failedFileCsv ="";//emailListService.getCSVFileName(failedFileName,
				//campaignId);
		if (batchFilePath == null)
			batchFilePath = getListFilePath(campaignId);
		long updatedCount=updateStatusFile(failedFileCsv, batchFilePath);
		if(updatedCount>0){
			CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
					
		   Campaign campaign=dao.getCampaign(campaignId);
		   campaign.setMailsFailed(campaign.getMailsFailed()+updatedCount);
		   dao.updateCampaign(campaign);
		}
		return updatedCount>0;

	}

	public boolean updateSentList(Long campaignId, String batchFilePath) {
		CampaignService campaignService = new CampaignService();
		String sentFileCsv = campaignService
				.getSentEmailsFileFullPath(campaignId);
		if (batchFilePath == null)
			batchFilePath = getListFilePath(campaignId);
		long updatedCount=updateStatusFile(sentFileCsv, batchFilePath);
		if(updatedCount>0){
			CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
			Campaign campaign=dao.getCampaign(campaignId);
		   campaign.setMailsSent(campaign.getMailsSent()+updatedCount);
		   dao.updateCampaign(campaign);
		}
		return updatedCount>0;
	}

	private long updateStatusFile(String csvFilePath, String batchFilePath) {
		// axiom sTax parser...
		File file = new File(batchFilePath);
		if (!file.exists())
			return 0;
		long updatedCount=0;
		RandomAccessFile csvFile = null;
		boolean processed = false;
		try {
			csvFile = new RandomAccessFile(csvFilePath, "rw");
			csvFile.seek(csvFile.length());
			XMLStreamReader parser = XMLInputFactory.newInstance()
					.createXMLStreamReader(new FileInputStream(batchFilePath));
			StAXOMBuilder builder = new StAXOMBuilder(parser);
			OMElement documentElement = builder.getDocumentElement();
			Iterator itr = documentElement.getChildrenWithLocalName("userdata");
			StringBuilder line = new StringBuilder();
			if (itr.hasNext()) {
				OMElement element = (OMElement) itr.next();
				Iterator users = element.getChildrenWithLocalName("user");
				while (users.hasNext()) {
					OMElement user = (OMElement) users.next();
					OMElement email = user.getFirstChildWithName(new QName(
							"http://webservices.ecircle-ag.com/ecm", "email"));
					OMElement name = user.getFirstChildWithName(new QName(
							"http://webservices.ecircle-ag.com/ecm",
							"firstname"));
					line.setLength(0);
					line.append("\"");
					if (name != null)
						line.append(name.getText());
					line.append("\",\"");
					if (email != null)
						line.append(email.getText());
					line.append("\"\n");
					csvFile.writeBytes(line.toString());
                    updatedCount++;
				}
			}
			// delete the xml file
			

		} catch (XMLStreamException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (csvFile != null)
					csvFile.close();
				if (updatedCount>0) {
					if (file.exists())
						file.delete();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return updatedCount;
	}

	
}
