package com.kenscio.litemail.exception;

import com.kenscio.litemail.workflow.common.CommonException;

/**
 * Created by IntelliJ IDEA.
 * User: srikanth
 * Date: Dec 11, 2006
 * Time: 11:24:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class NoAccountFoundException extends CommonException {
    public NoAccountFoundException(String message) {
        super(message);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
