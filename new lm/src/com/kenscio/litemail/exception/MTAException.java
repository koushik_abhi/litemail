package com.kenscio.litemail.exception;

public class MTAException extends Exception {
  public MTAException(String message){
	  super(message);
  }
}
