package com.kenscio.litemail.exception;

public class CampaignException extends Exception {

    public CampaignException() {

        super();
    }

    public CampaignException(String message, Throwable throwable) {

        super(message, throwable);
    }

    public CampaignException(Throwable throwable) {

        super(throwable);
    }

}
