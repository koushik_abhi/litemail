package com.kenscio.litemail.exception;

public class StampingException extends Exception {

    public StampingException() {
    }

    public StampingException(String string) {
        super(string);
    }
    
    public StampingException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public StampingException(Throwable throwable) {
        super(throwable);
    }

}
