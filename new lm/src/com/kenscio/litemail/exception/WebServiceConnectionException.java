package com.kenscio.litemail.exception;

import com.kenscio.litemail.workflow.common.CommonException;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Aug 10, 2006
 * Time: 4:29:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class WebServiceConnectionException extends CommonException {
    public WebServiceConnectionException() {
    }

    public WebServiceConnectionException(String string) {
        super(string);
    }

    public WebServiceConnectionException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public WebServiceConnectionException(Throwable throwable) {
        super(throwable);
    }
}
