package com.kenscio.litemail.workflow.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import au.com.bytecode.opencsv.CSVReader;

import com.dataservice.platform.persistence.impl.AttributeDataDaoImpl;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.dao.AttributeDao;
import com.kenscio.litemail.workflow.domain.AttributeEntity;
import com.kenscio.litemail.workflow.domain.AttributeMap;

public class AttributeService {

	public static Logger log = Logger.getLogger(AttributeService.class.getName());
		

	public void addAttribute(AttributeEntity attributesEntity) {
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		attributeDao.addAttribute(attributesEntity);
	}

	
	public List<List<AttributeEntity>> getAttributes(long accountId) {
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		return attributeDao.getAllAttributes(accountId);
	}
	
	
	public List<AttributeEntity> getCustomAttributes(long accountId) {
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		return attributeDao.getCustomAttributes(accountId);
	}
	
	public List<String> getAllStdAttributes() {
		
		List<String> atrNames = new ArrayList<String>();
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		List<AttributeEntity> attr = attributeDao.getStdAttributes();
		if(attr != null && attr.size() > 0)
		{
			for(AttributeEntity a: attr)
			{
				atrNames.add(a.getAttrName());
			}
		}
		return atrNames;
	}
   
	public List<String> getAllUserAttributes(Long accountId) {
		
		List<String> atrNames = new ArrayList<String>();
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		List<AttributeEntity> attr = attributeDao.getCustomAttributes(accountId);
		if(attr != null && attr.size() > 0)
		{
			for(AttributeEntity a: attr)
			{
				atrNames.add(a.getAttrName());
			}
		}
		return atrNames;
	}
	
	public boolean isAttributeAlreadyExist(String attrName, Long accountId) {
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		return attributeDao.isAttributeAlreadyExist(attrName, accountId);
		
	}

	public List<String> getAllAttributesNamesOnly(long accountId) {
		List<String> atrNames = new ArrayList<String>();
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		List<AttributeEntity> attr = attributeDao.getCustomAttributes(accountId);
		if(attr != null && attr.size() > 0)
		{
			for(AttributeEntity a: attr)
			{
				atrNames.add(a.getAttrName());
			}
		}
		List<AttributeEntity> stdattr = attributeDao.getStdAttributes();
		if(stdattr != null && stdattr.size() > 0)
		{
			for(AttributeEntity a: stdattr)
			{
				atrNames.add(a.getAttrName());
			}
		}
		return atrNames;
		
	}
	
	public List<String> getAllAttributesNamesExport(long accountId) {
		List<String> atrNames = new ArrayList<String>();
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		
		List<AttributeEntity> stdattr = attributeDao.getStdAttributes();
		if(stdattr != null && stdattr.size() > 0)
		{
			for(AttributeEntity a: stdattr)
			{
				atrNames.add(a.getAttrName());
			}
		}
		
		List<AttributeEntity> attr = attributeDao.getCustomAttributes(accountId);
		if(attr != null && attr.size() > 0)
		{
			for(AttributeEntity a: attr)
			{
				atrNames.add(a.getAttrName());
			}
		}
		
		return atrNames;
		
	}


	public void deleteAttribute(long attrId) {
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		attributeDao.deleteAttribute(attrId);
	}
	
	public boolean validateCSV(HttpServletRequest request)
	{
		
		InputStream in=null;
		String filename = "";
		String listname="";
		String userName="";
				
		boolean isMultipart=ServletFileUpload.isMultipartContent(request);

		if(isMultipart)
		{
			FileItemFactory factory=(FileItemFactory) new DiskFileItemFactory();
			ServletFileUpload upload=new ServletFileUpload(factory);
			List items=null;
			
			try {
				items=upload.parseRequest(request);
			} catch (FileUploadException e) {
				//request.setAttribute("errorMessage", "File upload failed");
				return false;
			}
			Iterator itr=items.iterator();
			while(itr.hasNext())
			{
				FileItem item=(FileItem) itr.next();
				filename=item.getName();
				try {
					in = item.getInputStream();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		request.setAttribute("listname", listname);
    	List<String> userHeaders = new ArrayList<String>();
        List<String> dbHeaders =new ArrayList<String>();
       // if(!readDataAndPopulateHeadersRunAsync(userHeaders,filename, in, userName,dbHeaders))
        {
        	//request.setAttribute("errorMessage", "File header names are not matching with the created attributes in "+filename );
        	return true;
        }
		
		
	}
		
	public boolean validateMailListHeaders(String filePath, long accountId)
	{
		
		CSVReader reader=null;
        String  nextLine[]=new String[2000];
        File file = null;
        List<String> userHeaders = new ArrayList<String>();
        List<String> dbHeaders = new ArrayList<String>();
        try
        {
        	 InputStreamReader fr =  null;
			 file = new File(filePath);
	         fr = new InputStreamReader(new FileInputStream(file));
	         reader = new CSVReader(fr);
	         int firstRow = 0;
	         if ((nextLine = reader.readNext()) != null)
	         {
	           
	           if(firstRow ==0)
	           {
	        	  int c = 0;
	         	  for(int nIndex = 0; nIndex <nextLine.length;nIndex++)
	              {
	                  //System.out.print(nextLine[nIndex]+" ");
	                  if(nextLine[nIndex].length() > 0)
	                  {
	                	  String head = nextLine[nIndex].trim();
	                	//  String low = head.toLowerCase();
	                	  //if(head != null && head != "" && !low.contains("mail"))
	                	  {
	                		  userHeaders.add(head);
	                	  }
	                  }
	                  c++;
	                  if(c >=50)
	                	  break;
	         		  
	              }
	         	 
	
	           }
	           
	         }
	       	         
	         //comparing user attributes with dbattributes
	         AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
	         List<AttributeEntity> dbattributes=attributeDao.getCustomAttributes(accountId);
	         for(int i=0;i<dbattributes.size();i++)
	         {
	        	dbHeaders.add(dbattributes.get(i).getAttrName()); 
	        	//System.out.println(dbattributes.get(i).getAttrName());
	         }

	         if(dbHeaders.containsAll(userHeaders))
	         {
	        	 return true;
	         }
	         else
	         {
	        	 return false;
	         }
	         
        } catch (IOException e) {
			
        	System.out.println("error in compare headers with attributes "+ e.getMessage());
		}
		return false;
      
		
	}

	public void addAttributeMap(String attrName, String dbAttr, long listId) {
		AttributeDataDaoImpl dao = new AttributeDataDaoImpl();
		dao.addAttributeMap(attrName, dbAttr, listId);
	}
	
	public Map<String, String> getListAttributesMapped(long listId) {
		AttributeDataDaoImpl dao = new AttributeDataDaoImpl();
		return dao.getListAttributesMapped(listId);
	}
	public List<String> getAllCustAttributes(long accountId,Long listId1){
		   System.out.println("In Service Class get custom attribues");
		   List<String> atrNames = new ArrayList<String>();
		  // AttributeDataDaoImpl dao = new AttributeDataDaoImpl();
		   AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
			List<AttributeMap> attr = attributeDao.getCustAttributes(listId1);
			if(attr != null && attr.size() > 0)
			{
				for(AttributeMap a: attr)
				{
					atrNames.add(a.getAttributeName());
					System.out.print("custom "+a.getAttributeName());
				}
			}/*for (int i = 0; i < attr.size(); i++) {
				System.out.println(attr.get(i));
			}*/
			return atrNames;
	   }

	

    public List<AttributeEntity> getAttributesByPartnerName(String partnerName, int pageNumber, int noOfRecordsperPage) {
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		return attributeDao.getAttributesByPartnerName(partnerName, pageNumber, noOfRecordsperPage);
	}


	public List<AttributeEntity> getStdAttributes() {
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		return attributeDao.getStdAttributes();
	}


	public int getTotalCustAttributes(String partnerName) {
		AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
		return attributeDao.getTotalCustAttributes(partnerName);
	}

	

	
}
