package com.kenscio.litemail.workflow.service;

import java.util.List;

import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.dao.LoginDao;
import com.kenscio.litemail.workflow.domain.AccountEntity;

public class LoginServiceImpl {

	public Long login(String userName,String password)
	{
	 
	  long accountId = 0;
	  LoginDao  dao = (LoginDao) AppContext.getFromApplicationContext("loginDao");
	  List<AccountEntity> accounts= dao.getAccounts(userName,password);
	  if(accounts.size()== 0)
	  {
		  return (long) 0;
	  }
	  else
	  {
		  for (int i = 0; i < accounts.size(); i++)
		  {
			  accountId=accounts.get(i).getAccountId();
		  }
		  return accountId;
	  
	  }
	  
	}
	
}
