package com.kenscio.litemail.workflow.service;

import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.util.ThreadPoolExecutorService;

public class BounceSpamAsyncServiceExecutor {
	private Campaign  campaign = null;
	public BounceSpamAsyncServiceExecutor()
	{
		
	}
	public BounceSpamAsyncServiceExecutor(Campaign campaign)
	{
		
	}
	public void callAsyncServiceForBounceSpams(Campaign campaign)
	{
		BounceSpamThread thread = new BounceSpamThread(campaign);
		ThreadPoolExecutorService.getInstance().execute(thread);
		
	}
	public Campaign getCampaign() {
		return campaign;
	}
	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

}
