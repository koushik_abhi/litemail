package com.kenscio.litemail.workflow.service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jcraft.jsch.UserInfo;

/**
* User info for silent operation.
* That is no password/passphrase prompt possible.
*
*/
public final class SshUserInfo implements UserInfo {

/** Our log. */
private static final Logger LOG = 
LoggerFactory.getLogger(SshUserInfo.class
.getName());

/** The password. */
private final String password;
/** True, if the password or passphrase has been retrieved at least 
once. */
private boolean secretDelivered = false;
/** The keyFile. */
private final String keyFile;
/** The passphrase needed to unlock the keyFile. */
private final String passphrase;

/**
* Create UserInfo for password authentication.
*
* @param password Password of the remote user.
*/
public SshUserInfo(final String password) {

super();
LOG.debug("SSHUserInfo(********)");
this.password = password;
this.keyFile = null;
this.passphrase = null;
}

/**
* Create UserInfo for public key/passphrase authentication.
*
* @param keyFileName File containing the users's private key.
* @param passphrase Passphrase securing the keyfile.
*/
public SshUserInfo(final String keyFileName, final String passphrase) {

super();
LOG.debug("SSHUserInfo(" + keyFileName + ", ********)");
this.password = null;
this.keyFile = keyFileName;
this.passphrase = passphrase;
}

/** {@inheritDoc} */
public String getPassphrase() {

LOG.debug("getPassphrase()=********");
secretDelivered = true;
return passphrase;
}

/** {@inheritDoc} */
public String getPassword() {

LOG.debug("getPassword()=********");
secretDelivered = true;
return password;
}

/** {@inheritDoc} */
public boolean promptPassword(final String message) {

// Tell Jsch prompting for PW was successful the first time, but
// failed if asked more than once (the first pw did not work, we
// got only one.
LOG.debug("promptPassword(" + message + ")=" + !secretDelivered);
return !secretDelivered;
}

/** {@inheritDoc} */
public boolean promptPassphrase(final String message) {


// got only one).
LOG.debug("promptPassphrase(" + message + ")=" + !secretDelivered);
return !secretDelivered;
}

/** {@inheritDoc} */
public boolean promptYesNo(final String message) {

LOG.debug("promptYesNo(" + message + ")=true");
// Used, if known hosts check failed (i.e. host key not known).
// Answer means: continue anyways
return true;
}

/** {@inheritDoc} */
public void showMessage(final String message) {

LOG.debug("showMessage(" + message + ")");
}

/**
* @return the key file.
*/
public String getKeyFile() {

LOG.debug("getKeyFile()=" + keyFile);
return keyFile;
}

}