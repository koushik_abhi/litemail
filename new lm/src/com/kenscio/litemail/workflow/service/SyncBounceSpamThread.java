package com.kenscio.litemail.workflow.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;

import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.dao.CampaignDao;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.EmtsResponse;
import com.kenscio.litemail.workflow.domain.EmtsResponseBounce;
import com.kenscio.litemail.workflow.domain.EmtsResponseSpam;
import com.kenscio.litemail.workflow.domain.MessageBounce;
import com.kenscio.litemail.workflow.domain.MessageSpam;


public class SyncBounceSpamThread extends Thread{

	private static final long bounceWaitTime = 15*60*1000; 
	public SyncBounceSpamThread()
	{
		
	}
	
	public void run() {
		
		while(true)
		{
			
			try {
				getBouncesSpamsFromEmtsAndSyncUp();
				Thread.currentThread().sleep(bounceWaitTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
						
		}
		
	}
	
	public EmtsResponseSpam getEmtsResponseSpam(String url) 
	{
		HttpClient client = new DefaultHttpClient();
		EmtsResponseSpam emtsObject = new EmtsResponseSpam() ;
		try{
		 
		  HttpGet request = new HttpGet(url);
		  HttpResponse response = client.execute(request);
		  ObjectMapper wrapper = new ObjectMapper();
		  SimpleDateFormat dateFormat =  new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
		  wrapper.getSerializationConfig().setDateFormat(dateFormat);
		  wrapper.getDeserializationConfig().setDateFormat(dateFormat); 
		  emtsObject = wrapper.readValue(response.getEntity().getContent(), EmtsResponseSpam.class);
		  client.getConnectionManager().shutdown();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			 client.getConnectionManager().shutdown();
		}
		return emtsObject;
		
	}
	
	public EmtsResponseBounce getEmtsResponseBounce(String url) 
	{
		HttpClient client = new DefaultHttpClient();
		EmtsResponseBounce emtsObject = new EmtsResponseBounce() ;
		try{
		 
		  HttpGet request = new HttpGet(url);
		  HttpResponse response = client.execute(request);
		  ObjectMapper wrapper = new ObjectMapper();
		  SimpleDateFormat dateFormat =  new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
		  wrapper.getSerializationConfig().setDateFormat(dateFormat);
		  wrapper.getDeserializationConfig().setDateFormat(dateFormat); 
		  emtsObject = wrapper.readValue(response.getEntity().getContent(), EmtsResponseBounce.class);
		  client.getConnectionManager().shutdown();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			 client.getConnectionManager().shutdown();
		}
		return emtsObject;
		
	}

	private void getBouncesSpamsFromEmtsAndSyncUp() 
	{
		
		String bounceurl = "ViewMailAllBounceApiPagination.htm";
		String spamurl = "ViewMailAllSpamsApiPagination.htm";
		
		String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		
		CampaignDao edao =(CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		
		boolean bflg = true;
		boolean sflg = true;
		long recId = edao.getMaxBounceRecId();	
		while(bflg)
		{
					
			String burl=baseUrl+"/"+bounceurl+"?pId="+platFormId+"&recId="+recId+"&recSize=500";
					
			EmtsResponseBounce obj= getEmtsResponseBounce(burl);
		
			if(obj != null)
			{
				List<MessageBounce> recs = obj.getItems();
				if(recs != null && recs.size() > 0)
			    {
					edao.addBulkBounces(recs);
			    	
			    }else
			    {
			    	bflg = false;
			    }
			}
			else
			{
				bflg = false;
			}
			recId = recId+500+1;
		}
		long srecId = edao.getMaxSpamRecId();
		while(sflg)
		{
			String surl=baseUrl+"/"+spamurl+"?pId="+platFormId+"&recId="+srecId+"&recSize=200";
			EmtsResponseSpam objs= getEmtsResponseSpam(surl);
			
			if(objs != null)
			{
			    List<MessageSpam> recss = objs.getItems();
			    if(recss != null && recss.size() > 0)
			    {
			    	edao.addBulkSpams(recss);
			    	
			    }else
			    {
			    	sflg = false;
			    }
			}
			else
			{
				sflg = false;
			}
			srecId = srecId+200+1;
		}
		
	}

	
}
