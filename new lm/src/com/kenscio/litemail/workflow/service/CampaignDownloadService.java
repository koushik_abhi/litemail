package com.kenscio.litemail.workflow.service;

import java.util.Date;

import au.com.bytecode.opencsv.CSVWriter;

import com.kenscio.litemail.workflow.dao.CampaignDao;
import com.kenscio.litemail.workflow.common.AppContext;

public class CampaignDownloadService {

	public void exportToExcelMailFailed(CSVWriter writer, Long campaignID,
			long accountId) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelMailFailed(writer,campaignID,accountId);
		
	}

	public void exportToExcelMailBounces(CSVWriter writer, Long campaignID,
			long accountId, String bounceType,String sp,String dm) {
		
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelMailBounces(writer,campaignID,accountId,bounceType,sp,dm);
	}

	public void exportToExcelMailBouncesDateFilter(CSVWriter writer,
			Long campaignID, long accountId, String bounceType,
			Date fromDate, Date toDate,String sp,String dm) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelMailBouncesDateFilter(writer,campaignID,accountId,bounceType,fromDate,toDate,sp,dm);
	}

	public void exportToExcelMailSpams(CSVWriter writer, Long campaignID,
			long accountId,String sp,String dm) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelMailSpams(writer,campaignID,accountId,sp,dm);
	}

	public void exportToExcelMailSpamsDateFilter(CSVWriter writer,
			Long campaignID, long accountId, Date fd, Date td,String sp,String dm) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelMailSpamsDateFilter(writer,campaignID,accountId,fd,td,sp,dm);
		
	}

	public void exportToExcelMailFailedDateFilter(CSVWriter writer,
			Long campaignID, long accountId, Date fromDate, Date toDate) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelMailFailedDateFilter(writer, campaignID, accountId, fromDate, toDate);
		
	}

	public void exportToExcelBouncesByDomain(CSVWriter writer,String bounceType, String dm,
			String sp, Date cstime, Date cendTime) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelBouncesByDomain(writer,bounceType,dm,sp,cstime,cendTime);
	}

	public void exportToExcelSpamsByDomain(CSVWriter writer, String dm,
			String sp, Date cstime, Date cendTime) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
        dao.exportToExcelSpamsByDomain(writer,dm,sp,cstime,cendTime);	
	}

	public void exportToExcelUnsubscribeMail(CSVWriter writer,
			String campaignId, long accountId) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelUnsubscribeMail(writer,campaignId,accountId);
	}

	public void exportToExcelUnsubscribeMail(CSVWriter writer, Long campaignID,
			long accountId, Date fd, Date td) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelUnsubscribeMail(writer, campaignID,accountId,fd,td);
	}
	
	public void exportToExcelMailSentSoFar(CSVWriter writer, String campaignId,
			long accountId) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelMailSentSoFar(writer,campaignId,accountId);
		
	}

	public void exportToExcelMailSentSoFar(CSVWriter writer, Long campaignID,
			long accountId, Date fd, Date td) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.exportToExcelMailSentSoFar(writer,campaignID,accountId,fd,td);
	}


}
