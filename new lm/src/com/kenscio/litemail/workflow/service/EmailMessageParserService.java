package com.kenscio.litemail.workflow.service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.kenscio.litemail.workflow.domain.AttributeEntity;
import com.kenscio.litemail.workflow.domain.ListDataEntity;
import com.kenscio.litemail.workflow.common.DateUtil;

public class EmailMessageParserService 
{
	public static Logger log = Logger.getLogger(EmailMessageParserService.class.getName());	
	public EmailMessageParserService()
	{
		
	}
	
	
	public static void main(String arg[])
	{
		String bodyMessage ="%[hi how r u, i am working] as %[firstname]% so how r u %[lastname]%";
		bodyMessage = bodyMessage.replace("%[firstname]%", "venu");
		System.out.println("mesage "+ bodyMessage);
		List<String> bodyAttributes= new ArrayList<String>();
		String[] splitString = bodyMessage.split("%");
		EmailMessageParserService e = new EmailMessageParserService();
		for(int i= 0; i< splitString.length; i++)
		{
			
			String token = splitString[i];
			//System.out.println(" tt " + token);
			if(token != null && token.length() > 0 && token.startsWith("[") && token.endsWith("]"))
			{
				
				token = token.substring(1);
				token = token.substring(0, token.length()-1);
				bodyAttributes.add(e.getTokenNameFromCondition(token).trim());				
				
			}
			
			
		}
		
		System.out.println("attributes are "+ bodyAttributes);


		
	}
	
	public List<String> validateAttributesExistence(String bodyMessage, long accountId)
	{
		List<String> bodyAttributes= new ArrayList<String>();
		String[] splitString = bodyMessage.split("%%");
		System.out.println(splitString [0]);
		System.out.println(splitString [1]);
		
		
		return bodyAttributes;
		
		
	}
	
	
	public List<String> validateForAttributesInBodyMessage(String bodyMessage, long accountId)
	{
		
		List<String> bodyAttributes= new ArrayList<String>();
		String[] splitString = bodyMessage.split("%");
		for(int i= 0; i< splitString.length; i++)
		{
			
			String token = splitString[i];
			if(token != null && token.length() > 0 && token.startsWith("[") && token.endsWith("]"))
			{
				
				token = token.substring(1);
				token = token.substring(0, token.length()-1);
				bodyAttributes.add(getTokenNameFromCondition(token).trim());				
				
			}
			
			
		}

		if(bodyAttributes != null && bodyAttributes.size() > 0)
		{
			AttributeService service = new AttributeService();
			List<String> dbattrNames = service.getAllAttributesNamesOnly(accountId);
		
		
			//System.out.println("dbattributes "+ dbattrNames );
			bodyAttributes.removeAll(dbattrNames);
			return bodyAttributes;
			
		}
		return bodyAttributes;
				
		
	}	
	
	public List<String> validateForAttributesInBodyMessage_old(String bodyMessage, long accountId)
	{
		
		List<String> bodyAttributes= new ArrayList<String>();
				
		StringTokenizer tokens = new StringTokenizer(bodyMessage, "$$");
		int i = tokens.countTokens();
		int count = 0;
		while(tokens.hasMoreTokens())
		{
			if(count ==0 )
			{
				String tken1  = tokens.nextToken();
				if(i == 1 && bodyMessage.startsWith("$$") && tken1 != null && tken1.trim() != "")
				bodyAttributes.add(tken1.trim());
				//builder.append(tokens.nextToken());
				count++;
				continue;
				
			}
			if(count == 1)
			{
				String token = tokens.nextToken();
				
				String tokenName = getTokenNameFromCondition(token);
				if(tokenName != null || tokenName != "")
				bodyAttributes.add(tokenName.trim());
				//builder.append(value);
				count =0;
			}
			
			
		}
		if(bodyAttributes != null && bodyAttributes.size() > 0)
		{
			AttributeService service = new AttributeService();
			List<String> dbattrNames = service.getAllAttributesNamesOnly(accountId);
		
		
			//System.out.println("dbattributes "+ dbattrNames );
			bodyAttributes.removeAll(dbattrNames);
			return bodyAttributes;
			
		}
		return bodyAttributes;
				
		
	}	
	private String getTokenNameFromCondition(String token ) {
		if(token != null && token.contains("||"))
		{
			StringTokenizer tokns = new StringTokenizer(token, "||");
			if(tokns.hasMoreTokens())
			{
				String priAttName = tokns.nextToken();
				
				String defaultAttVal = tokns.nextToken();
				return priAttName;
			}
			
		}
		else if(token != null && token.contains("@@"))
		{
			StringTokenizer tokns = new StringTokenizer(token, "@@");
			if(tokns.hasMoreTokens())
			{
				String priAttName = tokns.nextToken();
			
				String format = tokns.nextToken();
				return priAttName;
				
			}			
			
			
		}
		return token;
		
		
		
	}
	
	public String parseBodyMessageAndUpdateWithActualValues(String bodyMessage, long campaignId, long listId, String email)
	{
	
		Map<String, String> attridata =  getAttributesDataMap(email, listId);
		
		List<String> bodyAttributes= new ArrayList<String>();
		
		String builder=bodyMessage;
				
		String[] splitString = bodyMessage.split("%");
		Map<String, String> keyvalues = new HashMap<String, String>();
		for(int i= 0; i< splitString.length; i++)
		{
			
			String token = splitString[i];
			if(token != null && token.length() > 0 && token.startsWith("[") && token.endsWith("]"))
			{
				
				token = token.substring(1);
				token = token.substring(0, token.length()-1);
				String name = getTokenNameFromCondition(token).trim();
				
				bodyAttributes.add(name);	
												
				String value = getValueFromNameOrFromCondition(token, campaignId, listId, email, attridata);
				
				String replace = "%["+name.trim()+"]%";
				keyvalues.put(replace, value);
				
				//bodyMessage.replaceAll(replace, value);
						
				
				
			}
			
			
		}
		
	
		Set<String> keys = keyvalues.keySet();
		for(String k : keys)
		{
			//System.out.println(" keyvalues value contains  "+ builder.contains(k));
			builder = builder.replace(k, keyvalues.get(k));
		}
		
		return builder;
		
		
	}
	
	public String parseBodyMessageAndUpdateWithActualValues_NEW(String bodyMessage, long campaignId, long listId, String email)
	{
	
		Map<String, String> attridata =  getAttributesDataMap(email, listId);
		
		List<String> bodyAttributes= new ArrayList<String>();
		StringBuilder builder= new StringBuilder();
		String[] splitString = bodyMessage.split("%");
		for(int i= 0; i< splitString.length; i++)
		{
			
			String token = splitString[i];
			if(token != null && token.length() > 0 && token.startsWith("[") && token.endsWith("]"))
			{
				
				token = token.substring(1);
				token = token.substring(0, token.length()-1);
				bodyAttributes.add(getTokenNameFromCondition(token).trim());	
				
				String value = getValueFromNameOrFromCondition(token, campaignId, listId, email, attridata);
				
				//System.out.println("bodyMessage token "+ token + "value " +value);
				if(value != null && value.length() > 0)
				{
					builder.append(value);
				}
				else
				{
					builder.append(token);
				}
				
				
			}
			else
			{
				builder.append(token);
			}
			
			
		}
		
		
		String str = builder.toString();
		System.out.println("return string token "+ str);
		if(str != null && str.length() > 0)
		{
			
			return str;
		}
		else
			return "";
		
		
	}
	
	
	public String parseBodyMessageAndUpdateWithActualValues_old(String bodyMessage, long campaignId, long listId, String email)
	{
		//System.out.println("bodyMessage "+ bodyMessage);
		StringBuilder builder= new StringBuilder();
		StringTokenizer tokens = new StringTokenizer(bodyMessage, "$$");
		int i  = tokens.countTokens();
		
		int count = 0;
		Map<String, String> attridata =  getAttributesDataMap(email, listId);
		
		
		while(tokens.hasMoreTokens())
		{
			if(count ==0 )
			{
				String token = tokens.nextToken();
				
				String value = getValueFromNameOrFromCondition(token, campaignId, listId, email, attridata);
				
				//System.out.println("bodyMessage token "+ token + "value " +value);
				if(value != null && value.length() > 0)
				{
					builder.append(value);
				}
				else
				{
					builder.append(token);
				}
				count++;
				continue;
				
			}
			if(count == 1)
			{
				String token = tokens.nextToken();
				
				String value = getValueFromNameOrFromCondition(token, campaignId, listId, email, attridata);
				
				//System.out.println("bodyMessage token "+ token + "value " +value);
				if(value != null && value.length() > 0)
				{
					builder.append(value);
				}
				else
				{
					builder.append(token);
				}
				count =0;
			}
			
			
		}
		String str = builder.toString();
		//System.out.println("return string token "+ str);
		if(str != null && str.length() > 0)
		{
			
			return str;
		}
		else
			return "";
		
		
	}
	
	private String getValueFromNameOrFromCondition(String token, long campaignId, long listId , String email, Map<String, String> attridata ) {
		if(token != null && token.contains("||"))
		{
			StringTokenizer tokns = new StringTokenizer(token, "||");
			if(tokns.hasMoreTokens())
			{
				String priAttName = tokns.nextToken();
				String defaultAttVal = tokns.nextToken();
				System.out.println("priAttName "+priAttName);
				System.out.println("defaultAttVal "+defaultAttVal);
				String priAttValue = getValueForAttribute(priAttName, email, campaignId, listId, null, attridata );
				if(priAttValue != null && priAttValue.length() > 0)
				{
					return priAttValue;
				}else
				{
					return defaultAttVal;
				}
			}
			
		}
		else if(token != null && token.contains("@@"))
		{
			StringTokenizer tokns = new StringTokenizer(token, "@@");
			if(tokns.hasMoreTokens())
			{
				String priAttName = tokns.nextToken();
				String format = tokns.nextToken();
				String priAttValue = getValueForAttribute(priAttName, email, campaignId, listId, format, attridata );
				if(priAttValue != null && priAttValue !="")
				{
					return priAttValue;
				}else
				{
					return priAttName;
				}
			}			
			
			
		}
		else if(token != null || token !="")
		{
			String attValue = getValueForAttribute(token, email, campaignId, listId, null, attridata );
			return attValue;
		}
		return token;
		
	}
	
	private String getValueForAttribute(String attrName, String email,
			long campaignId, long listId, String format, Map<String, String> attridata ) {
	//	MailListServiceImpl impl = new MailListServiceImpl();
	//	ListDataEntity listData = impl.getAttributeDataForListAttribute(listId, email, attrName);
		if(format != null && format.length() >0)
		{
			Date date = new Date();
			try{	
				date = DateUtil.getLocalDateFromString(attridata.get(attrName));
			}catch(Exception e)
			{
				
			}
			return DateUtil.formatDateForFormat(date, format);
					
			
		}else
		{
			return attridata.get(attrName);
		}
				
	}
	
	private Map<String, String> getAttributesDataMap(String email, long listId)
	{
		MailListServiceImpl impl = new MailListServiceImpl();
		return impl.getAttributesDataMap(email, listId);
	}
	private String getValueForAttribute(String attrName, String email,long campaignId, long listId) {
		MailListServiceImpl impl = new MailListServiceImpl();
		ListDataEntity listData = impl.getAttributeDataForListAttribute(listId, email, attrName);
		if(listData != null)
			return listData.getAttributeValue();
		else
			return attrName;
		
	}

	public String parseLinksAndUpdateWithActualValues(String url, long campaignId, long listId, String email)
	{
		StringBuilder builder= new StringBuilder();
		StringTokenizer tokens = new StringTokenizer("$$");
		int count = 0;
		while(tokens.hasMoreTokens())
		{
			if(count == 0 )
			{
				builder.append(tokens.nextToken());
				count++;
				continue;
				
			}
			if(count == 1)
			{
				String token = tokens.nextToken();
				String value = getValueFromNameOrFromCondition(token, campaignId, listId, email, null);
				builder.append(value);
				count =0;
			}
			
			
		}
		return builder.toString();
		
	}
	
	public String convertHtmlBodyMessageToText(String htmlMessage,long campaignId,  long listId)
	{
		return "";
	}
	
	
}
