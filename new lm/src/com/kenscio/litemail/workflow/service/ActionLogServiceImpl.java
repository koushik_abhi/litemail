package com.kenscio.litemail.workflow.service;

import java.util.ArrayList;
import java.util.List;

import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.dao.ActionDaoImpl;
import com.kenscio.litemail.workflow.dao.MailListDao;
import com.kenscio.litemail.workflow.domain.ActionLogObject;


public class ActionLogServiceImpl {

	public void addActionLog(ActionLogObject obj) {

		ActionDaoImpl  imp = (ActionDaoImpl) AppContext.getFromApplicationContext("actionDao");
		imp.saveAction(obj);
		
	}

	public void updateActionLog(ActionLogObject obj) {

		ActionDaoImpl  imp = (ActionDaoImpl) AppContext.getFromApplicationContext("actionDao");
		imp.updateAction(obj);
		
	}
	
	public ActionLogObject[] getAllActionLogs() {

		ActionDaoImpl  imp = (ActionDaoImpl) AppContext.getFromApplicationContext("actionDao");
		return imp.getAllActionLogObjects();
			
	}
	
	
	public ActionLogObject getActionLog(long listId) {

		ActionDaoImpl  imp = (ActionDaoImpl) AppContext.getFromApplicationContext("actionDao");
		return imp.getActionLogObject(listId);
			
	}
	
	public ActionLogObject getActionLogOnId(long actionId) {

		ActionDaoImpl  imp = (ActionDaoImpl) AppContext.getFromApplicationContext("actionDao");
		return  imp.getActionLogOnId(actionId);
		
		
	}
	
	
	public List<ActionLogObject> getActionLogsOnUser(String userId) {

		ActionDaoImpl  imp = (ActionDaoImpl) AppContext.getFromApplicationContext("actionDao");
		return imp.getActionLogsOnUser(userId);
		
	}
	
	public void deleteactionlog(List<String> deleteList, String userName) {
		ActionDaoImpl  imp = (ActionDaoImpl) AppContext.getFromApplicationContext("actionDao");
		imp.deleteactionlog(deleteList, userName);
	}

	public ActionLogObject getActionLogByActionSource(String actionSource) {
		ActionDaoImpl  imp = (ActionDaoImpl) AppContext.getFromApplicationContext("actionDao");
		return imp.getActionLogByActionSource(actionSource);
	}
	
	
}
