package com.kenscio.litemail.workflow.service;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;



public class FTPFileManagementImpl 
{

	public static Logger log = Logger.getLogger(FTPFileManagementImpl.class.getName());	
	
	public Map<String, String> downloadFile(String host, String username, String password, String filelocation) throws IOException {
	
		Map<String, String> filenamepath = new HashMap<String, String>();
		FTPClient client = new FTPClient(); 
	    FileOutputStream fos = null; 
	    File file = null;
	    int in = filelocation.lastIndexOf("/");
	    String fileName = "";
	    if(in > 0)
	    {
	    	fileName = filelocation.substring(in+1);
	    	int inf = fileName.indexOf(".");
	    	fileName = fileName.substring(0, inf);
	    }
	    try {
	    	
	    	if(isHostLocalServer(host))
	    	{
	    		file = new File(filelocation);
	    		filenamepath.put(fileName, file.getAbsolutePath());
	    		return filenamepath;
	    		
	    	}
	    	client.connect(host);
	    	boolean login = client.login(username, password);
	    	
	    	System.out.println("login " +login + " username "+username );
	    	
	    	 String[] replies = client.getReplyStrings();
	    	 if (replies != null && replies.length > 0)
	    	 {
	    		 for (String aReply : replies) 
	    		 {
	    			 System.out.println("SERVER: " + aReply);
	    		 }
	    	 }
	    	if(!login)
	    		return filenamepath;
	    	client.setFileType(FTP.BINARY_FILE_TYPE);
	    	client.enterLocalPassiveMode();
	    	client.setAutodetectUTF8(true);
	    	
	    	//fileName = fileName+"_"+time+".csv";
			file = File.createTempFile(fileName,".csv");
			System.out.println("ftp file downlading with name "+ file.getName() + " remote filelocation " +filelocation);
			//client.retrieveFileStream(filelocation);
			
	        //file = new File(DataServiceUtil.getContextPath()+File.separator+fileName);
	        fos = new FileOutputStream(file); 
	        boolean falg = client.retrieveFile(filelocation, fos);
	        if(falg)
	        {
	        	 filenamepath.put(file.getName(), file.getAbsolutePath());	        	
	        }
	        

	    } catch (IOException e) 
	    { 
	    	  e.printStackTrace();
	    	  log.log(Level.SEVERE, "unable to download file from ftp server "+ e.getMessage()+ " file location :" +filelocation);
	    	  throw e;
	      
	    } finally { 
	      try { 
	        if (fos != null) { 
	          fos.close(); 
	        } 
	        	client.disconnect(); 
	      } catch (IOException e) {
	    	  
	    	  log.log(Level.SEVERE, "unable to disconnect ftp server "+ e.getMessage());
	      } 
	    }
	   
		return filenamepath;
	  
     
	}
	
	private boolean isHostLocalServer(String host) {
		
		InetAddress addr = null;
		try {
			addr = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			 log.log(Level.SEVERE, "unable to get localHost"+ e.getMessage());
		}
		if(addr != null)
		{
		    String ipAddress = addr.getHostAddress();
	        String hostname = addr.getHostName();
	        if(ipAddress.contains(host) || hostname.contains(host))
	        {
	        	return true;
	        }
	        else
	        {
	        	return false;
	        }
		}else
		{
			return false;
		}
	}

	public boolean uploadFile(String host, String username, String password, String filelocation) {
		
		FTPClient client = new FTPClient(); 
	    FileOutputStream fos = null; 
	    boolean flag = true;
	    try { 

	    	client.connect(host);
	    	client.login(username, password); 
	        client.remoteStore(filelocation);
	        
	    } catch (IOException e) 
	    { 
	    	  log.log(Level.SEVERE, "unable to upload file to ftp server "+ e.getMessage()+ " file location :" +filelocation);
	    	  flag = false;
	    	 	      
	    } finally { 
	      try { 
	        if (fos != null) { 
	          fos.close(); 
	        } 
	        	client.disconnect(); 
	      } catch (IOException e) {
	    	  
	    	  log.log(Level.SEVERE, "unable to disconnect ftp server "+ e.getMessage());
	      } 
	    }
	    return flag;
	        
	}
	
	public static void main(String arg[])
	{
		FTPFileManagementImpl imp = new FTPFileManagementImpl();
		try {
			imp.downloadFile("ds.kenscio.com", "kenscio", "k@nsc!o", "/home/kenscio/admintest/CandidateStatic.metadata.json");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
}
