package com.kenscio.litemail.workflow.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.common.FileUtil;
import com.kenscio.litemail.workflow.common.ManageMailBodyResponse;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.CampaignLinks;

public class CampaignServiceUtil {

	public boolean saveAttachmentFileCopyCampaign(long selectedCampaignId, Campaign copyCampaign){
			
        String imageRoot = ApplicationProperty.getProperty("stampit.attachment.dir") ;
        String copyFullFilePath = FileUtil.getFullFilePath(imageRoot,copyCampaign.getAccountId(),selectedCampaignId,copyCampaign.getAttachmentFileName());
        byte[] bytes = FileUtil.getFileByFullFilePath(copyFullFilePath);
        String saveFullFilePath = FileUtil.getFullFilePath(imageRoot,copyCampaign.getAccountId(),copyCampaign.getCampaignId(),copyCampaign.getAttachmentFileName());
        if(bytes !=null && bytes.toString().length() > 0 && FileUtil.saveFileToDisk(saveFullFilePath,bytes)){
            return true;
        }else{
            return false;
        }
        
   	}
	
	public boolean saveAttachmentFileNewCampaign(Campaign campaign, String attachedFileName, InputStream attachedFileStream) {
        if(campaign.getAttachmentFileName() != null) {
        	
           
        	byte[] attachedFileData = null;
            try {
            	int size = attachedFileStream.available();
            	attachedFileData = new byte[size];
				attachedFileStream.read(attachedFileData);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				try {
					attachedFileStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}            
            campaign.setAttachedFileData(attachedFileData);
            String imageRoot = ApplicationProperty.getProperty("stampit.attachment.dir") ;
            String fullFilePath = FileUtil.getFullFilePath(imageRoot,campaign.getAccountId(),campaign.getCampaignId(),campaign.getAttachmentFileName());
            if(FileUtil.saveFileToDisk(fullFilePath,campaign.getAttachedFileData())){
                return true;
            }else{
                return false;
            }
          
        }
        return true;
	}

	public void getZipMailBodyAndUpdateMessageCreateNewCampaign(Campaign campaign,
			String zipMailBodyName, InputStream zipMailBodyStream) {
		CampaignService campaignService = new CampaignService();
		long maxCampaignIdForImgName=campaign.getCampaignId();//campaignService.getMaxCampignIdForImgName()+1;
		HTMLParserService hTMLParserService = new HTMLParserService();
        ManageMailBodyResponse response  = hTMLParserService.saveZippedMailBodyAndGetHtmlContent(zipMailBodyStream, maxCampaignIdForImgName, campaign.getAccountId());
        campaign.setCampaignMessage(response.getHtmlContent());
        campaign.setHtmlFileName(response.getHtmlName());
            
	}
	public void copyUnZippedFilesIntoNewCampaignIdLocation(long selectedCampaignId , Campaign campaign,String zipMailBodyName, InputStream zipMailBodyStream) {
		CampaignService campaignService = new CampaignService();
		long maxCampaignIdForImgName=campaign.getCampaignId();//campaignService.getMaxCampignIdForImgName()+1;
		HTMLParserService hTMLParserService = new HTMLParserService();
		ManageMailBodyResponse response  = hTMLParserService.copyUnZippedFilesIntoNewCampaignIdLocation(campaign.getAccountId(),selectedCampaignId,maxCampaignIdForImgName);
	    campaign.setCampaignMessage(response.getHtmlContent());
	    campaign.setHtmlFileName(response.getHtmlName());
	}
	public String getCampaignMessageWithReplacedOriginalLinks(Long campaignId,String message){
	        String redirectedUrl = ApplicationProperty.getProperty("stampit.clickthrough.redirect.url");
	        CampaignLinkClicksService stampitClicksService = new CampaignLinkClicksService();
	        CampaignLinks[] stampitClicks=stampitClicksService.getStampitClicksByCampaignIdIncludingInactive(campaignId);
	        String replacedUrl="";
	        int counter=0;
	        for (int i = 0; i < stampitClicks.length; i++) {
	           CampaignLinks stampitClick = stampitClicks[i];
	           int end=0;
	           if(counter > 0){
	              message=replacedUrl;
	           }
	           if((end = message.indexOf(redirectedUrl+stampitClick.getUrlId())) >= 0){
	               int redirectedUrlLenght=redirectedUrl.length()+stampitClick.getUrlId().toString().length();
	               replacedUrl=message.substring(0,end)+stampitClick.getUrl()+message.substring(end+redirectedUrlLenght);
	               counter++;
	           }
	        }
	        return replacedUrl.equals("") ? message : replacedUrl;
	   }

}
