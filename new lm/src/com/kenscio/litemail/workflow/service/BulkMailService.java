package com.kenscio.litemail.workflow.service;

import com.kenscio.litemail.workflow.domain.BulkMailData;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.service.BulkMailer;

import java.util.concurrent.FutureTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jun 17, 2006
 * Time: 2:58:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class BulkMailService  {



   public void startBulkMailing(EmailData[] emailIds, BulkMailData bulkMailData){
    BulkMailer bulkMailer = initialiseBulkMailer(emailIds,bulkMailData);
    FutureTask task = new FutureTask (bulkMailer);
    ExecutorService es = Executors.newSingleThreadExecutor ();
    es.submit (task);
       System.out.println("Task submitted");
    try {
      Boolean result = (Boolean)task.get ();
      if(result){
          
      }
    }
    catch (Exception e) {
      System.err.println (e);
    }
    es.shutdown ();
   }


    private BulkMailer initialiseBulkMailer(EmailData[] emailIds, BulkMailData bulkMailData) {
        BulkMailer bulkMailer = new BulkMailer(500);
         return bulkMailer;
    }
}
