package com.kenscio.litemail.workflow.service;

import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.EmailException;

import javax.mail.internet.InternetAddress;
import java.util.List;
import java.util.Vector;
import java.io.UnsupportedEncodingException;

import com.kenscio.litemail.workflow.domain.EmailData;


public class EmailSender {
    private String smptpServer;
    private String smtpUserName;
    private String smtpPassword;
    private String fromAddress;
    private String fromName;

    public EmailSender() {
    }

    public void sendEmail(EmailData emailData,String message,String subject) throws EmailException,UnsupportedEncodingException {

                 HtmlEmail email = new HtmlEmail();
                 List dests = new Vector();
                 try {
                     dests.add(new InternetAddress(emailData.getEmailId(),
                             emailData.getName()));
                     email.setSubject(subject);
                     email.setFrom(fromAddress,fromName);
                     email.setTo(dests);
                     email.setHtmlMsg(message);
                     email.setHostName(smptpServer);
                     email.setSmtpPort(25);
                     if(smtpPassword != null && smtpUserName != null){
                        email.setAuthentication(smtpUserName,smtpPassword);
                     }

                    email.send();

                 } catch (UnsupportedEncodingException e) {
                     e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                     throw new UnsupportedEncodingException();
                 } catch (EmailException e) {
                     e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                     //System.out.println("Exception in sending mail--");
                     throw new EmailException();
                 }

                 catch (Exception e) {
                     e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    // System.out.println("Exception in sending mail--");
                     throw new EmailException();
                 }



    }

    public void setSmptpServer(String smptpServer) {
        this.smptpServer = smptpServer;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public void setSmtpUserName(String smtpUserName) {
        this.smtpUserName = smtpUserName;
    }
}
