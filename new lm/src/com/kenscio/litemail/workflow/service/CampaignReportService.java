package com.kenscio.litemail.workflow.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;












import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.json.JSONException;
import org.json.JSONObject;

import com.dataservice.platform.persistence.impl.ListDataDaoImpl;
import com.kenscio.litemail.workflow.domain.MessageSpam;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.dao.CampaignDao;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.CampaignBounce;
import com.kenscio.litemail.workflow.domain.CampaignMailFail;
import com.kenscio.litemail.workflow.domain.CampaignMailSentSoFar;
import com.kenscio.litemail.workflow.domain.EmtsResponse;
import com.kenscio.litemail.workflow.domain.EmtsResponseBounce;
import com.kenscio.litemail.workflow.domain.MessageBounce;
import com.kenscio.litemail.workflow.domain.MessageUnsubscribe;

public class CampaignReportService {

	public Campaign getCampaignStatusReport(long accountId,
			Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign =cdao.getCampaign(campaignID);
		return campaign;
	}

	public CampaignBounce getCampaignMailBounceCountByDomain(Long campaignID,
			String dm, String sp, String onDate) throws ParseException {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getCampaignMailBounceCountByDomain(campaignID,dm,sp,onDate);
	}
	
	public List<Campaign> getMailSentDataList(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign =cdao.getCampaign(campaignID);
		campaign.setFromAddress(campaign.getFromAddress());
		List<Campaign> list= new ArrayList<Campaign>();
		list.add(campaign);
		return list;
	}
	
	public List<MessageBounce> getBounceEmails(String bounceType,
			Date campaignStartTime,String sp,String dm,int pageNumber,int noOfRecordsperPage) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		return cdao.getAllBouncedEmails(bounceType, campaignStartTime,sp,dm,pageNumber,noOfRecordsperPage);
	}

	public List<String> getvalidEmails(List<String> emails) {
		ListDataDaoImpl impl=new ListDataDaoImpl();
		return impl.validEmailIdsinList(emails);
	}

	public List<MessageBounce> getBounceData(List<String> validEmail,Date startTime) {
		
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getBounceData(validEmail,startTime);
	}
	
	public List<MessageSpam> getCampaignSpamData(Date campaignStartTime,String sp,String dm) {
		ListDataDaoImpl impl = new ListDataDaoImpl();;
		List<String> spams = impl.getSpamEmails(campaignStartTime,sp,dm);
		
		if(spams != null && spams.size() > 0)
		{
			List<String> valid = getvalidEmails(spams);
			if(valid != null && valid.size() > 0)
			return getSpamData(valid);
		}
		return new ArrayList<MessageSpam>();
	}
	
	public List<MessageBounce> getCampaignBounceData(String bounceType, Date campaignStartTime,String sp,String dm) {
		ListDataDaoImpl impl = new ListDataDaoImpl();;
		List<String> bounces = impl.getAllBouncedEmails(bounceType, campaignStartTime,sp,dm);
		//List<String> bounces = getBounceEmails(bounceType, campaignStartTime);
		if(bounces != null && bounces.size() > 0)
		{
			List<String> valid = getvalidEmails(bounces);
			if(valid != null && valid.size() > 0)
			return getBounceData(valid,campaignStartTime);
		}
		return new ArrayList<MessageBounce>();
	}
	
	
	public List<MessageSpam> getSpamEmails(Date campaignStartTime,String sp,String dm) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getSpamEmails(campaignStartTime,sp,dm);
	}

	public List<MessageSpam> getSpamData(List<String> validEmail) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getSpamData(validEmail);
	}

	public List<CampaignMailFail> getMailFailDataList(Long campaignID, Integer pageNumber, int noOfRecordsperPage) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailFailDataList(campaignID,pageNumber,noOfRecordsperPage);
	}

	public List<CampaignMailFail> getMailFailDataListFilter(Long campaignID,
			Date fd, Date td, Integer pageNumber, int noOfRecordsperPage) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getCampaignMailFailFilterFromTodate(campaignID,fd,td,pageNumber,noOfRecordsperPage);
	}

	public EmtsResponse getEmtsResponse(String url)
    {
        HttpClient client;
        EmtsResponse emtsObject;
        client = new DefaultHttpClient();
        emtsObject = new EmtsResponse();
        try
        {
            HttpGet request = new HttpGet(url);
            //request.getParams().setParameter("cst", new Integer(5000));;
            HttpResponse response = client.execute(request);
            ObjectMapper wrapper = new ObjectMapper();
            emtsObject = (EmtsResponse)wrapper.readValue(response.getEntity().getContent(), EmtsResponse.class);
            client.getConnectionManager().shutdown();
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        client.getConnectionManager().shutdown();
		return emtsObject;
       
    }
	
	public static EmtsResponseBounce getEmtsResponseBounce(String url) 
	{
		HttpClient client = new DefaultHttpClient();
		EmtsResponseBounce emtsObject = new EmtsResponseBounce() ;
		try{
		 
		  HttpGet request = new HttpGet(url);
		  HttpResponse response = client.execute(request);
		  ObjectMapper wrapper = new ObjectMapper();
		  SimpleDateFormat dateFormat =  new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
		  wrapper.getSerializationConfig().setDateFormat(dateFormat);
		  wrapper.getDeserializationConfig().setDateFormat(dateFormat); 
		  emtsObject = wrapper.readValue(response.getEntity().getContent(), EmtsResponseBounce.class);
		  client.getConnectionManager().shutdown();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			 client.getConnectionManager().shutdown();
		}
		return emtsObject;
		
	}
	
	public static void main(String arg[])
	{
		String platformId = "litemail";
		long campaingId = 9;
		long accountId = 1;
		String baseURl = "http://ds.kenscio.com:8088/emts/";
		String url=baseURl+"ViewMailAllBounceApiPagination.htm?pId="+platformId+"&clId="+accountId+"&recId="+0+"&recSize=100";
		//CampaignReportService cr = new CampaignReportService();
		EmtsResponseBounce emtsObject =  getEmtsResponseBounce(url);
		System.out.println("emtsObject" +emtsObject.getCount());
		
		List<MessageBounce> mb = new ArrayList<MessageBounce>();
	    if(emtsObject.getItems() != null && emtsObject.getItems().size() > 0)
	    {
	    	List list = emtsObject.getItems() ;
	    	ObjectMapper obj = new ObjectMapper();
	    	
	    	for(Object k : list)
	    	{
	    		 System.out.println((MessageBounce)k);
	    	}
	    	
	    }
		
	}
	
	
	 public JSONObject execute(String url) throws ClientProtocolException, IOException, JSONException
		    {
		        HttpClient client = new DefaultHttpClient();
		        HttpGet request = new HttpGet(url);
		        HttpResponse response = client.execute(request);
		        BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		        String output;
		        String result;
		        for(result = ""; (output = br.readLine()) != null; result = (new StringBuilder(String.valueOf(result))).append(output).toString()) { }
		        JSONObject jResult = new JSONObject(result);
		        client.getConnectionManager().shutdown();
		        return jResult;
		    }

	public Map getActualUrls(long accountId, long campaignId)
    {
        CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
        return cdao.getActualUrls(accountId, Long.valueOf(campaignId));
    }

	public List<MessageBounce> getMailBounceListData(Long campaignId,
			String bounceType,int pageNumber,int noOfRecords) {
		  CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailBounceListData(campaignId,bounceType,pageNumber,noOfRecords);
	}

	public List<MessageBounce> getMailBounceListDataFilter(Long campaignID,String bounceType,
			Date fd, Date td, int pageNumber, int noOfRecordsperPage,String sp,String dm) throws ParseException {
		  CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
			return cdao.getMailBounceListDataFilter(campaignID,bounceType,fd,td,pageNumber,noOfRecordsperPage,sp,dm);
	}

	public List<MessageSpam> getMailSpamListData(List<String> validEmails,Date startTime ,int pageNumber, int noOfRecordsperPage) {
		  CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailSpamListData(validEmails,startTime,pageNumber,noOfRecordsperPage);
	}

	public List<MessageSpam> getMailSpamListDataFilter(List<String> emails,Date fd,Date td, int pageNumber, int noOfRecordsperPage) throws ParseException {
		  CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailSpamListDataFilter(emails,fd,td,pageNumber,noOfRecordsperPage);
	}

	public List<CampaignBounce> getCampaignMailBounceCount(long accountId,
			Long campaignID) {
		 CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getCampaignMailBounceCount(accountId,campaignID);
	}

	public int getMailBounceCount(List<String> emails,Date startTime,Date fd,Date td) {
		CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailBounceCount(emails,startTime,fd,td);
	}

	public int getMailBounceCountFilter(Long campaignID, String bounceType,
			Date fd, Date td) throws ParseException {
		CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailBounceCountFilter(campaignID,bounceType,fd,td);
	}

	public int getMailSpamCount(List<String> validEmails,Date startTime) {
		CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailSpamCount(validEmails,startTime);
	}

	public int getMailSpamCountDateFilter(Date fd,Date td,String sp,String dm) {
		CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailSpamCountDateFilter(fd,td,sp,dm);
	}

	public int getMailFailCount(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailFailCount(campaignID);
	}

	public int getMailFailDateFilterCount(Long campaignID, Date fd, Date td) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailFailDateFilterCount(campaignID,fd,td);
	}

	public CampaignBounce getCampaignMailBounceCountByDomain(Long campaignID, String dm,
			String sp, Date fromTime, Date toTime) throws ParseException {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getCampaignMailBounceCountByDomain(campaignID,dm,sp,fromTime,toTime);
	}

	public List<MessageBounce> getCampaignBounceDataByDomain(String dm,
			String sp, String bounceType, Date cstime, Date cendTime,int pageNumber, int noOfRecordsperPage) throws ParseException {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getCampaignBounceDataByDomain(dm,sp,bounceType,cstime,cendTime,pageNumber,noOfRecordsperPage);
	}

	public List<MessageSpam> getCampaignSpamData(String dm, String sp,
			Date cstime, Date cendTime, Integer pageNumber,
			int noOfRecordsperPage) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getCampaignSpamData(dm,sp,cstime,cendTime,pageNumber,noOfRecordsperPage);
	}

	public float getMailSpamCount( Date cstime,String sp, String dm,Date cetime) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailSpamCount(dm,sp,cstime,cetime);
	}

	public float getMailBounceCount(String dm, String sp, String bounceType,Date cstime, Date cendTime) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailBounceCount(dm,sp,cstime,cendTime,bounceType);
	}

	public List<MessageUnsubscribe> getCampaignUnsubscribeData(Long campaignID,
			Integer pageNumber, int noOfRecordsperPage) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getCampaignUnsubscribeData(campaignID,pageNumber,noOfRecordsperPage);
	}

	public float getMailUnsubscribeCount(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailUnsubscribeCount(campaignID);
	}

	public List<MessageUnsubscribe> getCampaignUnsubscribeData(Long campaignID,
			Date fd, Date td, Integer pageNumber, int noOfRecordsperPage) throws ParseException {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getCampaignUnsubscribeData(campaignID,fd,td,pageNumber,noOfRecordsperPage);
	}

		public List<CampaignMailSentSoFar> getCampaignMailSentSoFarData(
			Long campaignID, Integer pageNumber, int noOfRecordsperPage) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getCampaignMailSentSoFarData(campaignID, pageNumber, noOfRecordsperPage);
	}

	public float getMailMailSentSoFarCount(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailMailSentSoFarCount(campaignID);
	}

	public List<CampaignMailSentSoFar> getCampaignMailSentSoFarData(
			Long campaignID, Date fd, Date td, Integer pageNumber,
			int noOfRecordsperPage) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getCampaignMailSentSoFarData(campaignID, fd, td, pageNumber, noOfRecordsperPage);
	}

	public List<MessageSpam> getSpamEmails(Date fd,Date td, String sp,
			String dm, int pageNumber,int noOfRecordsperPage) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getSpamEmails(fd,td,sp,dm,pageNumber,noOfRecordsperPage);
	}

	public List<String> getBounceEmails(String bounceType,Date campaignStartTime, Date fromDate,
			Date toDate, String sp, String dm) {
CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		return cdao.getAllBouncedEmails(bounceType,campaignStartTime, fromDate,toDate,sp,dm);
		
	}

	public List<MessageSpam> getSpamEmails(String sp, String dm, Date fd, Date td
			) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return cdao.getSpamEmails(sp,dm,fd,td);
	}

	public List<MessageBounce> getCampaignBounceData(List<String> validEmails,Date startTime,Date toDate,
			Integer pageNumber, int noOfRecordsperPage) {
		  CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
			return cdao.getCampaignBounceData(validEmails,startTime,toDate,pageNumber,noOfRecordsperPage);
	}

	public List<MessageBounce> getBounceEmails(String bounceType, Date fromDate,
			Date toDate, String sp, String dm) {
CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		return cdao.getAllBouncedEmails(bounceType, fromDate,toDate,sp,dm);
	}

	public float getMailBounceCount(List<String> validEmail,
			Date campaignStartTime) {
		CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getMailBounceCount(validEmail,campaignStartTime);
	}

	public List<MessageSpam> getSpamEmails(Date campaignStartTime, String sp,
			String dm, Integer pageNumber, int noOfRecordsperPage) {
		CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getSpamEmails(campaignStartTime,sp,dm,pageNumber,noOfRecordsperPage);
	}

	public List<MessageBounce> getBounceEmails(String bounceType, Date fd,
			Date td, String sp, String dm, Integer pageNumber,
			int noOfRecordsperPage) {
		CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		return cdao.getBounceEmails(bounceType,fd,td,sp,dm,pageNumber,noOfRecordsperPage);
	}

/*	public List<String> getBounceEmails(String bounceType,
			Date campaignStartTime, String sp, String dm) {
		CampaignDao cdao = (CampaignDao)AppContext.getFromApplicationContext("campaignDao");
		
		return cdao.getBounceEmails(bounceType,campaignStartTime,sp,dm);
	}*/


	
}