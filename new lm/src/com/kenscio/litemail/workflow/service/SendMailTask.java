package com.kenscio.litemail.workflow.service;


import javax.mail.Address;
import javax.mail.Message;


public class SendMailTask implements Runnable {
	
	private BulkMailer bulkMailer;
	private int indexOfMessage ;
	
	
	public SendMailTask(BulkMailer bm,int index){
		bulkMailer=bm;
		indexOfMessage=index;
	}

	public void run() {
		
		try
		{
		
		if(bulkMailer.getCSpeed() <=3600)
		{
			try {
				Thread.currentThread().sleep(bulkMailer.getTSpeed());
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		}
		boolean pass=false;
		int trial=0;
		Address[] address =null; 
		Message message= bulkMailer.getStampedEmails()[indexOfMessage];
		//System.out.println("calling end thread to send message trying 4 times message to send ");
		while(trial<4&&!pass){
		try {
			address = message.getAllRecipients();
			bulkMailer.sendStampedEmail(message);
			pass=true;
			bulkMailer.incrementMailSentCounter();
		//	System.out.println("Successfully sent to " + address[0]);
			//concurrentModification exception
			bulkMailer.addToSentMailQueue(message);
			} catch (Exception e) {
			// TODO Auto-generated catch block
				trial++;
				e.printStackTrace();
				System.out.println("Exception in sending mail to -->" + address[0] + " " + e.getCause());
			} 
		}
		if(!pass && trial>=4){
			bulkMailer.incrementMailFailedCounter();
			bulkMailer.getMailsFailedInSMTP().add(message);
			System.out.println("Failed! sending mail to -->" + address[0]);
			
		}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		bulkMailer.incrementTaskCount();
		
	}

	public BulkMailer getBulkMailer() {
		return bulkMailer;
	}

	public void setBulkMailer(BulkMailer bulkMailer) {
		this.bulkMailer = bulkMailer;
	}



}
