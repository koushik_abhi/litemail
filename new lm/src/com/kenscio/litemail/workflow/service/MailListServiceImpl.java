package com.kenscio.litemail.workflow.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;






import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.dataservice.platform.persistence.impl.ListDataDaoImpl;
import com.kenscio.litemail.workflow.domain.AttributeEntity;
import com.kenscio.litemail.workflow.domain.CheckCSVDTO;
import com.kenscio.litemail.workflow.domain.DomainCount;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.domain.InsertSubscriberDTO;
import com.kenscio.litemail.workflow.domain.ListEntity;
import com.kenscio.litemail.workflow.domain.ListDataEntity;
import com.kenscio.litemail.workflow.domain.RoleEntity;
import com.kenscio.litemail.util.Page;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.BaseData;
import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.dao.AttributeDao;
import com.kenscio.litemail.workflow.dao.CampaignDao;
import com.kenscio.litemail.workflow.dao.MailListDao;


public class MailListServiceImpl {
	
	public static Logger log = Logger.getLogger(MailListServiceImpl.class.getName());	
	
	
	
	public void addAttributeDataForList(ListDataEntity data)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.addAttributeDataForList(data);
		
	}
	
	public Map<String, Long> getDomainCountMapForGraph(long listId) {
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.getDomainCountMapForGraph(listId);
		
	}
	
	public void addEmailDataForList(EmailData data)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.addEmailDataForList(data);
		
	}
	
	
	public void addAttributeDataListForList(List<ListDataEntity> attrData)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.addAttributeDataListForList(attrData);
				
	}
	public void deleteAllAttributeDataForList(long listId, List<String> emilids)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.deleteAllAttributeDataForList(listId, emilids);
	
		 
	}
	
	public void deleteAttributeDataForList(long listId, String emilids)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.deleteAttributeDataForList(listId, emilids);
		 
	}
	
	public void addNewEmailNameTolist(long listId, String email, String name)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.addNewEmailNameTolist(listId, email,name);
		 
	}
	
	public boolean isEmailExistInCampaignList(long listId, String email)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.isEmailExistInCampaignList(listId, email);
		
	}
	public List getAttributeDataForListGraph(long listId, String attribute)
	{
		ListDataDaoImpl imp = new ListDataDaoImpl();
		return imp.getAttributeDataForListGraph(listId, attribute);
		
	}
	public ListDataEntity getAttributeDataForListAttribute(long listId, String email, String attrName)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.getAttributeDataForListAttribute(listId, email, attrName);
				
	}
	
	public boolean isEmailExistInCampaignListDataService(long listId, String email)
	{
		ListDataDaoImpl  dao = new ListDataDaoImpl();
		return dao.isEmailExistInCampaignList(listId, email);
		
	}
	public List getAttributeDataForListGraphDataService(long listId, String attribute)
	{
		ListDataDaoImpl  dao = new ListDataDaoImpl();
		return dao.getAttributeDataForListGraph(listId, attribute);
		
	}
		
	
	private File savingTheUpLoadedFileIntoServer(CheckCSVDTO dto, String contextPath) {
        File file=new File(dto.getUploadedFileName());
        PrintStream p = null;
        try {
            FileOutputStream out=new FileOutputStream(file);
            p=new PrintStream(out);
            p.write(dto.getUploadedFileData());
            out.close();
            p.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }finally
        {
        	if(p != null)
        		p.close();
        }
        return file;
    }
	public String validateMailListHeaders(List<String> userHeaders, String contextPath, long accountId)
	{	
		
		
        List<String> dbHeaders = new ArrayList<String>();
        try
        {
        	
	        	        
	         if(userHeaders != null && userHeaders.size() <=0 )
	         {
	        	 return "invalid";
	         }
	         if(userHeaders != null && userHeaders.size() ==1 )
	         {
	        	 String str = userHeaders.get(0);
	        	 if(str != null && !str.toLowerCase().contains("mail"))
	        	 {
	        		 return "invalid";
	        	 }
	         }
	         List<String> emailHeader = new ArrayList<String>();
	         for(String head: userHeaders)
	         {
	        	 if(head.indexOf("@") > 0 && head.indexOf(".") > 0)
	        	 {
	        		 return "invalid";
	        		 
	        	 }else if(head.toLowerCase().contains("mail"))
	        	 {
	        		 emailHeader.add(head);
	        	 }
	         }
	         if(emailHeader.size() == 0)
	         {
	        	 return "invalid";
	         }
	         List<String> filterHeaders = new ArrayList<String>();
	         for(String head: userHeaders)
	         {
	        	 if(!head.toLowerCase().contains("mail") )
	        	 {
	        		 filterHeaders.add(head);
	        	 }
	         }	        
	       	         
	         //comparing user attributes with dbattributes
	         AttributeDao  attributeDao = (AttributeDao) AppContext.getFromApplicationContext("attributeDao");
	         List<AttributeEntity> dbattributes=attributeDao.getAllUserAttributes(accountId);
	         for(int i=0;i<dbattributes.size();i++)
	         {
	        	dbHeaders.add(dbattributes.get(i).getAttrName()); 
	        	//System.out.println(dbattributes.get(i).getAttrName());
	         }
	       
	         if(dbHeaders.containsAll(filterHeaders))
	         {
	        	 return "valid";
	         }
	         else
	         {
	        	 return "invalid";
	         }
	         
        } catch (Exception e) {
			
        	System.out.println("error in compare headers with attributes "+ e.getMessage());
        	e.printStackTrace();
		}
		return "invalid";
      
		
	}
	
	public void deleteImageFileFromDisk(File uploadedFile) {
        try {
            System.out.println("deleting serveruploaded csv file::" + uploadedFile.getName());
            FileUtils.forceDelete(uploadedFile.getAbsoluteFile());
            /*FileUtils.deleteDirectory(uploadedFile.getAbsoluteFile());
            uploadedFile.delete();
            uploadedFile.deleteOnExit();*/
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public void uploadMailListData(String filePath , long stampItlistId) 
	{
		
	   String  nextLine[]=new String[2000];
       CSVReader reader = null;
       List<String> headers = new ArrayList<String>();
       File file=new File(filePath);
       try
       {
	         int firstRow = 0;
	         InputStreamReader fr =  null;
			
	         fr = new InputStreamReader(new FileInputStream(file));
	         reader = new CSVReader(fr);
			 while ((nextLine = reader.readNext()) != null)
	         {
				 
				 if(firstRow > 0)
				 {
					 List<String> cols = new ArrayList<String>();
					 int c = 0;
					 int hsize = headers.size();
					 for(int nIndex = 0; nIndex <nextLine.length;nIndex++)
					 {
						 if(nIndex < hsize )
						 {
							 cols.add(nextLine[nIndex].trim());
						 }
						 c++;
						 if(c >= 50)
							 break;
					 }	                         
					
					 addColumnDataForCampaignList(stampItlistId, headers, cols, "new");
					 firstRow++;
					 
					 
				 }else if(firstRow == 0)
				 {
					 int cc = 0;
				     for(int nIndex = 0; nIndex <nextLine.length;nIndex++)
	                 {
		                 if(nextLine[nIndex].trim() != "" )
		                 {
		                	 headers.add(nextLine[nIndex].trim());
		                 }
		                 cc++;
			             if(cc >= 50)
			            	 break;
	                 }
				     firstRow++;
				    
				 }
	          	           
	         }
			 System.out.println("total number of records "+ firstRow);
			      
	         
       }catch(Exception e)
       {
    	   log.log(Level.SEVERE, "error during upload file "+ e.getMessage());
    	   System.out.println( "error during upload file "+e.getMessage());
	             	
       }
       finally
       {
  	 		
			try {
				if(reader != null)
					reader.close();
				
			} catch (IOException e) {
				
				System.out.println( "error during upload file "+e.getMessage());
			}
        }
       
		   
	}
	
	
	public void addColumnDataForCampaignList(long listId, List<String> headers, List<String> cols, String type) 
	{
	
		
		
		if(headers.size() == 1)
		{
			String email = cols.get(0);
			//ifEmailExitDeleteIt(stampitListId, email);
			ListDataEntity data = new ListDataEntity();
			data.setListId(listId);
			data.setReciepientEmail(email);
			EmailData da= new EmailData();
			da.setListId(listId);
			da.setEmailId(email);
			addEmailDataForList(da);
			ifEmailExitDeleteIt(listId, email, type);
			addAttributeDataForList(data);
			
		}
		else if(headers.size() >= 2)
		{
			
			String name = getNameFromColumnData(headers, cols);
			String email = getEmailFromColumnData(headers, cols);
			ifEmailExitDeleteIt(listId, email, type);
			for(int i= 0; i< headers.size(); i++)
			{
				String attrname = headers.get(i);
				String value = cols.get(i);
				/*if(value.equalsIgnoreCase(email))
				{
					continue;
				}*/
				ListDataEntity data = new ListDataEntity();
				data.setListId(listId);
				data.setReciepientName(name);
				data.setReciepientEmail(email);
				data.setAttributeName(attrname);
				data.setAttributeValue(value);
				EmailData da= new EmailData();
				da.setListId(listId);
				da.setEmailId(email);
				da.setName(name);
				addEmailDataForList(da);
				addAttributeDataForList(data);
			}
		}
		 
		
	}

	private void addColumnDataForCampaignList_OLD(long stampitListId, List<String> headers, List<String> cols) 
	{
		
		if(headers.size() == 1)
		{
			String email = cols.get(0);
			ifEmailExitDeleteIt(stampitListId, email, "new");
			ListDataEntity data = new ListDataEntity();
			data.setListId(stampitListId);
			data.setReciepientEmail(email);
			addAttributeDataForList(data);
			
		}else if(headers.size() == 2)
		{
			String head1 = headers.get(0);
			String head2 = headers.get(1);
			String email ="";
			String name = "";
			if(head1.toLowerCase().contains("mail"))
			{
				email = cols.get(0);
				name = cols.get(1);
			} else if (head2.toLowerCase().contains("mail"))
			{
				email = cols.get(1);
				name = cols.get(0);
			}					
			
			ifEmailExitDeleteIt(stampitListId, email, "new");
			ListDataEntity data = new ListDataEntity();
			data.setListId(stampitListId);
			data.setReciepientName(name);
			data.setReciepientEmail(email);
			addAttributeDataForList(data);
			
		}
		else if(headers.size() > 2)
		{

			System.out.println("headers.size()" + headers.size());
			System.out.println("cols.size()" + cols.size());
			String name = getNameFromColumnData(headers, cols);
			String email = getEmailFromColumnData(headers, cols);
			ifEmailExitDeleteIt(stampitListId, email, "new");
			for(int i= 0; i< headers.size(); i++)
			{
				String attrname = headers.get(i);
				String value = cols.get(i);
				ListDataEntity data = new ListDataEntity();
				data.setListId(stampitListId);
				data.setReciepientName(name);
				data.setReciepientEmail(email);
				data.setAttributeName(attrname);
				data.setAttributeValue(value);
				data.setAttributeType("string");
				addAttributeDataForList(data);
			}
		}
		 
		
	}

	private String getEmailFromColumnData(List<String> headers,
			List<String> cols) {
		
		String value = "";
		for(int i= 0; i< headers.size(); i++)
		{
			String name = headers.get(i);
			if(name.toLowerCase().contains("mail"))
			{
				value =  cols.get(i);
				break;
			}
		}
		
		return value;
	}

	private String getNameFromColumnData(List<String> headers, List<String> cols) {
		String value = "";
		for(int i= 0; i< headers.size(); i++)
		{
			String name = headers.get(i);
			if(name.toLowerCase().contains("name"))
			{
				value =  cols.get(i);
				break;
			}
		}
		
		return value;
	}

	public void ifEmailExitDeleteIt(long listId, String email, String type) {
		if(!type.toLowerCase().equals("add"))
		deleteAttributeDataForList(listId, email);
		
	}

	public void uploadMailListData(InsertSubscriberDTO[] insertSubscriberDTO,
			Long listId) {
		for(int i= 0; i <insertSubscriberDTO.length; i++ )
		{
			if(insertSubscriberDTO[i].getEmailId() != null && insertSubscriberDTO[i].getEmailId().length()  > 1)
			addNewEmailNameTolist(listId, insertSubscriberDTO[i].getEmailId(), insertSubscriberDTO[i].getName());
		}
		
	}
	
	public boolean isMailingListExist(long accountId,String listName)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.isMailingListExist(accountId, listName);
	}
		
		
	public void delteMailingList(long listId,long accountId)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.deleteMailingList(listId,accountId);
		ListDataDaoImpl imp = new ListDataDaoImpl();
		imp.deleteCustomRecordsFromExcel(listId);
	}
	public long getListIdOnName( long accountId, String listName)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.getListIdOnName(accountId, listName);
	}

	public List<ListEntity> getAllList( long accountId)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.getAllLists(accountId);
	}
	
	public ListEntity getListDetail(long listId){
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		List<ListEntity> list =  dao.getListDetail(listId);
		if(list != null && list.size() >0)
		{
			return list.get(0);
		}
		return null;
	}
	
	public void updateListConfigStatus(long listId, String status) {
		ListEntity li = getListDetail(listId);
		if(li != null)
		{
			li.setStatus(status);
			MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
			dao.updateMailingList(li);
		}
		
	}
	
	public void updateListConfigStatusWithRecords(long listId, String status,
			int totalRecords, int invalid) {
		ListEntity li = getListDetail(listId);
		if(li != null)
		{
			Calendar calendar1 = Calendar.getInstance();
		    Date myDate1 = new Date(); // May be your date too.
			calendar1.setTime(myDate1);
			li.setUploadTime(calendar1.getTime());    
			li.setUpdatedDate(calendar1.getTime());    
			li.setStatus(status);
			li.setTotalRecords(totalRecords);
			li.setInvalidRecords(invalid);
			MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
			dao.updateMailingList(li);
		}
		
	}
	
	public List<ListEntity> getListDetailsForUserAndlistname(long accountId,String listName){
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.getListDetailsForUserAndlistname(accountId, listName);
	}
	public void updateRecordAndStatusOfList(ListEntity listData)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.updateRecordAndStatusOfList(listData);
	}
	public List<ListEntity> searchList(long accountId,String listName)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.searchList(accountId, listName);
	}
	
	public void deleteListData(long listId)
	{
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.deleteListData(listId);
	}
	
	public long getAllRecordCountForList(long accountId,long listId)
	{
		ListDataDaoImpl  dao = new ListDataDaoImpl();
		return dao.getValidEmailCountForCampaignList(listId);
		
	}
	public long getListRecordCount(long accountId,long listId )
	{
		
		ListDataDaoImpl  dao = new ListDataDaoImpl();
		return dao.getValidEmailCountForCampaignList(listId);
	}

	
	public long getListRecordCount(long accountId,String listName)
	{
		long listId = getListIdOnName(accountId,listName);
		ListDataDaoImpl  dao = new ListDataDaoImpl();
		return dao.getValidEmailCountForCampaignList(listId);
	}

	public void addNewUpdateMailingList(ListEntity stampitList) {
			
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.updateMailingList(stampitList);
	
		
	}
	
	public void addMailingListOnly(ListEntity stampitList) {
		
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		dao.addNewMailingList(stampitList);
				
	}

	public ListEntity validateListName(String listname, Long accountId) {
		List lists = getListDetailsForUserAndlistname(accountId, listname);
		if(lists != null && lists.size() > 0)
		{
			return (ListEntity)lists.get(0);
		}
		return null;
	}
	
	public List<EmailData> getListDataEmailsPagination(int pageNumber , int pageSize, long listId) {
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.getListDataEmailsPagination(pageNumber, pageSize, listId);
	}
	
	public List<ListEntity> getAllListsInPagination(long accountId,
			String userName, int pagenumber, int noOfRecords) {
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.getAllListsInPagination(pagenumber, noOfRecords, accountId);
	}
	
	
	public void addUpdateDomainCounts(long listId, String syncMode,
			Map<String, Long> domainCountMap) {
		
	   if(syncMode.equalsIgnoreCase("delete"))
	   {
		   
		   Set<String> keys = domainCountMap.keySet();
		   for(String domainName : keys)
		   {
			   long emailCount = domainCountMap.get(domainName);
			   long count = getCountForDomain(listId, domainName);
			   if(count > 0)
			   {
				   count = count-emailCount;
			   }else
			   {
				   count =0;
			   }
			   updateDomainCount(listId, domainName, count);
		   }
	      
	   }
	   else if( syncMode.equalsIgnoreCase("addupdate"))
	   {
		 
		   Set<String> keys = domainCountMap.keySet();
		   for(String domainName : keys)
		   {
			   long emailCount = domainCountMap.get(domainName);
			   long count = getCountForDomain(listId, domainName);
			   count = count+emailCount;
			   updateDomainCount(listId, domainName, count);
		   }
		   
	   }if( syncMode.equalsIgnoreCase("update"))
	   {
		   
		   Set<String> keys = domainCountMap.keySet();
		   for(String domainName : keys)
		   {
			   long emailCount = domainCountMap.get(domainName);
			   long count = getCountForDomain(listId, domainName);
			   count = count+emailCount;
			   updateDomainCount(listId, domainName,count);
		   }
		   
		   
	   }
	   if( syncMode.equalsIgnoreCase("new"))
	   {
		   
		   Set<String> keys = domainCountMap.keySet();
		   for(String domainName : keys)
		   {
			   long emailCount = domainCountMap.get(domainName);
			   long count = getCountForDomain(listId, domainName);
			   count = count+emailCount;
			   updateDomainCount(listId, domainName,count);
		   }
	   }
	   else if (syncMode.equalsIgnoreCase("add"))
	   {
		   Set<String> keys = domainCountMap.keySet();
		   for(String domainName : keys)
		   {
			   long emailCount = domainCountMap.get(domainName);
			   long count = getCountForDomain(listId, domainName);
			   count = count+emailCount;
			   updateDomainCount(listId, domainName,count);
		   }
		   
		   
	   }
	
	}

	private void updateDomainCount(long listId, String domainName , long totalCount) {
        
        MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
        DomainCount count = dao.getCountForDomain(listId, domainName);
        if(count == null)
        {
            count = new DomainCount();
            count.setListId(listId);
            count.setDomainName(domainName);
            count.setNoOfEmails(totalCount);
            
        }
        dao.updateDomainCount(count);
        
	}

	private long getCountForDomain(long listId, String domainName) {
		
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		DomainCount dcount = dao.getCountForDomain(listId, domainName);
		if(dcount != null)
			return dcount.getNoOfEmails();
		
		return 0;
	}

	public List<String> getAllAvailableListNames(long accountId) {
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.getAllAvailableListNames(accountId);
		
	}

	public boolean isListExistInCampaignList(long listId) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao.checkIflistisUsed(listId);
		
	}

	public void exportListDataToExcel(CSVWriter writer, String listName, long accountId) {
		AttributeService ser = new AttributeService();
		long listId = getListIdOnName(accountId, listName);
		List<String> headers = ser.getAllAttributesNamesExport(accountId);
		headers.remove("email");
		headers.add(0, "email");
		for (int i = 0; i < headers.size(); i++) {
			System.out.println("headers:"+headers);
		}
	//	String [] harray = headers.toArray(new String[headers.size()]);
	   // writer.writeNext(harray);       
	    ListDataDaoImpl  dao = new ListDataDaoImpl();
	    dao.exportListDataToExcel(writer, headers, listName, listId, accountId);
		
	}
	public Map<String, String> getAttributesDataMap(String email, long listId) {
		ListDataDaoImpl  dao = new ListDataDaoImpl();
		return dao.getAttributesDataMap(email, listId);
	}

	public String getListNameOnId(Long accountId, Long listId) {
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		List<ListEntity> data = dao.getListDetail(listId);
		if(data != null && data.size() > 0)
		{
			return data.get(0).getListName();
		}
		return null;
	}

	public Map<Long, List<EmailData>> getEmailListFromDB(long listId, long fileSeeker, long noOfRecords) {
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		List<EmailData> data = dao.getEmailDataForList(listId, fileSeeker, noOfRecords );
		long nextPointer = fileSeeker+noOfRecords+1;
		Map<Long, List<EmailData>> records = new HashMap<Long, List<EmailData>>();
		records.put(nextPointer, data);
		return records;
	}

	public boolean isEmailExistForUpdate(long listId, String em) {
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.isEmailExistForUpdate(listId, em);
		
	}

	public int getTotalPagesOfList(String partnerName) {
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.getTotalPagesOfList(partnerName);
	}

	public List<List<String>> getAllListDataPagination(long listId,
			int pagenumber, int noOfRecords) {
		ListDataDaoImpl imp = new ListDataDaoImpl();
		return imp.getSubDataForList(listId, pagenumber, noOfRecords);
		
	}

	public void deleteSubscriber(long listId, String email) {
		ListDataDaoImpl imp = new ListDataDaoImpl();
		imp.deleteSubscriber(listId, email);
		
	}

	public Map<String, String> getSubscriberDetails(long listId, String email) {
		ListDataDaoImpl imp = new ListDataDaoImpl();
		return imp.getSubscriberDetails(listId, email);
		
	}
	public Map<String, String> getSubscriberCustomDetails(long listId,
			   String email) {
			  ListDataDaoImpl imp = new ListDataDaoImpl();
			  return imp.getSubscriberCustomDetails(listId, email);
			 }
	public void updateSubscriberDetails(long listId, String email, Map<String, String> stdAttr) {
		ListDataDaoImpl imp = new ListDataDaoImpl();
		imp.updateSubscriberDetails(listId, email, stdAttr);
		
	}

	public long getListDatCount(long accountId, String listName) {
		ListDataDaoImpl imp = new ListDataDaoImpl();
		long listId = this.getListIdOnName(accountId, listName);
		return imp.getListDatCount(listId);
	}

	public List<ListEntity> getAllListsInPagination(String partnerName,
			String userName, int pagenumber, int noOfRecords) {
		MailListDao  dao = (MailListDao) AppContext.getFromApplicationContext("mailListDao");
		return dao.getAllListsInPagination(pagenumber, noOfRecords, partnerName);
	}
	

	

	

	
}
