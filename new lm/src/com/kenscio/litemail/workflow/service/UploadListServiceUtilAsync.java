package com.kenscio.litemail.workflow.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import au.com.bytecode.opencsv.CSVReader;


import com.dataservice.platform.persistence.impl.AttributeDataDaoImpl;
import com.dataservice.platform.persistence.impl.ListDataDaoImpl;
import com.kenscio.litemail.util.CSVImportMySql;
import com.kenscio.litemail.util.DataServiceUtilLiteMail;
import com.kenscio.litemail.util.MailUtil;
import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.domain.ActionLogObject;
import com.kenscio.litemail.workflow.domain.AttributeEntity;
import com.kenscio.litemail.workflow.domain.ListEntity;

public class UploadListServiceUtilAsync {
	
	private static final Logger log = Logger.getLogger(UploadListServiceUtilAsync.class.getName());  
	
	private long listId = 0;
	private String username = "";
	private List<String> headers = null;
	private String filename  = "";
	private List<String> dbattr = null;
	private String syncMode = "";
	private String filePath = null;
	private String mode ="add";
	
	public UploadListServiceUtilAsync(long listId, String username,
			List<String> headers, String filename,
			List<String> dbattr, String syncMode, String mode, String filePath) {
		
		this.listId = listId;
		this.username = username;
		this.headers = headers;
		this.filename = filename;
		this.dbattr = dbattr;
		this.syncMode = syncMode;
		this.filePath = filePath;
		this.mode = mode;
		
	}

	public void uploadDataAsync() {
		
			
		ThreadPoolExecutorServiceImpl.executeUploadAsync(this);
		
	}
	
	private void sendEmailNotification(ActionLogObject ac1) {
		
		EmailServiceUtil util = new EmailServiceUtil();
		util.sendEmailNotification(ac1);
	}
	
   private void addNewRecordsNew(String sqlCreate, List<String>customHeadersMap, List<String> headers, List<String> row, long listId, String userName, List<String> dbattr, Map<String, List<String>> invalid, Map<String, Long> domainCountMap ) {

		
		if(row != null & row.size() > 0)
		{
			String em = row.get(0);
			if(validateEmail(em))
			{
				ListDataDaoImpl im = new ListDataDaoImpl();
				
				try
				{
					
					updateDomainCount(em,domainCountMap);
					im.addCustomRecordsFromExcelNew(sqlCreate, listId, row);
										
					
				}catch(Exception e)
				{
					if(em != null && em.length() >0){
						invalid.put(em,  row);
						log.log(Level.SEVERE, "error in adding record "+ em);
					}
				}
				
			}else
			{
				if(em != null && em.length() >0){
				invalid.put(em,  row);
				log.log(Level.WARNING, "invalid email Id dropping this record "+ em);
				}
			}
		}
		
		
		
	}

	
	private void addNewRecords(String sqlCreate, List<String>customHeadersMap, List<String> headers, List<String> row, long listId, String userName, List<String> dbattr, Map<String, List<String>> invalid, Map<String, Long> domainCountMap ) {

		
		if(row != null & row.size() > 0)
		{
			String em = row.get(0);
			if(validateEmail(em))
			{
				ListDataDaoImpl im = new ListDataDaoImpl();
				String str = im.isEmailListExist(em, listId);
				try
				{
					if(str == null || str =="")
					{
						updateDomainCount(em,domainCountMap);
						im.addCustomRecordsFromExcelNew(sqlCreate, listId, row);
						
											
					}
				}catch(Exception e)
				{
					if(em != null && em.length() >0){
						invalid.put(em,  row);
						log.log(Level.SEVERE, "error in adding record "+ em);
					}
				}
				
			}else
			{
				if(em != null && em.length() >0){
				invalid.put(em,  row);
				log.log(Level.WARNING, "invalid email Id dropping this record "+ em);
				}
			}
		}
		
		
		
	}

	private Map<String, String> getMapData(long listId, List<String> headers,
			List<String> row) {
		Map<String, String> newmaptemp  = new HashMap<String, String>();
	    for(int i = 0; i < headers.size(); i++)
	    {
	    	newmaptemp.put(headers.get(i), row.get(i));
	    }
		return newmaptemp;
	}

	private void updateDomainCount(String em, Map<String, Long> domainCountMap) {
		String domainName = getDomainNameOfEmail(em);
		if(domainCountMap.containsKey(domainName))
		{
			long count = domainCountMap.get(domainName);
			count++;
			domainCountMap.put(domainName, count);
			
		}else
		{
			long count = 1;
			domainCountMap.put(domainName, count);
			
		}
		
	}

	private String getDomainNameOfEmail(String em) {
		
		int ind = em.indexOf("@");
		String rtu = em.substring(ind+1);
		return rtu;
	}

	private void updateRecords(String updateQuery, List<String>customHeadersMap, List<String> headers, List<String> row, long listId, String userName, List<String> dbattr, Map<String, List<String>> invalid, String mode, Map<String, Long> domainCountMap ) {
		
		
		if(row != null & row.size() > 0)
		{
			String em = row.get(0);
			if(validateEmail(em))
			{
				try{
				ListDataDaoImpl im = new ListDataDaoImpl();
				String str = im.isEmailListExist(em, listId);
				if(str != null && str.length() > 0)
				{
					im.updateCustomRecordsFromExcel(listId, updateQuery, row, em);
					
					
				}}catch(Exception e)
				{
					if(em != null && em.length() >0){
						invalid.put(em,  row);
						log.log(Level.SEVERE, "error in update record "+ em);
					}
				}
				
				//updateDomainCount(em,domainCountMap);
				
			}else
			{
				
				if(em != null && em.length() >0){
					invalid.put(em,  row);
					log.log(Level.WARNING, "invalid email Id dropping this record "+ em);
				}
			}
		}
		
	}

	

	private void addUpdateRecords( String sqlcreate, String sqlupdate, List<String>customHeadersMap, List<String> headers, List<String> row, long listId, String userName, List<String> dbattr, Map<String, List<String>> invalid, String mode, Map<String, Long> domainCountMap ) 
	{
		
				
		if(row != null & row.size() > 0)
		{
			String em = row.get(0);
			if(validateEmail(em))
			{
				try{
				ListDataDaoImpl im = new ListDataDaoImpl();
				String str = im.isEmailListExist(em, listId);
				if(str != null && str.length() > 0)
				{
					im.updateCustomRecordsFromExcel(listId, sqlupdate , row, em);
					
					
				}else
				{
					updateDomainCount(em,domainCountMap);
					im.addCustomRecordsFromExcelNew(sqlcreate, listId,  row);
					
					
				}
				}catch(Exception e)
				{
					if(em != null && em.length() >0){
						invalid.put(em,  row);
						log.log(Level.SEVERE, "error in addupdate record "+ em);
					}
				}
				
				
				
			}else
			{
				if(em != null && em.length() >0){
				invalid.put(em,  row);
				log.log(Level.WARNING, "invalid email Id dropping this record "+ em);
				}
			}
		}
					
		
	}

	private boolean validateEmail(String email)
	{
		if(email != "" && email.indexOf("@") > 0)
		{
			return true;
			
		}
		return false;
		
	}
	

	private void deleteFileRecords(List<String> headers, List<String> row, long listId, String userName, Map<String, List<String>> invalid, Map<String, Long> domainCountMap ) {
		
		List<String> emilids = new ArrayList<String>();
		{
			
			if(row != null)
			{
				String em = row.get(0);
				if(validateEmail(em))
				{
					try{
					ListDataDaoImpl im = new ListDataDaoImpl();
					String str = im.isEmailListExist(em, listId);
					if(str != null && str.length() > 0)
					{
						emilids.add(em);
						updateDomainCount(em, domainCountMap);
						im.deleteCustomRecordsFromExcel(listId, em);
						
					}}catch(Exception e)
					{
						if(em != null && em.length() >0){
							invalid.put(em,  row);
							log.log(Level.SEVERE, "error in delete record "+ em);
						}
					}
					
				}
				else
				{
					invalid.put(em,  row);
				}
			}
		}
		
		
	}
	
	
	public void execute() 
	{
		
		   Map<String, List<String>> invalid = new HashMap<String, List<String>>();
		   Map<String, Long> domainCountMap = new HashMap<String, Long>();
	       String  nextLine[]=new String[2000];
	       CSVReader reader = null;
	       File file = null;
	       List<String> customHeadersMap = null;
	       MailListServiceImpl imp = new MailListServiceImpl();
	       try
	       {
		         int firstRow = 1;
		         System.out.println("headers execute "+ headers);
				 ActionLogObject ac1 = MailUtil.addListUploadStartActionAndSendNotification(listId, filename, username);
				 
				 imp.updateListConfigStatus(listId, "uploading");
				 int hsize = headers.size();
				 String email ="";
				 String name = "";
				
				 AttributeService as = new AttributeService();
				 List<String> stdAttrs = as.getAllStdAttributes();
				 customHeadersMap =  persistMapAndCreateCustomHeaders(headers, stdAttrs,listId);
				 String sqlCreate  = "";
				 String sqlUpdate  = "";
				
				 /*CSVImportMySql ms = new  CSVImportMySql();
				 ms.addCustomRecordsFromExcel(filePath, listId, customHeadersMap);*/
				 
				 sqlCreate =DataServiceUtilLiteMail.createSQLQuery(listId, customHeadersMap, DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA);
				 sqlUpdate =DataServiceUtilLiteMail.createUpdateSQLQuery(listId, customHeadersMap, DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA);
				
				 InputStreamReader fr =  null;
				 file = new File(filePath);
		         fr = new InputStreamReader(new FileInputStream(file));
		         reader = new CSVReader(fr);
		        
		         if(syncMode.equals("new"))
		         {
		        	 
		        	 System.out.println("starting upload now synmode new "+ new Date());
		        	 while ((nextLine = reader.readNext()) != null)
			         {
			           List<String> cols = new ArrayList<String>();
			           int c = 0;
		        	  
		               for(int nIndex = 0; nIndex <nextLine.length;nIndex++)
		               {
		                 if(nIndex < hsize )
		                 {
		                	 cols.add(nextLine[nIndex].trim());
		                 }
		                 c++;
			             if(c >60)
			            	 break;
		               }
		               addNewRecordsNew(sqlCreate, customHeadersMap, headers, cols, listId, username, dbattr ,invalid, domainCountMap);
					  					   
			           firstRow++;
			          	           
			        }
		        	System.out.println("done upload now synmode new "+ new Date()); 
		         }else
		         {
		        	 System.out.println("starting upload now synmode "+ syncMode +" and time "+ new Date());
					 while ((nextLine = reader.readNext()) != null)
			         {
			           List<String> cols = new ArrayList<String>();
			           int c = 0;
		        	  
		               for(int nIndex = 0; nIndex <nextLine.length;nIndex++)
		               {
		                 if(nIndex < hsize )
		                 {
		                	 cols.add(nextLine[nIndex].trim());
		                 }
		                 c++;
			             if(c >60)
			            	 break;
		               }
		             //  System.out.println("headers headers "+headers);
		               if (syncMode.equals("add"))
					   {
						   addNewRecords(sqlCreate, customHeadersMap, headers, cols, listId, username, dbattr ,invalid, domainCountMap);
					   }
		               else if( syncMode.equals("addupdate"))
					   {
						   addUpdateRecords(sqlCreate, sqlUpdate, customHeadersMap,  headers, cols, listId, username, dbattr ,invalid, mode, domainCountMap);
						   
					   }else if( syncMode.equals("update"))
					   {
						   updateRecords(sqlUpdate, customHeadersMap, headers, cols, listId, username, dbattr ,invalid, mode, domainCountMap);
						   
					   }else if(syncMode.equals("delete"))
					   {
					       deleteFileRecords(headers, cols, listId, username, invalid, domainCountMap);
					   }
					   
			           firstRow++;
			          	           
			        }
					System.out.println("done upload now synmode "+ syncMode +" and time "+ new Date());
		        }
				 
			
				if(firstRow > 1)
				{
					firstRow = firstRow-1;
				}
		        
			    int processed  = firstRow - (invalid.size());
			   		    
			    imp.updateListConfigStatusWithRecords(listId, "created", (firstRow-1),  invalid.size());
			    ListEntity li = imp.getListDetail(listId);

			    imp.addUpdateDomainCounts(listId, syncMode, domainCountMap);
			    if(invalid.size() >0)
			    {
			    	ac1.setActionSummaryDetails("Upload list records completed \n List Name: "+ li.getListName() + " \n Total records Uploaded : " + processed + " \n Invalid emails: " + invalid.size() + " \n User uploaded list is: "+ username);
			    	String str = invalid.toString();
			    	ac1.setActionLogFile(str.getBytes());
			    }
			    else 
			    {		    	
			    	ac1.setActionSummaryDetails("Upload list is completed sucessfully \n List Name: "+ li.getListName() + " \n Total records Uploaded:  " + (firstRow-1) + " \n User uploaded list is: "+ username);
			    }
			    ac1.setStatus("completed");
			    ac1.setTotalRecords((firstRow-1)+"");
			    ac1.setImportedRecords(processed+"");
			   // ac1.setDiscrepancyRecords(invalid.size()+"");
			    
			    MailUtil.updateListCompletedActionLogSendEmailNotification(ac1);
		      
		         
	       }catch(Exception e)
	       {
		       	e.printStackTrace();
		       	log.log(Level.SEVERE, "error during upload file "+e.getMessage());
		        imp.updateListConfigStatus(listId, "failed");   
	      	
	       }
	       finally
	       {
	  	 		
				try {
					if(reader != null)
					reader.close();
					if(file != null)
						file.delete();
				} catch (IOException e) {
					
					 log.log(Level.SEVERE, "error in closing file "+e.getMessage());
				}
	       }
		   
	}

	private boolean isAllAttributesStdInCustomTable(List<String> headers,
			List<String> stdAttrs, long listId) {
		
		List<String> hd = new ArrayList<String>();
		for(String str: headers)
		{
			hd.add(str);
		}
		hd.removeAll(stdAttrs);
		if(hd.size() == 0)
		{
			return true;
		}
		return false;
		
	}

	private List<String> persistMapAndCreateCustomHeaders(
			List<String> headers, List<String> stdAttrs, long listId) {
		ArrayList<String> customHdrs = new ArrayList<String>();
		
		AttributeService impl = new AttributeService();
		Map<String, String> attrDBMap  = impl.getListAttributesMapped(listId);
		int attrIndex = 0;
		if(attrDBMap != null && attrDBMap.size() > 0)
		{
			attrIndex= getMaxAttributeInex(attrDBMap);
		}
		for(String hd : headers)
		{
			if(stdAttrs.contains(hd))
				customHdrs.add(hd);
			else if(attrDBMap.containsKey(hd))
				customHdrs.add(attrDBMap.get(hd));
			else
			{
				attrIndex++;
				customHdrs.add(getCustomStdAttr(hd, stdAttrs, attrDBMap, attrIndex, listId));
			}
		}
		return customHdrs;
	}

	private int getMaxAttributeInex(Map<String, String> attrDBMap) {
		Collection<String> keys = attrDBMap.values();
		
		List<Integer> num = new ArrayList<Integer>();
		for(String str: keys)
		{
			String st  = str.substring(4);
			num.add(Integer.parseInt(st));
		}
		Collections.sort(num);
		int s = num.size();
		return num.get(s-1);
		
	}

	private String getCustomStdAttr(String hd, List<String> stdAttrs,
			Map<String, String> attrDBMap, int attrIndex, long listId) {
				
		String dbatt = "attr"+attrIndex;
		addAttributeMap(listId, hd, dbatt);
		return dbatt;
	
	}

	private void addAttributeMap(long listId, String hd, String dbatt) {
		AttributeDataDaoImpl dao = new AttributeDataDaoImpl();
		dao.addAttributeMap(hd, dbatt, listId);
		
	}

	
}
