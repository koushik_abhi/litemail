package com.kenscio.litemail.workflow.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.quartz.SchedulerException;

import com.kenscio.litemail.workflow.common.QuartzJobsServiceImpl;
import com.kenscio.litemail.workflow.domain.JobDetailsObject;


public class JobsManagementImpl 
{

	public static Logger log = Logger.getLogger(JobsManagementImpl.class.getName());	
	
	public boolean addJob(JobDetailsObject jobsEntityObject) throws Exception {
	
		boolean addflag  = true;
						
		try {
			
			QuartzJobsServiceImpl.getInstance().createJobAndStart(jobsEntityObject, true);
			
			
		} catch (Exception e) {
			
			log.log(Level.SEVERE, "unable to create job "+ e.getMessage());
			addflag = false;
			throw e;
		}
		return addflag;
		
	}
		
     

	public boolean deleteJob(JobDetailsObject jobsEntityObject) throws SchedulerException 
	{
	
		boolean addflag  = true;
		
		try {
			
			QuartzJobsServiceImpl.getInstance().deleteJob(jobsEntityObject);
			
		} catch (SchedulerException e) {
			
			log.log(Level.SEVERE, "unable to delete job "+ e.getMessage());
			addflag = false;
			throw e;
		}
	 	
		return addflag;
		
	}
	


	public boolean updateJob(JobDetailsObject jobsEntityObject) throws Exception {
		
		boolean addflag  = true;
				
		try {
			
			QuartzJobsServiceImpl.getInstance().updateJobAndStart(jobsEntityObject, true);
					
			
		} catch (Exception e) {
			
			log.log(Level.SEVERE, "unable to update job "+ e.getMessage());
			addflag = false;
			throw e;
		}
		return addflag;
		
	}



	public boolean isJobExist(String jobName) 
	{
		boolean isExist = false;
		
		try {
			
			isExist = QuartzJobsServiceImpl.getInstance().isJobExist(jobName);
			
		} catch (Exception e) {
			
			log.log(Level.SEVERE, "job exist "+ e.getMessage());
			
		}
		if(isExist)
			return true;
		else 
			return false;
		
		
	}
	
	
	
}
