package com.kenscio.litemail.workflow.service;

import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.Semaphore;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;


public class SMTPServerProxy {

	private String server;
	private String username;
	private String password;
	private Session session;
	private int maxConnections;
	private int maxMsgsInConnection;
	private int port;
	private int[]resTable;
	private SMTPConnection connections[];
	private Semaphore sema;
	private String fromAddress;//bounce
	private boolean isSSL;
	private boolean isTLS;
	private boolean requiresAuth;
	private Object lock;
	
	
	public SMTPServerProxy(String server,int port,String username,String password,String fromAddress,int
			maxConnections,int maxMsgsInConnection,boolean requiresAuth,boolean isSSL,boolean isTLS)
	{
	   this.server=server;
	   this.port=(port==-1)?25:port;
	   this.username=username;
	   this.password=password;
	   this.maxConnections=maxConnections;
	   this.fromAddress=fromAddress;
	   this.maxMsgsInConnection=maxMsgsInConnection;
	   this.requiresAuth=requiresAuth;
	   this.isSSL=isSSL;
	   this.isTLS=isTLS;
	}
	public boolean Init(){
	   Properties props=new Properties();
	   String protocol=isSSL?"smtps":"smtp";
	   props.put("mail.transport.protocol",protocol);
	   props.put("mail."+protocol+".host",server);
	   props.put("mail."+protocol+".port",String.valueOf(port));
	   props.put("mail."+protocol+".user",username);
	   props.put("mail."+protocol+".from",fromAddress);
	   props.put("mail."+protocol+".quitwait", "false");
	   //props.put("mail.smtp.sendpartial","true");
	   if(isTLS)
	     props.put("mail."+protocol+".starttls.enable","true");
	  // props.put("mail.debug","true");
	   Authenticator auth=null;
	   if(requiresAuth){
		  props.put("mail."+protocol+".auth","true");
		  auth=new SMTPAuthenticator();
	   }
	   session=Session.getInstance(props,auth);
	   connections=new SMTPConnection[maxConnections];
	   resTable=new int[maxConnections];
	   Arrays.fill(resTable,0);
	   sema=new Semaphore(maxConnections,true);
	   lock=new Object();
	   return true;
	}
	
	public boolean send(Message message){
	  SMTPConnection connection=getConnection();
	  if(connection==null)return false;
	  boolean status=connection.send(message);
	  ReleaseConnection(connection);
	  return status;
	}
	public void close(){
		for(int i=0;i<connections.length;i++)
			if(connections[i]!=null)connections[i].close();
	}
	private void ReleaseConnection(SMTPConnection connection){
	  /* synchronized(this){
		 sema.release();
		 resTable[connection.getIdx()]=0;
       }*/
		 resTable[connection.getIdx()]=0;
		 sema.release();		
	}
	private SMTPConnection getConnection(){
	  try {
		sema.acquire();
		synchronized(lock){
		 for(int i=0;i<maxConnections;i++){
		  if(resTable[i]==0){
			  if(connections[i]==null)
			    connections[i]=new SMTPConnection(i,session,maxMsgsInConnection); 
			  resTable[i]=1;
			  return connections[i];
		  }
		 }
	   }
	 } catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	 return null;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Session getSession() {
		return session;
	}
	public void setSession(Session session) {
		this.session = session;
	}
	public class SMTPAuthenticator extends Authenticator{
		public PasswordAuthentication getPasswordAuthentication(){
			return new PasswordAuthentication(username,password);
		}
	}
}
