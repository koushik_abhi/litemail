package com.kenscio.litemail.workflow.service;

import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.CampaignLinks;
import com.kenscio.litemail.workflow.domain.URLChangeData;
import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.common.FileUtil;
import com.kenscio.litemail.workflow.common.ManageMailBody;
import com.kenscio.litemail.workflow.common.ManageMailBodyResponse;
import com.kenscio.litemail.util.OIVHtmlDocument;
import com.kenscio.litemail.util.OIVHtmlLink;
import com.kenscio.litemail.util.OIVHtmlParser;
import com.kenscio.litemail.workflow.common.ApplicationProperty;

import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.io.InputStream;
import java.io.File;

import org.htmlparser.util.Translate;


public class HTMLParserService {
   
    public List getURLList(String htmlString) {
        OIVHtmlParser parser = new OIVHtmlParser();
        OIVHtmlDocument  htmlDocument =parser.parse(htmlString);
        List urlList = new ArrayList();
        OIVHtmlLink[] htmlLinks = htmlDocument.getOIVHtmlLinks();
        if(htmlLinks !=null && htmlLinks.length > 0){
            for (int i = 0; i < htmlLinks.length; i++) {
                OIVHtmlLink htmlLink = htmlLinks[i];
                String urlstr = htmlLink.getURL();
                
                System.out.println("fetched urls "+ urlstr);
                if(urlstr != null && urlstr.trim() !="#" && urlstr.length() > 1)
                urlList.add(Translate.decode(htmlLink.getURL()));
            }
        }
        return urlList;
    }
    
	public String replaceSelectedURLs(String htmlString,
			URLChangeData[] urlChangeDatas, Long campaignId, Campaign campaign) {
		 	OIVHtmlParser parser = new OIVHtmlParser();
	        OIVHtmlDocument  htmlDocument =parser.parse(htmlString);
	        for (int i = 0; i < urlChangeDatas.length; i++) {
	            Integer urlPosition = urlChangeDatas[i].getUrlPosition();
	            String actualURL =   htmlDocument.getOIVHtmlLinks()[urlPosition].getURL();
	            Long urlId = saveURLDetailsAndGetURLId(actualURL,campaignId,urlChangeDatas[i].getUrlName());
	            if(campaign.getEnableLinkTracking() != null && campaign.getEnableLinkTracking().equals("on"))
	            {
	            	String changedURL = createRedirectURL(urlId);
	            	htmlDocument.getOIVHtmlLinks()[urlPosition].setURL(changedURL);
	            }
	            else
	            {
	            	htmlDocument.getOIVHtmlLinks()[urlPosition].setURL(actualURL);
	            }
	        }
	        return htmlDocument.toString();
	}

    public String replaceSelectedURLs(String htmlString, URLChangeData[] urlChangeDatas,Long campaignId) {
        OIVHtmlParser parser = new OIVHtmlParser();
        OIVHtmlDocument  htmlDocument =parser.parse(htmlString);
        for (int i = 0; i < urlChangeDatas.length; i++) {
            Integer urlPosition = urlChangeDatas[i].getUrlPosition();
            String actualURL =   htmlDocument.getOIVHtmlLinks()[urlPosition].getURL();
            Long urlId = saveURLDetailsAndGetURLId(actualURL,campaignId,urlChangeDatas[i].getUrlName());
            String changedURL = createRedirectURL(urlId);
            htmlDocument.getOIVHtmlLinks()[urlPosition].setURL(changedURL);
        }
        return htmlDocument.toString();
    }

    private String createRedirectURL(Long urlId) {
     String redirectURL = ApplicationProperty.getProperty("stampit.clickthrough.redirect.url") +
                          urlId + ApplicationProperty.getProperty("stampit.clickthrough.recipient.link") +  //$recipient will replaced with email address @  RedeemStampDelegate.java
                          ApplicationProperty.getProperty("stampit.clickthrough.name.link") ;
                        //  ApplicationProperty.getProperty("stampit.clickthrough.cpid.link"); //$name will replaced with recipient name @  RedeemStampDelegate.java 
     //return Translate.decode(redirectURL); // to convert NCR like &amp; to &
        return redirectURL.replaceAll("&amp;","&");
    }

    private Long saveURLDetailsAndGetURLId(String actualURL, Long campaignId,String urlName) {
        CampaignLinks stampitClicks = new CampaignLinks();
        stampitClicks.setCampaignId(campaignId);
        //stampitClicks.setUrl(Translate.decode(actualURL));  // to convert NCR like &amp; to &
        stampitClicks.setUrl(actualURL.replaceAll("&amp;","&"));
        stampitClicks.setUrlName(urlName);
        stampitClicks.setCurrentStatus("active");
        stampitClicks.setNoOfClicks(new Long(0));
        stampitClicks.setCreatedBy("system");
        stampitClicks.setCreatedDate(new Date());
        stampitClicks.setUpdatedBy("system");
        stampitClicks.setUpdatedDate(new Date());
        CampaignLinkClicksService campaignLinkClicksService = new CampaignLinkClicksService();
        return campaignLinkClicksService.saveStampitClicks(stampitClicks);
    }

    /************************************************************************************
                            Below are the Mail body related
     **************************************************************************************/

    public ManageMailBodyResponse saveZippedMailBodyAndGetHtmlContent(InputStream zipFileStream, Long campId, Long accountId) {
        ManageMailBody manageMailBody = new ManageMailBody();
        String savePath = getFullPathToUnzipFile(campId,accountId);
        FileUtil.createDirectory(savePath);
        String replacePathWithoutFileName = getReplacePathWithoutFileName(campId, accountId);
        return manageMailBody.saveExtractedMailBodyAndGetHtmlContent(zipFileStream,savePath,replacePathWithoutFileName);
    }

    private String getReplacePathWithoutFileName(Long campId, Long accountId) {
          String replacePathWithoutFileName = ApplicationProperty.getProperty("mailbody.image.url")+
                                            "?type=1"+// type=1 for mailBody saving palce i.e folder name /inline
                                            "&accId="+accountId.toString()+
                                            "&campaignId="+campId.toString()+
                                            "&imgName=";
          return replacePathWithoutFileName;
    }

    public boolean deleteMailBodyFiles(Long campId, Long accountId) {
        String filePath = getFullPathToUnzipFile(campId,accountId);
        return FileUtil.deleteFile(filePath);
    }

    private String getFullPathToUnzipFile(Long campId, Long accountId) {
        String folderPath = ApplicationProperty.getProperty("stampit.image.dir")
                +"/"+accountId.toString()+"/"+campId.toString()+"/inline";
        return folderPath;
    }

    public ManageMailBodyResponse copyUnZippedFilesIntoNewCampaignIdLocation(Long accontId, Long oldcampId, Long newCampId) {
        String oldFilePath = getFullPathToUnzipFile(oldcampId,accontId);
        String newFilePath = getFullPathToUnzipFile(newCampId,accontId);
        FileUtil.createDirectory(newFilePath);
        FileUtil.saveAllFilesInFolder(oldFilePath,newFilePath);

        ManageMailBody manageMailBody = new ManageMailBody();
        String newReplacePathWithoutFileName = getReplacePathWithoutFileName(newCampId, accontId);
        String oldReplacePathWithoutFileName = getReplacePathWithoutFileName(oldcampId, accontId);
        return manageMailBody.ReplaceImageAndStylePathAndGetResponse(newFilePath,newReplacePathWithoutFileName,oldReplacePathWithoutFileName);
    }

   
}
