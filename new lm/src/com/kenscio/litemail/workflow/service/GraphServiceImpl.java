package com.kenscio.litemail.workflow.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.kenscio.litemail.workflow.common.BaseData;
import com.kenscio.litemail.workflow.domain.ListDataEntity;
import com.kenscio.litemail.workflow.domain.ListEntity;

public class GraphServiceImpl {

	public	Map<String, Integer> getListFieldGraphData(long accountId,String listName,
			String attribute) {
		
		MailListServiceImpl conf = new MailListServiceImpl();
		long listId = conf.getListIdOnName(accountId,listName);
		AttributeService att = new AttributeService();
		List<String> userattrObj = att.getAllUserAttributes(listId);
		List<ListDataEntity> lds = new ArrayList<ListDataEntity>();
		if(attribute.equalsIgnoreCase("domain"))
		{
			Map<String, Long> map = conf.getDomainCountMapForGraph(listId);
			
			Set<String> keys = map.keySet();
			for(String st: keys)
			{
				ListDataEntity ld  = new ListDataEntity();
				ld.setAttributeValue(st);
				Object value = map.get(st);
				long val = (Long)value;
				ld.setAttributeCount((int)val);
				lds.add(ld);
				
			}
		}
		else
		{
			lds = conf.getAttributeDataForListGraph(listId,attribute);
			
		}
		
		Map<String, Integer> sortedList = generateDataForFirst10Values(lds);
		return sortedList;
	}

	@SuppressWarnings("unchecked")
	private  Map<String, Integer> generateDataForFirst10Values(
			List<ListDataEntity> lds) {
		
		 Collections.sort(lds, new ValueCountComparable());
		 Map<String, Integer> generaredValues = new HashMap<String, Integer>();
		 List<String> keyvalues = new ArrayList<String>();
		 int emptycount = 0;
		 for(ListDataEntity li : lds)
		 {
			 if(li.getAttributeValue() != null &&  lds.size() > 0)
			 {
				 if(!keyvalues.contains(li.getAttributeValue()))
				 {
					 keyvalues.add(li.getAttributeValue());
				 }
				 generaredValues.put(li.getAttributeValue(), li.getAttributeCount());
			 }
			 else
			 {
				 emptycount = (int) (emptycount+li.getAttributeCount());
			 }
			 
		 }
		 
		 Map<String, Integer> retrunValues = new HashMap<String, Integer>();
		 if(keyvalues.size() > 10)
		 {
			 int count = 1;
			 int othercnt = 0;
			 for(String key : keyvalues)
			 {
				 if(count < 10)
				 {
					 retrunValues.put(key, generaredValues.get(key));
				 }
				 count++;
				 if(count >= 10)
				 {
					 othercnt = othercnt+generaredValues.get(key);
				 }
			 }
			 if(othercnt > 0)
			 retrunValues.put("others", othercnt);
			 if(emptycount > 0)
			 retrunValues.put("blank", (int) emptycount);
			 return retrunValues;
			 
		 }
		 if(emptycount > 0)
		 generaredValues.put("blank", (int) emptycount);
		 return generaredValues;
	}
		 
	public class ValueCountComparable implements Comparator<ListDataEntity>{
	 
	    public int compare(ListDataEntity o1, ListDataEntity o2) {
	    	if(o1.getAttributeCount() > o2.getAttributeCount())
	    		return -1;
	    	else if (o1.getAttributeCount() < o2.getAttributeCount())
	    		return 1;
	    	else
	    	{
	    		return o1.getAttributeValue().compareToIgnoreCase(o2.getAttributeValue());
	    		
	    	}
	    				
	       // return (o1.getDbAttributeValueCount() > o2.getDbAttributeValueCount() ? -1 : (o1.getDbAttributeValueCount()==o2.getDbAttributeValueCount() ? 0 : 1));
	    }
	} 
public List<ListDataEntity> getListGraphData(String listname, long accountId) {
		
	MailListServiceImpl imp = new MailListServiceImpl();
	List<ListEntity> lconfig = imp.getListDetailsForUserAndlistname(accountId, listname);
		return null;
		
	}
	public ListEntity getListDetailsForGraph(String listname, long accountId)
	{
		MailListServiceImpl imp = new MailListServiceImpl();
		List<ListEntity> lconfig = imp.getListDetailsForUserAndlistname(accountId, listname);
		if(lconfig != null)
			return lconfig.get(0);
		return null;
	}

}
