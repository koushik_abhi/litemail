
package com.kenscio.litemail.workflow.service;

import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.common.ApplicationProperty;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.*;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;

import org.apache.commons.mail.EmailException;

public class BulkMailer implements Callable {
    private Message[] stampedEmails;
    private List failedEmails;
    private List failedStampedEmails;
    //---------------------------------------------------------------
    private EmailData[] mailsFailedInStamping;
    private Campaign campaign;
    private Boolean campaignOver = new Boolean(false);
    private Long totalNoMails = 0L;

    private List mailsFailedInSMTP = Collections.synchronizedList(new java.util.ArrayList());
    private List mailsSentInBatch = new Vector();
    private Long fileSeeker=0L;
    
    private long batchWaitTime=  0;
    
    /************************************************************************************/
    private static final int THREAD_COUNT = Integer.parseInt(ApplicationProperty.getProperty("mail.thread.count").trim());
    private ThreadPoolExecutor pool = null;//new ThreadPoolExecutor(THREAD_COUNT, THREAD_COUNT, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
    private Long countOfMailsSent=0L;
    private Long countOfMailsSentInPreviousCampaigns=0L;
    private Long countOfMailsFailedInPreviousCampaigns=0L;
    private Long countOfMailsFailed=0L;
    private List listOfSendMails = Collections.synchronizedList(new java.util.ArrayList());
  //  private ConcurrentLinkedQueue listOfSendMails = new ConcurrentLinkedQueue();
    private volatile int taskCount;
    private SMTPServerProxy SMTPServer;
    private int tcount = 0;
    private long cspeed = 0;
    private long tsleep =0;
    /************************************************************************************/
    
    public BulkMailer(long camapignSpeed)
    {
    	int threadCount = getRequiredThreadCount(camapignSpeed);
    	tcount = threadCount;
    	cspeed =camapignSpeed;

    	if(cspeed <= 1000 && tcount <=1)
    	{
    		tsleep = 3000;
    	}
    	if(cspeed > 1000 && cspeed<=2200 && tcount <=1)
    	{
    		tsleep = 1500;
    	}
    	if(cspeed > 2200 && cspeed<=3000 && tcount <=1)
    	{
    		tsleep = 700;
    	}
    	if(threadCount <= 300)
    	{    		
    		if(threadCount <=100)
    			threadCount = threadCount*3;
    		
    		if(threadCount >100 && threadCount <=200)
    			threadCount = (threadCount*3)/2;
    		
    		pool = new ThreadPoolExecutor(threadCount, threadCount, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
    	}else if(threadCount > 300)
    	{
    		threadCount = 350;
    		pool = new ThreadPoolExecutor(350, threadCount, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
    	}
    	  
    }

    public int getRequiredThreadCount(long camapignSpeed) {
		
    	long hrspeed = camapignSpeed;
    	long secspeed = camapignSpeed/3600;
    	if(secspeed <= 0)
    	{
    		secspeed = 1;
    	}
    	else
    	{
    		secspeed = secspeed+secspeed/6;
    	}
    	return (int)secspeed;
    	
	}

	public Boolean call() throws Exception{
    	taskCount=0;
        Message[] stampedMails = getStampedEmails();
        System.out.println("calling bulk mailer call function to send messages >>>> ");
        
        for(int i=0;i<stampedMails.length;i++)
        {
        	       	
        	pool.execute(new SendMailTask(this,i));
        	
        }
        
        System.out.println("************************************Waiting for batch to finish***********************");
        //Thread.currentThread().sleep(2000);
        int noOfAttempts = 0;
        while(getTaskCount()<stampedMails.length){
        	System.out.println("************************************Inside sleep while ***********************Tasks:"+getTaskCount()+"mails "+stampedMails.length);
        	Thread.sleep(2000);
        	noOfAttempts++;
        	if(noOfAttempts >= 5)
        	{
        		break;
        	}
        }
        System.out.println("************************************Batch finished***********************");
        return Boolean.TRUE;
    }

    public Boolean isCampaignOver(){
        return campaignOver;
    }

    public void setCampaignOver(Boolean campaignOver){
        this.campaignOver = campaignOver;
    }

    public Message[] getStampedEmails(){
        return stampedEmails;
    }

    public synchronized void setStampedEmails(Message[] stampedEmails){
        this.stampedEmails = stampedEmails;
    }

    public void sendStampedEmail(Message email) throws EmailException,MessagingException{
    	if(!SMTPServer.send(email))
    		throw new EmailException("Couldn't send message");
    }

    public void saveCount(){
        System.out.println("SAVING THE  COUNT>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        long totalMailsSent =getCountOfMailsSentInPreviousCampaigns()+getCountOfMailsSent();
        System.out.println("totalMailsSent " + totalMailsSent);
        System.out.println("");
        long totalMailsFailed =getCountOfMailsFailedInPreviousCampaigns()+getCountOfMailsFailed();
        System.out.println(" totalMailsFailed  " + totalMailsFailed);
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        getCampaign().setMailsFailed(totalMailsFailed);
        getCampaign().setMailsSent(totalMailsSent);

    }

    public synchronized void setMailsFailedInStamping(EmailData[] emailIds){
        this.mailsFailedInStamping = emailIds;
    }

    public synchronized Campaign getCampaign(){
        return campaign;
    }

    public void setCampaign(Campaign campaign){
        this.campaign = campaign;
    }
    
    
    public synchronized List getFailedEmails(){
        if (failedEmails == null)
            failedEmails = new java.util.Vector();
        return failedEmails;
    }
    


    public synchronized List getFailedStampedEmails(){
       // failedStampedEmails = new ArrayList(Arrays.asList(getMailsFailedInSMTP().toArray()));
        return failedStampedEmails;
    }


    public synchronized void setFailedStampedEmails(List failedStampedEmails){
        this.failedStampedEmails = failedStampedEmails;
    }

    public Long getTotalNoMails(){
        return totalNoMails;
    }

    public void setTotalNoMails(Long totalNoMails){
        this.totalNoMails = totalNoMails;
    }

    public synchronized List getMailsFailedInSMTP(){
    	if(mailsFailedInSMTP==null)
    		mailsFailedInSMTP = Collections.synchronizedList(new ArrayList());
        return mailsFailedInSMTP;
    }

    public synchronized void clearMailsFailedInSMTP(){
         mailsFailedInSMTP.clear();
    }
    public void setMailsFailedInSMTP(List mailsFailedInSMTP){
        this.mailsFailedInSMTP = mailsFailedInSMTP;
    }

    public synchronized List getMailsSentInBatch(){
        return mailsSentInBatch;
    }

	public Long getFileSeeker() {
		return fileSeeker;
	}

	public void setFileSeeker(Long fileSeeker) {
		this.fileSeeker = fileSeeker;
	}

	
	public synchronized Long getCountOfMailsFailed() {
		return countOfMailsFailed;
	}

	public void setCountOfMailsFailed(Long countOfMailsFailed) {
		this.countOfMailsFailed = countOfMailsFailed;
	}

	public Long getCountOfMailsFailedInPreviousCampaigns() {
		return countOfMailsFailedInPreviousCampaigns;
	}

	public void setCountOfMailsFailedInPreviousCampaigns(Long countOfMailsFailedInPreviousCampaigns) {
		this.countOfMailsFailedInPreviousCampaigns = countOfMailsFailedInPreviousCampaigns;
	}

	public synchronized Long getCountOfMailsSent() {
		return countOfMailsSent;
	}

	public  void setCountOfMailsSent(Long countOfMailsSent) {
		this.countOfMailsSent = countOfMailsSent;
	}

	public  Long getCountOfMailsSentInPreviousCampaigns() {
		return countOfMailsSentInPreviousCampaigns;
	}

	public void setCountOfMailsSentInPreviousCampaigns(Long countOfMailsSentInPreviousCampaigns) {
		this.countOfMailsSentInPreviousCampaigns = countOfMailsSentInPreviousCampaigns;
	}
	
	public synchronized void incrementMailSentCounter(){
		countOfMailsSent++;
	}
	public synchronized void incrementMailFailedCounter(){
		countOfMailsFailed++;
	}
	
	public synchronized void incrementTaskCount(){
		taskCount++;
	}
	
	public synchronized int getTaskCount() {
		return taskCount;
	}
	public EmailData[] getMailsFailedInStamping() {
		return mailsFailedInStamping;
	}
	
	public synchronized void addToSentMailQueue(Object element){
		listOfSendMails.add(element);
	}

	public List getListOfSendMails() {
		return listOfSendMails;
	}

	public SMTPServerProxy getSMTPServer() {
		return SMTPServer;
	}

	public void setSMTPServer(SMTPServerProxy server) {
		SMTPServer = server;
	}

	public long getCSpeed() {
		// TODO Auto-generated method stub
		return cspeed;
	}

	public long getTSpeed() {
		// TODO Auto-generated method stub
		return tsleep;
	}

	


}