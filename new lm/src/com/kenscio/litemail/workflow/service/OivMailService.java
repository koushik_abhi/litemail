package com.kenscio.litemail.workflow.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.Message;

import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.CampaignLinks;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.domain.OivStampMessage;
import com.kenscio.litemail.exception.StampingException;
import com.kenscio.litemail.util.StringUtil;
import com.kenscio.litemail.util.URLEncodeDecodeService;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.common.TemplateEngine;
import com.kenscio.litemail.workflow.domain.MailRequestEntity;

public class OivMailService  {
    
    private TemplateEngine templateEngine = TemplateEngine.getInstnace();
         
    public Message[] getOivPreviewTestMail(MailRequestEntity request,Campaign campaign)throws StampingException{
        
        OivStampMessage oivStampMessage = createOivStampMessage(request);
        List<CampaignLinks> campLinks = getCampaingLinks(campaign);
        List stampedEmailList = new ArrayList();
        EmailData []emailData=request.getEmailDatas();
        for (int i = 0; i <emailData.length; i++) {
        	//System.out.println(emailData[i].getStamp() + "email "+ emailData[i].getEmailId());
            oivStampMessage.setStamp(emailData[i].getStamp());
            oivStampMessage.setToEmail(emailData[i].getEmailId());
            String receipientName = emailData[i].getName();
            oivStampMessage.setToName(receipientName);
            oivStampMessage.setClientCode(emailData[i].getClientCode());
            oivStampMessage.setSubject(getPersonalizedSubject(campaign, emailData[i].getEmailId()));
            oivStampMessage.setOivMailBody(getPreviewTestDisplayMailBody(request,campaign,emailData[i], receipientName, campLinks));
            //oivStampMessage.setOivSection(getOivSection(request,stampData,campaign,receipientName));
            String campaignName = StringUtil.getValidCSVFileName(request.getCampaignName());
            String oivFileName = campaignName + (request.getBatchStartPosition() + i);
            oivStampMessage.setOivFileName(oivFileName);
            oivStampMessage.setCampaignId(campaign.getCampaignId().toString());
            stampedEmailList.add(oivStampMessage.getStampedMail());
        }

        return (Message[]) stampedEmailList.toArray(new Message[stampedEmailList.size()]);
    }
    
    public Message[] getOivStampedMail(MailRequestEntity request,Campaign campaign)throws StampingException{
      
        OivStampMessage oivStampMessage = createOivStampMessage(request);
        List<CampaignLinks> campLinks = getCampaingLinks(campaign);
        List stampedEmailList = new ArrayList();
        EmailData []emailData=request.getEmailDatas();
        for (int i = 0; i <emailData.length; i++) {
        	//System.out.println(emailData[i].getStamp() + "email "+ emailData[i].getEmailId());
            //oivStampMessage.setStamp(emailData[i].getStamp());
            oivStampMessage.setToEmail(emailData[i].getEmailId());
            String receipientName = emailData[i].getName();
            oivStampMessage.setToName(receipientName);
            oivStampMessage.setSubject(getPersonalizedSubject(campaign, emailData[i].getEmailId()));
            oivStampMessage.setOivMailBody(getDisplayMailBody(request,campaign,emailData[i], receipientName, campLinks));
            oivStampMessage.setCampaignId(campaign.getCampaignId().toString());
            stampedEmailList.add(oivStampMessage.getStampedMail());
        }

        return (Message[]) stampedEmailList.toArray(new Message[stampedEmailList.size()]);
    }    
   

    private String getPersonalizedSubject(Campaign campaign, String email) {
    	EmailMessageParserService serv = new EmailMessageParserService();
    	return serv.parseBodyMessageAndUpdateWithActualValues(campaign.getSubject(), campaign.getCampaignId(), campaign.getListId(), email);
	}

	private List<CampaignLinks> getCampaingLinks(Campaign campaign) {
		CampaignService service = new CampaignService();
		return service.getCampaingLinks(campaign.getAccountId(), campaign.getCampaignId());
	}

	private OivStampMessage createOivStampMessage(MailRequestEntity request){
        OivStampMessage oivStampMessage = new OivStampMessage();
        oivStampMessage.setFromName(request.getFromName());
        oivStampMessage.setFromEmail(request.getFromEmail());
        oivStampMessage.setReplyName(request.getReplyName());
        oivStampMessage.setReplyTo(request.getReplyTo());
        oivStampMessage.setActualMailBody(request.getMailBody());
        if(request.getAttachement() != null && request.getAttachement().isFile()){
            oivStampMessage.setAttachement(request.getAttachement());
        }
        oivStampMessage.setSubject(request.getSubject());
        oivStampMessage.setStampValue(request.getStampValue());
        oivStampMessage.setSmtpServer(request.getSmtpServer());
        oivStampMessage.setSmtpUserName(request.getSmptpUserName());
        oivStampMessage.setSmtpPassword(request.getSmtpPassword());
        oivStampMessage.setSmtpRelayEmail(request.getSmtpRelayEmail());
        oivStampMessage.setBounceEmail(request.getBounceEmail());
        return oivStampMessage;
    }

    private String getPreviewTestDisplayMailBody(MailRequestEntity request,Campaign campaign,EmailData emailData,
            String toName, List<CampaignLinks> campLinks) {
		String serverName = ApplicationProperty.getProperty("com.server.full.path");
		Map values = new HashMap();
		values.put("server", serverName);
		values.put("fromName", request.getFromName());
		values.put("fromEmail", request.getFromEmail());
		values.put("toName", toName);
		values.put("recepientMail", emailData.getEmailId());
		String urlRedayStamp = "";
	//	String urlRedayStamp = convertStampToURLReadyString(emailData.getStamp());
		values.put("stamp", urlRedayStamp);
		//values.put("stampValue", ConverterUtil.getAmountInDollarFormat(request.getStampValue().doubleValue()));
		values.put("accId",campaign.getAccountId());
		values.put("campaignId",campaign.getCampaignId());
		values.put("imgName",campaign.getImageFileName());
		values.put("stampImg",campaign.getImageRadiobutton());
		values.put("category",campaign.getMailContentType());
		values.put("recipientCode",emailData.getClientCode());
	//	values.put("encReplace",encodedReplacement);
		values.put("openSrc",getOpenUrlPersonalization(campaign.getCampaignId(), campaign.getOpenTrackingSrc(),emailData,toName,campaign));
    	values.put("unsubUrl",getUnsubUrlPersonalization(campaign.getCampaignId(), campaign.getUnsubTrackingUrl(),emailData,toName,campaign));
      	
		values.put("mailBody",getCompleteMailBodyWithPersonalization( campaign.getCampaignId(), campaign.getCampaignMessage(),emailData,toName, campaign, campLinks));
			
    	String mailBodyAfterTemplate =  templateEngine.getEmailBody("unsealedEnvelope.htm",values);
    	EmailMessageParserService serv = new EmailMessageParserService();
    	return serv.parseBodyMessageAndUpdateWithActualValues(mailBodyAfterTemplate, campaign.getCampaignId(), campaign.getListId(), emailData.getEmailId());
    	//return mailBodyAfterTemplate;
		
		
	}
    
    
     private String getDisplayMailBody(MailRequestEntity request,Campaign campaign,EmailData emailData,
                                       String toName, List<CampaignLinks> campLinks) {
        String serverName = ApplicationProperty.getProperty("com.server.full.path");
        Map values = new HashMap();
        values.put("server", serverName);
        values.put("fromName", request.getFromName());
        values.put("fromEmail", request.getFromEmail());
        values.put("toName", toName);
        values.put("recepientMail", emailData.getEmailId());
        values.put("accId",campaign.getAccountId());
        values.put("campaignId",campaign.getCampaignId());
        values.put("imgName",campaign.getImageFileName());
        values.put("stampImg",campaign.getImageRadiobutton());
        values.put("category",campaign.getMailContentType());
        values.put("recipientCode",emailData.getClientCode());
      	values.put("mailBody",getCompleteMailBodyWithPersonalization(campaign.getCampaignId(), campaign.getCampaignMessage(),emailData,toName,campaign, campLinks));
      	values.put("openSrc",getOpenUrlPersonalization(campaign.getCampaignId(), campaign.getOpenTrackingSrc(),emailData,toName,campaign));
    	values.put("unsubUrl",getUnsubUrlPersonalization(campaign.getCampaignId(), campaign.getUnsubTrackingUrl(),emailData,toName,campaign));
        String mailBodyAfterTemplate =  templateEngine.getEmailBody("unsealedEnvelope.htm",values);
       	EmailMessageParserService serv = new EmailMessageParserService();
       	return serv.parseBodyMessageAndUpdateWithActualValues(mailBodyAfterTemplate, campaign.getCampaignId(), campaign.getListId(), emailData.getEmailId());
       	//return mailBodyAfterTemplate;
       
    }
     
   private Object getUnsubUrlPersonalization(Long campaignId,
			String unsubTrackingUrl, EmailData emailData, String toName,
			Campaign campaign) {
	   System.out.println("unsub url "+ unsubTrackingUrl);
	   if(unsubTrackingUrl != null)
	   {
		   unsubTrackingUrl = unsubTrackingUrl.replace("&litemail=", "&litemail="+emailData.getEmailId());
		  // unsubTrackingUrl = unsubTrackingUrl.replace("&nm=", "&nm="+emailData.getName());
		   System.out.println("unsub url aafter "+ unsubTrackingUrl);
		   return unsubTrackingUrl;
	   }
	   return "";
   }

   private String getOpenUrlPersonalization(Long campaignId,
			String openTrackingSrc, EmailData emailData, String toName,
			Campaign campaign) {
	 
	   if(openTrackingSrc != null)
	   {
		   openTrackingSrc = openTrackingSrc.replace("&litemail=", "&litemail="+emailData.getEmailId());
		 //  openTrackingSrc = openTrackingSrc.replace("&nm=", "&nm="+emailData.getName());
		  
		   return openTrackingSrc;
	   }
	   return "";
   }

   private String getCompleteMailBodyWithPersonalization(Long cpid, String compelteMailBody,EmailData data,String toName, Campaign campaign, List<CampaignLinks> campLinks) {
	   
	   for(CampaignLinks link : campLinks)
	   {
		   String origUrl= link.getUrl();
		   String redirectUrl = link.getRedirectUrl();
		   System.out.println("redirect urls origUrl "+ origUrl);
		   System.out.println("redirect urls redirectUrl "+ redirectUrl);
		   if(redirectUrl != null)
		   {
			   EmailMessageParserService serv = new EmailMessageParserService();
		       String ori =  serv.parseBodyMessageAndUpdateWithActualValues(origUrl, campaign.getCampaignId(), campaign.getListId(), data.getEmailId());
		       
			   String encUrl = URLEncodeDecodeService.encodeURLString(ori);
			   redirectUrl = redirectUrl.replace("&litemail=", "&litemail="+data.getEmailId());
			   redirectUrl = redirectUrl.replace("&nm=", "&nm="+data.getName());
			   redirectUrl = redirectUrl.replace("&ruenc=", "&ruenc="+encUrl);
			   compelteMailBody = compelteMailBody.replace(origUrl, redirectUrl);
		   }
		   
	   }
	   
	   //compelteMailBody = compelteMailBody.replaceAll("= 100%", "=700px");
	  // compelteMailBody = compelteMailBody.replaceAll("=100%", "=700px");
	 //  compelteMailBody = compelteMailBody.replaceAll(": 100%", ":700px");
	  // compelteMailBody = compelteMailBody.replaceAll(":100%", ":700px");
	  // System.out.println("mail body is "+compelteMailBody );
	   
        /*if(!"".equals(toName) || toName!=null){
             compelteMailBody = compelteMailBody.replaceAll("%NAME%",toName);
         }else{
            compelteMailBody = compelteMailBody.replaceAll("%NAME%","");
         }
         compelteMailBody = compelteMailBody.replaceAll("%EMAIL%",data.getEmailId());

         if(data.getReplace1()!=null){
            compelteMailBody = compelteMailBody.replaceAll("%REPLACE1%",data.getReplace1());
         }
         if(data.getReplace2()!=null){
            compelteMailBody = compelteMailBody.replaceAll("%REPLACE2%",data.getReplace2());
         }
         if(data.getReplace3()!=null){
            compelteMailBody = compelteMailBody.replaceAll("%REPLACE3%",data.getReplace3());
         }

         String recipientUrl = ApplicationProperty.getProperty("stampit.clickthrough.recipient.link");
         compelteMailBody = compelteMailBody.replace(recipientUrl,("&email="+data.getEmailId()));

         String recipientName = ApplicationProperty.getProperty("stampit.clickthrough.name.link");
         compelteMailBody = compelteMailBody.replace(recipientName,("&name="+toName+"&cpid="+cpid));
         */
               
         return compelteMailBody;
    }

    private String convertStampToURLReadyString(String stamp) {
        String urlReadyStamp = null;
        try {
        	urlReadyStamp = URLEncoder.encode(stamp,"utf-8");
        } catch (UnsupportedEncodingException e) {
            return stamp;
        }

        return urlReadyStamp;
    }

   
   

   
}
