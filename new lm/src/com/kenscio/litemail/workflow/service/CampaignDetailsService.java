package com.kenscio.litemail.workflow.service;

import com.kenscio.litemail.workflow.domain.Campaign;

import com.kenscio.litemail.workflow.domain.CampaignShowcase;
import com.kenscio.litemail.workflow.domain.ListEntity;
import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.common.MoneyValue;
import com.kenscio.litemail.workflow.domain.CampaignLinks;

import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;


public class CampaignDetailsService {
   
	
    public Campaign getcampaignDetails(Long campaignId) {
    	CampaignService campaignService = new CampaignService();
        Campaign campaign=campaignService.getCampaign(campaignId);
        
        return campaign;
    }

      public Campaign getPreviewCampaignDetails(Long campaignId){
    	CampaignService campaignService = new CampaignService();
        Campaign campaign=campaignService.getCampaign(campaignId);
        getCampaignMessageWithReplacedOriginalLinks(campaign);
        return campaign;
    }

    private void getCampaignMessageWithReplacedOriginalLinks(Campaign campaign){
        String redirectedUrl = ApplicationProperty.getProperty("stampit.clickthrough.redirect.url");
        String redirectedRecipient = ApplicationProperty.getProperty("stampit.clickthrough.recipient.link");
        String redirectedName = ApplicationProperty.getProperty("stampit.clickthrough.name.link");
        String message=campaign.getCampaignMessage();
        CampaignLinkClicksService campaignLinkClicksService = new CampaignLinkClicksService();
        CampaignLinks[] stampitClicks=campaignLinkClicksService.getStampitClicksByCampaignIdIncludingInactive(campaign.getCampaignId());
        String replacedUrl="";
        int counter=0;
        for (int i = 0; i < stampitClicks.length; i++) {
            CampaignLinks stampitClick = stampitClicks[i];
            int end=0;
            if(counter > 0){
               message=replacedUrl;
            }
            if((end = message.indexOf(redirectedUrl+stampitClick.getUrlId())) >= 0){
                int redirectedUrlLenght=redirectedUrl.length()+stampitClick.getUrlId().toString().length()+redirectedRecipient.length()+redirectedName.length();
                replacedUrl=message.substring(0,end)+stampitClick.getUrl()+message.substring(end+redirectedUrlLenght);
                counter++;
            }
        }
        String str =  replacedUrl.equals("") ? campaign.getCampaignMessage() : replacedUrl;
        campaign.setCampaignMessage(str);
    }

    public boolean isActiveCampaign(Long campaignId) {
    	 CampaignService campaignService = new CampaignService();
         Campaign campaign=campaignService.getCampaign(campaignId);
         if("running".equals(campaign.getStatus())){
             return true;
         }else if("completed".equals(campaign.getStatus()) || "stopped".equals(campaign.getStatus())){
             int campaignDifferenceTime = getCampaignTime(campaign);
             if(campaignDifferenceTime > 0){
                return true;
             }else{
                 return false;
             }
         }else{
            return false;
         }
        
    }

    private int getCampaignTime(Campaign campaign){
        long databasetime = campaign.getCampaignEndDateTime().getTime();
        long currentTime = System.currentTimeMillis();

        Calendar databasebca1endar = Calendar.getInstance();
        Calendar defaultCalendar = Calendar.getInstance();

        databasebca1endar.setTimeInMillis(databasetime);
        defaultCalendar.setTimeInMillis(currentTime);

        float timeInHours = DateUtil.getDifferenceInhours(defaultCalendar, databasebca1endar);
        String diffTimeToRunCampaignInString = ApplicationProperty.getProperty("stampit.startcampaign.defaulttime");
        float diffTimeToRunCampaignInFloat = Float.parseFloat(diffTimeToRunCampaignInString) - timeInHours;
        int diffTimeToRunCampaignInInt = (int) diffTimeToRunCampaignInFloat;
        return diffTimeToRunCampaignInInt ;
    }
    
    public CampaignShowcase[] getShowcaseCampaigns() {
         int maxCampaignSize = 8, activeCont=0, archiveCount=0; // add only 7 active and archive campaigns
         List camps = new ArrayList();
         CampaignService campaignService = new CampaignService();
         Campaign[] campaigns = campaignService.getAllShowcaseCampaigns();
             for (int i = 0; i < campaigns.length; i++) {
                 Campaign campaign = campaigns[i];
                 CampaignShowcase campDto = new CampaignShowcase();
                 int campaignDifferenceTime = getCampaignTime(campaign);
                 if(campaignDifferenceTime > 0 && (maxCampaignSize > activeCont)){
                     campDto.setIsActive(true);
                     campDto.setCampaignSubject(campaign.getSubject());
                     campDto.setCampaignId(campaign.getCampaignId().toString());
                     campDto.setStampitAccountId(campaign.getAccountId().toString());
                     campDto.setClientName(campaign.getClientName());
                     if(activeCont%2 == 0){ // split active campaign into two lines to show at jsp
                        campDto.setFirstLineFlag(true);
                     }else{
                        campDto.setFirstLineFlag(false);
                     }
                     camps.add(campDto);
                     activeCont++;
                 } else if(maxCampaignSize > archiveCount){
                     campDto.setIsActive(false);
                     campDto.setClientName(campaign.getClientName());
                     campDto.setCampaignSubject(campaign.getSubject());
                     campDto.setCampaignId(campaign.getCampaignId().toString());
                     if(archiveCount%2 == 0){ // split archive campaign into two lines to show at jsp
                        campDto.setFirstLineFlag(true);
                     }else{
                        campDto.setFirstLineFlag(false);
                     }
                     camps.add(campDto);
                     archiveCount++;
                 }
             }
         return (CampaignShowcase[])camps.toArray(new CampaignShowcase[camps.size()]);
     }

    
}
