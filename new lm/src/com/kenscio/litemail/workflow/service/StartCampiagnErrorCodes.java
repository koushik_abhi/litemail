package com.kenscio.litemail.workflow.service;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Aug 17, 2006
 * Time: 10:16:29 AM
 * To change this template use File | Settings | File Templates.
 */
public interface StartCampiagnErrorCodes {
    public static String NO_ENOUGH_MONEY_IN_WALLET_BUT_IN_MASTER = "NOMONWALLET";
    public static String NO_ENOUGH_MONEY_IN_WALLET_AND_MASTER = "NOMONWALLETMASTER";
    public static String ERROR_START_CAMPAIGN = "ERRSTARTCAMP";
    public static String ERROR_SMTP = "SMTPFAILURE";
    public static String CAMP_AL_RDY_RUNNING_WTH_SAME_FRM_ADDR="CAMPAIGNALLREDYRUNNINGWTHTHESAMEFRMADDRESS";
}
