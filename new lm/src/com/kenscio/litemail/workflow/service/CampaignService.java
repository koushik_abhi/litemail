
package com.kenscio.litemail.workflow.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;

import org.apache.commons.mail.EmailException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.quartz.SchedulerException;

import com.dataservice.platform.persistence.impl.ListDataDaoImpl;
import com.kenscio.litemail.workflow.dao.CampaignDao;
import com.kenscio.litemail.workflow.dao.MailListDao;
import com.kenscio.litemail.workflow.dao.SmtpDao;
import com.kenscio.litemail.workflow.domain.AccountCredit;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.CampaignLinks;
import com.kenscio.litemail.workflow.domain.CampaignMailFail;
import com.kenscio.litemail.workflow.domain.CampaignReport;
import com.kenscio.litemail.workflow.domain.CampaignStop;
import com.kenscio.litemail.workflow.domain.ClickThroughLinksDTO;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.domain.EmtsResponse;
import com.kenscio.litemail.workflow.domain.JobDetailsObject;
import com.kenscio.litemail.workflow.domain.ListEmailData;
import com.kenscio.litemail.workflow.domain.ListEntity;
import com.kenscio.litemail.workflow.domain.SmtpEntity;
import com.kenscio.litemail.workflow.domain.MailRequestEntity;
import com.kenscio.litemail.exception.CampaignException;
import com.kenscio.litemail.exception.NoValidMailingListException;
import com.kenscio.litemail.exception.SMTPAuthenticationException;
import com.kenscio.litemail.exception.StampingException;
import com.kenscio.litemail.workflow.common.FileUtil;
import com.kenscio.litemail.util.MailUtil;
import com.kenscio.litemail.util.StringUtil;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.common.QuartzJobsServiceImpl;
import com.kenscio.litemail.workflow.common.TemplateEngine;
import com.kenscio.mta.ecircle.service.EcircleService;



public class CampaignService {
	
	
	public boolean previewTestCampaign(Long accountId, String campaignName, List<String> testEmailIds)
			throws NoValidMailingListException, SMTPAuthenticationException, EmailException {

		
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		SmtpDao  smtpdao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		
		SmtpEntity smtp = smtpdao.getSmtpDetail(campaign.getSmtpServer());
		if(smtp == null )
		{
			System.out.println("SMTP details not found in system , getting default smtp detials from properties file ");
			smtp = new SmtpEntity();
			String server=  ApplicationProperty.getProperty("smtp.sever");
			String relayUserName = ApplicationProperty.getProperty("relay.username");
            String relayPassword = ApplicationProperty.getProperty("relay.password");
            smtp.setSmtpServer(server);
            smtp.setSmtpUser(relayUserName);
            smtp.setSmtpPassword(relayPassword);
                  
			
		}
		
		if (smtp != null && smtp.getSmtpServer() != null
				&& !smtp.getSmtpServer().trim().equals("")) {
			
			if (!isValidSmtp(smtp))
			{
				throw new SMTPAuthenticationException();
			}
		}
		else
		{
			throw new SMTPAuthenticationException("Smtp details not found");
			
		}
		
		if (campaign.getListId() == null) {
			throw new NoValidMailingListException();
		
		} else {
			
			campaign.setSmtpServer(smtp.getSmtpServer());
			campaign.setSmtpUserName(smtp.getSmtpUser());
			campaign.setSmtpPassword(smtp.getSmtpPassword());
			
			SMTPServerProxy proxy = createSMTPConnectionProxy(campaign, 10);
			proxy.Init();
			Message[] email = getEmailMessageToSend(campaign, testEmailIds);
			int fail = 0;
			for(int i = 0; i<email.length; i++)
			{
				if(!proxy.send(email[i]))
					fail++;
		    }
			
			if(fail > 0)
				throw new EmailException("Couldn't send message ");
			System.out
					.println("test preview mail sent..." +testEmailIds);
			
			
			
		}
		
		return true;
	}

	private Message[] getEmailMessageToSend(Campaign campaign, List<String> testEmailIds) {
		EmailData[] emailIds = getEmailData(testEmailIds);
		return getStampPreviewTestMailList(campaign, emailIds);
		
	}	
	
	private EcircleService  eCircleService = new EcircleService();	
	private EmailData[] getEmailData(List<String> testEmailIds) {
		
		List<EmailData> emaildata = new ArrayList<EmailData>();
		for(String em : testEmailIds)
		{
			EmailData email = new EmailData();
			email.setEmailId(em);
			email.setName(em);
			email.setStamp("test_stamp");
			emaildata.add(email);
			
		}			
		return (EmailData[]) emaildata.toArray(new EmailData[emaildata.size()]);
	}
	
	public Object startCampaign(Long campaignId, Long mailsPerMin) throws Exception {
		
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(campaignId);
		if(campaign.getStatus()!= null && campaign.getStatus().equalsIgnoreCase("scheduled"))
		{
			removeScheduleJob(campaign);
		}
		if(campaign.getStatus()!= null && campaign.getStatus().equalsIgnoreCase("running"))
		{
			throw new Exception("campaign is already running "+ campaign.getStatus());
		}
		else if(campaign.getDeleteCampaignFlag())
		{
			throw new Exception("campaign marked for delete, can not run it "+ campaign.getStatus());
			
		}else
		{
			campaign.setCampaignSpeed(mailsPerMin);
			return startCampaign(campaign);
		}
		
	}
	
	private void removeScheduleJob(Campaign campaign) {
		JobDetailsObject job = new JobDetailsObject();
		job.setCampaignName(campaign.getCampaignName());
		job.setListName(getListName(campaign));
		try {
			QuartzJobsServiceImpl.getInstance().deleteJob(job);
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	private String getListName(Campaign campaign) {
		MailListServiceImpl im = new MailListServiceImpl();
		return im.getListNameOnId(campaign.getAccountId(), campaign.getListId());
	}

	public Object startCampaignSchedule(String campaignName) throws Exception {
		
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(campaignName);
		if(campaign.getStatus()!= null && campaign.getStatus().equalsIgnoreCase("running"))
		{
			throw new Exception("campaign state is already running "+ campaign.getStatus());
		}
		else if(campaign.getDeleteCampaignFlag())
		{
			throw new Exception("campaign marked for delete, can not run it "+ campaign.getStatus());
			
		}else
		{
			return startCampaign(campaign);
		}
		
	}
	
	public Object startCampaign(Campaign campaign)
			throws NoValidMailingListException, SMTPAuthenticationException {

		
		SmtpDao  smtpdao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		SmtpEntity smtp = smtpdao.getSmtpDetail(campaign.getSmtpServer());
		if(smtp == null )
		{
			System.out.println("SMTP details not found in system , getting it from properties file ");
			smtp = new SmtpEntity();
			String server=  ApplicationProperty.getProperty("smtp.sever");
			String relayUserName = ApplicationProperty.getProperty("relay.username");
            String relayPassword = ApplicationProperty.getProperty("relay.password");
            smtp.setSmtpServer(server);
            smtp.setSmtpUser(relayUserName);
            smtp.setSmtpPassword(relayPassword);
			
		}
		if (smtp != null && smtp.getSmtpServer() != null
				&& !smtp.getSmtpServer().trim().equals("")) {
			
			if (!isValidSmtp(smtp))
			{
				throw new SMTPAuthenticationException();
			}
		}else
		{
			throw new SMTPAuthenticationException("Smtp Details not found");
		}
		
		if (campaign.getListId() == null) {
			throw new NoValidMailingListException();
		} else {
			
			CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
			CampaignStop cstop = dao.getCampaignStopRecord(campaign.getAccountId(), campaign.getCampaignId());
			if(cstop != null)
			{
				cstop.setStatus("running");
				dao.updateCampaignStopRecord(cstop);
			}
			
			long fileSeeker = 0l;
			if(cstop != null)
			{
				fileSeeker = cstop.getStopRecId();
			}
			campaign.setStatus("running");
			dao.update(campaign);
			
			campaign.setSmtpServer(smtp.getSmtpServer());
			campaign.setSmtpUserName(smtp.getSmtpUser());
			campaign.setSmtpPassword(smtp.getSmtpPassword());
			
			BulkMailer bulkMailer = new BulkMailer(campaign.getCampaignSpeed());
			
			bulkMailer.setFileSeeker(fileSeeker);
						
		
			int threadCount = bulkMailer.getRequiredThreadCount(campaign.getCampaignSpeed());
			if(threadCount <=100)
    			threadCount = threadCount*2;
    		
    		if(threadCount >100 && threadCount <=200)
    			threadCount = (threadCount*3)/2;
    		
    		if(threadCount > 350)
    		{
    			threadCount = 500;
    		}
    		
    		System.out.println("connection smtp is creating start "+ threadCount);
    		int connections = threadCount;
			bulkMailer.setSMTPServer(createSMTPConnectionProxy(campaign,connections));
			//bulkMailer.setSMTPServer(createSMTPConnectionProxy(campaign, bulkMailer.getRequiredThreadCount(campaign.getCampaignSpeed())));
			bulkMailer.setCampaign(campaign);
			//bulkMailer.setEmailIds(emailIds); //commented for performance
			// testing
			bulkMailer.setTotalNoMails(getValidEmailCountForList(campaign));
			
			bulkMailer.getSMTPServer().Init();
			//bulkMailer.setCampaignService(this);
            
			MailerDemon mailerDemon = new MailerDemon();
			mailerDemon.setBm(bulkMailer);
			mailerDemon.setCId(bulkMailer.getCampaign().getCampaignId());
			setCampaignIntialStatus(campaign.getCampaignId(), bulkMailer);
			mailerDemon.setCampaignService(this);
		//	mailerDemon.setECircleService(eCircleService);
			mailerDemon.start();
			System.out
					.println("campaign running.....");
			return mailerDemon;
		}

	}

	public void updateScheduleStatus(Long campaignId) {
		try {

			CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
			Campaign campaign = dao.getCampaign(campaignId);
			campaign.setStatus("scheduled");
			dao.saveCampaign(campaign);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private long getValidEmailCountForList(Campaign campaign) 
	{
		ListDataDaoImpl imp = new ListDataDaoImpl();
		return imp.getValidEmailCountForCampaignList(campaign.getListId());
		
	}

	public EmailData[]  getEmailsIdsbyBatch(BulkMailer bm) {
		List<EmailData> actualMailList = new ArrayList<EmailData>();
		MailListServiceImpl emailListService = new MailListServiceImpl();
		Map<Long, List<EmailData>> map = emailListService.getEmailListFromDB(bm.getCampaign().getListId(), bm.getFileSeeker(), 1000);
		Set keys = map.keySet();
		if (keys.iterator().hasNext()) {
			Long nextLocationToRead = (Long) keys.iterator().next();
			actualMailList = map.get(nextLocationToRead);
			bm.setFileSeeker(nextLocationToRead);
		}
				
		return (EmailData[]) actualMailList.toArray(new EmailData[actualMailList.size()]);
	}
	
	public EmailData[] getEmailsIdsbyBatchSpringJDBC(BulkMailer bm) {
		List<EmailData> actualMailList = new ArrayList<EmailData>();
		ListDataDaoImpl dao = new ListDataDaoImpl();
		Map<Long, List<EmailData>> map = dao.getEmailsIdsbyBatchSpringJDBC(bm.getCampaign().getListId(), bm.getFileSeeker(), 2000);
		Set keys = map.keySet();
		if (keys.iterator().hasNext()) {
			Long nextLocationToRead = (Long) keys.iterator().next();
			actualMailList = map.get(nextLocationToRead);
			bm.setFileSeeker(nextLocationToRead);
		}
		
		
		return (EmailData[]) actualMailList.toArray(new EmailData[actualMailList.size()]);
	}
	
	private long getValidEmailCountForCampaignList(Campaign campaign) 
	{
		ListDataDaoImpl dao = new ListDataDaoImpl();
		
		return dao.getValidEmailCountForCampaignList(campaign.getListId());
		
	}
	
	
		
	public Message[] getStampPreviewTestMailList(Campaign campaign, EmailData[] emailIds)
	{

		MailRequestEntity stampMailRequest = createStampMailRequest(campaign,
				emailIds);
		stampMailRequest.setBatchStartPosition(0);
		Message[] mailsGotStamped = null;
		try {
			OivMailService oivMailService = new OivMailService();
			mailsGotStamped = oivMailService.getOivPreviewTestMail(
					stampMailRequest, campaign);
		} catch (StampingException se) {
			se.printStackTrace();
			//throw new CampaignException();
		}
		return (mailsGotStamped);

	}

	public Message[] getStampMailList(Campaign campaign, EmailData[] emailIds)
			throws CampaignException {

		MailRequestEntity stampMailRequest = createStampMailRequest(campaign,
				emailIds);
		stampMailRequest.setBatchStartPosition(0);
		Message[] mailsGotStamped = null;
		try {
			OivMailService oivMailService = new OivMailService();
			mailsGotStamped = oivMailService.getOivStampedMail(
					stampMailRequest, campaign);
		} catch (StampingException se) {
			se.printStackTrace();
			throw new CampaignException();
		}
		return (mailsGotStamped);

	}
	
	private SMTPServerProxy createSMTPConnectionProxy(Campaign campaign, int connectionSize) {
		String smtpServer = ApplicationProperty.getProperty("smtp.sever");
		String bounceEmail = ApplicationProperty.getProperty("bounce.email");
		String userName = ApplicationProperty.getProperty("relay.username");
		String password = ApplicationProperty.getProperty("relay.password");
		
		int connectionLimit = connectionSize;//Integer.parseInt(ApplicationProperty.getProperty(
				//"mail.connection.count").trim());
		
		int msgLimit=Integer.parseInt(ApplicationProperty.getProperty(
		"mail.connection.maxmsgs").trim());

		if (campaign.getSmtpServer() != null
				&& !("".equals(campaign.getSmtpServer()))) {
			smtpServer = campaign.getSmtpServer();
			userName = campaign.getSmtpUserName();
			password = campaign.getSmtpPassword();
			bounceEmail = campaign.getFromAddress();

		}
		SMTPServerProxy proxy = new SMTPServerProxy(smtpServer, 25, userName,
				password, bounceEmail, connectionLimit, 20, true,
				false, false);
		return proxy;
	}
   
	private MailRequestEntity createStampMailRequest(Campaign campaign,
			EmailData[] emailIds) {

		MailRequestEntity stampMailRequest = new MailRequestEntity();
		stampMailRequest.setReplyName(campaign.getReplyName());
		stampMailRequest.setFromEmail(campaign.getFromAddress());
		stampMailRequest.setFromName(campaign.getFromName());
		if (campaign.getReplyTo() != null
				&& !("".equals(campaign.getReplyTo()))) {
			stampMailRequest.setReplyTo(campaign.getReplyTo());
		} else {
			stampMailRequest.setReplyTo(campaign.getFromAddress());
		}
	
		String mailBody = getCompleteMailBody(campaign);
		stampMailRequest.setMailBody(mailBody);
		if (campaign.getAttachmentFileName() != null) {
			String attachmentPath = getAttchmentPath(campaign);
			File attachmentFile = new File(attachmentPath);
			stampMailRequest.setAttachement(attachmentFile);
		}
		stampMailRequest.setCampaignName(campaign.getCampaignName());
		setSMTPDetailsInStampMailRequest(campaign, stampMailRequest);
		// stampMailRequest.setSmtpServer(getSMTPServer(campaign));
		stampMailRequest.setSubject(campaign.getSubject());
		stampMailRequest.setEmailDatas(emailIds);
		return stampMailRequest;
	}

	private void setSMTPDetailsInStampMailRequest(Campaign campaign,
			MailRequestEntity stampMailRequest) {
		if (campaign.getSmtpServer() != null
				&& !("".equals(campaign.getSmtpServer()))) {
			stampMailRequest.setSmtpServer(campaign.getSmtpServer());
			stampMailRequest.setSmptpUserName(campaign.getSmtpUserName());
			stampMailRequest.setSmtpPassword(campaign.getSmtpPassword());
			stampMailRequest.setSmtpRelayEmail(campaign.getFromAddress());
			stampMailRequest.setBounceEmail(campaign.getFromAddress());
		} else {
			String smtpServer = ApplicationProperty.getProperty("smtp.sever");
			String relayEmail = ApplicationProperty.getProperty("relay.mail");
			String userName = ApplicationProperty.getProperty("relay.username");
			String password = ApplicationProperty.getProperty("relay.password");
			stampMailRequest.setSmtpServer(smtpServer);
			stampMailRequest.setSmptpUserName(userName);
			stampMailRequest.setSmtpPassword(password);
			stampMailRequest.setSmtpRelayEmail(relayEmail);
			/*stampMailRequest.setFromName(campaign.getFromName() + " ("
					+ campaign.getFromAddress() + ")");*/
			stampMailRequest.setBounceEmail(ApplicationProperty
					.getProperty("bounce.email"));
		}

	}

	private String getAttchmentPath(Campaign campaign) {
		String filePath = ApplicationProperty
				.getProperty("stampit.attachment.dir")
				+ "/"
				+ campaign.getAccountId()
				+ "/"
				+ campaign.getCampaignId()
				+ "/"
				+ campaign.getAttachmentFileName();
		return filePath;
	}

	private String getCompleteMailBody(Campaign campaign) {
		String compelteMailBody = campaign.getCampaignMessage();
		/*
		 * if(campaign.getAttachmentFileName() != null){ compelteMailBody +=
		 * getAttachmentHTML(campaign); }
		 */
		compelteMailBody += ""; //getFooterHTML(campaign);
		return compelteMailBody;
	}

	private String getFooterHTML(Campaign campaign) {
		String serverName = ApplicationProperty
				.getProperty("current.server.full.path");
		Map values = new HashMap();
		values.put("server", serverName);
		values.put("accountId", campaign.getAccountId());
		values.put("campaignId", campaign.getCampaignId());
		values.put("postalAddress", campaign.getPostalAddress());
		values.put("fromAddress", campaign.getFromAddress());
		return TemplateEngine.getInstnace().getEmailBody("campaignFooter.htm", values);
	}

	
	public void setCampaignIntialStatus(Long id, BulkMailer bulkMailer) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		Campaign campaign = dao.getCampaign(id);
	
		campaign.setCampaignStartDateTime(new Timestamp(System
				.currentTimeMillis()));
		long totalNoOfMails = campaign.getNoOfMails() != null ? campaign
				.getNoOfMails() : new Long(0);

		// --------------------------------------------------------------------------------//
		Long temp = campaign.getMailsSent() != null ? campaign.getMailsSent()
				: new Long(0);
		bulkMailer.setCountOfMailsSentInPreviousCampaigns(temp);
		temp = campaign.getMailsFailed() != null ? campaign.getMailsFailed()
				: new Long(0);
		bulkMailer.setCountOfMailsFailedInPreviousCampaigns(temp);
		// -------------------------------------------------------------------------------//

		totalNoOfMails = totalNoOfMails + bulkMailer.getTotalNoMails();
		campaign.setNoOfMails(totalNoOfMails);
		long totalMailsSent = campaign.getMailsSent() != null ? campaign
				.getMailsSent() : new Long(0);
		campaign.setMailsSent(totalMailsSent);
		campaign.setCampaignEndDateTime(null);
		campaign.setStatus("running");
		Long startCount = campaign.getCampaignStartCount() != null ? campaign.getCampaignStartCount() : 0L;
		campaign.setCampaignStartCount(startCount+1);
		dao.saveCampaign(campaign);
	}

	public synchronized void saveCampaignStatus(Long id, BulkMailer bm) {
		Campaign campaign = null;
		int trial = 1;
		try {
			CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
			
			campaign = dao.getCampaign(id);
		
			do {
							
				bm.setCampaign(campaign);
				bm.saveCount();
				//saveSentEmails(bm);
			} while (!updateCampaignStatus(campaign) && trial < 4);
		} catch (Exception e) {
			System.out.println("Exception in saveCampaignStatus method!!!!!!");
			e.printStackTrace();
		}
	}

	public synchronized void saveCampaignStoppedStatus(BulkMailer bulkMailer,
			Long campaignId) {
		Campaign campaign = null;
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		
		do {
			campaign = dao.getCampaign(campaignId);
			campaign.setStatus("stopped");
			bulkMailer.setCampaign(campaign);
			//saveSentEmails(bulkMailer);
			campaign.setCampaignEndDateTime(new Timestamp(System
					.currentTimeMillis()));
		} while (!updateCampaignStatus(campaign));
	}

	public synchronized void saveCampaignFinalStatus(BulkMailer bulkMailer,
			Long campaignId) {
		Campaign campaign = null;
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		do {
			campaign = dao.getCampaign(campaignId);
			campaign.setStatus("completed");
			bulkMailer.setCampaign(campaign);
			bulkMailer.saveCount();
			//saveSentEmails(bulkMailer);
			campaign.setCampaignEndDateTime(new Timestamp(System
					.currentTimeMillis()));
		} while (!updateCampaignStatus(campaign));
	}

	
	public boolean isEnoughMoneyToRunCampaign(Long campaignId) {

		Campaign campaign = getCampaign(campaignId);
		AccountService accountService = new AccountService();
		AccountCredit account = accountService.getUserCredit(campaign
				.getAccountId());
		long emailCount = getEmailCount(campaign);
		long campaignBalance = getCampaignBalance(campaignId);
		boolean isEnoughMoneyForCampaign = isEnoughMoneyForCampaign(
				emailCount, campaignBalance, account);
		if (isEnoughMoneyForCampaign) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isEnoughMoneyForCampaign(long emailCount,
			long campaignBalance, AccountCredit account) {
		// TODO Auto-generated method stub
		return true;
	}

	private long getEmailCount(Campaign campaign) {
		// TODO Auto-generated method stub
		return 0;
	}

	private long getCampaignBalance(Long campaignId) {
		// TODO Auto-generated method stub
		return 0;
	}

		
	public Campaign[] getCampaignsByAccountId(Long accountId) {

		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		List campaignList = dao
				.getCampaignsByAccountId(accountId);
		return (Campaign[]) campaignList.toArray(new Campaign[campaignList
				.size()]);
	}

	public Campaign[] getCampaignsByKeyword(Long accountId, String keyword) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		return dao.getCampaignsByKeywordandAccountId(
				accountId, keyword);
	}

	public Campaign getCampaign(Long campaignId) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		Campaign campaign =dao.getCampaign(campaignId);
		SmtpDao  smtpdao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		
		SmtpEntity smtp = smtpdao.getSmtpDetail(campaign.getSmtpServer());
	
		if(smtp != null)
		{
			campaign.setSmtpServer(smtp.getSmtpServer());
			campaign.setSmtpUserName(smtp.getSmtpUser());
			campaign.setSmtpPassword(smtp.getSmtpPassword());
			
		}
		return campaign;
		
	}

	public boolean saveImageFileInFolder(Campaign campaign, Long accountId,
			long maxCampaignIdForImgName) {
		String imageRoot = ApplicationProperty.getProperty("stampit.image.dir");
		String imageFileName = "stampit-" + maxCampaignIdForImgName + "."
				+ ApplicationProperty.getProperty("stampit.image.ext");
		String imageFullFilePath = FileUtil.getFullFilePath(imageRoot,
				accountId, campaign.getCampaignId(), imageFileName);
		if (FileUtil.saveFileToDisk(imageFullFilePath, campaign
				.getImageFileData())) {
			return true;
		} else {
			return false;
		}
	}

	public void saveFailedEmails(BulkMailer bulkMailer) throws Exception {
		List failedStampedMails = bulkMailer.getFailedStampedEmails();
		EmailData[] failedMails2 = getEmailDataFromStampedMails(failedStampedMails);
		EmailData[] failedMails3 = bulkMailer.getMailsFailedInStamping();
		List<EmailData> mainList = new ArrayList<EmailData>();

		if (failedMails2 != null && failedMails2.length > 0) {
			mainList.addAll(Arrays.asList(failedMails2));
			bulkMailer.getFailedStampedEmails().clear();
			bulkMailer.clearMailsFailedInSMTP();
		}
		if (failedMails3 != null && failedMails3.length > 0) {
			mainList.addAll(Arrays.asList(failedMails3));
			bulkMailer.setMailsFailedInStamping(null);
		}

		if (mainList.size() > 0) {
			System.out.println("Saving  failed mails =" + mainList.size());
			CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
			
			for(EmailData data : mainList)
			{
				CampaignMailFail fail = new CampaignMailFail();
				fail.setAccountId(bulkMailer.getCampaign().getAccountId());
				fail.setListId(bulkMailer.getCampaign().getListId());
				fail.setCampaignId(bulkMailer.getCampaign().getCampaignId());
				fail.setEmail(data.getEmailId());
				fail.setName(data.getName());
				dao.saveCampignFail(fail);
			}
			
		}

	}

	public String getSentEmailsFileFullPath(Long campaignId) {
		String folderPath = ApplicationProperty.getProperty("stampit.csv.home")
				+ "/sentemails" + "/" + campaignId.toString();

		FileUtil.createDirectory(folderPath);
		String csvFullFilePath = folderPath +
		// "/" + StringUtil.getValidCSVFileName(mailingListName) +
				"/sentemails" + "."
				+ ApplicationProperty.getProperty("stampit.csv.ext");
		return csvFullFilePath;
	}

	public EmailData[] getEmailDataFromStampedMails(List failedStampedMails)
			throws Exception {

		if (failedStampedMails == null || failedStampedMails.size() <= 0)
			return null;
		List emailDatas = new ArrayList(failedStampedMails.size());
		for (Iterator j = failedStampedMails.iterator(); j.hasNext();) {
			Message m = (Message) j.next();
			Address recipients[] = m.getAllRecipients();
			String id = recipients[0].toString().trim();
			EmailData emailData = new EmailData();
			String name = StringUtil.substringBeforeFromEnd(id, "<");
			name = StringUtil.removeWhitespace(name);
			String emailWithClosingBraket = StringUtil.substringAfterFromEnd(
					id, "<");
			String email = StringUtil.substringBeforeFromEnd(
					emailWithClosingBraket, ">");
			emailData.setName(name);
			emailData.setEmailId(email);
			emailDatas.add(emailData);
		}
		return (EmailData[]) emailDatas
				.toArray(new EmailData[emailDatas.size()]);
	}

	private EmailData getEmailDataFromStampedMail(Object mail) throws Exception {

		Message m = (Message) mail;
		if(m==null)return null;
		Address recipients[] = m.getAllRecipients();
		String id = recipients[0].toString().trim();
		EmailData emailData = new EmailData();
		String name = StringUtil.substringBeforeFromEnd(id, "<");
		name = StringUtil.removeWhitespace(name);
		String emailWithClosingBraket = StringUtil.substringAfterFromEnd(id,
				"<");
		String email = StringUtil.substringBeforeFromEnd(
				emailWithClosingBraket, ">");
		emailData.setName(name);
		emailData.setEmailId(email);

		return emailData;
	}

	public long getMoneyNeededToRunCampaign(Long campaignId) {
		return 0;
	}

	public long getMaxCampaignId() {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		long maxCampaignId = dao.getMaximumCampignId();
		return maxCampaignId;
	}

	private Boolean isValidSmtp(SmtpEntity smtp) {
		String userid = smtp.getSmtpUser();
		String pwd = smtp.getSmtpPassword();
		String server = smtp.getSmtpServer();
		if ((userid != null && !userid.equals("")))
		{
			boolean falg = MailUtil.authenticateSMTPServer(server, userid, pwd);
			return falg;
		}
		else
		{
			return false;
		}
	}

	public void saveEditedCampaign(Campaign campaign) {
		//createChatAccount(campaign);
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		//Campaign camp = dao.getCampaign(campaign.getCampaignName());
		//updateCampaignObject(camp, campaign);
		dao.updateCampaign(campaign);
	}
	
	public Campaign getCampaign(long accountId, String name) {
		//createChatAccount(campaign);
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign camp = dao.getCampaign(accountId, name);
		return camp;
	}

	private void updateCampaignObject(Campaign camp, Campaign campaign) {
		camp.setAttachedFileData(campaign.getAttachedFileData());
		camp.setAttachmentFileName(campaign.getAttachmentFileName());
		camp.setCampaignMessage(campaign.getCampaignMessage());
		camp.setDescription(campaign.getDescription());
		camp.setEnableLinkTracking(campaign.getEnableLinkTracking());
		camp.setEnableOpenTracking(campaign.getEnableOpenTracking());
		camp.setFromAddress(campaign.getFromAddress());
		camp.setFromName(campaign.getFromName());
		camp.setHtmlFileName(campaign.getHtmlFileName());
		camp.setListId(campaign.getListId());
		camp.setListName(campaign.getListName());
		camp.setMailContentType(campaign.getMailContentType());
		camp.setPostalAddress(campaign.getPostalAddress());
		camp.setReplyName(campaign.getReplyName());
		camp.setReplyTo(campaign.getReplyTo());
		camp.setSmtpServer(campaign.getSmtpServer());
		camp.setSubject(campaign.getSubject());
		camp.setUpdatedDate(new Date());
		camp.setOpenTrackingSrc(campaign.getOpenTrackingSrc());
		camp.setUnsubTrackingUrl(campaign.getUnsubTrackingUrl());
		
		
	}

	public void updateCampaignRedeemNumber(Long campaignId) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.updateRedeemCounterByOne(campaignId);
	}

	public boolean getAckStatus(Long campaignId) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao.getAckStatus(campaignId);
	}

	public String getSubject(Long campaignId) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao.getSubject(campaignId);
	}

	public List getCampaignIdsWithSameFromEmailAddress(String fromEmailAddress) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao
				.getCampaignIdsWithSameFromEmailAddress(fromEmailAddress);
	}
	
	public Campaign getCampaignIdsWithName(long accountId, String campaignName) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao.getCampaign(accountId, campaignName);
	}

	
	public long getMaxCampignIdForImgName() {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		long maxCampId = dao.getMaximumCampignId();
		return maxCampId;
	}

	public ArrayList campaignIsRunningwithSameFromEmailId(String fromEmail) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign[] campaigns = dao
				.getAllCampaignswithSameFromEmailIds(fromEmail);
		ArrayList al = new ArrayList();
		for (int i = 0; i < campaigns.length; i++) {
			if ("running".equalsIgnoreCase(campaigns[i].getStatus())) {
				al.add(campaigns[i].getCampaignName());
			} else {
				continue;
			}
		}
		return al;
	}

	public Campaign[] getAllShowcaseCampaigns() {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao.getAllShowcaseCampaigns();
	}

	public List getActiveShowcaseCampaignsByEndDate(String endDateTime) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao
				.get8ActiveShowcaseCampaignsByEndDate(endDateTime);
	}

	public List getExpiredShowcaseCampaignByEndDate(String endDateTime) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao
				.get4ExpiredShowcaseCampaignByEndDate(endDateTime);
	}

			

	public Campaign[] getCampaignDetailsByKeyword(String keyword) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao.getCampaignsByKeyword(keyword);
	}
    
	private boolean updateCampaignStatus(Campaign campaign) {
		try {
			CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
			dao.updateCampaign(campaign);
			return true;
		} catch (RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public boolean isCampaignExist(String campaingName) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(campaingName);
		if(campaign != null)
			return true;
		else
			return false;
	}

	public void deleteCampaign(long accountId, String campaignName) throws Exception {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		if(campaign.getStatus() != null && campaign.getStatus().equalsIgnoreCase("running"))
			throw new Exception("campaign running can not be deleted "+ campaign.getCampaignName());
		else
		{
			campaign.setDeleteCampaignFlag(true);
			
			dao.update(campaign);
			removeScheduleJob(campaign);
		}
		
		
	}

	public boolean validStateForDelete(long accountId, String campaignName) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		if(campaign.getStatus() != null && campaign.getStatus().equalsIgnoreCase("running"))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public boolean validStateForUpdate(long accountId, String campaignName) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		if(campaign.getStatus() != null && campaign.getStatus().equalsIgnoreCase("running"))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public List<Campaign> getCampaignsInPagination(Long accountId,
			int pagenumber, int noOfRecords) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao.getAllCampaignInPagination(accountId, pagenumber, noOfRecords);
		
	}

	public CampaignReport getCampaignStatusReport(long accountId,
			String campaignName) {
		// TODO Auto-generated method stub
		return null;
	}

	public void copyCampaign(Long accountId, Campaign campaign) {
		campaign.setAccountId(accountId);
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		campaign.setAccountId(accountId);
		dao.saveCampaign(campaign);
		
	}

	public void createCampaign(Long accountId, Campaign campaign) {
		campaign.setAccountId(accountId);
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.saveCampaign(campaign);
				
	}

	public void updateCampaign(Long accountId, String campaignName,
			String listName, String smtpServer) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		campaign.setSmtpServerName(smtpServer);
		MailListServiceImpl imp = new MailListServiceImpl();
		long listId = imp.getListIdOnName(accountId, listName);
		campaign.setListId(listId);
		dao.updateCampaign(campaign);
		
		
	}

	public ArrayList getClickThroughData(Long campaignIdInLong) {
		// TODO Auto-generated method stub
		return null;
	}

	public String getCampaignMessage(Long accountId, String campaignName) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		if(campaign != null)
			return campaign.getCampaignMessage();
		return "";
	}
		
	public int getTotalPagesOfCampaign(String partnerName) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao.getTotalPagesOfCampaign(partnerName);
	}
	

	public Campaign[] searchCampaignWildCard(Long accountId, String campaignName) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign[]  campaigns = dao.getCampaignsByKeywordandAccountId(accountId, campaignName);
		return campaigns;
	}

	public void stopCampaignUpdate(Long accountId, String campaignName, long stopRecId)
	{
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		CampaignStop stop = dao.getCampaignStopRecord(accountId, campaign.getCampaignId());
		if(stop != null)
		{
			stop.setStopRecId(stopRecId);
			stop.setUpdatedDate(new Date());
			stop.setUpdatedBy(accountId+"");
			dao.updateCampaignStopRecord(stop);
		}
		campaign.setStatus("stopped");
		campaign.setUpdatedDate(new Date());
		campaign.setCampaignEndDateTime(new Timestamp(System
				.currentTimeMillis()));
		dao.update(campaign);
				
	}

	public void stopCampaign(Long accountId, String campaignName) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		CampaignStop stop = new CampaignStop();
		stop.setAccountId(accountId);
		stop.setCampaignId(campaign.getCampaignId());
		stop.setStatus("stopped");
		stop.setCreatedDate(new Date());
		stop.setUpdatedDate(new Date());
		stop.setCreatedBy(accountId+"");
		dao.addStopCampaignRecord(stop);
		if(campaign.getStatus() != null && campaign.getStatus().equalsIgnoreCase("running"))
		{
			campaign.setStatus("stopping...");
			campaign.setUpdatedDate(new Date());
			dao.update(campaign);
		}
		
		
	}

	public boolean validStatusForStop(Long accountId, String campaignName) {

		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		if(campaign.getStatus() != null && campaign.getStatus().equalsIgnoreCase("running"))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	private boolean saveAttachmentFileNewCampaign(Campaign campaign) {
        if(campaign.getAttachmentFileName() != null) {
            String imageRoot = ApplicationProperty.getProperty("stampit.attachment.dir") ;
            String fullFilePath = FileUtil.getFullFilePath(imageRoot,campaign.getAccountId(),campaign.getCampaignId(),campaign.getAttachmentFileName());
            if(FileUtil.saveFileToDisk(fullFilePath,campaign.getAttachedFileData())){
                return true;
            }else{
                return false;
            }
           
        }
        return true;
        
	 }
	
	private boolean saveAttachmentFileCopyCampaign(Campaign campaign) {
		if(campaign.getAttachmentFileName()!=null){
            String imageRoot = ApplicationProperty.getProperty("stampit.attachment.dir") ;
            String copyFullFilePath = FileUtil.getFullFilePath(imageRoot,campaign.getAccountId(),campaign.getCampaignId(),campaign.getAttachmentFileName());
            byte[] bytes = FileUtil.getFileByFullFilePath(copyFullFilePath);
            String saveFullFilePath = FileUtil.getFullFilePath(imageRoot,campaign.getAccountId(),campaign.getCampaignId(),campaign.getAttachmentFileName());
            if(FileUtil.saveFileToDisk(saveFullFilePath,bytes)){
                return true;
            }else{
                return false;
            }
        }
        return true;
        
	 }

	public void updateCampaign(Long accountId, String campaignName,
			Long mailsPerMinute, String status, String scheduleDate) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		campaign.setStatus(status);
		campaign.setCampaignSpeed(mailsPerMinute);
		if(scheduleDate != null && scheduleDate.length() > 0)
		{
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm");
			Date date = null;
			try {
				date = sdf.parse(scheduleDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(date != null)
			{
				Timestamp sDate = new Timestamp(date.getTime());
				campaign.setScheduleTime(sDate);
			}
		}
		
		dao.update(campaign);
		
	}
	
	public void updateCampaign(Long accountId, Campaign campaign) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.update(campaign);
	}
	
	private ClickThroughLinksDTO[] getClickThroughStatus(Campaign campaign) {
		CampaignLinkClicksService stampitClicksService = new CampaignLinkClicksService();
		CampaignLinks[] stampitClicks = stampitClicksService.getStampitClicksByCampaignId(campaign.getCampaignId());
        ClickThroughLinksDTO[] clickThroughLinksDTO = createDtoFromStampitClicks(stampitClicks, campaign);
        return clickThroughLinksDTO;
    }
	
    public ClickThroughLinksDTO[] createDtoFromStampitClicks(CampaignLinks[] stampitClicks, Campaign campaign) {
    	HTMLParserService hTMLParserService=  new HTMLParserService();
        List urlList = hTMLParserService.getURLList(campaign.getCampaignMessage());
        ClickThroughLinksDTO[] linksDto = createDtoFromList(urlList);
        if (linksDto.length > 0) {
            linksDto = createDtoFromPresentMessageBody(linksDto, stampitClicks,campaign);
        }
        return linksDto;
    }

    private ClickThroughLinksDTO[] createDtoFromPresentMessageBody(ClickThroughLinksDTO[] linksDto, CampaignLinks[] stampitClicks, Campaign campaign) {
        ArrayList urlClickList = new ArrayList();
        for (int i = 0; i < stampitClicks.length; i++) {
        	CampaignLinks stampitClick = stampitClicks[i];
            for (int j = 0; j < linksDto.length; j++) {
                ClickThroughLinksDTO clickThroughLinksDTO = linksDto[j];
                ClickThroughLinksDTO newLinkDTO = new ClickThroughLinksDTO();
                String oldredirectURL = ApplicationProperty.getProperty("stampit.clickthrough.redirect.url") + stampitClick.getUrlId();
                String newRedirectURL = ApplicationProperty.getProperty("stampit.clickthrough.redirect.url")+stampitClick.getUrlId()+ApplicationProperty.getProperty("stampit.clickthrough.recipient.link");
                 String newWithNameRedirectURL = ApplicationProperty.getProperty("stampit.clickthrough.redirect.url")+stampitClick.getUrlId()+
                                        ApplicationProperty.getProperty("stampit.clickthrough.recipient.link")+ApplicationProperty.getProperty("stampit.clickthrough.name.link");
                if (oldredirectURL.equals(clickThroughLinksDTO.getUrl()) || newRedirectURL.equals(clickThroughLinksDTO.getUrl()) || newWithNameRedirectURL.equals(clickThroughLinksDTO.getUrl())) {
                    newLinkDTO.setUrl(stampitClick.getUrl());
                    newLinkDTO.setUrlName(stampitClick.getUrlName());
                    newLinkDTO.setNoOfClicks(stampitClick.getNoOfClicks());
                    newLinkDTO.setNoOfemails((long)getclickedEmails(stampitClick.getUrlId(),campaign.getCampaignId()).size());
                    newLinkDTO.setUrlId(stampitClick.getUrlId().toString());
                    urlClickList.add(newLinkDTO);
                }
            }
        }
        return (ClickThroughLinksDTO[]) urlClickList.toArray(new ClickThroughLinksDTO[urlClickList.size()]);
    }

    private ArrayList getclickedEmails(Long urlId,Long campaignId) {
        ArrayList emailList = new ArrayList();
        CampaignLinkClicksService stampitClicksService = new CampaignLinkClicksService();
        EmailData[] cilckedEmails = stampitClicksService.getClickThroughEmailListsByUrlId(campaignId,urlId);
        if(cilckedEmails!=null && cilckedEmails.length > 0){
            for (int i = 0; i < cilckedEmails.length; i++) {
                EmailData cilckedEmail = cilckedEmails[i];
                emailList.add(cilckedEmail.getEmailId());
            }
        }
        return emailList;
    }

    private ClickThroughLinksDTO[] createDtoFromList(List urlList) {
        List linklist = new ArrayList();
        int count = 0;
        for (Iterator iterator = urlList.iterator(); iterator.hasNext();) {
        	count++;
            String urlLink = (String) iterator.next();
            ClickThroughLinksDTO linksDTO = new ClickThroughLinksDTO();
            linksDTO.setUrl(urlLink);
            linksDTO.setUrlName(urlLink);
            linksDTO.setPosition(count);
            linklist.add(linksDTO);
        }
        return (ClickThroughLinksDTO[]) linklist.toArray(new ClickThroughLinksDTO[linklist.size()]);
    }
    
    
	public boolean checkIfStdDataOnly(Long listId) {
		ListDataDaoImpl dao = new ListDataDaoImpl();
		String str = dao.checkIfStdDataOnly(listId);
		if(str != null && str.length() > 0)
		{
			return true;
		}else
		{
			return false;
		}
		
	}

	public void createCampaignLinks(Long accountId, Campaign campaign) {
		
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		deleteExistingLinks(accountId, campaign);
		HTMLParserService hTMLParserService = new HTMLParserService();
		List urlList = hTMLParserService.getURLList(campaign.getCampaignMessage());
        List<CampaignLinks> campaignLinks = createLinkList(campaign, campaign.getCampaignId(),accountId, urlList);
       
        for(CampaignLinks camLink : campaignLinks)
        {
        	dao.saveOnly(camLink);
        }
        if(campaign.getEnableOpenTracking() != null && campaign.getEnableOpenTracking().equalsIgnoreCase("on"))
        {
        	campaign.setOpenTrackingSrc(getOpenTrackingSrc(campaign));
        }
        campaign.setUnsubTrackingUrl(getUnsubscribeUrl(campaign));
        updateCampaign(campaign.getAccountId(), campaign);
		
	}	
	private String getUnsubscribeUrl(Campaign campaign) {
		String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		String instance = ApplicationProperty.getProperty("litemail.instance");
		long accountId = campaign.getAccountId();
		String viewUrl ="getUnsubscribeURL.htm";
		String url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+instance+"_"+accountId+"&cId="+campaign.getCampaignId();
		return getEncodedUrlString(url);
	}

	private String getOpenTrackingSrc(Campaign campaign) {
		String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		String instance = ApplicationProperty.getProperty("litemail.instance");
		long accountId = campaign.getAccountId();
		String viewUrl ="getOpensTrackingUrl.htm";
		String url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+instance+"_"+accountId+"&cId="+campaign.getCampaignId();
		return getEncodedUrlString(url);
	}

	private List<CampaignLinks> createLinkList(Campaign campaign, long campaignId, long accountId, List urlList) {
	 	List<CampaignLinks> campLinks = new ArrayList<CampaignLinks>();
        int count = 0;
        for (Iterator iterator = urlList.iterator(); iterator.hasNext();) {
        	count++;
            String urlLink = (String) iterator.next();
            CampaignLinks linksDTO = new CampaignLinks();
            linksDTO.setUrl(urlLink);
            String name = "";
            if(urlLink.length() > 100)
            {
            	name = urlLink.substring(0, 100);
            }
            if(campaign.getEnableLinkTracking() != null && campaign.getEnableLinkTracking().equalsIgnoreCase("on"))
            {
            	linksDTO.setRedirectUrl(getEncodedRedirectUrlFromEmts(count, campaignId, accountId ));
            	
            }else
            {
            	linksDTO.setRedirectUrl(urlLink);
            }
            linksDTO.setCampaignId(campaignId);
            linksDTO.setUrlName(name);
            linksDTO.setPosition(count);
            linksDTO.setCurrentStatus("active");
            linksDTO.setCreatedBy("system");
            linksDTO.setCreatedDate(new Date());
            linksDTO.setUpdatedDate(new Date());
            campLinks.add(linksDTO);
        }
        return campLinks;
	}	    

	private String getEncodedRedirectUrlFromEmts(long position, long campaignId, long accountId) {
		String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		String instance = ApplicationProperty.getProperty("litemail.instance");
		String viewUrl ="getEncodedUrlForCampaign.htm";
		String url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+instance+"_"+accountId+"&cId="+campaignId+"&uId="+position;
		return getEncodedUrlString(url);
		     
	}
	
	
	public String getEncodedUrlString(String url) 
	{
		HttpClient client = new DefaultHttpClient();
		EmtsResponse emtsObject = null;
		String redirecturl = "";
		try{
		 
		  HttpGet request = new HttpGet(url);
		  HttpResponse response = client.execute(request);
		  ObjectMapper wrapper = new ObjectMapper();
		  emtsObject = wrapper.readValue(response.getEntity().getContent(), EmtsResponse.class);
		  client.getConnectionManager().shutdown();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			 client.getConnectionManager().shutdown();
		}
		if(emtsObject != null)
		{
			List objs = emtsObject.getItems();
			if(objs != null && objs.size() > 0)
			{
				redirecturl = (String)objs.get(0);
			}
		}
		return redirecturl;
		
	}

	private void deleteExistingLinks(Long accountId, Campaign campaign) {
		 CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		 dao.deleteExistingLinks(campaign.getCampaignId(), accountId);
		
	}

	public List<CampaignLinks> getCampaingLinks(Long accountId, Long campaignId) {
		 CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		 return dao.getCampaingLinks(accountId, campaignId);
	}

	public CampaignStop getCampaigStopRecord(Long accountId, Long campaignId) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao.getCampaignStopRecord(accountId, campaignId);
	}

	public void resumeCampaign(Long accountId, String campaignName) throws Exception {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		if(campaign.getStatus()!= null && campaign.getStatus().equalsIgnoreCase("scheduled"))
		{
			removeScheduleJob(campaign);
		}
		if(campaign.getStatus()!= null && campaign.getStatus().equalsIgnoreCase("running"))
		{
			throw new Exception("campaign is already running "+ campaign.getStatus());
		}
		else if(campaign.getDeleteCampaignFlag())
		{
			throw new Exception("campaign marked for delete, can not run it "+ campaign.getStatus());
			
		}else
		{
			resumeCampaign(campaign);
		}
		
	}

	private Object resumeCampaign(Campaign campaign) {
		
		SmtpDao  smtpdao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		SmtpEntity smtp = smtpdao.getSmtpDetail(campaign.getSmtpServer());
		if(smtp == null )
		{
			System.out.println("SMTP details not found in system , getting it from properties file ");
			smtp = new SmtpEntity();
			String server=  ApplicationProperty.getProperty("smtp.sever");
			String relayUserName = ApplicationProperty.getProperty("relay.username");
            String relayPassword = ApplicationProperty.getProperty("relay.password");
            smtp.setSmtpServer(server);
            smtp.setSmtpUser(relayUserName);
            smtp.setSmtpPassword(relayPassword);
			
		}
		if (smtp != null && smtp.getSmtpServer() != null
				&& !smtp.getSmtpServer().trim().equals("")) {
			
			if (!isValidSmtp(smtp))
			{
				throw new SMTPAuthenticationException();
			}
		}else
		{
			throw new SMTPAuthenticationException("Smtp Details not found");
		}
		
		if (campaign.getListId() == null) {
			throw new NoValidMailingListException();
		} else {
			
			CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
			CampaignStop cstop = dao.getCampaignStopRecord(campaign.getAccountId(), campaign.getCampaignId());
			
			long fileSeeker = 0l;
			if(cstop != null)
			{
				fileSeeker = cstop.getStopRecId();
				cstop.setStatus("running");
				dao.updateCampaignStopRecord(cstop);
			}
			campaign.setStatus("running");
			dao.update(campaign);
			
			campaign.setSmtpServer(smtp.getSmtpServer());
			campaign.setSmtpUserName(smtp.getSmtpUser());
			campaign.setSmtpPassword(smtp.getSmtpPassword());
			
			BulkMailer bulkMailer = new BulkMailer(campaign.getCampaignSpeed());
			
			bulkMailer.setFileSeeker(fileSeeker);
			int threadCount = bulkMailer.getRequiredThreadCount(campaign.getCampaignSpeed());
			if(threadCount <=100)
    			threadCount = threadCount*2;
    		
    		if(threadCount >100 && threadCount <=200)
    			threadCount = (threadCount*3)/2;
    		
    		if(threadCount > 350)
    		{
    			threadCount = 500;
    		}
    		System.out.println("connection smtp is creating resume "+ threadCount);
    		int connections = threadCount;
			bulkMailer.setSMTPServer(createSMTPConnectionProxy(campaign,connections));
			bulkMailer.setCampaign(campaign);
			//bulkMailer.setEmailIds(emailIds); //commented for performance
			// testing
			bulkMailer.setTotalNoMails(getValidEmailCountForList(campaign));
			
			bulkMailer.getSMTPServer().Init();
			//bulkMailer.setCampaignService(this);
            
			MailerDemon mailerDemon = new MailerDemon();
			mailerDemon.setBm(bulkMailer);
			mailerDemon.setCId(bulkMailer.getCampaign().getCampaignId());
			setCampaignIntialStatus(campaign.getCampaignId(), bulkMailer);
			mailerDemon.setCampaignService(this);
		//	mailerDemon.setECircleService(eCircleService);
			mailerDemon.start();
			System.out
					.println("campaign running resumed ....");
			return mailerDemon;
		}
	}

	public void deleteNewCampaign(Long accountId, String campaignName) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign = dao.getCampaign(accountId, campaignName);
		dao.delete(campaign);
	
		
	}

	public List<Campaign> getCampaignsInPagination(String partnerName,int pagenumber, int noOfRecords) {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return dao.getAllCampaignInPagination(partnerName, pagenumber, noOfRecords);
		
	}
	    	
	

}
