package com.kenscio.litemail.workflow.service;

import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.ClickThroughLinksDTO;
import com.kenscio.litemail.workflow.domain.CampaignLinks;
import com.kenscio.litemail.workflow.domain.URLChangeData;
import com.kenscio.litemail.workflow.common.ApplicationProperty;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Jan 23, 2007
 * Time: 2:38:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClickThroughLinksService {
    private HTMLParserService hTMLParserService = new HTMLParserService();
    private CampaignService campaignService =new CampaignService();
     private CampaignLinkClicksService stampitClicksService = new CampaignLinkClicksService();

    public ClickThroughLinksDTO[] getClickThroughLinks(String campaignId) {
        Campaign campaign = campaignService.getCampaign(Long.parseLong(campaignId));
        List urlList=hTMLParserService.getURLList(campaign.getCampaignMessage());
        ClickThroughLinksDTO[] linksDto=createDtoFromList(urlList);
        return linksDto;
    }

    private ClickThroughLinksDTO[] createDtoFromList(List urlList){
        List linklist =new ArrayList();
        for (ListIterator iterator = urlList.listIterator(); iterator.hasNext();) {
            String  urlLink =(String)iterator.next();
            ClickThroughLinksDTO linksDTO = new ClickThroughLinksDTO();
            linksDTO.setUrl(urlLink);
            linksDTO.setUrlName(urlLink);
            //linksDTO.setReplacedUrlFlag(false);
            linksDTO.setPosition((iterator.nextIndex() - 1));
            linklist.add(linksDTO);
        }
        return (ClickThroughLinksDTO[])linklist.toArray(new ClickThroughLinksDTO[linklist.size()]) ;
    }



    public boolean saveClickThroughLinks(ClickThroughLinksDTO[] linksDTO, int[] selectedPosition,Long campaignId) {
        
        URLChangeData[] urlChangeDatas=createURLChangeDataFromDto(linksDTO,selectedPosition);
        Campaign campaign=campaignService.getCampaign(campaignId);
        isSameURLNameNotPresent(urlChangeDatas,campaign);
        String replacedCampaignMessage=hTMLParserService.replaceSelectedURLs(campaign.getCampaignMessage(),urlChangeDatas,campaignId, campaign);
        campaign.setCampaignMessage(replacedCampaignMessage);
        campaignService.saveEditedCampaign(campaign);
        
        return true;
    }

    private void isSameURLNameNotPresent(URLChangeData[] urlChangeDatas, Campaign campaign) {
        int counter=0;
        Set urlNameList=new TreeSet();
        for (int i = 0; i < urlChangeDatas.length; i++) {
            URLChangeData urlChangeData = urlChangeDatas[i];
            urlNameList.add(urlChangeData.getUrlName());
            
        }
        /**********************************************************************
         The following part checks whether same URL NAME present in database which is alredy selected.
         and the selected URL will have redirected URL at mailbody.
         There is also possibility that user can change mail body.
         Thats the reason here again check is required whether same urlId present
         in the user changed mail body for the same camapign Id
         */
        CampaignLinks[] stampitClicks=stampitClicksService.getStampitClicksByCampaignId(campaign.getCampaignId());
        if(stampitClicks!=null && stampitClicks.length > 0){
            List urlList=hTMLParserService.getURLList(campaign.getCampaignMessage());
            ClickThroughLinksDTO[] linksDto=createDtoFromList(urlList);
            for (int i = 0; i < stampitClicks.length; i++) {
                CampaignLinks stampitClick = stampitClicks[i];
                for (int j = 0; j < linksDto.length; j++) {
                    ClickThroughLinksDTO clickThroughLinksDTO = linksDto[j];
                    int idx=clickThroughLinksDTO.getUrl().indexOf('=');
                    if(idx!=-1){
                        String redirectURL = ApplicationProperty.getProperty("stampit.clickthrough.redirect.url");
                        if(redirectURL.equals(clickThroughLinksDTO.getUrl().substring(0,(idx+1)))) {
                            int lastIndex=0;
                            int jdx=clickThroughLinksDTO.getUrl().indexOf('&');
                            if(jdx!=-1){ // stored with emailLink at URL
                                lastIndex = jdx;
                            }else{ // stored without emailLink at URL which is alredy there at live server
                                lastIndex = clickThroughLinksDTO.getUrl().length();
                            }
                            String mailBodyUrlId=clickThroughLinksDTO.getUrl().substring(idx+1,lastIndex);
                            if(stampitClick.getUrlId().equals(Long.parseLong(mailBodyUrlId))){
                            	urlNameList.add(stampitClick.getUrlName());
                               
                            }
                        }
                    }
                }
            }
        }
       
    }

    public boolean selectedURLNotPresentInStampitClicks(ClickThroughLinksDTO[] linksDTO, CampaignLinks[] stampitClicks,int[] selectedPosition){
         int counter=0;
         for (int i = 0; i < selectedPosition.length; i++) {
             ClickThroughLinksDTO dto=linksDTO[selectedPosition[i]];
             for (int j = 0; j < stampitClicks.length; j++) {
                 CampaignLinks stampitClick = stampitClicks[j];
                 if(dto.getUrl().equals(stampitClick.getUrl())){
                     counter++;
                 }
             }
         }
         return counter == 0;
     }

    public URLChangeData[] createURLChangeDataFromDto(ClickThroughLinksDTO[] linksDTO, int[] selectedPosition){
        List urlLists=new ArrayList();
        for (int i = 0; i < linksDTO.length; i++) {
           URLChangeData uRLChangeData=new URLChangeData();
           ClickThroughLinksDTO dto=linksDTO[i];
          // System.out.println("position" + dto.getPosition());
          // System.out.println("setUrlName" + dto.getUrlName());
           uRLChangeData.setUrlPosition(dto.getPosition());
           uRLChangeData.setUrlName(dto.getPosition()+"");
           urlLists.add(uRLChangeData);
        }
        return (URLChangeData[])urlLists.toArray(new URLChangeData[urlLists.size()]);
    }


     public ClickThroughLinksDTO[] editClickThroughLinks(Long campaignId) {
        Campaign campaign=campaignService.getCampaign(campaignId);
        List urlList=hTMLParserService.getURLList(campaign.getCampaignMessage());
        ClickThroughLinksDTO[] linksDto=createDtoFromList(urlList);
        CampaignLinks[] stampitClicks=stampitClicksService.getStampitClicksByCampaignIdIncludingInactive(campaignId);
       if(stampitClicks.length > 0 ){
           linksDto=removeRedirectedURLsAndPlaceOriginalUrlInDTO(linksDto,stampitClicks);
       }
       return linksDto;
    }

     private ClickThroughLinksDTO[] removeRedirectedURLsAndPlaceOriginalUrlInDTO(ClickThroughLinksDTO[] linksDto, CampaignLinks[] stampitClicks){
         ArrayList noRedirectURL=new ArrayList();
         boolean found;
         for (int i = 0; i < linksDto.length; i++) {
             ClickThroughLinksDTO clickThroughLinksDTO = linksDto[i];
             found=false;
             for (int j = 0; j < stampitClicks.length; j++) {
                 CampaignLinks stampitClick = stampitClicks[j];
                 String oldRedirectURL = ApplicationProperty.getProperty("stampit.clickthrough.redirect.url")+stampitClick.getUrlId();
                 String newRedirectURL = ApplicationProperty.getProperty("stampit.clickthrough.redirect.url")+stampitClick.getUrlId()+
                                         ApplicationProperty.getProperty("stampit.clickthrough.recipient.link");
                 String newWithNameRedirectURL = ApplicationProperty.getProperty("stampit.clickthrough.redirect.url")+stampitClick.getUrlId()+
                                        ApplicationProperty.getProperty("stampit.clickthrough.recipient.link")+ApplicationProperty.getProperty("stampit.clickthrough.name.link");
                 if(oldRedirectURL.equals(clickThroughLinksDTO.getUrl()) || newRedirectURL.equals(clickThroughLinksDTO.getUrl()) || newWithNameRedirectURL.equals(clickThroughLinksDTO.getUrl())){
                     if(stampitClick.getCurrentStatus().equals("ACTIVE")){
                        clickThroughLinksDTO.setUrl(stampitClick.getUrl());
                        clickThroughLinksDTO.setUrlName(stampitClick.getUrlName());
                        clickThroughLinksDTO.setReplacedUrlFlag(true);
                        noRedirectURL.add(clickThroughLinksDTO);
                     }
                     found=true;
                     break;
                 }
             }
             if(!found){
                    noRedirectURL.add(clickThroughLinksDTO);
                 }
         }
         return (ClickThroughLinksDTO[])noRedirectURL.toArray (new ClickThroughLinksDTO[noRedirectURL.size()]);
     }

    public boolean deleteClickThroughLinks(ClickThroughLinksDTO[] linksDTO, int[] selectedPosition, Long campaignId) {
        CampaignLinks[] stampitClicks=stampitClicksService.getStampitClicksByCampaignId(campaignId);
        stampitClicks= getstampitClicksFromSelecetedUrlsToDelete(linksDTO, selectedPosition,stampitClicks);
        savestampitClicks(stampitClicks);
        return true;
    }

    /* public boolean selectedURLPresentInStampitClicks(ClickThroughLinksDTO[] linksDTO,int[] selectedPosition, StampitClicks[] stampitClicks){
        int counter=0;
        for (int i = 0; i < selectedPosition.length; i++) {
            ClickThroughLinksDTO dto=linksDTO[selectedPosition[i]];
            for (int j = 0; j < stampitClicks.length; j++) {
                StampitClicks stampitClick = stampitClicks[j];
                if(dto.getUrl().equals(stampitClick.getUrl())){
                    counter++;
                }
            }
        }
        return counter == selectedPosition.length;
    }*/

    public void savestampitClicks(CampaignLinks[] stampitClicks){
        for (int i = 0; i < stampitClicks.length; i++) {
            CampaignLinks stampitClick = stampitClicks[i];
            stampitClicksService.saveUpdateStampitClicks(stampitClick);
        }
    }

    public CampaignLinks[] getstampitClicksFromSelecetedUrlsToDelete(ClickThroughLinksDTO[] linksDTO, int[] selectedPosition,CampaignLinks[] stampitClicks){
        List deleteList=new ArrayList();
        for (int i = 0; i < selectedPosition.length; i++) {
            ClickThroughLinksDTO dto=linksDTO[selectedPosition[i]];
            for (int j = 0; j < stampitClicks.length; j++) {
                CampaignLinks stampitClick = stampitClicks[j];
                if(dto.getUrl().equals(stampitClick.getUrl())){
                    stampitClick.setCurrentStatus("INACTIVE");
                    deleteList.add(stampitClick);
                }
            }
        }
        return (CampaignLinks[])deleteList.toArray(new CampaignLinks[deleteList.size()]);
    }

 
}
