package com.kenscio.litemail.workflow.service;

import java.util.List;
import java.util.logging.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;


import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.dao.AccountDao;
import com.kenscio.litemail.workflow.dao.CampaignDao;
import com.kenscio.litemail.workflow.dao.SmtpDao;
import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.domain.EmtsResponse;
import com.kenscio.litemail.workflow.domain.SmtpEntity;


public class SmtpService {

	public static Logger log = Logger.getLogger(SmtpService.class.getName());
	

	public void addSmtpServer(SmtpEntity smtpEntity) {
		SmtpDao  smtpDao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		smtpDao.saveSmtpDetail(smtpEntity);
		if(smtpEntity.getSmtpServer().contains("mailgun"))
		registerSmtpServerDomainWithEmts(smtpEntity);
	}
	
	public void updateSmtpServer(SmtpEntity accountEntity) {
		SmtpDao  smtpDao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		SmtpEntity se = smtpDao.getSmtpDetail(accountEntity.getSmtpServerName());
		if(se != null)
		{
			se.setSmtpPassword(accountEntity.getSmtpPassword());
			se.setSmtpServer(accountEntity.getSmtpServer());
			se.setSmtpUser(accountEntity.getSmtpUser());
			se.setApiKey(accountEntity.getApiKey());
			se.setPartner(accountEntity.getPartner());
		}
		smtpDao.update(se);
		if(se.getSmtpServer().contains("mailgun"))
		registerSmtpServerDomainWithEmts(se);
	}
	
	private void registerSmtpServerDomainWithEmts(SmtpEntity se) {
		
		 String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		 String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		 String instance = ApplicationProperty.getProperty("litemail.instance");
		 String urlString = "registerMailgunDomain.htm";
		 long accountId = se.getAccountId();
		 String domainName = getDomainName(se.getSmtpUser());
		 String apiKey = se.getApiKey();
		 String smtpServer = se.getSmtpServer();
		 String sp = "amazon";
		 if(smtpServer != null && smtpServer.contains("mailgun"))
		 {
			 sp = "mailgun";
		 }
	     String url=baseUrl+"/"+urlString+"?pId="+platFormId+"&clId="+instance+"_"+accountId+"&dm="+domainName+"&ak="+apiKey+"&sp="+sp;
	     getEmtsResponse(url);
		
	}

	private String getDomainName(String smtpUser) {
		
		if(smtpUser != null)
		{
			int i = smtpUser.indexOf("@");
			return smtpUser.substring(i+1);
		}
		return "";
	}

	public EmtsResponse getEmtsResponse(String url)
    {
        HttpClient client;
        EmtsResponse emtsObject;
        client = new DefaultHttpClient();
        emtsObject = new EmtsResponse();
        try
        {
            HttpGet request = new HttpGet(url);
            HttpResponse response = client.execute(request);
            ObjectMapper wrapper = new ObjectMapper();
            emtsObject = (EmtsResponse)wrapper.readValue(response.getEntity().getContent(), EmtsResponse.class);
            client.getConnectionManager().shutdown();
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        client.getConnectionManager().shutdown();
		return emtsObject;
       
    }
	
	
	public void deleteSmtpServer(String servername) {
		SmtpDao  smtpDao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		smtpDao.deleteSmtpDetail(servername);
		SmtpEntity smtp = smtpDao.getSmtpDetail(servername);
		//deRegisterSmtpServerDomainWithEmts(smtp);
	}
	
	private void deRegisterSmtpServerDomainWithEmts(SmtpEntity smtp) {
		// TODO Auto-generated method stub
		
	}
	public SmtpEntity[] getAllSmtpServers() {
		//String partner = getPartnerForAccount(accountId);
		SmtpDao  smtpDao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		return smtpDao.getAllSmtpDetail();
        
	}

	public SmtpEntity[] getAllSmtpServers(long accountId) {
		String partner = getPartnerForAccount(accountId);
		SmtpDao  smtpDao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		return smtpDao.getAllSmtpDetail(partner);
        
	}

	public boolean isSmtpExit(String smtpServerName) {
		SmtpDao  smtpDao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		SmtpEntity smtp = smtpDao.getSmtpDetail(smtpServerName);
		if(smtp != null)
			return true;
		else
			return false;
	}

	public List<String> getAllSmtpServerNames(long accountId) {
		String partner = getPartnerForAccount(accountId);
		SmtpDao  smtpDao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		return smtpDao.getAllSmtpNames(partner);
	}

	private String getPartnerForAccount(long accountId) {
		AccountDao  smtpDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		AccountEntity ae = smtpDao.getAccount(accountId);
		return ae.getPartnerName();
		
	}

	public boolean isSmtpInUse(long accountId, String servername) {
		CampaignDao  smtpDao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return smtpDao.isSmtpUsed(servername);
		
	}

	public SmtpEntity getSmtpDetails(String smtpServerName) {
		SmtpDao  smtpDao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		SmtpEntity entity=smtpDao.getSmtpDetail(smtpServerName);
		return entity;
	}

	public SmtpEntity getSmtpDetailsById(Long smtpId) {
		SmtpDao  smtpDao = (SmtpDao) AppContext.getFromApplicationContext("smtpDao");
		SmtpEntity entity=smtpDao.getSmtpDetailById(smtpId);
		return entity;
	}
}
