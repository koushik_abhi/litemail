package com.kenscio.litemail.workflow.service;

import com.kenscio.litemail.workflow.common.AppContext;

import com.kenscio.litemail.workflow.dao.EmailDaoImpl;
import com.kenscio.litemail.workflow.domain.EmailObject;


public class EmailServiceImpl {

	public void addEmailConfig(EmailObject obj) {

		EmailDaoImpl  imp = (EmailDaoImpl) AppContext.getFromApplicationContext("emailDao");
		imp.saveEmail(obj);
		
	}

	public void updateEmailConfig(EmailObject obj) {

		EmailDaoImpl  imp = (EmailDaoImpl) AppContext.getFromApplicationContext("emailDao");
		imp.updateEmail(obj);
		
	}
	
	public EmailObject getEmailConfig() {
       
		EmailDaoImpl  imp = (EmailDaoImpl) AppContext.getFromApplicationContext("emailDao");
		EmailObject[] em = imp.getAllEmailConfig();
		if(em != null && em.length > 0)
			return em[0];
		
		return null;
	}

	public EmailObject getEmailConfigByUser(String userId) {
		EmailDaoImpl  imp = (EmailDaoImpl) AppContext.getFromApplicationContext("emailDao");
		EmailObject emailObject=imp.getEmailConfigByUser(userId);
		return emailObject;
	}
	
	
}
