package com.kenscio.litemail.workflow.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import javax.mail.Message;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;

import com.kenscio.litemail.exception.CampaignException;
import com.kenscio.litemail.exception.MTAException;
import com.kenscio.litemail.util.AddressFilter;
import com.kenscio.litemail.util.MailUtil;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.LoggerUtil;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.dao.CampaignDao;
import com.kenscio.litemail.workflow.domain.ActionLogObject;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.CampaignMailFail;
import com.kenscio.litemail.workflow.domain.CampaignMailSentSoFar;
import com.kenscio.litemail.workflow.domain.CampaignStop;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.domain.EmtsResponse;
import com.kenscio.litemail.workflow.domain.FilteredAddressData;
import com.kenscio.litemail.workflow.domain.SmtpEntity;
import com.kenscio.mta.ecircle.service.EcircleService;

public class MailerDemon extends Thread {
    private BulkMailer bm;
    private Long cId;
    private CampaignService campaignService = null;
    private EcircleService  eCircleService = new EcircleService();
    private volatile Boolean killThread = false;
    private FutureTask task;
    private ExecutorService executorService;
    private Long delayInSeconds = new Long(ApplicationProperty.getProperty("mail.delay.in.seconds") + "000");
  
    public void run()
    {

    	ActionLogObject ac1 = MailUtil.addCampaignSendOuttActionAndSendNotification(bm.getCampaign().getCampaignName(), bm.getCampaign().getCreatedBy());
    						
        Message[] stampedMails = null;
        // For long list of mails, Mails are grouped in batches.
        int trial = 1;
       // boolean isStd = campaignService.checkIfStdDataOnly(bm.getCampaign().getListId());
        EmailData[] batchEmailIds =campaignService.getEmailsIdsbyBatchSpringJDBC(getBm());
        EmailData[] prevBatch=null;
        //create envelope...one time activity...
        if(isEcircleMTA()){
           if(!eCircleService.createEnvelope(bm.getCampaign()))return;           
        }
            
        do{
        	try {
        		 if(batchEmailIds==null||batchEmailIds.length==0){
           		   System.out.println("No mails to sent so exiting the mailer !!");
           		   break;
           	      }
                  //MTA check
        		      if(isEcircleMTA()) 
        		      {
                	 
        		      do
                	  {
                	   FilteredAddressData data=AddressFilter.extractMTAtransferAddresses(bm.getCampaign(),batchEmailIds);
                      //generate stamps for this batch...
						if (bm.getCampaign().getMTAExcludeFilter() == null
								|| bm.getCampaign().getMTAExcludeFilter()
										.equals("")) 
						{
							eCircleService.send(bm.getCampaign()// send all through ecircle...
									.getCampaignId(), data.getRemaining());
							batchEmailIds=null;

						}else if (data.getFiltered() != null
								&& data.getFiltered().length > 0)
						{
							eCircleService.send(bm.getCampaign()
									.getCampaignId(), data.getFiltered());
							 batchEmailIds=data.getRemaining();
						}
                	  
                	  }while(batchEmailIds==null&&(batchEmailIds=campaignService.getEmailsIdsbyBatchSpringJDBC(getBm()))!=null&&batchEmailIds.length!=0);
                	}
        		    if(batchEmailIds==null||batchEmailIds.length==0)
        		    {
                  	   System.out.println("No mails to sent so exiting the mailer !!");
                  	   break;
                    }         		      
                    prevBatch=batchEmailIds;
                  //STEP  Stamp this single batch
                	
                    if(isCampaignStopRequest(bm.getCampaign().getAccountId(), bm.getCampaign().getCampaignId()))
                    {
                    	KillExecutorThreads();
                    	saveFailList();
                        saveSentMailList();
                    	campaignService.stopCampaignUpdate(bm.getCampaign().getAccountId(), bm.getCampaign().getCampaignName(), bm.getFileSeeker());
                    	sendNotificationMail(ac1);
                    	return;
                    }
                    
                   	stampedMails = campaignService.getStampMailList(bm.getCampaign(),batchEmailIds);
                   	bm.setStampedEmails(stampedMails);
                	batchEmailIds =campaignService.getEmailsIdsbyBatchSpringJDBC(getBm());       
                    
                    // Step  before submitting  this batch for mailing check wheather to request of stopping the campaign is there.
                    if (isInterrupted() || isKillThread()) {
                        shutDown();
                        saveFailList();
                        saveSentMailList();
                        return;
                    }
                    // Step  submit this batch for mailing.
                    System.out.println("**********************before mail batch total sent now " + bm.getCountOfMailsSent() );
                    mailBatch();
                    System.out.println("**********************after mail batch***total sent now  "+bm.getCountOfMailsSent());
                  // 	Thread.sleep(delayInSeconds);
                 //   System.out.println("**********************after sleep*********************************");
                    trial=0;
                } catch (CampaignException e) {
                    System.out.println("Stamping Failed!!" + e.getMessage());
                    e.printStackTrace();
                    batchEmailIds=prevBatch;
                    trial++;
                    if(trial==3){
                    // handleStampingError(batchEmailIds);
                     batchEmailIds =campaignService.getEmailsIdsbyBatchSpringJDBC(getBm());
                    trial=0;
                    }
                }
                catch (Exception e) {
                    System.out.println("Runtime Exception in run method of mailer !!" + e.getMessage());
                    e.printStackTrace();
                    //Get the next batch and continue mailing
                    System.out.println("getting next batch to stamp");
                    batchEmailIds =campaignService.getEmailsIdsbyBatchSpringJDBC(getBm());
                    trial=0;
                }
                // STEP  Save the status of campaign after completion of each Task.
                
        	    campaignService.saveCampaignStatus(getCId(),getBm());
               // System.out.println("**********************Before save fail list*********************************");
                saveFailList();
                saveSentMailList();
              //  System.out.println("**********************After save fail list*********************************");
                
        }while(batchEmailIds!=null && batchEmailIds.length>0) ;
        
        System.out.println("**********************Out of sending loop*********************************");
        // STEP  Set status as over and save the state to DB
        try {
        	if(isEcircleMTA())
			  eCircleService.upload(bm.getCampaign().getCampaignId(),bm.getCampaign().getCampaignName(),createEnvelopeSubject(bm.getCampaign().getSubject()));;
		} catch (MTAException e) {
			System.out.println(e.getMessage());
			LoggerUtil.doLog(e.getMessage());
			e.printStackTrace();
		}
        getBm().setCampaignOver(Boolean.TRUE);
        getBm().getSMTPServer().close();
        campaignService.saveCampaignFinalStatus(getBm(),getCId());
        System.out.println("Campaign Over!!" + getCId());
        // STEP  Save failed mails to file system.
        saveFailList();
        saveSentMailList();
        //getBounceAndSpamList(bm.getCampaign());
        
        ac1.setTotalRecords(bm.getFileSeeker()+"");
        ac1.setEndTime(new Date());
        ac1.setActionSummaryDetails("Campaign send out is completed for the campaign : '"+ bm.getCampaign().getCampaignName()+ " \n Campaign Speed is :" + bm.getCampaign().getCampaignSpeed() + " \n Campaign Starttime: "+ bm.getCampaign().getCampaignStartDateTime() +" \n Campaign Endtime:" +bm.getCampaign().getCampaignEndDateTime() +" \n Total users recieved campaign : " + ac1.getTotalRecords());
        ac1.setStatus("completed");
        triggerBouncesAndSpamsCollectionInEmts(bm.getCampaign());
    	MailUtil.updateCampaignCompletedActionLogSendEmailNotification(ac1);
    	
        this.bm=null;
       
        
    }
    
    private void triggerBouncesAndSpamsCollectionInEmts(Campaign campaign) {
    	String dm = "";
    	try
    	{
		
	    	String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
			String platFormId = ApplicationProperty.getProperty("emts.platform.id");
			String instance = ApplicationProperty.getProperty("litemail.instance");
			String username = campaign.getSmtpUserName();
			dm = getDomainFromAddress(username);
			String smtp = campaign.getSmtpServer();
			String sp = "";
			if(smtp != null && smtp.contains("mailgun"))
			{
				sp = "mailgun";
			}
			else if (smtp.contains("amazon"))
			{
				sp = "amazon";
			}
			String ak = getStmpApiKey(campaign.getSmtpServerName());
			if(dm != null && dm.length() > 0)
			{
				String url=baseUrl+"/"+"triggerBouncesSpamsForDomain.htm?pId="+platFormId+"&dm="+dm+"&ak="+ak+"&sp="+sp+"&clId="+instance+"_"+campaign.getAccountId()+"&cId="+campaign.getCampaignId();
				EmtsResponse obj= getEmtsResponse(url);
			}
    	}catch(Exception e)
    	{
    		System.out.println("unable to trigger bounces spam for this  domain with emts " + dm);
    	}
		    	
		
	}

	private void registerSmtpDomainWithEmts(Campaign campaign) 
    {
    	try
    	{
		
	    	String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
			String platFormId = ApplicationProperty.getProperty("emts.platform.id");
			String instance = ApplicationProperty.getProperty("litemail.instance");
			String fromAddress = campaign.getFromAddress();
			String dm = getDomainFromAddress(fromAddress);
			String smtp = campaign.getSmtpServer();
			String sp = "";
			if(smtp != null && smtp.contains("mailgun"))
			{
				sp = "mailgun";
			}
			else if (smtp.contains("amazon"))
			{
				sp = "amazon";
			}
			String ak = getStmpApiKey(campaign.getSmtpServerName());
			if(dm != null && dm.length() > 0)
			{
				String url=baseUrl+"/"+"registerMailgunDomain.htm?pId="+platFormId+"&dm="+dm+"&ak="+ak+"&sp="+sp+"&clId="+instance+"_"+campaign.getAccountId()+"&cId="+campaign.getCampaignId();
				EmtsResponse obj= getEmtsResponse(url);
			}
    	}catch(Exception e)
    	{
    		System.out.println("unable to register domain with emts ");
    	}
		    	
		
	}
    
    private String getStmpApiKey(String smtp) {
		SmtpService ser = new SmtpService();
		SmtpEntity se = ser.getSmtpDetails(smtp);
		if(se != null )
		{
			return se.getApiKey();
		}
		return "";
		    	
	}

	public static void main(String ar[])
    {
    	String fromAddress = "test@google.com";
    	
    	if(fromAddress != null && fromAddress.length() > 0)
    	{
    		int ind = fromAddress.indexOf("@");
    		String domain = fromAddress.substring(ind+1);
    		System.out.println("dom "+ domain);
    	}
    	
    }
    
    private String getDomainFromAddress(String fromAddress) {
		
    	if(fromAddress != null && fromAddress.length() > 0)
    	{
    		int ind = fromAddress.indexOf("@");
    		String domain = fromAddress.substring(ind+1);
    		return domain;
    	}
    	return "";
    	
	}

	public EmtsResponse getEmtsResponse(String url)
    {
        HttpClient client;
        EmtsResponse emtsObject;
        client = new DefaultHttpClient();
        emtsObject = new EmtsResponse();
        try
        {
            HttpGet request = new HttpGet(url);
            HttpResponse response = client.execute(request);
            ObjectMapper wrapper = new ObjectMapper();
            emtsObject = (EmtsResponse)wrapper.readValue(response.getEntity().getContent(), EmtsResponse.class);
            client.getConnectionManager().shutdown();
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        client.getConnectionManager().shutdown();
		return emtsObject;
       
    }

	private void sendNotificationMail(ActionLogObject ac1) {
    	 ac1.setTotalRecords(bm.getFileSeeker()+"");
         ac1.setEndTime(new Date());
         ac1.setActionSummaryDetails("Campaign send out is stopped for the campaign : '"+ bm.getCampaign().getCampaignName()+ " \n Campaign Speed is :" + bm.getCampaign().getCampaignSpeed() + " \n Campaign Starttime: "+ bm.getCampaign().getCampaignStartDateTime() +" \n Campaign Endtime:" +bm.getCampaign().getCampaignEndDateTime() +" \n Total users recieved campaign: " + ac1.getTotalRecords());
         
       //  ac1.setActionSummaryDetails("campaign send out is stopped , sent campaign to no of users  " + ac1.getTotalRecords());
         ac1.setStatus("stopped");
         
     	MailUtil.updateCampaignCompletedActionLogSendEmailNotification(ac1);
		
	}

	private boolean isCampaignStopRequest(Long accountId, Long campaignId) 
    {
    	CampaignStop camp = campaignService.getCampaigStopRecord(accountId,campaignId );
    	if(camp != null && camp.getStatus().toLowerCase().equalsIgnoreCase("stopped") )
    		return true;
    	else
    		return false;
	}

	private void KillExecutorThreads() {
    	try {
    		bm.getSMTPServer().close();
            if(executorService != null && !executorService.isTerminated())
                executorService.shutdownNow();
            if(task != null && !task.isDone())
                task.cancel(true);
           
            
        } catch (RuntimeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            
        }
              
		
	}

	public void getBounceAndSpamList(Campaign campaign)
    {
    	BounceSpamAsyncServiceExecutor exe  = new BounceSpamAsyncServiceExecutor();
    	exe.callAsyncServiceForBounceSpams(campaign);
    	
    	
    }
    boolean isEcircleMTA(){
    	return(bm.getCampaign().getMTA()!=null&&bm.getCampaign().getMTA().equalsIgnoreCase("ecircle"));
    }
    
    public BulkMailer getBm(){
        return bm;
    }

    public void setBm(BulkMailer bm){
        this.bm = bm;
    }

    public Long getCId(){
        return cId;
    }

    public void setCId(Long id){
        cId = id;
    }

    public CampaignService getCampaignService(){
        return campaignService;
    }

    public void setCampaignService(CampaignService campaignService_){
    	campaignService = campaignService_;
    }
    private String createEnvelopeSubject(String campaignSubject){
    	String subject=ApplicationProperty.getProperty("stamp.mail.subject")+
        " - "+
        "<%user.CampaignAttribute.Code%>"+" - "+
        campaignSubject;
    	return subject;
    }
    public void mailBatch(){ // Step 5 submit batch for mailing.
       
            try {
                if ( task==null ||task.isDone() ||executorService==null||executorService.isTerminated()) {
                    task = new FutureTask(getBm());
                    executorService = Executors.newFixedThreadPool(1);
                    executorService.submit(task);
                    // Step 6 Get the result of the task
                boolean result = ((Boolean) task.get()).booleanValue();
                if (result)
                    executorService.shutdown();
                }
                else throw new Exception("Not ready for mailing...");
            } catch (Exception ee) {
                ee.printStackTrace();
                LoggerUtil.setLoggerLevel("ERROR");
                LoggerUtil.doLog("Exception in Mailer Demon run :-(");
            }
        
    }

    public Boolean shutDown(){
        try {
            if(!executorService.isTerminated())
                executorService.shutdownNow();
            if(!task.isDone())
                task.cancel(true);
            
        } catch (RuntimeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        finally{
        	getBm().getCampaign().setStatus("stopped");
        	campaignService.saveCampaignStoppedStatus(getBm(),cId);
            System.out.println("STOPPING MAILER!!!!!!!!!!!!");
        }
        return true;
    }

    
    private void saveSentMailList(){
        try {
           
        	CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
        	List q = bm.getListOfSendMails();
        	EmailData[] emaildata = campaignService.getEmailDataFromStampedMails(q);
        	for(int i=0; i< emaildata.length; i++)
        	{
        		EmailData message = emaildata[i];
        		
        		CampaignMailSentSoFar send = new CampaignMailSentSoFar();
        		send.setCampaignId(bm.getCampaign().getCampaignId());
        		send.setEmail(message.getEmailId());
        		send.setName(message.getName());
				dao.saveCampignMailSent(send);
        		
        	}
        	q.clear();
        	
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    private void saveFailList(){
        try {
           // if (getBm().getMailsFailedInSMTP() != null || getBm().getMailsFailedInSMTP().size() > 0) {
                //System.out.println("Saving failed mails to disk for ID= " + getCId());
                campaignService.saveFailedEmails(bm);
          //  }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Move the mails which fails in stamping to a list
     */
    private void handleStampingError(EmailData[] mailIdsFailedInStamping){
        List subList = Arrays.asList(mailIdsFailedInStamping);
        EmailData [] existingList=getBm().getMailsFailedInStamping();
        List mainList =new ArrayList();
        
        if(existingList!=null)
        mainList = new ArrayList(java.util.Arrays.asList(existingList));
        mainList.addAll(subList);
        getBm().setMailsFailedInStamping((EmailData[]) mainList.toArray(new EmailData[mainList.size()]));
       
        if(subList!=null&& subList.size()>0){
        for(int i=0;i<subList.size();i++)
        getBm().incrementMailFailedCounter();
        }
    }



    public Boolean isKillThread(){
        return killThread;
    }

    public void setKillThread(Boolean killThread){
        this.killThread = killThread;
    }

    public FutureTask getTask(){
        return task;
    }

    public void setTask(FutureTask task){
        this.task = task;
    }

	   
}
