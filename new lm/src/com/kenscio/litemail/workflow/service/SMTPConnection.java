package com.kenscio.litemail.workflow.service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
/**
 * 
 * @author Admin
 * 
 */

public class SMTPConnection {

	private Transport  smtpTransport;
	private int maxToSend;
	private int mailSent=0;
	private Session session;
	private int idx;
	public SMTPConnection(int idx,Session session,int maxToSend){
	   this.idx=idx;
	   this.session=session;
	   this.maxToSend=maxToSend;
	}
	boolean send(Message message){
	 try {
			if(smtpTransport==null)
			   smtpTransport=session.getTransport();
			if(!smtpTransport.isConnected())smtpTransport.connect();
			message.saveChanges();
			smtpTransport.sendMessage(message,message.getAllRecipients());
			/*if(mailSent==maxToSend){
               mailSent=0;
               smtpTransport.close();
			}else
			 mailSent++;*/
			
			//commented not to close smtpTransport for any number of messages
			//System.out.println("mailSent from"+idx+"--"+mailSent);
			return true;
		 } catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (MessagingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	   }catch(Exception e){
		e.printStackTrace();
	   }
	  return false;
	  
	}
	public void close(){
	  try {
			if(smtpTransport!=null&&smtpTransport.isConnected())smtpTransport.close();
	 } catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	 }
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public int getMaxToSend() {
		return maxToSend;
	}
	public void setMaxToSend(int maxToSend) {
		this.maxToSend = maxToSend;
	}
	public Session getSession() {
		return session;
	}
	public void setSession(Session session) {
		this.session = session;
	}
	public Transport getSmtpTransport() {
		return smtpTransport;
	}
	public void setSmtpTransport(Transport smtpTransport) {
		this.smtpTransport = smtpTransport;
	}
	
}
