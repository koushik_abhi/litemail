package com.kenscio.litemail.workflow.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.kenscio.litemail.workflow.domain.ActionLogObject;
import com.kenscio.litemail.workflow.domain.EmailObject;
import com.kenscio.litemail.workflow.service.ThreadPoolExecutorServiceImpl;



public class EmailServiceUtil {
	
	private static final Logger log = Logger.getLogger(EmailServiceUtil.class.getName());  
	private ActionLogObject ac1 = null;
	private EmailObject emailObj = null;
	
	public void sendEmailNotification(ActionLogObject ac) {
		
		ac1 = ac;
		ThreadPoolExecutorServiceImpl.executeEmailAsync(this);
		
	}
	
	public void sendEmailNotification(EmailObject obj, String email, String password, String userId) {
		
		emailObj = obj;
		ac1 = new ActionLogObject();
		ac1.setActionName("forgot password");
		ac1.setStatus("password renerated");
		ac1.setActionSummaryDetails("generated new password for userId : " + userId + " and generated password is :" + password );
		
		ThreadPoolExecutorServiceImpl.executeEmailAsync(this);
		
	}
	
	public void sendSupportEmailNotification(EmailObject obj, String message, String userId) {
		
		emailObj = obj;
		ac1 = new ActionLogObject();
		ac1.setActionName("Support Message");
		ac1.setStatus("New");
		ac1.setActionSummaryDetails("Support Message Created By User : " + userId+ ":\n message: " + message );
		
		ThreadPoolExecutorServiceImpl.executeEmailAsync(this);
		
	}

	public void execute() {
		
		EmailServiceImpl impl = new EmailServiceImpl();
		EmailObject obj = null;
		if(ac1.getUserId() != null && ac1.getUserId().length() > 0)
		{
			obj = impl.getEmailConfig();
		}
		if(obj == null)
			obj = emailObj;
		
		String subject = obj.getSubject();
		String body = obj.getBody();
		StringBuilder sub = new StringBuilder();
		if(subject != null && subject.length() >0)
			sub.append(subject +"-");
		else
			sub.append("LiteMail-");
		sub.append("Action:"+ ac1.getActionName()+"-");
		sub.append("Status:"+ac1.getStatus()+"-");
		sub.append("Performed:"+ac1.getUserId()+"-");
		sub.append("Partner:"+ ac1.getPartner());
		
		StringBuilder bod = new StringBuilder();
		if(body != null && body.length() > 0)
		bod = new StringBuilder(body +"\n");
		bod.append("Message Summary Details : \n\n");
		bod.append(ac1.getActionSummaryDetails() +"\n\n");
		bod.append("Logs: ");
		if(ac1.getActionLogFile() !=null)
			bod.append(new String(ac1.getActionLogFile()));
		else
			bod.append("No Errors,");
		
		bod.append("\n\n");
		bod.append("Regards,");
		bod.append("Litemail Support Team");
		obj.setSubject(sub.toString());
		obj.setBody(bod.toString());
		MailServiceImpl mail = new MailServiceImpl();
		try
		{
			mail.send(obj);
			
		}catch(Exception e)
		{
			e.printStackTrace();
			log.log(Level.SEVERE, "email notification is failed " + e.getMessage());
			
		}
	}

}
