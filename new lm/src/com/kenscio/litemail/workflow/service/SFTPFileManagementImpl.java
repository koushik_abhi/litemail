package com.kenscio.litemail.workflow.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.io.OutputStreamWriter;

import java.io.FileOutputStream;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;

import com.jcraft.jsch.Session;


public class SFTPFileManagementImpl {
 
	public static Logger log = Logger.getLogger(FTPFileManagementImpl.class.getName());	
		
	public SFTPFileManagementImpl() 
	{
		
	}
	
	public Map<String, String> downloadfile(String host, String user, String filelocation, String password ) throws Exception {
		
		ChannelSftp sftp = null;
		Session session = null;
		String fileName = "";
		File file = null;
		Map<String, String> filenamepath = new HashMap<String, String>();
		try {		
			
			FileOutputStream fos = null; 
		    int in = filelocation.lastIndexOf("/");
		   
		    if(in > 0)
		    {
		    	fileName = filelocation.substring(in+1);
		    }
		    if(isHostLocalServer(host))
	    	{
	    		file = new File(filelocation);
	    		filenamepath.put(fileName, file.getAbsolutePath());
	    		return filenamepath;
	    		
	    	}
		    
			file = File.createTempFile(fileName,".csv");
		//	System.out.println("file path "+ file.getAbsolutePath() +"file location "+ filelocation);
		    fos = new FileOutputStream(file); 
		    JSch jsch = new JSch();
		   
		    SshUserInfo userInfo =	new SshUserInfo(password);
		 	    
		    Session sshSession = jsch.getSession(user, host, 22);
			sshSession.setUserInfo(userInfo);
			sshSession.setPassword(password);
			sshSession.connect(); // tries pubKey with provided key and asks
			Channel channel = sshSession.openChannel("sftp");
			sftp = (ChannelSftp) channel;
		    sftp.connect();
		    sftp.get(filelocation, fos);
		    filenamepath.put(fileName, file.getAbsolutePath());
		    
		    /*session = jsch.getSession(user, host, 22);
		   
		   
		    java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
		    Channel channel = session.openChannel("sftp");
		    sftp = (ChannelSftp) channel;
		    sftp.connect();*/
		   
		}catch(JSchException jsc)
		{	
			jsc.printStackTrace();
			log.log(Level.SEVERE, null, jsc);
			//System.err.println("Unable to connect to FTP server. "+e.toString());
			throw new Exception("unable to get file using sftp , please check ftp details key store file " + jsc.getMessage());
			
		}catch (Exception e) {
			e.printStackTrace();
			log.log(Level.SEVERE, null, e);
			//System.err.println("Unable to connect to FTP server. "+e.toString());
			throw e;
			
		}finally
		{
			if(session != null)
				session.disconnect();
			if(sftp != null)
				sftp.disconnect();
		}
		
	
	    
		return filenamepath;


	   }	
	
	public Map<String, String> downloadfile(String host, String user, String filelocation, String password , final InputStream keyin) throws Exception {

	ChannelSftp sftp = null;
	Session session = null;
	String fileName = "";
	File file = null;
	Map<String, String> filenamepath = new HashMap<String, String>();
	try {		
		
		FileOutputStream fos = null; 
	    int in = filelocation.lastIndexOf("/");
	   
	    if(in > 0)
	    {
	    	fileName = filelocation.substring(in+1);
	    }
	    if(isHostLocalServer(host))
    	{
    		file = new File(filelocation);
    		filenamepath.put(fileName, file.getAbsolutePath());
    		return filenamepath;
    		
    	}
	    
		file = File.createTempFile(fileName,".csv");
	    fos = new FileOutputStream(file); 
	    JSch jsch = new JSch();
	    String str = createTempFileForKey(keyin);
	    SshUserInfo userInfo =	new SshUserInfo(str, password);
	  
	    jsch.addIdentity(new File(userInfo.getKeyFile()).getAbsolutePath());
	    
	    Session sshSession = jsch.getSession(user, host, 22);
		sshSession.setUserInfo(userInfo);
		sshSession.connect(); // tries pubKey with provided key and asks
		Channel channel = sshSession.openChannel("sftp");
		sftp = (ChannelSftp) channel;
	    sftp.connect();
	    sftp.get(filelocation, fos);
	    filenamepath.put(fileName, file.getAbsolutePath());
	    
	    /*session = jsch.getSession(user, host, 22);
	   
	   
	    java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.connect();
	    Channel channel = session.openChannel("sftp");
	    sftp = (ChannelSftp) channel;
	    sftp.connect();*/
	   
	}catch(JSchException jsc)
	{	
		jsc.printStackTrace();
		log.log(Level.SEVERE, null, jsc);
		//System.err.println("Unable to connect to FTP server. "+e.toString());
		throw new Exception("unable to get file using sftp , please check ftp details key store file " + jsc.getMessage());
		
	}	
	catch (Exception e) {
		e.printStackTrace();
		log.log(Level.SEVERE, null, e);
		//System.err.println("Unable to connect to FTP server. "+e.toString());
		throw e;
		
	}finally
	{
		if(session != null)
			session.disconnect();
		if(sftp != null)
			sftp.disconnect();
	}
    
	return filenamepath;


   }	
   
	private String createTempFileForKey(InputStream in) {
		File file = null;
		BufferedWriter writer =null; BufferedReader reader = null;
		
		try{
						 
		 long time  = System.currentTimeMillis();
		 String fileName = "sshkey_"+time+".key";
		 file = File.createTempFile(fileName,".key");
		 FileOutputStream out = new FileOutputStream(file);
		 writer = new BufferedWriter(new OutputStreamWriter(out));
		 reader = new BufferedReader(new InputStreamReader(in));
		 String line=  "";
	     while ((line = reader.readLine()) != null) 
		 {
	    	 writer.write(line);
	    	 writer.newLine();
		 }	
			
			
		}catch(Exception e)
		{
			log.log(Level.SEVERE, "error in uploading file "+e.getMessage());
		}
		finally
		{
			
			try {
				if(writer != null)
				writer.close();
				if(reader != null)
					reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		return file.getAbsolutePath();
		
	}
	
	
	
   private String readMyPrivateKeyFromFile(InputStream keyin) {
	   
       StringBuilder builder = new StringBuilder();
       
       String line = null;
	   try {
		    BufferedReader reader = new BufferedReader(new InputStreamReader(keyin));
		    while ((line = reader.readLine()) != null) 
			{
		    	builder.append(line);
			}
			
		} catch (IOException e) {
			log.log(Level.SEVERE, null, e);
			
		}
	    System.out.println(builder.toString());
	    return builder.toString();
	}

   private boolean isHostLocalServer(String host) {
		
		InetAddress addr = null;
		try {
			addr = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			 log.log(Level.SEVERE, "unable to get localHost"+ e.getMessage());
		}
		if(addr != null)
		{
		    String ipAddress = addr.getHostAddress();
	        String hostname = addr.getHostName();
	        if(ipAddress.contains(host) || hostname.contains(host))
	        {
	        	return true;
	        }
	        else
	        {
	        	return false;
	        }
		}else
		{
			return false;
		}
	}
	
	
	public static void main(String[] args)
	{
		SFTPFileManagementImpl ref =new SFTPFileManagementImpl();
	
		try 
		{
			FileInputStream f = new FileInputStream(new File("C:\\venu\\184.73.242.15.key"));
			//ref.downloadfile("184.73.242.15", "root", "/opt/shinetest.csv", "RMHCI#111012" , f);
			ref.execute("root", "184.73.242.15");
		
		} catch (Exception ex) {
			log.log(Level.SEVERE, null, ex);
		}
	}
	
	

	public void execute(String username, String host)
	{
		
		JSch jsch = new JSch();
		
		
		try 
		{
			File f = new File("C:\\venu\\184.73.242.15.key");
			
			 String str = createTempFileForKey(new FileInputStream(f));
			 SshUserInfo userInfo =
						new SshUserInfo(str, "RMHCI#111012");
					 
			System.out.println("str "+ str);
			
			File file = File.createTempFile("mytest",".csv");
			FileOutputStream fos = new FileOutputStream(file); 
			
			jsch.addIdentity(new File(userInfo.getKeyFile()).getAbsolutePath());
			Session sshSession = jsch.getSession(username, host, 22);
			sshSession.setUserInfo(userInfo);
			sshSession.connect(); // tries pubKey with provided key and asks
			  Channel channel = sshSession.openChannel("sftp");
			  ChannelSftp sftp = (ChannelSftp) channel;
			    sftp.connect();
			    sftp.get("/opt/shinetest.csv", fos);
			  System.out.println("path downlad " + file.getAbsolutePath());
			
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
	
}