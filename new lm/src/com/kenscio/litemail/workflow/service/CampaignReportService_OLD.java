package com.kenscio.litemail.workflow.service;

import java.util.ArrayList;
import java.util.List;

import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.dao.CampaignDao;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.MessageBounce;
import com.kenscio.litemail.workflow.domain.MessageLinkClick;
import com.kenscio.litemail.workflow.domain.MessageOpen;
import com.kenscio.litemail.workflow.domain.MessageSpam;

public class CampaignReportService_OLD {

	public Campaign getCampaignStatusReport(long accountId,
			Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign =cdao.getCampaign(campaignID);
		return campaign;
	}

	public List<MessageOpen> getMailTotalOpnesList(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign =cdao.getCampaign(campaignID);
		String listmails=campaign.getFromAddress();
		List<MessageOpen> list = new ArrayList<MessageOpen>();
		MessageOpen mopen = new MessageOpen();
		mopen.setReciepientEmail(listmails);
		list.add(mopen);
		return list;
	}

	public List<Campaign> getMailSentDataList(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		Campaign campaign =cdao.getCampaign(campaignID);
		campaign.setFromAddress(campaign.getFromAddress());
		List<Campaign> list= new ArrayList<Campaign>();
		list.add(campaign);
		return list;
	}

	public List<MessageLinkClick> getTotalClicksMailList(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		MessageLinkClick totallinkClicks = cdao.getTotalClicksMailList(campaignID);
		return new ArrayList<MessageLinkClick>();
	}

	public List<MessageOpen> getUniqueOpensMailList(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageOpen>();
	}

	public List<MessageLinkClick> getUniqueClicksMailLsit(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageLinkClick>();
	}

	public List<MessageLinkClick> getClickedEMailLsit(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageLinkClick>();
	}

	public List<MessageBounce> getHardBouncedMailLsit(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getSoftBouncedMailLsit(Long campaignID) {
		// TODO Auto-generated method stub
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getGeneralBouncedMailLsit(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getTransientBouncedMailLsit(Long campaignID) {
		// TODO Auto-generated method stub
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getMailBlockBouncedMailLsit(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getTotalMailBouncedMailLsit(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getUnsubscribeMailBouncedMailLsit(
			Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getAutoReplyMailBouncedMailLsit(
			Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getOthersBouncedMailLsit(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageSpam> getSpamComplaintMailLsit(Long campaignID) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageSpam>();
	}

	
	public List<Campaign> getMailSentDataListFilter(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<Campaign>();
	}

	public List<Campaign> getMailFailDataListFilter(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<Campaign>();
	}

	public List<MessageOpen> getMailTotalOpnesListFilter(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageOpen>();
	}

	public List<MessageLinkClick> getTotalClicksMailListFilter(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageLinkClick>();
	}

	public List<MessageOpen> getUniqueOpensMailListFilter(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageOpen>();
	}

	public List<MessageLinkClick> getUniqueClicksMailLsitFilter(
			Long campaignID, String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageLinkClick>();
	}

	public List<MessageLinkClick> getClickedEMailLsitFilter(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageLinkClick>();
	}

	public List<MessageBounce> getHardBouncedMailLsitFilter(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getSoftBouncedMailLsitFilter(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getGeneralBouncedMailLsitFilter(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getTransientBouncedMailLsitFilter(
			Long campaignID, String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getMailBlockBouncedMailLsitFilter(
			Long campaignID, String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	
}

	public List<MessageBounce> getTotalMailBouncedMailLsitFilter(
			Long campaignID, String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getUnsubscribeMailBouncedMailLsitFilter(
			Long campaignID, String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getAutoReplyMailBouncedMailLsitFilter(
			Long campaignID, String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageBounce> getOthersBouncedMailLsitFilter(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageBounce>();
	}

	public List<MessageSpam> getSpamComplaintMailLsit(Long campaignID,
			String fromDate, String toDate) {
		CampaignDao  cdao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		return new ArrayList<MessageSpam>();
	}
}