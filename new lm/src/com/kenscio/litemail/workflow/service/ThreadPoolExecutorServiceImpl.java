package com.kenscio.litemail.workflow.service;


import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;



/**
 * 
 * @author venu T
 *
 */

public class ThreadPoolExecutorServiceImpl {
	
	 private static final ThreadPoolExecutor pool = new ThreadPoolExecutor(20, 100, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
	
	 	
	 public static void shutdown()
	 {
		 pool.shutdown();
	 }
	 
	
	 public static void executeUploadAsync(final UploadListServiceUtilAsync upload)
	 {
		
		 	pool.execute(new Runnable(){
				 
				 public void run()
				 {
					 upload.execute();
					 
				 }
				 
			 });
			
			 
	 }

	public static void executeEmailAsync(final EmailServiceUtil email)
	{
		
			pool.execute(new Runnable(){
				 
				 public void run()
				 {
					 email.execute();
					 
				 }
				 
			 });
			
			 
	 }
	 
			 
	 
	
		
}
