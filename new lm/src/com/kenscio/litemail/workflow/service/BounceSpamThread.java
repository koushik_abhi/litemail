package com.kenscio.litemail.workflow.service;

import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.domain.Campaign;

public class BounceSpamThread implements AsyncService
{
	private long campaignId = 0;
	private Campaign campaign;
	
	public BounceSpamThread(Campaign campaign )
	{
		this.setCampaign(campaign) ;
	}
	
	public BounceSpamThread(long campaignId )
	{
		this.setCampaignId(campaignId) ;
	}
	@Override
	public void execute() {
		
		try
		{
			MailGunAPIServiceImpl impl = new MailGunAPIServiceImpl();
			
			impl.getBounceAndSpamDataAndUpdate(getCampaign());
			
		}catch(Exception e)
		{
			System.out.println("failed to get mailgun data "+ e.getMessage());
			e.printStackTrace();
		}
			
				
	}
	
	public static long mailGunThreadSleepTime = 30*60*1000; 
	
	public void infiniteRun()
	{
		
		while(true)
		{
			try
			{
				String sleeptime = ApplicationProperty.getProperty("litemail.mailgun.service.sleeptime");
				if(sleeptime != null && sleeptime != "")
				{
					try
					{
						int time = Integer.parseInt(sleeptime);
						mailGunThreadSleepTime = time*60*1000;
					}catch(Exception e)
					{
						System.out.println("using default sleep time for mailgun service"+ mailGunThreadSleepTime);
					}
				}
				MailGunAPIServiceImpl impl = new MailGunAPIServiceImpl();
				impl.getBounceAndSpamDataAndUpdate(getCampaign());
				
			}catch(Exception e)
			{
				System.out.println("failed to get mailgun data "+ e.getMessage());
			}
			try {
				Thread.sleep(mailGunThreadSleepTime);
			} catch (InterruptedException e) {
				
			}
		}
		
	}
	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	public Campaign getCampaign() {
		return campaign;
	}

	public void setCampaign(Campaign campaign) {
		this.campaign = campaign;
	}

}
