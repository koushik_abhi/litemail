package com.kenscio.litemail.workflow.service;

import java.util.List;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.dao.CampaignClickDao;
import com.kenscio.litemail.workflow.domain.CampaignLinks;
import com.kenscio.litemail.workflow.domain.EmailData;

public class CampaignLinkClicksService  {

    public CampaignLinks[] getStampitClicksByCampaignId(Long campaignId){
    	CampaignClickDao  dao = (CampaignClickDao) AppContext.getFromApplicationContext("campaignClickDao");
    	
        List StampitClicksList= dao.getStampitClicksByCampaignId(campaignId);
        return ((StampitClicksList!=null)?
           (CampaignLinks []) StampitClicksList.toArray(new CampaignLinks [StampitClicksList.size()] ):null);

    }

    public CampaignLinks[] getStampitClicksByCampaignIdIncludingInactive(Long campaignId){
    	CampaignClickDao  dao = (CampaignClickDao) AppContext.getFromApplicationContext("campaignClickDao");
    	
        
        List StampitClicksList= dao.getStampitClicksByCampaignIdIncludingInactive(campaignId);
        return ((StampitClicksList!=null)?
           (CampaignLinks []) StampitClicksList.toArray(new CampaignLinks [StampitClicksList.size()] ):null);
    }
    
    public Long saveUpdateStampitClicks(CampaignLinks stampitClicks)
    {
    	CampaignClickDao  dao = (CampaignClickDao) AppContext.getFromApplicationContext("campaignClickDao");
    	
    	dao.saveStampitClicksUpdate(stampitClicks);
        return stampitClicks.getUrlId();
    }
    

    public Long saveStampitClicks(CampaignLinks stampitClicks){
    	CampaignClickDao  dao = (CampaignClickDao) AppContext.getFromApplicationContext("campaignClickDao");
    	dao.saveStampitClicks(stampitClicks);
        return stampitClicks.getUrlId();
    }
   
    public CampaignLinks getURLAndUpdateCounter(Long urlId) {
     	CampaignClickDao  dao = (CampaignClickDao) AppContext.getFromApplicationContext("campaignClickDao");
        
    	CampaignLinks stampitClicks = dao.getStampitClicks(urlId);
        long noOfClicks = stampitClicks.getNoOfClicks() != null ?  stampitClicks.getNoOfClicks() + 1 : 1;
        stampitClicks.setNoOfClicks(noOfClicks);
        dao.saveStampitClicksUpdate(stampitClicks);
        return stampitClicks;
    }

  
    public CampaignLinks getStampitClicks(Long urlId){
    	 CampaignClickDao  dao = (CampaignClickDao) AppContext.getFromApplicationContext("campaignClickDao");
        
         return dao.getStampitClicks(urlId);
    }

	public EmailData[] getClickThroughEmailListsByUrlId(Long campaignId,
			Long urlId) {
		// TODO Auto-generated method stub
		return null;
	}

    

}
