package com.kenscio.litemail.workflow.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;




import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.dao.AccountDao;
import com.kenscio.litemail.workflow.dao.MailListDao;
import com.kenscio.litemail.workflow.domain.AccountCredit;
import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.domain.PartnerEntity;
import com.kenscio.litemail.workflow.domain.RoleEntity;


public class AccountService {

	public static Logger log = Logger.getLogger(AccountService.class.getName());
	

	public void addUser(AccountEntity accountEntity,Long credits) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		accountDao.saveAccount(accountEntity);
		addInitialCredits(accountEntity.getLoginId(), accountEntity.getPartnerName(), accountEntity.getAccountId(), accountEntity.getCreatedBy(),credits);
	}
	
	public void addInitialCredits(String login, String partner, long accountId, String creatdBy,Long credits)
	{
		
		AccountCredit crt = new AccountCredit();
		crt.setAccountId(accountId);
		crt.setUsername(login);
		crt.setPartnername(partner);
		crt.setCredits(0l);
		crt.setBalance(credits);
		crt.setDebits(0l);
		crt.setTransType("initial credits assign");
		crt.setCreatedBy(creatdBy);
		crt.setUpdatedBy(creatdBy);
		crt.setCreatedDate(new Date());
		crt.setUpdatedDate(new Date());
		addUserCredits(crt);
		
	}
	public void deleteUserFromCredit(long accountId,String userId){
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		accountDao.deleteUserFromCredit(userId);
	}


	
	public void addUserCredits(AccountCredit accountEntity) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		accountDao.saveAccountCredits(accountEntity);
				
	}
	
	public RoleEntity[] getAllRoles() {
		AccountDao  dao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return dao.getAllRoles();
        
	}
	
	public AccountCredit getUserCredit(String login) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getAccontCredits(login);
	}
	
	public AccountEntity getUserDetails(String login) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getAccountByLogin(login);
	}
	
	
	public AccountCredit getUserCredit(long accntId) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getAccontCredits(accntId);
	}
	
	public void increaseUserCredits(String userName, long credits, String login) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		accountDao.increaseUserCredits(userName, credits, login);
	}
	
	public void decreaseUserCredits(String userName, long debits, String login) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		accountDao.decreaseUserCredits(userName, debits, login);
	}
	
	
	public void updateUser(AccountEntity accountEntity/*,Long credits*/) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		AccountEntity ae =  accountDao.getAccountByLogin(accountEntity.getLoginId());
		ae.setActive(accountEntity.getIsActive());
		ae.setAddress(accountEntity.getAddress());
		ae.setPassword(accountEntity.getPassword());
		ae.setUserRole(accountEntity.getUserRole());
		ae.setMobileNumber(accountEntity.getMobileNumber());
		ae.setUserName(accountEntity.getUserName());
		ae.setUserType(accountEntity.getUserType());
		ae.setEmailId(accountEntity.getEmailId());
		ae.setPhoneNumber(accountEntity.getPhoneNumber());
		ae.setDescription(accountEntity.getDescription());
		ae.setCostPerMail(accountEntity.getCostPerMail());
		accountDao.updateAccount(ae);
		
		//increaseUserCredits(accountEntity.getUserName(), credits, accountEntity.getLoginId());
	}


	public AccountEntity[] getAllUsers() {
		
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getAllAccounts();
		
	}
	
		
	public List<AccountEntity> getAllUsersInPagination(int pageNumber, int noOfRecords) {
		
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getAllAccountsInPagination(pageNumber, noOfRecords);
		
	}
	
	public List<AccountCredit> getAllUserCreditsInPagination(int pageNumber, int noOfRecords) {
		
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		List<AccountEntity> ac = accountDao.getAllAccountsForCredits();
		List<String> login = new ArrayList<String>();
		if(ac != null)
		{
			for(AccountEntity a : ac)
			login.add(a.getLoginId());
		}
		return accountDao.getAllUserCreditsInPagination(pageNumber, noOfRecords,login);
		
	}
	
	public AccountEntity getUser(long accountId) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getAccount(accountId);
	}		

	public AccountEntity getUser(String username) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getAccount(username);
	}	
	
	public void deleteUser(String login) 
	{
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		accountDao.deleteAccount(login);
	}
	
	
	public PartnerEntity getPartner(long partnerId) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getPartner(partnerId);
	}	
	
	public boolean checkPartnerName(String name) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.checkPartnerName(name);
		
	}

	public void deletePartner(String partnerName) 
	{
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		accountDao.deletePartner(partnerName);
	}
	
	public List<PartnerEntity> getAllPartnersInPagination(int pageNumber, int noOfRecords) {
		
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getAllPartnersInPagination(pageNumber, noOfRecords);
		
	}
	
   public List<PartnerEntity> getAllPartners() {
		
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getAllPartners();
		
	}
	
	public void addPartner(PartnerEntity accountEntity) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		accountDao.addPartner(accountEntity);
	}
		
	public void updatePartner(PartnerEntity accountEntity) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		PartnerEntity pe = accountDao.getPartner(accountEntity.getName());
		pe.setAddress(accountEntity.getAddress());
		pe.setBusinessNature(accountEntity.getBusinessNature());
		pe.setBusinessUrl(accountEntity.getBusinessUrl());
		pe.setComment(accountEntity.getComment());
		pe.setDescription(accountEntity.getDescription());
		pe.setStatus(accountEntity.getStatus());
		pe.setType(accountEntity.getType());
		pe.setEmailId(accountEntity.getEmailId());
		pe.setMobileNumber(accountEntity.getMobileNumber());
		accountDao.updatePartner(pe);
	}

	public boolean isUserExist(String loginId) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.isUserExist(loginId);
	}

	public boolean isPartnerInUse(long accountId, String partnerName) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.isPartnerInUse(accountId, partnerName);
	}

	public void deleteUser(long accountId, String userId) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		accountDao.deleteAccount(userId);
				
	}

	public void updateUserPassword(String newpassword, String username) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		accountDao.updateUserPassword(newpassword, username);
		
		
	}
	
	public int getNoOfUsers(long accountId) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		 return accountDao.getTotalPagesOfUsers(accountId);
	}

	public int getTotalPagesOfPatners() {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getTotalPagesOfPatners();
		
	}
	public int getTotalPagesOfCredits(long accountId) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getTotalPagesOfCredits(accountId);
		
	}

	public String getPartnerName(long accountId) {
		AccountDao  accountDao = (AccountDao) AppContext.getFromApplicationContext("accountDao");
		return accountDao.getPartnerName(accountId);
	}

	

	
}
