package com.kenscio.litemail.workflow.service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.dao.EmailTrackingDao;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.MailGunBounceSpamObject;
import com.kenscio.litemail.workflow.domain.MailGunBounceSpamReport;
import com.kenscio.litemail.workflow.domain.MessageBounce;
import com.kenscio.litemail.workflow.domain.MessageSpam;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

public class MailGunAPIServiceImpl {

	public static String mailGunAPIURL = "https://api.mailgun.net/v2";
	public static String defaultDomain = "hhalerts.in";
	public static String complaints = "complaints";
	public static String bounces = "bounces";
	
	public MailGunAPIServiceImpl() {
		
	}
	
	public  MailGunBounceSpamReport GetSpamComplaints() {
	   String domain = "";
	   domain = ApplicationProperty.getProperty("litemail.mailgun.domain");
	   if(domain == null || domain == "")
	   {
		   domain = defaultDomain;
	   }
	  // System.out.println("getting spams from domain "+domain );
       Client client = new Client();
       client.addFilter(new HTTPBasicAuthFilter("api",MailGunAPIMessageCode.API_Key));
       WebResource webResource =
               client.resource(mailGunAPIURL+"/"+domain+"/" +complaints);
                              
       ClientResponse res = webResource.get(ClientResponse.class);
       ObjectMapper map = new ObjectMapper();
	   MailGunBounceSpamReport mailGun = null;
	   try {
		   // System.out.println("got" + map.readTree(res.getEntityInputStream()));
			mailGun = map.readValue(res.getEntityInputStream(), MailGunBounceSpamReport.class);
			
	   } catch (JsonProcessingException e) {
			
		   System.out.println("error in parsing mailgun data "+ e.getMessage());
		   
	   } catch (IOException e) {
		   System.out.println("error in reading mailgun data "+ e.getMessage());
	   }
	   return mailGun;
	}
	public  MailGunBounceSpamReport GetBounces() {
	   String domain = "";
	   domain = ApplicationProperty.getProperty("litemail.mailgun.domain");
	   if(domain == null || domain == "")
	   {
		   domain = defaultDomain;
	   }
	   //System.out.println("getting bounces from domain "+domain );
       Client client = new Client();
       client.addFilter(new HTTPBasicAuthFilter("api",MailGunAPIMessageCode.API_Key));
       WebResource webResource =
               client.resource(mailGunAPIURL+"/"+domain+"/" +bounces);
      
       ClientResponse res = webResource.get(ClientResponse.class);
       ObjectMapper map = new ObjectMapper();
	   MailGunBounceSpamReport mailGun = null;
	   try {
		 //  System.out.println("got" + map.readTree(res.getEntityInputStream()));
			
			mailGun = map.readValue(res.getEntityInputStream(), MailGunBounceSpamReport.class);
			
	   } catch (JsonProcessingException e) {
			
		   System.out.println("error in parsing mailgun data "+ e.getMessage());
		   
	   } catch (IOException e) {
		   System.out.println("error in reading mailgun data "+ e.getMessage());
	   }
	   
	   return mailGun;
	   
	   
	}
	
	public static void main(String arg[])
	{
		MailGunAPIServiceImpl ap = new MailGunAPIServiceImpl();
		MailGunBounceSpamReport res  = ap.GetBounces();
		
				
	}

	public  void getBounceAndSpamDataAndUpdate(Campaign campaign) 
	{
		try {
			System.out.println("waiting for 5sec before getting bounces from mailgun ");
			Thread.currentThread().sleep(5000);
		} catch (InterruptedException e) {
			System.out.println("error in thread wait for bounces");
		}
		System.out.println("starting mailgun bounce and spam retrieval..." + new Date());
		int spamcount= 0;
		int bouncecount= 0;
		MailGunBounceSpamReport spamReport = GetSpamComplaints();
		if(spamReport != null)
		{
			List<MailGunBounceSpamObject> spams = spamReport.getItems();
			for(MailGunBounceSpamObject spam: spams)
			{
				if(isAddressExistInCampaign(spam, campaign.getCampaignId(), campaign.getListId()))
				{
					addSpamRecords(getSpamRecordFromMailGunObject(spam, campaign.getCampaignId()),campaign.getCampaignId());
					spamcount++;
				}
			}
			
		}
		MailGunBounceSpamReport bounce = GetBounces();
		if(bounce != null)
		{
			List<MailGunBounceSpamObject> bunceRecs = bounce.getItems();
			for(MailGunBounceSpamObject boun: bunceRecs)
			{
				if(isAddressExistInCampaign(boun,campaign.getCampaignId(),campaign.getListId()))
				{
					addBounceRecords(getBounceRecordFromMailGunObject(boun,campaign.getCampaignId()),campaign.getCampaignId());
					bouncecount++;
				}
			}
			
		}
		
		System.out.println("end mailgun bounce and spam retrieval " + new Date() + " spams "+ spamcount + " bounces "+  bouncecount);
		
		
	}

	private boolean isAddressExistInCampaign(MailGunBounceSpamObject boun,
			long campaignId, Long listId) {
		MailListServiceImpl impl = new MailListServiceImpl();
		return impl.isEmailExistInCampaignList(listId, boun.getAddress());
		
	}

	private  MessageBounce getBounceRecordFromMailGunObject(
			MailGunBounceSpamObject boun, long campaignId) {
		MessageBounce bounce = new MessageBounce();
		String error = boun.getError();
		if(error != null )
		{
			String err  =error.toLowerCase();
			if(err.toLowerCase().contains("invalid") || err.toLowerCase().contains("domain name") || err.toLowerCase().contains("does not exist") || 
						err.toLowerCase().contains("delivery error") || err.toLowerCase().contains("unavailable") )
			{
				bounce.setHardBounce(error);
				bounce.setReasonForBounce(error);
			}
			else if(err.toLowerCase().contains("mailbox full") || err.toLowerCase().contains("out of office"))
			{
				bounce.setSoftBounce(error);
				bounce.setReasonForBounce(error);
			}
			else
			{
				bounce.setOtherReason(error);
				bounce.setReasonForBounce(error);
			}
		}
		bounce.setCampaignId(campaignId);
		bounce.setBounceCode(boun.getCode());
		bounce.setReciepientEmail(boun.getAddress());
		bounce.setCreatedDate(getCreatedDateFromString(boun.getCreated_at()));
		return bounce;
	}
	public static String userDefinedTZ = "Asia/Calcutta";
	private  Date getCreatedDateFromString(String created_at) {
		
		SimpleDateFormat sdfgmt = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
		sdfgmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		
		Date outputDate = new Date();
		try {
			outputDate = sdfgmt.parse(created_at);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			
		}
		Calendar quartzStartDate = new GregorianCalendar();
		quartzStartDate.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
	    quartzStartDate.setTime(outputDate);
	    
	//	Date translatedTime = DateBuilder.translateTime(outputDate.getTime(), TimeZone.getDefault(), TimeZone.getTimeZone(userDefinedTZ));
    //	Calendar quartzStartDate = new GregorianCalendar();
    //	quartzStartDate.setTime(translatedTime);
    	
    	return quartzStartDate.getTime();
	//	return outputDate;
	}

	private  MessageSpam getSpamRecordFromMailGunObject(
			MailGunBounceSpamObject spams, long campaignId) {
		MessageSpam spam = new MessageSpam();
		spam.setCampaignId(campaignId);
		spam.setSpamCode(spams.getCode());
		spam.setReciepientEmail(spams.getAddress());
		spam.setCreatedDate(getCreatedDateFromString(spams.getCreated_at()));
		spam.setOtherReason(spams.getError());
		return spam;
	
	}

	private  void addBounceRecords(MessageBounce bounce, long campaignId) {
		EmailTrackingDao  dao = (EmailTrackingDao) AppContext.getFromApplicationContext("emailTrackingDao");
		dao.addEmailBounceStatusUpdate(bounce,campaignId);
		
	}

	private  void addSpamRecords(MessageSpam spam,long campaignId) {
		EmailTrackingDao  dao = (EmailTrackingDao) AppContext.getFromApplicationContext("emailTrackingDao");
		dao.addEmailSpamStatusUpdate(spam,campaignId);
	}
	

}
