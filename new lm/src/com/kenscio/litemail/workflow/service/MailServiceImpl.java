package com.kenscio.litemail.workflow.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.NamingException;

import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.domain.EmailObject;

  
public class MailServiceImpl {  
      
    private static final Logger log = Logger.getLogger(MailServiceImpl.class.getName());  
  
    public void send(EmailObject email)  
            throws MessagingException, NamingException, IOException {  
  
    	Transport transport =null;
    	try {  
         
        Properties props = (Properties) System.getProperties().clone();  
        props.put("mail.transport.protocol", "smtp");  
        props.put("mail.smtp.host", email.getHost());  
        props.put("mail.smtp.port", email.getPort());  
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable","true"); 
      //  props.put("mail.smtp.EnableSSL.enable","true");
       // props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");   
        // props.setProperty("mail.smtp.socketFactory.fallback", "false");   
        
      //  props.put("mail.smtp.socketFactory.port", email.getPort());
      //  props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
    //    props.put("mail.smtp.socketFactory.fallback", "false");
       // props.setProperty("mail.smtp.socketFactory.port", email.getPort()); 
       // props.put("mail.debug", "true");  
       /* Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() 
		{
			protected PasswordAuthentication getPasswordAuthentication()
			{ return new PasswordAuthentication("username","password");	}
		});*/	
        Session session = Session.getDefaultInstance(props, null);  
        MimeMessage message = new MimeMessage(session);  
       
        message.setFrom(InternetAddress.parse(email.getFromAddress(), false)[0]);  
        message.setSubject(email.getSubject());  
        Calendar calendar1 = Calendar.getInstance();
		Date myDate1 = new Date(); // May be your date too.
		calendar1.setTime(myDate1);
        message.setSentDate(calendar1.getTime());
        message.setRecipients(  
            javax.mail.Message.RecipientType.TO,  
            InternetAddress.parse(email.getTo(), false)  
        );  
        if(email.getCc() != null && email.getCc().length() > 0)
        {
        	message.setRecipients(  
                javax.mail.Message.RecipientType.CC,  
                InternetAddress.parse(email.getCc(), false)  
        	);
        }
    
        Multipart multiPart = new MimeMultipart();  
        MimeBodyPart messageText = new MimeBodyPart();  
        messageText.setContent(email.getBody(), "text/plain");  
        multiPart.addBodyPart(messageText);  
         
        message.setContent(multiPart);  
        message.saveChanges();
   
        transport = session.getTransport("smtp");  
  
        
  
            transport.connect( email.getHost(), email.getUsername(), email.getPassword());  
            System.out.println("email transport created message reciepients : " +message.getAllRecipients());
            transport.sendMessage(message, message.getAllRecipients());  
  
            log.log(Level.FINE,  "Email message sent");  
        }catch(Exception e)
        {
        	log.log(Level.SEVERE, "unable to send email "+ e.getMessage() );
  
        } finally { 
        	if(transport != null)
            transport.close();  
        }  
    }
    
    
    // A simple, single-part text/plain e-mail.
    public static void setTextContent(Message msg) throws MessagingException {
            // Set message content
            String mytxt = "This is a test of sending a " +
                            "plain text e-mail through Java.\n" +
                            "Here is line 2.";
            msg.setText(mytxt);
 
            // Alternate form
            msg.setContent(mytxt, "text/plain");
 
    }
 
    // A simple multipart/mixed e-mail. Both body parts are text/plain.
    public static void setMultipartContent(Message msg) throws MessagingException {
        // Create and fill first part
        MimeBodyPart p1 = new MimeBodyPart();
        p1.setText("This is part one of a test multipart e-mail.");
 
        // Create and fill second part
        MimeBodyPart p2 = new MimeBodyPart();
        // Here is how to set a charset on textual content
        p2.setText("This is the second part", "us-ascii");
 
        // Create the Multipart.  Add BodyParts to it.
        Multipart mp = new MimeMultipart();
        mp.addBodyPart(p1);
        mp.addBodyPart(p2);
 
       
        msg.setContent(mp);
    }
 
    // Set a file as an attachment.  Uses JAF FileDataSource.
    public static void setFileAsAttachment(Message msg, String filename)
             throws MessagingException {
 
        // Create and fill first part
        MimeBodyPart p1 = new MimeBodyPart();
        p1.setText("This is part one of a test multipart e-mail." +
                    "The second part is file as an attachment");
 
        // Create second part
        MimeBodyPart p2 = new MimeBodyPart();
 
        // Put a file in the second part
        FileDataSource fds = new FileDataSource(filename);
        p2.setDataHandler(new DataHandler(fds));
        p2.setFileName(fds.getName());
 
        Multipart mp = new MimeMultipart();
        mp.addBodyPart(p1);
        mp.addBodyPart(p2);
 
        // Set Multipart as the message's content
        msg.setContent(mp);
    }
 
    // Set a single part html content.
    // Sending data of any type is similar.
    public static void setHTMLContent(Message msg) throws MessagingException {
 
        String html = "<html><head><title>" +
                        msg.getSubject() +
                        "</title></head><body><h1>" +
                        msg.getSubject() +
                        "</h1><p>This is a test of sending an HTML e-mail" +
                        " through Java.</body></html>";
 
        // HTMLDataSource is an inner class
        msg.setDataHandler(new DataHandler(new HTMLDataSource(html)));
    }
 
    /*
     * Inner class to act as a JAF datasource to send HTML e-mail content
     */
    static class HTMLDataSource implements DataSource {
        private String html;
 
        public HTMLDataSource(String htmlString) {
            html = htmlString;
        }
 
        // Return html string in an InputStream.
        // A new stream must be returned each time.
        public InputStream getInputStream() throws IOException {
            if (html == null) throw new IOException("Null HTML");
            return new ByteArrayInputStream(html.getBytes());
        }
 
        public OutputStream getOutputStream() throws IOException {
            throw new IOException("This DataHandler cannot write HTML");
        }
 
        public String getContentType() {
            return "text/html";
        }
 
        public String getName() {
            return "JAF text/html dataSource to send e-mail only";
        }
    }
 
   
    
}