package com.kenscio.litemail.workflow.service;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Logger;


import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;




public class JobsManagementExecuter	implements Job
{
	public static Logger log = Logger.getLogger(JobsManagementExecuter.class.getName());

	@Override
	public void execute(JobExecutionContext ctx) throws JobExecutionException {

		
		JobKey jobKey = ctx.getJobDetail().getKey();
	
		String jobName = jobKey.getName();
		List<String> jbName = new ArrayList<String>();
		String campaignName = "";
		String listName = "";
		
		StringTokenizer st = new StringTokenizer(jobName, ":");
		while (st.hasMoreElements()) {
			jbName.add((String) st.nextElement());
		}
		campaignName = jbName.get(0);
		listName = jbName.get(1);

		System.out.println(campaignName + " and " + listName);

		CampaignService service=new CampaignService();
		try {
			
			service.startCampaignSchedule(campaignName);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
		
	
}
