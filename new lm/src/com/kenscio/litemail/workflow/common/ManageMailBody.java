package com.kenscio.litemail.workflow.common;


import java.util.*;
import java.io.InputStream;


/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Jun 25, 2008
 * Time: 1:27:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class ManageMailBody {
    private  final String imageStart="src=";
    private  final String imageEnd=",";
    private  final String imageEnd1="]";
    private  final String slash="/";
    private  final String httpEnd="://";
    private  final String http="http";
    private  final String https="https";
    private  final String cssImageLinkStart = ": url(";
    private  final String cssImageLinkEnd = ")";

    public ManageMailBodyResponse saveExtractedMailBodyAndGetHtmlContent(InputStream zipFileStream, String output, String replacePathWithoutFileName){
        ManageMailBodyResponse response = new ManageMailBodyResponse();

        String htmlName = ZipFileUtil.zipFilesAndGetHtmlName(zipFileStream,output);
        System.out.println(htmlName  + "htmlname");
        String htmlFilePath = output + htmlName;
        System.out.println(htmlFilePath + "html file path");
        if(htmlName == null && htmlName.length() <=0)
        	return response;
        MailBodyPath[] mailBodyStylesheetPaths= null;
        MailBodyPath[] mailBodyImagePaths = null;
        MailBodyPath[] mailBodyStylesheetImagePaths = null;
        MailBodyHtmlParser mailBodyHtmlParser = new MailBodyHtmlParser(htmlFilePath);

        /***************************
         * Manage External Style Sheet Links :
         * First step here is getting all style sheet links from MailBodyHtmlParser
         * Second step is to get Present Style Sheet Links(Orginal Links) and Replaced (Where it is saved) Links
         * Third step is if there is any external style sheet/s find and replace
         *  image paths of external style sheet/s and save
         ****************************/
        Set styleSheetCollections = mailBodyHtmlParser.getStyleSheetLinksFromHtmlFile();
        if(styleSheetCollections.size() > 0){
            mailBodyStylesheetPaths = getOriginalAndReplacedStyleSheetPaths(styleSheetCollections, replacePathWithoutFileName);
            if(mailBodyStylesheetPaths !=null && mailBodyStylesheetPaths.length > 0){
                findReplaceImagePathsInExternalStyleSheetAndSave(mailBodyStylesheetPaths, mailBodyHtmlParser, replacePathWithoutFileName);
            }
        }

        /***************************
         * Manage Image Paths of Style :
         * First step here is getting all style sheet Image Paths <img src= ..> and <input type=image..>) from
         *   MailBodyHtmlParser
         * Second step is to get Present Image Paths From Style(Orginal Links) and Replaced Image Paths(Where it is saved)
         ****************************/
        Collection styleSheets  = mailBodyHtmlParser.getStyleSheetsFromHtmlFile();
        if(styleSheets.size() > 0){
            Set styleSheetsImages = getStyleSheetImagePaths(styleSheets.toString());
            mailBodyStylesheetImagePaths = getOriginalAndReplacedStyleSheetPaths(styleSheetsImages, replacePathWithoutFileName);
        }

       /***************************
         * Manage Image Paths :
         * First step here is getting all Image Paths from  MailBodyHtmlParser
         * Second step is to get Present Image Paths (Orginal Links) and Replaced Image Paths(Where it is saved)
        ****************************/
        Set imageCollections = mailBodyHtmlParser.getImagePathsFromHtmlFile();
        if(imageCollections.size() > 0){
             mailBodyImagePaths = getOriginalAndReplacedImagePaths(imageCollections, replacePathWithoutFileName);
        }

       /***************************
         * Get Html Content With Replaced Paths:
         * First step is add all original and replaced paths into one MailBodyPath[]
         * Read Html File and Replace original paths with replaced paths
        ****************************/
        MailBodyPath[] styleAndImagePaths = addStylePathsAndImagePaths(mailBodyStylesheetPaths,mailBodyImagePaths,mailBodyStylesheetImagePaths);
        if(styleAndImagePaths!=null && styleAndImagePaths.length > 0){
            response.setHtmlContent(FileUtil.replaceAndSaveHtml(styleAndImagePaths,htmlFilePath));
        }else{
            response.setHtmlContent(FileUtil.getContentsFromFile(htmlFilePath));
        }
        response.setHtmlName(htmlName.substring(1)); // to remove "/"
        return response;
    }

    private void findReplaceImagePathsInExternalStyleSheetAndSave(MailBodyPath[] mailBodyStylesheetPaths, MailBodyHtmlParser mailBodyHtmlParser, String replacePath) {
        for (int i = 0; i < mailBodyStylesheetPaths.length; i++) {
            MailBodyPath mailBodyStylesheetPath = mailBodyStylesheetPaths[i];
            String cssFileContent = FileUtil.getContentsFromFile(mailBodyStylesheetPath.getReplacedPath());
            Set styleSheetsImages = getStyleSheetImagePaths(cssFileContent);
            if(styleSheetsImages.size() > 0){
                MailBodyPath[] externalStylesheetImagePaths = getOriginalAndReplacedStyleSheetPaths(styleSheetsImages, replacePath);
                FileUtil.replaceAndSaveHtml(externalStylesheetImagePaths,mailBodyStylesheetPath.getReplacedPath());
            }
        }
    }

    private Set getStyleSheetImagePaths(String styles) {
        Set styleSheetsImages = new TreeSet();
        int start = 0;
        int end = 0;
        while((end = styles.toLowerCase().indexOf(cssImageLinkStart,start)) > 0){
            String cssImagePathStart = styles.substring(end+cssImageLinkStart.length());
            int pos1 = cssImagePathStart.indexOf(cssImageLinkEnd);
            if(pos1 != -1){
                String imagePath = cssImagePathStart.substring(0,pos1);
                styleSheetsImages.add(imagePath);
                start = end + imagePath.length()+1;
            }else{
                start = end + cssImageLinkStart.length();
            }
        }
        return styleSheetsImages;
    }

    private MailBodyPath[] getOriginalAndReplacedStyleSheetPaths(Set styleSheetCollections, String replacePathWithoutFileName) {
        ArrayList stylePaths = new ArrayList();
        for (Iterator iterator = styleSheetCollections.iterator(); iterator.hasNext();) {
            String styleSheetPath = (String)iterator.next();
            if(!checkForStartsWithHttp(styleSheetPath) && !styleSheetPath.equals(getReplacedPath(styleSheetPath,replacePathWithoutFileName))){
                MailBodyPath mailBodyPath = new MailBodyPath();
                mailBodyPath.setOriginalPath(styleSheetPath);
                mailBodyPath.setReplacedPath(getReplacedPath(styleSheetPath,replacePathWithoutFileName));
                stylePaths.add(mailBodyPath);
            }
        }
        return (MailBodyPath[])stylePaths.toArray (new MailBodyPath[stylePaths.size()]);
    }

    private boolean checkForStartsWithHttp(String originalPath) {
        boolean status = true;
        int pos = originalPath.indexOf(httpEnd);
        if(-1 != pos){
            if(!originalPath.substring(0,pos).equalsIgnoreCase(http) &&
                    !originalPath.substring(0,pos).equalsIgnoreCase(https)){
                status = false;
            }
        }else{
            status = false;
        }
        return status;
    }

    private MailBodyPath[] getOriginalAndReplacedImagePaths(Set imageCollections, String replacePathWithoutFileName) {
       ArrayList imagePaths =new ArrayList();
        for (Iterator iterator = imageCollections.iterator(); iterator.hasNext();) {
            String orignalImagePath = getOrignalImagePath((String)iterator.next());
            if(!checkForStartsWithHttp(orignalImagePath) && !replacePathWithoutFileName.equals(getReplacedPath(orignalImagePath,replacePathWithoutFileName))){
                MailBodyPath mailBodyPath = new MailBodyPath();
                mailBodyPath.setOriginalPath(orignalImagePath);
                mailBodyPath.setReplacedPath(getReplacedPath(orignalImagePath,replacePathWithoutFileName));
                imagePaths.add(mailBodyPath);
           }
        }
        return (MailBodyPath[])imagePaths.toArray(new MailBodyPath[imagePaths.size()]);
    }

    private String getReplacedPath(String orignalPath, String replacePathWithoutFileName) {
    	return orignalPath;
       // int pos = orignalPath.lastIndexOf(slash);
        //String fileName = orignalPath.substring(pos+1);
        //return replacePathWithoutFileName+fileName;
    }

    private String getOrignalImagePath(String imagePath) {
        int startPos = imagePath.indexOf(imageStart);
        String imageStartPath = imagePath.substring(startPos+imageStart.length());
        int endPos= imageStartPath.indexOf(imageEnd);
        if(endPos==-1)endPos= imageStartPath.indexOf(imageEnd1);
        return imageStartPath.substring(0,endPos);
    }

    private MailBodyPath[] addStylePathsAndImagePaths(MailBodyPath[] stylePaths, MailBodyPath[] imagePaths, MailBodyPath[] styleImagePaths){
        ArrayList pathList = new ArrayList();
        if(stylePaths != null && stylePaths.length > 0){
            for (int i = 0; i < stylePaths.length; i++) {
                pathList.add(stylePaths[i]);
            }
        }
        if(imagePaths != null && imagePaths.length > 0){
            for (int i = 0; i < imagePaths.length; i++) {
                pathList.add(imagePaths[i]);
            }
        }
        if(styleImagePaths != null && styleImagePaths.length > 0){
            for (int i = 0; i < styleImagePaths.length; i++) {
                pathList.add(styleImagePaths[i]);

            }
        }
        return (MailBodyPath[])pathList.toArray(new MailBodyPath[pathList.size()]);
    }

    public ManageMailBodyResponse ReplaceImageAndStylePathAndGetResponse(String  newFilePath, String newReplacePathWithoutFileName, String oldReplacePathWithoutFileName){
        ManageMailBodyResponse response = new ManageMailBodyResponse();
        String htmlFileName = FileUtil.getHtmlFileName(newFilePath);

        String htmlFilePath = newFilePath+slash+htmlFileName;
        MailBodyPath[] mailBodyPath = getPathToReplace(oldReplacePathWithoutFileName,newReplacePathWithoutFileName);
        response.setHtmlContent(FileUtil.replaceAndSaveHtml(mailBodyPath,htmlFilePath));
        response.setHtmlName(htmlFileName);
        return response;
    }

    private MailBodyPath[] getPathToReplace(String input, String output) {
        MailBodyPath[] mailBodyPaths = new MailBodyPath[1];
        MailBodyPath mailBodyPath = new MailBodyPath();
        mailBodyPath.setOriginalPath(input);
        mailBodyPath.setReplacedPath(output);
        mailBodyPaths[0] = mailBodyPath;
        return mailBodyPaths;
    }
}
