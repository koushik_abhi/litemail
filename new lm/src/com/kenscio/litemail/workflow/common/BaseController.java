package com.kenscio.litemail.workflow.common;

import javax.servlet.http.HttpServletRequest;

public class BaseController {

	SessionDTO sessionDTO=new SessionDTO();

	 public void setSessionDTO(HttpServletRequest request,SessionDTO dto){
         request.getSession().setAttribute("sessionDTO",dto);
    }

     public SessionDTO getSessionDTO(HttpServletRequest request){
         return (SessionDTO)request.getSession().getAttribute("sessionDTO");
     }
	
	
}
