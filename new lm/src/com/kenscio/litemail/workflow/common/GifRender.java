package com.kenscio.litemail.workflow.common;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Sep 21, 2006
 * Time: 12:55:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class GifRender extends HttpServlet {

    protected Color transparentColor;


    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        String imageName = request.getParameter("imgName");
        String accId = request.getParameter("accId");
        String campaignId = request.getParameter("campaignId");
        String type=request.getParameter("type");
        String imagePath ;
        if(type==null){
          imagePath=ApplicationProperty.getProperty("stampit.image.dir")
                + "/" + accId + "/" + campaignId + "/"+ imageName;
        }else if("showcase".equals(type)){ // campaign showcase image
          imagePath=ApplicationProperty.getProperty("stampit.image.dir")
                + "/" + accId + "/" + campaignId + "/showcase/"+ imageName;
        }else if("charity".equals(type)){ // charity logo image
          imagePath=ApplicationProperty.getProperty("stampit.image.dir")
                +"/charity/"+ imageName;
        }else   // mailbody image
          imagePath=ApplicationProperty.getProperty("stampit.image.dir")
          + "/" + accId + "/" + campaignId + "/inline/"+imageName;
        BufferedImage image;
        try {
            File imageFile = new File(imagePath);
            if(type!=null&&type.equals("1")){
               if(imageFile.canRead()){
            	 sendImageFile(imageFile,response);
               }
            }
            else if (imageFile.canRead()) {
                image = ImageIO.read(imageFile);
                if (image != null && image.getHeight() > 0 && image.getWidth() > 0)
                    writeGif(request, response, image);
                else
                    throw new Exception();
            } else {
                writeDefaultImage(request, response);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            writeDefaultImage(request, response);
        }
    }
    private void sendImageFile(File file,HttpServletResponse response)
    {
      try {
		InputStream in=new FileInputStream(file);
		  OutputStream out=response.getOutputStream();
		  byte bytes[]=new byte[4096];
		  int read;
		  while((read=in.read(bytes,0,4096))>0){
			   out.write(bytes,0,read);
		  }
		  out.flush();
		  out.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    }
    private void writeDefaultImage(HttpServletRequest request, HttpServletResponse response) {
        try {
            String imagePath = ApplicationProperty.getProperty("stampit.image.dir");
            imagePath = imagePath + "/" + ApplicationProperty.getProperty("stampit.image.redstamp");
            File imageFile = new File(imagePath);
            BufferedImage image = ImageIO.read(imageFile);
            writeGif(request, response, image);
        }
        catch (Exception ex) {
            ex.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void writeGif(HttpServletRequest request, HttpServletResponse response, Image image) throws IOException {
        OutputStream out = null;

        /*  if (cache == null)
            // Get the binary stream headed for the browser
            out = response.getOutputStream();
        else*/
        // Save the resulting byte array
        out = new ByteArrayOutputStream();

        // Set the content-type header

        response.setContentType("image/gif");

        // Encode the image as a GIF (Thanks to Jef Poskanzer! www.acme.com)
        GifEncoder encoder;

        /* if (transparentColor != null)
             encoder = new GifEncoder(new FilteredImageSource(image.getSource(), new TransparentFilter(transparentColor)), out);
         else*/
        encoder = new GifEncoder(image, out);

        encoder.encode();

        /*    if (cache != null)
        {*/
        byte[] gif = ((ByteArrayOutputStream) out).toByteArray();
        out = response.getOutputStream();

        //cache.put(getQueryString(request), gif);

        out.write(gif, 0, gif.length);
        /*      }*/

        // Send the GIF to the browser
        out.flush();
    }
}
