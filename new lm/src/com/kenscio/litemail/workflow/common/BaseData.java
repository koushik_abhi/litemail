package com.kenscio.litemail.workflow.common;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;


public class BaseData implements Serializable {
    private String createdBy;
    @JsonSerialize(using=JsonDateSerializer.class)   
    private Date createdDate;
    private String updatedBy;
    @JsonSerialize(using=JsonDateSerializer.class)
    private Date updatedDate;
    private long versionNumber=-1;
    private String os="";
    private String device="";
    private String browser = "";
        
    public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public BaseData()
    {
    	
    }
    
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    @JsonSerialize(using=JsonDateSerializer.class)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
       
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    @JsonSerialize(using=JsonDateSerializer.class)
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = updatedDate;
        
    }

    public long getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(long versionNumber) {
        this.versionNumber = versionNumber;
    }
}
