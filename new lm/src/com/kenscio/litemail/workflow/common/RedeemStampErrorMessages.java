package com.kenscio.litemail.workflow.common;

import java.util.Map;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Oct 16, 2006
 * Time: 2:50:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class RedeemStampErrorMessages {

     public static Integer VALID_STAMP = new Integer(0);
     public static Integer SENDER_NO_FUNDS = new Integer(1);
     public static Integer INVALID_STAMP = new Integer(2);
     public static Integer BAD_RECIPIENT = new Integer(3);
     public static Integer STAMP_EXPIRED = new Integer(4);
     public static Integer STAMP_ALREADY_REDEEMD = new Integer(5);


    /*
         #define RTCD_StampInfoRequest_SNDR_NO_FUNDS		1
         #define RTCD_StampInfoRequest_STAMP_INVALID		2
         #define RTCD_StampInfoRequest_BAD_RECIPIENT		3
         #define RTCD_StampInfoRequest_STAMP_EXPIRED		4
         #define RTCD_StampInfoRequest_ALREADY_COLLECTED	5
    */
    public static String getMessage(Integer responseCode){
        return (String)messageMap.get(responseCode);
    }

    private static Map messageMap = new HashMap();
    static{
        messageMap.put(SENDER_NO_FUNDS,"Sender has no funds to honour this stamp");
        messageMap.put(INVALID_STAMP,"Invalid Stamp");
        messageMap.put(BAD_RECIPIENT,"Bad recipient");
        messageMap.put(STAMP_EXPIRED,"Expired Stamp");
        messageMap.put(STAMP_ALREADY_REDEEMD,"Stamp has been already redeemed");
    }
}
