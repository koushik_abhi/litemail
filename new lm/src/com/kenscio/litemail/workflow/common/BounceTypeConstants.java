package com.kenscio.litemail.workflow.common;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Apr 19, 2007
 * Time: 2:22:26 PM
 * To change this template use File | Settings | File Templates.
 */
public interface BounceTypeConstants {

public static final String HARD_BOUNCE ="HB";

public static final String SOFT_BOUNCE="SB";
public static final String SOFT_BOUNCE_DNS_FAIL="SBDF";
public static final String SOFT_BOUNCE_MAIL_FULL="SBMF";
public static final String SOFT_BOUNCE_SIZE_LARGE="SBMS";

public static final String GENERAL_BOUNCE="GB";

public static final String MAIL_BLOCK_GENERAL="MB";
public static final String MAIL_BLOCK_KNOWN_SPAMMER="MBKS";
public static final String MAIL_BLOCK_SPAM_DETECTED="MBSD";
public static final String MAIL_BLOCK_ATTACHMENT="MBAD";
public static final String MAIL_BLOCK_RELAY_DENIED="MBRD";
    
public static final String AUTO_REPLY="AR";
public static final String TRANSIENT_BOUNCE="TB";
public static final String SUBSCRIBE_REQUEST="SR";
public static final String UNSUBSCRIBE_REQUEST="UR";
public static final String CHALLENGE_RESPONSE="CR";
public static final String BOUNCE_NO_EMAIL_RETURNED="BN";
public static final String NON_BOUNCE="NB";
}
