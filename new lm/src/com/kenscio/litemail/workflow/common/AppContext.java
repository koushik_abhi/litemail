package com.kenscio.litemail.workflow.common;

import org.springframework.context.ApplicationContext;

import com.kenscio.litemail.workflow.common.LoggerUtil;


public class AppContext 
{
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static void setApplicationContext(ApplicationContext applicationContext) {
        AppContext.applicationContext = applicationContext;
    }


    public static Object getFromApplicationContext(String beanName) {
        Object obj = null;
        try {
            obj = applicationContext.getBean(beanName);
           
        }
        catch (Exception e) {
        	 System.out.println("error in creating object");
            LoggerUtil.doLog("Error in creating object "+beanName);
        }

        return obj;

    }

    public static Object getFromApplicationContext(Class idClass) {
        Object obj = null;
        System.out.println(idClass.getName());
        try {
            obj = applicationContext.getBean(idClass.getName());
        }
        catch (Exception e) {
        	System.out.println("error in creating object");
            LoggerUtil.doLog("Error in creating object "+idClass.getName());
        }

        return obj;

        
    }

    public static String getContextPath() {
		return contextPath;
	}

	public static void setContextPath(String contextPath) {
		AppContext.contextPath = contextPath;
	}

	private static ApplicationContext applicationContext;
    private static String contextPath;
}
