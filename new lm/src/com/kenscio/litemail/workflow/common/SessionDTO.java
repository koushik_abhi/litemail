package com.kenscio.litemail.workflow.common;

/**
 * Created by IntelliJ IDEA.
 * User: Comp
 * Date: Jul 11, 2006
 * Time: 11:26:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class SessionDTO extends BaseData{
    private Long accountId;
    private Long listId;
    private String loginEmailId;


    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public String getLoginEmailId() {
        return loginEmailId;
    }

    public void setLoginEmailId(String loginEmailId) {
        this.loginEmailId = loginEmailId;
    }
}
