package com.kenscio.litemail.workflow.common;

import java.io.InputStream;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.quartz.CronExpression;
import org.quartz.DateBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;
import org.quartz.impl.triggers.SimpleTriggerImpl;

import com.kenscio.litemail.workflow.domain.JobDetails;
import com.kenscio.litemail.workflow.domain.JobDetailsObject;

import com.kenscio.litemail.workflow.service.JobsManagementExecuter;

public class QuartzJobsServiceImpl  {
	
	public static Logger log = Logger.getLogger(QuartzJobsServiceImpl.class.getName());
	private static QuartzJobsServiceImpl qService = new QuartzJobsServiceImpl();
	private static StdSchedulerFactory scheduleFactory = null;
	private static Scheduler sched = null;
	
	static
	{
	
		try{
		
			scheduleFactory = new StdSchedulerFactory();
			Properties prop  = new Properties();
			InputStream props = QuartzJobsServiceImpl.class.getResourceAsStream("quartz.properties");
			prop.load(props);
			scheduleFactory.initialize(prop);
			sched = scheduleFactory.getScheduler();
			
			System.out.println("schedular created with name:  "+sched.getSchedulerName());
			
		}catch (Exception e) {
			e.printStackTrace();
			log.log(Level.SEVERE, "unable to get the schedular "+ e.getMessage());
			
		}
		
		
	}
	
	private Map<String, Integer> pasrseDateStringForTime(String startTime)
	{
		
		 Map<String, Integer> datetime = new HashMap<String, Integer>();
		 if(startTime == null || startTime =="")
			 return null;
		 startTime = startTime.trim();
		 int ind = startTime.indexOf("-");
		 int date = 1;
		 if(ind > 0)
		 {
			 String da = startTime.substring(0,ind);
			 startTime = startTime.substring(ind+1);
			 date = Integer.parseInt(da);
		 }
		 
		 datetime.put("dayOfMonth", date);
		 int ind1 = startTime.indexOf("-");
		 int mon = 1;
		 if(ind1 > 0)
		 {
			 String da = startTime.substring(0,ind1);
			 startTime = startTime.substring(ind1+1);
			 mon = Integer.parseInt(da);
		 }
		 datetime.put("month", mon);
		
		 int ind2 = startTime.indexOf(" ");
		 int year = 1;
		 if(ind2 > 0)
		 {
			 String da = startTime.substring(0,ind2);
			 startTime = startTime.substring(ind2+1);
			 year = Integer.parseInt(da);
		 }
		 datetime.put("year", year);
		
		 int ind3 = startTime.indexOf(":");
		 int hour = 0;
		 int min = 0;
		 if(ind3 > 0)
		 {
			 String da = startTime.substring(0,ind3);
			 startTime = startTime.substring(ind3+1);
			 hour = Integer.parseInt(da);
		 }
		 datetime.put("hour", hour);
		
		 if(startTime != "")
		 {
			 min = Integer.parseInt(startTime);
			 
		 }
		 datetime.put("minute", min);
		
		return datetime;
	}
	
	private Date convertDateToServerTimeZone(String startTime) {
		 
		int year =0;
		int month =1;
		int dayOfMonth =1; 
		int hour=0;
		int minute =0;
		int sec = 10; 
		
		Map<String, Integer> datetime = pasrseDateStringForTime(startTime);
		
		year = datetime.get("year");
		month = datetime.get("month");
		dayOfMonth = datetime.get("dayOfMonth");
		hour = datetime.get("hour");
		minute = datetime.get("minute");
		month = month-1;
				
		Calendar userStartDefinedTime = Calendar.getInstance();
		userStartDefinedTime.set(year, month, dayOfMonth, hour, minute, sec);
		
		Calendar userDefinedTime = Calendar.getInstance();
	    userDefinedTime.setTime(userStartDefinedTime.getTime());
	    return userDefinedTime.getTime();
	  //  return userDefinedTime.getTime();
	/*    if(!TimeZone.getDefault().getID().equalsIgnoreCase(userDefinedTZ)) 
	    {
	      
	    	Date translatedTime = DateBuilder.translateTime(userDefinedTime.getTime(), TimeZone.getDefault(), TimeZone.getTimeZone(userDefinedTZ));
	    	Calendar quartzStartDate = new GregorianCalendar();
	    	quartzStartDate.setTime(translatedTime);
	    	
	    	return quartzStartDate.getTime();
	      
	    } else 
	    {
	    	return userDefinedTime.getTime();
	    }*/
	}
	
	
	public static String userDefinedTZ = "Asia/Calcutta";
		
	private Calendar convertDateToServerTimeZone(String startTime, int year,int month, int dayOfMonth, int hour,int minute,int sec ) {
	   
		Calendar userStartDefinedTime = Calendar.getInstance();
		userStartDefinedTime.set(year, month, dayOfMonth, hour, minute, sec);
		Calendar userDefinedTime = Calendar.getInstance();
	    userDefinedTime.setTime(userStartDefinedTime.getTime());
	    
	    if(!TimeZone.getDefault().getID().equalsIgnoreCase(userDefinedTZ)) 
	    {
	      
	    	Date translatedTime = DateBuilder.translateTime(userDefinedTime.getTime(), TimeZone.getDefault(), TimeZone.getTimeZone(userDefinedTZ));

	    	Calendar quartzStartDate = new GregorianCalendar();
	    	quartzStartDate.setTime(translatedTime);
	    	
	    	return quartzStartDate;
	      
	    } else 
	    {
	    	return userDefinedTime;
	    }
	}
	

	
	
	public void createJobAndStart(JobDetailsObject jobDetail, boolean update) throws SchedulerException, ParseException
	{
		JobDetail job=(JobDetail) JobBuilder.newJob((Class<? extends Job>) JobsManagementExecuter.class)
  		      .withIdentity(jobDetail.getCampaignName()+":"+jobDetail.getListName(),"startcampaigngruop")
  		      .storeDurably()
  		      .build();
		
		sched.addJob(job, update);
		addTrigger(jobDetail,job);
		startSchedular();
								
	}
	
	public void updateJobAndStart(JobDetailsObject jobDetail, boolean update) throws SchedulerException, ParseException
	{
		JobDetail job=(JobDetail) JobBuilder.newJob((Class<? extends Job>) JobsManagementExecuter.class)
	  		      .withIdentity(jobDetail.getCampaignName()+":"+jobDetail.getListName(),"startcampaigngruop")
	  		      .storeDurably()
	  		      .build();
			
		
		sched.addJob(job, update);
		
		addTrigger(jobDetail, job);
		startSchedular();		
		
	}
	
	
	
	public void deleteJob(JobDetailsObject jobDetail) throws SchedulerException
	{
		
		JobDetail job=(JobDetail) JobBuilder.newJob((Class<? extends Job>) JobsManagementExecuter.class)
	  		      .withIdentity(jobDetail.getCampaignName()+":"+jobDetail.getListName())
	  		      .storeDurably()
	  		      .build();
			
		if(sched.checkExists(job.getKey()))
		{
			sched.deleteJob(job.getKey());
			TriggerKey key = new TriggerKey(jobDetail.getCampaignName()+"_trigger");
			sched.unscheduleJob(key);
			
		}
		startSchedular();
			
				
	}
	

	
	
	public JobDetail getJobDetails(String jobName) throws SchedulerException 
	{
		JobKey key = new JobKey(jobName, "startcampaigngruop");
		return sched.getJobDetail(key);	
						
	}
	
		
		
	
	public void addTrigger(JobDetails jobDetail, JobDetail job) throws SchedulerException, ParseException {
		
		
		Map<String, Integer> datetimemap = pasrseDateStringForTime(jobDetail.getScheduleDate());
		
		int hrs = datetimemap.get("hour");
		int min = datetimemap.get("minute");
		int dayOfMonth = datetimemap.get("dayOfMonth");
		int month = datetimemap.get("month");
		int year = datetimemap.get("year");
		
			
			createJobTriggerOnceInFuture(jobDetail,  job, year, month, dayOfMonth, min, hrs, 0);
			return;
		
		
		
	}
	
	private void createJobTriggerOnceInFuture(JobDetails jobDetail, JobDetail job, int year, int month, int dayOfMonth,  int hrs, int min, int sec ) throws ParseException, SchedulerException
	{
		
		TriggerKey key = new TriggerKey(jobDetail.getCampaignName()+"_trigger");
		sched.unscheduleJob(key);
		String str = sec+" "+min+" "+hrs+" "+dayOfMonth+" "+month+" * "+year;
		Date startT = convertDateToServerTimeZone(jobDetail.getScheduleDate());
		//Date startT=jobDetail.getScheduleDate();
		//System.out.println(jobDetail.getScheduleDate() + "schedule time" + startT +"after conversion");
		SimpleTriggerImpl trigger= (SimpleTriggerImpl) TriggerBuilder.newTrigger()
		         .withIdentity(jobDetail.getCampaignName()+"_trigger")
		         .forJob(job).startAt(startT)
		         .build();
		 trigger.setStartTime(convertDateToServerTimeZone(jobDetail.getScheduleDate()));
		 sched.scheduleJob(trigger);
		 startSchedular();
		
	}
	
	
	public static QuartzJobsServiceImpl getInstance()
	{
		return qService;
		
	}
	
	public static void shutdownSchedular() throws SchedulerException
	{
		if(sched != null)
			try {
				
				sched.shutdown();
				
			} catch (SchedulerException e) {
				log.log(Level.SEVERE, "unable to shutdown the job schedular "+ e.getMessage());
				throw e;
			}
		
	}
	
	public boolean isJobExist(String jobName) throws SchedulerException {
		
		JobKey key = new JobKey(jobName);
		return sched.checkExists(key);
	}
	
	public  void startSchedular() throws SchedulerException
	{
		if(sched != null)
			try {
				
				if(sched.isShutdown() || !sched.isStarted())
				{
					sched.start();
				}
				
			} catch (SchedulerException e) {
				log.log(Level.SEVERE, "unable to shutdown the job schedular "+ e.getMessage());
				throw e;
			}
		
	}
	
	public static void main(String args[] )
	{
		QuartzJobsServiceImpl imp = new QuartzJobsServiceImpl();
		int hr = 12;
		String hrt = "0/"+hr;
		String str  = "0 0 "+hrt+" 1/1 * ? *";
		CronExpression exp = null;
		try {
			
			exp = new CronExpression(str);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		System.out.println(exp.toString());
	}


	
	

}
