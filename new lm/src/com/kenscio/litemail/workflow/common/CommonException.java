package com.kenscio.litemail.workflow.common;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jul 1, 2006
 * Time: 3:32:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class CommonException extends RuntimeException{
    public CommonException() {
    }

    public CommonException(String string) {
        super(string);
    }

    public CommonException(String string, Throwable throwable) {
        super(string, throwable);
    }

    public CommonException(Throwable throwable) {
        super(throwable);
    }
}
