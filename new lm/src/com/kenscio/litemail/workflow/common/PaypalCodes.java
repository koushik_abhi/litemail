package com.kenscio.litemail.workflow.common;

import com.paypal.soap.api.CountryCodeType;
import com.paypal.soap.api.CreditCardTypeType;

import java.util.Map;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Aug 14, 2006
 * Time: 4:28:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class PaypalCodes {
    private static Map paypalCountryCodeMap = new HashMap();
    private static Map paypalCardTypeMap = new HashMap();

    public static CountryCodeType getPaypalCountryCode(String countryCode){
        return (CountryCodeType)paypalCountryCodeMap.get(countryCode);
    }

    public static CreditCardTypeType getPaypalCreditCardType(String creditCard){
        return (CreditCardTypeType) paypalCardTypeMap.get(creditCard);
    }

    static{
        paypalCountryCodeMap.put("AI", CountryCodeType.AI);
        paypalCountryCodeMap.put("AR", CountryCodeType.AR);
        paypalCountryCodeMap.put("AU", CountryCodeType.AU);
        paypalCountryCodeMap.put("AT", CountryCodeType.AT);
        paypalCountryCodeMap.put("BE", CountryCodeType.BE);
        paypalCountryCodeMap.put("BR", CountryCodeType.BR);
        paypalCountryCodeMap.put("CA", CountryCodeType.CA);
        paypalCountryCodeMap.put("CL", CountryCodeType.CL);
        paypalCountryCodeMap.put("CN", CountryCodeType.CN);
        paypalCountryCodeMap.put("CR", CountryCodeType.CR);
        paypalCountryCodeMap.put("CY", CountryCodeType.CY);
        paypalCountryCodeMap.put("CZ", CountryCodeType.CZ);
        paypalCountryCodeMap.put("DK", CountryCodeType.DK);
        paypalCountryCodeMap.put("DO", CountryCodeType.DO);
        paypalCountryCodeMap.put("EC", CountryCodeType.EC);
        paypalCountryCodeMap.put("EE", CountryCodeType.EE);
        paypalCountryCodeMap.put("FI", CountryCodeType.FI);
        paypalCountryCodeMap.put("FR", CountryCodeType.FR);
        paypalCountryCodeMap.put("DE", CountryCodeType.DE);
        paypalCountryCodeMap.put("GR", CountryCodeType.GR);
        paypalCountryCodeMap.put("HK", CountryCodeType.HK);
        paypalCountryCodeMap.put("HU", CountryCodeType.HU);
        paypalCountryCodeMap.put("IS", CountryCodeType.IS);
        paypalCountryCodeMap.put("IN", CountryCodeType.IN);
        paypalCountryCodeMap.put("IE", CountryCodeType.IE);
        paypalCountryCodeMap.put("IL", CountryCodeType.IL);
        paypalCountryCodeMap.put("IT", CountryCodeType.IT);
        paypalCountryCodeMap.put("JM", CountryCodeType.JM);
        paypalCountryCodeMap.put("JP", CountryCodeType.JP);
        paypalCountryCodeMap.put("LV", CountryCodeType.LV);
        paypalCountryCodeMap.put("LT", CountryCodeType.LT);
        paypalCountryCodeMap.put("LU", CountryCodeType.LU);
        paypalCountryCodeMap.put("MY", CountryCodeType.MY);
        paypalCountryCodeMap.put("MT", CountryCodeType.MT);
        paypalCountryCodeMap.put("MX", CountryCodeType.MX);
        paypalCountryCodeMap.put("NL", CountryCodeType.NL);
        paypalCountryCodeMap.put("NZ", CountryCodeType.NZ);
        paypalCountryCodeMap.put("LT", CountryCodeType.LT);
        paypalCountryCodeMap.put("NO", CountryCodeType.NO);
        paypalCountryCodeMap.put("PL", CountryCodeType.PL);
        paypalCountryCodeMap.put("PT", CountryCodeType.PT);
        paypalCountryCodeMap.put("SG", CountryCodeType.SG);
        paypalCountryCodeMap.put("SK", CountryCodeType.SK);
        paypalCountryCodeMap.put("SI", CountryCodeType.SI);
        paypalCountryCodeMap.put("ZA", CountryCodeType.ZA);
        paypalCountryCodeMap.put("KR", CountryCodeType.KR);
        paypalCountryCodeMap.put("ES", CountryCodeType.ES);
        paypalCountryCodeMap.put("SE", CountryCodeType.SE);
        paypalCountryCodeMap.put("CH", CountryCodeType.CH);
        paypalCountryCodeMap.put("TW", CountryCodeType.TW);
        paypalCountryCodeMap.put("TH", CountryCodeType.TH);
        paypalCountryCodeMap.put("GB", CountryCodeType.GB);
        paypalCountryCodeMap.put("US", CountryCodeType.US);
        paypalCountryCodeMap.put("UY", CountryCodeType.UY);
        paypalCountryCodeMap.put("VE", CountryCodeType.VE);


        paypalCardTypeMap.put("VISA", CreditCardTypeType.Visa);
        paypalCardTypeMap.put("MASTER", CreditCardTypeType.MasterCard);
        paypalCardTypeMap.put("AMEX", CreditCardTypeType.Amex);
        paypalCardTypeMap.put("DISCOVER", CreditCardTypeType.Discover);

    }
}
