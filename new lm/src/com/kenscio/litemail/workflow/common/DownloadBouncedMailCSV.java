package com.kenscio.litemail.workflow.common;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.BufferedOutputStream;

/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Apr 23, 2007
 * Time: 11:18:19 AM
 * To change this template use File | Settings | File Templates.
 */
public class DownloadBouncedMailCSV extends HttpServlet {
     public void init(ServletConfig servletConfig) throws ServletException
     {
         super.init(servletConfig);
     }

    public void doPost(HttpServletRequest request, HttpServletResponse responce){
        doGet(request,responce);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        String campaignId=request.getParameter("campaignId");
        String csvExt=ApplicationProperty.getProperty("stampit.csv.ext");
        String filePath= ApplicationProperty.getProperty("stampit.csv.home")+
                        "/bounce"+ "/" + campaignId + "/bounce" +"."+csvExt;
        try {
               FileInputStream fileInputStream = new FileInputStream(filePath);
               response.setContentType("application/x-download");
               response.setHeader("Content-Disposition", "attachment; filename=" + "bounce" + "." + csvExt );
               response.setHeader("Cache-Control", "max-age=0");
               ServletOutputStream out = response.getOutputStream();
               writeToServletOutputStream(fileInputStream,out);
           } catch (FileNotFoundException e) {
               e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
           } catch (IOException e) {
               e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
           }
    }

    public void writeToServletOutputStream(FileInputStream is,ServletOutputStream out) throws IOException {
        BufferedOutputStream outStream =new BufferedOutputStream(out);
        try {
            int c;
            while((c= is.read()) != -1){
                 outStream.write(c);
                outStream.flush();
            }
        } catch (IOException e) {
            throw e;
        }finally{
            outStream.close() ;
        }
    }
}
