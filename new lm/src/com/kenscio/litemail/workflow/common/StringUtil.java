package com.kenscio.litemail.workflow.common;

import org.springframework.util.StringUtils;

import java.util.regex.Pattern;
import java.util.Random;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.UnsupportedEncodingException;

import sun.misc.BASE64Encoder;
import com.unearthedjava.passwordPress.UnearthedPassword;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jun 13, 2006
 * Time: 2:51:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class StringUtil {
    public static final String EMPTY = "";

     public static String replace(String string, String pattern,
                                  String value) {
        StringBuffer returnValue   = new StringBuffer();
        int          patternLength = pattern.length();
        while (true) {
            int idx = string.indexOf(pattern);
            if (idx < 0) {
                break;
            }
            returnValue.append(string.substring(0, idx));
            if (value != null) {
                returnValue.append(value);
            }
            string = string.substring(idx + patternLength);
        }
        returnValue.append(string);
        return returnValue.toString();
    }

    /**
     * Replaces all occurrences of "patterns" in "v" with "values"
     *
     * @param v            original String
     * @param patterns     patterns to match
     * @param values       replacement values
     * @return  munged String
     */
    public static String replaceList(String v, String[] patterns,
                                     String[] values) {
        for (int i = 0; i < patterns.length; i++) {
            v = replace(v, patterns[i], values[i]);
        }
        return v;
    }

    public static String removeWhitespace(String inputString) {
        StringBuffer sb    = new StringBuffer();
        char[]       chars = inputString.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if (Character.isWhitespace(c)) {
                continue;
            }
            sb.append(c);
        }
        return sb.toString();
    }

    public static String getValidCSVFileName(String inputString){
       String[] patterns = {"_","-","/"};
       String[] values   = {"","",""};
       inputString = removeWhitespace(inputString);
       inputString = replaceList(inputString,patterns,values);
       return inputString;
    }

   /* public static String splitStringOf15Length(String inputString){
        String returnString ="";
        if(inputString != null && inputString.length() > 15){
            int totalLength = inputString.length();
            int position =0;
            while(position < totalLength){
                if((position+15) <= totalLength) {
                  returnString += inputString.substring(position,(position+15))+"/n";
                  position=+15;
                }else{
                  returnString += inputString.substring(position,totalLength)+"/n";
                  position=totalLength;
                }

            }
        }
        return returnString;
    }*/

    public static String ImageFileNameReplaceWithCampaignName(String str, String str1, String str2) {
        String ret = StringUtils.replace(str, str1, str2);
        return ret;
    }

    public static boolean isValidEmail(String email) {
       boolean flag=true;
      
       if(email.trim().equals(""))
    	   return false;
       
        if(!email.equals("")){
             String pattern = "^[a-zA-Z0-9][\\w.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
             flag = Pattern.matches(pattern, email);
        }
        return flag;
    }

    public static boolean isValidAlphaNumeric(String value) {
       boolean flag=true;
        if(!value.equals("")){
             String pattern = "^[a-zA-Z0-9'.`-�\\s]*$";
             flag = Pattern.matches(pattern, value);
        }
        return flag;
    }

    public static boolean isValidEmails(String emails){
        emails=emails.replaceAll("\n",",");
        emails=emails.replaceAll("\r ",",");
        emails = StringUtil.removeWhitespace(emails);
        emails=emails.replaceAll(",,",",");
        System.out.print("util >>>>>>>"+emails);
        String[] emailAddresses=emails.split(",");
        if(emailAddresses.length==1){
            boolean flag=false;
            for (int i = 0; i < emailAddresses.length; i++) {
                if(!emailAddresses[i].equals("")){
                String emailAdress = emailAddresses[i];
                 //System.out.println("emailAdress @ util"+emailAdress); 
                 flag=isValidEmail(emailAdress.trim());
                }
            }
            return flag;
        }else{
            int counter=0;
            for (int i = 0; i < emailAddresses.length; i++) {
                if(!emailAddresses[i].equals("")){
                String emailAdress = emailAddresses[i];
                if(!isValidEmail(emailAdress.trim())){
                    counter++;
                    }
                }
            }
            return counter <= 0;
        }
    }

    public static String substringBefore(String str, String separator) {
        if (isEmpty(str) || separator == null) {
            return str;
        }
        if (separator.length() == 0) {
            return EMPTY;
        }
        int pos = str.indexOf(separator);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }

    public static String substringAfter(String str, String separator) {
            if (isEmpty(str)) {
                return str;
            }
            if (separator == null) {
                return EMPTY;
            }
            int pos = str.indexOf(separator);
            if (pos == -1) {
                return EMPTY;
            }
            return str.substring(pos + separator.length());
    }

    public static String substringBeforeFromEnd(String str, String separator) {
        if (isEmpty(str) || separator == null) {
            return str;
        }
        if (separator.length() == 0) {
            return EMPTY;
        }
        int pos = str.lastIndexOf(separator);
        if (pos == -1) {
            return str;
        }
        return str.substring(0, pos);
    }

    public static String substringAfterFromEnd(String str, String separator) {
            if (isEmpty(str)) {
                return str;
            }
            if (separator == null) {
                return EMPTY;
            }
            int pos = str.lastIndexOf(separator);
            if (pos == -1) {
                return EMPTY;
            }
            return str.substring(pos + separator.length());
    }

    public static boolean isEmpty(String str) {
        return (str == null || str.length() == 0);
    }

    public static String encryptedPassword(String password){
        MessageDigest md = null;
        try{
            md = MessageDigest.getInstance("SHA"); // SHA-1 message digest algorithm used
            md.update(password.getBytes("UTF-8")); // password into a byte-representation using UTF-8 encoding format
            }catch (NoSuchAlgorithmException e) {
            e.getMessage();
            }catch(UnsupportedEncodingException e) {
            e.getMessage();
            }
        byte raw[] = md.digest();
        String encyptedPassword = (new BASE64Encoder()).encode(raw);
        return encyptedPassword;
    }

    public static String generateAlphaNumericKey(int length){

        try {
            UnearthedPassword password = new UnearthedPassword(length, // length required
                                                           false, // allow duplicate characters
                                                           true, // mix letter case
                                                           UnearthedPassword.IS_ALPHANUM); // alphanumeric
            return password.getNewPassword();
        } catch (Exception e) {
            return ""+new Random(3).nextInt();
        }
    }

    public static String convetArraytoString(String[] arrString){
        StringBuffer retString=new StringBuffer();
        if(arrString.length > 0){
            retString.append(arrString[0]);
            for (int i = 1; i < arrString.length; i++) {
                retString.append(","); 
                retString.append(arrString[i]);
            }
        }
        return retString.toString();
    }

    public static String searchAndReplace(String str, String pattern, String replace){
        int start= 0;
        int end = 0;
        StringBuffer result = new StringBuffer();
        while((end = str.indexOf(pattern,start)) >= 0){
            result.append(str.substring(start,end));
            result.append(replace);
            start = end + pattern.length();
        }
        result.append(str.substring(start));
        return result.toString();
    }

   public static boolean isHtmlFileName(String entryName) {
        int pos = entryName.lastIndexOf(".");
        if((-1 != pos && entryName.substring(pos).equalsIgnoreCase(".html"))
                || (-1 != pos && entryName.substring(pos).equalsIgnoreCase(".htm"))){
            return true;
        }else{
            return false;
        }
    }

}
