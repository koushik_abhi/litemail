package com.kenscio.litemail.workflow.common;


import java.io.*;


public class FileUtil {
 public static boolean saveFileToDisk(String fullFileName,byte[] data){
        File file = new File(fullFileName);
        if (file.exists()) {
            file.delete();
        }
        try {
            if (file.createNewFile()) {
                FileOutputStream out = new FileOutputStream(file);
                PrintStream p = new PrintStream(out);                     
                p.write(data);
                p.close();
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
            LoggerUtil.doLog(e.getMessage());
            return false;
        }
        return false;
    }

     public static String getFullFilePath(String documentRoot,Long oivAccountId,Long campaignId,
                                          String fileNameWithExt){
        String folderPath = documentRoot + "/" + oivAccountId.toString();
        File folder = new File(folderPath);
        if (folder.isDirectory()) {
            String folderPathWithCampaignId = folderPath + "/" + campaignId;
            File folderWithCampaignId = new File(folderPathWithCampaignId);
            if (folderWithCampaignId.isDirectory()) {
                String imageFullFilePath = folderWithCampaignId + "/" + fileNameWithExt;
                return imageFullFilePath;
            } else {
                folderWithCampaignId.mkdir();
                String imageFullFilePath = folderWithCampaignId + "/" + fileNameWithExt ;
                return imageFullFilePath;
            }
        } else {
            folder.mkdir();
            String folderPathWithCampaignId = folderPath + "/" + campaignId;
            File folderWithCampaignId = new File(folderPathWithCampaignId);
            folderWithCampaignId.mkdir();
           //String imageName = StringUtil.ImageFileNameReplaceWithCampaignName(campaign.getImageFileName(), campaign.getImageFileName(), campaign.getCampaignName());
            String imageFullFilePath = folderWithCampaignId + "/" + fileNameWithExt ;
            return imageFullFilePath;
        }

    }



  public static byte[] getFileByFullFilePath(String fullFileName){
      File file = new File(fullFileName);
      InputStream is = null;
      try {
          long length = file.length();
         /* if (length > Integer.MAX_VALUE) {
              // File is too large
          }*/
          byte[] bytes = new byte[(int)length];
          is = new FileInputStream(file);
          int offset = 0;
          int numRead = 0;
          while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length-offset)) >= 0) {
              offset += numRead;
          }
          // Ensure all the bytes have been read in
          if (offset < bytes.length) {
              throw new IOException("Could not completely read file "+file.getName());
          }
          is.close();
          return bytes;
      } catch (IOException e) {
          e.printStackTrace();
          return null;
      }
  }

    public static boolean createDirectory(String folderPath){
     String path = org.apache.velocity.texen.util.FileUtil.mkdir(folderPath);
     //   System.out.println(path);
     return true;
    }

    /****************
     * Following methods are used in ManageMailBody class
     * @param input
     * @param output
     */


    public static void saveAllFilesInFolder(String input, String output){
        File inputFile = new File(input);
        File[] files = inputFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            File file = files[i];
            byte[] bytes = getFileByFullFilePath(file.getPath());
            saveFileToDisk(output+"/"+file.getName(),bytes);
        }
    }

    public static boolean deleteFile(String filePath){
        try{
            File folder = new File(filePath);
            if(folder.exists()){
                File[] files = folder.listFiles();
                for (int i = 0; i < files.length; i++) {
                    files[i].delete();
                }
            }
            return true;
        }catch(Exception e){
            return false;
        }
    }

    public static String getHtmlFileName(String input){
        String htmlFile = "";
        File inputFile = new File(input);
        File[] files = inputFile.listFiles();
        for (int i = 0; i < files.length; i++) {
            if(StringUtil.isHtmlFileName(files[i].getName())){
                htmlFile = files[i].getName();
                break;
            }
        }
        return htmlFile;
    }

    public static String replaceAndSaveHtml(MailBodyPath[] mailBodyImagePath, String htmlPath) {
        String htmlContent="";
        try {
            FileInputStream fis = new FileInputStream(htmlPath);
            int size = fis.available();
            byte[] b = new byte[size];
            if(fis.read(b)!=-1){
                htmlContent = new String(b);
                for (int i = 0; i < mailBodyImagePath.length; i++) {
                    MailBodyPath bodyPath = mailBodyImagePath[i];
                    System.out.println("saving >>"+bodyPath.getReplacedPath());
                    htmlContent = StringUtil.searchAndReplace(htmlContent,bodyPath.getOriginalPath(),bodyPath.getReplacedPath());
                }
            }
            FileWriter fw= new FileWriter(htmlPath);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(htmlContent);
            bw.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
        return htmlContent;
    }

    public static String getContentsFromFile(String filePath){
       String content = "";
        try {
            FileInputStream fis = new FileInputStream(filePath);
            int size = fis.available();
            byte[] b = new byte[size];
            fis.read(b);
            content = new String(b);
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
}
