package com.kenscio.litemail.workflow.common;


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Nov 23, 2006
 * Time: 4:30:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class Download extends HttpServlet {
        public void init(ServletConfig servletConfig) throws ServletException
     {
         super.init(servletConfig);
     }


    public void doPost(HttpServletRequest request, HttpServletResponse response)
        {
            doGet(request, response);
        }

       public void doGet(HttpServletRequest request, HttpServletResponse response) {
        String fileName = request.getParameter("fn");
        String accId = request.getParameter("aId");
        String campaignId = request.getParameter("cId");
        String filePath= ApplicationProperty.getProperty("stampit.attachment.dir")
                + "/" + accId + "/" + campaignId + "/" + fileName;
           try {
               FileInputStream fileInputStream = new FileInputStream(filePath);
               response.setContentType("application/binary");
               response.setHeader("Content-Disposition", "attachment; filename=\""
                               + fileName + "\";");
               response.setHeader("Cache-Control", "no-cache");
               ServletOutputStream out = response.getOutputStream();
               writeToServletOutputStream(fileInputStream,out);
           } catch (FileNotFoundException e) {
               e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
           } catch (IOException e) {
               e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
           }
       }

     public static void writeToServletOutputStream(InputStream is,
                                                   ServletOutputStream out) throws IOException {
        BufferedOutputStream outputStream = new BufferedOutputStream(out);
        try {
            int c;
            while((c = is.read()) != -1){
                outputStream.write(c);
                outputStream.flush();
            }
        } catch (IOException e) {
            throw e;
        }finally{
            outputStream.close() ;
        }

    }
}
