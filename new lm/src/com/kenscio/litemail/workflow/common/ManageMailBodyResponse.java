package com.kenscio.litemail.workflow.common;

/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Jun 26, 2008
 * Time: 7:04:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class ManageMailBodyResponse {
    private String htmlName;
    private String htmlContent;

    public String getHtmlName() {
        return htmlName;
    }

    public void setHtmlName(String htmlName) {
        this.htmlName = htmlName;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }


}
