package com.kenscio.litemail.workflow.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;



/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jun 13, 2006
 * Time: 10:58:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class ApplicationProperty {

    // TODO: currently the file name of the application properties is hardcoded.
    private static final String APP_PROP_FILE_NAME = "application";
    //private static final String APP_PROP_FILE_NAME_TARGET_ENV = "application-";
   // private static final String BUILD_PROP_FILE_NAME_TARGET_ENV = "build-";


    
   // private static ResourceBundle envPropertyBundle = null;
  
    private static Set envBundleKeySet = new HashSet();
    private static Properties prop  = new Properties();

    public static void init(String targetEnv) {

    	
    	InputStream props = ApplicationProperty.class.getResourceAsStream("application.properties");
		try {
			prop.load(props);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(" properties read:  "+prop);
		
    	
        /*envPropertyBundle =
                PropertyResourceBundle.getBundle(APP_PROP_FILE_NAME_TARGET_ENV + targetEnv);*/
        envBundleKeySet = getKeySet(prop);

       // buildPropertyBundle = PropertyResourceBundle.getBundle(BUILD_PROP_FILE_NAME_TARGET_ENV + targetEnv);

    }


    public static String getProperty(String key)
    {
        return (envBundleKeySet.contains(key))
            ? prop.getProperty(key)
            : prop.getProperty(key);
    }

    @SuppressWarnings("unchecked")
	private static Set getKeySet(Properties bundle) {
        Set ret = new HashSet();
        Enumeration enume = bundle.keys();
        while (enume.hasMoreElements())
        {
            ret.add(enume.nextElement());
        }
        return ret;
    }


   /* public static String getBuildProperty(String key) {

        return buildPropertyBundle.getString(key);
    }*/

    public static Set getValueSet(String resourceName){
        ResourceBundle bundle = PropertyResourceBundle.getBundle(resourceName);
        Set ret = new HashSet();
        Enumeration bundleKeys = bundle.getKeys();
        while (bundleKeys.hasMoreElements()) {
            String key = (String) bundleKeys.nextElement();
            String value = bundle.getString(key);
            ret.add(value);
        }
        return ret;
    }

}

