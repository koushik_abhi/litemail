package com.kenscio.litemail.workflow.common;

import org.springframework.ui.velocity.VelocityEngineUtils;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Aug 10, 2006
 * Time: 1:51:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class TemplateEngine {
    
	private static VelocityEngine velocityEngine = new VelocityEngine();
	private static TemplateEngine templateEngine = new TemplateEngine();
	
	public static TemplateEngine getInstnace()
	{
		return templateEngine;
	}
    public String getEmailBody(String emailFileName, Map values){
          String result = null;
            try {
                result = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
                                "velocity/"+emailFileName, values);
                                //"/velocity/"+
            } catch (VelocityException e) {
                e.printStackTrace();
            }
            return result;
    }

    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }
}
