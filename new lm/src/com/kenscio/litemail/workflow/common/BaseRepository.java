package com.kenscio.litemail.workflow.common;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jun 13, 2006
 * Time: 5:11:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class BaseRepository extends
        HibernateDaoSupport {

    public BaseData get(Class className,Long id){
    
      return (BaseData) getHibernateTemplate().get(className, id);
    }
    
    public Session getSessionFromBase()
    {
    	return getSession();
    }

    public void save(BaseData data){
        getHibernateTemplate().saveOrUpdate(data);
       
    }
    
    public void saveOnly(BaseData data){
        getHibernateTemplate().save(data);
       
    }
    
    public void update(BaseData data){
        getHibernateTemplate().update(data);
       
    }

    public void merge(BaseData data){
        getHibernateTemplate().merge(data);
       
    }

    
    public void delete(BaseData data){
        getHibernateTemplate().delete(data);
    }
}
