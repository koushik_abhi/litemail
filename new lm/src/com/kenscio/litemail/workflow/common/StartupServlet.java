package com.kenscio.litemail.workflow.common;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServlet;
import javax.servlet.UnavailableException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

import com.kenscio.litemail.emts.syncer.BounceSpamAsyncServiceExecutor;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.service.SyncBounceSpamThread;


/**
 * Created by IntelliJ IDEA.
 * User: Jisso  
 * Date: Jun 15, 2006
 * Time: 11:50:03 AM
 * To change this template use File | Settings | File Templates.
 */
public class StartupServlet extends HttpServlet {
    public void init(ServletConfig config) throws UnavailableException {    	
        //TODO: change it to Logger
    	try{
        System.out.println("*********************StartupServlet.init litemail*****************");
        ApplicationProperty.init("application");
       // String[] paths = {"applicationContext.xml"};
        ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        ServletContext context = config.getServletContext();
        context.setAttribute("appContext", appContext);
        AppContext.setApplicationContext(appContext);
        System.out.println("*********************spring context created*****************");
     //   SyncBounceSpamThread th  = new SyncBounceSpamThread();
      //  th.start();
        /*CharityStandingSchedular charityStandingSchedular = new CharityStandingSchedular();
        charityStandingSchedular.start();*/
    //BounceSpamAsyncServiceExecutor ex = new BounceSpamAsyncServiceExecutor();
     // ex.start();
        
        
    	}catch(Exception e)
    	{
    		System.out.println("*****startup servlet not able to start****************");
    		e.printStackTrace();
    	}
               
    }
}
