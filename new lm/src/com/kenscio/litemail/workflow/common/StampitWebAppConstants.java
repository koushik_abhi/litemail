package com.kenscio.litemail.workflow.common;

/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jul 25, 2006
 * Time: 12:33:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class StampitWebAppConstants {



    //BusinessAdminLogin related
    public static String UN_REGISTERED_BUSINESS_ADMIN_LOGIN_ID="UnRegisteredBusinessAdminLoginId";
    public static String BUSINESS_ADMIN_LOGIN_WRONG_PASSWORD="WrongPassword";

    //Login related
    public static String LOGIN_WRONG_PASSWORD = "WrongPassword";
    public static String LOGIN_NON_EXISTANT_EMAIL = "NoPrimaryEmail";
    public static String LOGIN_NOT_ACTIVATED_EMAIL = "NotActivatedEmail";

    //Duplicate confirmation mail related
    public static String ALREADY_ACTIVATED_EMAIL = "NotActivatedEmail";
    //Register related

    public static String REGISTER_ALREADY_PRIMARY = "ExistingPrimary";
    public static String REGISTER_ALREADY_SECONDARY = "ExistingSecondary";
    public static String REGISTER_ALREADY_RUPEEMAIL = "ExistingRupeeMail";
    //compose mail realted

    public static String MAIL_ATTACHMENT_ERROR="attachmentFailed";
    public static String STAMP_GENERATION_FAILD="stampGenerationFailed";
    public static String SEND_MAIL_GENERAL_ERROR="sendMailFailed";
    public static String NO_VALID_ACCOUNT_LEVEL = "NotVerifiedCreditCard";

    //Fund Transfer related

    public static String IN_SUFFICIENT_FUNDS="InSufficientFunds";
    public static String EMAIL_NOT_REGISTERED_WITH_OIVMAIL="UnRegisteredOIVEmailID";
    public static String SAME_EMAIL="SameEmail";
    public static String NOT_VALID_AMOUNT = "NotValidAmount";
    public static String AMOUNT_IS_ZERO="AmountIsZero";
    public static String TRANSFER_FUND_SEND_MAIL_FAILURE="sendMailFailed";

    //Balance Enquiry email realted

    public static String NOT_VALID_ACCOUNT = "NoAccount";
    public static String SEND_MAIL_FAILURE = "SendMailFailure";

    //OIV account creation related

    public static String OIV_ACCOUNT_ALREADY_EXIST = "OIVACCEXITS";
    public static String STAMPIT_ACCOUNT_ALREADY_EXIST = "STAMPITACCEXITS";
    public static String OIV_ACCOUNT_CREATION_ERROR = "OIVACCCREERR";
    public static String OIV_ACCOUNT_TRY_ANOTHER_EMAIL="OIVACCTRYANOTHEREMAIL";

    //Click Through Link related

    public static String CLICK_THROUGH_LINK_ALREADY_PRESENT = "Wrong Selection";
    public static String CLICK_THROUGH_LINK_SAME_URLNAME = "Same URL Name";

    //Mail Query category related
    public static String ACTIVE_MAIL_CATEGORY_EXISTS = "ActiveMailCategory exists";


    //campaign related
    public static String MAILINGLIST_EMPTY="Selected MailingList is empty";
    public static String INVALID_ZIP_FILE="Invalid Zip File";
}
