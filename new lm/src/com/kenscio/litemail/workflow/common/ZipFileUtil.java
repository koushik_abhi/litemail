package com.kenscio.litemail.workflow.common;

import java.util.zip.ZipInputStream;
import java.util.zip.ZipEntry;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Jun 25, 2008
 * Time: 1:09:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class ZipFileUtil {
     public static String zipFilesAndGetHtmlName(InputStream zipFileStream, String destinationPath) {
         String htmlFileName ="";
         try {
            ZipInputStream zipinputstream = new ZipInputStream(zipFileStream);
            ZipEntry zipentry = zipinputstream.getNextEntry();
            while (zipentry != null) { //for each entry to be extracted
                String entryName = zipentry.getName();
               // System.out.println("zipFileName entryName  "+entryName);
                int pos = entryName.indexOf("/");
                entryName = entryName.substring(pos);
                
                if(StringUtil.isHtmlFileName(entryName))
                    htmlFileName = entryName;
              //  System.out.println("htmlFileName htmlFileName "+htmlFileName);
                File newFile = new File(entryName);
                
                if(newFile.isDirectory())
                {
                	zipentry = zipinputstream.getNextEntry();
                	continue;
                }
                String zipFileName = newFile.getParent();
               // System.out.println("zipFileName parent  "+zipFileName);
                if(zipFileName == null){
                    break;
                }
               
                File saveFileName = new File(destinationPath);
                if(!saveFileName.exists()) {
                    saveFileName.mkdir();
                }
                
                FileOutputStream fileoutputstream = new FileOutputStream(destinationPath+entryName);
               
                byte[] buf = new byte[1024];
                int n;
                while ((n = zipinputstream.read(buf, 0, 1024)) > -1)
                    fileoutputstream.write(buf, 0, n);

               fileoutputstream.close();
               zipinputstream.closeEntry();
               zipentry = zipinputstream.getNextEntry();
            }//while
            zipinputstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return htmlFileName;
    }

     public static boolean isValidZipFileWithHtmlAndNoDirectory(InputStream zipFileStream) {
        boolean status = false;
        try {
            ZipInputStream zipinputstream = new ZipInputStream(zipFileStream);
            ZipEntry zipentry = zipinputstream.getNextEntry();
            int htmlCount = 0;
            while (zipentry != null) { //for each entry to be extracted
                if(zipentry.isDirectory()){
                	status = false;
                    break;
                }
                String entryName = zipentry.getName();
               
                int pos = entryName.indexOf("/");
                entryName = entryName.substring(pos);
                if(StringUtil.isHtmlFileName(entryName))
                    htmlCount++;
                    //FileOutputStream fileoutputstream = new FileOutputStream(entryName);
                    //fileoutputstream.close();
                    zipentry = zipinputstream.getNextEntry();
                }//while
                status = htmlCount == 1;
               // zipinputstream.close();
                status  = true;
        } catch (Exception e) {
            status = false;
            e.printStackTrace();
        }
      
        return status;
    }
}
