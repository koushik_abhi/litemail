package com.kenscio.litemail.workflow.common;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Aug 10, 2006
 * Time: 1:40:22 PM
 * To change this template use File | Settings | File Templates.
 */
public interface MailTypeConstants {
    public static final String MAIL_UFO ="0";
    public static final String MAIL_UNSTAMPED="1";
    public static final String MAIL_ORDINARY = "2";
    public static final String MAIL_REQRESP="3";
    public static final String MAIL_PRIORITY="4";
    public static final String MAIL_URGENT="5";
    public static final String MAIL_SECURE="6";
    public static final String MAIL_SIGNED="7";
    public static final String MAIL_SYSTEM="8";
    public static final String MAIL_PERM_CODE = "9";
    public static final String MAIL_STAMP_ACK="20";
}
