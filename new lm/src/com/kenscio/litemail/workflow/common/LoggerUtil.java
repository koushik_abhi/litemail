package com.kenscio.litemail.workflow.common;

import org.apache.log4j.Logger;
import org.apache.log4j.Level;



/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jun 13, 2006
 * Time: 11:26:27 AM
 * To change this template use File | Settings | File Templates.
 */
public class LoggerUtil {
	private static Logger  oivLogger;
    private static void initLogger(){
    	oivLogger =  Logger.getLogger("stampit.logger");
        
    }
    public static void setLoggerLevel(String level){
    	if(level.equalsIgnoreCase("INFO"))
    		oivLogger.setLevel(Level.INFO);
    	else if(level.equalsIgnoreCase("DEBUG"))
    		oivLogger.setLevel(Level.DEBUG);
    	else if(level.equalsIgnoreCase("ERROR"))
    		oivLogger.setLevel(Level.ERROR);
    }
    
    public static void doLog(String logString){
    initLogger();
    if(oivLogger != null){
    if(Level.DEBUG.equals(oivLogger.getLevel())){
    oivLogger.debug(logString);
    }else if(Level.INFO.equals(oivLogger.getLevel())){
    oivLogger.info(logString);
    }else if(Level.ERROR.equals(oivLogger.getLevel())){
    oivLogger.error(logString);
    }
    }
   }
}
