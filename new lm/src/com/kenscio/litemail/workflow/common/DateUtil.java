package com.kenscio.litemail.workflow.common;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil 
{

	public static String userDefinedTZ = "Asia/Calcutta";
	public static String getLocalDateString(Date date)
	{		
		SimpleDateFormat dateFormat =  new SimpleDateFormat ();
		dateFormat.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
		Calendar originalDate = Calendar.getInstance();
		originalDate.setTime(date);
		originalDate.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
		String formattedDate = dateFormat.format(originalDate.getTime());
		return formattedDate;
		
	}
	public static Date getLocalDate(Date date)
	{		
		SimpleDateFormat dateFormat =  new SimpleDateFormat ();
		dateFormat.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
		Calendar originalDate = Calendar.getInstance();
		originalDate.setTime(date);
		originalDate.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
		return originalDate.getTime();
		
	}
	
	public static Date getDateFromString(String date, String format)
	{		
		SimpleDateFormat dateFormat =  new SimpleDateFormat (format);
		Date formattedDate;
		try {
			formattedDate = dateFormat.parse(date);
		} catch (ParseException e) {
			return DateUtil.getLocalDate(new Date());
		}
		return formattedDate;
		
	}
	
	public static Date getLocalDateFromString(String date)
	{		
		SimpleDateFormat dateFormat =  new SimpleDateFormat ();
		dateFormat.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
		Date formattedDate;
		try {
			formattedDate = dateFormat.parse(date);
		} catch (ParseException e) {
			return new Date();
		}
		return formattedDate;
		
	}
	
	public static String formatDateForFormat(Date date, String format)
	{	
		SimpleDateFormat dateFormat = null;
		try
		{
			dateFormat =  new SimpleDateFormat(format);
		}catch(Exception e)
		{
			dateFormat = new SimpleDateFormat(); 
		}
		dateFormat.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
		return dateFormat.format(date);
			
	} 
	
	
    public static float getDifferenceInhours( Calendar date1,Calendar date2 )
    {

        long differnceInMillis = date1.getTimeInMillis() - date2.getTimeInMillis();

        float noOfHours =  (float) differnceInMillis / ( 1000*60*60 );

        return noOfHours;

    }

     public static long getDifferenceInSeconds( Date date1,Date date2 )
    {
        long differnceInMillis = date1.getTime() - date2.getTime();
        long timeInSeconds =  Math.abs( differnceInMillis / 1000);
        return timeInSeconds;

    }

    public static Date getDateFromStringDayMonthAndYear(String day, String month, String year){
        if(day != null && month != null && year != null){
            String selectedDate = day+"/"+month+"/"+year;
            DateFormat df= new SimpleDateFormat("dd/MM/yyyy");
            Date date = null;
            try {
                date = df.parse(selectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return date;
        }else{
            return null;
        }
    }

    public static String getDayFromDate(Date date){
        if(date != null){
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return ""+cal.get(Calendar.DATE);
        }else{
            return "";
        }
    }

    public static String getMonthFromDate(Date date){
        if(date != null){
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int month = cal.get(Calendar.MONTH)+1;
            return ""+month;
        }else{
            return "";
        }
    }

    public static String getYearFromDate(Date date){
        if(date != null){
           Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            return ""+cal.get(Calendar.YEAR);
        }else{
            return "";
        }
    }

    public static String getStringDateFromDate(Date date){
        if(date != null){
            DateFormat df= new SimpleDateFormat("dd/MM/yyyy");
            return df.format(date);
        }else{
            return "";
        }
    }

    public static String getStringUSFormatDateFromDate(Date date){
        if(date != null){
            DateFormat df= new SimpleDateFormat("yyyy/MM/dd");
            return df.format(date);
        }else{
            return "";
        }
    }

     public static Date getDateFromString(String stringDate){
        if(stringDate != null && !stringDate.equals("")){
            DateFormat df= new SimpleDateFormat("dd/MM/yyyy");
            Date date = null;
            try {
                date = df.parse(stringDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return date;
        }else{
            return null;
        }
    }

    public static Date getUsDateFormatFromIndianDateFormatString(String indianDateFormat){
        Date indianDate = getDateFromString(indianDateFormat);
        if(indianDate != null){
            DateFormat df= new SimpleDateFormat("yyyy/MM/dd");
            Date date = null;
            try {
                date = df.parse(df.format(indianDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return date;
        }else{
            return null;
        }
    }

    public static String getStringIndianDateFromUsDateFormat(Date usDateFormat1){
        String usDateFormat = getStringDateFromDate(usDateFormat1);
        if(usDateFormat != null && !usDateFormat.equals("")){
            DateFormat df= new SimpleDateFormat("dd/MM/yyyy");
            Date date = null;
            try {
                date = df.parse(usDateFormat);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return df.format(date);
        }else{
            return null;
        }
    }
	
}
