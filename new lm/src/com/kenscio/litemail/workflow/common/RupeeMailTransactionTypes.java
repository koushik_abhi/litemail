package com.kenscio.litemail.workflow.common;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jun 18, 2007
 * Time: 12:52:23 PM
 * To change this template use File | Settings | File Templates.
 */
public interface RupeeMailTransactionTypes {
    public final String REEDEM_STAMP = "REEDEM_STAMP";
    public final String REQUEST_CHQ = "REQUEST_CHEQUE";
    public final String REFERAL_COMMISSION = "REFERAL_COMMISSION";
    public final String DEBIT_WLT = "DEBIT_WALLET";
    public final String CREDIT_WLT = "CREDIT_WALLET";
    public final String CHARITY_DONATION = "CHARITY_DONATION";
}
