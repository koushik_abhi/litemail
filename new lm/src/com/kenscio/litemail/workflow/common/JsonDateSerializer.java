package com.kenscio.litemail.workflow.common;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

public class JsonDateSerializer extends JsonSerializer<Date>
{
	
	//private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	public static String userDefinedTZ = "Asia/Calcutta";
	@Override
	
	public void serialize(Date date, JsonGenerator gen, SerializerProvider provider)
	
	throws IOException, JsonProcessingException {
	
		//System.out.println(date.toGMTString());
		// SimpleDateFormat dateFormat = 
		//	      new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
	
		
		
		//
		SimpleDateFormat dateFormat =  new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
		
		Calendar originalDate = Calendar.getInstance();
		originalDate.setTime(date);
		originalDate.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
		String formattedDate = dateFormat.format(originalDate.getTime());
		//String formattedDate  = dateFormat.format(originalDate.getTime());
		//System.out.println("originalDate "+formattedDate);
		gen.writeString(formattedDate);//(date.toLocaleString());
	
	}
	
}