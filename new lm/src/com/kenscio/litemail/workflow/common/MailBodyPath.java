package com.kenscio.litemail.workflow.common;

/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Jun 25, 2008
 * Time: 12:48:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class MailBodyPath {
    String originalPath;
    String replacedPath;

    public String getOriginalPath() {
        return originalPath;
    }

    public void setOriginalPath(String originalPath) {
        this.originalPath = originalPath;
    }

    public String getReplacedPath() {
        return replacedPath;
    }

    public void setReplacedPath(String replacedPath) {
        this.replacedPath = replacedPath;
    }
}
