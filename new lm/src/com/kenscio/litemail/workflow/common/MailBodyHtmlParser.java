package com.kenscio.litemail.workflow.common;

import org.lobobrowser.html.*;
import org.lobobrowser.html.test.*;
import org.lobobrowser.html.parser.*;
import org.lobobrowser.html.domimpl.*;

import org.w3c.dom.*;
import org.w3c.dom.html2.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.*;
import java.util.Set;
import java.util.TreeSet;
import java.util.Collection;


public class MailBodyHtmlParser {
    private final String stylesheetType = "link";
    private final String stylesheetExt = "css";
    private final String inputTagName = "input";
    private HTMLDocumentImpl document;

    public MailBodyHtmlParser(String htmlFilePath) {
        this.document = parseHtmlFileAndGetHTMLDocument(htmlFilePath);
    }

    public Set getImagePathsFromHtmlFile(){
        Set imageCollections = new TreeSet();
        HTMLCollection imagePaths = document.getImages();
        for (int i = 0; i < imagePaths.getLength(); i++) {
            imageCollections.add(imagePaths.item(i).toString());
        }
        NodeList inputTypeImagePaths =  document.getElementsByTagName(inputTagName);
        if(inputTypeImagePaths.getLength() > 0){
            for (int i = 0; i < inputTypeImagePaths.getLength(); i++) {
                  imageCollections.add(inputTypeImagePaths.item(i).toString());
            }
        }
        return imageCollections;
    }

     public Set getStyleSheetLinksFromHtmlFile(){
         HTMLCollection links = document.getLinks();
         return getStyleSheetLinksFromLinks(links);
     }

    public Collection getStyleSheetsFromHtmlFile() {
         return document.getStyleSheets();
     }

    private HTMLDocumentImpl parseHtmlFileAndGetHTMLDocument(String htmlFile) {
         UserAgentContext uacontext = new SimpleUserAgentContext();
         DocumentBuilderImpl builder = new DocumentBuilderImpl(uacontext);
         HTMLDocumentImpl document = null;
         FileInputStream in = null;
         try {
             in = new  FileInputStream(new File(htmlFile));
             Reader reader = new InputStreamReader(in);
             InputSource inputSource = new InputSource(reader);
             Document d = builder.parse(inputSource);
             document = (HTMLDocumentImpl) d;
         } catch (FileNotFoundException e) {
             e.printStackTrace();
         } catch (IOException e) {
             e.printStackTrace();
         } catch (SAXException e) {
             e.printStackTrace();
         } finally {
             try {
                 if(in!=null)
                 in.close();
             } catch (IOException e) {
                 e.printStackTrace();
             }
         }
         return document;
     }

    private Set getStyleSheetLinksFromLinks(HTMLCollection links) {
        Set styleSheetLinks = new TreeSet();
        for (int i = 0; i < links.getLength(); i++) {
            if(stylesheetType.equalsIgnoreCase(links.item(i).getNodeName())){
                String link = links.item(i).toString();
                int pos = links.item(i).toString().lastIndexOf(".");
                if(-1 != pos && link.substring(pos+1).equalsIgnoreCase(stylesheetExt)){
                    styleSheetLinks.add(link);
                }
            }
        }
        return styleSheetLinks;
    }
}
