package com.kenscio.litemail.workflow.common;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jun 30, 2006
 * Time: 3:26:17 PM
 * To change this template use File | Settings | File Templates.
 */
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Currency;


public class MoneyValue {
	private static final Currency INR = Currency.getInstance("INR");
	private static final int DEFAULT_ROUNDING_MODE = BigDecimal.ROUND_HALF_EVEN;

	private BigDecimal amount;
	private Currency currency;

	public MoneyValue() {
		this.currency = INR;
		this.amount = new BigDecimal(0.00);
	}

	/**
	 * The constructor does not complex computations and requires simple, inputs
	 * consistent with the class invariant. Other creation methods are available
	 * for convenience.
	 */
	public MoneyValue(BigDecimal amount, Currency currency) {
		this.currency = currency;
		this.amount = amount.setScale(INR.getDefaultFractionDigits(), DEFAULT_ROUNDING_MODE);
	}

	/**
	 * The constructor takes only the amount field and default currency
	 * to US $.
	 */
	public MoneyValue(BigDecimal amount) {
		this.currency = INR;
		if (amount != null)
			this.amount = amount.setScale(INR.getDefaultFractionDigits(), DEFAULT_ROUNDING_MODE);
		else
			this.amount = amount;
	}

   public MoneyValue(double amount) {
        this.currency = INR;
		this.amount = new BigDecimal(amount, new MathContext(4));
    }
    /**
	 * This creation method is safe to use. It will adjust scale, but will not
	 * round off the amount.
	 */
	public static MoneyValue valueOf(BigDecimal amount, Currency currency) {
		return MoneyValue.valueOf(amount, currency, DEFAULT_ROUNDING_MODE);
	}

	/**
	 * For convenience, an amount can be rounded to create a Money.
	 */
	public static MoneyValue valueOf(BigDecimal rawAmount, Currency currency, int roundingMode) {
		BigDecimal amount = rawAmount.setScale(currency.getDefaultFractionDigits(), roundingMode);
		return new MoneyValue(amount, currency);
	}

	/**
	 * WARNING: Because of the indefinite precision of double, this method must
	 * round off the value.
	 */
	public static MoneyValue valueOf(double dblAmount, Currency currency) {
		return MoneyValue.valueOf(dblAmount, currency, DEFAULT_ROUNDING_MODE);
	}

	/**
	 * Because of the indefinite precision of double, this method must round off
	 * the value. This method gives the client control of the rounding mode.
	 */
	public static MoneyValue valueOf(double dblAmount, Currency currency, int roundingMode) {
		BigDecimal rawAmount = new BigDecimal(dblAmount);
		return MoneyValue.valueOf(rawAmount, currency, roundingMode);
	}

	/**
	 * WARNING: Because of the indefinite precision of double, thismethod must
	 * round off the value.
	 */
	public static MoneyValue dollars(double amount) {
		return MoneyValue.valueOf(amount, INR);
	}

	/**
	 * This creation method is safe to use. It will adjust scale, but will not
	 * round off the amount.
	 */
	public static MoneyValue dollars(BigDecimal amount) {
		return MoneyValue.valueOf(amount, INR);
	}

	boolean isSameCurrencyAs(MoneyValue arg) {
		return currency.equals(arg.currency);
	}

	public MoneyValue negated() {
		return MoneyValue.valueOf(amount.negate(), currency);
	}

	public MoneyValue abs() {
		return MoneyValue.valueOf(amount.abs(), currency);
	}

	public boolean isNegative() {
		return amount.compareTo(new BigDecimal(0)) < 0;
	}

	public boolean isPositive() {
		return amount.compareTo(new BigDecimal(0)) > 0;
	}

	public boolean isZero() {
		return this.equals(MoneyValue.valueOf(0.0, currency));
	}

	public MoneyValue plus(MoneyValue other) {
		if (!isSameCurrencyAs(other)) throw new IllegalArgumentException("Addition is not defined between different currencies");
		return MoneyValue.valueOf(amount.add(other.amount), currency);
	}

	public MoneyValue minus(MoneyValue other) {
		return this.plus(other.negated());
	}

	public MoneyValue dividedBy(BigDecimal divisor, int roundingMode) {
		BigDecimal newAmount = amount.divide(divisor, roundingMode);
		return MoneyValue.valueOf(newAmount, currency);
	}

	public MoneyValue dividedBy(double divisor) {
		return dividedBy(divisor, DEFAULT_ROUNDING_MODE);
	}

	public MoneyValue dividedBy(double divisor, int roundingMode) {
		return dividedBy(new BigDecimal(divisor), roundingMode);
	}

	public MoneyValue times(BigDecimal factor) {
		return times(factor, DEFAULT_ROUNDING_MODE);
	}

	public MoneyValue times(BigDecimal factor, int roundingMode) {
		return MoneyValue.valueOf(amount.multiply(factor), currency, roundingMode);
	}

	public MoneyValue times(double amount, int roundingMode) {
		return times(new BigDecimal(amount), roundingMode);
	}

	public MoneyValue times(double amount) {
		return times(new BigDecimal(amount));
	}

	public MoneyValue times(int i) {
		return times(new BigDecimal(i));
	}

	public int compareTo(Object other) {
		return compareTo((MoneyValue) other);
	}

	public int compareTo(MoneyValue other) {
		if (!isSameCurrencyAs(other)) throw new IllegalArgumentException("Compare is not defined between different currencies");
		return amount.compareTo(other.amount);
	}


    public boolean isGreaterThan(MoneyValue other) {
		return (compareTo(other) > 0);
	}

    public boolean isGreaterThanOrEqual(MoneyValue other) {
        int compareValue = compareTo(other);
        return (compareValue >= 0);
	}

    public boolean isLessThan(MoneyValue other) {
		return (compareTo(other) < 0);
	}

   public boolean isLessThanOrEqual(MoneyValue other) {
		return (compareTo(other) <= 0);
	}
    public boolean equals(Object other) {
		//revisit: maybe use: Reflection.equalsOverClassAndNull(this, other)
		return
		        ((other != null) && MoneyValue.class.isAssignableFrom(other.getClass())) &&
		        this.equals((MoneyValue) other);
	}

	public boolean equals(MoneyValue other) {
		return
		        currency.equals(other.currency) &&
		        amount.equals(other.amount);
	}

	public String toString() {
		//return currency.toString() + " " + amount;
		return  "" + amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public Currency getCurrency() {
		return currency;
	}
}


