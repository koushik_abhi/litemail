package com.kenscio.litemail.workflow.dao;

import com.kenscio.litemail.exception.NoAccountFoundException;
import com.kenscio.litemail.workflow.common.BaseRepository;
import com.kenscio.litemail.workflow.domain.SmtpEntity;

import java.util.List;


import org.hibernate.criterion.Expression;
import org.hibernate.criterion.DetachedCriteria;


public class SmtpDao extends BaseRepository {
   
    
    public void saveSmtpDetail(SmtpEntity smtpdetail) {
        save(smtpdetail);
    }
    
    public List<SmtpEntity> getAllSmtpInPagination(int pagenumber,
			int noOfRecords) {
    	DetachedCriteria criteria = DetachedCriteria.forClass(SmtpEntity.class);
		int index = (pagenumber - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria, index, noOfRecords); 
	}
    
    public void updateSmtpDetail(SmtpEntity smtpdetail) {
        merge(smtpdetail);
    }
    public void deleteSmtpDetail(String smtpServerName) {
    	SmtpEntity pe = getSmtpDetail(smtpServerName);
    	delete(pe);
    }
    public SmtpEntity[] getAllSmtpDetail(){
        List smtpacs = getHibernateTemplate().find("from SmtpEntity smtpdetail "+
                                                                   " order by smtpdetail.smtpServerName asc");
        return(SmtpEntity[])smtpacs.toArray(new SmtpEntity[smtpacs.size()]);
    }
    public SmtpEntity[] getAllSmtpDetail(String partner){
        List smtpacs = getHibernateTemplate().find("from SmtpEntity smtpdetail where  partner= '"+partner+"'"+
                                                                   " order by smtpdetail.smtpServerName asc");
        return(SmtpEntity[])smtpacs.toArray(new SmtpEntity[smtpacs.size()]);
    }

    
    public SmtpEntity getSmtpDetail(String smtpServerName) throws NoAccountFoundException {
    	SmtpEntity account = null;
        
        DetachedCriteria criteria = DetachedCriteria.forClass(SmtpEntity.class);
        criteria.add(Expression.eq("smtpServerName",smtpServerName));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        if(accountList != null && accountList.size() > 0)
        account = (SmtpEntity) accountList.get(0);
       
        return account;
    }
    public List<String> getAllSmtpNames(String partner){
        List smtpacs = getHibernateTemplate().find("select smtpServerName from SmtpEntity smtpdetail where  partner= '"+partner+"'"+
                                                                   " order by smtpdetail.smtpServerName asc");
        return smtpacs;
    }

	public SmtpEntity getSmtpDetailById(Long smtpServerId) {
        SmtpEntity account = null;
        
        DetachedCriteria criteria = DetachedCriteria.forClass(SmtpEntity.class);
        criteria.add(Expression.eq("smtpServerId",smtpServerId));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        if(accountList != null && accountList.size() > 0)
        account = (SmtpEntity) accountList.get(0);
       
        return account;
	}
      
	   

}
