package com.kenscio.litemail.workflow.dao;


import com.kenscio.litemail.workflow.common.BaseRepository;

import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.domain.PartnerEntity;

import java.util.List;



import org.hibernate.criterion.Expression;
import org.hibernate.criterion.DetachedCriteria;


public class PartnerDao extends BaseRepository {
   
    
    public void savePartnerDetail(PartnerEntity partner) {
        save(partner);
    }
    
    public void updatePartnerDetail(PartnerEntity partner) {
        merge(partner);
    }
    
    public List<PartnerEntity> getAllPartnersInPagination(int pagenumber,
			int noOfRecords) {
    	DetachedCriteria criteria = DetachedCriteria.forClass(PartnerEntity.class);
		int index = (pagenumber - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria, index, noOfRecords); 
	}
    
    public void deletePartnerDetail(String partner) {
    	PartnerEntity pe = getPartnerDetail(partner);
    	delete(pe);
    }
    
    
    public PartnerEntity[] getAllPartners(){
        List smtpacs = getHibernateTemplate().find("from PartnerEntity partner "+
                                                                   "order by partner.name asc");
        return(PartnerEntity[])smtpacs.toArray(new PartnerEntity[smtpacs.size()]);
    }

    
    public PartnerEntity getPartnerDetail(String name) {
    	PartnerEntity account = null;
        
        DetachedCriteria criteria = DetachedCriteria.forClass(PartnerEntity.class);
        criteria.add(Expression.eq("name",name));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        if(accountList != null && accountList.size() > 0)
        account = (PartnerEntity) accountList.get(0);
       
        return account;
    }
    
    public int getTotalPagesOfList(long accountId) {
		String hql = "select count(pid) from PartnerEntity ";  
		List list=getHibernateTemplate().find(hql);
		if(list != null && list.size() > 0)
		{
			return (Integer)list.get(0);
		}
		
		return 0;
		
	}
   
      
	   

}
