package com.kenscio.litemail.workflow.dao;


import com.kenscio.litemail.workflow.common.BaseRepository;

import com.kenscio.litemail.workflow.domain.ActionLogObject;
import com.kenscio.litemail.workflow.domain.EmailObject;
import com.kenscio.litemail.workflow.domain.SmtpEntity;


import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;



public class EmailDaoImpl extends BaseRepository {
   
    public EmailObject[] getEmailConfig() {
    	List emailConfigs = getHibernateTemplate().find("from EmailObject stampitAccount "+
                  "order by stampitAccount.id desc limit 1");
    	return(EmailObject[])emailConfigs.toArray(new EmailObject[emailConfigs.size()]);
    }

    public void saveEmail(EmailObject action) {
        
        save(action);
    }
    
    public void updateEmail(EmailObject action) {
        
        merge(action);
    }
    
    public void deleteEmail(long id) {
        
    	EmailObject[] ae= getEmailConfig();
    	if(ae != null && ae.length > 0)
    	delete(ae[0]);
    }
         
    public EmailObject[] getAllEmailConfig(){
        List stampitAccounts = getHibernateTemplate().find("from EmailObject stampitAccount "+
                                                                   "order by stampitAccount.id desc");
        return(EmailObject[])stampitAccounts.toArray(new EmailObject[stampitAccounts.size()]);
    }

	public EmailObject getEmailConfigByUser(String userId) {
		
		EmailObject emailObject = null;
        
        DetachedCriteria criteria = DetachedCriteria.forClass(EmailObject.class);
        criteria.add(Expression.eq("userid",userId));
        List emailObjectList = getHibernateTemplate().findByCriteria(criteria);
        if(emailObjectList != null && emailObjectList.size() > 0)
        	emailObject = (EmailObject) emailObjectList.get(0);
       
        return emailObject;
	}

  
	   

}
