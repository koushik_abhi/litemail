package com.kenscio.litemail.workflow.dao;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;

import com.kenscio.litemail.workflow.domain.AttributeEntity;
import com.kenscio.litemail.workflow.domain.AttributeMap;
import com.kenscio.litemail.workflow.domain.ListEntity;
import com.kenscio.litemail.workflow.common.BaseRepository;

public class AttributeDao extends BaseRepository{

	
	public void addAttribute(AttributeEntity attributeEntity) {

		save(attributeEntity);
	}
		
	public List<List<AttributeEntity>> getAllAttributes(long accountId) {

		List<List<AttributeEntity>> customStdList= new ArrayList<List<AttributeEntity>>();
		
		DetachedCriteria criteria = DetachedCriteria.forClass(AttributeEntity.class);
		criteria.add(Expression.eq("accountId", accountId));
		System.out.println("criterla all atributes "+criteria);
		List customAtt =  getHibernateTemplate().findByCriteria(criteria);
		
		DetachedCriteria criteria1 = DetachedCriteria.forClass(AttributeEntity.class);
		criteria1.add(Expression.isNull("accountId"));
		List stdAtt =  getHibernateTemplate().findByCriteria(criteria1);
	   
		customStdList.add(customAtt);
		customStdList.add(stdAtt);
	    return customStdList;
	}
	
	@SuppressWarnings("unchecked")
	public List<AttributeEntity> getAllUserAttributes(Long accountId)
	{
		List<AttributeEntity> attributesList = new ArrayList<AttributeEntity>();
		DetachedCriteria criteria = DetachedCriteria.forClass(AttributeEntity.class);
		List coll = new ArrayList();
		coll.add(accountId);
		coll.add(null);
        criteria.add(Expression.in("accountId", coll));
        attributesList =  getHibernateTemplate().findByCriteria(criteria);
        return attributesList;
	}
	
	public AttributeEntity getAttribute(long attrId) {
		DetachedCriteria criteria = DetachedCriteria.forClass(AttributeEntity.class);
		criteria.add(Expression.eq("attrId", attrId));
		List<AttributeEntity> customAtt =  getHibernateTemplate().findByCriteria(criteria);
		if(customAtt != null && customAtt.size() > 0)
			return customAtt.get(0);
					
		return null;
	}
	
	public void deleteAttribute(long attrId)
	{
		AttributeEntity ae = getAttribute(attrId);
		if(ae != null)
		delete(ae);
		
	}


	public List<AttributeEntity> getCustomAttributes(long accountId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(AttributeEntity.class);
		criteria.add(Expression.eq("accountId", accountId));
		List customAtt =  getHibernateTemplate().findByCriteria(criteria);
		return customAtt;
	}
	
	public List<AttributeEntity> getStdAttributes() {
		DetachedCriteria criteria1 = DetachedCriteria.forClass(AttributeEntity.class);
		criteria1.add(Expression.isNull("accountId"));
		System.out.println("criteria for std atrib"+criteria1);
		//criteria1.add(Expression.eq("accountId",0l));
		List stdAtt =  getHibernateTemplate().findByCriteria(criteria1);
	   
		return stdAtt;
	}
  public List<AttributeMap> getCustAttributes(Long listId1){
	  System.out.println("In Dao Class ");
		//String sql="select attribute_name from litemail_listattribute_map where list_id='"+listId1+"'";
		DetachedCriteria criteria1 = DetachedCriteria.forClass(AttributeMap.class);
		//criteria1.add(Expression.eq("accountId",accountId));
		System.out.println("list id in dao"+listId1);
		criteria1.add(Expression.eq("listId",listId1));
		System.out.println("dfd"+criteria1);
		@SuppressWarnings("unchecked")
		List<AttributeMap> custAtt =  getHibernateTemplate().findByCriteria(criteria1);
		System.out.println("size of custom attrib "+custAtt.size());
		
		return custAtt;
  }
	public boolean isAttributeAlreadyExist(String attrName, Long accountId) {
		DetachedCriteria criteria1 = DetachedCriteria.forClass(AttributeEntity.class);
		criteria1.add(Expression.isNull("accountId"));
		criteria1.add(Expression.eq("attrName",attrName));
		List stdAtt =  getHibernateTemplate().findByCriteria(criteria1);
	    if(stdAtt != null && stdAtt.size() > 0)
	    {
	    	return true;
	    }
	    DetachedCriteria criteria = DetachedCriteria.forClass(AttributeEntity.class);
		criteria.add(Expression.eq("accountId", accountId));
		criteria.add(Expression.eq("attrName",attrName));
		List cuAtt =  getHibernateTemplate().findByCriteria(criteria);
		
	    if(cuAtt != null && cuAtt.size() > 0)
	    {
	    	return true;
	    }
	    
	    return false;
	}
	
	public List<AttributeEntity> getAttributesByPartnerName(
			String partnerName, int pageNumber, int noOfRecordsperPage) {		
		DetachedCriteria criteria = DetachedCriteria.forClass(AttributeEntity.class);
    	criteria.add(Expression.eq("partnerName", partnerName));
    	int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria, index, noOfRecordsperPage); 
	}

	public int  getTotalCustAttributes(String partnerName) {
				  String hql = "select count(attrId) from AttributeEntity where partnerName ='"+partnerName+"'";  
			  List<?> list=getHibernateTemplate().find(hql);
			  if(list != null && list.size() > 0)
			  {
			   return (Integer)list.get(0);
			  }
			  return 0;
			 }
	}
	

