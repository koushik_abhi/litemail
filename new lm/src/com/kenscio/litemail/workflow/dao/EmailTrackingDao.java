package com.kenscio.litemail.workflow.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.hibernate.Criteria;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;


import au.com.bytecode.opencsv.CSVWriter;

import com.kenscio.litemail.workflow.domain.CampaignMailFail;
import com.kenscio.litemail.workflow.domain.CampaignReport;
import com.kenscio.litemail.workflow.domain.MessageBlockList;
import com.kenscio.litemail.workflow.domain.MessageBounce;
import com.kenscio.litemail.workflow.domain.MessageLinkClick;
import com.kenscio.litemail.workflow.domain.MessageOpen;
import com.kenscio.litemail.workflow.domain.MessageSpam;
import com.kenscio.litemail.workflow.domain.MessageUnsubscribe;
import com.kenscio.litemail.workflow.domain.SendOutMessage;
import com.kenscio.litemail.workflow.common.BaseRepository;

public class EmailTrackingDao extends BaseRepository{
	
	public void addEmailOpenStatus(MessageOpen messageOpen)
	{
		
		saveOnly(messageOpen);
		
	}
	
	public void addEmalUnsubscribeStatus(MessageUnsubscribe unsub)
	{
		saveOnly(unsub);
		
	}
		
	public void addEmailBounceStatusUpdate(MessageBounce bounce, long campaignId)
	{
		
		List bounces = getMailExistInBounce(bounce.getReciepientEmail(),campaignId);
		if(bounces != null && bounces.size() >0 )
		{
			MessageBounce boun = (MessageBounce)bounces.get(0);
			Date time = bounce.getCreatedDate();
			Calendar dbtime = new GregorianCalendar();
			dbtime.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
			dbtime.setTime( boun.getCreatedDate());
			Date dbrectime =  dbtime.getTime();
			if(dbrectime.compareTo(time) <= -1)
			{
				boun.setReasonForBounce(bounce.getReasonForBounce());
				boun.setBounceCode(bounce.getBounceCode());
				boun.setOtherReason(bounce.getOtherReason());
				boun.setCreatedDate(bounce.getCreatedDate());
				boun.setCount(boun.getCount()+1);
				boun.setCampaignId(bounce.getCampaignId());
				boun.setHardBounce(bounce.getHardBounce());
				boun.setInboxFull(bounce.getInboxFull());
				boun.setOutOfOffice(bounce.getOutOfOffice());
				boun.setSoftBounce(bounce.getSoftBounce());
				merge(boun);
			}
					
						
		}else
		{
			saveOnly(bounce);
						
		}
				
		
	}
	
	public void addEmailSpamStatusUpdate(MessageSpam spam, long campaignId)
	{
		
		List spams = getMailExistInSpams(spam.getReciepientEmail(),campaignId);
		if(spams != null && spams.size() > 0 )
		{
			Date time = spam.getCreatedDate();
			MessageSpam boun = (MessageSpam)spams.get(0);
			Calendar dbtime = new GregorianCalendar();
			dbtime.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
			dbtime.setTime( boun.getCreatedDate());
			Date dbrectime =  dbtime.getTime();
			if(dbrectime.compareTo(time) <= -1)
			{
				boun.setOtherReason(spam.getOtherReason());
				boun.setSpamCode(spam.getSpamCode());
				boun.setCreatedDate(spam.getCreatedDate());
				boun.setCount(boun.getCount()+1);
				boun.setCampaignId(spam.getCampaignId());
				merge(boun);
			}								
						
		}else
		{
			saveOnly(spam);
	
		}
				
		
	}
	
	public static String userDefinedTZ = "Asia/Calcutta";
	public void addEmailSpamStatusOld(MessageSpam spam)
	{		
		List spams = getCheckIfMailExistInSpam(spam.getReciepientEmail());
		if(spams != null && spams.size() >=4 )
		{
			delete((MessageSpam)spams.get(0));
			saveOnly(spam);
			
		}else
		{
			Date time = spam.getCreatedDate();
			for(Object sp : spams)
			{
				MessageSpam ms = (MessageSpam)sp;
				Calendar dbtime = new GregorianCalendar();
				dbtime.setTimeZone(TimeZone.getTimeZone(userDefinedTZ));
				dbtime.setTime( ms.getCreatedDate());
				Date dbrectime =  dbtime.getTime();
				if(dbrectime.compareTo(time) <= -1)
				{
					saveOnly(spam);
					break;
				}
			}
			
		}
	}
	
	

	public void addEmailSendOutStatus(SendOutMessage sendout)
	{
		saveOnly(sendout);
	}
		
	public void addEmailMessageLinkClickStatus(MessageLinkClick link)
	{
		saveOnly(link);
	}

	public CampaignReport getCampaignMailOpenReport(Long campaignId,
			Long accountId) {
		 
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageOpen.class);
	     criteria.add(Expression.eq("campaignId",campaignId));
	     List messageOpenList = getHibernateTemplate().findByCriteria(criteria);
		 CampaignReport report = new CampaignReport();
	     report.setCampaignId(campaignId);
	     report.setMessageOpenList(messageOpenList);
	     return report;
	}

	public CampaignReport getCampaignMailSoftBounceReport(Long campaignId,
			Long accountId) {
		 
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageBounce.class);
		 criteria.add(Expression.isNotNull("softBounce"));
	     criteria.add(Expression.eq("campaignId",campaignId));
	     List softBounceList = getHibernateTemplate().findByCriteria(criteria);
	     		
	     CampaignReport report = new CampaignReport();
	     report.setCampaignId(campaignId);
	     report.setSoftBounceList(softBounceList);
	     return report;
	     
	}

	public CampaignReport getCampaignMailUnsubscribeReport(Long campaignId,
			Long accountId) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageUnsubscribe.class);
	     criteria.add(Expression.eq("campaignId",campaignId));
	     List unsubscribeList = getHibernateTemplate().findByCriteria(criteria);
		 
	     CampaignReport report = new CampaignReport();
	     report.setCampaignId(campaignId);
	     report.setUnsubscribeObjectList(unsubscribeList);
	     return report;
	}

	public CampaignReport getCampaignMailBlockListReport(Long campaignId,
			Long accountId) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageBlockList.class);
	     criteria.add(Expression.eq("campaignId",campaignId));
	     List messageBlockList = getHibernateTemplate().findByCriteria(criteria);
	     	 
	     CampaignReport report = new CampaignReport();
	     report.setCampaignId(campaignId);
	     report.setMessageBlockList(messageBlockList);
	     return report;
	}

	public CampaignReport getCampaignMailBounceReport(Long campaignId,
			Long accountId) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageBounce.class);
	     criteria.add(Expression.eq("campaignId",campaignId));
	     List bounceList = getHibernateTemplate().findByCriteria(criteria);
	     		 
	     CampaignReport report = new CampaignReport();
	     report.setCampaignId(campaignId);
	     report.setMessageBounceList(bounceList);
	     return report;
	}

	public CampaignReport getCampaignMailClickReport(Long campaignId,
			Long accountId) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageLinkClick.class);
	     criteria.add(Expression.eq("campaignId",campaignId));
	     List messageLinkClickList = getHibernateTemplate().findByCriteria(criteria);
	     
		 
	     CampaignReport report = new CampaignReport();
	     report.setCampaignId(campaignId);
	     report.setMessageLinkClickList(messageLinkClickList);
	     return report;
	}

	public CampaignReport getCampaignMailHardBounceReport(Long campaignId,
			Long accountId) {
		
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageBounce.class);
		 criteria.add(Expression.isNotNull("hardBounce"));
	     criteria.add(Expression.eq("campaignId",campaignId));
	     List hardBounceList = getHibernateTemplate().findByCriteria(criteria);
	     
	     CampaignReport report = new CampaignReport();
	     report.setCampaignId(campaignId);
	     report.setHardBounceList(hardBounceList);
	     return report;
	}
	
	public long getCampaignGeneralBounceCount(Long campaignId, Long accountId) {
		long count = 0;
		String hsql  = "select count(distinct reciepientEmail) from MessageBounce where ( otherReason is not null or otherReason='') and campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
	}
	
	public CampaignReport getCampaignMailGeneralBounceReport(Long campaignId,
			Long accountId) {
		
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageBounce.class);
		 criteria.add(Expression.isNotNull("otherReason"));
	     criteria.add(Expression.eq("campaignId",campaignId));
	     List hardBounceList = getHibernateTemplate().findByCriteria(criteria);
	     
	     CampaignReport report = new CampaignReport();
	     report.setCampaignId(campaignId);
	     report.setHardBounceList(hardBounceList);
	     return report;
	}
	
	
	public CampaignReport getCampaignMailTotalBounceReport(Long campaignId,
			Long accountId) {
		
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageBounce.class);
		 criteria.add(Expression.eq("campaignId",campaignId));
	     List totalBounces = getHibernateTemplate().findByCriteria(criteria);
	     
	     CampaignReport report = new CampaignReport();
	     report.setCampaignId(campaignId);
	     report.setHardBounceList(totalBounces);
	     return report;
	}
	
	public List getCheckIfMailExistInSpam(String reciepintMailId) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageSpam.class);
	     criteria.add(Expression.eq("reciepientEmail",reciepintMailId));
	     criteria.addOrder(Order.asc("createdDate"));
	     List messageSpamList = getHibernateTemplate().findByCriteria(criteria);
	     return messageSpamList;
	    
	}
	
	public List getMailExistInBounce(String reciepintMailId,long campaignId) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageBounce.class);
	     criteria.add(Expression.eq("reciepientEmail",reciepintMailId));
	     criteria.add(Expression.eq("campaignId",campaignId));
	     return getHibernateTemplate().findByCriteria(criteria);
	     
	}
	
	public List getMailExistInSpams(String reciepintMailId, long campaignId) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageSpam.class);
	     criteria.add(Expression.eq("reciepientEmail",reciepintMailId));
	     criteria.add(Expression.eq("campaignId",campaignId));
	     return getHibernateTemplate().findByCriteria(criteria);
	     
	}
	
	public List getCheckIfMailExistInBounce(String reciepintMailId) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageBounce.class);
	     criteria.add(Expression.eq("reciepientEmail",reciepintMailId));
	     criteria.addOrder(Order.asc("createdDate"));
	     return getHibernateTemplate().findByCriteria(criteria);
	     
	}

	public CampaignReport getCampaignMailSpamReport(Long campaignId,
			Long accountId) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageSpam.class);
	     criteria.add(Expression.eq("campaignId",campaignId));
	     List messageSpamList = getHibernateTemplate().findByCriteria(criteria);
	     		 
	     CampaignReport report = new CampaignReport();
	     report.setCampaignId(campaignId);
	     report.setMessageSpamList(messageSpamList);
	     return report;
	}
	

	public CampaignReport getCampaignMailUniqueClickReport(Long campaignId,
			Long accountId) {
		 
		List<MessageLinkClick> returnValues = new ArrayList<MessageLinkClick>();
		String hsql  = "select distinct reciepientEmail, reciepientName from MessageLinkClick where campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		Iterator it = results.iterator();
		
        while(it.hasNext())
        {
            Object vals[] = (Object[])it.next();
            MessageLinkClick link = new MessageLinkClick();
	    	link.setReciepientEmail((String)vals[0]);
	    	link.setReciepientName((String)vals[1]);
	    	returnValues.add(link);
	        System.out.println(link.getLinkClickId()+"-------"+link.getReciepientEmail()+"-----"+link.getReciepientName());
           
           
        }     
	
	    CampaignReport report = new CampaignReport();
	    report.setCampaignId(campaignId);
	    report.setMessageLinkUniqueClickList(returnValues);
	    return report;			
		 
	}
	
	public long getCampaignMailUniqueClickCount(Long campaignId,
			Long accountId) {
		 
		long count = 0;
		String hsql  = "select count(distinct reciepientEmail) from MessageLinkClick where campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
       		
		 
	}
	
	public long getCampaignTotalBounceCount(Long campaignId,
			Long accountId) {
		 
		long count = 0;
		String hsql  = "select count(reciepientEmail) from MessageBounce where campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
       		
		 
	}
	
	
	public long getCampaignMailClickCount(Long campaignId,
			Long accountId) {
		 
		long count = 0;
		String hsql  = "select count(reciepientEmail) from MessageLinkClick where campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
       		
		 
	}
	
	public long getCampaignMailOpenCount(Long campaignId,
			Long accountId) {
		 
		long count = 0;
		String hsql  = "select count(reciepientEmail) from MessageOpen where campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
       		
		 
	}
	
	public long getCampaignMailUnsubsribeCount(Long campaignId,
			Long accountId) {
		 
		long count = 0;
		String hsql  = "select count(reciepientEmail) from MessageUnsubscribe where campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
       		
		 
	}
	
	public long getCampaignMailOpenIUniqueCount(Long campaignId,
			Long accountId) {
		 
		long count = 0;
		String hsql  = "select count(distinct reciepientEmail) from MessageOpen where campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
       		
		 
	}


	public CampaignReport getCampaignMailUniqueOpenReport(Long campaignId,
			Long accountId) {
		 
		List<MessageOpen> returnValues = new ArrayList<MessageOpen>();
		String hsql  = "select distinct reciepientEmail, reciepientName from MessageOpen where campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		Iterator it = results.iterator();
		
        while(it.hasNext())
        {
            Object vals[] = (Object[])it.next();
            MessageOpen link = new MessageOpen();
	    	link.setReciepientEmail((String)vals[0]);
	    	link.setReciepientName((String)vals[1]);
	    	returnValues.add(link);
	        System.out.println("-------"+link.getReciepientEmail()+"-----"+link.getReciepientName());
           
           
        }     
	
	    CampaignReport report = new CampaignReport();
	    report.setCampaignId(campaignId);
	    report.setMessageUniqueOpenList(returnValues);
	    return report;			
		 
		 
	}

	public long getHardBounceCount(Long campaignId, Long accountId) {
		long count = 0;
		String hsql  = "select count(distinct reciepientEmail) from MessageBounce where hardBounce is not null and campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
	}
	
	public long getMaxBounceRecId() {
		long count = 0;
		String hsql  = "select max(bounceId) from MessageBounce ";
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
	}
	
	public long getMaxSpamRecId() {
		long count = 0;
		String hsql  = "select max(spamId) from MessageSpam ";
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
	}
	
	public long getAllBounceCount() {
		long count = 0;
		String hsql  = "select count(id) from MessageBounce ";
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
	}
	
	public long getAllSpamCount() {
		long count = 0;
		String hsql  = "select count(id) from MessageSpam ";
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
	}

	public long getSoftBounceCount(Long campaignId, Long accountId) {
		long count = 0;
		String hsql  = "select count(distinct reciepientEmail) from MessageBounce where softBounce is not null and campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
	}

	public long getSpamComplaintsCount(Long campaignId, Long accountId) {
		long count = 0;
		String hsql  = "select count(distinct reciepientEmail) from MessageSpam where campaignId = "+ campaignId;
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
	}
	
	public List<CampaignMailFail> getMailFailDataList(Long campaignID) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(CampaignMailFail.class);
	     criteria.add(Expression.eq("campaignId",campaignID));
	     List<CampaignMailFail> mailFailList = getHibernateTemplate().findByCriteria(criteria);
		return mailFailList;
	}

	public List<CampaignMailFail> getCampaignMailFailFilterFromTodate(
			Long campaignID, Date from_Date, Date to_Date) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(CampaignMailFail.class);
	     criteria.add(Expression.eq("campaignId",campaignID));
	     criteria.add(Expression.between("createdDate" ,from_Date, to_Date));
	     List<CampaignMailFail> mailFailList = getHibernateTemplate().findByCriteria(criteria);
		return mailFailList;
	}
	
	public void exportToExcelMailFailed(CSVWriter writer, Long campaignID,
			long accountId) {
		 Criteria criteria = getSession().createCriteria(CampaignMailFail.class);
		 criteria.add(Expression.eq("campaignId",campaignID));
		 ScrollableResults results = criteria.scroll();
		 List<String> data = new ArrayList<String>();
		 String headers ="Email";
		 data.add(headers);
		 while(results.next())
		 {
			 Object[] objArr = results.get();
			 CampaignMailFail open =  (CampaignMailFail)objArr[0];
			 data.add(open.getEmail()+"");
			 String [] harray = data.toArray(new String[data.size()]);
		     writer.writeNext(harray);
			 
		 }
		 results.close();
		
		
	}

	public void addBulkBounces(List recs) {
		 getHibernateTemplate().saveOrUpdateAll(recs);
		
	}
	
	public void addBulkSpams(List recs) {
		 getHibernateTemplate().saveOrUpdateAll(recs);
		
	}


	
	

}
