package com.kenscio.litemail.workflow.dao;

import au.com.bytecode.opencsv.CSVWriter;

import com.kenscio.litemail.workflow.domain.CampaignBounce;
import com.kenscio.litemail.workflow.domain.CampaignMailSentSoFar;
import com.kenscio.litemail.workflow.domain.MessageSpam;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.CampaignLinks;
import com.kenscio.litemail.workflow.domain.CampaignMailFail;
import com.kenscio.litemail.workflow.domain.CampaignStop;
import com.kenscio.litemail.workflow.domain.MessageBounce;
import com.kenscio.litemail.workflow.domain.MessageLinkClick;
import com.kenscio.litemail.workflow.domain.MessageOpen;
import com.kenscio.litemail.workflow.domain.MessageUnsubscribe;
import com.kenscio.litemail.workflow.service.CampaignReportService;
import com.kenscio.litemail.workflow.service.CampaignService;
import com.kenscio.litemail.workflow.common.BaseRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.hibernate.*;
import org.hibernate.criterion.*;
import org.springframework.dao.DataAccessException;


public class CampaignDao extends BaseRepository {

    public void saveCampaign(Campaign campaign) {
        //campaign.setCampaignMessage(Translate.decode(campaign.getCampaignMessage())); // to avoid NCR
        if(campaign.getCampaignMessage() != null && !campaign.getCampaignMessage().equals(""))
            campaign.setCampaignMessage(campaign.getCampaignMessage().replaceAll("&amp;","&"));
        save(campaign);
      //  saveShowcaseCampaign(campaign);
    }

    public List<Campaign> getAllCampaignInPagination(long accountId, int pagenumber,
			int noOfRecords) {
    	
    	DetachedCriteria criteria = DetachedCriteria.forClass(Campaign.class);
    	criteria.add(Expression.eq("accountId", accountId));
        criteria.add(Expression.eq("deleteCampaignFlag", false));
        criteria.addOrder(Order.desc("campaignId"));
		int index = (pagenumber - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria, index, noOfRecords); 
	
	}
    public void saveCampignMailSent(CampaignMailSentSoFar sent) {
    	saveOnly(sent);
		
	}
    
    public void saveCampignFail(CampaignMailFail fail) {
    	saveOnly(fail);
		
	}
    private void saveShowcaseCampaign(Campaign campaign) { // update showcase Campaign attached to this campaign
        if(campaign.getShowcaseCampaignId()!=null){
            Campaign showcaseCampaign = getCampaign(campaign.getShowcaseCampaignId());
            showcaseCampaign.setCampaignMessage(campaign.getCampaignMessage());
            showcaseCampaign.setStatus(campaign.getStatus());
            showcaseCampaign.setCampaignStartDateTime(campaign.getCampaignStartDateTime());
            showcaseCampaign.setCampaignEndDateTime(campaign.getCampaignEndDateTime());
            if(campaign.getDeleteCampaignFlag())   // if campaign deleted then make ShowcaseActiveFlag false
                showcaseCampaign.setShowcaseActiveFlag(false);
            showcaseCampaign.setUpdatedDate(new Date());
            save(showcaseCampaign);
        }
    }
    public void addStopCampaignRecord(CampaignStop stop) {
    	saveOnly(stop);
	}


    public void updateCampaign(Campaign campaign) {
        update(campaign);
       // saveShowcaseCampaign(campaign);
    }
    
    public Campaign getCampaign(Long campaignId) {
        return (Campaign) get(Campaign.class,campaignId);
    }
	public Campaign getCampaign(Long accountId, String campaignName) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Campaign.class)
                .add(Expression.eq("accountId", accountId))
                .add(Expression.eq("campaignName", campaignName));
        List<?> campaignList = getHibernateTemplate().findByCriteria(criteria);
        if(campaignList != null && campaignList.size() > 0)
        {
        	return (Campaign)campaignList.get(0);
        }
        return null;
	}
	
	public boolean isSmtpUsed(String servername) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Campaign.class)
				.add(Expression.eq("smtpServerName", servername));
		List<?> campaignList = getHibernateTemplate().findByCriteria(criteria);
		if(campaignList != null && campaignList.size() > 0)
		{
			return true;
		}
		return false;
	}
	
	public Campaign getCampaign(String campaignName) {
		DetachedCriteria criteria = DetachedCriteria.forClass(Campaign.class)
							.add(Expression.eq("campaignName", campaignName));
        List<?> campaignList = getHibernateTemplate().findByCriteria(criteria);
        if(campaignList != null && campaignList.size() > 0)
        {
        	return (Campaign)campaignList.get(0);
        }
        return null;
	}
	
	public CampaignStop getCampaignStopRecord(Long accountId, Long campaignId) {
		DetachedCriteria criteria = DetachedCriteria.forClass(CampaignStop.class)
                .add(Expression.eq("accountId", accountId))
                .add(Expression.eq("campaignId", campaignId))
                .addOrder(Order.desc("createdDate"));
        List campaignList = getHibernateTemplate().findByCriteria(criteria);
        if(campaignList != null && campaignList.size() > 0)
        {
        	return (CampaignStop)campaignList.get(0);
        }
        return null;
	}
	


    public List<?> getCampaignsByAccountId(Long accountId) {
        DetachedCriteria criteria = DetachedCriteria.forClass(Campaign.class)
                .add(Expression.eq("accountId", accountId))
                .addOrder(Order.desc("campaignId"))
                .add(Expression.eq("deleteCampaignFlag", false));
        List<?> campaignList = getHibernateTemplate().findByCriteria(criteria);
        return campaignList;
    }

    public Campaign getCampaignByFromEmailIdAndAccountId(String emailId, Long accountId) {
        DetachedCriteria criteria = DetachedCriteria.forClass(Campaign.class)
                .add(Expression.eq("accountId", accountId))
                .add(Expression.eq("fromAddress", emailId))
                .addOrder(Order.desc("campaignId"))
                .add(Expression.eq("deleteCampaignFlag", false));
        List<?> campaignList = getHibernateTemplate().findByCriteria(criteria);
        return campaignList.size() != 0 ? (Campaign)campaignList.get(0) : null;
    }

/*   public Campaign[] getAllCampaigns(){
        List allCampaigns = getHibernateTemplate().find("from Campaign campaign ");
        return (Campaign[]) allCampaigns.toArray(new Campaign[allCampaigns.size()]);
        DetachedCriteria crit =  DetachedCriteria.forClass(Campaign.class);
        DetachedCriteria  address = Restrictions.like("fromAddress","Mou%");
        DetachedCriteria  name = Restrictions.like("name","Mou%");
        LogicalExpression orExp = Restrictions.or(address,name);
        crit.add(orExp);
        List results = crit.list();
        
        
        
    }*/
    
    public Campaign[] getCampaignsByKeyword(String keyword){
        DetachedCriteria crit =  DetachedCriteria.forClass(Campaign.class);
        Criterion  address = Restrictions.like("fromAddress","%"+keyword+"%");
        Criterion  name = Restrictions.like("campaignName","%"+keyword+"%");
        LogicalExpression orExp = Restrictions.or(address,name);
        crit.add(orExp);
        List<?> campaignList = getHibernateTemplate().findByCriteria(crit);
        return (Campaign[]) campaignList.toArray(new Campaign[campaignList.size()]);
    }
    
    public Campaign[] getCampaignsByKeywordandAccountId (Long accountId,String keyword) {
    	 DetachedCriteria crit =  DetachedCriteria.forClass(Campaign.class);
         Criterion  address = Restrictions.like("fromAddress","%"+keyword+"%");
         Criterion  name = Restrictions.like("campaignName","%"+keyword+"%");
         Criterion  id = Restrictions.eq("accountId", accountId);
         LogicalExpression orExp = Restrictions.or(address,name);
         crit.add(orExp);
         crit.add(id);
         List<?> campaignList = getHibernateTemplate().findByCriteria(crit);
         return (Campaign[]) campaignList.toArray(new Campaign[campaignList.size()]);
         
       
    }
    
    public void deleteExistingLinks(Long campaignId, Long accountId) {
    	String query = "delete from CampaignLinks where campaignId = "+campaignId ; 
    	int i = getHibernateTemplate().bulkUpdate(query);
    			
	}
    
    @SuppressWarnings("unchecked")
	public List<CampaignLinks> getCampaingLinks(Long accountId, Long campaignId) {
    	DetachedCriteria criteria = DetachedCriteria.forClass(CampaignLinks.class);
    	criteria.add(Expression.eq("campaignId", campaignId));
    	List<CampaignLinks> list = getHibernateTemplate().findByCriteria(criteria);
    	if(list == null)
    		list = new ArrayList<CampaignLinks>();
    	
    	return list;
	}
  
    public void updateRedeemCounterByOne(Long campaignId){
    int trial=0;
    boolean pass=false;
    do{
    try {
		System.out.println("updating count of campaign in repository " +campaignId);
		Campaign c=(Campaign) getHibernateTemplate().load(Campaign.class, campaignId, org.hibernate.LockMode.UPGRADE);
		c.setNoOfMailsRedeemed(c.getNoOfMailsRedeemed()+1);
		getHibernateTemplate().merge(c);
		pass=true;
	} catch (DataAccessException e) {
		trial++;
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
	}while(!pass&&trial<4);

    }
    public boolean getAckStatus(Long campaignId){
        String hql = "select ackFlag from Campaign where campaignId = "+campaignId;
        List<?> ackFlag = getHibernateTemplate().find(hql);
        return (Boolean)ackFlag.get(0);
    }
    public String getSubject(Long campaignId){
        String hql = "select subject from Campaign where campaignId = "+campaignId;
        List<?> subject = getHibernateTemplate().find(hql);
        return (String)subject.get(0);
    }
    
   
    
  
    public List<?> getCampaignIdsWithSameFromEmailAddress(String fromEmailAddress) {
         String hql = "select campaignId from Campaign where fromAddress = '"+fromEmailAddress+"'";
         return getHibernateTemplate().find(hql);

    }

    public long getMaximumCampignId(){
        String hql="select max(campaignId) from Campaign";
        Query query=getSession().createQuery(hql);
        return (Long)query.uniqueResult() == null ? 0 : (Long)query.uniqueResult();
    }

    public Campaign[] getAllCampaignswithSameFromEmailIds(String fromEmailAddress){
        List<?> allCampaigns = getHibernateTemplate().find("from Campaign campaign where campaign.fromAddress ='"+fromEmailAddress+"'");
        return (Campaign[]) allCampaigns.toArray(new Campaign[allCampaigns.size()]);
    }

    public Campaign[] getAllSubmittedCampaigns() {
        DetachedCriteria criteria = DetachedCriteria.forClass(Campaign.class)
                .add(Expression.in( "status", new String[] {"submitted", "completed","stopped","running"}))
                //.addOrder( Order.desc("status"))
                .addOrder( Order.desc("campaignId"))
                .add(Expression.eq("deleteCampaignFlag", false));
      List<?> campaignList = getHibernateTemplate().findByCriteria(criteria);
      return (Campaign[]) campaignList.toArray(new Campaign[campaignList.size()]);
    }

    public Campaign[] getAllShowcaseCampaigns() {

        DetachedCriteria criteria = DetachedCriteria.forClass(Campaign.class)
                .add(Expression.isNotNull("campaignStartDateTime"))
                .add(Expression.isNotNull("campaignEndDateTime"))
                .add(Expression.eq("showcaseActiveFlag", true))
                .add(Expression.eq("deleteCampaignFlag", true))
                .add(Expression.eq("status", "completed"))
                .addOrder(Order.desc("campaignEndDateTime"));
        List<?> campaignList = getHibernateTemplate().findByCriteria(criteria);
      return (Campaign[]) campaignList.toArray(new Campaign[campaignList.size()]);
    }

     public List<Campaign> get8ActiveShowcaseCampaignsByEndDate(String endDateTime) {
         SQLQuery query = getSession().createSQLQuery("SELECT SUBJECT," +
					"CLIENT_NAME from STAMPIT_CAMPAIGN "+
					"where SHOWCASE_ACK_FLAG='1' and STATUS='completed' and END_DATE_TIME>='"+endDateTime+"' LIMIT 8" );
         query.addScalar("SUBJECT",Hibernate.TEXT)
		 .addScalar("CLIENT_NAME",Hibernate.TEXT);

         List<?> list= query.list();
		 if(list==null)return null;
         return getCampaignListsFromQueryList(list);
    }

    private List<Campaign> getCampaignListsFromQueryList(List<?> list) {
        List<Campaign> campaignList = new ArrayList<Campaign>();
        for (Iterator<?> iterator = list.iterator(); iterator.hasNext();) {
            Object[] result = (Object[]) iterator.next();
            Campaign campaign = new Campaign();
            String subject = (String)result[0];
            campaign.setSubject(subject.replaceAll("'","&#39;"));  // replace single quotes with html symbol
            String client = (String)result[1];
            campaign.setClientName(client.replaceAll("'","&#39;"));  // replace single quotes with html symbol
            campaignList.add(campaign);
        }
        return campaignList;
    }

    public List<Campaign> get4ExpiredShowcaseCampaignByEndDate(String endDateTime) {
         SQLQuery query = getSession().createSQLQuery("SELECT SUBJECT," +
                    "CLIENT_NAME from STAMPIT_CAMPAIGN "+
                    "where SHOWCASE_ACK_FLAG='1' and STATUS='completed' and END_DATE_TIME<'"+endDateTime+"' order by END_DATE_TIME desc LIMIT 4" );
         query.addScalar("SUBJECT",Hibernate.TEXT)
         .addScalar("CLIENT_NAME",Hibernate.TEXT);

         List<?> list= query.list();
         if(list==null)return null;
         return getCampaignListsFromQueryList(list);
    }

	public boolean checkIflistisUsed(long listId) {
		String hql = "select campaignId from Campaign where listId =" + listId + " and deleteCampaignFlag ="+ 0;  
		
		List<?> list=getHibernateTemplate().find(hql);
		if(list != null && list.size() > 0)
		{
			return true;
		}
	
		return false;
    }
	
	public int getTotalPagesOfCampaign(long accountId) {
		String hql = "select count(campaignId) from Campaign where accountId =" + accountId + " and deleteCampaignFlag ="+ 0;  
		
		List<?> list=getHibernateTemplate().find(hql);
		if(list != null && list.size() > 0)
		{
			return (Integer)list.get(0);
		}
		
		return 0;
		
	}

	public Campaign getCampaignStatusReport(long accountId, Long campaignID) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<MessageOpen> getMailTotalOpnesList(Long campaignID) {
		// TODO Auto-generated method stub
		return null;
	}

	public MessageLinkClick getTotalClicksMailList(Long campaignID) {
		// TODO Auto-generated method stub
		return null;
	}

	public Map<Long, String> getActualUrls(long accountId, Long campaignId)
    {
        Map<Long, String> positionLinks = new HashMap<Long, String>();
        DetachedCriteria criteria = DetachedCriteria.forClass(CampaignLinks.class);
        criteria.add(Expression.eq("campaignId", campaignId));
        List<?> list = getHibernateTemplate().findByCriteria(criteria);
        CampaignLinks cl;
        for(Iterator<?> iterator = list.iterator(); iterator.hasNext(); positionLinks.put(Long.valueOf(cl.getPosition()), cl.getUrl()))
        {
            cl = (CampaignLinks)iterator.next();
        }

        return positionLinks;
    }

	@SuppressWarnings("unchecked")
	public List<MessageBounce> getMailBounceListData(Long campaignId,
			String bounceType,int pageNumber,int noOfRecords) {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(MessageBounce.class);
		criteria.add(Expression.eq("campaignId", campaignId));
		criteria.add(Expression.eq("bounceType", bounceType));
		int index = (pageNumber - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria,index, noOfRecords);
	}

	@SuppressWarnings("unchecked")
	public List<MessageBounce> getMailBounceListDataFilter(Long campaignId,
			String bounceType, Date fd, Date td, int pageNumber, int noOfRecordsperPage,String sp,String dm) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String fd1 = sdf.format(fd);
		String td1 = sdf.format(td);
		Date fromdate = sdf.parse(fd1);
		System.out.println(fromdate + "fromdate");
		Date todate = sdf.parse(td1);
		System.out.println(todate + "fromdate");
		DetachedCriteria criteria = DetachedCriteria
				.forClass(MessageBounce.class);
		criteria.add(Expression.eq("campaignId", campaignId));
		criteria.add(Expression.eq("bounceType", bounceType));
		criteria.add(Expression.eq("serviceProvider", sp));
		criteria.add(Expression.eq("domainName", dm));
		criteria.add(Expression.between("createdDate", fromdate, todate));
		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
	}

	@SuppressWarnings("unchecked")
	public List<MessageSpam> getMailSpamListData(List<String> validEmails,Date startTime, int pageNumber, int noOfRecordsperPage) {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(MessageSpam.class);
		criteria.add(Expression.in("reciepientEmail", validEmails));
		criteria.add(Expression.between("clickDate", startTime, new Date()));
		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index,noOfRecordsperPage);
	}

	@SuppressWarnings("unchecked")
	public List<MessageSpam> getMailSpamListDataFilter(List<String> emails, Date fd,Date td,int pageNumber, int noOfRecordsperPage) throws ParseException {
	
		DetachedCriteria criteria = DetachedCriteria
				.forClass(MessageSpam.class);
		criteria.add(Expression.in("reciepientEmail", emails));
		criteria.add(Expression.between("clickDate", fd, td));
		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index,noOfRecordsperPage);
	}

	public List<CampaignBounce> getCampaignMailBounceCount(long accountId,
			Long campaignID) {
		
		String hardBounceQuery="select count(*) from MessageBounce where campaignId=" + campaignID +" and bounceType='hardbounce'";
		List<?> hardBounce= getHibernateTemplate().find(hardBounceQuery);
		String sofBounceQuery="select count(*) from MessageBounce where campaignId=" + campaignID +" and bounceType='softbounce'";
		List<?> sofBounce= getHibernateTemplate().find(sofBounceQuery);
		String generalBounceQuery="select count(*) from MessageBounce where campaignId=" + campaignID +" and bounceType='generalbounce'";
		List<?> generalBounce= getHibernateTemplate().find(generalBounceQuery);
		String spamQuery="select count(*) from MessageSpam where campaignId=" + campaignID;
		List<?> spamCount= getHibernateTemplate().find(spamQuery);
		CampaignBounce bounce=new CampaignBounce();
		List<CampaignBounce> bounces=  new ArrayList<CampaignBounce>();
		bounce.setHardBounce((Integer) hardBounce.get(0)) ;
		bounce.setSoftBounce( (Integer) sofBounce.get(0));
		bounce.setGeneralBounce((Integer) generalBounce.get(0));
		bounce.setSpamCount((Integer) spamCount.get(0));
		bounces.add(bounce);
		return bounces;
	}

	public int getMailBounceCount(List<String> emails,Date startTime,Date fd,Date td) {
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageBounce.class);
		criteria.add(Expression.in("reciepientEmail", emails));
		criteria.add(Expression.between("bounceTime", startTime, td));
		//criteria.setProjection(Projections.rowCount());
		List<MessageBounce> bounceCount= getHibernateTemplate().findByCriteria(criteria);
		if(bounceCount != null && bounceCount.size() > 0)
		{
			//System.out.println("Count***"+bounceCount.size());
			//return (Integer)spamCount.get(0);
		
			return bounceCount.size();
		}
		
		return 0;
	}

	public int getMailBounceCountFilter(Long campaignID, String bounceType,
			Date fd, Date td) throws ParseException {
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageBounce.class);
		criteria.add(Expression.eq("campaignId", campaignID));
		criteria.add(Expression.eq("bounceType", bounceType));
		criteria.add(Expression.between("createdDate", fd, td));
		criteria.setProjection(Projections.rowCount());
		List<?> bounceCount= getHibernateTemplate().findByCriteria(criteria);
		if(bounceCount != null && bounceCount.size() > 0)
		{
			System.out.println("Count***"+bounceCount.get(0));
			return (Integer)bounceCount.get(0);
		}
		
		return 0;
	}
	
	public void exportToExcelMailFailed(CSVWriter writer, Long campaignID,
			long accountId) {
		 String[] headers ={"Email","Name"};
		 writer.writeNext(headers);
		 DetachedCriteria criteria = DetachedCriteria.forClass(CampaignMailFail.class);
			 criteria.add(Expression.eq("campaignId",campaignID));
			 @SuppressWarnings("unchecked")
			List<CampaignMailFail> datalist=getHibernateTemplate().findByCriteria(criteria);
			 for( int i = 0; i < datalist.size(); i++ ){
		           String[] data = {
		        		   datalist.get(i).getEmail(),
		        		   datalist.get(i).getName(),
		        		   };
		           writer.writeNext(data);
		    }
		
		
	}

public void exportToExcelMailFailedDateFilter(CSVWriter writer,
		Long campaignID, long accountId, 
		Date fromDate, Date toDate) {
	 String[] headers ={"Email","Name"};
	 writer.writeNext(headers);
	 DetachedCriteria criteria = DetachedCriteria.forClass(CampaignMailFail.class);
		 criteria.add(Expression.eq("campaignId",campaignID));
		 criteria.add(Expression.between("createdDate", fromDate, toDate));
		 @SuppressWarnings("unchecked")
		List<CampaignMailFail> datalist=getHibernateTemplate().findByCriteria(criteria);
		 for( int i = 0; i < datalist.size(); i++ ){
	           String[] data = {
	        		   datalist.get(i).getEmail(),
	        		   datalist.get(i).getName(),
	        		   };
	           writer.writeNext(data);
	    }
}
	public void exportToExcelMailBounces(CSVWriter writer, Long campaignID,
			long accountId, String bounceType,String sp,String dm) {
		
		String[] headers ={"Email","Name","Bounce Time","Bounce Type","Domain Name","Error Code","Reason For Bounce"};
		writer.writeNext(headers);
		CampaignReportService reportService = new CampaignReportService();
  	   	Date campaignStartTime=new CampaignService().getCampaign(campaignID).getCampaignStartDateTime();
  	    Date endTime=new CampaignService().getCampaign(campaignID).getCampaignEndDateTime();
  	    if(endTime == null)
  	    {
  	    	endTime=new Date();
  	    }
  	   	List<MessageBounce> datalist=reportService.getBounceEmails(bounceType,campaignStartTime,endTime,sp,dm);
		 for( int i = 0; i < datalist.size(); i++ ){
			  String[] data = {
	        		   datalist.get(i).getReciepientEmail(),
	        		   datalist.get(i).getReciepientName(),
	        		   datalist.get(i).getBounceTime().toString(),
	        		   datalist.get(i).getBounceType(),
	        		   datalist.get(i).getDomainName(),
	        		   Long.toString(datalist.get(i).getBounceCode()),
	        		   datalist.get(i).getReasonForBounce()
	        		   };
	           writer.writeNext(data);
	    }
		
		 
			
	}

	public void exportToExcelMailBouncesDateFilter(CSVWriter writer,
			Long campaignID, long accountId, String bounceType,
			Date fromDate, Date toDate,String sp,String dm) {
		 String[] headers ={"Email","Name","Bounce Time","Bounce Type","Domain Name","Error Code","Reason For Bounce"};
		 writer.writeNext(headers);
		 CampaignReportService reportService = new CampaignReportService();
	  	   	List<MessageBounce> datalist=reportService.getBounceEmails(bounceType,fromDate,toDate,sp,dm);

	  	    
			 for( int i = 0; i < datalist.size(); i++ ){
		           String[] data = {
		        		   datalist.get(i).getReciepientEmail(),
		        		   datalist.get(i).getReciepientName(),
		        		   datalist.get(i).getBounceTime().toString(),
		        		   datalist.get(i).getBounceType(),
		        		   datalist.get(i).getDomainName(),
		        		   Long.toString(datalist.get(i).getBounceCode()),
		        		   datalist.get(i).getReasonForBounce()
		        		   };
		           writer.writeNext(data);
		    }
	}

	public void exportToExcelMailSpams(CSVWriter writer, Long campaignID,
			long accountId,String sp,String dm) {
	   String[] headers ={"Email","Name","Click Time","Domain Name","Error Code","Other Reason"};
	   writer.writeNext(headers);
	   CampaignReportService reportService = new CampaignReportService();
	   Date campaignStartTime=new CampaignService().getCampaign(campaignID).getCampaignStartDateTime();
	   List<MessageSpam> datalist=reportService.getSpamEmails(campaignStartTime,sp,dm);
	   
	   for( int i = 0; i < datalist.size(); i++ ){
           String[] data = {
        		   datalist.get(i).getReciepientEmail(),
        		   datalist.get(i).getReciepientName(),
        		   datalist.get(i).getClickDate().toString(),
        		   datalist.get(i).getDomainName(),
        		   Long.toString(datalist.get(i).getSpamCode()),
        		   datalist.get(i).getOtherReason()
        		   
        		   
           };
           writer.writeNext(data);
	   }
	}
	public void exportToExcelMailSpamsDateFilter(CSVWriter writer,
			Long campaignID, long accountId, Date fd, Date td,String sp,String dm) {
		 String[] headers ={"Email","Name","Click Time","Domain Name","Error Code","Other Reason"};
		 writer.writeNext(headers);
		 CampaignReportService reportService = new CampaignReportService();
		   Date campaignStartTime=new CampaignService().getCampaign(campaignID).getCampaignStartDateTime();
		   List<MessageSpam> datalist=reportService.getSpamEmails(sp,dm,fd,td);
		   
		 for( int i = 0; i < datalist.size(); i++ ){
			   String[] data = {
					   datalist.get(i).getReciepientEmail(),
					   datalist.get(i).getReciepientName(),
					   datalist.get(i).getClickDate().toString(),
					   datalist.get(i).getDomainName(),
					   datalist.get(i).getSpamCode()+"",
					   datalist.get(i).getOtherReason()
					   
					   
			   };
			   writer.writeNext(data);
  }
		
		
		
	}

	public int getMailSpamCount(List<String> validEmails,Date startTime) {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(MessageSpam.class);
		criteria.add(Expression.in("reciepientEmail", validEmails));
		criteria.add(Expression.between("clickDate" ,startTime, new Date()));
		List<MessageSpam> spamCount= getHibernateTemplate().findByCriteria(criteria);
		if(spamCount != null && spamCount.size() > 0)
		{
			return spamCount.size();
		}
		return 0;
	}

	public int getMailSpamCountDateFilter(Date fd,Date td,String sp,String dm) {
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageSpam.class);
		criteria.add(Expression.eq("serviceProvider", sp));
		criteria.add(Expression.eq("domainName", dm));
		criteria.add(Expression.between("clickDate" ,fd, td));
		//criteria.setProjection(Projections.rowCount());
		List<MessageSpam> spamCount= getHibernateTemplate().findByCriteria(criteria);
		if(spamCount != null && spamCount.size() > 0)
		{
			System.out.println("Count***"+spamCount.size());
			//return (Integer)spamCount.get(0);
			return spamCount.size();
		}
		
		return 0;
	}
	@SuppressWarnings("unchecked")
	public List<CampaignMailFail> getMailFailDataList(Long campaignID, int pageNumber, int noOfRecordsperPage) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(CampaignMailFail.class);
	     criteria.add(Expression.eq("campaignId",campaignID));
	     int index = (pageNumber - 1) * noOfRecordsperPage;
	     List<CampaignMailFail> mailFailList = getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
	     System.out.println(mailFailList.size());
		return mailFailList;
	}
	@SuppressWarnings("unchecked")
	public List<CampaignMailFail> getCampaignMailFailFilterFromTodate(
			Long campaignID, Date fd, Date td, int pageNumber, int noOfRecordsperPage) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(CampaignMailFail.class);
	     criteria.add(Expression.eq("campaignId",campaignID));
	     criteria.add(Expression.between("createdDate" ,fd, td));
	     int index = (pageNumber - 1) * noOfRecordsperPage;
	     List<CampaignMailFail> mailFailList = getHibernateTemplate().findByCriteria(criteria, index, noOfRecordsperPage);
		return mailFailList;
	}

	public int getMailFailCount(Long campaignID) {
		String mailFailQuery="select count(*) from CampaignMailFail where campaignId=" + campaignID;
		List<?> failCount= getHibernateTemplate().find(mailFailQuery);
		if(failCount != null && failCount.size() > 0)
		{
			System.out.println("Count***"+failCount.get(0));
			return (Integer)failCount.get(0);
		}
		return 0;
	}

	public int getMailFailDateFilterCount(Long campaignID, Date fd, Date td) {
		DetachedCriteria criteria=DetachedCriteria.forClass(CampaignMailFail.class);
		criteria.add(Expression.eq("campaignId", campaignID));
		criteria.add(Expression.between("createdDate", fd, td));
		criteria.setProjection(Projections.rowCount());
		List<?> failCount= getHibernateTemplate().findByCriteria(criteria);
		if(failCount != null && failCount.size() > 0)
		{
			return (Integer)failCount.get(0);
		}
		
		return 0;
	}
	
	public void updateCampaignStopRecord(CampaignStop stop) {
		update(stop);
		
	}
	
	public void addBulkBounces(List<MessageBounce> recs) {
		 getHibernateTemplate().saveOrUpdateAll(recs);
		
	}
	
	public void addBulkSpams(List<MessageSpam> recs) {
		 getHibernateTemplate().saveOrUpdateAll(recs);
		
	}
	
	public long getMaxBounceRecId() {
		long count = 0;
		DetachedCriteria criteria = DetachedCriteria.forClass(MessageBounce.class);
				criteria.setProjection(Projections.max("bounceId"));
		List results = getHibernateTemplate().findByCriteria(criteria);
		if(results != null && results.size() > 0)
		{
			
			if(results.get(0) != null)
			count = (Long)results.get(0);
			
		}
		
		return count;
	}
	
	public long getMaxSpamRecId() {
		
		long count = 0;
		DetachedCriteria criteria = DetachedCriteria.forClass(MessageSpam.class);
				criteria.setProjection(Projections.max("spamId"));
		List results = getHibernateTemplate().findByCriteria(criteria);
		if(results != null && results.size() > 0)
		{
			
			if(results.get(0) != null)
			count = (Long)results.get(0);
			
		}
		
		return count;
		
	}
	
	public long getAllBounceCount() {
		long count = 0;
		String hsql  = "select count(id) from MessageBounce ";
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
	}
	
	public long getAllSpamCount() {
		long count = 0;
		String hsql  = "select count(id) from MessageSpam ";
		List results = getHibernateTemplate().find(hsql);
		if(results != null && results.size() > 0)
		{
			count = (Integer)results.get(0);
		}
		return count;
	}

	public List<MessageBounce> getAllBouncedEmails(String bounceType,
			Date campaignStartTime,String sp,String dm,int pageNumber,int noOfRecordsperPage) {
		Date now = new Date();
		if(bounceType.equals("totalmailBounce")){
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageBounce.class);
		criteria.add(Expression.eq("serviceProvider", sp));
		criteria.add(Expression.eq("domainName", dm));
		criteria.add(Expression.ge("bounceTime", campaignStartTime));
		criteria.add(Expression.le("bounceTime", new Date()));
		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
		}
		else
		{
			DetachedCriteria criteria=DetachedCriteria.forClass(MessageBounce.class);
			criteria.add(Expression.eq("bounceType", bounceType));
			criteria.add(Expression.eq("serviceProvider", sp));
			criteria.add(Expression.eq("domainName", dm));
			criteria.add(Expression.ge("bounceTime", campaignStartTime));
			criteria.add(Expression.le("bounceTime", new Date()));
			int index = (pageNumber - 1) * noOfRecordsperPage;
			return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
		}
		}
	

	public List<MessageBounce> getBounceData(List<String> validEmail,Date startTime) {
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageBounce.class);
		criteria.add(Expression.in("reciepientEmail", validEmail));
		criteria.add(Expression.between("bounceTime", startTime,new Date()));
		return getHibernateTemplate().findByCriteria(criteria);
		
	}

	public List<MessageSpam> getSpamEmails(Date campaignStartTime,String sp,String dm) {
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageSpam.class);
		criteria.add(Expression.eq("serviceProvider", sp));
		criteria.add(Expression.eq("domainName", dm));
		criteria.add(Expression.between("clickDate", campaignStartTime,new Date()));
		return getHibernateTemplate().findByCriteria(criteria);
		
	}
	public List<MessageSpam> getSpamEmails(String sp,String dm,Date fd,Date td) {
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageSpam.class);
		criteria.add(Expression.eq("serviceProvider", sp));
		criteria.add(Expression.eq("domainName", dm));
		criteria.add(Expression.between("clickDate", fd,td));
		return getHibernateTemplate().findByCriteria(criteria);
		
	}

	public List<MessageSpam> getSpamData(List<String> validEmail) {
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageSpam.class);
		criteria.add(Expression.in("reciepientEmail", validEmail));
		return getHibernateTemplate().findByCriteria(criteria);
	}

	
	 public List<Campaign> getAllCampaignInPagination(String partnerName, int pagenumber,
				int noOfRecords) {
	    	
	    	DetachedCriteria criteria = DetachedCriteria.forClass(Campaign.class);
	    	criteria.add(Expression.eq("partnerName", partnerName));
	        criteria.add(Expression.eq("deleteCampaignFlag", false));
	        criteria.addOrder(Order.desc("campaignId"));
			int index = (pagenumber - 1) * noOfRecords;
			return getHibernateTemplate().findByCriteria(criteria, index, noOfRecords); 
		
		}

	public int getTotalPagesOfCampaign(String partnerName) {
			String hql = "select count(campaignId) from Campaign where partnerName ='"+partnerName+"' and deleteCampaignFlag ="+ 0;  
			List<?> list=getHibernateTemplate().find(hql);
			if(list != null && list.size() > 0)
			{
				return (Integer)list.get(0);
			}
			return 0;
		}

	public CampaignBounce getCampaignMailBounceCountByDomain(Long campaignID, String dm,
			String sp, Date fromTime, Date toTime) throws ParseException {
		
		System.out.println("starttime:"+fromTime+"endtime:"+toTime+"domainname:"+dm+"sp is :"+sp);
		String hardBounceQuery="select count(*) from MessageBounce where domainName='"+dm+"' and bounceType='hardbounce' and serviceProvider='"+sp+"' and bounceTime >= '"+fromTime+"' and bounceTime <= '"+toTime+"' ";
		List<?> hardBounce= getHibernateTemplate().find(hardBounceQuery);
		String sofBounceQuery="select count(*) from MessageBounce where domainName='" + dm+"' and bounceType='softbounce' and serviceProvider='"+sp+"' and bounceTime >=  '"+fromTime+"' and bounceTime <= '"+toTime+"' ";
		List<?> sofBounce= getHibernateTemplate().find(sofBounceQuery);
		String generalBounceQuery="select count(*) from MessageBounce where domainName='" + dm+"' and bounceType='generalbounce' and serviceProvider='"+sp+"' and bounceTime >=  '"+fromTime+"' and bounceTime <= '"+toTime+"'" ;
		List<?> generalBounce= getHibernateTemplate().find(generalBounceQuery);
        
		String transientBounceQuery="select count(*) from MessageBounce where domainName='" + dm+"' and bounceType='transientbounce' and serviceProvider='"+sp+"' and bounceTime >=  '"+fromTime+"' and bounceTime <= '"+toTime+"'" ;
		List<?> transientBounce= getHibernateTemplate().find(transientBounceQuery);
		
		String totalBounceQuery="select count(*) from MessageBounce where domainName='" + dm+"' and serviceProvider='"+sp+"' and bounceTime >=  '"+fromTime+"' and bounceTime <= '"+toTime+"'" ;
		List<?> totalBounce= getHibernateTemplate().find(totalBounceQuery);
        
		String spamsql  = "select count(*) from MessageSpam where domainName='"+dm+"' and serviceProvider= '"+ sp+"' and clickDate >= '"+fromTime +"' and  clickDate <= '"+toTime+"'" ;
		List<?> spamcount=getHibernateTemplate().find(spamsql);
		
		
		//String hsql  = "select count(*) from MessageUnsubscribe where campaignId="+campaignID ;
		//List<?> unsubscribeCount=getHibernateTemplate().find(hsql);
		
		CampaignBounce bounce=new CampaignBounce();
		//List<CampaignBounce> bounces=  new ArrayList<CampaignBounce>();
		bounce.setHardBounce((Integer) hardBounce.get(0)) ;
		bounce.setSoftBounce( (Integer) sofBounce.get(0));
		bounce.setGeneralBounce((Integer) generalBounce.get(0));
		bounce.setSpamCount((Integer)spamcount.get(0));
		bounce.setTransientBounce((Integer)transientBounce.get(0) );
		bounce.setTotalNoOfBounces((Integer) totalBounce.get(0));
		System.out.println((Integer) totalBounce.get(0) +"total bounce");
		
		
		//System.out.println(spamcount.get(0) + "spam count");
		//bounce.setUnsubscribeCount((Integer)unsubscribeCount.get(0));
	//	bounces.add(bounce);
		
	/*	System.out.println(hardBounce.get(0) +"hard bouncee");
		System.out.println(sofBounce.get(0) +"sofBounce");
		System.out.println(generalBounce.get(0) +"generalBounce");
		System.out.println(spamcount.get(0) +"spamcount");*/
		return bounce;
	}
	
	public CampaignBounce getCampaignMailBounceCountByDomain(Long campaignID,
			String dm, String sp, String onDate) throws ParseException {
		//System.out.println("starttime:"+onDate+" domainname:"+dm+"sp is :"+sp);
		
		String endDate=(String) onDate.subSequence(0, 10)+"23:59:59";
		//System.out.println(endDate +"end date");
		
		//String hardBounceQuery="select count(*) from MessageBounce where domainName='"+dm+"' and bounceType='hardbounce' and serviceProvider='"+sp+"' and bounceTime between '"+onDate1+"' and '"+onDate1+"' ";
		String hardBounceQuery="select count(*) from MessageBounce where domainName='"+dm+"' and bounceType='hardbounce' and serviceProvider='"+sp+"' and bounceTime >= '"+onDate+"' and bounceTime <= '"+endDate+"' ";
		List<?> hardBounce= getHibernateTemplate().find(hardBounceQuery);
		
		String sofBounceQuery="select count(*) from MessageBounce where domainName='" + dm +"' and bounceType='softbounce' and serviceProvider='"+sp+"' and bounceTime >= '"+onDate+"' and bounceTime <= '"+endDate+"' ";
		List<?> sofBounce= getHibernateTemplate().find(sofBounceQuery);
		
		String generalBounceQuery="select count(*) from MessageBounce where domainName='" + dm +"' and bounceType='generalbounce' and serviceProvider='"+sp+"' and  bounceTime >= '"+onDate+"' and bounceTime <= '"+endDate+"' ";
		List<?> generalBounce= getHibernateTemplate().find(generalBounceQuery);
		
		String transientBounceQuery="select count(*) from MessageBounce where domainName='" + dm+"' and bounceType='transientbounce' and serviceProvider='"+sp+"' and bounceTime >=  '"+onDate+"' and bounceTime <= '"+endDate+"'" ;
		List<?> transientBounce= getHibernateTemplate().find(generalBounceQuery);
		
		String spamsql  = "select count(*) from MessageSpam where domainName='"+dm+"' and serviceProvider= '"+ sp+"' and clickDate >= '"+onDate +"' and clickDate <='"+endDate +"'";
		List<?> spamcount=getHibernateTemplate().find(spamsql);
		
				
		CampaignBounce bounce=new CampaignBounce();
		bounce.setHardBounce((Integer) hardBounce.get(0)) ;
		bounce.setSoftBounce( (Integer) sofBounce.get(0));
		bounce.setGeneralBounce((Integer) generalBounce.get(0));
		bounce.setSpamCount((Integer)spamcount.get(0) );
		bounce.setTransientBounce((Integer)transientBounce.get(0) );
		//bounce.setUnsubscribeCount((Integer)unsubscribeCount.get(0));	
	/*	System.out.println(hardBounce.get(0) + "hard bounce");
		System.out.println(sofBounce.get(0) + "sofBounce bounce");
		System.out.println(generalBounce.get(0) + "generalBounce bounce");
		System.out.println(spamcount.get(0) + "spam");*/
		
		return bounce;
	}

	@SuppressWarnings("unchecked")
	public List<MessageBounce> getCampaignBounceDataByDomain(String dm,
			String sp, String bounceType, Date cstime, Date cendTime,int pageNumber, int noOfRecordsperPage) throws ParseException {
			DetachedCriteria criteria = DetachedCriteria
					.forClass(MessageBounce.class);
			criteria.add(Expression.eq("domainName", dm));
			criteria.add(Expression.eq("serviceProvider", sp));
			criteria.add(Expression.eq("bounceType", bounceType));
			criteria.add(Expression.ge("bounceTime", cstime));
			criteria.add(Expression.le("bounceTime", cendTime));
			int index = (pageNumber - 1) * noOfRecordsperPage;
			return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
	}

	@SuppressWarnings("unchecked")
	public List<MessageSpam> getCampaignSpamData(String dm, String sp,
			Date cstime, Date cendTime, Integer pageNumber,
			int noOfRecordsperPage) {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(MessageSpam.class);
		criteria.add(Expression.eq("domainName", dm));
		criteria.add(Expression.eq("serviceProvider", sp));
		criteria.add(Expression.ge("clickDate", cstime));
		criteria.add(Expression.le("clickDate", cendTime));
		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
	}

	public float getMailSpamCount(String dm, String sp, Date cstime,Date cendTime) {
		  String hsql  = "select count(*) from MessageSpam where domainName='"+dm+"' and serviceProvider= '"+ sp+"' and clickDate >= '"+cstime +"' and  clickDate <= '"+cendTime+"'" ;
		  List<?> spamcount=getHibernateTemplate().find(hsql);
		  if(spamcount != null && spamcount.size() > 0)
			{
				return (Integer)spamcount.get(0);
			}
			return 0;
	}
	
	public float getMailBounceCount(String dm, String sp, Date cstime,
			Date cendTime,String bounceType) {
		String bounceQuery=null;
		if(bounceType.equals("totalmailBounce")){
		 bounceQuery="select count(*) from MessageBounce where domainName='"+dm+"' and serviceProvider='"+sp+"' and bounceTime >= '"+cstime+"' and bounceTime <= '"+cendTime+"' ";
		}
		else
		{
			bounceQuery="select count(*) from MessageBounce where domainName='"+dm+"' and bounceType='"+bounceType+"' and serviceProvider='"+sp+"' and bounceTime >= '"+cstime+"' and bounceTime <= '"+cendTime+"' ";
		}
		  List<?> bouncecount=getHibernateTemplate().find(bounceQuery);
		  if(bouncecount != null && bouncecount.size() > 0)
			{
				return (Integer)bouncecount.get(0);
			}
			return 0;
	}
	 @SuppressWarnings("unchecked")
	public void exportToExcelBouncesByDomain(CSVWriter writer, String bounceType,String dm,
			String sp, Date cstime, Date cendTime) {
		String[] headers ={"Email","Name","Bounce Time","Bounce Type","Domain Name","Error Code","Reason For Bounce"};
		writer.writeNext(headers);
		DetachedCriteria criteria = DetachedCriteria
				.forClass(MessageBounce.class);
		criteria.add(Expression.eq("domainName", dm));
		criteria.add(Expression.eq("serviceProvider", sp));
		criteria.add(Expression.eq("bounceType", bounceType));
		criteria.add(Expression.ge("bounceTime", cstime));
		criteria.add(Expression.le("bounceTime", cendTime));
		
		List<MessageBounce> datalist=getHibernateTemplate().findByCriteria(criteria);
		
		 for( int i = 0; i < datalist.size(); i++ ){
			  String[] data = {
	        		   datalist.get(i).getReciepientEmail(),
	        		   datalist.get(i).getReciepientName(),
	        		   datalist.get(i).getBounceTime().toString(),
	        		   datalist.get(i).getBounceType(),
	        		   datalist.get(i).getDomainName(),
	        		   Long.toString(datalist.get(i).getBounceCode()),
	        		   datalist.get(i).getReasonForBounce()
	        		   };
	           writer.writeNext(data);
	    }
		
		 
	}
	 @SuppressWarnings("unchecked")
	public void exportToExcelSpamsByDomain(CSVWriter writer, String dm,
			String sp, Date cstime, Date cendTime) {
		
		   String[] headers ={"Email","Name","Click Time","Domain Name","Error Code","Other Reason"};
		   writer.writeNext(headers);
		   DetachedCriteria criteria = DetachedCriteria
					.forClass(MessageSpam.class);
			criteria.add(Expression.eq("domainName", dm));
			criteria.add(Expression.eq("serviceProvider", sp));
			criteria.add(Expression.ge("clickDate", cstime));
			criteria.add(Expression.le("clickDate", cendTime));
		   List<MessageSpam> datalist=getHibernateTemplate().findByCriteria(criteria);
		   for( int i = 0; i < datalist.size(); i++ ){
	           String[] data = {
	        		   datalist.get(i).getReciepientEmail(),
	        		   datalist.get(i).getReciepientName(),
	        		   datalist.get(i).getClickDate().toString(),
	        		   datalist.get(i).getDomainName(),
	        		   datalist.get(i).getSpamCode()+"",
	        		   datalist.get(i).getOtherReason()
	        		   
	        		   
	           };
	           writer.writeNext(data);
		   }
		
		
		
	}

	@SuppressWarnings("unchecked")
	public List<MessageUnsubscribe> getCampaignUnsubscribeData(Long campaignID,
			Integer pageNumber, int noOfRecordsperPage) {
		DetachedCriteria criteria = DetachedCriteria.forClass(MessageUnsubscribe.class);
		criteria.add(Expression.eq("campaignId", campaignID));
		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
	}

	public float getMailUnsubscribeCount(Long campaignID) {
		String unsubscribeQuery="select count(*) from MessageUnsubscribe where campaignId="+campaignID;
		  List<?> unsubscribecount=getHibernateTemplate().find(unsubscribeQuery);
		  if(unsubscribecount != null && unsubscribecount.size() > 0)
			{
				return (Integer)unsubscribecount.get(0);
			}
			return 0;
	}

	@SuppressWarnings("unchecked")
	public List<MessageUnsubscribe> getCampaignUnsubscribeData(Long campaignID,
			Date fd, Date td, Integer pageNumber, int noOfRecordsperPage) throws ParseException {
		
		DetachedCriteria criteria = DetachedCriteria
				.forClass(MessageUnsubscribe.class);
		criteria.add(Expression.eq("campaignId", campaignID));
		criteria.add(Expression.ge("unsubscribeDate", fd));
		criteria.add(Expression.le("unsubscribeDate", td));

		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
	}
	
	public void exportToExcelUnsubscribeMail(CSVWriter writer,
			String campaignId, long accountId) {
		 String[] headers ={"Reciepient Email","Reciepient name","Unsubscribe Time","Unsubscribe Ip","Unsubscribe Location","Browser","Device","OS"};
		 writer.writeNext(headers);
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageUnsubscribe.class);
			 criteria.add(Expression.eq("campaignId",campaignId));
			 @SuppressWarnings("unchecked")
			 List<MessageUnsubscribe> datalist= getHibernateTemplate().find("from MessageUnsubscribe where campaignId=" + campaignId);
			 
			 for( int i = 0; i < datalist.size(); i++ ){
		           String[] data = {
		        		   datalist.get(i).getReciepientEmail(),
		        		   datalist.get(i).getReciepientName(),
		        		   datalist.get(i).getUnsubscribeDate().toString(),
		        		   datalist.get(i).getUnsubscribeIP(),
		        		   datalist.get(i).getBrowser(),
		        		   datalist.get(i).getDevice(),
		        		   datalist.get(i).getOs(),
		        		   };
		           writer.writeNext(data);
		    }
	}

	public void exportToExcelUnsubscribeMail(CSVWriter writer, Long campaignID,
			long accountId, Date fd, Date td) {
		 String[] headers ={"Reciepient Email","Reciepient name","Unsubscribe Time","Unsubscribe Ip","Unsubscribe Location","Browser","Device","OS"};
		 writer.writeNext(headers);
		 DetachedCriteria criteria=DetachedCriteria.forClass(MessageUnsubscribe.class);
			criteria.add(Expression.eq("campaignId", campaignID));
			criteria.add(Expression.ge("unsubscribeDate", fd));
			criteria.add(Expression.le("unsubscribeDate", td));
			@SuppressWarnings("unchecked")
			List<MessageUnsubscribe> datalist= getHibernateTemplate().findByCriteria(criteria);
			 for( int i = 0; i < datalist.size(); i++ ){
		           String[] data = {
		        		   datalist.get(i).getReciepientEmail(),
		        		   datalist.get(i).getReciepientName(),
		        		   datalist.get(i).getUnsubscribeDate().toString(),
		        		   datalist.get(i).getUnsubscribeIP(),
		        		   datalist.get(i).getBrowser(),
		        		   datalist.get(i).getDevice(),
		        		   datalist.get(i).getOs(),
		        		   };
		           writer.writeNext(data);
		    }
	}

		
	

	@SuppressWarnings("unchecked")
	public List<CampaignMailSentSoFar> getCampaignMailSentSoFarData(
			Long campaignID, Date fd, Date td, Integer pageNumber,
			int noOfRecordsperPage) {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(CampaignMailSentSoFar.class);
		criteria.add(Expression.eq("campaignId", campaignID));
		/*criteria.add(Expression.ge("unsubscribeDate", fd));
		criteria.add(Expression.le("unsubscribeDate", td));*/

		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
	}
	@SuppressWarnings("unchecked")
	public List<CampaignMailSentSoFar> getCampaignMailSentSoFarData(
			Long campaignID, Integer pageNumber, int noOfRecordsperPage) {
		DetachedCriteria criteria = DetachedCriteria.forClass(CampaignMailSentSoFar.class);
		criteria.add(Expression.eq("campaignId", campaignID));
		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
	}
	

	public float getMailMailSentSoFarCount(Long campaignID) {
		String mailSentSoFarQuery="select count(*) from CampaignMailSentSoFar where campaignId="+campaignID;
		  List<?> mailSentSoFarCount=getHibernateTemplate().find(mailSentSoFarQuery);
		  if(mailSentSoFarCount != null && mailSentSoFarCount.size() > 0)
			{
				return (Integer)mailSentSoFarCount.get(0);
			}
			return 0;
	}


public void exportToExcelMailSentSoFar(CSVWriter writer, String campaignId,
			long accountId) {
		 String[] headers ={"Reciepient Email","Reciepient name"};
		 writer.writeNext(headers);
		 DetachedCriteria criteria = DetachedCriteria.forClass(CampaignMailSentSoFar.class);
			 criteria.add(Expression.eq("campaignId",campaignId));
			 @SuppressWarnings("unchecked")
			 List<CampaignMailSentSoFar> datalist= getHibernateTemplate().find("from CampaignMailSentSoFar where campaignId=" + campaignId);
			 
			 for( int i = 0; i < datalist.size(); i++ ){
		           String[] data = {
		        		   datalist.get(i).getEmail(),
		        		   datalist.get(i).getName()
		        		   };
		           writer.writeNext(data);
		    }
		
	}

	public void exportToExcelMailSentSoFar(CSVWriter writer, Long campaignID,
			long accountId, Date fd, Date td) {
		 String[] headers ={"Reciepient Email","Reciepient name"};
		 writer.writeNext(headers);
		 DetachedCriteria criteria = DetachedCriteria.forClass(CampaignMailSentSoFar.class);
			criteria.add(Expression.eq("campaignId", campaignID));
			/*criteria.add(Expression.ge("unsubscribeDate", fd));
			criteria.add(Expression.le("unsubscribeDate", td));*/
			@SuppressWarnings("unchecked")
			List<CampaignMailSentSoFar> datalist= getHibernateTemplate().findByCriteria(criteria);
			 for( int i = 0; i < datalist.size(); i++ ){
		           String[] data = {
		        		   datalist.get(i).getEmail(),
		        		   datalist.get(i).getName()	
		           };
		           
		           writer.writeNext(data);
			 }
	}

	public List<MessageSpam> getSpamEmails(Date fd,Date td, String sp,
			String dm,int pageNumber,int noOfRecordsperPage) {
		
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageSpam.class);
		 criteria.add(Expression.eq("serviceProvider",sp));
		 criteria.add(Expression.eq("domainName",dm));
		 criteria.add(Expression.between("clickDate", fd, td));
		 int index = (pageNumber - 1) * noOfRecordsperPage;
			return getHibernateTemplate().findByCriteria(criteria,index,noOfRecordsperPage);
	}

	public List<String> getAllBouncedEmails(String bounceType,Date campaignStartTime, Date fd,
			Date td, String sp, String dm) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(MessageBounce.class);
		 criteria.add(Expression.eq("bounceType", bounceType));
		 criteria.add(Expression.eq("serviceProvider",sp));
		 criteria.add(Expression.eq("domainName",dm));
		 criteria.add(Expression.between("bounceTime", fd, td));

		 @SuppressWarnings("unchecked")
		 List<MessageBounce> datalist=getHibernateTemplate().findByCriteria(criteria);
		 List<String> emails=new ArrayList<String>();
		 if(datalist !=null && datalist.size() >0)
		 {
			 for(int i=0;i<datalist.size();i++)
			 {
				 emails.add(datalist.get(i).getReciepientEmail());
			 }
			 return emails;
		 }
		
		
		return new ArrayList<String>();
	}

	public List<MessageBounce> getCampaignBounceData(List<String> validEmails,Date startTime,Date toDate,
			Integer pageNumber, int noOfRecordsperPage) {
		DetachedCriteria criteria = DetachedCriteria
				.forClass(MessageBounce.class);
		criteria.add(Expression.in("reciepientEmail", validEmails));
		criteria.add(Expression.between("bounceTime", startTime,toDate));
		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index,noOfRecordsperPage);
	}

	public List<MessageBounce> getAllBouncedEmails(String bounceType, Date fromDate,
			Date toDate, String sp, String dm) {
		if(bounceType.equals("totalmailBounce"))
		{
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageBounce.class);
		criteria.add(Expression.eq("serviceProvider", sp));
		criteria.add(Expression.eq("domainName", dm));
		criteria.add(Expression.between("bounceTime", fromDate,toDate));
		return getHibernateTemplate().findByCriteria(criteria);
		}
		else{
			DetachedCriteria criteria=DetachedCriteria.forClass(MessageBounce.class);
			criteria.add(Expression.eq("bounceType", bounceType));
			criteria.add(Expression.eq("serviceProvider", sp));
			criteria.add(Expression.eq("domainName", dm));
			criteria.add(Expression.between("bounceTime", fromDate,toDate));
			return getHibernateTemplate().findByCriteria(criteria);
		}
	}

	public float getMailBounceCount(List<String> validEmail,
			Date campaignStartTime) {
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageBounce.class);
		criteria.add(Expression.in("reciepientEmail", validEmail));
		criteria.add(Expression.between("bounceTime", campaignStartTime, new Date()));
		//criteria.setProjection(Projections.rowCount());
		List<MessageBounce> bounceCount= getHibernateTemplate().findByCriteria(criteria);
		if(bounceCount != null && bounceCount.size() > 0)
		{
			//System.out.println("Count***"+bounceCount.size());
			//return (Integer)spamCount.get(0);
		
			return bounceCount.size();
		}
		
		return 0;
	}

	public List<MessageSpam> getSpamEmails(Date campaignStartTime, String sp,
			String dm, Integer pageNumber, int noOfRecordsperPage) {
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageSpam.class);
		criteria.add(Expression.eq("serviceProvider", sp));
		criteria.add(Expression.eq("domainName", dm));
		criteria.add(Expression.between("clickDate", campaignStartTime,new Date()));
		 int index = (pageNumber - 1) * noOfRecordsperPage;
			return getHibernateTemplate().findByCriteria(criteria,index,noOfRecordsperPage);
	}

	public List<MessageBounce> getBounceEmails(String bounceType, Date fd,
			Date td, String sp, String dm, Integer pageNumber,
			int noOfRecordsperPage) {
		
		if(bounceType.equals("totalmailBounce")){
		DetachedCriteria criteria=DetachedCriteria.forClass(MessageBounce.class);
		criteria.add(Expression.eq("serviceProvider", sp));
		criteria.add(Expression.eq("domainName", dm));
		criteria.add(Expression.ge("bounceTime", fd));
		criteria.add(Expression.le("bounceTime",td));
		int index = (pageNumber - 1) * noOfRecordsperPage;
		return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
		}
		else
		{
			
			DetachedCriteria criteria=DetachedCriteria.forClass(MessageBounce.class);
			criteria.add(Expression.eq("bounceType", bounceType));
			criteria.add(Expression.eq("serviceProvider", sp));
			criteria.add(Expression.eq("domainName", dm));
			criteria.add(Expression.ge("bounceTime", fd));
			criteria.add(Expression.le("bounceTime",td));
			int index = (pageNumber - 1) * noOfRecordsperPage;
			return getHibernateTemplate().findByCriteria(criteria,index, noOfRecordsperPage);
		}
	}



}


