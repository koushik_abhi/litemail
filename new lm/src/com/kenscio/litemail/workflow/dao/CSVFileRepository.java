package com.kenscio.litemail.workflow.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;


import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.kenscio.litemail.workflow.domain.CSVFile;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.common.LoggerUtil;


public class CSVFileRepository  {
	public CSVFile getData(String csvFullFileName) {
		CSVReader reader = null;
		CSVFile csvFile = new CSVFile();
		try {
			reader = new CSVReader(new FileReader(csvFullFileName));
			List emailList = reader.readAll();
			reader.close();
			csvFile.setCsvFileName(csvFullFileName);
            csvFile.setData(getEmailDataList(emailList));
		} catch (FileNotFoundException e) {
			LoggerUtil.doLog(e.getMessage());
		} catch (IOException e) {
			LoggerUtil.doLog(e.getMessage());
		}
		return csvFile;

	}
	
	private EmailData[] getEmailDataListFromHeaders(List dataList) {
		List emaildataList = new ArrayList();
		int firstRow = 0;
		int emailInd = 0;
		int nameInd = 0;
		for (Iterator iterator = dataList.iterator(); iterator.hasNext();) {
			if(firstRow == 0)
			{
				String[] data = (String[]) iterator.next();
				for(int i=0 ; i< data.length; i++)
				{
					String str = data[i];
					if(str != null && str.toLowerCase().contains("mail"))
					{
						emailInd = i;
					}
					if(str != null && str.toLowerCase().contains("name"))
					{
						nameInd = i;
					}
				}
			}else
			{
				String[] data = (String[]) iterator.next();
				EmailData emailData = new EmailData();
	            if (data.length >= 2) {
					emailData.setName(data[nameInd]);
					emailData.setEmailId(data[emailInd]);
					emaildataList.add(emailData);
				} else if (data.length == 1) {
					if (!"".equals(data[0])) {
						emailData.setEmailId(data[0]);
						emaildataList.add(emailData);
					}
	            }
			}
            firstRow++;
		}
        return (EmailData[]) emaildataList.toArray(new EmailData[emaildataList
				.size()]);
	}
	
	public CSVFile getDataWithoutHeaders(String csvFullFileName) {
		CSVReader reader = null;
		CSVFile csvFile = new CSVFile();
		try {
			reader = new CSVReader(new FileReader(csvFullFileName));
			List emailList = reader.readAll();
			reader.close();
			csvFile.setCsvFileName(csvFullFileName);
            csvFile.setData(getEmailDataListFromHeaders(emailList));
		} catch (FileNotFoundException e) {
			LoggerUtil.doLog(e.getMessage());
		} catch (IOException e) {
			LoggerUtil.doLog(e.getMessage());
		}
		return csvFile;

	}
	
/*	public CSVFile getDataFromImportedCsvWithoutHeaders(FormFile formfile) {
		CSVReader reader = null;
		CSVFile csvFile = new CSVFile();
		try {
			reader = new CSVReader(new InputStreamReader(formfile.getInputStream()));
			List emailList = reader.readAll();
			if(emailList != null && emailList.size() > 0)
				emailList.remove(0);
			reader.close();
			csvFile.setCsvFileName(formfile.getFileName());
            csvFile.setData(getEmailDataListFromHeaders(emailList));
		} catch (FileNotFoundException e) {
			LoggerUtil.doLog(e.getMessage());
		} catch (IOException e) {
			LoggerUtil.doLog(e.getMessage());
		}
		return csvFile;

	}*/


/*	public CSVFile getDataFromImportedCsv(FormFile formfile) {
		CSVReader reader = null;
		CSVFile csvFile = new CSVFile();
		try {
			reader = new CSVReader(new InputStreamReader(formfile.getInputStream()));
			List emailList = reader.readAll();
			reader.close();
			csvFile.setCsvFileName(formfile.getFileName());
            csvFile.setData(getEmailDataList(emailList));
		} catch (FileNotFoundException e) {
			LoggerUtil.doLog(e.getMessage());
		} catch (IOException e) {
			LoggerUtil.doLog(e.getMessage());
		}
		return csvFile;

	}*/
	
	public List getDataFromFile(String filePath) {
		CSVReader reader = null;
		List dataList = new ArrayList();
		try {
			reader = new CSVReader(new FileReader(filePath));
			dataList = reader.readAll();
			reader.close();
		} catch (FileNotFoundException e) {
			LoggerUtil.doLog(e.getMessage());
		} catch (IOException e) {
			LoggerUtil.doLog(e.getMessage());
		}
		return dataList;
	}

	public boolean storeData(CSVFile csvFile) {
		try {
			saveFile(csvFile.getCsvFileName(), getDataStringList(csvFile
					.getData()));
		} catch (IOException e) {
			LoggerUtil.doLog(e.getMessage());
			return false;
		}
		return true;
	}

	public boolean storeValuesToFile(String filePath, List dataList) {
		try {
			List existingList = null;
			File storeFile = new File(filePath);
			if (storeFile.exists()) {
				CSVReader reader = new CSVReader(new FileReader(filePath));
				existingList = reader.readAll();
				reader.close();
				existingList.addAll(dataList);
				saveFile(filePath, existingList);
			} else {
				saveFile(filePath, dataList);
			}
		} catch (IOException e) {
			LoggerUtil.doLog(e.getMessage());
			return false;
		}
		return true;
	}

	public HashMap getEmailDataFromCSVFile(String filepath, Long filePointer,
			int rows) {
		HashMap result = new HashMap();
		List list = new ArrayList();
		Long nextPosToRead = 0L;
		try {
			java.io.RandomAccessFile csvfile = new java.io.RandomAccessFile(
					filepath, "r");
			String line = null;
			int rowsRead = 0;
			csvfile.seek(filePointer);

			while ((line = csvfile.readLine()) != null && rowsRead < rows) {
				EmailData emailData = getEmailDataFromString(line);
				if (emailData != null)
					list.add(emailData);
				rowsRead++;
				nextPosToRead = csvfile.getFilePointer();
			}
			result.put(nextPosToRead, (EmailData[]) list
					.toArray(new EmailData[list.size()]));
			// System.out.println(list.size());
			csvfile.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;

	}
	private EmailData getEmailDataFromString(String line) {
		EmailData emailData = new EmailData();
		StringTokenizer tokenizer = new StringTokenizer(line, ",");
		String name = null;
		if (tokenizer.hasMoreTokens() && (name = tokenizer.nextToken()) != null) {
			name = name.replaceAll("\"", "");
			name = name.replaceAll("'", "");
			if (name != null) {
				emailData.setName(name);
			}
		}
		String email = null;
		if (tokenizer.hasMoreTokens()
				&& (email = tokenizer.nextToken()) != null) {
			email = email.replaceAll("\"", "");
			email = email.replaceAll("'", "");
			if (com.kenscio.litemail.util.StringUtil.isValidEmail(email)) {
				emailData.setEmailId(email);
				//return emailData;
			} else {
				System.out.println(" email " + email + " is not a valid id");
			}
		}
      
        String stamp=null;
        if (tokenizer.hasMoreTokens()
				&& (stamp= tokenizer.nextToken()) != null) {
			stamp =stamp.replaceAll("\"", "");
	        stamp =stamp.replaceAll("'", "");
			emailData.setStamp(stamp);
		}  
        String clientCode=null;
        if (tokenizer.hasMoreTokens()
				&& (clientCode= tokenizer.nextToken()) != null) {
			clientCode =clientCode.replaceAll("\"","");
	        clientCode =clientCode.replaceAll("'", "");
			emailData.setClientCode(clientCode);
		}    
        String replace1 = null;
		if (tokenizer.hasMoreTokens()
				&& (replace1 = tokenizer.nextToken()) != null) {
			replace1 = replace1.replaceAll("\"", "");
			replace1 = replace1.replaceAll("'", "");
			emailData.setReplace1(replace1);
		}
        String replace2 = null;
		if (tokenizer.hasMoreTokens()
				&& (replace2 = tokenizer.nextToken()) != null) {
			replace2 = replace2.replaceAll("\"", "");
			replace2 = replace2.replaceAll("'", "");
			emailData.setReplace2(replace2);
		}
        String replace3 = null;
		if (tokenizer.hasMoreTokens()
				&& (replace3 = tokenizer.nextToken()) != null) {
			replace3 = replace3.replaceAll("\"", "");
			replace3 = replace3.replaceAll("'", "");
			emailData.setReplace3(replace3);
		}         
        return emailData;
	}

	private void saveFile(String filePath, List dataList) throws IOException {
		FileWriter fileWriter = new FileWriter(filePath);
		CSVWriter writer = new CSVWriter(fileWriter);
		writer.writeAll(dataList);
		writer.close();
		fileWriter.close();
	}
	
	public  boolean appendDataToFile(String filePath,List<EmailData> dataList){
		CSVWriter writer=null;
		try {
		writer=new CSVWriter(new FileWriter(filePath,true));
		writer.writeAll(getEmailListRows(dataList));
		writer.close();
		return true;
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		return false;
	}finally{
		try{
			if(writer==null)writer.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	}
	
	private List getEmailListRows(List<EmailData>emailDataList){
		List dataList=new ArrayList();
		if (emailDataList!= null) {
			Iterator<EmailData> itr=emailDataList.iterator();
		    while(itr.hasNext()){
		     EmailData emailData=itr.next();
			 String[] dataRow = {emailData.getName(),
							emailData.getEmailId(),emailData.getStamp(),emailData.getClientCode(),
							emailData.getReplace1(),
							emailData.getReplace2(),emailData.getReplace3()};
					dataList.add(dataRow);
				}
		}
	return dataList;
	}

	public boolean addData(String csvFileName, EmailData data) {
		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(csvFileName));
			List emailList = reader.readAll();
			reader.close();
			addNewDataToExisitingList(emailList, data);
			saveFile(csvFileName, emailList);
		} catch (IOException e) {
			LoggerUtil.doLog(e.getMessage());
			return false;
		}
		return true;
	}

	public boolean addDataAtEnd(String csvFileName, EmailData data) {
		try {
			java.io.RandomAccessFile csvfile = new java.io.RandomAccessFile(
					csvFileName, "rw");
			String line = data.getName() + "," + data.getEmailId()
					+ System.getProperty("line.separator");
			csvfile.seek(csvfile.length());
			csvfile.writeBytes(line);
			csvfile.close();

		} catch (IOException e) {
			System.out
					.println("-------------Exception in method addDataAtEnd CSVFileRepository.java---------------");
			e.printStackTrace();
			System.out
					.println("------------------------------------------------------------------------------------");
			return false;
		}
		return true;
	}

	private void addNewDataToExisitingList(List emailList, EmailData data) {
		String[] dataRow = { data.getName(), data.getEmailId() };
		emailList.add(dataRow);
	}

	private List getDataStringList(EmailData[] data) {
		List stringDataList = new ArrayList();
		if (data != null) {
			for (int i = 0; i < data.length; i++) {
				EmailData emailData = data[i];
				if (emailData != null) {
					String[] dataRow = { emailData.getName(),
							emailData.getEmailId() };
					stringDataList.add(dataRow);
				}
			}
		}
		return stringDataList;
	}

	private EmailData[] getEmailDataList(List dataList) {
		List emaildataList = new ArrayList();
		for (Iterator iterator = dataList.iterator(); iterator.hasNext();) {
			String[] data = (String[]) iterator.next();
			EmailData emailData = new EmailData();
            if (data.length >= 2) {
				emailData.setName(data[0]);
				emailData.setEmailId(data[1]);
				emaildataList.add(emailData);
			} else if (data.length == 1) {
				if (!"".equals(data[0])) {
					emailData.setEmailId(data[0]);
					emaildataList.add(emailData);
				}
            }
		}
        return (EmailData[]) emaildataList.toArray(new EmailData[emaildataList
				.size()]);
	}
}
