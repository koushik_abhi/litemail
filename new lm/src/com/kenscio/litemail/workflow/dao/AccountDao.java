package com.kenscio.litemail.workflow.dao;

import com.kenscio.litemail.exception.NoAccountFoundException;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.BaseRepository;
import com.kenscio.litemail.workflow.common.MoneyValue;
import com.kenscio.litemail.workflow.domain.AccountCredit;
import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.domain.ListEntity;
import com.kenscio.litemail.workflow.domain.PartnerEntity;
import com.kenscio.litemail.workflow.domain.RoleEntity;
import com.kenscio.litemail.workflow.domain.SmtpEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import java.util.Iterator;
import java.util.List;




import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.DetachedCriteria;


public class AccountDao extends BaseRepository {
   
    public AccountEntity getAccount(Long accountId) {
    	
	   DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
       criteria.add(Expression.eq("accountId",accountId));
       List accountList = getHibernateTemplate().findByCriteria(criteria);
       if( accountList.size() != 0 )
    	   return (AccountEntity)accountList.get(0);
       return null;
   	
    }
    
    public void deleteUserFromCredit(String userId) {
		AccountCredit ac= getAccontCreditsOnly(userId);
    		if(ac != null)
    		delete(ac);
		
	}
    
    public AccountEntity getAccount(String username) {
    	
 	   DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
        criteria.add(Expression.eq("userName",username));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        if( accountList.size() != 0 )
     	   return (AccountEntity)accountList.get(0);
        return null;
    	
     }     
    
    public List<AccountEntity> getAllAccountsForPartner(String partner){
    	
		DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
    	criteria.add(Expression.eq("partnerName",partner));
		return getHibernateTemplate().findByCriteria(criteria); 
                  
    	
	}
    
    public List<AccountEntity> getAllAccountsForCredits(){
    	
		DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
    	return getHibernateTemplate().findByCriteria(criteria); 
                  
    	
	}
	
       
    public List<AccountEntity> getAllAccountsInPagination(int pagenumber,
			int noOfRecords) {
    	
		DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
		int index = (pagenumber - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria, index, noOfRecords); 
                  
    	
	}
	
    public void deletePartner(String partnerName) {
    	PartnerEntity ae= getPartner(partnerName);
    	delete(ae);
		
	}

	public List<PartnerEntity> getAllPartnersInPagination(int pagenumber,
			int noOfRecords) {
		DetachedCriteria criteria = DetachedCriteria.forClass(PartnerEntity.class);
		int index = (pagenumber - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria, index, noOfRecords); 
	}
	
	public List<AccountCredit> getAllUserCreditsInPagination(int pagenumber,
			int noOfRecords, List<String> loginIds) {
		DetachedCriteria criteria = DetachedCriteria.forClass(AccountCredit.class);
		criteria.add(Expression.in("username",loginIds));
		int index = (pagenumber - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria, index, noOfRecords); 
	}

	public void addPartner(PartnerEntity accountEntity) {
		save(accountEntity);
		
	}

	public void updatePartner(PartnerEntity accountEntity) {
		 update(accountEntity);
		
	}

    public void saveAccount(AccountEntity account) {
        
        save(account);
    }
    
    public void updateAccount(AccountEntity account) {
        
    	save(account);
    }
    
    public void deleteAccount(String loginId) {
        
    	AccountEntity ae= getAccountByLogin(loginId);
    	if(ae != null)
    	delete(ae);
    }
    
    public void deleteAccount(long accountId) {
        
    	AccountEntity ae= getAccount(accountId);
    	if(ae != null)
    	delete(ae);
    }
    
    public void saveAccountCredits(AccountCredit account) {
        
        save(account);
    }
    
    public AccountCredit getAccontCredits(long accountId) {
        DetachedCriteria criteria = DetachedCriteria.forClass(AccountCredit.class);
        criteria.add(Expression.eq("accountId",accountId));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        return accountList.size() != 0 ? (AccountCredit)accountList.get(0) : null;
    }    
    
    public AccountCredit getAccontCredits(String login) {
    	AccountEntity ac = getAccountByLogin(login);
    	long acntId = 0;
    	if(ac != null)
    	{
    		acntId = ac.getAccountId();
    	}
        DetachedCriteria criteria = DetachedCriteria.forClass(AccountCredit.class);
        criteria.add(Expression.eq("accountId",acntId));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        return accountList.size() != 0 ? (AccountCredit)accountList.get(0) : null;
    }  
    
    public AccountCredit getAccontCreditsOnly(String login) {
    	DetachedCriteria criteria = DetachedCriteria.forClass(AccountCredit.class);
        criteria.add(Expression.eq("username",login));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        return accountList.size() != 0 ? (AccountCredit)accountList.get(0) : null;
    }    
    
    public void increaseUserCredits(String userName, long credits, String login) {
    	AccountCredit ac = getAccontCreditsOnly(userName);
    	long balance = ac.getBalance();
    	long total = balance+credits;
    	ac.setBalance(total);
    	ac.setCredits(credits);
    	ac.setTransType("increase user account credits");
    	ac.setUpdatedDate(new Date());
    	ac.setUpdatedBy(login);
    	update(ac);
		
	}
    
    public void decreaseUserCredits(String userName, long debits, String login) {
    	AccountCredit ac = getAccontCreditsOnly(userName);
    	long balance = ac.getBalance();
    	long total = balance-debits;
    	ac.setBalance(total);
    	ac.setDebits(debits);
    	ac.setTransType("decrease user account balance");
    	ac.setUpdatedDate(new Date());
    	ac.setUpdatedBy(login);
    	update(ac);
		
	}
    
    public AccountCredit[] getAllLatestAccountCredits() {
        
    	List stampitAccounts = getHibernateTemplate().find("from AccountCredit stampitAccount "+
                "order by stampitAccount.createdDate desc");
    	return(AccountCredit[])stampitAccounts.toArray(new AccountCredit[stampitAccounts.size()]);
    	
    }   
    
    public RoleEntity[] getAllRoles() {
        
    	List stampitAccounts = getHibernateTemplate().find("from RoleEntity stampitAccount "+
                "order by stampitAccount.createdDate desc");
    	return(RoleEntity[])stampitAccounts.toArray(new RoleEntity[stampitAccounts.size()]);
    	
    }          
    
    public Long doLogin(String userName, String password) {
        DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
        criteria.add(Expression.eq("loginId",userName));
        criteria.add(Expression.eq("password",password));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        return accountList.size() != 0 ? ((AccountEntity)accountList.get(0)).getAccountId() : null;
    }    
    
    public AccountEntity getAccountByLogin(String login) {
        DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
        criteria.add(Expression.eq("loginId",login));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        if( accountList.size() != 0 )
    	   return (AccountEntity)accountList.get(0);
    	
        return null;
     }
     
    public boolean isRegisteredAccount(String email) {
        DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
        criteria.add(Expression.eq("loginId",email));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        return accountList.size() != 0 ? true : false;
     }

    public AccountEntity getAccountByEmail(String primaryEmail) throws NoAccountFoundException {
    	AccountEntity account = null;
        
        try{
        DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
        criteria.add(Expression.eq("loginId",primaryEmail));
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        account = (AccountEntity) accountList.get(0);

        } catch(Exception e){
          e.printStackTrace();
          throw new NoAccountFoundException("No account found with primary email "+primaryEmail);
        }
        return account;
    }
    
   
    public AccountEntity[] getAllAccounts(){
        List stampitAccounts = getHibernateTemplate().find("from AccountEntity stampitAccount "+
                                                                   "order by stampitAccount.createdData desc");
        return(AccountEntity[])stampitAccounts.toArray(new AccountEntity[stampitAccounts.size()]);
    }

    public List<PartnerEntity> getAllPartners() {
 	    DetachedCriteria criteria = DetachedCriteria.forClass(PartnerEntity.class);
        List accountList = getHibernateTemplate().findByCriteria(criteria);
        return accountList;
 	}
    
	public PartnerEntity getPartner(long partnerId) {
	   DetachedCriteria criteria = DetachedCriteria.forClass(PartnerEntity.class);
       criteria.add(Expression.eq("id",partnerId));
       List accountList = getHibernateTemplate().findByCriteria(criteria);
       if( accountList.size() != 0 )
    	   return (PartnerEntity)accountList.get(0);
       return null;
	}
	
	public PartnerEntity getPartner(String partner) {
	   DetachedCriteria criteria = DetachedCriteria.forClass(PartnerEntity.class);
       criteria.add(Expression.eq("name",partner));
       List accountList = getHibernateTemplate().findByCriteria(criteria);
       if( accountList.size() != 0 )
    	   return (PartnerEntity)accountList.get(0);
       return null;
	}

	public boolean checkPartnerName(String name) {
		PartnerEntity pe = getPartner(name);
		if(pe != null)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	public void addSmtpServer(SmtpEntity smtpEntity) {
		saveOnly(smtpEntity);
		
	}
	
	public void updateSmtpServer(SmtpEntity smtpEntity) {
		update(smtpEntity);
		
	}
	public void deleteSmtpServer(String smtpServerName) {
		SmtpEntity smtp = getSmtpServer(smtpServerName);
		if(smtp != null)
		delete(smtp);
		
	}
	public SmtpEntity getSmtpServer(String smtpServerName) {
	   DetachedCriteria criteria = DetachedCriteria.forClass(SmtpEntity.class);
       criteria.add(Expression.eq("smtpServerName",smtpServerName));
       List accountList = getHibernateTemplate().findByCriteria(criteria);
       if( accountList.size() != 0 )
    	   return (SmtpEntity)accountList.get(0);
       return null;
	}
	public SmtpEntity[] getAllSmtpServers() {
		List stampitAccounts = getHibernateTemplate().find("from SmtpEntity stampitAccount "+
                "order by stampitAccount.createdDate desc");
    	return(SmtpEntity[])stampitAccounts.toArray(new SmtpEntity[stampitAccounts.size()]);
       
	}

	public boolean isUserExist(String loginId) {
	   DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
       criteria.add(Expression.eq("loginId",loginId));
       List accountList = getHibernateTemplate().findByCriteria(criteria);
       if( accountList.size() >  0 )
    	   return true;
       return false;
	}

	public boolean isPartnerInUse(long accountId, String partnerName) {
	   DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
       criteria.add(Expression.eq("partnerName",partnerName));
       List accountList = getHibernateTemplate().findByCriteria(criteria);
       if( accountList.size() >  0 )
    	   return true;
       return false;

	}

	public void updateUserPassword(String newpassword, String username) {
		AccountEntity ae = getAccount(username);
		ae.setPassword(newpassword);
		if(ae != null)
		update(ae);
		
	}
	
	public int getTotalPagesOfUsers(long accountId) {
		String hql = "select count(accountId) from AccountEntity ";  
		List list=getHibernateTemplate().find(hql);
		if(list != null && list.size() > 0)
		{
			return (Integer)list.get(0);
		}
		
		return 0;
		
	}
	

	
	public int getTotalPagesOfPatners() {
		String hql = "select count(pid) from PartnerEntity ";
		List list=getHibernateTemplate().find(hql);
		if(list != null && list.size() > 0)
		{
			return (Integer)list.get(0);
		}
		
		return 0;
	}
	
	public int getTotalPagesOfCredits(long accountId) {
		String hql = "select count(accountId) from AccountCredit ";  
		List list=getHibernateTemplate().find(hql);
		if(list != null && list.size() > 0)
		{
			return (Integer)list.get(0);
		}
		
		return 0;
		
	}

	public String getPartnerName(long accountId) {
		  DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
	       criteria.add(Expression.eq("accountId",accountId));
	       List<AccountEntity> accountList = getHibernateTemplate().findByCriteria(criteria);
	       String partnerName=null;
	       if( accountList.size() >  0 )
	       {
	    	   partnerName=accountList.get(0).getPartnerName();
	       }
		return partnerName;
	}
	  
	   

}
