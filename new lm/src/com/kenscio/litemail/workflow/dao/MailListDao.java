package com.kenscio.litemail.workflow.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.hibernate.Query;
import org.hibernate.Session;

import au.com.bytecode.opencsv.CSVWriter;




import com.kenscio.litemail.workflow.common.BaseRepository;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.DomainCount;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.domain.ListDataEntity;
import com.kenscio.litemail.workflow.domain.ListEntity;

public class MailListDao extends BaseRepository
{
	
	public void addAttributeDataForList(ListDataEntity data)
	{
		saveOnly(data);
		
		
	}
	public void addEmailDataForList(EmailData data) {
		save(data);
		
	}
	
	public void addAttributeDataListForList(List<ListDataEntity> attrData)
	{
		for(ListDataEntity data : attrData)
		saveOnly(data);
		
	}
	public void deleteAttributeDataForList(long listId, List<String> emilids)
	{
		 DetachedCriteria criteria = DetachedCriteria.forClass(ListDataEntity.class);
		 criteria.add(Expression.eq("listId",listId));
		 criteria.add(Expression.in("reciepientEmail",emilids));
	     List attrList = getHibernateTemplate().findByCriteria(criteria);
	     getHibernateTemplate().deleteAll(attrList);
		
	}
	
	public void deleteAllAttributeDataForList(long listId, List<String> emilids)
	{
		 DetachedCriteria criteria = DetachedCriteria.forClass(ListDataEntity.class);
		 criteria.add(Expression.eq("listId",listId));
		 criteria.add(Expression.in("reciepientEmail",emilids));
	     List attrList = getHibernateTemplate().findByCriteria(criteria);
	     getHibernateTemplate().deleteAll(attrList);
	     	     
	     DetachedCriteria criteria1 = DetachedCriteria.forClass(EmailData.class);
		 criteria1.add(Expression.eq("listId",listId));
		 criteria1.add(Expression.in("email",emilids));
	     List attrList1 = getHibernateTemplate().findByCriteria(criteria1);
	     getHibernateTemplate().deleteAll(attrList1);
		
	}
	
	public void deleteAttributeDataForList(long listId, String emilids) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(ListDataEntity.class);
		 criteria.add(Expression.eq("listId",listId));
		 criteria.add(Expression.eq("reciepientEmail",emilids));
	     List attrList = getHibernateTemplate().findByCriteria(criteria);
	     getHibernateTemplate().deleteAll(attrList);
		
	}
	public List getAttributeDataForList(long listId, String email)
	{
		 DetachedCriteria criteria = DetachedCriteria.forClass(ListDataEntity.class);
		 criteria.add(Expression.eq("listId",listId));
		 criteria.add(Expression.eq("reciepientEmail",email));
	     List attrList = getHibernateTemplate().findByCriteria(criteria);
	     return attrList;
		
	}
	
	public List getAttributeDataForListGraph(long listId, String attribute)
	{
		 String sql  ="";
		 if(attribute.equalsIgnoreCase("email"))
		 {
			  sql  = "select reciepientEmail, count(reciepientEmail) from ListDataEntity where listId ="+ listId + " group by reciepientEmail ";
		 }else
		 {
			  sql  = "select attributeName, count(attributeName) from ListDataEntity where listId ="+ listId + " and attributeName = '"+attribute+"' group by attributeName ";
		 
		 }
		/* DetachedCriteria criteria = DetachedCriteria.forClass(ListDataEntity.class);
		 criteria.add(Expression.eq("listId",listId));
		 criteria.add(Expression.eq("attributeName",attribute));
		 criteria.add(Expression.eq("attributeName",attribute));
		 criteria.setProjection(Projections.projectionList()
                 .add(Projections.groupProperty("attributeName"))
                 .add(Projections.count("attributeName"))           
         );*/
		 List<ListDataEntity> lds = new ArrayList<ListDataEntity>();
		 List<Object[]> attrList = getHibernateTemplate().find(sql);
		 
		 if(attrList != null)
		 {
			 for(Object[] obj : attrList)
			 {
				
				 ListDataEntity ld = new ListDataEntity();
				 if(obj[0] != null && obj[1] != null) 
				 {
					 String name = (String)obj[0];
					 int cont = (Integer)obj[1];
					 ld.setAttributeValue(name);
					 ld.setAttributeCount(cont);
					 lds.add(ld);
				 }
			 }
		 }
	     return lds;
		
	}
	public ListDataEntity getAttributeDataForListAttribute(long listId, String email, String attrName)
	{
		 DetachedCriteria criteria = DetachedCriteria.forClass(ListDataEntity.class);
		 criteria.add(Expression.eq("listId",listId));
		 criteria.add(Expression.eq("reciepientEmail",email));
		 criteria.add(Expression.eq("attributeName",attrName));
	     List attrList = getHibernateTemplate().findByCriteria(criteria);
	     if(attrList != null && attrList.size() > 0)
	     {
	    	 return (ListDataEntity)attrList.get(0);
	     }
	     return null;
		
	}

	public boolean isEmailExistInCampaignList(long listId, String email) {
		 DetachedCriteria criteria = DetachedCriteria.forClass(ListDataEntity.class);
		 criteria.add(Expression.eq("listId",listId));
		 criteria.add(Expression.eq("reciepientEmail",email));
	     List attrList = getHibernateTemplate().findByCriteria(criteria);
	     if(attrList != null && attrList.size() > 0)
	    	 return true;
	     
	     return false;
	}

	public void addNewEmailNameTolist(long listId, String email, String name) {
		ListDataEntity list = new ListDataEntity();
		list.setReciepientEmail(email);
		list.setReciepientName(name);
		list.setListId(listId);
		List exirec = getAttributeDataForList(listId, email);
		if(exirec != null && exirec.size() > 0)
		{
			//don;t add it as it is already there
		}else
		{
			saveOnly(list);
		}
		
	}
	
	public List<EmailData> getListDataEmailsPagination(int pagenumber , int noOfRecords, long listId) {
		
		DetachedCriteria criteria = DetachedCriteria.forClass(EmailData.class);
    	criteria.add(Expression.eq("listId", listId));
    	int index = (pagenumber - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria, index, noOfRecords); 
		
      
	}
	
	public List<ListEntity> getAllListsInPagination(int pagenumber,
			int noOfRecords, long accountId) {
		
		DetachedCriteria criteria = DetachedCriteria.forClass(ListEntity.class);
    	criteria.add(Expression.eq("accountId", accountId));
    	criteria.add(Expression.eq("deleteListFlag", false));
    	criteria.addOrder(Order.desc("listId"));
    	int index = (pagenumber - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria, index, noOfRecords); 
		
		
	}
	
	public void addNewMailingList(ListEntity listData)
	{
		saveOnly(listData);
	}
	public List<ListEntity> getListDetail(long listId) {
		DetachedCriteria criteria=DetachedCriteria.forClass(ListEntity.class);
		criteria.add(Expression.eq("listId", listId));
		List listData=getHibernateTemplate().findByCriteria(criteria);
		return listData;
	}
	
	public void updateMailingList(ListEntity listData)
	{
		
		update(listData);
	}
	
	
	public void updateRecordAndStatusOfList(ListEntity listData)
	{
		long records=listData.getTotalRecords();
		long invalidRecords=listData.getInvalidRecords();
		long listId=listData.getListId();
		String status=listData.getStatus();
		ListEntity stampitList= (ListEntity) get(ListEntity.class, listId);
		stampitList.setTotalRecords(records);
		stampitList.setInvalidRecords(invalidRecords);
		stampitList.setStatus(status);
		merge(stampitList);
	}
	
	public void deleteMailingList(long listId,long accountId)
	{
		ListEntity stampitList= (ListEntity) get(ListEntity.class, listId);
		stampitList.setDeleteListFlag(true);
		update(stampitList);
	}
	public boolean isMailingListExist(long accountId,String listName)
	{
		DetachedCriteria criteria=DetachedCriteria.forClass(ListEntity.class);
		criteria.add(Expression.eq("accountId", accountId));
		criteria.add(Expression.eq("listName", listName));
		List exilist= getHibernateTemplate().findByCriteria(criteria);
		if(exilist !=null && exilist.size() > 0)
		{	return true;
		}else
		{
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public long getListIdOnName(long accountId,String listName)
	{
		long listId=0l;
		DetachedCriteria criteria=DetachedCriteria.forClass(ListEntity.class);
		criteria.add(Expression.eq("accountId", accountId));
		criteria.add(Expression.eq("listName", listName));
		List<ListEntity> exilist= (List<ListEntity>)getHibernateTemplate().findByCriteria(criteria);
		for(int i=0;i<exilist.size();i++)
		{
			listId= exilist.get(i).getListId();
		}
		return listId;
	}
	
	public List<ListEntity> getAllLists(long accountId)
	{
		DetachedCriteria criteria=DetachedCriteria.forClass(ListEntity.class);
		criteria.add(Expression.eq("accountId",accountId));
		criteria.add(Expression.eq("deleteListFlag", false));
		List<ListEntity> stampitLists= getHibernateTemplate().findByCriteria(criteria);
		return stampitLists;
	}
	
	public List<ListEntity> getListDetailsForUserAndlistname(long accountId,String listName)
	{
		DetachedCriteria criteria=DetachedCriteria.forClass(ListEntity.class);
		criteria.add(Expression.eq("accountId", accountId));
		criteria.add(Expression.eq("listName", listName));
		List<ListEntity> allListData= getHibernateTemplate().findByCriteria(criteria);	
		return allListData;
		
	}
	
	public List<ListEntity> searchList(long accountId,String listName)
	{
		DetachedCriteria criteria=DetachedCriteria.forClass(ListEntity.class);
		criteria.add(Expression.eq("accountId", accountId));
		//criteria.add(Expression.("listName", listName));
		criteria.add(Expression.eq("deleteListFlag", false));
		criteria.add(Restrictions.like("listName", listName + "%"));
         
		List<ListEntity> searchResult= getHibernateTemplate().findByCriteria(criteria);	
		System.out.println("searchResult "+ searchResult.size());
		return searchResult;
	}
	
	public void deleteListData(long stampitListId)
	{
		DetachedCriteria criteria=DetachedCriteria.forClass(ListEntity.class);
		criteria.add(Expression.eq("listId", stampitListId));
		List listData=getHibernateTemplate().findByCriteria(criteria);
		if(listData != null && listData.size() > 0)
		{
			ListEntity stampitList = (ListEntity)listData.get(0);
			stampitList.setDeleteListFlag(true);
			merge(stampitList);
		}
	}
	
	public long getListRecordCount(long accountId,String listName)
	{
		long records=0l;
		DetachedCriteria criteria=DetachedCriteria.forClass(ListEntity.class);
		criteria.add(Expression.eq("accountId", accountId));
		criteria.add(Expression.eq("listName", listName));
		List<ListEntity> listData=getHibernateTemplate().findByCriteria(criteria);
		if(listData != null && listData.size() > 0)
		{
			records= listData.get(0).getTotalRecords();
		}
		return records;
	}
	
	public long getAllRecordCountForList(long accountId,long listId)
	{
		long records=0l;
		String hql = "select count(email) from EmailData where listId =" + listId;  
		List list=getHibernateTemplate().find(hql);
		if(list != null && list.size() > 0)
		{
			records= (Long)list.get(0);
		}
		return records;
	}
	
	public long getListInvalidRecordCount(long accountId,String listName)
	{
		long records=0l;
		DetachedCriteria criteria=DetachedCriteria.forClass(ListEntity.class);
		criteria.add(Expression.eq("accountId", accountId));
		criteria.add(Expression.eq("listName", listName));
		List<ListEntity> listData=getHibernateTemplate().findByCriteria(criteria);
		if(listData != null && listData.size() > 0)
		{
			records= listData.get(0).getInvalidRecords();
		}
		return records;
	}
	public DomainCount getCountForDomain(long listId, String domainName) {
		
		DetachedCriteria criteria=DetachedCriteria.forClass(DomainCount.class);
		criteria.add(Expression.eq("listId", listId));
		criteria.add(Expression.eq("domainName", domainName));
		List<DomainCount> allListData= getHibernateTemplate().findByCriteria(criteria);
		if(allListData != null && allListData.size() > 0)
		{
			return allListData.get(0);
		}
		return null;
		
	}
	
	public Map<String, Long> getDomainCountMapForGraph(long listId) {
		
		 Map<String, Long> map = new HashMap<String, Long>();
		DetachedCriteria criteria=DetachedCriteria.forClass(DomainCount.class);
		criteria.add(Expression.eq("listId", listId));
		List<DomainCount> allListData= getHibernateTemplate().findByCriteria(criteria);
		if(allListData != null && allListData.size() > 0)
		{
			for(DomainCount dc: allListData)
			{
				map.put(dc.getDomainName(), dc.getNoOfEmails());
			}
			
		}
		return map;
		
	}
	public void updateDomainCount(DomainCount dcount) {
        save(dcount);
        
	}
	public List<EmailData> getEmailDataForList(Long listId,
			long fileSeeker, long noOfRecords) {
		
		DetachedCriteria criteria = DetachedCriteria.forClass(EmailData.class);
    	criteria.add(Expression.eq("listId", listId));
    	long index = (fileSeeker - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria, (int)index, (int)noOfRecords); 
		
	}
	public List getAllAvailableListNames(long accountId) {
		String hql = "select listName from ListEntity where accountId =" + accountId + " and deleteListFlag ="+ 0;  
		List list=getHibernateTemplate().find(hql);
		return list;
	}
	
		
	public boolean isEmailExistForUpdate(long listId, String em) {
		return isEmailExistInCampaignList(listId, em);
	}
	
	public int getTotalPagesOfList(String partnerName) {
		String hql = "select count(listId) from ListEntity where partnerName ='" + partnerName + "' and deleteListFlag ="+ 0;  
		List list=getHibernateTemplate().find(hql);
		if(list != null && list.size() > 0)
		{
			System.out.println(list.get(0) + "total list form mail list dao");
			return (Integer)list.get(0);
		}
		
		return 0;
		
	}
	public void exportListDataToExcel(CSVWriter writer, List<String> headers,
			String listName, long accountId) {
		Session session = null;
		try{
		long listId = getListIdOnName(accountId, listName);
		
		if(listId > 0)
		{
						
			String hql = "select emailId from EmailData where listId =" + listId ;
			session = getSession();
			Query query =session.createQuery(hql);
			Iterator iter = query.iterate();
			while(iter.hasNext())
			{
				String email = (String)iter.next();
				List<String> emaildata = getEmailDataForList(headers, email, listId);
				String [] earray = emaildata.toArray(new String[emaildata.size()]);
			    writer.writeNext(earray);    
			}
			
		}
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally
		{
			if(session != null)
			releaseSession(session); 
		}
		
		
	}
	
	private List<String> getEmailDataForList(List<String> headers,
			String email, long listId) 
	{
		List<String> data = new ArrayList<String>();
		data.add(email);
		DetachedCriteria criteria=DetachedCriteria.forClass(ListDataEntity.class);
		criteria.add(Expression.eq("listId", listId));
		criteria.add(Expression.eq("reciepientEmail", email));
		criteria.add(Expression.isNotNull("attributeName"));
		List<ListDataEntity> allListData= getHibernateTemplate().findByCriteria(criteria);
		if(allListData != null && allListData.size() > 0)
		{
			for(String str : headers)
			{
				String value = getValueFromEntity(str, allListData);
				if(value != null)
					data.add(value);
				else
					data.add("");
			}
			
		}
		return data;
		
		
	}
	private String getValueFromEntity(String str,
			List<ListDataEntity> allListData) {
		for(ListDataEntity ld : allListData)
		{
			if(ld.getAttributeName().equalsIgnoreCase(str))
			{
				return ld.getAttributeValue();
			}
		}
		return "";
	}
		public List<ListEntity> getAllListsInPagination(int pagenumber,
			int noOfRecords, String partnerName) {
		
		DetachedCriteria criteria = DetachedCriteria.forClass(ListEntity.class);
    	criteria.add(Expression.eq("partnerName", partnerName));
    	criteria.add(Expression.eq("deleteListFlag", false));
    	criteria.addOrder(Order.desc("listId"));
    	int index = (pagenumber - 1) * noOfRecords;
		return getHibernateTemplate().findByCriteria(criteria, index, noOfRecords); 
		
		
	}
}
