package com.kenscio.litemail.workflow.dao;

import com.kenscio.litemail.workflow.common.BaseRepository;

import com.kenscio.litemail.workflow.domain.ActionLogObject;


import java.util.List;



import org.hibernate.criterion.Expression;
import org.hibernate.criterion.DetachedCriteria;

public class ActionDaoImpl extends BaseRepository {
   
    public ActionLogObject getActionDetails(Long id) {
        return (ActionLogObject)get(ActionLogObject.class,id);
    }

    public void saveAction(ActionLogObject action) {
        
        save(action);
    }
    
    public void updateAction(ActionLogObject action) {
        
        merge(action);
    }
    
    public void deleteAction(long id) {
        
    	ActionLogObject ae= getActionDetails(id);
    	delete(ae);
    }
    public ActionLogObject getActionLogObject(long listId)
    { 
    	String hsql  = "select * from ActionLogObject where listId = "+ listId+ " order by id desc limit 1 ";
		List accountList = getHibernateTemplate().find(hsql);
	    return accountList.size() != 0 ? (ActionLogObject)accountList.get(0) : null;
    }
     
    public ActionLogObject[] getAllActionLogObjects(){
        List stampitAccounts = getHibernateTemplate().find("from ActionLogObject stampitAccount "+
                                                                   "order by stampitAccount.id desc");
        return(ActionLogObject[])stampitAccounts.toArray(new ActionLogObject[stampitAccounts.size()]);
    }

	public ActionLogObject getActionLogOnId(long actionId) {
		DetachedCriteria criteria = DetachedCriteria.forClass(ActionLogObject.class);
	    criteria.add(Expression.eq("id",actionId));
	    List accountList = getHibernateTemplate().findByCriteria(criteria);
	    return accountList.size() != 0 ? (ActionLogObject)accountList.get(0) : null;
	}

	public List<ActionLogObject> getActionLogsOnUser(String userId) {
		// TODO Auto-generated method stub
		return null;
		
	}

	public void deleteactionlog(List<String> deleteList, String userName) {
		// TODO Auto-generated method stub
		
	}

	public ActionLogObject getActionLogByActionSource(String actionSource) {
		DetachedCriteria criteria = DetachedCriteria.forClass(ActionLogObject.class);
	    criteria.add(Expression.eq("actionSource",actionSource));
	    List accountList = getHibernateTemplate().findByCriteria(criteria);
	    return accountList.size() != 0 ? (ActionLogObject)accountList.get(0) : null;
	}
	
	public int getTotalPagesOfActions(long accountId) {
		String hql = "select count(id) from ActionLogObject ";  
		List list=getHibernateTemplate().find(hql);
		if(list != null && list.size() > 0)
		{
			return (Integer)list.get(0);
		}
		
		return 0;
		
	}

  
	   

}
