package com.kenscio.litemail.workflow.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;




import com.kenscio.litemail.workflow.common.BaseRepository;
import com.kenscio.litemail.workflow.domain.AccountEntity;

public class LoginDao extends BaseRepository {

	@SuppressWarnings("unchecked")
	public List<AccountEntity> getAccounts(String loginId, String password)
	{
		DetachedCriteria criteria = DetachedCriteria.forClass(AccountEntity.class);
	    criteria.add(Expression.eq("loginId",loginId));
	    criteria.add(Expression.eq("password",password));
	    List<AccountEntity> attrList = getHibernateTemplate().findByCriteria(criteria);
	    return attrList;
	}
	
}
