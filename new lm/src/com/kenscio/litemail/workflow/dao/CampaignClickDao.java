package com.kenscio.litemail.workflow.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;


import com.kenscio.litemail.workflow.common.BaseRepository;
import com.kenscio.litemail.workflow.domain.CampaignLinks;

public class CampaignClickDao extends BaseRepository  {

    public void saveStampitClicks(CampaignLinks stampitClicks){
    	saveOnly(stampitClicks);

    }
    
    public void saveStampitClicksUpdate(CampaignLinks stampitClicks){
        merge(stampitClicks);

    }

    public CampaignLinks getStampitClicks(Long urlId){

        return (CampaignLinks) get(CampaignLinks.class,urlId);
    }

    public List getStampitClicksByCampaignId(Long campaignId) {
        //Criteria criteria = getSession().createCriteria(StampitClicks.class);
        DetachedCriteria criteria = DetachedCriteria.forClass(CampaignLinks.class);
        criteria.add(Expression.eq("campaignId",campaignId));
        criteria.add(Expression.eq("currentStatus","active"));
        //List results = criteria.list();
        List results = getHibernateTemplate().findByCriteria(criteria);
        return results;
    }

    public List getStampitClicksByCampaignIdIncludingInactive(Long campaignId) {
        Criteria criteria = getSession().createCriteria(CampaignLinks.class);
        criteria.add(Expression.eq("campaignId",campaignId));
        List results = criteria.list();
        return results;
    }
    
    public CampaignLinks getStampitClicksByUrlIdAndCampaignId(String urlId,Long campaignId){
      //  Criteria criteria = getSession().createCriteria(StampitClicks.class);
        DetachedCriteria criteria = DetachedCriteria.forClass(CampaignLinks.class);
        criteria.add(Expression.eq("urlId",urlId));
        criteria.add(Expression.eq("campaignId",campaignId));
        criteria.add(Expression.eq("currentStatus","active"));
        //List results = criteria.list();
        List results = getHibernateTemplate().findByCriteria(criteria);
        return results.size() != 0 ? (CampaignLinks) results.get(0) : null;
    }

}
