package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.AttributeEntity;
import com.kenscio.litemail.workflow.service.AccountService;
import com.kenscio.litemail.workflow.service.AttributeService;
@SuppressWarnings("unused")
public class AttributeManagementController implements Controller{
	
	@RequestMapping(value = "/displyattributes.htm", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}	
		long accountId = (Long) session.getAttribute("accountId");
		String partnerName = (String) session.getAttribute("partnerName");
		String pagenum = request.getParameter("pagenumber");
		System.out.println(pagenum);
		Integer pageNumber = 1;
		if (pagenum != null) {
			pageNumber = new Integer(pagenum);
		}
		int noOfRecordsperPage = 10;
		float totalRecords = 0;
		ObjectMapper mapper = new ObjectMapper();
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mapper.getDeserializationConfig().setDateFormat(df);
		String jsonString = "";  
		Map<String, Object> finalMap = new HashMap<String, Object>();
		List<AttributeEntity> allCustAttrb= getAttributesByPartnerName(partnerName,pageNumber,noOfRecordsperPage);
		List<AttributeEntity> allstdAttrb=getStdAttributes();
		totalRecords=getTotalCustAttributes(partnerName);
		double totalPages = totalRecords / noOfRecordsperPage;
		totalPages = Math.ceil(totalPages);
		double temp = noOfRecordsperPage*totalPages;
		if(totalRecords > temp)
			totalPages = totalPages+1;
		System.out.println(totalPages);
		finalMap.put("totalpages", totalPages);
		finalMap.put("pagenumber", pageNumber);
		finalMap.put("result", allCustAttrb);
		finalMap.put("result1", allstdAttrb);
		if(allCustAttrb == null || allCustAttrb.size() == 0)
		{
			Map<String, String> map = new HashMap<String, String>();
			map.put("errorMessage", "No attibute records found.");
			jsonString = mapper.writeValueAsString(map);
			
		}else
		{
			jsonString = mapper.writeValueAsString(finalMap);
		}
	
	
		AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
	
	//MappingJacksonHttpMessageConverter jsonConverter = new MappingJacksonHttpMessageConverter();
	
		MediaType jsonMimeType = MediaType.APPLICATION_JSON;
	
		if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType, new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}

		}

		return null;

	}
	
	/*private List<List<AttributeEntity>> getAvailableAttributes(long accountId) {
		AttributeService service= new AttributeService();
		return service.getAttributes(accountId);
	}*/
	
	private int getTotalCustAttributes(String partnerName) {
		AttributeService service= new AttributeService();
		return service.getTotalCustAttributes(partnerName);
	}

	private List<AttributeEntity> getStdAttributes(){
	AttributeService service= new AttributeService();
		return service.getStdAttributes();
	}
	
	private List<AttributeEntity> getAttributesByPartnerName(String partnerName, int pageNumber, int noOfRecordsperPage){
		AttributeService service= new AttributeService();
		return service.getAttributesByPartnerName(partnerName,pageNumber,noOfRecordsperPage);
	}

	
}