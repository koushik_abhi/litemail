package com.kenscio.litemail.workflow.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.util.MailUtil;
import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.domain.SmtpEntity;
import com.kenscio.litemail.workflow.service.SmtpService;

public class UpdateSmtpController implements Controller {

	@Override
	@RequestMapping(value="/updatesmtp.htm", method=RequestMethod.POST)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		String user=(String) session.getAttribute("login");
		long accountId = (Long) session.getAttribute("accountId");
		SmtpEntity entity=getSmtpEntity(request, user);
		entity.setAccountId(accountId);
		if(!validateSmtpSettings(entity))
		{
		 	request.setAttribute("errorMessage", "Please provide valid smtp details");
		}
	    SmtpService service = new SmtpService();
	    service.updateSmtpServer(entity);
		return new ModelAndView("smtpManagement.jsp");
		
	}
	
	private SmtpEntity getSmtpEntity(HttpServletRequest request, String createdBy)
	{
			
		  String servername=request.getParameter("smtpservername");
		  String serverip=request.getParameter("smtpserverip");
		  String username=request.getParameter("smtpusername");
		  String password=request.getParameter("smtppassword");
		  String apiKey=request.getParameter("apikey");	 
		  String partner=request.getParameter("partner");
		  System.out.println("update partners "+ partner);
		  SmtpEntity entity=new SmtpEntity();
		  entity.setSmtpServerName(servername);
		  entity.setPartner(partner);
		  entity.setSmtpServer(serverip);
		  entity.setSmtpUser(username);
		  entity.setSmtpPassword(password);
		  entity.setApiKey(apiKey);
		  entity.setCreatedBy(createdBy);
		  entity.setCreatedDate(new Date());
		  entity.setUpdatedDate(new Date());
		  entity.setUpdatedBy(createdBy);
		 
		  return entity;
	}
		
	private boolean validateSmtpSettings(SmtpEntity smtpEntity)
	{
		boolean authirize= MailUtil.authenticateSMTPServer(smtpEntity.getSmtpServer().trim(),smtpEntity.getSmtpUser().trim(),smtpEntity.getSmtpPassword().trim());
	    return authirize;
	}

	
}