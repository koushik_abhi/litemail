package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.dataservice.platform.persistence.impl.ListDataDaoImpl;
import com.kenscio.litemail.util.Page;
import com.kenscio.litemail.workflow.domain.ListDataEntity;
import com.kenscio.litemail.workflow.service.AttributeService;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;
import com.lowagie.text.Header;

@Controller
public class SubscribersController {

	@RequestMapping(value = "/subsribers.htm", method = RequestMethod.GET)
	public ModelAndView showSubscribepage(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		String listName=request.getParameter("listName");
		request.setAttribute("listName", listName);
		request.setAttribute("listname", listName);
		return new ModelAndView("subscribers.jsp");
	}
	
	@RequestMapping(value = "/getallsubsribers.htm", method = RequestMethod.GET)
	public ModelAndView getAllSubscribers(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		long accountId=(Long) session.getAttribute("accountId");
		String listName=request.getParameter("listName");
		String pagenum =request.getParameter("pagenumber");
		
		int pagenumber = 1;
		double totalPages = 0;
		long recordsPerPage = 15;
		if (pagenum != null)
			pagenumber = Integer.parseInt(pagenum);
		
		MailListServiceImpl impl = new MailListServiceImpl();
		long listId = impl.getListIdOnName(accountId, listName);

		int totalRecords = (int)impl.getListRecordCount(accountId, listId);
		
		totalPages = Math.ceil(totalRecords / recordsPerPage);
		double temp = recordsPerPage*totalPages;
		if(totalRecords > temp)
			totalPages = totalPages+1; 
		List<String> headers=new ArrayList<String>();
		headers.add("Email");
		headers.add("FirstName");
		headers.add("Age");
		headers.add("Gender");
		headers.add("City");
		request.setAttribute("listName", listName);	
		List<List<String>> data= impl.getAllListDataPagination(listId, pagenumber, 15);

		 ObjectMapper mapper = new ObjectMapper();
	       final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			mapper.getDeserializationConfig().setDateFormat(df);
	       String jsonString = "";
			Map<String, Object> finalMap = new HashMap<String, Object>();
			
			AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
			finalMap.put("values", data);
			finalMap.put("headers", headers);
			finalMap.put("totalpages", totalPages);
			finalMap.put("pagenumber", pagenumber);
			
			if(data !=null && data.size() < 0)
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put("errorMessage", "No list records found.");
				
			}else
			{
				 jsonString = mapper.writeValueAsString(finalMap);
				//System.out.println(jsonString);
			}
			
		MediaType jsonMimeType = MediaType.APPLICATION_JSON;

		if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType,
						new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}
		}
		return null;
	}
	
	@RequestMapping(value = "/showeditsubscriber.htm", method = RequestMethod.GET)
	public ModelAndView showEditSubscriber(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		String listName=request.getParameter("listName");
		String email=request.getParameter("email");
		
		request.setAttribute("email", email);
		request.setAttribute("listName", listName);
		return new ModelAndView("editsubscribers.jsp");
	}
	
	@RequestMapping(value = "/deletesubscriber.htm", method = RequestMethod.GET)
	public ModelAndView deleteSubscriber(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		long accountId=(Long) session.getAttribute("accountId");
		
		String listName=request.getParameter("listName");
		String email=request.getParameter("email");
		request.setAttribute("email", email);
		request.setAttribute("listName", listName);
		MailListServiceImpl impl = new MailListServiceImpl();
		long listId = impl.getListIdOnName(accountId, listName);
		impl.deleteSubscriber(listId, email);
		
		return new ModelAndView("subscribers.jsp");
	}

	
	@RequestMapping(value = "/getsubscriber.htm", method = RequestMethod.GET)
	public ModelAndView getSubscriber(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		long accountId=(Long) session.getAttribute("accountId");
		String listName=request.getParameter("listName");
		String email=request.getParameter("email");
		request.setAttribute("email", email);
		request.setAttribute("listName", listName);
		MailListServiceImpl impl = new MailListServiceImpl();
		long listId = impl.getListIdOnName(accountId, listName);
		Map<String, String> stdAttr = impl.getSubscriberDetails(listId, email);
		Map<String, String> custAttr=null;
		try{
			custAttr=impl.getSubscriberCustomDetails(listId,email);
		
		}
		catch(Exception e){
			
		}
				
		session.setAttribute("customAttribute", custAttr);
		session.setAttribute("stdAttribute", stdAttr);
		ObjectMapper mapper = new ObjectMapper();
	       final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			mapper.getDeserializationConfig().setDateFormat(df);
	       String jsonString = "";
			Map<String, Object> finalMap = new HashMap<String, Object>();
			
			AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
			finalMap.put("stdAttr", stdAttr);
			finalMap.put("custAttr", custAttr);
			
			if(custAttr.size() < 0 && stdAttr.size() < 0)
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put("errorMessage", "No attributes found.");
				
			}else
			{
				 jsonString = mapper.writeValueAsString(finalMap);
				System.out.println(jsonString);
			}
			
			MediaType jsonMimeType = MediaType.APPLICATION_JSON;

			if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

				try {

					stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

				} catch (IOException e) {
					e.printStackTrace();

				} catch (HttpMessageNotWritableException e1) {
					e1.printStackTrace();
				}
			}
			return null;
	}
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/editsubscriber.htm", method = RequestMethod.POST)
	public ModelAndView editSubscriber(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		long accountId=(Long) session.getAttribute("accountId");
		Map<String, String> custMap=(Map<String, String>) session.getAttribute("customAttribute");
		Map<String, String> stdMap=(Map<String, String>) session.getAttribute("stdAttribute");
		String listName=request.getParameter("listName");
		String email=request.getParameter("email");
		request.setAttribute("email", email);
		request.setAttribute("listName", listName);
		Map<String, String> stdAttr = new HashMap<String, String>();
		for (String name : stdMap.keySet()) {
			stdAttr.put(name, request.getParameter(name));
		}
		
		Map<String, String> custAttr = new HashMap<String, String>();
		for (String name : custMap.keySet()) {
			custAttr.put(name, request.getParameter(name));
		}				
		MailListServiceImpl impl = new MailListServiceImpl();
		
		long listId = impl.getListIdOnName(accountId, listName);
		impl.updateSubscriberDetails(listId, email, stdAttr);
		
		return new ModelAndView("subscribers.jsp");
	}
	
	public static void main(String a[])
	{
		
		double totalPages = Math.ceil(31 / 15);
	
		System.out.println("" + totalPages);
	}
		
	
	
}
