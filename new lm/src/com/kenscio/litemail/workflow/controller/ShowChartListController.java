package com.kenscio.litemail.workflow.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class ShowChartListController implements Controller {
	
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		String listName = request.getParameter("listname");
		long listId=Long.parseLong(request.getParameter("listId"));
		System.out.println("list id in show charts"+listId);
		request.setAttribute("listName", listName);
		request.setAttribute("listId", listId);
		return new ModelAndView("listreport.jsp");
		}

}
