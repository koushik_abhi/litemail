package com.kenscio.litemail.workflow.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.domain.AttributeEntity;
import com.kenscio.litemail.workflow.service.AttributeService;


public class AddAttributeController implements Controller {

	@RequestMapping(value = "/addattribute.htm", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		Long accountId = (Long) session.getAttribute("accountId");
		String userId=(String) session.getAttribute("login");
		String partnerName = (String) session.getAttribute("partnerName");
		String attrName = request.getParameter("attrname");
		String attrType = request.getParameter("attrtype");
		String initValue = "";
		
		
		if (attrType.equals("Boolean")) {
			initValue = request.getParameter("boolvalue");
		} else {
			initValue = request.getParameter("value");
		}
		
		AttributeEntity attributeEntity=new AttributeEntity();
		
		
	    int stringCount=0;
	    int numCount=0;
	    int datecount=0;
	    int boolCount=0;
	   
	    List<AttributeEntity> attrTest=new ArrayList<AttributeEntity>();  
	    List<String> attrDbName=new ArrayList<String>();
	    List<String> attrDbType=new ArrayList<String>();
	    AttributeService service=new AttributeService();
	   
	    attrTest=service.getCustomAttributes(accountId);
	   
	    for(int i=0;i<attrTest.size();i++)
	    {
		  attrDbName.add(attrTest.get(i).getAttrName());
		  attrDbType.add(attrTest.get(i).getAttrType());
	    }
	    
	    if (service.isAttributeAlreadyExist(attrName,accountId))
		{
			request.setAttribute("errorMessage"," attribute name already exist  "+attrName);
			return new ModelAndView("attributeManagement.jsp");
								
		}
	   
	    if (attrDbName.size() >= 50)
		{
			request.setAttribute("errorMessage","only 50 User Attributes are supported ");
			return new ModelAndView("attributeManagement.jsp");
								
		}
		for (int i = 0; i < attrDbType.size(); i++) 
		{
			if (attrDbType.get(i).equals("String")) {
				stringCount++;

			}
			if (attrDbType.get(i).equals("Numeric")) {
				numCount++;
			}
			if (attrDbType.get(i).equals("Date")) {
				datecount++;
			}
			if (attrDbType.get(i).equals("Boolean")) {
				boolCount++;
			}
		}
		   		

		if (attrDbName.contains(attrName)) {
			request.setAttribute("errorMessage", "Attribute already exist");
			return new ModelAndView("attributeManagement.jsp");
			
		}

		else if ((stringCount == 20 && attrType.equals("String"))
				|| (numCount == 10 && attrType.equals("Numeric"))
				|| (datecount == 10 && attrType.equals("Date"))
				|| (boolCount == 10 && attrType.equals("Boolean"))) {
			request.setAttribute("errorMessage","Cannot add more attribute for selected type");
			return new ModelAndView("attributeManagement.jsp");
		}
			
		attributeEntity.setAttrName(attrName);
		attributeEntity.setAttrType(attrType);
		attributeEntity.setAttrInitialValue(initValue);
		attributeEntity.setAccountId(accountId);
		attributeEntity.setPartnerName(partnerName);
		attributeEntity.setCreatedBy(userId);
		attributeEntity.setCreatedDate(new Date());
		attributeEntity.setUpdatedDate(new Date());
	
		service.addAttribute(attributeEntity);
		return new ModelAndView("attributeManagement.jsp");
		
	}

	
	
}