package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.SmtpEntity;
import com.kenscio.litemail.workflow.service.SmtpService;

public class SmtpDisplayConfigurationsController implements Controller{

	@RequestMapping(value="/smtpDisplayConfigs.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		String prev =request.getParameter("prev");
		String next =request.getParameter("next");
		String pagenum =request.getParameter("pagenumber");
		
		long accountId = (Long) session.getAttribute("accountId");
		  SmtpService serv = new SmtpService();
		  SmtpEntity[] smtps =serv.getAllSmtpServers();
		  ArrayList<SmtpEntity> list = new ArrayList<SmtpEntity>();
		  for(SmtpEntity e : smtps)
		  {
			  list.add(e);
		  }
		  	  
		  
			ObjectMapper mapper = new ObjectMapper();
		       final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				mapper.getDeserializationConfig().setDateFormat(df);
		       String jsonString = "";
				Map<String, Object> finalMap = new HashMap<String, Object>();
				
				int pagenumber=1;
				int totalPages = 1;
				
				AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
				
				request.setAttribute("totalpages", totalPages);
			   	request.setAttribute("pagenumber", pagenumber);
				
			   	finalMap.put("totalpages", totalPages);
				finalMap.put("pagenumber", pagenumber);
				finalMap.put("result", list);
			
				
				if(list.size() <=0)
				{
					Map<String, String> map = new HashMap<String, String>();
					map.put("errorMessage", "No  records found.");
					
				}else
				{
					 jsonString = mapper.writeValueAsString(finalMap);
				}
				
				MediaType jsonMimeType = MediaType.APPLICATION_JSON;

				if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

					try {

						stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

					} catch (IOException e) {
						e.printStackTrace();

					} catch (HttpMessageNotWritableException e1) {
						e1.printStackTrace();
					}
				}
				return null;
	
	}

	
}
