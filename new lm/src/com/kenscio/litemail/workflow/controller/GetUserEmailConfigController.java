package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.domain.EmailObject;
import com.kenscio.litemail.workflow.service.AccountService;
import com.kenscio.litemail.workflow.service.EmailServiceImpl;

@Controller
public class GetUserEmailConfigController {
  
	@RequestMapping(value="/getEamilUserConfig.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException
	{
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		//String userId = request.getParameter("userId");
		long accountId = (Long) session.getAttribute("accountId");
		EmailServiceImpl emailImpl = new EmailServiceImpl();
		//AccountService service = new AccountService();
		//AccountEntity  en = service.getUser(accountId);
		EmailObject emailObject=emailImpl.getEmailConfig();
		System.out.println("got obje" + emailObject.getTo());
		ObjectMapper mapper = new ObjectMapper();
	       final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			mapper.getDeserializationConfig().setDateFormat(df);
	       String jsonString = "";
			Map<String, Object> finalMap = new HashMap<String, Object>();
			
			AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
			finalMap.put("result", emailObject);
		
			
			if(emailObject == null)
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put("errorMessage", "No  records found.");
				
			}else
			{
				 jsonString = mapper.writeValueAsString(finalMap);
				 System.out.println(jsonString);
			}
			
			MediaType jsonMimeType = MediaType.APPLICATION_JSON;

			if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

				try {

					stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

				} catch (IOException e) {
					e.printStackTrace();

				} catch (HttpMessageNotWritableException e1) {
					e1.printStackTrace();
				}
			}
			return null;

}
		
	}
