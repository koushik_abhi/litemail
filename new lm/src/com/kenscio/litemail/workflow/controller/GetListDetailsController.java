package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.ListEntity;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;

public class GetListDetailsController implements Controller{

	@Override
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}	
		String listName =request.getParameter("listname");
		long accountId = (Long) session.getAttribute("accountId");
		String status="created";
		MailListServiceImpl impl = new MailListServiceImpl();
		List<ListEntity> le = impl.getListDetailsForUserAndlistname(accountId, listName);
			
	    ObjectMapper mapper = new ObjectMapper();
	       final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			mapper.getDeserializationConfig().setDateFormat(df);
	       String jsonString = "";
			
			
			AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
				
			
			jsonString = mapper.writeValueAsString(le);
		
			MediaType jsonMimeType = MediaType.APPLICATION_JSON;

			if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

				try {

					stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

				} catch (IOException e) {
					e.printStackTrace();

				} catch (HttpMessageNotWritableException e1) {
					e1.printStackTrace();
				}
			}
			return null;

}

}
