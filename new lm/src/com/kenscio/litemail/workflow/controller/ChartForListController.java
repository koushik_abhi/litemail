package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.service.GraphServiceImpl;

public class ChartForListController implements Controller{

	@Override
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		long accountId=(Long) session.getAttribute("accountId");
		String attrName = request.getParameter("attribute");
		String listName=request.getParameter("listName");
		request.setAttribute("attrname", attrName);
		request.setAttribute("listname", listName);
	   /* GraphServiceImpl impl = new GraphServiceImpl();
		 Map<String, Integer> listGraphData = new HashMap<String, Integer>();
		    Map<String, List> finalMap = new HashMap<String, List>();
		    List<String> data = new ArrayList<String>();
		    List<Integer> number = new ArrayList<Integer>();*/
		    
		    
		    GraphServiceImpl impl = new GraphServiceImpl();
		    Map<String, Integer> listGraphData = new HashMap<String, Integer>();
		    Map<String, List> finalMap = new HashMap<String, List>();
		    List<String> data = new ArrayList<String>();
		    List<Integer> number = new ArrayList<Integer>();
		    if(attrName != null && attrName.length() > 0 )
		    {
		    	listGraphData = impl.getListFieldGraphData(accountId,listName,attrName);
		    	Set<String> keys = listGraphData.keySet();
		    	for(String str: keys)
		    	{
		    		if(!str.equalsIgnoreCase("others") && !str.equalsIgnoreCase("blank"))
		    		{
		    			data.add(str);
		    		}
		    	}
		    	Collections.sort(data);
		    	for(String st : data)
		    	{
		    		number.add(listGraphData.get(st));
		    	}
		    	if(listGraphData.get("others") != null)
		    	{
		    		data.add("others");
		    		number.add(listGraphData.get("others") );
		    	}
		    	if(listGraphData.get("blank") != null)
		    	{
		    		data.add("blank");
		    		number.add(listGraphData.get("blank") );
		    	}
		    	finalMap.put("data", data);
		    	finalMap.put("number", number);
		    	
		    }
		    ObjectMapper mapper = new ObjectMapper();
		    final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			mapper.getDeserializationConfig().setDateFormat(df);
		    String jsonString = mapper.writeValueAsString(finalMap);
		        System.out.println("data in selected attribute option"+jsonString);
		    AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();

		    MediaType jsonMimeType = MediaType.APPLICATION_JSON;
		
		    if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {
		
		        try {
		
		            stringHttpMessageConverter.write(jsonString, jsonMimeType, new ServletServerHttpResponse(response));
		
		        } catch (IOException e) {
		        	e.printStackTrace();
		
		        } catch (HttpMessageNotWritableException e1) {
		        	e1.printStackTrace();
		        }
		
		    }
			return null;

	}
}


