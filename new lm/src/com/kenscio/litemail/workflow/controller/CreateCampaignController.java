package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.common.ZipFileUtil;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.service.CampaignService;
import com.kenscio.litemail.workflow.service.CampaignServiceUtil;
import com.kenscio.litemail.workflow.service.EmailMessageParserService;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;

public class CreateCampaignController  implements Controller {

	@Override
	@RequestMapping(value = "/createcampaign.htm", method = RequestMethod.POST)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		Long accountId=(Long) session.getAttribute("accountId");
		String user=(String) session.getAttribute("login");
		String partnerName=(String) session.getAttribute("partnerName");
		
	  	boolean isMultipart = ServletFileUpload.isMultipartContent(request);
	  	List items = null;
	  	String workFlow = "";
	  	
	  	if(isMultipart)
		{
	  		FileItemFactory factory = new DiskFileItemFactory();
	  		ServletFileUpload upload = new ServletFileUpload(factory);
			
			try {
				
				items = upload.parseRequest(request);
				
			} catch (FileUploadException e) 
			{
				e.printStackTrace();
				request.setAttribute("errorMessage", "create campaign failed, not a valid zip file");
		  		return new ModelAndView("createCampaign.jsp");
			}
		}
	  	
	   	String listName ="";
		long listId = 0;
		String campaignName="";
		String description="";
		String subject="";
		String enableOpenTracking="";
		String enableLinkTracking="";
		String sendername="";
		String senderemail="";
		String replyname="";
		String replyemail="";
		String smtpserver="";
		String campaignMessage="";
		String filename = "";
		InputStream zipMailBodyStream=null;
		InputStream attachedFileStream=null;
		String filedName="";
		String zipMailBodyName=null;
		String attachedFileName=null;
		String workflow="";
		String pastehtml = "";
				
		Iterator itr = items.iterator();
		while (itr.hasNext()) 
		{
			FileItem item = (FileItem) itr.next();
								
			if (item.isFormField()) 
			{
				String name = item.getFieldName();
				String value = item.getString();
				
				if (name.equals("campaignname")) 
				{
					campaignName = value;	
					
				} else if(name.equals("listname"))
				{
					listName = value;
					
				}
				else if(name.equals("subject"))
				{
					subject = value;
				}else if (name.equals("datasource")) 
				{
					pastehtml = value;	
					
				}
				else if(name.equals("description"))
				{
					description = value;
				}
				
				else if(name.equals("opentracking"))
				{
					enableOpenTracking = value;
				}
				else if(name.equals("linktracking"))
				{
					enableLinkTracking = value;
				}
				else if(name.equals("sendername"))
				{
					sendername = value;
				}
				else if(name.equals("senderemail"))
				{
					senderemail = value;
				}
				else if(name.equals("replyname"))
				{
					replyname = value;
				}
				else if(name.equals("replyemail"))
				{
					replyemail = value;
				}
				else if(name.equals("smtpserver"))
				{
					smtpserver = value;
				}
				else if(name.equals("campaignMessage"))
				{
					campaignMessage = value;
				}
				else if(name.equals("wokflow"))
				{
					workflow = value;
				}
			} else {
					
					filedName=item.getFieldName();
					
					if(filedName.equals("datafile")){
						zipMailBodyName = item.getName();
						try {
							zipMailBodyStream = item.getInputStream();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if(filedName.equals("attachement")){
						attachedFileName = item.getName();
						try {
							attachedFileStream = item.getInputStream();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
			
			}
	
		}
		
		
		Campaign campaign=new Campaign();
		campaign.setCampaignName(campaignName);
		campaign.setListName(listName);
		campaign.setAccountId(accountId);
		
		campaign.setDescription(description);
		campaign.setSubject(subject);
		System.out.println(enableLinkTracking + "enable trancking");
		campaign.setEnableLinkTracking(enableLinkTracking);
		campaign.setEnableOpenTracking(enableOpenTracking);	
		campaign.setFromName(sendername);
		campaign.setFromAddress(senderemail);
		campaign.setReplyName(replyname);
		campaign.setReplyTo(replyemail);
		campaign.setSmtpServer(smtpserver);
		campaign.setStatus("created");
		campaign.setCreatedBy(user);
		campaign.setCreatedDate(new Date());
		campaign.setUpdatedDate(new Date());
		campaign.setUpdatedBy(user);
		//campaign.setCampaignMessage(campaignMessage);
		campaign.setAttachmentFileName(attachedFileName);
		campaign.setPartnerName(partnerName);
		
		if(pastehtml != null && pastehtml.equalsIgnoreCase("pastehtml") && (campaignMessage == null ||campaignMessage.length() <=0) )
	  	{
			
	  		request.setAttribute("errorMessage", "please provide valid mail body ");
	  		campaign.setCampaignMessage(campaignMessage);
	  		ObjectMapper map = new ObjectMapper();
	  		String josn = map.writeValueAsString(campaign);
	  		request.setAttribute("campaignObj", josn);
	  		if(workFlow.equals("true"))
			{
	  			return new ModelAndView("createCampaign_wf.jsp");
			}else
			{
				return new ModelAndView("createCampaign.jsp");
			}
	  		
	  	}else if(pastehtml != null && pastehtml.equalsIgnoreCase("fileupload") && (zipMailBodyStream == null ||zipMailBodyStream.available() <= -1) )
	  	{
	  		request.setAttribute("errorMessage", "please provide valid zip file ");
	  		campaign.setCampaignMessage(campaignMessage);
	  		ObjectMapper map = new ObjectMapper();
	  		String josn = map.writeValueAsString(campaign);
	  		request.setAttribute("campaignObj", josn);
	  		if(workFlow.equals("true"))
			{
	  			return new ModelAndView("createCampaign_wf.jsp");
			}else
			{
				return new ModelAndView("createCampaign.jsp");
			}
	  	}
		if(isCamapaignExist(accountId, campaignName))
	  	{
			campaign.setCampaignMessage(campaignMessage);
			request.setAttribute("errorMessage", "campaign name is exist, please try with other name");
			ObjectMapper map = new ObjectMapper();
	  		String josn = map.writeValueAsString(campaign);
	  		request.setAttribute("campaignObj", josn);
	  		
			//request.setAttribute("campaignObj", campaign);
			if(workFlow.equals("true"))
			{
	  			return new ModelAndView("createCampaign_wf.jsp");
			}else
			{
				return new ModelAndView("createCampaign.jsp");
			}
	  		
	  	}
	//	System.out.println("enableOpenTracking "+ enableOpenTracking + "enableLinkTracking " + enableLinkTracking);
		MailListServiceImpl impl=new MailListServiceImpl();
		listId=impl.getListIdOnName(accountId, listName);
		//System.out.println(zipMailBodyName + "campaignMessage " + campaignMessage);
		campaign.setListId(listId);
		
		List<String> extraAttSub = validateForMessageAttributesOnSubject(campaign.getSubject(), accountId);
        if(extraAttSub != null && extraAttSub.size() > 0)
        {
        	request.setAttribute("errorMessage", "campaign subject message using non defined attibutes "+ extraAttSub);
        	campaign.setCampaignMessage(campaignMessage);
        	ObjectMapper map = new ObjectMapper();
	  		String josn = map.writeValueAsString(campaign);
	  		request.setAttribute("campaignObj", josn);
	  		return new ModelAndView("createCampaign.jsp");
        }
		if(pastehtml != null && pastehtml.equalsIgnoreCase("pastehtml") && campaignMessage != null && campaignMessage.length() > 0)
		{
			campaign.setHtmlFileName(null);
			campaign.setCampaignMessage(campaignMessage);
            //campaign.setCampaignMessage(getCampaignMessageWithReplacedOriginalLinks(campaign.getCampaignId(), campaignMessage));
            List<String> extraAtt = validateForMessageAttributes(campaign.getCampaignMessage(), accountId);
            if(extraAtt != null && extraAtt.size() > 0)
            {
            	request.setAttribute("errorMessage", "campaign body message using non defined attibutes "+ extraAtt);
            	ObjectMapper map = new ObjectMapper();
    	  		String josn = map.writeValueAsString(campaign);
    	  		request.setAttribute("campaignObj", josn);
    	  		return new ModelAndView("createCampaign.jsp");
            }
            campaign =  createCampaign(accountId, campaign);
			
		}else 
		{
			try{
				
				campaign= createCampaign(accountId, campaign);
				getZipMailBodyAndUpdateMessage(campaign, zipMailBodyName, zipMailBodyStream);
				List<String> extraAtt = validateForMessageAttributes(campaign.getCampaignMessage(), accountId);
				if(extraAtt != null && extraAtt.size() > 0)
	            {
	            	deleteCampaign(accountId, campaign);
	            	campaign.setCampaignMessage(null);
	            	//campaign.setCampaignMessage(response.getHtmlContent());
	                campaign.setHtmlFileName(null);
	            	request.setAttribute("errorMessage", "campaign upload zip file using non defined attibutes "+ extraAtt);
	            	ObjectMapper map = new ObjectMapper();
	    	  		String josn = map.writeValueAsString(campaign);
	    	  		request.setAttribute("campaignObj", josn);
	    	  		return new ModelAndView("createCampaign.jsp");
	            }
				
				
			}catch(Exception e)
			{
				deleteCampaign(accountId, campaign);
				ObjectMapper map = new ObjectMapper();
		  		String josn = map.writeValueAsString(campaign);
		  		request.setAttribute("campaignObj", josn);
				request.setAttribute("errorMessage", "please provide valid zip file, html should be contained in top level folder ");
		  		if(workFlow.equals("true"))
				{
		  			return new ModelAndView("createCampaign_wf.jsp");
				}else
				{
					return new ModelAndView("createCampaign.jsp");
				}
				
			}
		}
			
		if(attachedFileStream != null && attachedFileStream.available() > -1)
		{
			try{
				
				getAttachmentAndUpdateCampaign(campaign, attachedFileName, attachedFileStream);
				
			}catch(Exception e)
			{
				deleteCampaign(accountId, campaign);
				campaign.setCampaignMessage(campaignMessage);
				ObjectMapper map = new ObjectMapper();
		  		String josn = map.writeValueAsString(campaign);
		  		request.setAttribute("campaignObj", josn);
				request.setAttribute("errorMessage", "please provide valid attachment file ");
		  		if(workFlow.equals("true"))
				{
		  			return new ModelAndView("createCampaign_wf.jsp");
				}else
				{
					return new ModelAndView("createCampaign.jsp");
				}
		  		
				
			}
			
		}
		Random ran = new Random();
	    campaign.setConfirmationNo(new Long(ran.nextInt(12)));
	    updateCampaign(accountId, campaign);
	    createCampaignLinks(accountId, campaign);
		if(workFlow.equals("true"))
		{
			request.setAttribute("campaignName", campaignName);
			request.setAttribute("listnName", listName);
			request.setAttribute("workflow", "true");
			return new ModelAndView("directsendout.jsp");
		}
		
		return new ModelAndView("campaignManagement.jsp");
	}
	private List<String> validateForMessageAttributesOnSubject(String campaignMessage, long accountId) {
    	EmailMessageParserService service  = new EmailMessageParserService();
    	return service.validateForAttributesInBodyMessage(campaignMessage, accountId);
			
	}

	private void createCampaignLinks(Long accountId, Campaign campaign) {
		CampaignService service=new CampaignService();
		service.createCampaignLinks(accountId, campaign);
	}

	private void updateCampaign(Long accountId, Campaign campaign) {
		CampaignService service=new CampaignService();
		service.updateCampaign(accountId, campaign);
		
	}

	private void getAttachmentAndUpdateCampaign(Campaign campaign,
			String attachedFileName, InputStream attachedFileStream) {
		CampaignServiceUtil service=new CampaignServiceUtil();
		service.saveAttachmentFileNewCampaign(campaign, attachedFileName, attachedFileStream);
	}

	private boolean isValidZipFile(InputStream in)
	{
		if(ZipFileUtil.isValidZipFileWithHtmlAndNoDirectory(in))
		{
           return true;
        }
		return false;
	}
	private void getZipMailBodyAndUpdateMessage(Campaign campaign,
			String zipMailBodyName, InputStream zipMailBodyStream) {
		CampaignServiceUtil service=new CampaignServiceUtil();
		service.getZipMailBodyAndUpdateMessageCreateNewCampaign(campaign, zipMailBodyName, zipMailBodyStream);
	}
	
	private List<String> validateForMessageAttributes(String campaignMessage, long accountId) {
    	EmailMessageParserService service  = new EmailMessageParserService();
    	return service.validateForAttributesInBodyMessage(campaignMessage, accountId);
			
	}

	private boolean isCamapaignExist(Long accountId,String campaingName)
	{
		CampaignService service=new CampaignService();
	  	if(service.isCampaignExist(campaingName))
	  	{
	  		return true;
	  	}
		return false;
	}
	
	private String getListName(Long accountId,Long listId)
	{
		MailListServiceImpl impl=new MailListServiceImpl();
		return impl.getListNameOnId(accountId,listId);
	}
	
		
		
	private String getCampaignMessageWithReplacedOriginalLinks(Long campaignId,
			String campaignMessage) {
		CampaignServiceUtil util = new CampaignServiceUtil();
		return util.getCampaignMessageWithReplacedOriginalLinks(campaignId, campaignMessage);
	}
	

	private void deleteCampaign(Long accountId,Campaign campaign)
	{
		CampaignService service=new CampaignService();
		try{
			service.deleteNewCampaign(accountId,campaign.getCampaignName());
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
		

	private Campaign createCampaign(Long accountId,Campaign campaign)
	{
		CampaignService service=new CampaignService();
		service.createCampaign(accountId,campaign);
		return service.getCampaign(accountId, campaign.getCampaignName());
	}
		
	
}
