package com.kenscio.litemail.workflow.controller.reports;

import java.awt.Color;
import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;
import org.json.JSONException;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.kenscio.litemail.workflow.domain.MessageSpam;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.CampaignBounce;
import com.kenscio.litemail.workflow.domain.CampaignLinks;
import com.kenscio.litemail.workflow.domain.CampaignMailFail;
import com.kenscio.litemail.workflow.domain.CampaignMailSentSoFar;
import com.kenscio.litemail.workflow.domain.CampaignStatusCount;
import com.kenscio.litemail.workflow.domain.EmtsResponse;
import com.kenscio.litemail.workflow.domain.MessageBounce;
import com.kenscio.litemail.workflow.domain.SampleDemo;
import com.kenscio.litemail.workflow.domain.SmtpEntity;
import com.kenscio.litemail.workflow.service.CampaignReportService;
import com.kenscio.litemail.workflow.service.CampaignService;
import com.kenscio.litemail.workflow.service.SmtpService;

@Controller
public class GetMailOpendStatusController{
	
	
	
	
		
	@RequestMapping(value="/getMailOpendStatus.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		int width = 450;
	    int height = 300;
	    HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		String mailSent=request.getParameter("mailsent");
		String mailsOpen=request.getParameter("totalopens");
		Long mailsSent =0l;
		Long mailsOpend =0l;
		if(mailSent != null)
		{
			
			mailsSent =new Long(mailSent);
		}
		if(mailSent != null)
		{
			mailsOpend = new Long(mailsOpen);
		}
		
		DefaultPieDataset data = getData(mailsSent,mailsOpend);
		JFreeChart chart = ChartFactory.createPieChart("Mail Opened Status", data,false, false, false);
		
		chart.setBackgroundPaint(Color.white);
		PiePlot plot = (PiePlot) chart.getPlot();
		PieSectionLabelGenerator labelGenerator = new StandardPieSectionLabelGenerator("{0} = {2}", NumberFormat.getNumberInstance(), NumberFormat.getPercentInstance() );
		plot.setLabelGenerator(labelGenerator);
		ChartUtilities.writeChartAsJPEG(response.getOutputStream(),chart,width,height);

		return null;
	}
	private DefaultPieDataset getData(Long mailsSent,Long mailsOpend)
	{
        Double mailsOpenedInDobule =   new Double(mailsOpend);
        Double mailsNotOpend = new Double(mailsSent).doubleValue() - mailsOpenedInDobule.doubleValue();
        DefaultPieDataset data = new DefaultPieDataset();
        data.setValue("Mails Opened", mailsOpenedInDobule);
        data.setValue("Mails Not Opened", mailsNotOpend);
        return data;
	 }
	 
	 @RequestMapping(value="/getMailFailData.htm", method=RequestMethod.GET)
	 public String getMailFailData(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(false);
		if (session == null) {
			request.setAttribute("errorMessage",
					"session expired, please login again ");
			return "loginscreen.jsp";
		}
		 
       String camapignId = request.getParameter("campaignId");
       String fromDate = request.getParameter("fromdate");
       String toDate = request.getParameter("todate");
       Long campaignID = new Long(camapignId);
       String pageNo=request.getParameter("pageNumber");
       System.out.println("****Fail"+pageNo);
       Integer pageNumber=0;
       if(pageNo != null )
       {
    	   pageNumber = new Integer(pageNo);
       }
       int noOfRecordsperPage=10;
       float totalRecords=0;
       List<CampaignMailFail> totalFailedList =null;
       if(fromDate == null || fromDate == "" )
       {
	   CampaignReportService reportService = new CampaignReportService();
	   totalFailedList = reportService.getMailFailDataList(campaignID,pageNumber,noOfRecordsperPage);
	   totalRecords = reportService.getMailFailCount(campaignID);
       }
       else
       {
    	   SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	       Date fd = formatter.parse(fromDate);
	       Date td = formatter.parse(toDate);
	       CampaignReportService reportService = new CampaignReportService();
    	   totalFailedList = reportService.getMailFailDataListFilter(campaignID,fd,td,pageNumber,noOfRecordsperPage); 
    	   totalRecords = reportService.getMailFailDateFilterCount(campaignID,fd,td);
       }
		double totalPages = totalRecords / noOfRecordsperPage;
	//	System.out.println(totalPages);
		totalPages = Math.ceil(totalPages);
		double temp = noOfRecordsperPage*totalPages;
		if(totalRecords > temp)
			totalPages = totalPages+1;
		
		//System.out.println("totalpages:" + totalPages);
		ObjectMapper mapper = new ObjectMapper();
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mapper.getDeserializationConfig().setDateFormat(df);
		String jsonString = "";
       
       
	   	Map<String, Object> finalMap = new HashMap<String, Object>();
		
		AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
		
		
			finalMap.put("result", totalFailedList);
			finalMap.put("pagenumber", pageNumber);
			finalMap.put("totalpages", totalPages);
			 jsonString = mapper.writeValueAsString(finalMap);
		//	 System.out.println(jsonString);
		
		
		MediaType jsonMimeType = MediaType.APPLICATION_JSON;

		if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}
		}
		return null;

	}
	 
	 @RequestMapping(value="/getClickThroughLinkCounts.htm", method=RequestMethod.GET)
	 public  String getClickThroughLinkCounts(HttpServletRequest request,HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException, ParseException, JSONException
	 {
		 HttpSession session=request.getSession(false);
			if(session == null)
			{
				request.setAttribute("errorMessage","session expired, please login again ");
				return "loginscreen.jsp";
			}
		   long accountId= (Long) session.getAttribute("accountId");
	       String viewUrl = "ViewMailLinkClickCountApi.htm";
		   String camapignId = request.getParameter("campaignId");
		   Long campaignID = new Long(camapignId);
	       String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		   String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		   String instance = ApplicationProperty.getProperty("litemail.instance");
	       String url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+instance+"_"+accountId+"&cId="+campaignID;
	      // System.out.println("count url >>>  "+ url);
	      // GetMailOpendStatusController controller= new GetMailOpendStatusController();
	       CampaignReportService reportservice = new CampaignReportService();
	       EmtsResponse emtsObject = getEmtsResponse(url);
	       List<CampaignLinks>  links = new ArrayList<CampaignLinks>();
	       if(emtsObject != null && emtsObject.getItems() != null && emtsObject.getItems().size()>0)
	       {
	    	   @SuppressWarnings("unchecked")
	    	   Map<String, Integer> positionCounts = ( Map<String, Integer>)emtsObject.getItems().get(0);
	    	//   System.out.println("positionCounts  >>>  "+ positionCounts.get("1"));
	    	 //  System.out.println("positionCounts  >>>  "+ positionCounts.get("2"));
	    	   CampaignService serv = new CampaignService();
		       links = serv.getCampaingLinks(accountId, campaignID);
		       
		       for(CampaignLinks cliks : links)
		       {
		    	 //  System.out.println("CampaignLinks "+cliks.getPosition()  + "positionCounts.get(cliks.getPosition() "+ positionCounts.get(cliks.getPosition()+"".trim()));
		    	   if(positionCounts != null && positionCounts.size() > 0)
		    	   {
		    		   String key = cliks.getPosition()+"".trim();
		    		   if(positionCounts.containsKey(key))
		    		   {
		    			   int size = positionCounts.get(key);
		    			   cliks.setNoOfClicks(new Long(size));
		    		   }
		    	   }
		    	 //  System.out.println("size "+ size);
		    	  
		       }
	    	   
	       }
	       
	       ObjectMapper wrapper = new ObjectMapper();
	       MediaType jsonMimeType = MediaType.APPLICATION_JSON;
           AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
           String jsonString = wrapper.writeValueAsString(links);
           
	       if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {
	
		      try {
		       
		       
		       stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));
		
		      } catch (IOException e) {
		    	  e.printStackTrace();
		
		      } catch (HttpMessageNotWritableException e1) {
		    	  e1.printStackTrace();
		      }
	      }
		return null;
		       
		    
			
	}
	 
	 @RequestMapping(value="/searchMailBounceSpamList.htm", method=RequestMethod.GET)
	 public String searchMailBounceSpamList(HttpServletRequest request,HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException, ParseException, JSONException
	 {
		 
		 HttpSession session=request.getSession(false);
			if(session == null)
			{
				request.setAttribute("errorMessage","session expired, please login again ");
				return "loginscreen.jsp";
			}
		   long accountId= (Long) session.getAttribute("accountId");
		   String camapignId = request.getParameter("campaignId");
		   String uId=request.getParameter("uId");
		   String fromdate = request.getParameter("fromdate");
	       String todate = request.getParameter("todate");
	       String pageNo=request.getParameter("pageNumber");
	       Integer pageNumber=0;
	       if(pageNo != null )
	       {
	    	   pageNumber = new Integer(pageNo);
	       }
	      	       
	       Long campaignID = new Long(camapignId);
	       int noOfRecordsperPage=20;
	       float totalRecords=0;
	       
	       String viewUrl=request.getParameter("url");
	       String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		   String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		   String url="";
		   CampaignReportService reportservice = new CampaignReportService();
		   Campaign report = reportservice.getCampaignStatusReport(accountId, campaignID);
			    List<CampaignBounce> bounceCount = reportservice.getCampaignMailBounceCount(accountId, campaignID);
			    for(int i=0;i<bounceCount.size();i++)
			    {
			    	System.out.println(bounceCount.get(i) + "bounce count");
			    }
			   
		   String dm = getDomainFromAddress(report);
		   String instance = ApplicationProperty.getProperty("litemail.instance");
		   if(fromdate == null || todate== "")
 		   {
			   
			  
				
				Date time = report.getCampaignStartDateTime();
				if(time == null)
				{
					time = report.getCreatedDate();
				}
				String fromTime = "";
				try{
					
					  SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					  fromTime = formatter.format(time);
				}catch(Exception e)
				{
					e.printStackTrace();
				}
			   
			   url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&dm="+dm+"&clId="+instance+"_"+accountId+"&cId="+campaignID+"&uId="+uId+"&cst="+fromTime;
 		   }
 		   else
 		   {
 			  // SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
 			 //  Date fd = formatter.parse(fromdate);
 			  // Date td = formatter.parse(todate);
 		     			  
 			   url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&dm="+dm+"&clId="+instance+"_"+accountId+"&cId="+campaignID+"&uId="+uId+"&fd="+fromdate+"&td="+todate;  
 		   }
		   
		   System.out.println("search urls bounces "+ url);
		   
		  /* if(fd == null || fd== "")
		   {
			   url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+platFormId+"_"+accountId+"&cId="+campaignID;
		   }
		   else
		   {
			   url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+platFormId+"_"+accountId+"&cId="+campaignID+"&fd="+fd+"&td="+td;  
		   }*/
		  
	       EmtsResponse emtsObject = reportservice.getEmtsResponse(url);
	       double totalPages = totalRecords / noOfRecordsperPage;
		   totalPages = Math.ceil(totalPages);
		   double temp = noOfRecordsperPage*totalPages;
			if(totalRecords > temp)
				totalPages = totalPages+1;
	       ObjectMapper wrapper = new ObjectMapper();
	       MediaType jsonMimeType = MediaType.APPLICATION_JSON;
	   	   Map<String, Object> finalMap = new HashMap<String, Object>();
           AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
           
			finalMap.put("results", bounceCount);
			finalMap.put("pagenumber", pageNumber);
			finalMap.put("totalpages", totalPages);
			String jsonString = wrapper.writeValueAsString(finalMap);
	
        /*   String jsonString = wrapper.writeValueAsString(emtsObject);*/
     
	       if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {
	
		      try {
		       
		       stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));
		
		      } catch (IOException e) {
		       e.printStackTrace();
		
		      } catch (HttpMessageNotWritableException e1) {
		       e1.printStackTrace();
		      }
	      }
		return null;
		    
			
	}
	 private String getDomainFromAddress(Campaign report) {
			SmtpService ser = new SmtpService();
			SmtpEntity se = ser.getSmtpDetails(report.getSmtpServer());
			if(se != null && se.getSmtpServer().contains("mailgun"))
			{
				return getDomainName(se.getSmtpUser());
			}
			else
			{
				String str = report.getFromAddress();
				return getDomainName(str); //get domain from fromaddress for amazon
			}
			
	}
	 private String getDomainName(String smtpUser) {
			
			if(smtpUser != null && smtpUser.length() > 1)
			{
				int i = smtpUser.indexOf("@");
				return smtpUser.substring(i+1);
			}
			return "";
		}

	 /* All Mail Search Action*/
	
	 @RequestMapping(value="/searchMailList.htm", method=RequestMethod.GET)
	 public String SearchMailLsit(HttpServletRequest request,HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException, ParseException, JSONException
	 {
		 
		 HttpSession session=request.getSession(false);
			if(session == null)
			{
				request.setAttribute("errorMessage","session expired, please login again ");
				return "loginscreen.jsp";
			}
		   long accountId= (Long) session.getAttribute("accountId");
		   String camapignId = request.getParameter("campaignId");
		   String uId=request.getParameter("uId");
		   String fromdate = request.getParameter("fromdate");
	       String todate = request.getParameter("todate");
	       String pageNo=request.getParameter("pageNumber");
	       String recId1=request.getParameter("recId");
	       long recId=0l;
	       if(recId1 !=null)
	       {
	    	   recId=new Long(recId1);
	       }
	       Integer pageNumber=0;
	       int recSize=20;
	       if(pageNo != null )
	       {
	    	   pageNumber = new Integer(pageNo);
	       }
	       
	      	       
	       Long campaignID = new Long(camapignId);
	       int noOfRecordsperPage=20;
	       float totalRecords=0;
	       
	       String viewUrl=request.getParameter("url");
	       String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		   String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		   String instance = ApplicationProperty.getProperty("litemail.instance");
		   String url="";
		   
		  
		   if(fromdate == null || todate== "")
 		   {
			   
			   url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+instance+"_"+accountId+"&cId="+campaignID+"&uId="+uId+"&recId="+recId+"&recSize="+recSize;
 		   }
 		   else
 		   {
 			  // SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
 			 //  Date fd = formatter.parse(fromdate);
 			  // Date td = formatter.parse(todate);
 		     			  
 			   url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+instance+"_"+accountId+"&cId="+campaignID+"&uId="+uId+"&fd="+fromdate+"&td="+todate+"&recId="+recId+"&recSize="+recSize;  
 		   }
		   
		   System.out.println("search urls "+ url);
		   
		  /* if(fd == null || fd== "")
		   {
			   url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+platFormId+"_"+accountId+"&cId="+campaignID;
		   }
		   else
		   {
			   url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+platFormId+"_"+accountId+"&cId="+campaignID+"&fd="+fd+"&td="+td;  
		   }*/
		   CampaignReportService reportservice = new CampaignReportService();
	       EmtsResponse emtsObject = reportservice.getEmtsResponse(url);
	       double totalPages = totalRecords / noOfRecordsperPage;
		   totalPages = Math.ceil(totalPages);
		   double temp = noOfRecordsperPage*totalPages;
			if(totalRecords > temp)
				totalPages = totalPages+1;
	       ObjectMapper wrapper = new ObjectMapper();
	       MediaType jsonMimeType = MediaType.APPLICATION_JSON;
	   	   Map<String, Object> finalMap = new HashMap<String, Object>();
           AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
           
			finalMap.put("results", emtsObject);
			finalMap.put("pagenumber", pageNumber);
			finalMap.put("totalpages", totalPages);
			String jsonString = wrapper.writeValueAsString(finalMap);
	
        /*   String jsonString = wrapper.writeValueAsString(emtsObject);*/
     
	       if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {
	
		      try {
		       
		       stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));
		
		      } catch (IOException e) {
		       e.printStackTrace();
		
		      } catch (HttpMessageNotWritableException e1) {
		       e1.printStackTrace();
		      }
	      }
		return null;
		    
			
	}
	/*public EmtsResponse execute(String url,HttpServletResponse response)
	          throws ClientProtocolException, IOException, JSONException
	{
      HttpClient client = new DefaultHttpClient();
      HttpGet request = new HttpGet(url);
      HttpResponse response1 = client.execute(request);
      ObjectMapper wrapper = new ObjectMapper();
      EmtsResponse emtsObject = (EmtsResponse)wrapper.readValue(response1.getEntity().getContent(), EmtsResponse.class);
      client.getConnectionManager().shutdown();
      return emtsObject;      
         
     
   	    
	}*/
	
	public EmtsResponse getEmtsResponse(String url)
    {
        HttpClient client;
        EmtsResponse emtsObject;
        client = new DefaultHttpClient();
        emtsObject = new EmtsResponse();
        try
        {
            HttpGet request = new HttpGet(url);
            HttpResponse response = client.execute(request);
            ObjectMapper wrapper = new ObjectMapper();
            emtsObject = (EmtsResponse)wrapper.readValue(response.getEntity().getContent(), EmtsResponse.class);
            client.getConnectionManager().shutdown();
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        client.getConnectionManager().shutdown();
		return emtsObject;
       
    }
	
	
	
	 @RequestMapping(value="/urlrender.htm", method=RequestMethod.GET)
	 public String urlRender(HttpServletRequest request,HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException, ParseException, JSONException
	 {
		 HttpSession session=request.getSession(false);
			if(session == null)
			{
				request.setAttribute("errorMessage","session expired, please login again ");
				return "loginscreen.jsp";
			}
		   long accountId= (Long) session.getAttribute("accountId");
		   String camapignId = request.getParameter("campaignId");
		   String uId=request.getParameter("uId");
		   session.setAttribute("uId", uId);
	       String viewUrl=request.getParameter("url");
	       String exportUrl=request.getParameter("url2");
	       String type=request.getParameter("type");
	     //  System.out.println("****"+viewUrl);
	       session.setAttribute("campaignId",camapignId);
	       session.setAttribute("viewUrl",viewUrl);
	       session.setAttribute("exportUrl",exportUrl);
	       session.setAttribute("type",type);
	       return null;
	 }
	 
	 
	 
	 
	 @RequestMapping(value="/bounceList.htm", method=RequestMethod.GET)
	 public String getMailBounceListData(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
		 
		 HttpSession session=request.getSession(false);
			if(session == null)
			{
				request.setAttribute("errorMessage","session expired, please login again ");
				return "loginscreen.jsp";
			}
			long accountId = (Long)session.getAttribute("accountId");
       String camapignId = request.getParameter("campaignId");
       String fromDate = request.getParameter("fromdate");
       String toDate = request.getParameter("todate");
       String bounceType=request.getParameter("type");
       String pageNo=request.getParameter("pageNumber");
       Integer pageNumber=0;
       if(pageNo != null )
       {
    	   pageNumber = new Integer(pageNo);
       }
       Long campaignID = new Long(camapignId);
       
		CampaignReportService reportservice = new CampaignReportService();
	    Campaign report = reportservice.getCampaignStatusReport(accountId, campaignID);
	    
	    
	    String cmpStarTime  = "";
		Date campaignStartTime = report.getCampaignStartDateTime();
		Date cendTime=report.getCampaignEndDateTime();
		if(campaignStartTime == null)
		{
			campaignStartTime = report.getCreatedDate();
		}
		String fromTime = "";
		try{
			
			  SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			  fromTime = formatter.format(campaignStartTime);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		if(cendTime == null)
		{
			cendTime = new Date();
		}
		
		//long fromTime = time.getTime()-(6*60*60*1000);
		String dm = getDomainFromAddress(report);
		String sp=report.getSmtpServer();
		if(sp != null && sp.contains("mailgun") )
		{
			sp = "mailgun";
		}else
		{
			sp = "amazon";
		}
       int noOfRecordsperPage=10;
       float totalRecords=0;

       List<MessageBounce> totalBounceList =null;
       if(fromDate == null || fromDate == "" )
       {
    	   Date td=new Date();
    	   CampaignReportService reportService = new CampaignReportService();
    	   //Date campaignStartTime=new CampaignService().getCampaign(campaignID).getCampaignStartDateTime();
    	   totalBounceList=reportService.getBounceEmails(bounceType,campaignStartTime,sp,dm,pageNumber,noOfRecordsperPage);
			totalRecords = reportService.getMailBounceCount(dm,sp,bounceType,campaignStartTime,new Date());
    	   }
    	 
       else
       {
    	   SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	       Date fd = formatter.parse(fromDate);
	       Date td = formatter.parse(toDate);
	       CampaignReportService reportService = new CampaignReportService();
	       
	       totalBounceList=reportService.getBounceEmails(bounceType,fd,td,sp,dm,pageNumber,noOfRecordsperPage);
	       totalRecords = reportService.getMailBounceCount(dm,sp,bounceType,fd,td);
	       
		}
		
       
       
       
       
		double totalPages = totalRecords / noOfRecordsperPage;
		totalPages = Math.ceil(totalPages);
		double temp = noOfRecordsperPage*totalPages;
		if(totalRecords > temp)
			totalPages = totalPages+1;
		ObjectMapper mapper = new ObjectMapper();
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mapper.getDeserializationConfig().setDateFormat(df);
		String jsonString = "";
       
       
       
	   	Map<String, Object> finalMap = new HashMap<String, Object>();
		
		AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
		
		
			finalMap.put("result", totalBounceList);
			finalMap.put("pagenumber", pageNumber);
			finalMap.put("totalpages", totalPages);
			 jsonString = mapper.writeValueAsString(finalMap);
			// System.out.println(jsonString);
		
		
		MediaType jsonMimeType = MediaType.APPLICATION_JSON;

		if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}
		}
		return null;

	}
	 
	 @RequestMapping(value="/spamList.htm", method=RequestMethod.GET)
	 public String getMailSpamListData(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
		 HttpSession session=request.getSession(false);
			if(session == null)
			{
				request.setAttribute("errorMessage","session expired, please login again ");
				return "loginscreen.jsp";
			}
		 
       String camapignId = request.getParameter("campaignId");
       String fromDate = request.getParameter("fromdate");
       String toDate = request.getParameter("todate");
       String pageNo=request.getParameter("pageNumber");
       Integer pageNumber=0;
       long accountId = (Long)session.getAttribute("accountId");
       Long campaignID = new Long(camapignId);
       CampaignReportService reportservice = new CampaignReportService();
	    Campaign report = reportservice.getCampaignStatusReport(accountId, campaignID);
	    
	    
	    String cmpStarTime  = "";
		Date campaignStartTime = report.getCampaignStartDateTime();
		Date cendTime=report.getCampaignEndDateTime();
		if(campaignStartTime == null)
		{
			campaignStartTime = report.getCreatedDate();
		}
		String fromTime = "";
		try{
			
			  SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			  fromTime = formatter.format(campaignStartTime);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		if(cendTime == null)
		{
			cendTime = new Date();
		}
		
		//long fromTime = time.getTime()-(6*60*60*1000);
		String dm = getDomainFromAddress(report);
		String sp=report.getSmtpServer();
		if(sp != null && sp.contains("mailgun") )
		{
			sp = "mailgun";
		}else
		{
			sp = "amazon";
		}
       if(pageNo != null )
       {
    	   pageNumber = new Integer(pageNo);
       }
       int noOfRecordsperPage=10;
       float totalRecords=0;
   
       List<MessageSpam> totalSpamList =null;
       if(fromDate == null || fromDate == "" )
       {
		   CampaignReportService reportService = new CampaignReportService();
		  // Date campaignStartTime=new CampaignService().getCampaign(campaignID).getCampaignStartDateTime();
		  // totalSpamList = reportService.getCampaignSpamData(campaignStartTime,sp,dm);
		    totalSpamList=reportService.getSpamEmails(campaignStartTime,sp,dm,pageNumber,noOfRecordsperPage);
					totalRecords = reportService.getMailSpamCount(campaignStartTime,sp,dm,cendTime);
			}
		   
       
       else
       {
    	   SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	       Date fd = formatter.parse(fromDate);
	       Date td = formatter.parse(toDate);
	       CampaignReportService reportService = new CampaignReportService();
	       totalSpamList=reportService.getSpamEmails(fd,td,sp,dm,pageNumber,noOfRecordsperPage);
	       totalRecords = reportService.getMailSpamCountDateFilter(fd,td,sp,dm);
	      
       }
        double totalPages = totalRecords / noOfRecordsperPage;
		
		totalPages = Math.ceil(totalPages);
		double temp = noOfRecordsperPage*totalPages;
		if(totalRecords > temp)
			totalPages = totalPages+1;
		ObjectMapper mapper = new ObjectMapper();
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mapper.getDeserializationConfig().setDateFormat(df);
		String jsonString = "";
       
       
       
	   	Map<String, Object> finalMap = new HashMap<String, Object>();
		
		AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
		
		
			finalMap.put("result", totalSpamList);
			finalMap.put("pagenumber", pageNumber);
			finalMap.put("totalpages", totalPages);
			 jsonString = mapper.writeValueAsString(finalMap);
			 System.out.println(jsonString);
		
		
		MediaType jsonMimeType = MediaType.APPLICATION_JSON;

		if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}
		}
		return null;

	}
	 
	 @RequestMapping(value="/getMailSentSoFar.htm", method=RequestMethod.GET)
	 public String getMailSentSoFarListData(HttpServletRequest request,
				HttpServletResponse response) throws Exception {
		 HttpSession session=request.getSession(false);
			if(session == null)
			{
				request.setAttribute("errorMessage","session expired, please login again ");
				return "loginscreen.jsp";
			}
	   long accountId= (Long) session.getAttribute("accountId");
       String camapignId = request.getParameter("campaignId");
       String fromDate = request.getParameter("fromdate");
       String toDate = request.getParameter("todate");
       String pageNo=request.getParameter("pageNumber");
       Integer pageNumber=0;
       if(pageNo != null )
       {
    	   pageNumber = new Integer(pageNo);
       }
       Long campaignID = new Long(camapignId);
       int noOfRecordsperPage=20;
       float totalRecords=0;
       List<CampaignMailSentSoFar> totalUnsubscribeList =null;
       CampaignReportService reportservice = new CampaignReportService();
       if(fromDate == null || fromDate == "" )
       {
		   
    	   totalUnsubscribeList = reportservice.getCampaignMailSentSoFarData(campaignID,pageNumber,noOfRecordsperPage);
		   totalRecords=reportservice.getMailMailSentSoFarCount(campaignID);
		 
       }
       else
       {
    	   SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	       Date fd = formatter.parse(fromDate);
	       Date td = formatter.parse(toDate);
	       totalUnsubscribeList = reportservice.getCampaignMailSentSoFarData(campaignID,fd,td,pageNumber,noOfRecordsperPage);
		   totalRecords=reportservice.getMailMailSentSoFarCount(campaignID);
       }
        double totalPages = totalRecords / noOfRecordsperPage;
		
		totalPages = Math.ceil(totalPages);
		double temp = noOfRecordsperPage*totalPages;
		if(totalRecords > temp)
			totalPages = totalPages+1;
		ObjectMapper mapper = new ObjectMapper();
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mapper.getDeserializationConfig().setDateFormat(df);
		String jsonString = "";
       
       
       
	   	Map<String, Object> finalMap = new HashMap<String, Object>();
		
		AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
		
		
			finalMap.put("result", totalUnsubscribeList);
			finalMap.put("pagenumber", pageNumber);
			finalMap.put("totalpages", totalPages);
			 jsonString = mapper.writeValueAsString(finalMap);
			 System.out.println(jsonString);
		
		
		MediaType jsonMimeType = MediaType.APPLICATION_JSON;

		if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}
		}
		return null;

	}
	

	 
	//advanced analytics 
	 
	 @RequestMapping(value="/showAdvAnalytics.htm", method=RequestMethod.GET)
	 public String showAdvAnalyticsForm(HttpServletRequest request,
	 		HttpServletResponse response)
	 {
	 	HttpSession session=request.getSession(false);
	 	if(session == null)
	 	{
	 		request.setAttribute("errorMessage","session expired, please login again ");
	 		return "loginscreen.jsp";
	 	}
	 	String camapignId = request.getParameter("campaignId");
	 	Long campaignID=0l;
	 	if(camapignId!= null)
	 	{
	 		campaignID=new Long(camapignId);
	 	}
	 	CampaignService service=new CampaignService();
	 	Campaign object=service.getCampaign(campaignID);
	 	request.setAttribute("campaignname", object.getCampaignName());
	 	request.setAttribute("campaignId", campaignID);
	 	return "AdvancedAnalytics.jsp";
	 	
	 }
	 
	 @RequestMapping(value="/timeperiod.htm", method=RequestMethod.GET)
	 public String showAdvTimeperiod(HttpServletRequest request,
	 		HttpServletResponse response) throws JsonGenerationException, JsonMappingException, IOException
	 {
	 	HttpSession session=request.getSession(false);
	 	if(session == null)
	 	{
	 		request.setAttribute("errorMessage","session expired, please login again ");
	 		return "loginscreen.jsp";
	 	}
	 	String camapignId = request.getParameter("campaignId");
	 	Long campaignID=new Long(camapignId);
	 	CampaignService service = new CampaignService();
	 	Date startTime = service.getCampaign(campaignID).getCampaignStartDateTime();
	  	Calendar c=Calendar.getInstance();
	  	SimpleDateFormat df1 = new SimpleDateFormat("dd-MM-yyyy");
	 	List<String> list= new ArrayList<String>();
	 	String cstTime=df1.format(startTime);
	 	list.add(""+cstTime);
	 	c.setTime(startTime);
	 	for (int i = 0; i <6; i++) {
	 		c.add(Calendar.DATE,1);
	 		list.add(""+df1.format(c.getTime()));
	 	}	
	 	ObjectMapper mapper = new ObjectMapper();
	 	final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 	mapper.getDeserializationConfig().setDateFormat(df);
	 	String jsonString = "";
	     Map<String, Object> finalMap = new HashMap<String, Object>();
	 	
	 	 AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
	 	
	 	
	 		finalMap.put("result", list);
	 		
	 		 jsonString = mapper.writeValueAsString(finalMap);
	 		 System.out.println(jsonString);
	 	
	 	
	 	MediaType jsonMimeType = MediaType.APPLICATION_JSON;

	 	if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

	 		try {

	 			stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

	 		} catch (IOException e) {
	 			e.printStackTrace();

	 		} catch (HttpMessageNotWritableException e1) {
	 			e1.printStackTrace();
	 		}
	 	}
	 	return null;

	 }
	 	
	 @RequestMapping(value="/advAnalytics.htm", method=RequestMethod.GET)
	 public String getAdvacedAnalyticsData(HttpServletRequest request,
	 			HttpServletResponse response) throws Exception {
		 
			HttpSession session=request.getSession(false);
			if(session == null)
			{
				request.setAttribute("errorMessage","session expired, please login again ");
				return "loginscreen.jsp";
			} 	 
 		
 		   long accountId= (Long) session.getAttribute("accountId");
		   String camapignId =request.getParameter("campaignId");
	       String period = request.getParameter("period");
	       String period1=null;
	      if(!period.equalsIgnoreCase("all")){
	       CharSequence date= period.subSequence(0, 2);
			  CharSequence month=period.subSequence(4, 5);
			  CharSequence year= period.subSequence(6, 10);
			  period1=year+"-"+month+"-"+date+" 00:00:00";
	      }
	     
	       System.out.println("priod:"+period);
	       Long campaignID = new Long(camapignId);
	       CampaignReportService reportservice = new CampaignReportService();
		   Campaign report = reportservice.getCampaignStatusReport(accountId, campaignID);
	       String viewUrl="getAllCampaignStatusCounts.htm";
	       String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		   String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		   String instance = ApplicationProperty.getProperty("litemail.instance");
		   String url="";
		   String dm = getDomainFromAddress(report);
		   //String dm="transtutorsnewsletter.com";
		   String sp=report.getSmtpServer();
		   if(sp != null && sp.contains("mailgun") )
			{
				sp = "mailgun";
			}else
			{
				sp = "amazon";
			} 
		   Date time = report.getCampaignStartDateTime();
		   Date cendTime=report.getCampaignEndDateTime();
		   
			if(time == null)
			{
				time = report.getCreatedDate();
			}
			String fromTime = "";
			try{
				
				  SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				  fromTime = formatter.format(time);
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			if(cendTime == null)
			{
				cendTime = new Date();
			}
			CampaignStatusCount campStatus = new CampaignStatusCount();
			CampaignBounce bounceCountByDomain=null;
			
		   if(period.equals("All"))
		   {
			   url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&dm="+dm+"&clId="+instance+"_"+accountId+"&cId="+campaignID+"&cst="+fromTime;
			   bounceCountByDomain=reportservice.getCampaignMailBounceCountByDomain(campaignID,dm,sp,time,cendTime);
		   }
		   else
		   {
			  // SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:MM:SS");
		      // Date period1 = formatter.parse(period);
		       System.out.println(period1 + "converted date");
			   url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&dm="+dm+"&clId="+instance+"_"+accountId+"&cId="+campaignID+"&onDate="+period;
			   bounceCountByDomain=reportservice.getCampaignMailBounceCountByDomain(campaignID,dm,sp,period1);
			   
			   				
		   }
		   if(bounceCountByDomain != null)
		   {
			    campStatus.setGeneralbounce(bounceCountByDomain.getGeneralBounce());
		    	campStatus.setHardbounce(bounceCountByDomain.getHardBounce());
		    	
		    	campStatus.setSoftbounce(bounceCountByDomain.getSoftBounce());
		    	
		    	campStatus.setSpam(bounceCountByDomain.getSpamCount());
		    	
		    	campStatus.setTotalbounce(bounceCountByDomain.getTotalNoOfBounces());
	
		    	campStatus.setTransientbounce(bounceCountByDomain.getTransientBounce());
		    	
		   }
		   
	/*	   EmtsResponse obj= getEmtsResponse(url);
		   if(obj != null)
		   {
			    List recs = obj.getItems();
			   
			    if(recs != null && recs.size() > 0)
			    {
			    	Map<String, Integer> countMap  = (Map<String, Integer>)recs.get(0);
			    	
			    	campStatus.setTotalclicks(countMap.get("totalclicks"));
			    	
			    	campStatus.setTotalopens(countMap.get("totalopens"));
			    	
			    	campStatus.setUniqueclicks(countMap.get("uniqueclicks"));
			    	
			    	campStatus.setUniqueopens(countMap.get("uniqueopens"));
			    	
			    	campStatus.setUnsubscribe(countMap.get("unsubscribe"));
			    	
			    }
		   }*/
		   System.out.println("search urls "+ url);
	       ObjectMapper wrapper = new ObjectMapper();
	       MediaType jsonMimeType = MediaType.APPLICATION_JSON;
	   	   Map<String, Object> finalMap = new HashMap<String, Object>();
           AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
          
			finalMap.put("results", campStatus);
			String jsonString = wrapper.writeValueAsString(finalMap);
			
			System.out.println("jsonString:"+jsonString);
			
	       if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {
		      try {
		       stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));
		      } catch (IOException e) {
		       e.printStackTrace();
		      } catch (HttpMessageNotWritableException e1) {
		       e1.printStackTrace();
		      }
	      }
		return null;
	 }

	 public Date addDays(Date date, int days)
	    {
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(date);
	        cal.add(Calendar.DATE, days); 
	        return cal.getTime();
	    }
	 
}


