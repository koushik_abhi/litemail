package com.kenscio.litemail.workflow.controller.reports;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.client.ClientProtocolException;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.json.JSONException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.domain.CampaignLinks;
import com.kenscio.litemail.workflow.domain.EmtsResponse;
import com.kenscio.litemail.workflow.service.CampaignReportService;
import com.kenscio.litemail.workflow.service.CampaignService;

public class GetCampaignClickThroughStatusController implements Controller{
	
	   
	@RequestMapping(value="/getClickthroughStatus.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		  int width = 450;
		  int height = 300;
		String baseUrl    =     ApplicationProperty.getProperty("emts.redirect.url");
		String platFormId =     ApplicationProperty.getProperty("emts.platform.id");
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		long accountId = (Long) session.getAttribute("accountId");
		String campaignId=request.getParameter("campaignId");
	        final JFreeChart chart = ChartFactory.createBarChart3D(
	            "Click Through Analysis",      // chart title
	            "URL's",               // domain axis label
	            "Clicks",                  // range axis label
	            createDataset(baseUrl, campaignId,platFormId,accountId),                  // data
	            PlotOrientation.VERTICAL, // orientation
	            true,                     // include legend
	            true,                     // tooltips
	            false                     // urls
	        );
	        final CategoryPlot plot = chart.getCategoryPlot();
	                final CategoryAxis axis = plot.getDomainAxis();
	               axis.setCategoryLabelPositions(
	                    CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 8.0)

	                );

	                axis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);
	                CategoryItemRenderer renderer = plot.getRenderer();
	                renderer.setItemLabelsVisible(true);
	                DecimalFormat decimalformat1 = new DecimalFormat("##,###");

	                renderer.setItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2}", decimalformat1));

	                BarRenderer r = (BarRenderer) renderer;
	                r.setMaximumBarWidth(0.05);
	        renderer.setItemLabelGenerator(new StandardCategoryItemLabelGenerator("{2}", decimalformat1)); //i added your line here.
	        renderer.setItemLabelsVisible(true);
	        renderer.setBaseItemLabelsVisible(true);
	        chart.getCategoryPlot().setRenderer(renderer);
	        ChartUtilities.writeChartAsJPEG(response.getOutputStream(),chart,width,height);
	        return null;
	}
	
	private CategoryDataset createDataset(String baseUrl, String campaignIdstr, String platFormId, long accountId) throws ClientProtocolException, IOException, JSONException  {
		long campaignId = Long.parseLong(campaignIdstr);
		String instance = ApplicationProperty.getProperty("litemail.instance");
		 String url=baseUrl+"/ViewMailLinkClickCountApi.htm?pId="+platFormId+"&clId="+instance+"_"+accountId+"&cId="+campaignId;
		 CampaignReportService reportservice = new CampaignReportService();
	     EmtsResponse emtsObject = reportservice.getEmtsResponse(url);
	     List<String> urlList=new ArrayList<String>();
	     List<Double> totalClicks=new ArrayList<Double>();
	     
	     List<CampaignLinks>  links = new ArrayList<CampaignLinks>();
	     if(emtsObject != null && emtsObject.getItems() != null && emtsObject.getItems().size()>0)
	       {
	    	 
	    	   @SuppressWarnings("unchecked")
	    	   Map<String, Integer> positionCounts = ( Map<String, Integer>)emtsObject.getItems().get(0);
	    	   CampaignService serv = new CampaignService();
		       links = serv.getCampaingLinks(accountId, campaignId);
		       
		       for(CampaignLinks cliks : links)
		       {
		    	   if(positionCounts != null && positionCounts.size() > 0)
		    	   {
		    		   String key = cliks.getPosition()+"".trim();
		    		   urlList.add(key);
		    		   
		    		   if(positionCounts.containsKey(key))
		    		   {
		    			   int size = positionCounts.get(key);
		    			   cliks.setNoOfClicks(new Long(size));
		    		   }
		    		   if(cliks.getNoOfClicks() != null)
		    			   totalClicks.add(cliks.getNoOfClicks().doubleValue());
		    		   else
		    			   totalClicks.add(0.0);
		    	   }
		    	  
		       }
	    	   
	       }
	    	  
        String[] xAxisLabels = (String[])urlList.toArray(new String[urlList.size()]);
        final Double[] data =  totalClicks.toArray(new Double[totalClicks.size()]);
       
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0; i < data.length; i++) {
            double doubles = data[i];
            dataset.addValue(doubles,"Clicks",xAxisLabels[i]);
        }
      
        return dataset;
        
	}

	
	/*private CategoryDataset createDataset(String baseUrl, String campaignIdstr, String platFormId, long accountId) throws ClientProtocolException, IOException, JSONException  {
		long campaignId = Long.parseLong(campaignIdstr);
		String url=baseUrl+"/ViewMailLinkClickCountApi.htm?pId="+platFormId+"&clId="+platFormId+"_"+accountId+"&cId="+campaignId;
		 CampaignReportService reportservice = new CampaignReportService();
	     EmtsResponse emtsObject = reportservice.getEmtsResponse(url);
	     List<String> urlList=null;
	     List<Double> totalClicks=null;
	     if(emtsObject != null && emtsObject.getItems() != null && emtsObject.getItems().size()>0)
	       {
	    	 
	    	   @SuppressWarnings("unchecked")
	    	   Map<String, Double> positionCounts = ( Map<String, Double>)emtsObject.getItems().get(0);
	    	   urlList = new ArrayList<String>(positionCounts.keySet()); 
	    	   System.out.println("url is " + url + " urllist "+ urlList.size() + "posi "+ positionCounts);
	    	   totalClicks = new ArrayList<Double>(positionCounts.values());
	    	  
	    	  
	       }
	   
        String[] xAxisLabels = (String[])urlList.toArray(new String[urlList.size()]);
        final Integer[] data =  totalClicks.toArray(new Integer[totalClicks.size()]);
       
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0; i < data.length; i++) {
            double doubles = data[i];
           // System.out.println("doubles "+ doubles);
           // System.out.println("xAxisLabels "+ xAxisLabels[i]);
            dataset.addValue(doubles,"Clicks",xAxisLabels[i]);
        }
      
        return dataset;
        
	}
	
	/*private CategoryDataset createDataset(String baseUrl, String campaignIdstr, String platFormId, long accountId) throws ClientProtocolException, IOException, JSONException  {
		long campaignId = Long.parseLong(campaignIdstr);
	    baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		String url=baseUrl+"/ViewMailLinkClickCountApi.htm?pId="+platFormId+"&clId="+platFormId+"_"+accountId+"&cId="+campaignId;
		CampaignReportService service=new CampaignReportService();
		EmtsResponse object=service.getEmtsResponse(url);
		
		Map<Long, String> positionUrls = getActualUrls(accountId, campaignId);
        List<String> urlList = new ArrayList<String>();
        List<Double> totalClicks = new ArrayList<Double>();
		if(object != null)
		{
			List items = object.getItems();
		          
	        if(items != null && items.size() >0)
	        {
	        	Map<Long, Long> map = (Map<Long, Long>)items.get(0);
	        	if(map != null && map.size() > 0)
	        	{
	        		
		        	Set<Long> keys = map.keySet();
		        	if(keys != null)
		        	{
		        		
		        		Iterator<Long> l = keys.iterator();
						while(l.hasNext())
						{
							Object ob = l.next();
							if(ob != null && positionUrls.get(ob) != null)
							{
								urlList.add(positionUrls.get(ob));
								Object obj = map.get(ob);
								if(obj != null)
									totalClicks.add(new Double(obj.toString()));
							}
			        	}
			        	
		        	}
	        	}
	        	
	        }        
		}else
		{
			Set<Long> keys = positionUrls.keySet();
			if(keys != null)
        	{
				Iterator<Long> l = keys.iterator();
				while(l.hasNext())
				{
					Object ob = l.next();
					if(ob != null && positionUrls.get(ob) != null)
					{
						urlList.add(positionUrls.get(ob));
						totalClicks.add(new Double(0l));
					}
	        	}
        	}
			
		}
        
        String[] xAxisLabels = (String[])urlList.toArray(new String[urlList.size()]);
        String xAxisTitle = "URL's";
        String yAxisTitle = "Click Through's";
        String title = "Click Through Analysis";
        final Double[] data =  (Double[])totalClicks.toArray(new Double[totalClicks.size()]);
       
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0; i < data.length; i++) {
            double doubles = data[i];
           // System.out.println("doubles "+ doubles);
          //  System.out.println("xAxisLabels "+ xAxisLabels[i]);
            dataset.addValue(doubles,"Clicks",xAxisLabels[i]);
        }
        return dataset;
        
	}*/

	private Map<Long, String> getActualUrls(long accountId, long campaignId) {
		CampaignReportService service=new CampaignReportService();
		return service.getActualUrls(accountId, campaignId);
		
	}

	 
}
