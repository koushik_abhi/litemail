package com.kenscio.litemail.workflow.controller.reports;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import au.com.bytecode.opencsv.CSVWriter;

import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.SmtpEntity;
import com.kenscio.litemail.workflow.service.CampaignDownloadService;
import com.kenscio.litemail.workflow.service.CampaignReportService;
import com.kenscio.litemail.workflow.service.SmtpService;

@Controller
public class CampaignReportDownloadController {
	
	
	@RequestMapping(value="/getmailFailedDownload.htm", method=RequestMethod.GET)
	public ModelAndView getmailFailedDownload(HttpServletRequest request,HttpServletResponse response) throws IOException{
	HttpSession session=request.getSession(false);
	if(session == null)
	{
		request.setAttribute("errorMessage","session expired, please login again ");
		return new ModelAndView("loginscreen.jsp");
	}

	long accountId = (Long) session.getAttribute("accountId");
	String campaignId=request.getParameter("campaignId");
	Long campaignID = new Long(campaignId);
	String fromDate = request.getParameter("fromdate");
    String toDate = request.getParameter("todate");
	OutputStreamWriter outputwriter = null;
	try{
	
	
	String fileName = campaignId;

	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-disposition", "attachment;filename="+fileName+"_data_export.csv");  
    OutputStream fout= response.getOutputStream();  
    OutputStream bos = new BufferedOutputStream(fout);   
    outputwriter = new OutputStreamWriter(bos);  
    CSVWriter writer = new CSVWriter(outputwriter);  
    CampaignDownloadService impl = new CampaignDownloadService();
    if(fromDate == null || fromDate == "" ){
    impl.exportToExcelMailFailed(writer, campaignID,accountId); 
    }
    else
    {
    	   SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	       Date fd = formatter.parse(fromDate);
	       Date td = formatter.parse(toDate);
	       impl.exportToExcelMailFailedDateFilter(writer, campaignID,accountId,fd,td);
    }
             
    outputwriter.flush();   
    outputwriter.close();  
           

	}catch(Exception e)
	{
		e.printStackTrace();
	}finally{
		if(outputwriter != null)
			outputwriter.close();
	}

	return null;
}
	
@RequestMapping(value="/getMailSentSoFarDownload.htm", method=RequestMethod.GET)
public ModelAndView getMailSentSoFarDownload(HttpServletRequest request,HttpServletResponse response) throws IOException{
   System.out.println("inside MailSent download");
	HttpSession session=request.getSession(false);
	if(session == null)
	{
		request.setAttribute("errorMessage","session expired, please login again ");
		return new ModelAndView("loginscreen.jsp");
	}

	long accountId = (Long) session.getAttribute("accountId");
	String campaignId=request.getParameter("campaignId");
	Long campaignID = new Long(campaignId);
	String fromDate = request.getParameter("fromdate");
    String toDate = request.getParameter("todate");
	OutputStreamWriter outputwriter = null;
	try{
	
	
	String fileName = campaignId;

	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-disposition", "attachment;filename="+fileName+"_data_export.csv");  
    OutputStream fout= response.getOutputStream();  
    OutputStream bos = new BufferedOutputStream(fout);   
    outputwriter = new OutputStreamWriter(bos);  
    CSVWriter writer = new CSVWriter(outputwriter);  
    CampaignDownloadService impl = new CampaignDownloadService();
    if(fromDate == null || fromDate == "" ){
    impl.exportToExcelMailSentSoFar(writer,campaignId,accountId);
    }
    else
    {
    	   SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	       Date fd = formatter.parse(fromDate);
	       Date td = formatter.parse(toDate);
	       impl.exportToExcelMailSentSoFar(writer, campaignID,accountId,fd,td);
    }
             
    outputwriter.flush();   
    outputwriter.close();  
           

	}catch(Exception e)
	{
		e.printStackTrace();
	}finally{
		if(outputwriter != null)
			outputwriter.close();
	}

	return null;
}

	
@RequestMapping(value="/campaignReportsDownload.htm", method=RequestMethod.GET)
	public String campaignReportsDownload(HttpServletRequest request,HttpServletResponse response) throws IOException, ParseException
	{
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return "loginscreen.jsp";
		}
	
		long accountId = (Long) session.getAttribute("accountId");
		String campaignId=request.getParameter("campaignId");
		Long campaignID = new Long(campaignId);
		String fromDate = request.getParameter("fromdate");
	    String toDate = request.getParameter("todate");
		String viewUrl=request.getParameter("url");
		String baseUrl    =     ApplicationProperty.getProperty("emts.redirect.url");
	    String platFormId =     ApplicationProperty.getProperty("emts.platform.id");
	    String instance = ApplicationProperty.getProperty("litemail.instance");
	    String url="";
	    if(fromDate == null || fromDate == "" ){
	    	url=baseUrl+"/"+viewUrl+"?pId="+platFormId+"&clId="+instance+"_"+accountId+"&cId="+campaignID;
	    }
	    else
	    {
	    	 
	    	 url=baseUrl+viewUrl+"?pId="+platFormId+"&clId="+instance+"_"+accountId+"&cId="+campaignID+"&fd="+fromDate+"&td="+toDate;	
	    }
	    System.out.println("download url :"+url);
		return "redirect:"+url;
	}
	
	
	@RequestMapping(value="/exportBounces.htm", method=RequestMethod.GET)
	public ModelAndView exportBounces(HttpServletRequest request,HttpServletResponse response) throws IOException{
	HttpSession session=request.getSession(false);
	if(session == null)
	{
		request.setAttribute("errorMessage","session expired, please login again ");
		return new ModelAndView("loginscreen.jsp");
	}

	long accountId = (Long) session.getAttribute("accountId");
	String campaignId=request.getParameter("campaignId");
	String bounceType=request.getParameter("bounceType");
	Long campaignID = new Long(campaignId);
	String fromDate = request.getParameter("fromdate");
    String toDate = request.getParameter("todate");
	System.out.println("FromDAte:"+fromDate);
	OutputStreamWriter outputwriter = null;
	   CampaignReportService reportservice = new CampaignReportService();
	    Campaign report = reportservice.getCampaignStatusReport(accountId, campaignID);
	    
	    
	    String cmpStarTime  = "";
		Date campaignStartTime = report.getCampaignStartDateTime();
		Date cendTime=report.getCampaignEndDateTime();
		if(campaignStartTime == null)
		{
			campaignStartTime = report.getCreatedDate();
		}
		String fromTime = "";
		try{
			
			  SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			  fromTime = formatter.format(campaignStartTime);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		if(cendTime == null)
		{
			cendTime = new Date();
		}
		
		//long fromTime = time.getTime()-(6*60*60*1000);
		String dm = getDomainFromAddress(report);
		String sp=report.getSmtpServer();
		if(sp != null && sp.contains("mailgun") )
		{
			sp = "mailgun";
		}else
		{
			sp = "amazon";
		}
	try{
	
	
	String fileName = campaignId;

	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-disposition", "attachment;filename="+fileName+"_data_export.csv");  
    OutputStream fout= response.getOutputStream();  
    OutputStream bos = new BufferedOutputStream(fout);   
    outputwriter = new OutputStreamWriter(bos);  
    CSVWriter writer = new CSVWriter(outputwriter);  
    CampaignDownloadService impl = new CampaignDownloadService();
    if(fromDate == null || fromDate == "" ){
    	
    impl.exportToExcelMailBounces(writer, campaignID,accountId,bounceType,sp,dm); 
    }
    else
    {
    	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	       Date fd = formatter.parse(fromDate);
	       Date td = formatter.parse(toDate);
    	 impl.exportToExcelMailBouncesDateFilter(writer, campaignID,accountId,bounceType,fd,td,sp,dm); 	
    }
    outputwriter.flush();   
    outputwriter.close();  
           

	}catch(Exception e)
	{
		e.printStackTrace();
	}finally{
		if(outputwriter != null)
			outputwriter.close();
	}

	return null;
}
	
	@RequestMapping(value="/exportSpams.htm", method=RequestMethod.GET)
	public ModelAndView exportSpams(HttpServletRequest request,HttpServletResponse response) throws IOException{
	HttpSession session=request.getSession(false);
	if(session == null)
	{
		request.setAttribute("errorMessage","session expired, please login again ");
		return new ModelAndView("loginscreen.jsp");
	}

	long accountId = (Long) session.getAttribute("accountId");
	String campaignId=request.getParameter("campaignId");
	Long campaignID = new Long(campaignId);
	String fromDate = request.getParameter("fromdate");
    String toDate = request.getParameter("todate");
	
    CampaignReportService reportservice = new CampaignReportService();
    Campaign report = reportservice.getCampaignStatusReport(accountId, campaignID);
    
    
    String cmpStarTime  = "";
	Date campaignStartTime = report.getCampaignStartDateTime();
	Date cendTime=report.getCampaignEndDateTime();
	if(campaignStartTime == null)
	{
		campaignStartTime = report.getCreatedDate();
	}
	String fromTime = "";
	try{
		
		  SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		  fromTime = formatter.format(campaignStartTime);
	}catch(Exception e)
	{
		e.printStackTrace();
	}
	if(cendTime == null)
	{
		cendTime = new Date();
	}
	
	//long fromTime = time.getTime()-(6*60*60*1000);
	String dm = getDomainFromAddress(report);
	String sp=report.getSmtpServer();
	if(sp != null && sp.contains("mailgun") )
	{
		sp = "mailgun";
	}else
	{
		sp = "amazon";
	}
	OutputStreamWriter outputwriter = null;
	try{
	
	
	String fileName = campaignId;

	response.setContentType("application/vnd.ms-excel");
	response.setHeader("Content-disposition", "attachment;filename="+fileName+"_data_export.csv");  
    OutputStream fout= response.getOutputStream();  
    OutputStream bos = new BufferedOutputStream(fout);   
    outputwriter = new OutputStreamWriter(bos);  
    CSVWriter writer = new CSVWriter(outputwriter);  
    CampaignDownloadService impl = new CampaignDownloadService();
    if(fromDate == null || fromDate == "" ){
    impl.exportToExcelMailSpams(writer, campaignID,accountId,sp,dm); 
    }
    else
    {
    	 SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	       Date fd = formatter.parse(fromDate);
	       Date td = formatter.parse(toDate);
    	 impl.exportToExcelMailSpamsDateFilter(writer, campaignID,accountId,fd,td,sp,dm); 	
    }
    outputwriter.flush();   
    outputwriter.close();  
           

	}catch(Exception e)
	{
		e.printStackTrace();
	}finally{
		if(outputwriter != null)
			outputwriter.close();
	}

	return null;
}
	
	 private String getDomainFromAddress(Campaign report) {
			SmtpService ser = new SmtpService();
			SmtpEntity se = ser.getSmtpDetails(report.getSmtpServer());
			if(se != null && se.getSmtpServer().contains("mailgun"))
			{
				return getDomainName(se.getSmtpUser());
			}
			else
			{
				String str = report.getFromAddress();
				return getDomainName(str); //get domain from fromaddress for amazon
			}
			
	}
	 private String getDomainName(String smtpUser) {
			
			if(smtpUser != null && smtpUser.length() > 1)
			{
				int i = smtpUser.indexOf("@");
				return smtpUser.substring(i+1);
			}
			return "";
		}
}
