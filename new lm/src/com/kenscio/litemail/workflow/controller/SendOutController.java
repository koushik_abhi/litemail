package com.kenscio.litemail.workflow.controller;


import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.JobDetailsObject;
import com.kenscio.litemail.workflow.service.CampaignService;
import com.kenscio.litemail.workflow.service.JobsManagementImpl;



public class SendOutController  implements Controller{

	@Override
	@RequestMapping(value="/schedulelater.htm",method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		Long accountId=(Long) session.getAttribute("accountId");
		String campaignName = request.getParameter("campaignName");
		String listName=request.getParameter("listName");
		String scheduleDate=request.getParameter("scheduleDate");
		String speed = request.getParameter("mailsperminute");
		Long mailsPerMinute = 5000l;
		if(speed != null && speed.length() > 0)
		{
			mailsPerMinute=Long.parseLong(speed);
		}
		String status="scheduled";
		
		JobDetailsObject jobDetails=new JobDetailsObject();
		jobDetails.setCampaignName(campaignName);
		jobDetails.setListName(listName);
		jobDetails.setScheduleDate(scheduleDate);
		
		System.out.println("camp name "+ campaignName + "schedule dat " +scheduleDate );
		
		JobsManagementImpl impl=new JobsManagementImpl();
		//impl.deleteJob(jobDetails);
		impl.addJob(jobDetails);
		CampaignService  service=new CampaignService();
		service.updateCampaign(accountId, campaignName, mailsPerMinute,status,scheduleDate);
		ObjectMapper mapper = new ObjectMapper();
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        mapper.getDeserializationConfig().setDateFormat(df);
        String jsonString = "";
        Map<String, String> finalMap = new HashMap<String, String>();
        AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
        finalMap.put("result", scheduleDate);
        if(scheduleDate == null || scheduleDate.length()<0)
        {
	          Map<String, String> map = new HashMap<String, String>();
	          map.put("errorMessage", "No  records found.");
	          
        }else
        {
               jsonString = mapper.writeValueAsString(finalMap);
        }
              
        MediaType jsonMimeType = MediaType.APPLICATION_JSON;

        if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) 
        {

			try {
	
					stringHttpMessageConverter.write(jsonString, jsonMimeType,
						new ServletServerHttpResponse(response));
				
	
			} catch (IOException e) {
				e.printStackTrace();
	
			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}

        }
           // return new ModelAndView("sendoutdirect.jsp");
        return null;
	}
	
}