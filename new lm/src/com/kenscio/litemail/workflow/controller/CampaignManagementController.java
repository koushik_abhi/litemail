package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.service.CampaignService;

public class CampaignManagementController implements Controller {

	@Override
	@RequestMapping(value = "/displayCampaigns.htm", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(false);
		if (session == null) {
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		Long accountId = (Long) session.getAttribute("accountId");
		String partnerName=(String) session.getAttribute("partnerName");
		String userName = (String) request.getSession(false).getAttribute("login");
		Integer pageNumber = 1;
		String pagenum = request.getParameter("pagenumber");
		if (pagenum != null) {
			pageNumber = new Integer(pagenum);
		}
		int noOfRecordsperPage = 10;
		float totalRecords = 0;
		ObjectMapper mapper = new ObjectMapper();
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mapper.getDeserializationConfig().setDateFormat(df);
		String jsonString = "";
		Map<String, Object> finalMap = new HashMap<String, Object>();
		CampaignService service = new CampaignService();
		List<Campaign> camps = service.getCampaignsInPagination(partnerName,pageNumber, noOfRecordsperPage);
		totalRecords = service.getTotalPagesOfCampaign(partnerName);
		System.out.println(totalRecords + "total records");
		double totalPages = totalRecords / noOfRecordsperPage;
		totalPages = Math.ceil(totalPages);
		double temp = noOfRecordsperPage*totalPages;
		if(totalRecords > temp)
			totalPages = totalPages+1;

		AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
		finalMap.put("result", camps);
		finalMap.put("totalpages", totalPages);
		finalMap.put("pagenumber", pageNumber);

		if (camps == null || camps.size() <= 0) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("errorMessage", "No campaign records found.");

		} else {
			jsonString = mapper.writeValueAsString(finalMap);
		}

		MediaType jsonMimeType = MediaType.APPLICATION_JSON;

		if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType,
						new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}
		}
		return null;

	}

}
