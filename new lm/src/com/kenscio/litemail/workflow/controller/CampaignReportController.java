package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.CampaignBounce;
import com.kenscio.litemail.workflow.domain.CampaignStatusCount;
import com.kenscio.litemail.workflow.domain.EmtsResponse;
import com.kenscio.litemail.workflow.domain.SmtpEntity;
import com.kenscio.litemail.workflow.service.CampaignReportService;
import com.kenscio.litemail.workflow.service.SmtpService;

public class CampaignReportController implements Controller{

	@RequestMapping(value="/displaycampaignreport.htm", method=RequestMethod.POST)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		long accountId = (Long)session.getAttribute("accountId");
		String campaignId = request.getParameter("campaignId");
		Long campaignID = new Long(campaignId);
		CampaignReportService reportservice = new CampaignReportService();
	    Campaign report = reportservice.getCampaignStatusReport(accountId, campaignID);
	/*    List<CampaignBounce> bounceCount = reportservice.getCampaignMailBounceCount(accountId, campaignID);
	   for (int i = 0; i < bounceCount.size(); i++) {
		   System.out.println(bounceCount.get(i) + "bounce count");
		
	}*/
		String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		String instance = ApplicationProperty.getProperty("litemail.instance");
		String cmpStarTime  = "";
		Date time = report.getCampaignStartDateTime();
		Date endTime=report.getCampaignEndDateTime();
		
		if(time == null)
		{
			time = report.getCreatedDate();
		}
		String fromTime = "";
		try{
			
			  SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			  fromTime = formatter.format(time);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		if(endTime == null)
		{
			endTime = new Date();
		}
		
		//long fromTime = time.getTime()-(6*60*60*1000);
		String dm = getDomainFromAddress(report);
		String sp=report.getSmtpServer();
		if(sp != null && sp.contains("mailgun") )
		{
			sp = "mailgun";
		}else
		{
			sp = "amazon";
		}
		CampaignBounce bounceCountByDomain=reportservice.getCampaignMailBounceCountByDomain(campaignID,dm,sp,time,endTime);
		String url=baseUrl+"/"+"getAllCampaignStatusCounts.htm?pId="+platFormId+"&dm="+dm+"&clId="+instance+"_"+accountId+"&cId="+campaignID+"&cst="+fromTime;
		CampaignStatusCount campStatus = new CampaignStatusCount();
		if(bounceCountByDomain != null)
		{
		    campStatus.setGeneralbounce(bounceCountByDomain.getGeneralBounce());
	    	campStatus.setHardbounce(bounceCountByDomain.getHardBounce());
	    	
	    	campStatus.setSoftbounce(bounceCountByDomain.getSoftBounce());
	    	
	    	campStatus.setSpam(bounceCountByDomain.getSpamCount());
	    	
	    	campStatus.setTotalbounce(bounceCountByDomain.getTotalNoOfBounces());

	    	campStatus.setTransientbounce(bounceCountByDomain.getTransientBounce());
		}
		EmtsResponse obj= getEmtsResponse(url, cmpStarTime);
		if(obj != null)
		{
		    List recs = obj.getItems();
		   
		    if(recs != null && recs.size() > 0)
		    {
		    	Map<String, Integer> countMap  = (Map<String, Integer>)recs.get(0);
		    	//campStatus.setGeneralbounce(bounceCountByDomain.getGeneralBounce());
		    	//campStatus.setHardbounce(bounceCountByDomain.getHardBounce());
		    	
		    	//campStatus.setSoftbounce(bounceCountByDomain.getSoftBounce());
		    	
		    	//campStatus.setSpam(bounceCountByDomain.getSpamCount());
		    	//campStatus.setTotalbounce(bounceCountByDomain.getTotalNoOfBounces());
		    	//if(countMap.get("transientbounce") != null)
		      //	campStatus.setTransientbounce(bounceCountByDomain.getTransientBounce());
		    	
		    	campStatus.setTotalclicks(countMap.get("totalclicks"));
		    	
		    	campStatus.setTotalopens(countMap.get("totalopens"));
		    	
		    	campStatus.setUniqueclicks(countMap.get("uniqueclicks"));
		    	
		    	campStatus.setUniqueopens(countMap.get("uniqueopens"));
		    	
		    	campStatus.setUnsubscribe(countMap.get("unsubscribe"));
		    	
		    }
		}
		request.setAttribute("campaignId", campaignId);
		String jsonString = "";
		ObjectMapper mapper = new ObjectMapper();
	//mapper.write
		
		AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
		Map<String, Object> finalMap = new HashMap<String, Object>();
		finalMap.put("report", report);
		finalMap.put("counts", campStatus);
		//finalMap.put("bounce", bounceCount);
		jsonString = mapper.writeValueAsString(finalMap);
		MediaType jsonMimeType = MediaType.APPLICATION_JSON;
        if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}

		}

	
		
		return null;
	}

	private String getDomainFromAddress(Campaign report) {
		SmtpService ser = new SmtpService();
		SmtpEntity se = ser.getSmtpDetails(report.getSmtpServer());
		if(se != null && se.getSmtpServer().contains("mailgun"))
		{
			return getDomainName(se.getSmtpUser());
		}
		else
		{
			String str = report.getFromAddress();
			return getDomainName(str); //get domain from fromaddress for amazon
		}
		
	}

	private String getDomainName(String smtpUser) {
		
		if(smtpUser != null && smtpUser.length() > 1)
		{
			int i = smtpUser.indexOf("@");
			return smtpUser.substring(i+1);
		}
		return "";
	}
		
	protected String addCstToUrl(String url, String cst){
	  
	    List<NameValuePair> params = new LinkedList<NameValuePair>();

	    if (cst != null){
	        params.add(new BasicNameValuePair("cst", cst));
	       
	    }	  
	    String paramString = URLEncodedUtils.format(params, "utf-8");
	    url += "&"+paramString;
	    return url;
	}
	
	public EmtsResponse getEmtsResponse(String url, String cst)
    {
		//String csturl = addCstToUrl(url,cst);
        HttpClient client;
        EmtsResponse emtsObject;
        client = new DefaultHttpClient();
        emtsObject = new EmtsResponse();
        try
        {
        	System.out.println("url   "+ url);
        	HttpGet  httpost = new HttpGet(url);
            
            HttpResponse response = client.execute(httpost);
            ObjectMapper wrapper = new ObjectMapper();
            emtsObject = (EmtsResponse)wrapper.readValue(response.getEntity().getContent(), EmtsResponse.class);
            client.getConnectionManager().shutdown();
            
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        client.getConnectionManager().shutdown();
		return emtsObject;
       
    }

}
