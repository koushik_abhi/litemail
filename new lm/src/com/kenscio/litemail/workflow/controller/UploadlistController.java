package com.kenscio.litemail.workflow.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kenscio.litemail.util.DataServiceUtilLiteMail;
import com.kenscio.litemail.workflow.domain.ListEntity;
import com.kenscio.litemail.workflow.service.AttributeService;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;
import com.kenscio.litemail.workflow.service.UploadListServiceUtilAsync;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;



public class UploadlistController implements Controller
{

	public static Logger log = Logger.getLogger(UploadlistController.class.getName());
	
	@Override
	@RequestMapping(value="/uploadlist.htm",method=RequestMethod.POST)
	public ModelAndView handleRequest(HttpServletRequest request,
		HttpServletResponse response) throws Exception {
			
			
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
			String partnerName=(String) session.getAttribute("partnerName");
			String listname="";//request.getParameter("listname");
			String description="";
			String uploadOpt=request.getParameter("uploadopt");
			String syncMode=request.getParameter("syncmode");
			String mode=request.getParameter("mode");
			String filename = "";
			InputStream in = null;
			String work_flow="";
			String userName =(String)request.getSession(false).getAttribute("login");
			Long accountId =(Long)request.getSession(false).getAttribute("accountId");
			
		  	boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		  	boolean listAlreadyExist = false;
			if(isMultipart)
			{
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List items = null;
				try {
					
					items = upload.parseRequest(request);
					
				} catch (FileUploadException e) 
				{
					e.printStackTrace();
					
				}
				Iterator itr = items.iterator();
				while (itr.hasNext()) 
				{
					FileItem item = (FileItem) itr.next();
										
					if (item.isFormField()) 
					{
						String name = item.getFieldName();
						String value = item.getString();
						
						if (name.equals("listname")) 
						{
							listname = value;	
							request.setAttribute("listname", listname);
						}else if(name.equals("description"))
						{
							description = value;
						}
						else if(name.equals("syncmode"))
						{
							syncMode = value;
						}
						else if(name.equals("mode"))
						{
							mode = value;
						}
						else if(name.equals("datasource"))
						{
							uploadOpt = value;
						}
						else if(name.equals("work_flow"))
						{
							work_flow = value;
						}
					} else {
					
						if(filename == null || filename =="")
						{
							if(item.getName() != null && item.getName() != "" )
							{
								filename = item.getName();
								in = item.getInputStream();
							}
						}
						
						//FileInputStream st  = new FileInputStream(item.getInputStream());
										
					}
				}
			}else
			{
				
				request.setAttribute("errorMessage", "request is not file upload, please select correct request ");
			}
			MailListServiceImpl impl = new MailListServiceImpl();
			//System.out.println("list name "+ listname + "syncMode "+ syncMode + "uploadOpt " +uploadOpt);
			if(listname != null && listname.length() > 0)
		    {
				ListEntity listObj = impl.validateListName(listname, accountId);
				if(listObj != null)
			    {
					listAlreadyExist = true;
					listObj.setListDescription(description);
					listObj.setStatus("uploading");
					listObj.setListFileName(filename);
				    Calendar calendar11 = Calendar.getInstance();
					Date myDate11 = new Date(); // May be your date too.
					calendar11.setTime(myDate11);
					listObj.setUploadTime(calendar11.getTime());
					listObj.setUpdatedBy(userName);
					listObj.setUpdatedDate(calendar11.getTime());
					listObj.setPartnerName(partnerName);
			    	//request.setAttribute("errorMessage", "list name already exist, please create with different name "+ listname);
			    	//return new ModelAndView("uploadlist.jsp?listname="+listname);
			    	
			    }else
			    {
			    	listObj = new ListEntity();
				    listObj.setListName(listname);
				    listObj.setAccountId(accountId);
				    listObj.setListDescription(description);
				    listObj.setCreatedBy(userName);
				    listObj.setUpdatedBy(userName);
				    listObj.setStatus("uploading");
				    listObj.setListFileName(filename);
				    Calendar calendar11 = Calendar.getInstance();
					Date myDate11 = new Date(); // May be your date too.
					calendar11.setTime(myDate11);
				    listObj.setUploadTime(calendar11.getTime());
				    listObj.setCreatedDate(calendar11.getTime());	
				   listObj.setPartnerName(partnerName);
			    }
				request.setAttribute("listname", listname);
				
		    	List<String> headers = new ArrayList<String>();
		    	// List<String> invheader = new ArrayList<String>();
		      //  List<List<String>> rows = new ArrayList<List<String>>();
		        List<String> dbattr  = null;
		        
		        if(uploadOpt != null && uploadOpt.equals("fileupload"))
		        {
		        	if(syncMode == null || syncMode == ""  )
				    {
				    	syncMode = "new";
				    }		    
					if (filename != "" && (filename.contains(".csv") || (filename.contains(".txt")))) 
					{
						
						try{
					    	if(!listAlreadyExist)
					    	{
					    		
					    		impl.addMailingListOnly(listObj);
					    		
					    	}else
					    	{
					    		
					    		impl.addNewUpdateMailingList(listObj);
					    	}
					    	
					    }catch(Exception e)
					    {
					    	e.printStackTrace();
					    	if(work_flow.equals("true"))
					    	{
					    		request.setAttribute("errorMessage", "exception occured in creating new list, please try again "+ listname);
					    		return new ModelAndView("createlist_wf.jsp?listname="+listname);
					    	}
					    	else
					    	{
					    		request.setAttribute("errorMessage", "exception occured in creating new list, please try again "+ listname);
						    	return new ModelAndView("createlist.jsp?listname="+listname);
					    	}
					    }
						List<ListEntity> liObj = impl.getListDetailsForUserAndlistname(accountId, listname);
						long listId = 0;
						if(liObj != null && liObj.size() > 0)
							listId = liObj.get(0).getListId();
						AttributeService attimpl = new AttributeService();
						dbattr = attimpl.getAllAttributesNamesOnly(accountId);
						//System.out.println("dbattr "+ dbattr);
						List<String> invheader = new ArrayList<String>();
			            if(!readDataAndPopulateHeadersRunAsync(request, listId, userName, headers, filename, in,  invheader, dbattr, syncMode,mode ))
			            {
			            	//revert changes as data is not uploaded 
			            	try{
			            		if(!listAlreadyExist)
						    	{
			            			impl.delteMailingList(listId, accountId);
						    	}
						    	
						    }catch(Exception e)
						    {
						    	e.printStackTrace();
						    	if(work_flow.equals("true"))
						    	{
						    		request.setAttribute("errorMessage", "exception occured in creating new list, please try again "+ listname);
						    		return new ModelAndView("createlist_wf.jsp?listname="+listname);
						    	}
						    	else
						    	{
						    		request.setAttribute("errorMessage", "exception occured in creating new list, please try again "+ listname);
							    	return new ModelAndView("createlist.jsp?listname="+listname);
						    	}
						    }//request.setAttribute("errorMessage", " unable to upload complete file : "+filename + "invalid headers: " +invheader);
			            	if(work_flow.equals("true"))
					    	{
			            		return new ModelAndView("createlist_wf.jsp?listname="+listname);
					    	}
			            	else
			            	{
			            		return new ModelAndView("createlist_wf.jsp?listname="+listname);	
			            	}
			            }
			           				   		    
			          
					}else 
					{
						if(work_flow.equals("true"))
						{
						request.setAttribute("errorMessage", "Please upload a valid file, it supports only csv file." +filename);
						return new ModelAndView("createlist_wf.jsp?listname="+listname);
						}
						else
						{
							request.setAttribute("errorMessage", "Please upload a valid file, it supports only csv file." +filename);
							return new ModelAndView("createlist.jsp?listname="+listname);
						}
					}
				    
		        }
		    }
			request.setAttribute("listname", listname);
		if(work_flow.equals("true"))
		{
			return new ModelAndView("createCampaign_wf.jsp?listname="+listname);
		}
		else
		{
			return new ModelAndView("listManagement.jsp");
		}
			
		
	    
	}	
	
	private boolean readDataAndPopulateHeadersRunAsync(HttpServletRequest request, long listId, String username, List<String> headers,
			 String filename, InputStream in,  List<String> invheader, List<String> dbattr, String syncMode, String mode) {
		
		CSVReader reader =null;
		String  nextLine[]=new String[2000];
        try
        {
	         InputStreamReader fr =  null;
	         fr = new InputStreamReader(in);
	         reader = new CSVReader(fr);
	         int firstRow = 0;
	        
	         if ((nextLine = reader.readNext()) != null)
	         {
	          	           
	           if(firstRow ==0)
	           {
	        	  int c = 0;
	         	  for(int nIndex = 0; nIndex <nextLine.length;nIndex++)
	              {
	                  //System.out.print(nextLine[nIndex]+" ");
	                  if(nextLine[nIndex].length() > 0)
	                  {
	                	  headers.add(nextLine[nIndex].trim());
	                  }
	                  c++;
	                  if(c >60)
	                	  break;
	         		  
	              }
	         	  	
	         	  if(!DataServiceUtilLiteMail.validateMailListHeaders(headers, dbattr))
	         	  {
	         		 request.setAttribute("errorMessage", "invalid headers in the file  "+filename + " headers list :"+ headers);
	         		 return false;
	         	  }
	         	  	   
	         	  	           	  
	         	  
				   	         	  
	           }
	          
	           firstRow++;
	          	           
	        }
	        String filePath = writeFileIntoTemp(request,listId, filename, in, reader,nextLine );
	        if(filePath != null)
	        {
	        	populateDataInAsync(listId,username,headers,filename,dbattr,syncMode,mode, filePath);
	        	return true;
	        }
	         
        }catch(Exception e)
        {
	       	e.printStackTrace();
	       	log.log(Level.SEVERE, "error during upload file "+e.getMessage());
       	
        }
        
		return false;
	}
	
	private String writeFileIntoTemp(HttpServletRequest request, long listId, String filename, InputStream in,
			CSVReader reader, String[] headers) {
		File file = null;
		CSVWriter writer =null;
		try{
						 
			 long time  = System.currentTimeMillis();
			 String fileName = listId+"_"+time+".csv";
			 file = File.createTempFile(fileName,"");
			 FileOutputStream out = new FileOutputStream(file);
			 writer = new CSVWriter(new OutputStreamWriter(out));
			// writer.writeNext(headers);
			 String  nextLine[]=new String[2000];
			 while ((nextLine = reader.readNext()) != null)
	         {
				 writer.writeNext(nextLine);
	         }
			
			
		}catch(Exception e)
		{
			log.log(Level.SEVERE, "error in uploading file "+e.getMessage());
		}
		finally
		{
			
				try {
					if(writer != null)
					writer.close();
					if(reader != null)
						reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
		}
		return file.getAbsolutePath();
		
	}

	private void populateDataInAsync(long listId, String username, List<String> headers,
			 String filename, List<String> dbattr, String syncMode, String mode, String filePath) {
		
	   UploadListServiceUtilAsync upload = new UploadListServiceUtilAsync(listId,username,headers,filename,dbattr,syncMode, mode, filePath);
	   upload.uploadDataAsync();
	 
	}
			
				
	
}