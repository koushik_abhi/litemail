package com.kenscio.litemail.workflow.controller;


import com.kenscio.litemail.workflow.service.AccountService;
import com.kenscio.litemail.workflow.service.LoginServiceImpl;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginController implements Controller {
	@RequestMapping(value="/login.htm",method=RequestMethod.GET)
	 public ModelAndView handleRequest(HttpServletRequest request,
	   HttpServletResponse response) throws Exception {

	  String userName=request.getParameter("username");
	  String password=request.getParameter("password");
	  HttpSession session =request.getSession(true);
	  Long accountId=0l;
	  LoginServiceImpl impl=new LoginServiceImpl();
	  accountId=impl.login(userName, password);
	  AccountService service=new AccountService();
	  String partnerName=service.getPartnerName(accountId);
	  
	  if(accountId == 0)
	  {
		  request.setAttribute("errorMessage", "username or password is wrong");
		  return new ModelAndView("loginscreen.jsp");
	  }
	  else
	  {
		  session.setAttribute("accountId", accountId);
		  session.setAttribute("login", userName);
		  session.setAttribute("partnerName", partnerName);
		  return new ModelAndView("dashboard.jsp");
	  }
	  
	 }
		
}
