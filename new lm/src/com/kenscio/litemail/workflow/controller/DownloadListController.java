package com.kenscio.litemail.workflow.controller;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.service.AccountService;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;

import au.com.bytecode.opencsv.CSVWriter;

public class DownloadListController implements Controller{


	@RequestMapping(value = "/downloadlist.htm", method = RequestMethod.POST)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		long accountId = (Long) session.getAttribute("accountId");
		String listName=request.getParameter("listname");
		OutputStreamWriter outputwriter = null;
		try{
				
		String fileName = listName;
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-disposition", "attachment;filename="+fileName+"_data_export.csv");  
        OutputStream fout= response.getOutputStream();  
        OutputStream bos = new BufferedOutputStream(fout);   
        outputwriter = new OutputStreamWriter(bos);  
        CSVWriter writer = new CSVWriter(outputwriter);  
        MailListServiceImpl impl = new MailListServiceImpl();
        impl.exportListDataToExcel(writer, listName,accountId);
                 
        outputwriter.flush();   
        outputwriter.close();  
               
	
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally{
			if(outputwriter != null)
				outputwriter.close();
		}
	
		return null;
	}
}
