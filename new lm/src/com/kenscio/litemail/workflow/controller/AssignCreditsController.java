package com.kenscio.litemail.workflow.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.domain.AccountCredit;
import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.service.AccountService;

public class AssignCreditsController implements Controller{

	@RequestMapping(value = "/assigncredits.htm", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		Long accountId = (Long) session.getAttribute("accountId");
		String login = (String) session.getAttribute("login");
		String userName=request.getParameter("username");
		String credits=request.getParameter("creditvalue");
		AccountService accountService = new AccountService();
		
		
		if(credits != null && credits.startsWith("-"))
		{
			
			credits = credits.substring(1);
			long debits = new Long(credits);
			accountService.decreaseUserCredits(userName, debits, login);
			
			
		}
		else
		{
			if(credits.indexOf("+") > 0)
			{
				credits =credits.substring(1);
			}
			
			long crds = new Long(credits);
			
			accountService.increaseUserCredits(userName, crds, login);
		}
		
		
		return new ModelAndView("credits.jsp");
	}
}
