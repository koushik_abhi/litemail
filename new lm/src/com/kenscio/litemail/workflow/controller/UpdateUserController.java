package com.kenscio.litemail.workflow.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.service.AccountService;

public class UpdateUserController implements Controller{

	@Override
	@RequestMapping(value="/updateuser.html",method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		long accountId = (Long) session.getAttribute("accountId");
		String user=(String) session.getAttribute("login");
		
		
	    AccountService accservice = new AccountService();
        AccountEntity accountentity =  getUserFormDetails(request);
	    accservice.updateUser(accountentity/*,credits*/);
		
		return new ModelAndView("users.jsp");
	}
	
	private AccountEntity getUserFormDetails(HttpServletRequest request)
	{
		
		String userId=request.getParameter("userid");
		String userName=request.getParameter("name");
		String password=request.getParameter("password");
		String userType=request.getParameter("type");
		String role=request.getParameter("role");
		String emailId=request.getParameter("email");
		long mobileNumber=(Long.parseLong(request.getParameter("mobilenumber")));
		String phoneNumber=request.getParameter("phonenumber");
		String description=request.getParameter("description");
		String partnerName=request.getParameter("pname");
		String cpm=request.getParameter("costpermail");
		
		
		AccountEntity entity = new AccountEntity();
		entity.setLoginId(userId);
		entity.setUserName(userName);
		entity.setPassword(password);
		entity.setUserType(userType);
		entity.setUserRole(role);
		entity.setEmailId(emailId);
		entity.setMobileNumber(mobileNumber);
		entity.setPartnerName(partnerName);
		if( phoneNumber != "")
		{
			entity.setPhoneNumber(new Long(phoneNumber));
		}
		
		if(cpm != null && cpm != "")
		{
			entity.setCostPerMail(new Long(cpm));
		}
	
		return entity;
		
	}
}
