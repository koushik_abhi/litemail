package com.kenscio.litemail.workflow.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class UploadRecordController implements Controller {

	@Override
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession(false);
		if(session == null)
		{
		   request.setAttribute("errorMessage", "your session expired, please login again");
		   return new ModelAndView("loginscreen.jsp");
		}
		String listname=request.getParameter("listname");
		
		request.setAttribute("listname", listname);
		 
		return new ModelAndView("uploadlistimport.jsp?listname="+listname);
	}

}
