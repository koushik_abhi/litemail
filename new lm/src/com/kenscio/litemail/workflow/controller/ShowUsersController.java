package com.kenscio.litemail.workflow.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.service.AccountService;

public class ShowUsersController implements Controller{
	@RequestMapping(value = "/showusers.htm", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		AccountService imp = new AccountService();
		long accountId = (Long)session.getAttribute("accountId");
		AccountEntity ae = imp.getUser(accountId);
		if(ae.getUserRole() != null && (ae.getUserRole().equalsIgnoreCase("admin") || ae.getUserRole().equalsIgnoreCase("administrator") || ae.getUserRole().equalsIgnoreCase("superuser") || ae.getUserRole().equalsIgnoreCase("clientadmin")))
		{	
			return new ModelAndView("users.jsp");
		}
		else
		{
			request.setAttribute("errorMessage","only administrators can access this module");
			return new ModelAndView("dashboard.jsp");
		}
	}

}
