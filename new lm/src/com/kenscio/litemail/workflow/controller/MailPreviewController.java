package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.service.CampaignService;


public class MailPreviewController  implements Controller{

	@Override
	@RequestMapping(value="/mailpreview.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		String campaignName=request.getParameter("campaigname");
		long accountId = (Long) session.getAttribute("accountId");
		
		CampaignService impl=new CampaignService();
		String campaignMessage = impl.getCampaignMessage(accountId,campaignName);
		ObjectMapper mapper = new ObjectMapper();
		final DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		mapper.getDeserializationConfig().setDateFormat(df);
		String jsonString = "";
		if (campaignMessage == null) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("errorMessage", "Preview not available");
			jsonString = mapper.writeValueAsString(map);

		} else {

			Map<String, Object> finalMap = new HashMap<String, Object>();

			finalMap.put("result", campaignMessage);
			jsonString = mapper.writeValueAsString(finalMap);

			AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();

			MediaType jsonMimeType = MediaType.APPLICATION_JSON;

			if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

				try {

					stringHttpMessageConverter.write(jsonString, jsonMimeType,
							new ServletServerHttpResponse(response));

				} catch (IOException e) {
					e.printStackTrace();

				} catch (HttpMessageNotWritableException e1) {
					e1.printStackTrace();
				}

			}
		}

		return null;
	}
	
	 

}
