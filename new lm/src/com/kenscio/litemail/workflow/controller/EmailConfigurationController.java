package com.kenscio.litemail.workflow.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.domain.EmailObject;
import com.kenscio.litemail.workflow.service.AccountService;
import com.kenscio.litemail.workflow.service.EmailServiceImpl;

public class EmailConfigurationController implements Controller{
	
	
	@RequestMapping(value = "/emailconfig.htm", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		long accountId = (Long) session.getAttribute("accountId");
		String name=(String) session.getAttribute("name");
		EmailObject emailObject= getEmailEntity(request,name);
		AccountService servc = new AccountService();
		AccountEntity en = servc.getUser(accountId);
		EmailServiceImpl ser = new EmailServiceImpl();
		emailObject.setPartnerId(en.getPartnerName());
		emailObject.setUserid(en.getPartnerName());
		System.out.println("adding email configuration object to DB to"+ emailObject.getTo());
		ser.addEmailConfig(emailObject);
		return new ModelAndView("emailconfig.jsp");
	}
	private EmailObject	getEmailEntity(HttpServletRequest request,String createdBy)
		{
			String smtpHost=request.getParameter("smtphost");
			String smtpPort=request.getParameter("smtpport");
			String userName=request.getParameter("smtpuser");
			String password=request.getParameter("password");
			String messageBody=request.getParameter("body");
			String cc=request.getParameter("cc");
			String bcc=request.getParameter("bcc");
			String from=request.getParameter("from");
			String to=request.getParameter("to");
			String subject=request.getParameter("subject");
			String description=request.getParameter("description");
			EmailObject object=new EmailObject();
			object.setBcc(bcc);
			object.setHost(smtpHost);
			object.setPort(smtpPort);
			object.setBody(messageBody);
			object.setCc(cc);
			object.setSubject(subject);
			object.setFromAddress(from);
			object.setTo(to);
			object.setUsername(userName);
			object.setPassword(password);
			object.setCreatedBy(createdBy);
			object.setCreatedDate(new Date());
			object.setDescription(description);
			
			return object;
		}
}
