package com.kenscio.litemail.workflow.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.service.CampaignService;

public class DisplayCampaignReportController implements Controller {

	@Override
	@RequestMapping(value="/campaignreports.htm",method=RequestMethod.POST)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
        String workflow = request.getParameter("workflow");
		Long accountId = (Long) session.getAttribute("accountId");
		
	 	Campaign campaign = getFormFields(request,accountId);
		Long campaignID = 0l;
		String campaignName = campaign.getCampaignName();
		CampaignService service=new CampaignService();
		Campaign campaign1=service.getCampaignIdsWithName(accountId, campaignName);
		if(campaign1 != null )
		{
			campaignID=(Long) campaign1.getCampaignId();
		}
			
		 request.setAttribute("campaignname", campaignName);
		 //String campaignId=request.getParameter("campaignId");
		 request.setAttribute("campaignId", campaignID);
		 if(workflow != null && workflow.equalsIgnoreCase("tbl"))
		 {
			 return new ModelAndView("campaignreport_tbl.jsp");	 
		 }
		 else
		 {
			 return new ModelAndView("campaignreport.jsp");
		 }
	}
	private Campaign getFormFields(HttpServletRequest request, long accountId)
	{
		String campaignName=request.getParameter("campaignname");
		Campaign campaign = new Campaign();
		campaign.setCampaignName(campaignName);
		campaign.setAccountId(accountId);
		
		return campaign;
		
	}
}
