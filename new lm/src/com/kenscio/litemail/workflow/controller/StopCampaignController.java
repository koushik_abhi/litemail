package com.kenscio.litemail.workflow.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.service.CampaignService;

public class StopCampaignController implements Controller{
	@RequestMapping(value = "/stopcampaign.htm", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		Long accountId = (Long) session.getAttribute("accountId");
		String campaignName=request.getParameter("campaignName");
		if(validStatusForStop(accountId, campaignName))
		{
			stopCampaign(accountId, campaignName);
			request.setAttribute("errorMessage","campaign sucessfully stopped  " + campaignName);
			
		}else
		{
			request.setAttribute("errorMessage","can not stop the campaign for this state ");
			return new ModelAndView("campaignManagement.jsp");
		}
		
		return new ModelAndView("campaignManagement.jsp");
	}	
	
	private boolean validStatusForStop(Long accountId, String campaignName) {
		CampaignService service=new CampaignService();
		return service.validStatusForStop(accountId, campaignName);
		
	}

	private void stopCampaign(Long accountId,String campaignName)
	{
		CampaignService service=new CampaignService();
		service.stopCampaign(accountId,campaignName);
	}
}
