package com.kenscio.litemail.workflow.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileUploadException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.common.QuartzJobsServiceImpl;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.JobDetails;
import com.kenscio.litemail.workflow.domain.JobDetailsObject;
import com.kenscio.litemail.workflow.service.CampaignService;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;
import com.kenscio.litemail.workflow.service.MailServiceImpl;

public class StartCampaignControllerWF implements Controller{

	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		long accountId = (Long) session.getAttribute("accountId");
		String campaignName=request.getParameter("campaignName");
		String listName=request.getParameter("listName");
		String mailpermin = request.getParameter("mailstosend");
		Long mailsPerhr = 500000l;
		if(mailpermin != null && mailpermin.length() > 0)
		{
			mailsPerhr=Long.parseLong(mailpermin);
		}
		String workflow = request.getParameter("workflow");
		Long campaignID = 0l;
		CampaignService service=new CampaignService();
		Campaign campaign1=service.getCampaignIdsWithName(accountId, campaignName);
		if(campaign1 != null )
		{
			campaignID=(Long) campaign1.getCampaignId();
		}
		request.setAttribute("campaignname", campaignName);
		request.setAttribute("campaignName", campaignName);
		request.setAttribute("campaignId", campaignID);
		
		
		try
		{
			startCampaign(accountId, campaignName, campaignID, listName,mailsPerhr);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
			request.setAttribute("errorMessage", "unable to start campaign ");
			return new ModelAndView("sendoutdirect.jsp");
			
		}
		
		return new ModelAndView("campaignreport.jsp");
		
	}
		
	private void startCampaign(Long accountId,String campaignName,Long campaignID, String listName,Long mailsPerMin)
	{
		MailListServiceImpl impl=new MailListServiceImpl();
		Long listId=impl.getListIdOnName(accountId, listName);
		
		CampaignService service=new CampaignService();
		
		try {
			service.startCampaign(campaignID, mailsPerMin);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 
}
