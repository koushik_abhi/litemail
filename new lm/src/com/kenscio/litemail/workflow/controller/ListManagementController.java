package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kenscio.litemail.workflow.domain.ListEntity;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;
import com.kenscio.litemail.util.Page;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;

@Controller
public class ListManagementController {
	@RequestMapping(value = "/displylists.htm", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session = request.getSession(false);
		if (session == null) {
			request.setAttribute("errorMessage",
					"session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		long accountId = (Long) session.getAttribute("accountId");
		String userName = (String) session.getAttribute("login");
		String partnerName=(String) session.getAttribute("partnerName");
		
		Integer pageNumber = 1;
		String pagenum = request.getParameter("pagenumber");
		if (pagenum != null) {
			pageNumber = new Integer(pagenum);
		}
		int noOfRecordsperPage = 10;
		float totalRecords = 0;
		ObjectMapper mapper = new ObjectMapper();
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mapper.getDeserializationConfig().setDateFormat(df);
		String jsonString = "";
		Map<String, Object> finalMap = new HashMap<String, Object>();

		AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
		List<ListEntity> allLists = getAllListsInPagination(partnerName,
				userName, pageNumber, noOfRecordsperPage);
		checkListEntityRecords(allLists,accountId);
		totalRecords = getTotalPagesOfList(partnerName);
		
		double totalPages = totalRecords / noOfRecordsperPage;
		totalPages = Math.ceil(totalPages);
		double temp = noOfRecordsperPage*totalPages;
		if(totalRecords > temp)
			totalPages = totalPages+1;
		finalMap.put("totalpages", totalPages);
		finalMap.put("pagenumber", pageNumber);
		finalMap.put("result", allLists);

		if (allLists == null || allLists.size() == 0) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("errorMessage", "No list records found.");

		} else {
			jsonString = mapper.writeValueAsString(finalMap);
		}

		MediaType jsonMimeType = MediaType.APPLICATION_JSON;

		if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType,
						new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}

		}
		return null;

	}

	private List<ListEntity> getAllListsInPagination(String partnerName,
			String userName, int pagenumber, int noOfRecords) {

		MailListServiceImpl impl = new MailListServiceImpl();
		return impl.getAllListsInPagination(partnerName, userName, pagenumber,
				noOfRecords);
	}

	private void checkListEntityRecords(List<ListEntity> allLists, long accountId) {
		MailListServiceImpl impl = new MailListServiceImpl();
		for(ListEntity le : allLists)
		{
			le.setTotalRecords(impl.getListDatCount(accountId, le.getListName()));
		}
		
	}

	private int getTotalPagesOfList(String partnerName) {
		MailListServiceImpl impl = new MailListServiceImpl();
		return impl.getTotalPagesOfList(partnerName);
	}

	private List<ListEntity> getAllListsInPagination(long accountId,
			String userName, int pagenumber, int noOfRecords) {

		MailListServiceImpl impl = new MailListServiceImpl();
		return impl.getAllListsInPagination(accountId, userName, pagenumber,
				noOfRecords);
	}

	public List<ListEntity> getAllListItems(Page page) {
		List<ListEntity> msgs = new ArrayList<ListEntity>();
		List items = page.getPageItems();
		for (Object obj : items) {
			msgs.add((ListEntity) obj);
		}
		return msgs;

	}

}
