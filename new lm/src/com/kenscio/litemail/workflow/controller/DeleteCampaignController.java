package com.kenscio.litemail.workflow.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.service.CampaignService;

public class DeleteCampaignController implements Controller{

	@Override
	@RequestMapping(value="/deletecampaign.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		long accountId = (Long) session.getAttribute("accountId");
		String campaignName= request.getParameter("campaignName");
		CampaignService camp = new CampaignService();
		try
		{
			camp.deleteCampaign(accountId,campaignName );
			
		}catch(Exception e)
		{
			e.printStackTrace();
			request.setAttribute("errorMessage","unable to delete campaign "+ e.getMessage());
			return new ModelAndView("campaignManagement.jsp");
		}

		
		
		
		
		return null;
	}

}
