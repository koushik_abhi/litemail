package com.kenscio.litemail.workflow.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.kenscio.litemail.util.DataServiceUtilLiteMail;
import com.kenscio.litemail.workflow.domain.AttributeEntity;
import com.kenscio.litemail.workflow.domain.ListEntity;
import com.kenscio.litemail.workflow.service.AttributeService;
import com.kenscio.litemail.workflow.service.FTPFileManagementImpl;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;
import com.kenscio.litemail.workflow.service.SFTPFileManagementImpl;
import com.kenscio.litemail.workflow.service.UploadListServiceUtilAsync;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;



public class UploadlistFTPController implements Controller
{

	public static Logger log = Logger.getLogger(UploadlistFTPController.class.getName());
	
	@Override
	@RequestMapping(value="/uploadlistftp.htm",method=RequestMethod.POST)
	public ModelAndView handleRequest(HttpServletRequest request,
		HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
			String userName =(String)session.getAttribute("login");
			String listname=request.getParameter("listname");
			String description=request.getParameter("description");
			String uploadOpt=request.getParameter("datasource");
			String syncMode=request.getParameter("syncmode");
			String mode=request.getParameter("mode");
			String keyfilename = "";
			InputStream keyin = null;
			String work_flow = "";
			String ftpserver=request.getParameter("ftpserver");
			String ftplogin=request.getParameter("ftplogin");
			String ftppassword=request.getParameter("ftppassword");
			String ftppath=request.getParameter("ftppath");
			String sftp=request.getParameter("sftp");
			

			String ftpserver1=request.getParameter("ftpserver1");
			String ftplogin1=request.getParameter("ftplogin1");
			String ftppassword1=request.getParameter("ftppassword1");
			String ftppath1=request.getParameter("ftppath1");
			Long accountId =(Long)request.getSession(false).getAttribute("accountId");
			
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			String partnerName=(String) session.getAttribute("partnerName");
		    
			if(isMultipart)
			{
				
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List items = null;
				try {
					
					items = upload.parseRequest(request);
					
				} catch (FileUploadException e) 
				{
					request.setAttribute("errorMessage", "unable to upload sftp key file");
					return new ModelAndView("createlist_wf.jsp?listname="+listname);
				}
				Iterator itr = items.iterator();
				while (itr.hasNext()) 
				{
					FileItem item = (FileItem) itr.next();
					if (item.isFormField()) 
					{
						String name = item.getFieldName();
						String value = item.getString();
						
						if (name.equals("listname")) 
						{
							listname = value;	
							request.setAttribute("listname", listname);
							
						}else if(name.equals("description"))
						{
							description = value;
						}
						else if(name.equals("syncmode"))
						{
							syncMode = value;
						}
						else if(name.equals("mode"))
						{
							mode = value;
						}
						else if(name.equals("datasource"))
						{
							uploadOpt = value;
						}else if(name.equals("ftpserver"))
						{
							ftpserver = value;
						}
						else if(name.equals("ftplogin"))
						{
							ftplogin = value;
						}
						else if(name.equals("ftppassword"))
						{
							ftppassword = value;
						}
						else if(name.equals("ftppath"))
						{
							ftppath = value;
						}else if(name.equals("ftpserver1"))
						{
							ftpserver1 = value;
						}
						else if(name.equals("ftplogin1"))
						{
							ftplogin1 = value;
						}
						else if(name.equals("ftppassword1"))
						{
							ftppassword1 = value;
						}
						else if(name.equals("ftppath1"))
						{
							ftppath1 = value;
						}
						else if(name.equals("sftp"))
						{
							sftp = value;
						}else if(name.equals("work_flow"))
						{
							work_flow = value;
						}
						
					} else {
					
						if(keyfilename == null || keyfilename =="")
						{
							if(item.getName() != null && item.getName() != "" )
							{
								keyfilename = item.getName();
								keyin = item.getInputStream();
							}
						}
																						
					}
				}
			}
			
			String filename = "";
			InputStream in = null;
			File fi = null;
			MailListServiceImpl impl = new MailListServiceImpl();
		   	boolean listAlreadyExist = false;
			if((ftpserver != "" && ftplogin != "" && ftppassword != "" && ftppath != "" ) || (ftpserver1 != "" && ftplogin1 != "" && ftppassword1 != "" && ftppath1 != "" ))
			{
				Map<String, String> filenamepath  = null;
				FTPFileManagementImpl ftpimpl = new FTPFileManagementImpl();
				try{
					
					if(keyin != null)
					{
						if (keyfilename != "" && keyfilename.contains(".key")) 
						{
							
							SFTPFileManagementImpl simpl = new SFTPFileManagementImpl();
							filenamepath = simpl.downloadfile(ftpserver1, ftplogin1, ftppath1, ftppassword1, keyin);
						}else if(keyfilename != "" && !keyfilename.contains(".key"))
						{
							request.setAttribute("errorMessage", "please provide valid key store .key file : "+ keyfilename);
							if(work_flow.equals("true"))
							{
								
								return new ModelAndView("createlist_wf.jsp?listname="+listname);
							}
							else
							{
								
								return new ModelAndView("createlist.jsp?listname="+listname);
							}
							
						}
						
					}else if(sftp != null && sftp.equalsIgnoreCase("true"))
					{
						
						SFTPFileManagementImpl simpl = new SFTPFileManagementImpl();
						filenamepath = simpl.downloadfile(ftpserver1, ftplogin1, ftppath1, ftppassword1);
					}
					else
					{
						
						filenamepath = ftpimpl.downloadFile(ftpserver, ftplogin, ftppassword, ftppath);
						
					}
					if(filenamepath == null || filenamepath.size() == 0)
					{
						request.setAttribute("errorMessage", "please provide valid ftp details along with file location  : "+ ftppath);
						if(work_flow.equals("true"))
						{
							
							return new ModelAndView("createlist_wf.jsp?listname="+listname);
						}
						else
						{
							
							return new ModelAndView("createlist.jsp?listname="+listname);
						}
					
					}else
					{
						Set<String> keys = filenamepath.keySet();
						for(String k : keys)
						{
							filename = k;
							String filePath = filenamepath.get(filename);
							fi = new File(filePath);
							in = new FileInputStream(fi);
							break;
						}
						
					}
				}catch(Exception e)
				{
					e.printStackTrace();
					request.setAttribute("errorMessage", "please provide valid ftp details along with file location  : "+ e.getMessage());
					if(work_flow.equals("true"))
					{
						
						return new ModelAndView("createlist_wf.jsp?listname="+listname);
					}
					else
					{
						
						return new ModelAndView("createlist.jsp?listname="+listname);
					}
				}
				
			
			}else
			{
				request.setAttribute("errorMessage", "please provide valid FTP server details :" + ftpserver + "file path : "+ ftppath);
				if(work_flow.equals("true"))
				{
					
					return new ModelAndView("createlist_wf.jsp?listname="+listname);
				}
				else
				{
					
					return new ModelAndView("createlist.jsp?listname="+listname);
				}
			}
			
			if(listname != null && listname.length() > 0 && in != null)
		    {
				ListEntity listObj = impl.validateListName(listname, accountId);
				if(listObj != null)
			    {
					listAlreadyExist = true;
					listObj.setListDescription(description);
					listObj.setStatus("uploading");
					listObj.setListFileName(filename);
				    Calendar calendar11 = Calendar.getInstance();
					Date myDate11 = new Date(); // May be your date too.
					calendar11.setTime(myDate11);
					listObj.setUploadTime(calendar11.getTime());
					listObj.setUpdatedBy(userName);
					listObj.setUpdatedDate(calendar11.getTime());
					listObj.setPartnerName(partnerName);
			    	//request.setAttribute("errorMessage", "list name already exist, please create with different name "+ listname);
			    	//return new ModelAndView("uploadlist.jsp?listname="+listname);
			    	
			    }else
			    {
			    	listObj = new ListEntity();
				    listObj.setListName(listname);
				    listObj.setAccountId(accountId);
				    listObj.setListDescription(description);
				    listObj.setCreatedBy(userName);
				    listObj.setUpdatedBy(userName);
				    listObj.setStatus("uploading");
				    listObj.setListFileName(filename);
				    Calendar calendar11 = Calendar.getInstance();
					Date myDate11 = new Date(); // May be your date too.
					calendar11.setTime(myDate11);
				    listObj.setUploadTime(calendar11.getTime());
				    listObj.setCreatedDate(calendar11.getTime());	
				    listObj.setPartnerName(partnerName);
			    }
			   				
				request.setAttribute("listname", listname);
		    
		    	List<String> headers = new ArrayList<String>();
		    	// List<String> invheader = new ArrayList<String>();
		      //  List<List<String>> rows = new ArrayList<List<String>>();
		        List<String> dbattr  = null;
		        
		        if(uploadOpt != null && (uploadOpt.equals("ftpupload") || uploadOpt.equals("sftpupload")))
		        {
		        	if(syncMode == null || syncMode == ""  )
				    {
				    	syncMode = "new";
				    }		    
					if (filename != "" && (filename.contains(".csv") || (filename.contains(".txt")))) 
					{
						
						try{
							if(!listAlreadyExist)
					    	{
					    		impl.addMailingListOnly(listObj);
					    	}else
					    	{
					    		
					    		impl.addNewUpdateMailingList(listObj);
					    	}
					    	
					    }catch(Exception e)
					    {
					    	//e.printStackTrace();
					    	request.setAttribute("errorMessage", "exception occured in creating new list, please try again "+ listname);
					    	if(work_flow.equals("true"))
							{
								
								return new ModelAndView("createlist_wf.jsp?listname="+listname);
							}
							else
							{
								
								return new ModelAndView("createlist.jsp?listname="+listname);
							}
					    }
						List<ListEntity> liObj = impl.getListDetailsForUserAndlistname(accountId, listname);
						long listId = 0;
						if(liObj != null && liObj.size() > 0)
							listId = liObj.get(0).getListId();
						AttributeService attimpl = new AttributeService();
						dbattr = attimpl.getAllAttributesNamesOnly(accountId);
						
						List<String> invheader = new ArrayList<String>();
						
						if(!readDataAndPopulateHeadersRunAsync(request, listId, userName, headers, filename, in, fi, invheader, dbattr, syncMode,mode ))
			            {
			            	//revert changes as data is not uploaded 
							try{
			            		
			            			impl.delteMailingList(listId, accountId);
						     	
						    }catch(Exception e)
						    {
						    	e.printStackTrace();
						    	
						    	
						    }//request.setAttribute("errorMessage", " unable to upload complete file : "+filename + "invalid headers: " +invheader);
							request.setAttribute("errorMessage", "exception occured in creating new list, please try again "+ listname);
							if(work_flow.equals("true"))
							{
								
								return new ModelAndView("createlist_wf.jsp?listname="+listname);
							}
							else
							{
								
								return new ModelAndView("createlist.jsp?listname="+listname);
							}
			            }else
			            {
			            	request.setAttribute("listname", listname);
			            	if(work_flow.equals("true"))
							{
								
			            		return new ModelAndView("createCampaign_wf.jspp?listname="+listname);
							}
							else
							{
								
								return new ModelAndView("listManagement.jsp?listname="+listname);
							}
			    			
			            }
			           				   		    
			          
					}else 
					{
						
						request.setAttribute("errorMessage", "Please upload a valid file, it supports only csv file." +filename);
						if(work_flow.equals("true"))
						{
							
							return new ModelAndView("createlist_wf.jsp?listname="+listname);
						}
						else
						{
							
							return new ModelAndView("createlist.jsp?listname="+listname);
						}
					}
				    
		        }
		        	 
		    }else
		    {
		    	request.setAttribute("errorMessage", "not able to get file from ftp location" +ftppath);
		    	if(work_flow.equals("true"))
				{
					
					return new ModelAndView("createlist_wf.jsp?listname="+listname);
				}
				else
				{
					
					return new ModelAndView("createlist.jsp?listname="+listname);
				}
		    }
			request.setAttribute("listname", listname);
			
			if(work_flow.equals("true"))
			{
				
        		return new ModelAndView("createCampaign_wf.jspp?listname="+listname);
			}
			else
			{
				
				return new ModelAndView("listManagement.jsp?listname="+listname);
			}
	    
	}	
	
	private boolean readDataAndPopulateHeadersRunAsync(HttpServletRequest request, long listId, String username, List<String> headers,
			 String filename, InputStream in,  File fi, List<String> invheader, List<String> dbattr, String syncMode, String mode) {
		
		
		CSVReader reader =null;
		String  nextLine[]=new String[2000];
        try
        {
	         InputStreamReader fr =  null;
	         fr = new InputStreamReader(in);
	         reader = new CSVReader(fr);
	         int firstRow = 0;
	        
	         if ((nextLine = reader.readNext()) != null)
	         {
	          
	           
	           if(firstRow ==0)
	           {
	        	  int c = 0;
	         	  for(int nIndex = 0; nIndex <nextLine.length;nIndex++)
	              {
	                 
	                  if(nextLine[nIndex].length() > 0)
	                  {
	                	  headers.add(nextLine[nIndex].trim());
	                  }
	                  c++;
	                  if(c >60)
	                	  break;
	         		  
	              }
	         	  		         	  
	         	 /// createAttributesOnHeaders(listId,username, headers, invheader , dbattr);
	         	//  AttributeService attimpl = new AttributeService();
	         	//  dbattr = attimpl.getAllUserAttributes(accountId); 
	         	  if(!DataServiceUtilLiteMail.validateMailListHeaders(headers, dbattr))
	         	  {
	         		 request.setAttribute("errorMessage", "invalid headers in the file  "+filename + "headers list :"+ headers);
	         		 return false;
	         	  }
	         	  	         	  
				   	         	  
	           }
	          
	           firstRow++;
	          	           
	        }
	        String filePath = writeFileIntoTemp(request,listId, filename, in, reader,nextLine );
	        
	        if(filePath != null)
	        {
	        	populateDataInAsync(listId,username,headers,filename,dbattr,syncMode,mode, filePath);
	        	return true;
	        }
	         
        }catch(Exception e)
        {
	       	e.printStackTrace();
	       	log.log(Level.SEVERE, "error during upload file "+e.getMessage());
       	
        }finally
        {
        	if(fi != null)
        	{
        		fi.delete();
        	}
        }
        
		return false;
	}
	
	private String writeFileIntoTemp(HttpServletRequest request, long listId, String filename, InputStream in,
			CSVReader reader, String[] headers) {
		File file = null;
		CSVWriter writer =null;
		try{
						 
			 long time  = System.currentTimeMillis();
			 String fileName = listId+"_"+time+".csv";
			 file = File.createTempFile(fileName,"");
			 FileOutputStream out = new FileOutputStream(file);
			 writer = new CSVWriter(new OutputStreamWriter(out));
			// writer.writeNext(headers);
			 String  nextLine[]=new String[2000];
			 while ((nextLine = reader.readNext()) != null)
	         {
				 writer.writeNext(nextLine);
	         }
			
			
		}catch(Exception e)
		{
			log.log(Level.SEVERE, "error in uploading file "+e.getMessage());
		}
		finally
		{
			
			try {
				if(writer != null)
				writer.close();
				if(reader != null)
					reader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		return file.getAbsolutePath();
		
	}

	private void populateDataInAsync(long listId, String username, List<String> headers,
			 String filename, List<String> dbattr, String syncMode, String mode, String filePath) {
		
	   UploadListServiceUtilAsync upload = new UploadListServiceUtilAsync(listId,username,headers,filename,dbattr,syncMode, mode, filePath);
	   upload.uploadDataAsync();
	 
	}
			
				
	
}