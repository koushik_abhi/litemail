package com.kenscio.litemail.workflow.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class ValidateAdminController implements Controller{
	
	
	@Override
	//@RequestMapping(value="/attrmapgrid.htm",method=RequestMethod.POST)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		   
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
	    String userName = (String) session.getAttribute("login");
	    Object str = session.getAttribute( "adminaccess" );
	    if(str != null)
	    {
		   if(str.equals("admin") || str.equals("superuser") || str.equals("administrator"))
		   {
			   return new ModelAndView("users.jsp");
		   }
		   else
		   {
			   request.setAttribute("errorMessage", "you don't have administration access");
			   return new ModelAndView("dashboard.jsp");
		   }
	    }
	    else
	    {
		    request.setAttribute("errorMessage", "you don't have administration access");
		    return new ModelAndView("dashboard.jsp");
	    }
		   
		  		   
	}

}
