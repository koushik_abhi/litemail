package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.util.CreditsCaluclator;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.domain.AccountCredit;
import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.service.AccountService;
import com.kenscio.litemail.workflow.service.CampaignService;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;

public class GetSendoutDataController implements Controller{
	@Override
	@RequestMapping(value="/getsendoutdata.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		Long accountId = (Long)session.getAttribute("accountId");
		String campaignName=request.getParameter("campaignname");
		
		
		Campaign campaign=getCampaign(accountId, campaignName);
		Long listId=campaign.getListId();
		Long listRecords=getListRecord(accountId, listId);
		Map<String, Long> domainCount=getDomainCount(listId);
		Map<String, Object> finalMap = new HashMap<String, Object>();
		String system=ApplicationProperty.getProperty("credit.system");
		//System.out.println(system);
		AccountService accservice = new AccountService();
		AccountEntity entity = null;
		
		//if(loginId != null && loginId != "")
		//{
		//	entity = accservice.getUserDetails(loginId);
	//	}
	//	else
	//	{
		entity = accservice.getUser(accountId);
		if (entity.getUserType() != null && entity.getUserType().equals("credits")) 
		{
			Long creditsAvl = getAvlCredits(accountId);
			//Long credtsAvl=100l;
			Long creditsReq = getReqCredits(accountId, campaign.getListName());
			finalMap.put("campdata", campaign);
			finalMap.put("listrecords", listRecords);
			finalMap.put("domaincount", domainCount);
			finalMap.put("creditsavl", creditsAvl);
			finalMap.put("creditsreq", creditsReq);

		}
		else{
			finalMap.put("campdata", campaign);
			finalMap.put("listrecords", listRecords);
			finalMap.put("domaincount", domainCount);
			finalMap.put("creditsavl", "");
			finalMap.put("creditsreq", "");
		}
		ObjectMapper mapper = new ObjectMapper();
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		mapper.getDeserializationConfig().setDateFormat(df);
		String jsonString = "";
		
			
		AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();

		
		ArrayList<Campaign> campaigns=new ArrayList<Campaign>();
		campaigns.add(campaign);
		if (campaigns == null || campaigns.size() <= 0) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("errorMessage", "No campaign details found.");

		} else {
			jsonString = mapper.writeValueAsString(finalMap);
			System.out.println(jsonString);
		}
		

		MediaType jsonMimeType = MediaType.APPLICATION_JSON;

		if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}
		}
			return null;

}
	private Campaign getCampaign(long accountId, String campaignName)
	{
		Long campaignId=0l;
		CampaignService service=new CampaignService();
		Campaign campaign1 =service.getCampaign(accountId, campaignName);
		
		return campaign1;
	}

	private Long getListRecord(Long accountId, Long listId) {
		MailListServiceImpl impl = new MailListServiceImpl();
		return impl.getAllRecordCountForList(accountId, listId);
	}

	private Map<String, Long> getDomainCount(Long listId) {
		MailListServiceImpl impl = new MailListServiceImpl();
		return impl.getDomainCountMapForGraph(listId);
	}
	
	private Long getAvlCredits(Long accountId)
	{
		AccountService service=new AccountService();
		AccountCredit credit=service.getUserCredit(accountId);
		if(credit != null)
			return credit.getBalance();
		else
			return 0l;
	}
	
	private Long getReqCredits(Long accountId,String listName)
	{
		CreditsCaluclator caluclator=new CreditsCaluclator();
		return caluclator.getReqCredits(accountId,listName);
		
	}
}
