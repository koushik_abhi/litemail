package com.kenscio.litemail.workflow.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.domain.PartnerEntity;
import com.kenscio.litemail.workflow.service.AccountService;

public class UpdatePartnerController implements Controller{
	@Override
	@RequestMapping(value="/updateuser.html",method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		long accountId = (Long) session.getAttribute("accountId");
		String user=(String) session.getAttribute("login");
	    AccountService accservice = new AccountService();
	    PartnerEntity entity =  getPartnerEntity(request, user);
	    accservice.updatePartner(entity);
		
		return new ModelAndView("partners.jsp");
	}
	
	private PartnerEntity getPartnerEntity(HttpServletRequest request,String userName)
	{
		String name= request.getParameter("name");
		String type=request.getParameter("type");
		String status=request.getParameter("status");
		String description=request.getParameter("description");
		String comment=request.getParameter("comment");
		String emailId=request.getParameter("emailid");
		String address=request.getParameter("address");
		String url=request.getParameter("url");
		String businessNature=request.getParameter("bnature");
		Long mobileNo=Long.parseLong(request.getParameter("mobilenumber"));
		
		
		PartnerEntity entity=new PartnerEntity();
		entity.setName(name);
		entity.setType(type);
		entity.setStatus(status);
		entity.setDescription(description);
		entity.setComment(comment);
		entity.setEmailId(emailId);
		entity.setAddress(address);
		entity.setBusinessUrl(url);
		entity.setBusinessNature(businessNature);
		entity.setMobileNumber(mobileNo);
		entity.setCreatedBy(userName);
		entity.setCreatedDate(new Date());
		entity.setUpdatedDate(new Date());
		entity.setUpdatedBy(userName);
		
		return entity;
	}
}
