package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.service.MailListServiceImpl;

public class GetListRecordsController implements Controller{

	@Override
	@RequestMapping(value="/deleteattribute.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		long accountId=(Long) session.getAttribute("accountId");
		String listName=request.getParameter("listname");
		
		
		MailListServiceImpl impl=new MailListServiceImpl();
		long records=impl.getListRecordCount(accountId, listName);
		String jsonString = "";
		ObjectMapper mapper = new ObjectMapper();
		AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
		Map<String, Object> finalMap = new HashMap<String, Object>();
		finalMap.put("records", records);
				
		jsonString = mapper.writeValueAsString(finalMap);
		MediaType jsonMimeType = MediaType.APPLICATION_JSON;

		if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

			try {

				stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

			} catch (IOException e) {
				e.printStackTrace();

			} catch (HttpMessageNotWritableException e1) {
				e1.printStackTrace();
			}

		}

		
		
		return null;
	}

}
