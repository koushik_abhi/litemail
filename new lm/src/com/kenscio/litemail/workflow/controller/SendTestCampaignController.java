package com.kenscio.litemail.workflow.controller;

import org.apache.commons.mail.EmailException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kenscio.litemail.exception.NoValidMailingListException;
import com.kenscio.litemail.exception.SMTPAuthenticationException;

import com.kenscio.litemail.workflow.common.BaseController;
import com.kenscio.litemail.workflow.service.CampaignService;

import java.util.Arrays;
import java.util.List;


public class SendTestCampaignController extends BaseController implements Controller {
	@RequestMapping(value="/sendtestcampaign.htm",method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		Long accountId=(Long) session.getAttribute("accountId");
    	String campaignName = request.getParameter("campaignName");
    	String listName = request.getParameter("listName");
      	String emailsids = request.getParameter("emailsTest");
    	request.setAttribute("campaignName", campaignName);
		request.setAttribute("listName", listName);
		
	
    	List<String> emailIds = Arrays.asList(emailsids.split("\\s*,\\s*"));
    	
    	
			if(!validateEmail(emailIds))
			{ 
				request.setAttribute("errorMessage", "Invalid emailId  please provide valid emailId "+ emailsids);
				return new ModelAndView("sendoutCampaign_tbl.jsp");
			}
			else
			{
				try
				{
				
					if(sendTestCampaign(accountId,campaignName,emailIds))
					{
						request.setAttribute("errorMessage", "test campaign is successfully sent to "+ emailsids);
						return new ModelAndView("sendoutCampaign_tbl.jsp");
					}
					else{
						request.setAttribute("errorMessage", "unable to send test campaign to "+ emailsids);
						return new ModelAndView("sendoutCampaign_tbl.jsp");
					}
				}catch(Exception e)
				{
					request.setAttribute("errorMessage", "unable to send test campaign smtp details not valid ");
					return new ModelAndView("sendoutCampaign_tbl.jsp");
				}
				
				
			}
			
	}

	private boolean validateEmail(List<String> emailIds)
	{
		String emailId="";
		for(int i=0;i<emailIds.size();i++){
			emailId=emailIds.get(i);
		if(emailId != "" && emailId.indexOf("@") > 0)
		{
			return true;
		}
		}
		return false;
		
	}
	
	private boolean sendTestCampaign(Long accountId,String campaignName,List<String> emailIds) throws NoValidMailingListException, SMTPAuthenticationException, EmailException
	{
		CampaignService service=new CampaignService();
		return	service.previewTestCampaign(accountId,campaignName,emailIds);
	}
	
    }

