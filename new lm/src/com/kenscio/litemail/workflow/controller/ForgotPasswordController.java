package com.kenscio.litemail.workflow.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.domain.EmailObject;
import com.kenscio.litemail.workflow.service.AccountService;
import com.kenscio.litemail.workflow.service.EmailServiceUtil;


public class ForgotPasswordController implements Controller{

	@RequestMapping(value="/login.htm",method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response)  {
		
			String username =request.getParameter("username");
			String email =request.getParameter("email");
			
			//System.out.println("login user" + userName );
			if(username == null || username =="" ||email == null || email =="")
			{
				request.setAttribute("errorMessage", "Please provide registered userId, e-mail Id for generating new password");
				return new ModelAndView("loginscreen.jsp");
			}
			AccountService loginService = new AccountService();
			AccountEntity userObj = loginService.getUser(username);
			if(userObj == null )
			{
				request.setAttribute("errorMessage", "user is not registered with the given userId");
				return new ModelAndView("loginscreen.jsp");
			}
			if(userObj.getUserName().equals(username) && userObj.getEmailId().equals(email))
			{
				String newpassword = getAlphaNumeric(6);
				loginService.updateUserPassword(newpassword, username);
				sendEmailNotifcation(newpassword, email, username);
				request.setAttribute("errorMessage", "password sent to the registered e-mail id, please login application with the new password and reset password after login");
				return new ModelAndView("loginscreen.jsp");
				
			}else
			{
				request.setAttribute("errorMessage", "user id, and email id is not matching with the records");
				return new ModelAndView("loginscreen.jsp");
			}
	}
	
	private void sendEmailNotifcation(String newpassword, String forgotemail,
			String username) {
		
		EmailServiceUtil emailSer = new EmailServiceUtil();
		EmailObject em = new EmailObject();
		em.setFromAddress("litemailadmin@kenscio.com");
		em.setTo(forgotemail);
		em.setHost("smtp.gmail.com");
		em.setPort("587");
		em.setUsername("litemailadmin@kenscio.com");
		em.setPassword("KenSxcI098");
		emailSer.sendEmailNotification(em, forgotemail, newpassword, username);
		
	}

		
	private static final String ALPHA_NUM = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	public String getAlphaNumeric(int len) {
		StringBuffer sb = new StringBuffer(len);
		for (int i = 0; i < len; i++) 
		{
			int ndx = (int) (Math.random() * ALPHA_NUM.length());
			sb.append(ALPHA_NUM.charAt(ndx));
		}
		return sb.toString();
	}
				
}
	
