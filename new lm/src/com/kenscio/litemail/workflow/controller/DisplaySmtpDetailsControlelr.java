package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.SmtpEntity;
import com.kenscio.litemail.workflow.service.SmtpService;

public class DisplaySmtpDetailsControlelr implements Controller{

	@Override
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		  String smtpId = request.getParameter("smtpId");
		  Long smtpID = new Long(smtpId);
		  SmtpService serv = new SmtpService();
		  SmtpEntity smtps =(SmtpEntity) serv.getSmtpDetailsById(smtpID);
		  ArrayList<SmtpEntity> list = new ArrayList<SmtpEntity>();
		  list.add(smtps);
		  	  
		  
			ObjectMapper mapper = new ObjectMapper();
		       final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				mapper.getDeserializationConfig().setDateFormat(df);
		       String jsonString = "";
				Map<String, Object> finalMap = new HashMap<String, Object>();
				
				AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
				finalMap.put("result", list);
			
				
				if(list.size() <=0)
				{
					Map<String, String> map = new HashMap<String, String>();
					map.put("errorMessage", "No  records found.");
					
				}else
				{
					 jsonString = mapper.writeValueAsString(finalMap);
					 System.out.println("**smtp"+jsonString);
				}
				
				MediaType jsonMimeType = MediaType.APPLICATION_JSON;

				if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

					try {

						stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

					} catch (IOException e) {
						e.printStackTrace();

					} catch (HttpMessageNotWritableException e1) {
						e1.printStackTrace();
					}
				}
				return null;
	
	}

	

}
