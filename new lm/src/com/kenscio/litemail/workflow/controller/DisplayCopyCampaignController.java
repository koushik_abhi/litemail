package com.kenscio.litemail.workflow.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.Campaign;

public class DisplayCopyCampaignController implements Controller{

	@Override
	@RequestMapping(value="/displaycopycampaign.htm",method=RequestMethod.POST)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		long accountId=(Long) session.getAttribute("accountId");
		String campaignName=request.getParameter("campaignname");
		request.setAttribute("campaignname", campaignName);
		
		return new ModelAndView("copycampaign.jsp");
	}
	 
	
}
