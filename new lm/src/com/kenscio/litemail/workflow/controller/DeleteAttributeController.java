package com.kenscio.litemail.workflow.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.service.AttributeService;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.BaseController;

public class DeleteAttributeController extends BaseController implements Controller {
	@RequestMapping(value = "/deleteattribute.htm", method = RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		String user=(String) session.getAttribute("login");
		long accountId = (Long) session.getAttribute("accountId");
		String attrId = request.getParameter("attributeid");
		
		if(attrId != null )
		{
			Long att = new Long(attrId);
			AttributeService service=new AttributeService();
			service.deleteAttribute(att);
		}
		else
		{
			request.setAttribute("errorMessage","attribute not found ");
			
		}
		
		return new ModelAndView("attributeManagement.jsp");
}
}
