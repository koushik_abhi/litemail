package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.domain.AccountCredit;
import com.kenscio.litemail.workflow.service.AccountService;


public class CreditManagementController implements Controller{

	@Override
	@RequestMapping(value="/creditmanagement.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		long accountId=(Long) session.getAttribute("accountId");
		String pagenum =request.getParameter("pagenumber");
		int pagenumber=1;
	
		if(pagenum !=null)
		{
			pagenumber=Integer.parseInt(pagenum);
		}
		double totalPages = 1;
		int recordsPerPage=10;
		AccountService ser = new AccountService();
		float totalRecords=ser.getTotalPagesOfCredits(accountId);
		totalPages=Math.ceil(totalRecords/recordsPerPage);
		double temp = recordsPerPage*totalPages;
		if(totalRecords > temp)
			totalPages = totalPages+1;
		List<AccountCredit> userCredits = ser.getAllUserCreditsInPagination(pagenumber, recordsPerPage);
		System.out.println("userCredits "+userCredits);
		ObjectMapper mapper = new ObjectMapper();
	       final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			mapper.getDeserializationConfig().setDateFormat(df);
	       String jsonString = "";
			Map<String, Object> finalMap = new HashMap<String, Object>();
			
			AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
			
		  	
		   	finalMap.put("totalpages", totalPages);
			finalMap.put("pagenumber", pagenumber);
			finalMap.put("result", userCredits);
			
			
			if(userCredits == null || userCredits.size() == 0)
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put("errorMessage", "No partner records found.");
				
			}else
			{
				 jsonString = mapper.writeValueAsString(finalMap);
			}
			
			MediaType jsonMimeType = MediaType.APPLICATION_JSON;

			if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

				try {

					stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

				} catch (IOException e) {
					e.printStackTrace();

				} catch (HttpMessageNotWritableException e1) {
					e1.printStackTrace();
				}

			}
	       
	       
	       return null;
	    }

}

