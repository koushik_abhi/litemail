package com.kenscio.litemail.workflow.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.service.CampaignService;

public class ResumeCampaignController implements Controller{
	
	
	@Override
	@RequestMapping(value="/resumecampaign.htm", method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		Long accountId = (Long) session.getAttribute("accountId");
		String campaignName=request.getParameter("campaignName");
		try{
			resumeCampaign(accountId, campaignName);
		}catch(Exception e)
		{
			request.setAttribute("errorMessage","exception in resume campaign  " + campaignName +  " message "+ e.getMessage());
		}
		request.setAttribute("errorMessage","campaign sucessfully resumed  " + campaignName);
	
		return new ModelAndView("campaignManagement.jsp");
		
		
	}
	
	private void resumeCampaign(Long accountId,String campaignName) throws Exception
	{
		CampaignService service=new CampaignService();
		service.resumeCampaign(accountId,campaignName);
	}

}



