package com.kenscio.litemail.workflow.controller;

import com.kenscio.litemail.workflow.service.MailListServiceImpl;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by IntelliJ IDEA.
 * User: Comp
 * Date: Aug 24, 2006
 * Time: 6:19:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class DeleteListController implements Controller{
	
	@RequestMapping(value="/deletelist.htm",method=RequestMethod.GET)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}

		long listId = Long.parseLong(request.getParameter("listrecs"));
		try{
		long accountId=(Long) session.getAttribute("accountId");
		MailListServiceImpl impl=new MailListServiceImpl();
		boolean listInUse=impl.isListExistInCampaignList(listId);
		System.out.println("listInUse "+listInUse);
		if(listInUse)
		{
			request.setAttribute("errorMessage", "List is Associated With Campaign.");
			return new ModelAndView("listManagement.jsp");
		}
		else
		{
			
			impl.delteMailingList(listId, accountId);
		}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;

}
}