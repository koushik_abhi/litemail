package com.kenscio.litemail.workflow.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.service.AttributeService;
import com.kenscio.litemail.workflow.service.LoginServiceImpl;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;


public class ChartForListAttributeController implements Controller{

	@Override
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
	
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		String listname = request.getParameter("listName");
		long listId1=Long.parseLong(request.getParameter("listId"));
		System.out.println("dsf lsit id"+listId1);
		request.setAttribute("listname", listname);
		long accountId=(Long) session.getAttribute("accountId");
		MailListServiceImpl conf = new MailListServiceImpl();
		long listId = conf.getListIdOnName(accountId,listname);
		AttributeService att = new AttributeService();
		List<String> userattrObj = getAlAttributes(accountId,listId1);
		if(userattrObj !=null)
			userattrObj.add(0, "domain");
	    ObjectMapper mapper = new ObjectMapper();
	       final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			mapper.getDeserializationConfig().setDateFormat(df);
	       String jsonString = "";
			Map<String, Object> finalMap = new HashMap<String, Object>();
			
			AbstractHttpMessageConverter<String> stringHttpMessageConverter = new StringHttpMessageConverter();
			finalMap.put("result", userattrObj);
		
			
			if(userattrObj == null)
			{
				Map<String, String> map = new HashMap<String, String>();
				map.put("errorMessage", "No list records found.");
				
			}else
			{
				 jsonString = mapper.writeValueAsString(finalMap);
				System.out.println("attribute for list"+jsonString);
			}
			
			MediaType jsonMimeType = MediaType.APPLICATION_JSON;

			if (stringHttpMessageConverter.canWrite(String.class, jsonMimeType)) {

				try {

					stringHttpMessageConverter.write(jsonString, jsonMimeType,new ServletServerHttpResponse(response));

				} catch (IOException e) {
					e.printStackTrace();

				} catch (HttpMessageNotWritableException e1) {
					e1.printStackTrace();
				}
			}
			return null;

	}
	private List<String> getAlAttributes(Long accountId,Long listId1)
	{
		AttributeService att = new AttributeService();
		List<String> stdattrrObj=new ArrayList<String>();
		
		List<String> stdatt=att.getAllStdAttributes();
		for (int i=0;i<stdatt.size();i++) {
		 
				stdatt.remove("email");
				stdatt.remove("emailId");
			
		}
		List<String> custtList=att.getAllCustAttributes(accountId,listId1);
		stdattrrObj.addAll(stdatt);
		stdattrrObj.addAll(custtList);
		//List<String> userattriObj=att.getAllUserAttributes(accountId);
		/*for (int i=0;i<stdattrrObj.size();i++) {
			String attrib=stdattrrObj.get(i);
			if((attrib.equalsIgnoreCase("city")))
			{
				userattriObj.add(attrib);
			}
			else if((attrib.equalsIgnoreCase("age")))
			{
				userattriObj.add(attrib);
			}
			else if((attrib.equalsIgnoreCase("gender")))
			{
				userattriObj.add(attrib);
			}
			else if((attrib.equalsIgnoreCase("salary")))
			{
				userattriObj.add(attrib);
			}
		}*/
		
		return stdattrrObj;
	}

}
