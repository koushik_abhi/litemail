package com.kenscio.litemail.workflow.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.service.SmtpService;

public class DeleteSmtpController implements Controller {

	@Override
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		long accountId=(Long) session.getAttribute("accountId");
		String servername = request.getParameter("servername");
		SmtpService ser = new SmtpService();
		boolean smtpInUse = ser.isSmtpInUse(accountId,servername);
		if(!smtpInUse)
		{
			ser.deleteSmtpServer(servername);
		}
		else
		{
			request.setAttribute("errorMessage", "smtp is in use cannot be delete");
			return new ModelAndView("smtpManagement.jsp");
		}
		
		return null;
	}
	
	

}
