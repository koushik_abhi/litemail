package com.kenscio.litemail.workflow.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.service.AccountService;

public class AddUserController implements Controller{

	@Override
	@RequestMapping(value="/adduser.html",method=RequestMethod.POST)
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HttpSession session=request.getSession(false);
		if(session == null)
		{
			request.setAttribute("errorMessage","session expired, please login again ");
			return new ModelAndView("loginscreen.jsp");
		}
		
		String user=(String) session.getAttribute("login");
		String cred=request.getParameter("credits");
		Long credits=0l;
		
		if(cred != "")
		{
			credits=new Long(cred);
		}
		
		AccountEntity entity=getAccountEntity(request, user);
		if (!validate(entity)) {
			request.setAttribute("errorMessage","userId,password,role,email and partner name is mandatory");
			return new ModelAndView("adduser.jsp");
		}
		AccountService service = new AccountService();
		if(service.isUserExist(entity.getLoginId()))
		{
			request.setAttribute("errorMessage","user id already exist");
			return new ModelAndView("adduser.jsp");
		}
		service.addUser(entity,credits);
		
		return new ModelAndView("users.jsp");
	}
	
	private AccountEntity getAccountEntity(HttpServletRequest request,String createdBy)
	{
		String loginId=request.getParameter("userid");
		String userName=request.getParameter("name");
		String password=request.getParameter("password");
		String userType=request.getParameter("type");
		String role=request.getParameter("role");
		String emailId=request.getParameter("email");
		Long mobileNumber=Long.parseLong(request.getParameter("mobilenumber"));
		String phoneNumber=request.getParameter("phonenumber");
		String description=request.getParameter("description");
		String partnerName=request.getParameter("pname");
		String cpm=request.getParameter("costpermail");
		
		AccountEntity entity=new AccountEntity();
		entity.setLoginId(loginId);
		entity.setUserName(userName);
		entity.setPassword(password);
		entity.setUserType(userType);
		entity.setUserRole(role);
		entity.setEmailId(emailId);
		entity.setMobileNumber(mobileNumber);
		
		if( phoneNumber != "")
		{
			entity.setPhoneNumber(new Long(phoneNumber));
		}
		
		entity.setPartnerName(partnerName);
		if(cpm !=""){
			entity.setCostPerMail(new Long(cpm));	
		}
		
		entity.setCreatedBy(createdBy);
		entity.setUpdatedBy(createdBy);
	
		entity.setCreatedDate(new Date());
		entity.setUpdatedDate(new Date());
		
		return entity;
		
	}
	
	boolean validate(AccountEntity entity)
	{
		String userId=entity.getLoginId();
		String pwd=entity.getPassword();
		String role=entity.getUserRole();
		String email=entity.getEmailId();
		String pName=entity.getPartnerName();
		if(  userId == null || userId == "" || pwd == null || pwd == "" || role == null || role == "" || email== null || email == "" || pName== null || pName == "")
		{
			return false;
		}
		return true;
	}

}
