package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

import java.io.File;
import java.util.List;



/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Sep 19, 2007
 * Time: 5:38:06 PM
 * To change this template use File | Settings | File Templates.
 */
public class CopyCampaignForm extends BaseData {
    private String copyCampaignId;
    private String campaignName;
    private String description;
    private Long listId=0l;
    //private String mailingListName;   //used at campaigndetails.jsp
    private String fromName;
    private String fromAddress;
    private String replyName;
    private String enableLinkTracking;
    private String enableOpenTracking;
    private String replyTo;
    private String postalAddress;
    private String subject;
    private String stampValue;
    private String smtpServer;
    private String smtpUserName;
    private String smtpPassword;
    private String campaignMessage;
    private List stampitListId;
    private List stampitListName;
    private File myFile;
    private String ackFlag;
    private String chatRequired;
    private File attachedFile;
    private String attachmentFileName;
    private String mailContentType;
    //private String timeLeft;
    private String stampImage;
    private boolean importFlag;
    private String authenticateSMTPFlag;
     private File zipMailBody;
    private String htmlFileName;

    /*private String actualFromAddress; //to check whether from-email id changed
  private String currentBalance;
  private String campaignAmount;
  private String requiredAmount;
  private String walletId;
  private String stampitAccountId;*/

    public String getCopyCampaignId() {
        return copyCampaignId;
    }

    public void setCopyCampaignId(String copyCampaignId) {
        this.copyCampaignId = copyCampaignId;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(String Long) {
        this.listId = listId;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStampValue() {
        return stampValue;
    }

    public void setStampValue(String stampValue) {
        this.stampValue = stampValue;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getSmtpUserName() {
        return smtpUserName;
    }

    public void setSmtpUserName(String smtpUserName) {
        this.smtpUserName = smtpUserName;
    }

    public String getSmtpPassword() {
        return smtpPassword;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    public String getCampaignMessage() {
        return campaignMessage;
    }

    public void setCampaignMessage(String campaignMessage) {
        this.campaignMessage = campaignMessage;
    }

    public List getStampitListId() {
        return stampitListId;
    }

    public void setStampitListId(List stampitListId) {
        this.stampitListId = stampitListId;
    }

    public List getStampitListName() {
        return stampitListName;
    }

    public void setStampitListName(List stampitListName) {
        this.stampitListName = stampitListName;
    }

    public File getMyFile() {
        return myFile;
    }

    public void setMyFile(File myFile) {
        this.myFile = myFile;
    }

    public String getAckFlag() {
        return ackFlag;
    }

    public void setAckFlag(String ackFlag) {
        this.ackFlag = ackFlag;
    }

    public File getAttachedFile() {
        return attachedFile;
    }

    public void setAttachedFile(File attachedFile) {
        this.attachedFile = attachedFile;
    }

    public String getAttachmentFileName() {
        return attachmentFileName;
    }

    public void setAttachmentFileName(String attachmentFileName) {
        this.attachmentFileName = attachmentFileName;
    }

    public String getMailContentType() {
        return mailContentType;
    }

    public void setMailContentType(String mailContentType) {
        this.mailContentType = mailContentType;
    }

    public String getStampImage() {
        return stampImage;
    }

    public void setStampImage(String stampImage) {
        this.stampImage = stampImage;
    }

    public boolean getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(boolean importFlag) {
        this.importFlag = importFlag;
    }

    public String getAuthenticateSMTPFlag() {
        return authenticateSMTPFlag;
    }

    public void setAuthenticateSMTPFlag(String authenticateSMTPFlag) {
        this.authenticateSMTPFlag = authenticateSMTPFlag;
    }

    public File getZipMailBody() {
        return zipMailBody;
    }

    public void setZipMailBody(File zipMailBody) {
        this.zipMailBody = zipMailBody;
    }

    public String getHtmlFileName() {
        return htmlFileName;
    }

    public void setHtmlFileName(String htmlFileName) {
        this.htmlFileName = htmlFileName;
    }

    public void reset()
    {
        campaignName="";
        description="";
        listId=0l;
        fromName="";
        fromAddress="";
        postalAddress="";
        replyTo="";
        subject="";
        stampValue="";
        smtpServer="";
        smtpUserName="";
        smtpPassword="";
        campaignMessage="";
        myFile=null;
        authenticateSMTPFlag="";
        zipMailBody=null;
        htmlFileName="";
    }

    public String getChatRequired(){
        return chatRequired;
    }

    public void setChatRequired(String chatRequired){
        this.chatRequired = chatRequired;
    }

	public String getReplyName() {
		return replyName;
	}

	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}

	public String getEnableLinkTracking() {
		return enableLinkTracking;
	}

	public void setEnableLinkTracking(String enableLinkTracking) {
		this.enableLinkTracking = enableLinkTracking;
	}

	public String getEnableOpenTracking() {
		return enableOpenTracking;
	}

	public void setEnableOpenTracking(String enableOpenTracking) {
		this.enableOpenTracking = enableOpenTracking;
	}


}
