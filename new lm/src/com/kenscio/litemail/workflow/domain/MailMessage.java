package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.ApplicationProperty;


import java.io.*;
import java.math.BigDecimal;

import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.EmailAttachment;

import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.MessagingException;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.BodyPart;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.DataHandler;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Aug 10, 2006
 * Time: 10:01:58 AM
 * To change this template use File | Settings | File Templates.
 */
public class MailMessage {
    private String stamp;
    private BigDecimal stampValue;
    private String actualMailBody;
    private String oivMailBody;
    private String oivFileName;
    private File attachement;
    private String oivSection;
    private MultiPartEmail oivMessage;
    private String smtpServer;
    private String smtpUserName;
    private String smtpPassword;
    private String smtpRelayEmail;
    private String fromEmail;
    private String fromName;
    private String replyName;
    private String replyTo;
    private String toEmail;
    private String toName;
    private String bounceEmail;
    private String subject;
    private String clientCode;
    private String campaignId;
    public String getStamp() {
        return stamp;
    }

    public void setStamp(String stamp) {
        this.stamp = stamp;
    }

    public String getActualMailBody() {
        return actualMailBody;
    }

    public void setActualMailBody(String actualMailBody) {
        this.actualMailBody = actualMailBody;
    }

    public String getOivMailBody() {
        return oivMailBody;
    }

    public void setOivMailBody(String oivMailBody) {
        this.oivMailBody = oivMailBody;
    }

    public String getOivFileName() {
        return oivFileName;
    }

    public void setOivFileName(String oivFileName) {
        this.oivFileName = oivFileName;
    }

    public String getSmtpUserName() {
        return smtpUserName;
    }

    public void setSmtpUserName(String smtpUserName) {
        this.smtpUserName = smtpUserName;
    }

    public String getSmtpPassword() {
        return smtpPassword;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getSmtpRelayEmail() {
        return smtpRelayEmail;
    }

    public void setSmtpRelayEmail(String smtpRelayEmail) {
        this.smtpRelayEmail = smtpRelayEmail;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }
    
    public Message getStampedTextEmail()
    {
    	 Message mimeMessage = null;
         HtmlEmail email = new HtmlEmail();
         email.setHostName(this.smtpServer);
         email.setAuthentication(this.smtpUserName,this.smtpPassword);
         try {
             email.addTo(this.toEmail, this.toName);
         // String fromAddress= ApplicationProperty.getProperty("support.email");  //TODO: need to change it to use users SMTP
          email.setFrom(this.smtpRelayEmail,this.fromName);
          String subject=ApplicationProperty.getProperty("stamp.mail.subject")+
                  " - ";
          if(clientCode != null && clientCode.length() > 1)
          {       
          	subject = subject+this.clientCode +" - ";
          }
          subject = subject+this.subject;
          email.setSubject(subject);
          email.addReplyTo(this.replyTo, this.replyName);
          email.setBounceAddress(this.bounceEmail);
          email.addHeader("X-CampaignId",this.campaignId);
          //email.setFrom(this.fromEmail,this.fromName);
         // email.setSubject(this.subject);
             email.buildMimeMessage();
             mimeMessage = email.getMimeMessage();
             addMailContentToEmail(mimeMessage);
          } catch (EmailException e) {
             e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
         }
         return mimeMessage;
    	
    }
    
    private void addTextMailContentToEmail(Message mimeMessage) {
        Multipart multipart = new MimeMultipart();
        BodyPart messageBodyPart = new MimeBodyPart();
        try {
           
        	System.setProperty("mail.mime.charset","iso-8859-1");
            messageBodyPart.setContent(this.oivMailBody, "text/plain");
            messageBodyPart.setHeader("Content-Transfer-Encoding","8bit");
            multipart.addBodyPart(messageBodyPart);
            /*BodyPart attachement = new MimeBodyPart();
            File oivFile = getOivFile();
            DataSource source = new FileDataSource(oivFile);
            attachement.setDataHandler(new DataHandler(source));
            attachement.setFileName(oivFile.getName());
            attachement.setHeader( "Content-Transfer-Encoding", "base64" );
            multipart.addBodyPart(attachement);*/
            mimeMessage.setContent(multipart);
            
        } catch (MessagingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public Message getStampedMail(){
       Message mimeMessage = null;
     
       HtmlEmail email = new HtmlEmail();
       email.setHostName(this.smtpServer);
       email.setAuthentication(this.smtpUserName,this.smtpPassword);
       try {
           email.addTo(this.toEmail, this.toName);
       // String fromAddress= ApplicationProperty.getProperty("support.email");  //TODO: need to change it to use users SMTP
        email.setFrom(this.smtpRelayEmail,this.fromName);
        
      // String subject=ApplicationProperty.getProperty("stamp.mail.subject")+
       //         " - "+
       //        this.clientCode +" - "+
       //         this.subject;
        email.setSubject(this.subject);
        
        /*String subject=ApplicationProperty.getProperty("stamp.mail.subject")+
                " - ";
        if(this.clientCode != null && this.clientCode.length() > 1)
        {       
        	subject = subject+this.clientCode +" - ";
        }
        subject = subject+this.subject;
        email.setSubject(subject);*/
        email.addReplyTo(this.replyTo, this.replyName);
        email.setBounceAddress(this.bounceEmail);
        email.addHeader("X-CampaignId",this.campaignId);
        //email.setFrom(this.fromEmail,this.fromName);
       // email.setSubject(this.subject);
           email.buildMimeMessage();
           mimeMessage = email.getMimeMessage();
           addMailContentToEmail(mimeMessage);
        } catch (EmailException e) {
           e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
       }
       return mimeMessage;
   }

   private void addMailContentToEmail(Message mimeMessage) {
        Multipart multipart = new MimeMultipart();
        BodyPart messageBodyPart = new MimeBodyPart();
        try {
           
        	System.setProperty("mail.mime.charset","iso-8859-1");
            messageBodyPart.setContent(this.oivMailBody, "text/html");
            messageBodyPart.setHeader("Content-Transfer-Encoding","8bit");
            multipart.addBodyPart(messageBodyPart);
            /*BodyPart attachement = new MimeBodyPart();
            File oivFile = getOivFile();
            DataSource source = new FileDataSource(oivFile);
            attachement.setDataHandler(new DataHandler(source));
            attachement.setFileName(oivFile.getName());
            attachement.setHeader( "Content-Transfer-Encoding", "base64" );
            multipart.addBodyPart(attachement);*/
            mimeMessage.setContent(multipart);
            
        } catch (MessagingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public File getAttachement() {
        return attachement;
    }

    public void setAttachement(File attachement) {
        this.attachement = attachement;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

      public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public BigDecimal getStampValue() {
        return stampValue;
    }

    public void setStampValue(BigDecimal stampValue) {
        this.stampValue = stampValue;
    }

    public String getOivSection() {
        return oivSection;
    }

    public void setOivSection(String oivSection) {
        this.oivSection = oivSection;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    public String getBounceEmail() {
        return bounceEmail;
    }

    public void setBounceEmail(String bounceEmail) {
        this.bounceEmail = bounceEmail;
    }

	public String getReplyName() {
		return replyName;
	}

	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}
}