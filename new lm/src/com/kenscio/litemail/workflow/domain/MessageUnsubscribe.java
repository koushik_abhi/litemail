package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;
import java.util.Date;

import com.kenscio.litemail.workflow.common.BaseData;



public class MessageUnsubscribe extends BaseData implements Serializable {

	private static final long serialVersionUID = -7414837885372058477L;
	private long unsubId = 0;
	private long messageId =0;
	private long stampitListId =0;
	private long campaignId=0;
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public long getStampitListId() {
		return stampitListId;
	}
	public void setStampitListId(long stampitListId) {
		this.stampitListId = stampitListId;
	}
	
	public String getReciepientEmail() {
		return reciepientEmail;
	}
	public void setReciepientEmail(String reciepientEmail) {
		this.reciepientEmail = reciepientEmail;
	}

	public long getUnsubId() {
		return unsubId;
	}
	public void setUnsubId(long unsubId) {
		this.unsubId = unsubId;
	}
	public Date getUnsubscribeDate() {
		return unsubscribeDate;
	}
	public void setUnsubscribeDate(Date unsubscribeDate) {
		this.unsubscribeDate = unsubscribeDate;
	}
	public String getUnsubscribeLocation() {
		return unsubscribeLocation;
	}
	public void setUnsubscribeLocation(String unsubscribeLocation) {
		this.unsubscribeLocation = unsubscribeLocation;
	}
	public String getUnsubscribeIP() {
		return unsubscribeIP;
	}
	public void setUnsubscribeIP(String unsubscribeIP) {
		this.unsubscribeIP = unsubscribeIP;
	}
	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	public String getReciepientName() {
		return reciepientName;
	}
	public void setReciepientName(String reciepientName) {
		this.reciepientName = reciepientName;
	}
	private String reciepientEmail ="";
	private Date unsubscribeDate;
	
	private String unsubscribeLocation;
	private String unsubscribeIP;
	private String reciepientName = "";
	private String os="";
	private String device="";
	private String browser="";
	private String clientId="";
	
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	
	
}
