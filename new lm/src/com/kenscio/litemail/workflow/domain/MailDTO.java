package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Dec 9, 2006
 * Time: 12:01:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class MailDTO extends BaseData {
    private String fromEmail;
    private String fromName;
    private String toEmail;
    private String toName;
    private String subject;
    private String htmlMessage;
    private String alternativeTextMessage;
    private String smtpServer;
    private String attachmentFilePath;
    private String attachmentFileName;

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHtmlMessage() {
        return htmlMessage;
    }

    public void setHtmlMessage(String htmlMessage) {
        this.htmlMessage = htmlMessage;
    }

    public String getAlternativeTextMessage() {
        return alternativeTextMessage;
    }

    public void setAlternativeTextMessage(String alternativeTextMessage) {
        this.alternativeTextMessage = alternativeTextMessage;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getAttachmentFilePath() {
        return attachmentFilePath;
    }

    public void setAttachmentFilePath(String attachmentFilePath) {
        this.attachmentFilePath = attachmentFilePath;
    }

    public String getAttachmentFileName() {
        return attachmentFileName;
    }

    public void setAttachmentFileName(String attachmentFileName) {
        this.attachmentFileName = attachmentFileName;
    }


    public MailDTO() {
    }

}
