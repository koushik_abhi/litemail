package com.kenscio.litemail.workflow.domain;

import java.util.ArrayList;
import java.util.List;

public class EmtsResponse<T> {
	
	private String status = "";
	private int count =0;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(String responseTime) {
		this.responseTime = responseTime;
	}
	
	public List<T> getItems() {
		return items;
	}
	public void setItems(List<T> items) {
		this.items = items;
	}

	private String responseTime = "";
	private List<T> items = new ArrayList<T>();

}
