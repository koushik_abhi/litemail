package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

public class DomainCount extends BaseData{
	
    private String domainName;
    private Long listId;
    private long noOfEmails;
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public long getNoOfEmails() {
		return noOfEmails;
	}
	public void setNoOfEmails(long noOfEmails) {
		this.noOfEmails = noOfEmails;
	}
	public long getListId() {
		return listId;
	}
	public void setListId(long listId) {
		this.listId = listId;
	}
    
  
}
