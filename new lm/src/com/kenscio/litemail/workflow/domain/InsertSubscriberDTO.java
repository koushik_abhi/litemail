package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;


/**
 * Created by IntelliJ IDEA.
 * User: Administrator
 * Date: Jul 7, 2006
 * Time: 7:51:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class InsertSubscriberDTO extends BaseData {
    private String name;
    private String emailId;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

}
