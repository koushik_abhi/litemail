package com.kenscio.litemail.workflow.domain;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: May 12, 2006
 * Time: 3:48:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class CheckoutTokenDTO {
    private boolean tokenStatus;
    private String token;
    private String transactionId;
    private String[] errors;

    public boolean isTokenStatus() {
        return tokenStatus;
    }

    public void setTokenStatus(boolean tokenStatus) {
        this.tokenStatus = tokenStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String[] getErrors() {
        return errors;
    }

    public void setErrors(String[] errors) {
        this.errors = errors;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
