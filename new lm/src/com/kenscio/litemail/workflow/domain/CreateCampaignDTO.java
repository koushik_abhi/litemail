package com.kenscio.litemail.workflow.domain;

import java.io.File;

import com.kenscio.litemail.workflow.common.BaseData;


public class CreateCampaignDTO extends BaseData{
    private String campaignName;
    private String description;
    private Long stampitListId;
    private String fromName;
    private String fromAddress;
    private String postalAddress;
    private String replyName;
    private String enableLinkTracking ="";
    private String enableOpenTracking ="";
    private String replyTo;
    private String subject;
    private String stampValue;
    private String smtpServer;
    private String smtpUserName;
    private String smtpPassword;
    private boolean ackFlag;
    private boolean chatRequired=false;
    private String campaignMessage;
    private int imageSize;                      //To Get the Stamp image and upload to the server & some loaction into the system
    private String imageFileName;
    private byte[] imageFileData;
    private int attachedFileSize;
    private String attachedFileName;
    private byte[] attachedFileData;
    private boolean fromEmailFlag=false;    //checking whether from mail same as login email
    private String mailContentType;
    private Long copyCampaignId;            // used at copy camapign
    private String copyCampaignAttachedFileName;  // used at copy camapign
    private String copyCampaignStampImage;        // used at copy camapign
    public File zipMailBody;
    private String htmlFileName;

    public int getImageSize() {
        return imageSize;
    }

    public void setImageSize(int imageSize) {
        this.imageSize = imageSize;
    }

    public String getImageFileName() {
        return imageFileName;
    }

    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }

    public byte[] getImageFileData() {
        return imageFileData;
    }

    public void setImageFileData(byte[] imageFileData) {
        this.imageFileData = imageFileData;
    }

    public String getCampaignName() {

        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStampitListId() {
        return stampitListId;
    }

    public void setStampitListId(Long stampitListId) {
        this.stampitListId = stampitListId;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStampValue() {
        return stampValue;
    }

    public void setStampValue(String stampValue) {
        this.stampValue = stampValue;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getSmtpUserName() {
        return smtpUserName;
    }

    public void setSmtpUserName(String smtpUserName) {
        this.smtpUserName = smtpUserName;
    }

    public String getSmtpPassword() {
        return smtpPassword;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    public boolean isAckFlag() {
        return ackFlag;
    }

    public void setAckFlag(boolean ackFlag) {
        this.ackFlag = ackFlag;
    }

    public String getCampaignMessage() {
        return campaignMessage;
    }

    public void setCampaignMessage(String campaignMessage) {
        this.campaignMessage = campaignMessage;
    }

    public int getAttachedFileSize() {
        return attachedFileSize;
    }

    public void setAttachedFileSize(int attachedFileSize) {
        this.attachedFileSize = attachedFileSize;
    }

    public String getAttachedFileName() {
        return attachedFileName;
    }

    public void setAttachedFileName(String attachedFileName) {
        this.attachedFileName = attachedFileName;
    }

    public byte[] getAttachedFileData() {
        return attachedFileData;
    }

    public void setAttachedFileData(byte[] attachedFileData) {
        this.attachedFileData = attachedFileData;
    }

    public boolean isFromEmailFlag() {
        return fromEmailFlag;
    }

    public void setFromEmailFlag(boolean fromEmailFlag) {
        this.fromEmailFlag = fromEmailFlag;
    }

    public String getMailContentType() {
        return mailContentType;
    }

    public void setMailContentType(String mailContentType) {
        this.mailContentType = mailContentType;
    }

    public Long getCopyCampaignId() {
        return copyCampaignId;
    }

    public void setCopyCampaignId(Long copyCampaignId) {
        this.copyCampaignId = copyCampaignId;
    }

    public String getCopyCampaignAttachedFileName() {
        return copyCampaignAttachedFileName;
    }

    public void setCopyCampaignAttachedFileName(String copyCampaignAttachedFileName) {
        this.copyCampaignAttachedFileName = copyCampaignAttachedFileName;
    }

    public String getCopyCampaignStampImage() {
        return copyCampaignStampImage;
    }

    public void setCopyCampaignStampImage(String copyCampaignStampImage) {
        this.copyCampaignStampImage = copyCampaignStampImage;
    }

    public boolean getChatRequired(){
        return chatRequired;
    }

    public void setChatRequired(boolean chatRequired){
        this.chatRequired = chatRequired;
    }

    public File getZipMailBody() {
        return zipMailBody;
    }

    public void setZipMailBody(File zipMailBody) {
        this.zipMailBody = zipMailBody;
    }

    public String getHtmlFileName() {
        return htmlFileName;
    }

    public void setHtmlFileName(String htmlFileName) {
        this.htmlFileName = htmlFileName;
    }

	public String getReplyName() {
		return replyName;
	}

	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}

	public String getEnableLinkTracking() {
		return enableLinkTracking;
	}

	public void setEnableLinkTracking(String enableLinkTracking) {
		this.enableLinkTracking = enableLinkTracking;
	}

	public String getEnableOpenTracking() {
		return enableOpenTracking;
	}

	public void setEnableOpenTracking(String enableOpenTracking) {
		this.enableOpenTracking = enableOpenTracking;
	}

}
