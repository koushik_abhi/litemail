package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

public class AttributeMap extends BaseData{
	
    private String attributeName;
    private Long listId;
    private String dbattributeName;
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public Long getListId() {
		return listId;
	}
	public void setListId(Long listId) {
		this.listId = listId;
	}
	public String getDbattributeName() {
		return dbattributeName;
	}
	public void setDbattributeName(String dbattributeName) {
		this.dbattributeName = dbattributeName;
	}
	
}
