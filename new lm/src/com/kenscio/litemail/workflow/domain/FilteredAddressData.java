package com.kenscio.litemail.workflow.domain;

import java.util.List;


public class FilteredAddressData {
  private EmailData[]filtered;
  private EmailData[]remaining;
  
public EmailData[] getFiltered() {
	return filtered;
}
public EmailData[] getRemaining() {
	return remaining;
}
public void setFiltered(EmailData[] filtered) {
	this.filtered = filtered;
}
public void setRemaining(EmailData[] remaining) {
	this.remaining = remaining;
}

}
