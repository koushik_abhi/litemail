package com.kenscio.litemail.workflow.domain;

/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Apr 9, 2008
 * Time: 3:28:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClickThroughListDTO {
    private String name;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
