package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

public class CampaignStop extends BaseData
{
	 public Long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public long getStopRecId() {
		return stopRecId;
	}
	public void setStopRecId(long stopRecId) {
		this.stopRecId = stopRecId;
	}
	
	public long getStopId() {
		return stopId;
	}
	public void setStopId(long stopId) {
		this.stopId = stopId;
	}

	 private Long campaignId =0l;
	 private Long accountId = 0l;
	 private String status = "";
	 private long stopRecId=0l;
	 private long stopId =01;
	   

}
