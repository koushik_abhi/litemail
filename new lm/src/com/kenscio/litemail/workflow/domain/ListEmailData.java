package com.kenscio.litemail.workflow.domain;


public class ListEmailData extends EmailData{
	private Long listId; 
	private String email;
	private String name;
	public long getListId() {
		return listId;
	}
	public void setListId(long listId) {
		this.listId = listId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
