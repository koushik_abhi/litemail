package com.kenscio.litemail.workflow.domain;

public class CampaignBounce {

	private int hardBounce =0;
	private int softBounce=0;
	private int generalBounce=0;
	private int transientBounce=0;
	private int totalNoOfBounces=0;
	private int spamCount=0;
	private int unsubscribeCount;
	
	public int getUnsubscribeCount() {
		return unsubscribeCount;
	}
	public void setUnsubscribeCount(int unsubscribeCount) {
		this.unsubscribeCount = unsubscribeCount;
	}
	public int getHardBounce() {
		return hardBounce;
	}
	public void setHardBounce(int hardBounce) {
		this.hardBounce = hardBounce;
	}
	public int getSoftBounce() {
		return softBounce;
	}
	public void setSoftBounce(int softBounce) {
		this.softBounce = softBounce;
	}
	public int getGeneralBounce() {
		return generalBounce;
	}
	public void setGeneralBounce(int generalBounce) {
		this.generalBounce = generalBounce;
	}
	public int getTransientBounce() {
		return transientBounce;
	}
	public void setTransientBounce(int transientBounce) {
		this.transientBounce = transientBounce;
	}
	public int getTotalNoOfBounces() {
		return totalNoOfBounces;
	}
	public void setTotalNoOfBounces(int totalNoOfBounces) {
		this.totalNoOfBounces = totalNoOfBounces;
	}
	public int getSpamCount() {
		return spamCount;
	}
	public void setSpamCount(int spamCount) {
		this.spamCount = spamCount;
	}
	
	
}
