package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.kenscio.litemail.workflow.common.BaseData;

public class MailGunBounceSpamReport extends BaseData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int total_count =0;
	private List<MailGunBounceSpamObject> items = new ArrayList<MailGunBounceSpamObject>();
	public int getTotal_count() {
		return total_count;
	}
	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}
	public List<MailGunBounceSpamObject> getItems() {
		return items;
	}
	public void setItems(List<MailGunBounceSpamObject> items) {
		this.items = items;
	}
		
	
	

}
