package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: srikanth
 * Date: Apr 4, 2007
 * Time: 3:50:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class CheckCSVDTO extends BaseData {

    private int uploadedFileSize;
    private String uploadedFileName;
    private byte[] uploadedFileData;


    public int getUploadedFileSize() {
        return uploadedFileSize;
    }

    public void setUploadedFileSize(int uploadedFileSize) {
        this.uploadedFileSize = uploadedFileSize;
    }

    public String getUploadedFileName() {
        return uploadedFileName;
    }

    public void setUploadedFileName(String uploadedFileName) {
        this.uploadedFileName = uploadedFileName;
    }

    public byte[] getUploadedFileData() {
        return uploadedFileData;
    }

    public void setUploadedFileData(byte[] uploadedFileData) {
        this.uploadedFileData = uploadedFileData;
    }
}
