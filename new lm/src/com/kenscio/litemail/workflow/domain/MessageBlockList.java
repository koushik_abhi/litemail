package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;
import java.util.Date;

import com.kenscio.litemail.workflow.common.BaseData;



public class MessageBlockList extends BaseData implements Serializable{

	private static final long serialVersionUID = 5605825879552534532L;
	private long blockId= 0;
	private long campaignId=0;
	private long blockCode = 0;
	public long getBlockIdId() {
		return blockId;
	}
	public void setBlockId(long blockId) {
		this.blockId = blockId;
	}
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public long getStampitListId() {
		return stampitListId;
	}
	public void setStampitListId(long stampitListId) {
		this.stampitListId = stampitListId;
	}
	public String getReciepientEmail() {
		return reciepientEmail;
	}
	public void setReciepientEmail(String reciepientEmail) {
		this.reciepientEmail = reciepientEmail;
	}
	
	public String getOtherReason() {
		return otherReason;
	}
	public void setOtherReason(String otherReason) {
		this.otherReason = otherReason;
	}
	
	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	public String getReciepientName() {
		return reciepientName;
	}
	public void setReciepientName(String reciepientName) {
		this.reciepientName = reciepientName;
	}

	public Date getClickDate() {
		return clickDate;
	}
	public void setClickDate(Date clickDate) {
		this.clickDate = clickDate;
	}

	public String getClickLocation() {
		return clickLocation;
	}
	public void setClickLocation(String clickLocation) {
		this.clickLocation = clickLocation;
	}

	public String getClickIP() {
		return clickIP;
	}
	public void setClickIP(String clickIP) {
		this.clickIP = clickIP;
	}

	public long getBlockCode() {
		return blockCode;
	}
	public void setBlockCode(long blockCode) {
		this.blockCode = blockCode;
	}

	private long messageId = 0;
	private long stampitListId =0;
	private String reciepientEmail;
	private String otherReason  = "";
	private String reciepientName = "";
	
	private Date clickDate;
		
	private String clickLocation;
	private String clickIP;
	
	
	
}
