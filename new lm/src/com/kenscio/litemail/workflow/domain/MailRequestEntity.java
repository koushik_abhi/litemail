package com.kenscio.litemail.workflow.domain;


import java.math.BigDecimal;
import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Aug 10, 2006
 * Time: 3:33:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class MailRequestEntity {
    private String campaignName;
    private Long campaignWalletId;
    private BigDecimal stampValue;
    private String mailBody;
    private File attachement;
    private String smtpServer;
    private String smptpUserName;
    private String smtpPassword;
    private String smtpRelayEmail;
    private String fromEmail;
    private String fromName;
    private String replyTo;
    private String bounceEmail;
    private String subject;
    private Integer batchStartPosition;
    private String replyName;
    private EmailData[] emailDatas;

    public BigDecimal getStampValue() {
        return stampValue;
    }

    public void setStampValue(BigDecimal stampValue) {
        this.stampValue = stampValue;
    }

    public String getMailBody() {
        return mailBody;
    }

    public void setMailBody(String mailBody) {
        this.mailBody = mailBody;
    }

    public File getAttachement() {
        return attachement;
    }

    public void setAttachement(File attachement) {
        this.attachement = attachement;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getSmptpUserName() {
        return smptpUserName;
    }

    public void setSmptpUserName(String smptpUserName) {
        this.smptpUserName = smptpUserName;
    }

    public String getSmtpPassword() {
        return smtpPassword;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

  
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Long getCampaignWalletId() {
        return campaignWalletId;
    }

    public void setCampaignWalletId(Long campaignWalletId) {
        this.campaignWalletId = campaignWalletId;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public Integer getBatchStartPosition() {
        return batchStartPosition;
    }

    public void setBatchStartPosition(Integer batchStartPosition) {
        this.batchStartPosition = batchStartPosition;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getSmtpRelayEmail() {
        return smtpRelayEmail;
    }

    public void setSmtpRelayEmail(String smtpRelayEmail) {
        this.smtpRelayEmail = smtpRelayEmail;
    }

    public String getBounceEmail() {
        return bounceEmail;
    }

    public void setBounceEmail(String bounceEmail) {
        this.bounceEmail = bounceEmail;
    }

	public String getReplyName() {
		return replyName;
	}

	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}

	public EmailData[] getEmailDatas() {
		return emailDatas;
	}

	public void setEmailDatas(EmailData[] emailDatas) {
		this.emailDatas = emailDatas;
	}
}
