package com.kenscio.litemail.workflow.domain;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;


import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.kenscio.litemail.workflow.common.BaseData;
import com.kenscio.litemail.workflow.common.JsonDateSerializer;


public class ActionLogObject extends BaseData implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5799538682392916208L;
	private String actionName;
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public String getListId() {
		return listId;
	}
	public void setListId(String listId) {
		this.listId = listId;
	}
	public String getActionSummaryDetails() {
		return actionSummaryDetails;
	}
	public void setActionSummaryDetails(String actionSummaryDetails) {
		this.actionSummaryDetails = actionSummaryDetails;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	private String actionType;
	private String listId;
	private String actionSummaryDetails;
	private String userId;
	private String partner;
	private String status;
	private String accountId ;
	public String getActionSource() {
		return actionSource;
	}
	public void setActionSource(String actionSource) {
		this.actionSource = actionSource;
	}
	public String getActionFile() {
		return actionFile;
	}
	public void setActionFile(String actionFile) {
		this.actionFile = actionFile;
	}
	public String getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(String totalRecords) {
		this.totalRecords = totalRecords;
	}
	public String getDiscrepancyRecords() {
		return discrepancyRecords;
	}
	public void setDiscrepancyRecords(String discrepancyRecords) {
		this.discrepancyRecords = discrepancyRecords;
	}
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	private String actionSource;
	private String actionFile;
	private String totalRecords;
	private String importedRecords;
	private String description;
	private String discrepancyRecords;
	@JsonSerialize(using=JsonDateSerializer.class)
	private Date startTime;
	@JsonSerialize(using=JsonDateSerializer.class)
	private Date endTime;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	
	private byte[] actionLogFile;
	
	
	public byte[] getActionLogFile() {
		return actionLogFile;
	}
	public void setActionLogFile(byte[] actionLogFile) {
		this.actionLogFile = actionLogFile;
	}
	public String getImportedRecords() {
		return importedRecords;
	}
	public void setImportedRecords(String importedRecords) {
		this.importedRecords = importedRecords;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getActionType() {
		return actionType;
	}
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	private Long id;
}
