package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

/**
 * Created by IntelliJ IDEA.
 * User: srikanth
 * Date: Apr 5, 2007
 * Time: 3:36:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class InvalidEmailsDTO extends BaseData {

    private String name;
    private String emailId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
