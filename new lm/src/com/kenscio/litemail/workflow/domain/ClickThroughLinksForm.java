package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Jan 23, 2007
 * Time: 2:31:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class ClickThroughLinksForm extends BaseData {
    private String campaignMessage;
    private ClickThroughLinksDTO[] clickThroughLinksDTO;
    private int[] selectedItems;
    private String campaignId;  //set at EditClickThroughLinksAction
    private boolean selectedUrlFlag;
    private boolean unSelectedUrlFlag;

    public String getCampaignMessage() {
        return campaignMessage;
    }

    public void setCampaignMessage(String campaignMessage) {
        this.campaignMessage = campaignMessage;
    }

    public ClickThroughLinksDTO[] getClickThroughLinksDTO() {
        return clickThroughLinksDTO;
    }

    public void setClickThroughLinksDTO(ClickThroughLinksDTO[] clickThroughLinksDTO) {
        this.clickThroughLinksDTO = clickThroughLinksDTO;
    }

    public int[] getSelectedItems() {
        return selectedItems;
    }

    public void setSelectedItems(int[] selectedItems) {
        this.selectedItems = selectedItems;
    }

    public void reset() {
        this.selectedItems = null;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public boolean isSelectedUrlFlag() {
        return selectedUrlFlag;
    }

    public void setSelectedUrlFlag(boolean selectedUrlFlag) {
        this.selectedUrlFlag = selectedUrlFlag;
    }

    public boolean isUnSelectedUrlFlag() {
        return unSelectedUrlFlag;
    }

    public void setUnSelectedUrlFlag(boolean unSelectedUrlFlag) {
        this.unSelectedUrlFlag = unSelectedUrlFlag;
    }
}
