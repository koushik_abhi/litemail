package com.kenscio.litemail.workflow.domain;

import java.util.ArrayList;


public class ClickThroughLinksDTO {
    private String url;
    private String urlName="";
    private Long noOfClicks;
    private int position;
    private String urlId;
    private Long noOfemails;
    private boolean replacedUrlFlag;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    public Long getNoOfClicks() {
        return noOfClicks;
    }

    public void setNoOfClicks(Long noOfClicks) {
        this.noOfClicks = noOfClicks;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getUrlId() {
        return urlId;
    }

    public void setUrlId(String urlId) {
        this.urlId = urlId;
    }

    public Long getNoOfemails() {
        return noOfemails;
    }

    public void setNoOfemails(Long noOfemails) {
        this.noOfemails = noOfemails;
    }

    public boolean isReplacedUrlFlag() {
        return replacedUrlFlag;
    }

    public void setReplacedUrlFlag(boolean replacedUrlFlag) {
        this.replacedUrlFlag = replacedUrlFlag;
    }

}
