package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;
import com.kenscio.litemail.workflow.common.MoneyValue;

import java.util.Set;
import java.util.Iterator;


public class AccountEntity extends BaseData {
    private Long accountId = 0l;
    private String loginId;
    private String emailId;
    private String userName;
    private String password;
    private String businessName;
    private String partnerName;
    private String address;
    private Long mobileNumber;
    private Long phoneNumber;
    private String userRole;
    private boolean isActive;
    private Long creditLimit;
    private boolean isValidEmail;
    
    private Long costPerMail = 0l;
    
    private String description;
   
    public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setValidEmail(boolean isValidEmail) {
		this.isValidEmail = isValidEmail;
	}

	public void setValidPhone(boolean isValidPhone) {
		this.isValidPhone = isValidPhone;
	}

	public void setValidPhysicalAddress(boolean isValidPhysicalAddress) {
		this.isValidPhysicalAddress = isValidPhysicalAddress;
	}

	private String userType;
    private boolean isValidPhone;

    private boolean isValidPhysicalAddress;
    private String passwordQuestion;
    private String passwordAnswer;
   

    public String getPasswordQuestion() {
        return passwordQuestion;
    }

    public void setPasswordQuestion(String passwordQuestion) {
        this.passwordQuestion = passwordQuestion;
    }

    public String getPasswordAnswer() {
        return passwordAnswer;
    }

    public void setPasswordAnswer(String passwordAnswer) {
        this.passwordAnswer = passwordAnswer;
    }
  
    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public boolean getIsValidEmail() {
        return isValidEmail;
    }

    public void setIsValidEmail(boolean validEmail) {
        isValidEmail = validEmail;
    }

   

    public boolean getIsValidPhone() {
        return isValidPhone;
    }

    public void setIsValidPhone(boolean validPhone) {
        isValidPhone = validPhone;
    }

   

    public boolean getIsValidPhysicalAddress() {
        return isValidPhysicalAddress;
    }

    public void setIsValidPhysicalAddress(boolean validPhysicalAddress) {
        isValidPhysicalAddress = validPhysicalAddress;
    }

   
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public Long getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(Long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public Long getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(Long creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getCostPerMail() {
		return costPerMail;
	}

	public void setCostPerMail(Long costPerMail) {
		this.costPerMail = costPerMail;
	}

}
