package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;
import java.util.Date;

import com.kenscio.litemail.workflow.common.BaseData;



public class MessageLinkClick extends BaseData implements Serializable {

	private static final long serialVersionUID = -7414837885372058477L;
	private long linkClickId = 0;
	private long linkId = 0;
	private long messageId =0;
	private long stampitListId =0;
	private String linkUrl ="";
	private String linkEncodedUrl ="";
	private long campaignId=0;
	public long getLinkClickId() {
		return linkClickId;
	}
	public void setLinkClickId(long linkClickId) {
		this.linkClickId = linkClickId;
	}
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public long getStampitListId() {
		return stampitListId;
	}
	public void setStampitListId(long stampitListId) {
		this.stampitListId = stampitListId;
	}
	public String getLinkUrl() {
		return linkUrl;
	}
	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}
	public String getReciepientEmail() {
		return reciepientEmail;
	}
	public void setReciepientEmail(String reciepientEmail) {
		this.reciepientEmail = reciepientEmail;
	}
	public Date getClickDate() {
		return clickDate;
	}
	public void setClickDate(Date clickDate) {
		this.clickDate = clickDate;
	}
	public String getConversion() {
		return conversion;
	}
	public void setConversion(String conversion) {
		this.conversion = conversion;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	public String getClickLocation() {
		return clickLocation;
	}
	public void setClickLocation(String clickLocation) {
		this.clickLocation = clickLocation;
	}
	public String getClickIP() {
		return clickIP;
	}
	public void setClickIP(String clickIP) {
		this.clickIP = clickIP;
	}
	public String getLinkEncodedUrl() {
		return linkEncodedUrl;
	}
	public void setLinkEncodedUrl(String linkEncodedUrl) {
		this.linkEncodedUrl = linkEncodedUrl;
	}
	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	public String getReciepientName() {
		return reciepientName;
	}
	public void setReciepientName(String reciepientName) {
		this.reciepientName = reciepientName;
	}
	public long getLinkId() {
		return linkId;
	}
	public void setLinkId(long linkId) {
		this.linkId = linkId;
	}
	private String reciepientEmail ="";
	private Date clickDate;
	private String conversion;
	private String tagName;
	private String forward;
	
	private String clickLocation;
	private String clickIP;
	
	private String reciepientName = "";
	
}
