package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;
import java.util.Date;

import com.kenscio.litemail.workflow.common.BaseData;



public class MessageOpen extends BaseData implements Serializable {

	private static final long serialVersionUID = -7414837885372058477L;
	private long messageOpenId = 0;
	private long messageId =0;
	private long campaignId=0;
	private long stampitListId =0;
	
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public long getStampitListId() {
		return stampitListId;
	}
	public void setStampitListId(long stampitListId) {
		this.stampitListId = stampitListId;
	}
	
	public String getReciepientEmail() {
		return reciepientEmail;
	}
	public void setReciepientEmail(String reciepientEmail) {
		this.reciepientEmail = reciepientEmail;
	}
	public long getMessageOpenId() {
		return messageOpenId;
	}
	public void setMessageOpenId(long messageOpenId) {
		this.messageOpenId = messageOpenId;
	}
	public Date getOpenDate() {
		return openDate;
	}
	public void setOpenDate(Date openDate) {
		this.openDate = openDate;
	}
	public String getOpenLocation() {
		return openLocation;
	}
	public void setOpenLocation(String openLocation) {
		this.openLocation = openLocation;
	}
	public String getOpenIP() {
		return openIP;
	}
	public void setOpenIP(String openIP) {
		this.openIP = openIP;
	}
	public String getTrackingSrc() {
		return trackingSrc;
	}
	public void setTrackingSrc(String trackingSrc) {
		this.trackingSrc = trackingSrc;
	}
	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	public String getReciepientName() {
		return reciepientName;
	}
	public void setReciepientName(String reciepientName) {
		this.reciepientName = reciepientName;
	}
	private String reciepientEmail ="";
	private Date openDate;
	private String openLocation;
	private String openIP;
	private String trackingSrc;
	private String reciepientName = "";
	
}
