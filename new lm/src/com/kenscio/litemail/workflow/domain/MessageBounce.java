package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.kenscio.litemail.workflow.common.BaseData;
import com.kenscio.litemail.workflow.common.JsonDateSerializer;



public class MessageBounce extends BaseData implements Serializable{

	private static final long serialVersionUID = 5605825879552534532L;
	private long bounceId= 0;
	private long campaignId=0;
	private long count = 0;
	private long bounceCode = 0;
	private String clientId ="";
	private String domainName= "";
	private String serviceProvider = "";
	private String bounceType = "";
	public MessageBounce()
	{
		
	}
	public long getBounceId() {
		return bounceId;
	}
	public void setBounceId(long bounceId) {
		this.bounceId = bounceId;
	}
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public long getStampitListId() {
		return stampitListId;
	}
	public void setStampitListId(long stampitListId) {
		this.stampitListId = stampitListId;
	}
	public String getReciepientEmail() {
		return reciepientEmail;
	}
	public void setReciepientEmail(String reciepientEmail) {
		this.reciepientEmail = reciepientEmail;
	}
	public String getHardBounce() {
		return hardBounce;
	}
	public void setHardBounce(String hardBounce) {
		this.hardBounce = hardBounce;
	}
		
	public String getInboxFull() {
		return inboxFull;
	}
	public void setInboxFull(String inboxFull) {
		this.inboxFull = inboxFull;
	}
	public String getOutOfOffice() {
		return outOfOffice;
	}
	public void setOutOfOffice(String outOfOffice) {
		this.outOfOffice = outOfOffice;
	}
	public String getOtherReason() {
		return otherReason;
	}
	public void setOtherReason(String otherReason) {
		this.otherReason = otherReason;
	}
	public String getReasonForBounce() {
		return reasonForBounce;
	}
	public void setReasonForBounce(String reasonForBounce) {
		this.reasonForBounce = reasonForBounce;
	}
	public String getInvalidMailId() {
		return invalidMailId;
	}
	public void setInvalidMailId(String invalidMailId) {
		this.invalidMailId = invalidMailId;
	}
	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	public String getReciepientName() {
		return reciepientName;
	}
	public void setReciepientName(String reciepientName) {
		this.reciepientName = reciepientName;
	}
	public String getSoftBounce() {
		return softBounce;
	}
	public void setSoftBounce(String softBounce) {
		this.softBounce = softBounce;
	}
	public long getBounceCode() {
		return bounceCode;
	}
	public void setBounceCode(long bounceCode) {
		this.bounceCode = bounceCode;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getBounceTime() {
		return bounceTime;
	}
	public void setBounceTime(Date bounceTime) {
		this.bounceTime = bounceTime;
	}
	public String getDomainNotExist() {
		return domainNotExist;
	}
	public void setDomainNotExist(String domainNotExist) {
		this.domainNotExist = domainNotExist;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getBounceType() {
		return bounceType;
	}
	public void setBounceType(String bounceType) {
		this.bounceType = bounceType;
	}
	public String getServiceProvider() {
		return serviceProvider;
	}
	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	private long messageId = 0;
	private long stampitListId =0;
	private String reciepientEmail;
	private String hardBounce;
	private String softBounce;
	private String invalidMailId;
	private String inboxFull ="";
	private String outOfOffice="";
	private String reasonForBounce="";
	private String otherReason  = "";
	private String reciepientName = "";
	@JsonSerialize(using=JsonDateSerializer.class)
	private Date bounceTime=null;
	private String domainNotExist ="";
	
	
	
}
