package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

public class AttributeEntity extends BaseData {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long attrId;
	private String attrName;
	private String attrType;
	private String attrInitialValue;
	private Long accountId = 0l;
	private String description;
	private String partnerName;
	
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public Long getAttrId() {
		return attrId;
	}
	public void setAttrId(Long attrId) {
		this.attrId = attrId;
	}
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	public String getAttrType() {
		return attrType;
	}
	public void setAttrType(String attrType) {
		this.attrType = attrType;
	}
	public String getAttrInitialValue() {
		return attrInitialValue;
	}
	public void setAttrInitialValue(String attrInitialValue) {
		this.attrInitialValue = attrInitialValue;
	}


	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	
	
	
	
	
}
