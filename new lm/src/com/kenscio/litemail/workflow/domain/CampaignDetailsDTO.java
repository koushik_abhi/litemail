package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;


/**
 * Created by IntelliJ IDEA.
 * User: Comp
 * Date: Aug 16, 2006
 * Time: 5:21:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class CampaignDetailsDTO extends BaseData {
    private String campaignName;
    private String mailingListName;
    private String fromName;
    private String replyTo;
    private String subject;
    private String stampValue;
    private String campaignStartDateTime;
    private Long campaingId;
    private String campaignMessage;
    private String fromEmailId;
    private String stampImage;
    private Long listId;
    private String campaignOffer;

    public Long getCampaingId() {
        return campaingId;
    }

    public void setCampaingId(Long campaingId) {
        this.campaingId = campaingId;
    }

    public String getCampaignMessage() {
        return campaignMessage;
    }

    public void setCampaignMessage(String campaignMessage) {
        this.campaignMessage = campaignMessage;
    }

    public String getCampaignStartDateTime() {
        return campaignStartDateTime;
    }

    public void setCampaignStartDateTime(String campaignStartDateTime) {
        this.campaignStartDateTime = campaignStartDateTime;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getMailingListName() {
        return mailingListName;
    }

    public void setMailingListName(String mailingListName) {
        this.mailingListName = mailingListName;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStampValue() {
        return stampValue;
    }

    public void setStampValue(String stampValue) {
        this.stampValue = stampValue;
    }

    public String getFromEmailId() {
        return fromEmailId;
    }

    public void setFromEmailId(String fromEmailId) {
        this.fromEmailId = fromEmailId;
    }

    public String getStampImage() {
        return stampImage;
    }

    public void setStampImage(String stampImage) {
        this.stampImage = stampImage;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public String getCampaignOffer() {
        return campaignOffer;
    }

    public void setCampaignOffer(String campaignOffer) {
        this.campaignOffer = campaignOffer;
    }
}
