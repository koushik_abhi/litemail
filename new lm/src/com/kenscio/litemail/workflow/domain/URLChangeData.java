package com.kenscio.litemail.workflow.domain;


public class URLChangeData {
   private Integer urlPosition;
   private String urlName;

    public Integer getUrlPosition() {
        return urlPosition;
    }

    public void setUrlPosition(Integer urlPosition) {
        this.urlPosition = urlPosition;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }
}
