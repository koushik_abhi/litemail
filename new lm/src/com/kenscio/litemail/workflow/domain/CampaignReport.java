package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.kenscio.litemail.workflow.domain.SendOutMessage;
import com.kenscio.litemail.workflow.common.BaseData;

public class CampaignReport  extends BaseData implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long messageId =0l;
	private Long campaignId=0l;
	private Long listId =0l;
	
	List<MessageBounce> messageBounceList = new ArrayList<MessageBounce>();
	List<MessageBounce> hardBounceList = new ArrayList<MessageBounce>();
	List<MessageBounce> softBounceList = new ArrayList<MessageBounce>();
	List<MessageBounce> messageBlockList = new ArrayList<MessageBounce>();
	public List<MessageBounce> getMessageBounceList() {
		return messageBounceList;
	}
	public void setMessageBounceList(List<MessageBounce> messageBounceList) {
		this.messageBounceList = messageBounceList;
	}
	public List<MessageBounce> getHardBounceList() {
		return hardBounceList;
	}
	public void setHardBounceList(List<MessageBounce> hardBounceList) {
		this.hardBounceList = hardBounceList;
	}
	public List<MessageBounce> getSoftBounceList() {
		return softBounceList;
	}
	public void setSoftBounceList(List<MessageBounce> softBounceList) {
		this.softBounceList = softBounceList;
	}
	public List<MessageBounce> getMessageBlockList() {
		return messageBlockList;
	}
	public void setMessageBlockList(List<MessageBounce> messageBlockList) {
		this.messageBlockList = messageBlockList;
	}
	public List<MessageSpam> getMessageSpamList() {
		return messageSpamList;
	}
	public void setMessageSpamList(List<MessageSpam> messageSpamList) {
		this.messageSpamList = messageSpamList;
	}
	public List<MessageOpen> getMessageOpenList() {
		return messageOpenList;
	}
	public void setMessageOpenList(List<MessageOpen> messageOpenList) {
		this.messageOpenList = messageOpenList;
	}
	public List<MessageOpen> getMessageUniqueOpenList() {
		return messageUniqueOpenList;
	}
	public void setMessageUniqueOpenList(List<MessageOpen> messageUniqueOpenList) {
		this.messageUniqueOpenList = messageUniqueOpenList;
	}
	public List<MessageLinkClick> getMessageLinkClickList() {
		return messageLinkClickList;
	}
	public void setMessageLinkClickList(List<MessageLinkClick> messageLinkClickList) {
		this.messageLinkClickList = messageLinkClickList;
	}
	public List<MessageLinkClick> getMessageLinkUniqueClickList() {
		return messageLinkUniqueClickList;
	}
	public void setMessageLinkUniqueClickList(
			List<MessageLinkClick> messageLinkUniqueClickList) {
		this.messageLinkUniqueClickList = messageLinkUniqueClickList;
	}
	public List<SendOutMessage> getSendOutMessageList() {
		return sendOutMessageList;
	}
	public void setSendOutMessageList(List<SendOutMessage> sendOutMessageList) {
		this.sendOutMessageList = sendOutMessageList;
	}
	public List<MessageUnsubscribe> getUnsubscribeObjectList() {
		return unsubscribeObjectList;
	}
	public void setUnsubscribeObjectList(
			List<MessageUnsubscribe> unsubscribeObjectList) {
		this.unsubscribeObjectList = unsubscribeObjectList;
	}
	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	
	
	public Long getListId() {
		return listId;
	}
	public void setListId(Long listId) {
		this.listId = listId;
	}
	List<MessageSpam> messageSpamList = new ArrayList<MessageSpam>();
	List<MessageOpen> messageOpenList = new ArrayList<MessageOpen>();
	List<MessageOpen> messageUniqueOpenList = new ArrayList<MessageOpen>();
	List<MessageLinkClick> messageLinkClickList = new ArrayList<MessageLinkClick>();
	List<MessageLinkClick> messageLinkUniqueClickList = new ArrayList<MessageLinkClick>();
	List<SendOutMessage> sendOutMessageList = new ArrayList<SendOutMessage>();
	List<MessageUnsubscribe> unsubscribeObjectList = new ArrayList<MessageUnsubscribe>();
	

}
