package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;
import com.kenscio.litemail.workflow.common.JsonDateSerializer;
import com.kenscio.litemail.workflow.common.MoneyValue;


import java.sql.Timestamp;
import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;


public class Campaign extends BaseData {
	
    private Long campaignId =0l;
    private Long accountId = 0l;
    private String campaignName;
    private String listName;
    private String description;
    private String campaignMessage;
    private Long listId =0l;
    private String fromName;
    private String fromAddress;
    private String postalAddress;
    private String replyName;
    private String enableLinkTracking;
    private String enableOpenTracking;
    private String replyTo;
    private String subject;
    private String smtpServer;
    private String adRequired;
    private String smtpServerName;
    private String smtpUserName;
    private String smtpPassword;
    private String campaignWallet;
    private String status;
    @JsonSerialize(using=JsonDateSerializer.class)
    private Timestamp scheduleTime;
    @JsonSerialize(using=JsonDateSerializer.class)
    private Timestamp campaignStartDateTime;
    @JsonSerialize(using=JsonDateSerializer.class)
    private Timestamp campaignEndDateTime;
    private Long mailsSent;
    private Long mailsFailed;
    private Long noOfMails;
    private Long noOfMailsRedeemed;
    private Long campaignSpeed;
    private int imageSize;
    private String imageFileName;
    private byte[] imageFileData;
    
    private byte[] attachedFileData;
    private String liveChatURL;

    private String imageRadiobutton;
    private String attachmentFileName;
    private boolean ackFlag;
    private String optOutText;
    private String mailContentType;

    private Long confirmationNo;
    private boolean isConfirmFromAddress;
    private Long noOfStampsExpired;

    private boolean deleteCampaignFlag;
    private String HtmlFileName;
    private boolean showcaseActiveFlag;
    private String clientName;
    private String campaignOffer;
    private Long showcaseCampaignId;
    private String mta;
    private String mtaExcludeFilter;
    private Long noOfshowcaseClicks;
    private String stampGenStatus;
   
    private boolean unSealedFlag;
    private Long campaignStartCount;
    
    private String openTrackingSrc = "";
    private String unsubTrackingUrl  = "";
    
    private Long totalOpensCount;
    private Long totalClicksCount;
    private String partnerName="";
    
    
	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getOpenTrackingSrc() {
		return openTrackingSrc;
	}

	public void setOpenTrackingSrc(String openTrackingSrc) {
		this.openTrackingSrc = openTrackingSrc;
	}

	public String getUnsubTrackingUrl() {
		return unsubTrackingUrl;
	}

	public void setUnsubTrackingUrl(String unsubTrackingUrl) {
		this.unsubTrackingUrl = unsubTrackingUrl;
	}

	public String getMtaExcludeFilter() {
		return mtaExcludeFilter;
	}

	public void setMtaExcludeFilter(String mtaExcludeFilter) {
		this.mtaExcludeFilter = mtaExcludeFilter;
	}

	public Long getTotalOpensCount() {
		return totalOpensCount;
	}

	public void setTotalOpensCount(Long totalOpensCount) {
		this.totalOpensCount = totalOpensCount;
	}

	public Long getTotalClicksCount() {
		return totalClicksCount;
	}

	public void setTotalClicksCount(Long totalClicksCount) {
		this.totalClicksCount = totalClicksCount;
	}

	public Long getUniqueOpensCount() {
		return uniqueOpensCount;
	}

	public void setUniqueOpensCount(Long uniqueOpensCount) {
		this.uniqueOpensCount = uniqueOpensCount;
	}

	public Long getUniqueClickCount() {
		return uniqueClickCount;
	}

	public void setUniqueClickCount(Long uniqueClickCount) {
		this.uniqueClickCount = uniqueClickCount;
	}

	public Long getUnSubscribeCount() {
		return unSubscribeCount;
	}

	public void setUnSubscribeCount(Long unSubscribeCount) {
		this.unSubscribeCount = unSubscribeCount;
	}

	public Long getHardBounceCount() {
		return hardBounceCount;
	}

	public void setHardBounceCount(Long hardBounceCount) {
		this.hardBounceCount = hardBounceCount;
	}

	public Long getSoftBounceCount() {
		return softBounceCount;
	}

	public void setSoftBounceCount(Long softBounceCount) {
		this.softBounceCount = softBounceCount;
	}

	public Long getGeneralBounceCount() {
		return generalBounceCount;
	}

	public void setGeneralBounceCount(Long generalBounceCount) {
		this.generalBounceCount = generalBounceCount;
	}

	public Long getTotalBounceCount() {
		return totalBounceCount;
	}

	public void setTotalBounceCount(Long totalBounceCount) {
		this.totalBounceCount = totalBounceCount;
	}

	public Long getOtherCount() {
		return otherCount;
	}

	public void setOtherCount(Long otherCount) {
		this.otherCount = otherCount;
	}

	public Long getReplyCount() {
		return replyCount;
	}

	public void setReplyCount(Long replyCount) {
		this.replyCount = replyCount;
	}

	public Long getSpamCount() {
		return spamCount;
	}

	public void setSpamCount(Long spamCount) {
		this.spamCount = spamCount;
	}

	public void setConfirmFromAddress(boolean isConfirmFromAddress) {
		this.isConfirmFromAddress = isConfirmFromAddress;
	}

	private Long uniqueOpensCount;
    private Long uniqueClickCount;
    
    private Long unSubscribeCount;
    private Long hardBounceCount;
    private Long softBounceCount;
    private Long generalBounceCount;
    private Long totalBounceCount;
    private Long otherCount;
    private Long replyCount;
    private Long spamCount;
    
    
    
    public Long getNoOfStampsExpired() {
        return noOfStampsExpired;
    }

    public void setNoOfStampsExpired(Long noOfStampsExpired) {
        this.noOfStampsExpired = noOfStampsExpired;
    }

    public Long getConfirmationNo() {
        return confirmationNo;
    }

    public void setConfirmationNo(Long confirmationNo) {
        this.confirmationNo = confirmationNo;
    }

    public boolean getIsConfirmFromAddress() {
        return isConfirmFromAddress;
    }

    public void setIsConfirmFromAddress(boolean confirmFromAddress) {
        isConfirmFromAddress = confirmFromAddress;
    }

    public String getImageRadiobutton() {
        return imageRadiobutton;
    }

    public void setImageRadiobutton(String imageRadiobutton) {
        this.imageRadiobutton = imageRadiobutton;
    }

    public int getImageSize() {
        return imageSize;
    }

    public void setImageSize(int imageSize) {
        this.imageSize = imageSize;
    }

    public String getImageFileName() {
        return imageFileName;
    }

    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
    }

    public byte[] getImageFileData() {
        return imageFileData;
    }

    public void setImageFileData(byte[] imageFileData) {
        this.imageFileData = imageFileData;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCampaignMessage() {
        return campaignMessage;
    }

    public void setCampaignMessage(String campaignMessage) {
        this.campaignMessage = campaignMessage;
    }

    
    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getSmtpUserName() {
        return smtpUserName;
    }

    public void setSmtpUserName(String smtpUserName) {
        this.smtpUserName = smtpUserName;
    }

    public String getSmtpPassword() {
        return smtpPassword;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    public String getCampaignWallet() {
        return campaignWallet;
    }

    public void setCampaignWallet(String campaignWallet) {
        this.campaignWallet = campaignWallet;
    }
    @JsonSerialize(using=JsonDateSerializer.class)
    public Timestamp getCampaignStartDateTime() {
        return campaignStartDateTime;
    }

    public void setCampaignStartDateTime(Timestamp campaignStartDateTime) {
        this.campaignStartDateTime = campaignStartDateTime;
    }
    @JsonSerialize(using=JsonDateSerializer.class)
    public Timestamp getCampaignEndDateTime() {
        return campaignEndDateTime;
    }

    public void setCampaignEndDateTime(Timestamp campaignEndDateTime) {
        this.campaignEndDateTime = campaignEndDateTime;
    }

    

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public Long getMailsFailed() {
        return mailsFailed;
    }

    public void setMailsFailed(Long mailsFailed) {
        this.mailsFailed = mailsFailed;
    }

    public Long getMailsSent() {
        return mailsSent;
    }

    public void setMailsSent(Long mailsSent) {
        this.mailsSent = mailsSent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getNoOfMails() {
        return noOfMails;
    }

    public void setNoOfMails(Long noOfMails) {
        this.noOfMails = noOfMails;
    }

    

    public String getAttachmentFileName() {
        return attachmentFileName;
    }

    public void setAttachmentFileName(String attachmentFileName) {
        this.attachmentFileName = attachmentFileName;
    }

    public boolean getAckFlag() {
        return ackFlag;
    }

    public void setAckFlag(boolean ackFlag) {
        this.ackFlag = ackFlag;
    }

    public String getOptOutText() {
        return optOutText;
    }

    public void setOptOutText(String optOutText) {
        this.optOutText = optOutText;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public Long getNoOfMailsRedeemed() {
        return noOfMailsRedeemed;
    }

    public void setNoOfMailsRedeemed(Long noOfMailsRedeemed) {
        this.noOfMailsRedeemed = noOfMailsRedeemed;
    }

    public String getMailContentType() {
        return mailContentType;
    }

    public void setMailContentType(String mailContentType) {
        this.mailContentType = mailContentType;
    }

    public boolean getDeleteCampaignFlag() {
        return deleteCampaignFlag;
    }

    public void setDeleteCampaignFlag(boolean deleteCampaignFlag) {
        this.deleteCampaignFlag = deleteCampaignFlag;
    }

    
    
    public String getLiveChatURL(){
        return liveChatURL;
    }

    public void setLiveChatURL(String liveChatURL){
        this.liveChatURL = liveChatURL;
    }

    
    public String getHtmlFileName() {
        return HtmlFileName;
    }

    public void setHtmlFileName(String htmlFileName) {
        HtmlFileName = htmlFileName;
    }

    public boolean getShowcaseActiveFlag() {
        return showcaseActiveFlag;
    }

    public void setShowcaseActiveFlag(boolean showcaseActiveFlag) {
        this.showcaseActiveFlag = showcaseActiveFlag;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getCampaignOffer() {
        return campaignOffer;
    }

    public void setCampaignOffer(String campaignOffer) {
        this.campaignOffer = campaignOffer;
    }

    public Long getShowcaseCampaignId() {
        return showcaseCampaignId;
    }

    public void setShowcaseCampaignId(Long showcaseCampaignId) {
        this.showcaseCampaignId = showcaseCampaignId;
    }

   
	public String getMTA() {
		return mta;
	}

	public void setMTA(String mta) {
		this.mta = mta;
	}

	public String getMTAExcludeFilter() {
		return mtaExcludeFilter;
	}

	public void setMTAExcludeFilter(String mtaExcludeFilter) {
		this.mtaExcludeFilter = mtaExcludeFilter;
	}

    public Long getNoOfshowcaseClicks() {
        return noOfshowcaseClicks;
    }

    public void setNoOfshowcaseClicks(Long noOfshowcaseClicks) {
        this.noOfshowcaseClicks = noOfshowcaseClicks;
    }

	
	public String getStampGenStatus() {
		return stampGenStatus;
	}

	public void setStampGenStatus(String stampGenStatus) {
		this.stampGenStatus = stampGenStatus;
	}

	public boolean getUnSealedFlag() {
		return unSealedFlag;
	}

	public void setUnSealedFlag(boolean unSealedFlag) {
		this.unSealedFlag = unSealedFlag;
	}

	public Long getCampaignStartCount() {
		return campaignStartCount;
	}

	public void setCampaignStartCount(Long campaignStartCount) {
		this.campaignStartCount = campaignStartCount;
	}

	public String getReplyName() {
		return replyName;
	}

	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}

	public String getEnableLinkTracking() {
		return enableLinkTracking;
	}

	public void setEnableLinkTracking(String enableLinkTracking) {
		this.enableLinkTracking = enableLinkTracking;
	}

	public String getEnableOpenTracking() {
		return enableOpenTracking;
	}

	public void setEnableOpenTracking(String enableOpenTracking) {
		this.enableOpenTracking = enableOpenTracking;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public Long getListId() {
		return listId;
	}

	public void setListId(Long listId) {
		this.listId = listId;
	}

	public String getSmtpServerName() {
		return smtpServerName;
	}

	public void setSmtpServerName(String smtpServerName) {
		this.smtpServerName = smtpServerName;
	}

	public Long getCampaignSpeed() {
		return campaignSpeed;
	}

	public void setCampaignSpeed(Long campaignSpeed) {
		this.campaignSpeed = campaignSpeed;
	}

	public byte[] getAttachedFileData() {
		return attachedFileData;
	}

	public void setAttachedFileData(byte[] attachedFileData) {
		this.attachedFileData = attachedFileData;
	}
	@JsonSerialize(using=JsonDateSerializer.class)
	public Timestamp getScheduleTime() {
		return scheduleTime;
	}

	public void setScheduleTime(Timestamp scheduleTime) {
		this.scheduleTime = scheduleTime;
	}

	public String getAdRequired() {
		return adRequired;
	}

	public void setAdRequired(String adRequired) {
		this.adRequired = adRequired;
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}
	
	public static void main(String ar[])
	{
		Campaign c = new Campaign();
		c.setCampaignStartDateTime(new Timestamp(new Date().getTime()));
		System.out.println("hi "+c.getCampaignStartDateTime());
	}

    
}
