package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jun 6, 2006
 * Time: 4:51:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class EmailData extends BaseData {
	private Long listId;
    private String name;
    private String emailId;
    private String stamp;
    private String clientCode;
    private String replace1;
    private String replace2;
    private String replace3;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }


    public boolean equals(Object obj) {
       EmailData email2=(EmailData)obj;
       if(email2.getEmailId().trim().equalsIgnoreCase(getEmailId().trim()))
           return true;
       else
           return false;
    }

    public String getReplace1() {
        return replace1;
    }

    public void setReplace1(String replace1) {
        this.replace1 = replace1;
    }

    public String getReplace2() {
        return replace2;
    }

    public void setReplace2(String replace2) {
        this.replace2 = replace2;
    }

    public String getReplace3() {
        return replace3;
    }

    public void setReplace3(String replace3) {
        this.replace3 = replace3;
    }

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}

	public String getStamp() {
		return stamp;
	}

	public void setStamp(String stamp) {
		this.stamp = stamp;
	}

	public long getListId() {
		return listId;
	}

	public void setListId(long listId) {
		this.listId = listId;
	}
    
}
