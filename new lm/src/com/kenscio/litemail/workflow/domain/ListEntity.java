package com.kenscio.litemail.workflow.domain;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import com.kenscio.litemail.util.*;
import com.kenscio.litemail.workflow.common.BaseData;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jun 13, 2006
 * Time: 6:00:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class ListEntity extends BaseData {

    private Long listId =0l;
    private Long accountId =0l;
    private String listName;
    private String listDescription;
    private String listFileName;
    private boolean deleteListFlag;
    private long totalRecords;
    private long invalidRecords;
    private String status;
    @JsonSerialize(using=JsonDateSerializer.class)
    private Date uploadTime;
    private String partnerName="";
    
    public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    public String getListDescription() {
        return listDescription;
    }

    public void setListDescription(String listDescription) {
        this.listDescription = listDescription;
    }

    public String getListFileName() {
        return listFileName;
    }

    public void setListFileName(String listFileName) {
        this.listFileName = listFileName;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public boolean getDeleteListFlag() {
        return deleteListFlag;
    }

    public void setDeleteListFlag(boolean deleteListFlag) {
        this.deleteListFlag = deleteListFlag;
    }

	public long getInvalidRecords() {
		return invalidRecords;
	}

	public void setInvalidRecords(long invalidRecords) {
		this.invalidRecords = invalidRecords;
	}

	public long getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(long totalRecords) {
		this.totalRecords = totalRecords;
	}
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(Date uploadTime) {
		this.uploadTime = uploadTime;
	}


}
