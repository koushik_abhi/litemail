package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

public class AccountCredit extends BaseData {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long accountId = 0l;
	 private Long crId = 0l;
	 private Long balance = 0l;
	 private Long credits = 0l;
	 private Long debits = 0l;
	 private String transType;
	 
	 private String username="";
	 private String partnername="";
	 
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public Long getBalance() {
		return balance;
	}
	public void setBalance(Long balance) {
		this.balance = balance;
	}
	public Long getCredits() {
		return credits;
	}
	public void setCredits(Long credits) {
		this.credits = credits;
	}
	public Long getDebits() {
		return debits;
	}
	public void setDebits(Long debits) {
		this.debits = debits;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public Long getCrId() {
		return crId;
	}
	public void setCrId(Long crId) {
		this.crId = crId;
	}
	public String getPartnername() {
		return partnername;
	}
	public void setPartnername(String partnername) {
		this.partnername = partnername;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	

}
