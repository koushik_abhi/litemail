package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;


import java.io.File;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * User: Comp
 * Date: Jul 28, 2006
 * Time: 10:26:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class EditCampaignDTO extends BaseData {
    private String campaignName;
    private String description;
    private Long listId;
    private String mailingListName; //used at campaigndetails.jsp                           
    private String fromName;
    private String fromAddress;
    private String replyName;
    private String enableLinkTracking ="";
    private String enableOpenTracking ="";
    private String replyTo;
    private String postalAddress;
    private String subject;
    private String stampValue;
    private String smtpServer;
    private String smtpUserName;
    private String smtpPassword;
    private String campaignMessage;
    private List stampitListId;
    private List stampitListName;
    private boolean ackFlag;
    private String attachmentFileName;
    private String mailContentType;
    private String timeLeft;
    private String stampImage;
    private String currentBalance;
    private String campaignAmount;
    private String requiredAmount;
    private String walletId;
    private String stampitAccountId;
    private String creditLimit;
    private boolean importFlag;
    private boolean chatRequired;
    private boolean adRequired;
    private boolean status; // used at IsActiveCampaignAction
    private File zipMailBody;
    private String htmlFileName;
    private boolean showcase;
    private Long showcaseCampaignId;
    private Long showcaseAccountId;
    private String clientName;
    private String campaignOffer;
    private String bannerAd;
    private String mtaExcludeFilter;
    private String mta;
    private String stampGenStatus;
    private Long  stampCount;
    private String showMailerType;
    
    public String getStampImage() {
        return stampImage;
    }

    public void setStampImage(String stampImage) {
        this.stampImage = stampImage;
    }

    public String getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(String timeLeft) {
        this.timeLeft = timeLeft;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getDescription() {
        return description;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public String getMailingListName() {
        return mailingListName;
    }

    public void setMailingListName(String mailingListName) {
        this.mailingListName = mailingListName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getFromAddress() {
        return fromAddress;
    }

    public void setFromAddress(String fromAddress) {
        this.fromAddress = fromAddress;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(String postalAddress) {
        this.postalAddress = postalAddress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStampValue() {
        return stampValue;
    }

    public void setStampValue(String stampValue) {
        this.stampValue = stampValue;
    }

    public String getSmtpServer() {
        return smtpServer;
    }

    public void setSmtpServer(String smtpServer) {
        this.smtpServer = smtpServer;
    }

    public String getSmtpUserName() {
        return smtpUserName;
    }

    public void setSmtpUserName(String smtpUserName) {
        this.smtpUserName = smtpUserName;
    }

    public String getSmtpPassword() {
        return smtpPassword;
    }

    public void setSmtpPassword(String smtpPassword) {
        this.smtpPassword = smtpPassword;
    }

    public String getCampaignMessage() {
        return campaignMessage;
    }

    public void setCampaignMessage(String campaignMessage) {
        this.campaignMessage = campaignMessage;
    }

    public List getStampitListId() {
        return stampitListId;
    }

    public void setStampitListId(List stampitListId) {
        this.stampitListId = stampitListId;
    }

    public List getStampitListName() {
        return stampitListName;
    }

    public void setStampitListName(List stampitListName) {
        this.stampitListName = stampitListName;
    }

    public boolean isAckFlag() {
        return ackFlag;
    }

    public void setAckFlag(boolean ackFlag) {
        this.ackFlag = ackFlag;
    }

    public String getAttachmentFileName() {
        return attachmentFileName;
    }

    public void setAttachmentFileName(String attachmentFileName) {
        this.attachmentFileName = attachmentFileName;
    }

    public String getMailContentType() {
        return mailContentType;
    }

    public void setMailContentType(String mailContentType) {
        this.mailContentType = mailContentType;
    }

    public String getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getCampaignAmount() {
        return campaignAmount;
    }

    public void setCampaignAmount(String campaignAmount) {
        this.campaignAmount = campaignAmount;
    }

    public String getRequiredAmount() {
        return requiredAmount;
    }

    public void setRequiredAmount(String requiredAmount) {
        this.requiredAmount = requiredAmount;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getStampitAccountId() {
        return stampitAccountId;
    }

    public void setStampitAccountId(String stampitAccountId) {
        this.stampitAccountId = stampitAccountId;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public boolean getImportFlag() {
        return importFlag;
    }

    public void setImportFlag(boolean importFlag) {
        this.importFlag = importFlag;
    }

    public boolean isChatRequired(){
        return chatRequired;
    }

    public void setChatRequired(boolean chatRequired){
        this.chatRequired = chatRequired;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isAdRequired() {
        return adRequired;
    }

    public void setAdRequired(boolean adRequired) {
        this.adRequired = adRequired;
    }

    public File getZipMailBody() {
        return zipMailBody;
    }

    public void setZipMailBody(File zipMailBody) {
        this.zipMailBody = zipMailBody;
    }

    public String getHtmlFileName() {
        return htmlFileName;
    }

    public void setHtmlFileName(String htmlFileName) {
        this.htmlFileName = htmlFileName;
    }

    public boolean isShowcase() {
        return showcase;
    }

    public void setShowcase(boolean showcase) {
        this.showcase = showcase;
    }

    public Long getShowcaseCampaignId() {
        return showcaseCampaignId;
    }

    public void setShowcaseCampaignId(Long showcaseCampaignId) {
        this.showcaseCampaignId = showcaseCampaignId;
    }

    public Long getShowcaseAccountId() {
        return showcaseAccountId;
    }

    public void setShowcaseAccountId(Long showcaseAccountId) {
        this.showcaseAccountId = showcaseAccountId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getCampaignOffer() {
        return campaignOffer;
    }

    public void setCampaignOffer(String campaignOffer) {
        this.campaignOffer = campaignOffer;
    }

    public String getBannerAd() {
        return bannerAd;
    }

    public void setBannerAd(String bannerAd) {
        this.bannerAd = bannerAd;
    }

	public String getMTA() {
		return mta;
	}

	public void setMTA(String mta) {
		this.mta = mta;
	}

	public String getMTAExcludeFilter() {
		return mtaExcludeFilter;
	}

	public void setMTAExcludeFilter(String mtaExcludeFilter) {
		this.mtaExcludeFilter = mtaExcludeFilter;
	}

	public Long getStampCount() {
		return stampCount;
	}

	public void setStampCount(Long stampCount) {
		this.stampCount = stampCount;
	}

	public String getStampGenStatus() {
		return stampGenStatus;
	}

	public void setStampGenStatus(String stampGenStatus) {
		this.stampGenStatus = stampGenStatus;
	}

	public String getShowMailerType() {
		return showMailerType;
	}

	public void setShowMailerType(String showMailerType) {
		this.showMailerType = showMailerType;
	}

	public String getReplyName() {
		return replyName;
	}

	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}

	public String getEnableLinkTracking() {
		return enableLinkTracking;
	}

	public void setEnableLinkTracking(String enableLinkTracking) {
		this.enableLinkTracking = enableLinkTracking;
	}

	public String getEnableOpenTracking() {
		return enableOpenTracking;
	}

	public void setEnableOpenTracking(String enableOpenTracking) {
		this.enableOpenTracking = enableOpenTracking;
	}

	
	
    
}
