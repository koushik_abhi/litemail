package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.kenscio.litemail.workflow.common.BaseData;
import com.kenscio.litemail.workflow.common.JsonDateSerializer;



public class MessageSpam extends BaseData implements Serializable{

	private static final long serialVersionUID = 5605825879552534532L;
	private long campaignId =0;
	private long spamCode = 0;
	private long count = 0;
	private String clientId ="";
	public MessageSpam()
	{
		
	}
	
	public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public long getStampitListId() {
		return stampitListId;
	}
	public void setStampitListId(long stampitListId) {
		this.stampitListId = stampitListId;
	}
	public String getReciepientEmail() {
		return reciepientEmail;
	}
	public void setReciepientEmail(String reciepientEmail) {
		this.reciepientEmail = reciepientEmail;
	}
	
	public String getOtherReason() {
		return otherReason;
	}
	public void setOtherReason(String otherReason) {
		this.otherReason = otherReason;
	}
	
	public long getSpamId() {
		return spamId;
	}
	public void setSpamId(long spamId) {
		this.spamId = spamId;
	}
	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	public String getReciepientName() {
		return reciepientName;
	}
	public void setReciepientName(String reciepientName) {
		this.reciepientName = reciepientName;
	}
	public long getSpamCode() {
		return spamCode;
	}
	public void setSpamCode(long spamCode) {
		this.spamCode = spamCode;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	@JsonSerialize(using=JsonDateSerializer.class)
	public Date getClickDate() {
		return clickDate;
	}
	public void setClickDate(Date clickDate) {
		this.clickDate = clickDate;
	}
	public String getClickLocation() {
		return clickLocation;
	}
	public void setClickLocation(String clickLocation) {
		this.clickLocation = clickLocation;
	}
	public String getClickIP() {
		return clickIP;
	}
	public void setClickIP(String clickIP) {
		this.clickIP = clickIP;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(String serviceProvider) {
		this.serviceProvider = serviceProvider;
	}
	private long spamId = 0;
	private long messageId = 0;
	private long stampitListId =0;
	private String reciepientEmail;
	private String otherReason  = "";
	private String serviceProvider = "";
	
	private String reciepientName = "";
	@JsonSerialize(using=JsonDateSerializer.class)
	private Date clickDate=null;
	private String clickLocation="";
	private String clickIP="";
	private String domainName = "";
		
	
	
}
