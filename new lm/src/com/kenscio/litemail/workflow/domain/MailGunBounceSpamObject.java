package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;

import com.kenscio.litemail.workflow.common.BaseData;

public class MailGunBounceSpamObject extends BaseData implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1544605317259212966L;
	private int code = 0;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	private String created_at = "";
	private String error = "";
	private String address = "";
	private int count=0;
	
	

}
