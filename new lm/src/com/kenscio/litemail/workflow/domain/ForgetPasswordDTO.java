package com.kenscio.litemail.workflow.domain;


public class ForgetPasswordDTO {
    private String stampitUserName;
    private boolean staus;
    private String secretQuestion;
    private String secretAnswer;

    public String getStampitUserName() {
        return stampitUserName;
    }

    public void setStampitUserName(String stampitUserName) {
        this.stampitUserName = stampitUserName;
    }

    public boolean isStaus() {
        return staus;
    }

    public void setStaus(boolean staus) {
        this.staus = staus;
    }

    public String getSecretQuestion() {
        return secretQuestion;
    }

    public void setSecretQuestion(String secretQuestion) {
        this.secretQuestion = secretQuestion;
    }

    public String getSecretAnswer() {
        return secretAnswer;
    }

    public void setSecretAnswer(String secretAnswer) {
        this.secretAnswer = secretAnswer;
    }

}
