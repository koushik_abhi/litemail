package com.kenscio.litemail.workflow.domain;

public class JobDetailsObject extends JobDetails{
	

	private static final long serialVersionUID = -8805218173105123292L;
	private String jobStatus = "";
	private String jobName="";
	private String jobType="";
	private String status="";
	private String description;
	
	private int failureCount=0;
	private int successCount=0;
	private String nextFireTime="";
	
	private String error="";
	
	private String listName = "";
	
	private String actionName = "";
	private String actionSource = "";
	private String actionFile="";
	
	private String actionDataProvider="";
	private String dataproviderDBUrl = "";
	private String dataproviderUsername = "";
	private String dataproviderPassword = "";
	private String dataproviderServiceName = "";
	
	private String importFromCSV="";
	private String ftpServerHost = "";
	private String ftpUserName = "";
	private String ftpPassword = "";
	private String ftpFileLocation = "";
	
	private String scheduleStartDate ="";
	private String scheduleEndDate ="";
	private String repeatInterval = "";
	private int repeatIntervalHr = 0;
	
	private String userId = "";
	
	
	private String runNow = "false";
	
	public String getRunNow() {
		return runNow;
	}
	public void setRunNow(String runNow) {
		this.runNow = runNow;
	}
	
	private String group = "DataEnhance";
	
	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getListName() {
		return listName;
	}
	public void setListName(String listName) {
		this.listName = listName;
	}
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public String getActionFile() {
		return actionFile;
	}
	public void setActionFile(String actionFile) {
		this.actionFile = actionFile;
	}
	public String getActionDataProvider() {
		return actionDataProvider;
	}
	public void setActionDataProvider(String actionDataProvider) {
		this.actionDataProvider = actionDataProvider;
	}
	public String getScheduleStartDate() {
		return scheduleStartDate;
	}
	public void setScheduleStartDate(String scheduleStartDate) {
		this.scheduleStartDate = scheduleStartDate;
	}
	public String getScheduleEndDate() {
		return scheduleEndDate;
	}
	public void setScheduleEndDate(String scheduleEndDate) {
		this.scheduleEndDate = scheduleEndDate;
	}
	public String getRepeatInterval() {
		return repeatInterval;
	}
	public void setRepeatInterval(String repeatInterval) {
		this.repeatInterval = repeatInterval;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFtpServerHost() {
		return ftpServerHost;
	}
	public void setFtpServerHost(String ftpServerHost) {
		this.ftpServerHost = ftpServerHost;
	}
	public String getFtpUserName() {
		return ftpUserName;
	}
	public void setFtpUserName(String ftpUserName) {
		this.ftpUserName = ftpUserName;
	}
	public String getFtpPassword() {
		return ftpPassword;
	}
	public void setFtpPassword(String ftpPassword) {
		this.ftpPassword = ftpPassword;
	}
	public String getFtpFileLocation() {
		return ftpFileLocation;
	}
	public void setFtpFileLocation(String ftpFileLocation) {
		this.ftpFileLocation = ftpFileLocation;
	}
	public String getActionSource() {
		return actionSource;
	}
	public void setActionSource(String actionSource) {
		this.actionSource = actionSource;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public int getRepeatIntervalHr() {
		return repeatIntervalHr;
	}
	public void setRepeatIntervalHr(int repeatIntervalHr) {
		this.repeatIntervalHr = repeatIntervalHr;
	}
	public String getImportFromCSV() {
		return importFromCSV;
	}
	public void setImportFromCSV(String importFromCSV) {
		this.importFromCSV = importFromCSV;
	}
	public String getDataproviderDBUrl() {
		return dataproviderDBUrl;
	}
	public void setDataproviderDBUrl(String dataproviderDBUrl) {
		this.dataproviderDBUrl = dataproviderDBUrl;
	}
	public String getDataproviderUsername() {
		return dataproviderUsername;
	}
	public void setDataproviderUsername(String dataproviderUsername) {
		this.dataproviderUsername = dataproviderUsername;
	}
	public String getDataproviderPassword() {
		return dataproviderPassword;
	}
	public void setDataproviderPassword(String dataproviderPassword) {
		this.dataproviderPassword = dataproviderPassword;
	}
	public String getDataproviderServiceName() {
		return dataproviderServiceName;
	}
	public void setDataproviderServiceName(String dataproviderServiceName) {
		this.dataproviderServiceName = dataproviderServiceName;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public int getSuccessCount() {
		return successCount;
	}
	public void setSuccessCount(int successCount) {
		this.successCount = successCount;
	}
	public int getFailureCount() {
		return failureCount;
	}
	public void setFailureCount(int failureCount) {
		this.failureCount = failureCount;
	}
	public String getNextFireTime() {
		return nextFireTime;
	}
	public void setNextFireTime(String nextFireTime) {
		this.nextFireTime = nextFireTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
