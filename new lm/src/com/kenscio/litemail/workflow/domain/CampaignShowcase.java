package com.kenscio.litemail.workflow.domain;

/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Oct 16, 2008
 * Time: 1:35:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class CampaignShowcase {
    private String campaignSubject;
    private boolean isActive;
    private String campaignId;
    private String stampitAccountId;
    private String clientName;
    private boolean firstLineFlag;

    public String getCampaignSubject() {
        return campaignSubject;
    }

    public void setCampaignSubject(String campaignSubject) {
        this.campaignSubject = campaignSubject;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getStampitAccountId() {
        return stampitAccountId;
    }

    public void setStampitAccountId(String stampitAccountId) {
        this.stampitAccountId = stampitAccountId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public boolean getFirstLineFlag() {
        return firstLineFlag;
    }

    public void setFirstLineFlag(boolean firstLineFlag) {
        this.firstLineFlag = firstLineFlag;
    }
}
