package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;


import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;


public class ImportEmailDTO extends BaseData {
    private Long accountId;
    private Long listId;
    private byte[] csvData;
    private File csvFormFile;
    private boolean flag;
    private String activeMailCatagory;

  
    private Long specifiedNumberOfMail;                                        // used for user can specify no of emails address are required for mailing
     private Long noOfEmailAddressAvailableIntheQuery;                          //Usre to display the no of emails available in the specified rupeemail query

    private String[] selectedCategories;                                       //used for user selected categories
    private String selectall;

    private String city;
    private String state;
    private String sex;
    private String education;
    private String industry;
    private String jobCategory;
    private String annualIncome;
    private String ageGroup;
    private String maritalStatus;
    private String registeredFrom;
    private String registeredTo;
    private Double rank;
    private String ageFrom;
    private String ageTo;

    private boolean displayImportPageFlag = false; // to decide request from edit import page or display import page

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getListId() {
        return listId;
    }

    public void setListId(Long listId) {
        this.listId = listId;
    }

    public byte[] getCsvData() {
        return csvData;
    }

    public void setCsvData(byte[] csvData) {
        this.csvData = csvData;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    

    public String[] getSelectedCategories() {
        return selectedCategories;
    }

    public void setSelectedCategories(String[] selectedCategories) {
        this.selectedCategories = selectedCategories;
    }

    public Long getSpecifiedNumberOfMail() {
        return specifiedNumberOfMail;
    }

    public void setSpecifiedNumberOfMail(Long specifiedNumberOfMail) {
        this.specifiedNumberOfMail = specifiedNumberOfMail;
    }

  

    public Long getNoOfEmailAddressAvailableIntheQuery() {
        return noOfEmailAddressAvailableIntheQuery;
    }

    public void setNoOfEmailAddressAvailableIntheQuery(Long noOfEmailAddressAvailableIntheQuery) {
        this.noOfEmailAddressAvailableIntheQuery = noOfEmailAddressAvailableIntheQuery;
    }

    public String getSelectall() {
        return selectall;
    }

    public void setSelectall(String selectall) {
        this.selectall = selectall;
    }

    public String getActiveMailCatagory() {
        return activeMailCatagory;
    }

    public void setActiveMailCatagory(String activeMailCatagory) {
        this.activeMailCatagory = activeMailCatagory;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getJobCategory() {
        return jobCategory;
    }

    public void setJobCategory(String jobCategory) {
        this.jobCategory = jobCategory;
    }

    public String getAnnualIncome() {
        return annualIncome;
    }

    public void setAnnualIncome(String annualIncome) {
        this.annualIncome = annualIncome;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getRegisteredFrom() {
        return registeredFrom;
    }

    public void setRegisteredFrom(String registeredFrom) {
        this.registeredFrom = registeredFrom;
    }

    public String getRegisteredTo() {
        return registeredTo;
    }

    public void setRegisteredTo(String registeredTo) {
        this.registeredTo = registeredTo;
    }

    public boolean isDisplayImportPageFlag() {
        return displayImportPageFlag;
    }

    public void setDisplayImportPageFlag(boolean displayImportPageFlag) {
        this.displayImportPageFlag = displayImportPageFlag;
    }
    
    public Double getRank() {
		return rank;
	}

	public void setRank(Double rank) {
		this.rank = rank;
	}

    public String getAgeFrom() {
        return ageFrom;
    }

    public void setAgeFrom(String ageFrom) {
        this.ageFrom = ageFrom;
    }

    public String getAgeTo() {
        return ageTo;
    }

    public void setAgeTo(String ageTo) {
        this.ageTo = ageTo;
    }

	public File getCsvFormFile() {
		return csvFormFile;
	}

	public void setCsvFormFile(File csvFormFile) {
		this.csvFormFile = csvFormFile;
	}
}
