package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;

public class CampaignLinks  extends BaseData {

    private Long urlId;
    private Long campaignId;
    private String url;
    private String urlName;
    private Long noOfClicks;
    private String currentStatus;
    private String redirectUrl;
    private long position = 0;

    public Long getCampaignId(){
        return campaignId;
    }
    public void setCampaignId(Long campaignId){
        this.campaignId = campaignId;
    }
    public Long getNoOfClicks(){
        return noOfClicks;
    }
    public void setNoOfClicks(Long noOfClicks){
        this.noOfClicks = noOfClicks;
    }

    public String getUrl(){
        return url;
    }
    public void setUrl(String url){
        this.url = url;
    }
    public String getUrlName(){
        return urlName;
    }
    public void setUrlName(String urlName){
        this.urlName = urlName;
    }
    public Long getUrlId(){
        return urlId;
    }
    public void setUrlId(Long urlId){
        this.urlId = urlId;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }
	public long getPosition() {
		return position;
	}
	public void setPosition(long position) {
		this.position = position;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}

}
