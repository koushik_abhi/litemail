package com.kenscio.litemail.workflow.domain;



public class CampaignTest {
	
	Long campaignId;
	Long stampitAccountId;
	Long stampitListId;
	String campaingName;
	String campaingMessage;
	public Long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(Long campaignId) {
		this.campaignId = campaignId;
	}
	public Long getStampitAccountId() {
		return stampitAccountId;
	}
	public void setStampitAccountId(Long stampitAccountId) {
		this.stampitAccountId = stampitAccountId;
	}
	public Long getStampitListId() {
		return stampitListId;
	}
	public void setStampitListId(Long stampitListId) {
		this.stampitListId = stampitListId;
	}
	public String getCampaingName() {
		return campaingName;
	}
	public void setCampaingName(String campaingName) {
		this.campaingName = campaingName;
	}
	public String getCampaingMessage() {
		return campaingMessage;
	}
	public void setCampaingMessage(String campaingMessage) {
		this.campaingMessage = campaingMessage;
	}
	
	
	}
