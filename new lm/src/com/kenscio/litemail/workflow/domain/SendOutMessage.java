package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;

import com.kenscio.litemail.workflow.common.BaseData;




public class SendOutMessage extends BaseData implements Serializable {

	
	private static final long serialVersionUID = -8570597249021802259L;
	
	private long campaignId =0;
	private String viewLink ="";
	private String unsubscribeLink ="";
	
	private long messageId = 0;
	private long stampitListId =0;
	private String campaignMessage = "";
	private String messageName ="";
	private String subject ="";
	private long noOfOpens=0; 
    public long getMessageId() {
		return messageId;
	}
	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}
	public long getStampitListId() {
		return stampitListId;
	}
	public void setStampitListId(long stampitListId) {
		this.stampitListId = stampitListId;
	}
	public String getCampaignMessage() {
		return campaignMessage;
	}
	public void setCampaignMessage(String campaignMessage) {
		this.campaignMessage = campaignMessage;
	}
	public String getMessageName() {
		return messageName;
	}
	public void setMessageName(String messageName) {
		this.messageName = messageName;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public long getNoOfOpens() {
		return noOfOpens;
	}
	public void setNoOfOpens(long noOfOpens) {
		this.noOfOpens = noOfOpens;
	}
	public long getNoOfUniqueOpens() {
		return noOfUniqueOpens;
	}
	public void setNoOfUniqueOpens(long noOfUniqueOpens) {
		this.noOfUniqueOpens = noOfUniqueOpens;
	}
	public long getNoOfSoftBounces() {
		return noOfSoftBounces;
	}
	public void setNoOfSoftBounces(long noOfSoftBounces) {
		this.noOfSoftBounces = noOfSoftBounces;
	}
	public long getNoOfHardBounces() {
		return noOfHardBounces;
	}
	public void setNoOfHardBounces(long noOfHardBounces) {
		this.noOfHardBounces = noOfHardBounces;
	}
	public long getNoOfSpam() {
		return noOfSpam;
	}
	public void setNoOfSpam(long noOfSpam) {
		this.noOfSpam = noOfSpam;
	}
	public String getUnsubscribeLink() {
		return unsubscribeLink;
	}
	public void setUnsubscribeLink(String unsubscribeLink) {
		this.unsubscribeLink = unsubscribeLink;
	}
	public String getViewLink() {
		return viewLink;
	}
	public void setViewLink(String viewLink) {
		this.viewLink = viewLink;
	}
	private long noOfUniqueOpens= 0;  
    private long noOfSoftBounces= 0;  
    private long noOfHardBounces =0; 
    private long noOfSpam =0;
    
    private String forward;
    private String tagName;
    private String conversion;
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getConversion() {
		return conversion;
	}
	public void setConversion(String conversion) {
		this.conversion = conversion;
	}
	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	
	

}
