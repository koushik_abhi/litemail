package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;

import com.kenscio.litemail.workflow.common.BaseData;

public class ListDataEntity extends BaseData implements Serializable{

	private static final long serialVersionUID = -3269885034416036111L;
	private Long listId = 0l;
	private String reciepientEmail="";
	private String reciepientName ="";
	private int attributeCount=0;
	
	public String getReciepientEmail() {
		return reciepientEmail;
	}
	public void setReciepientEmail(String reciepientEmail) {
		this.reciepientEmail = reciepientEmail;
	}
	public String getReciepientName() {
		return reciepientName;
	}
	public void setReciepientName(String reciepientName) {
		this.reciepientName = reciepientName;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public String getAttributeValue() {
		return attributeValue;
	}
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	public String getAttributeType() {
		return attributeType;
	}
	public void setAttributeType(String attributeType) {
		this.attributeType = attributeType;
	}
	public long getListId() {
		return listId;
	}
	public void setListId(long listId) {
		this.listId = listId;
	}
	public int getAttributeCount() {
		return attributeCount;
	}
	public void setAttributeCount(int attributeCount) {
		this.attributeCount = attributeCount;
	}
	private String attributeName ="";
	private String attributeValue ="";
	private String attributeType ="";
	

}
