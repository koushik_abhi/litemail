package com.kenscio.litemail.workflow.domain;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jun 13, 2006
 * Time: 11:16:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class CSVFile {
    private String csvFileName;
    private EmailData[] data;

    public String getCsvFileName() {
        return csvFileName;
    }

    public void setCsvFileName(String csvFileName) {
        this.csvFileName = csvFileName;
    }

    public EmailData[] getData() {
        return data;
    }

    public void setData(EmailData[] data) {
        this.data = data;
    }

}
