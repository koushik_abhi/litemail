package com.kenscio.litemail.workflow.domain;

/**
 * Created by IntelliJ IDEA.
 * User: kausar
 * Date: Feb 7, 2007
 * Time: 11:31:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class ClickThroughLinksResponseDTO {
    private boolean status;
    private String failureReason;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }
}
