package com.kenscio.litemail.workflow.domain;

import com.kenscio.litemail.workflow.common.BaseData;


public class SmtpEntity extends BaseData {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long smtpServerId = 0l;
	private String apiKey="";
	private String partner = "";
	private long accountId = 0;
	
	public long getSmtpServerId() {
		return smtpServerId;
	}
	public void setSmtpServerId(long smtpServerId) {
		this.smtpServerId = smtpServerId;
	}
	public String getSmtpServer() {
		return smtpServer;
	}
	public void setSmtpServer(String smtpServer) {
		this.smtpServer = smtpServer;
	}
	public String getSmtpUser() {
		return smtpUser;
	}
	public void setSmtpUser(String smtpUser) {
		this.smtpUser = smtpUser;
	}
	public String getSmtpPassword() {
		return smtpPassword;
	}
	public void setSmtpPassword(String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isAuthenticateSMTPFlag() {
		return authenticateSMTPFlag;
	}
	public void setAuthenticateSMTPFlag(boolean authenticateSMTPFlag) {
		this.authenticateSMTPFlag = authenticateSMTPFlag;
	}
	public String getSmtpServerName() {
		return smtpServerName;
	}
	public void setSmtpServerName(String smtpServerName) {
		this.smtpServerName = smtpServerName;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public long getAccountId() {
		return accountId;
	}
	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	private String smtpServer="";
	private String smtpUser="";
	private String smtpPassword="";
	private String description="";
	private boolean authenticateSMTPFlag = false;
	private String smtpServerName = "";
	private String domain = "";
	
   
}
