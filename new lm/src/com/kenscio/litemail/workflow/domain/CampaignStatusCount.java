package com.kenscio.litemail.workflow.domain;

import java.io.Serializable;

public class CampaignStatusCount implements Serializable
{
	private int totalopens =0;
	private int totalclicks =0;
	private int uniqueopens =0;
	private int uniqueclicks =0;
	private int hardbounce =0;
	private int softbounce =0;
	private int generalbounce =0;
	private int transientbounce =0;
	private int totalbounce =0;
	private int mailblock =0;
	public int getTotalopens() {
		return totalopens;
	}
	public void setTotalopens(int totalopens) {
		this.totalopens = totalopens;
	}
	public int getTotalclicks() {
		return totalclicks;
	}
	public void setTotalclicks(int totalclicks) {
		this.totalclicks = totalclicks;
	}
	public int getUniqueopens() {
		return uniqueopens;
	}
	public void setUniqueopens(int uniqueopens) {
		this.uniqueopens = uniqueopens;
	}
	public int getUniqueclicks() {
		return uniqueclicks;
	}
	public void setUniqueclicks(int uniqueclicks) {
		this.uniqueclicks = uniqueclicks;
	}
	public int getHardbounce() {
		return hardbounce;
	}
	public void setHardbounce(int hardbounce) {
		this.hardbounce = hardbounce;
	}
	public int getSoftbounce() {
		return softbounce;
	}
	public void setSoftbounce(int softbounce) {
		this.softbounce = softbounce;
	}
	public int getGeneralbounce() {
		return generalbounce;
	}
	public void setGeneralbounce(int generalbounce) {
		this.generalbounce = generalbounce;
	}
	public int getTransientbounce() {
		return transientbounce;
	}
	public void setTransientbounce(int transientbounce) {
		this.transientbounce = transientbounce;
	}
	public int getTotalbounce() {
		return totalbounce;
	}
	public void setTotalbounce(int totalbounce) {
		this.totalbounce = totalbounce;
	}
	public int getMailblock() {
		return mailblock;
	}
	public void setMailblock(int mailblock) {
		this.mailblock = mailblock;
	}
	public int getSpam() {
		return spam;
	}
	public void setSpam(int spam) {
		this.spam = spam;
	}
	public int getUnsubscribe() {
		return unsubscribe;
	}
	public void setUnsubscribe(int unsubscribe) {
		this.unsubscribe = unsubscribe;
	}
	private int spam =0;
	private int unsubscribe =0;
	

}
