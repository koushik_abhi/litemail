/**
 * 
 */
package com.kenscio.litemail.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Biju
 *
 */
public class OIVHtmlDocument {

	 private StringBuilder  html;
	 private OIVHtmlLink[]links;
	// public static final String LINK_PATTERN="<a\\s[^>]*\\s*href=\\s*([^>]*)>.*?</a>";
	 public static final String LINK_PATTERN="<a\\s[^>]*\\s*href=(.*?)>.*?</a>" +
	 		                                 "|<form\\s[^>]*\\s*action=(.*?)>.*?</form>"+
	 		                                 "|<area\\s[^>]*\\s*href=\\s*([^>]*)>";
	 public static final String HREF_PATTERN="<a\\s[^>]*\\s*href=|<form\\s[^>]*\\s*action=|<area\\s[^>]*\\s*href=";
	 
	 public static final Pattern linkPattern=Pattern.compile(LINK_PATTERN,Pattern.CASE_INSENSITIVE|Pattern.UNICODE_CASE|Pattern.DOTALL);
	 public static final Pattern hrefPattern=Pattern.compile(HREF_PATTERN,Pattern.CASE_INSENSITIVE|Pattern.UNICODE_CASE|Pattern.DOTALL);
	 public OIVHtmlDocument(String html){
		this.html=new StringBuilder(html==null?"":html); 
	 }
	 public OIVHtmlLink[] getOIVHtmlLinks(){
		return links;
	  }
	 public void setOIVHtmlLinks(OIVHtmlLink []links){
	   this.links=links;
	 }
	 public boolean replaceLink(OIVHtmlLink link,String newVal,int type){
		switch(type){
		 case OIVHtmlLink.URL:{
			String curLink=link.toString();
			int curLinkStart=0;
			int cnt=-1;
			int idx=link.getIdx();
			int pos=0;
			if(idx>0){	
				Matcher skipMatcher=linkPattern.matcher(html);
		        while(skipMatcher.find()){
		    	  cnt++;
		    	if(cnt==(idx-1)){
		    	  pos=skipMatcher.end(); 
				  break;
		    	}
		       }
		     }
		    //Form the new link..
		     curLinkStart=html.indexOf(curLink,pos);
		     if(curLinkStart==-1)return false;
		     int aterm=0;
		 	 int hrefStart=0;
		 	 int hrefEnd=0;
		 	 String newLink="";
		     Matcher hrefMatcher=hrefPattern.matcher(curLink);
		      if(hrefMatcher.find()){
		           aterm=curLink.indexOf('>',hrefMatcher.end());
		        if(aterm!=-1){
		           newLink=curLink.substring(hrefMatcher.start(),hrefMatcher.end());
		           char lchar=curLink.charAt(hrefMatcher.end());
		           hrefStart=hrefMatcher.end();
		           if(lchar=='\''||lchar=='\"')
					 hrefStart+=1;
				   hrefEnd=curLink.indexOf('\"',hrefStart);
				   if(hrefEnd==-1)hrefEnd=curLink.indexOf('\'',hrefStart);
				   if(hrefEnd==-1)hrefEnd=curLink.indexOf(' ',hrefStart);
				   if(hrefEnd>aterm)
					 hrefEnd=aterm-1;
		 			 newLink+="\""+newVal+"\""+curLink.substring(hrefEnd+1);
		 			 html.replace(curLinkStart,curLinkStart+curLink.length(),newLink);
		 			 link.setLink(newLink);
		 			 return true;
		 		   }
		 		}
		      }
		 	  break;
		case OIVHtmlLink.NAME:
			break;
		case OIVHtmlLink.LINK:
			break;	
		} 
		return false;
	 }
	 public String toString(){
		return html.toString(); 
	 }
}
