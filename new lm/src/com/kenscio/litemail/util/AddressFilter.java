package com.kenscio.litemail.util;

import java.util.ArrayList;
import java.util.List;

import com.kenscio.litemail.workflow.domain.Campaign;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.domain.FilteredAddressData;

public class AddressFilter {
  
	public static FilteredAddressData extractMTAtransferAddresses(Campaign campaign,EmailData []allRecipients){
	  FilteredAddressData adData=new FilteredAddressData(); 
	  String excludeFilter=campaign.getMTAExcludeFilter();
	  if(excludeFilter==null||excludeFilter.equals(""))
		 adData.setRemaining(allRecipients);
	  else{
		String domains[]=excludeFilter.split(",");
		List<EmailData>filteredList=new ArrayList<EmailData>();
		List<EmailData>remainingList=new ArrayList<EmailData>();
		boolean found;
		String address;
		String domain;
		for(int i=0;i<allRecipients.length;i++){
			found=false;
			address=allRecipients[i].getEmailId().toLowerCase();
			int aIdx=address.lastIndexOf('@');
			domain="";
			if(aIdx!=-1)
			   domain=address.substring(aIdx+1);
			for(int j=0;j<domains.length;j++){
				if(domain.equals(domains[j])){
				  found=true;
				  break;
				}
					
			}
			if(found)filteredList.add(allRecipients[i]);
			else remainingList.add(allRecipients[i]);
		}
		if(!filteredList.isEmpty())
			adData.setFiltered(filteredList.toArray(new EmailData[filteredList.size()]));
		if(!remainingList.isEmpty())
			adData.setRemaining(remainingList.toArray(new EmailData[remainingList.size()]));
	  }
	  return adData;
	}
	
	
}
