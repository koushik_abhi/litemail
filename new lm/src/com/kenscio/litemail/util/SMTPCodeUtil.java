package com.kenscio.litemail.util;

import java.util.HashMap;
import java.util.Map;

public class SMTPCodeUtil {

	public static Map<Integer, String> smtpCodes = new HashMap<Integer, String>();
	
	static
	{
		smtpCodes.put(211, "System status, or system help reply");
		smtpCodes.put(214, "Help message");
		smtpCodes.put(220, "Domain service ready.Ready to start TLS");
		smtpCodes.put(221, "Domain service closing transmission channel");
		smtpCodes.put(250, "OK, queuing for node node started.Requested mail action okay, completed.");
		smtpCodes.put(251, "OK, no messages waiting for node node.User not local, will forward to forwardpath");
		smtpCodes.put(252, "OK, pending messages for node started.Cannot VRFY user (e.g., info is not local), but will take message for this user and attempt delivery.");
		smtpCodes.put(253, "OK, messages pending messages for node node started.");
		smtpCodes.put(354, "Start mail input; end with ..");
		smtpCodes.put(355, "Octet-offset is the transaction offset.");
		
		smtpCodes.put(421, "Domain service not available, closing transmission channel.");
		smtpCodes.put(432, "A password transition is needed.");
		smtpCodes.put(450, "Requested mail action not taken: mailbox unavailable.(ex. mailbox busy)");
		
		smtpCodes.put(451, "Requested action aborted: local error in processing.Unable to process ATRN request now");
		smtpCodes.put(452, "Requested action not taken: insufficient system storage.");
		smtpCodes.put(453, "You have no mail.");
		
		smtpCodes.put(454, "TLS not available due to temporary reason.Encryption required for requested authentication mechanism.");
		smtpCodes.put(458, "Unable to queue messages for node node.");
		
		smtpCodes.put(459, "Node node not allowed: reason.");
		smtpCodes.put(500, "Command not recognized: command.Syntax error.");
		smtpCodes.put(501, "Syntax error, no parameters allowed.");
		
		smtpCodes.put(502, "Command not implemented.");
		smtpCodes.put(503, "Bad sequence of commands");
		smtpCodes.put(504, "Command parameter not implemented.");
		smtpCodes.put(521, "Machine does not accept mail.");
		smtpCodes.put(530, " Must issue a STARTTLS command first.Encryption required for requested authentication mechanism");
		
		smtpCodes.put(534, "Authentication mechanism is too weak.");
		smtpCodes.put(538, "Encryption required for requested authentication mechanism.");
		smtpCodes.put(550, "Requested action not taken: mailbox unavailable.");
		smtpCodes.put(551, "User not local; please try forwardpath.");
		smtpCodes.put(552, "Requested mail action aborted: exceeded storage allocation.");
		smtpCodes.put(553, "Requested action not taken: mailbox name not allowed.");
		
		smtpCodes.put(554, "Transaction failed.");
		
		
	}
	
	public String getSmtpMesageForCode(int code)
	{
		return smtpCodes.get(code);
	}
	
	public void addSmtpMessageCode(int code, String message)
	{
		if(!smtpCodes.containsKey(code))
			smtpCodes.put(code, message);
	}
	
		
	
}
