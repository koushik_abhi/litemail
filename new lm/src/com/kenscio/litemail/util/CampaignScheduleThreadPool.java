package com.kenscio.litemail.util;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.kenscio.litemail.workflow.service.AsyncService;


/**
 * 
 * @author venu T
 *
 */

public class CampaignScheduleThreadPool {
	
	 private int poolSize = 10;
	 
	 public static CampaignScheduleThreadPool impl = new CampaignScheduleThreadPool();
	 
	 public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public int getMaxPoolSize() {
		return maxPoolSize;
	}

	public void setMaxPoolSize(int maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}

	public int getQueueCapacity() {
		return queueCapacity;
	}

	public void setQueueCapacity(int queueCapacity) {
		this.queueCapacity = queueCapacity;
	}

	public ThreadPoolTaskExecutor getThreadPool() {
		return threadPool;
	}

	public void setThreadPool(ThreadPoolTaskExecutor threadPool) {
		this.threadPool = threadPool;
	}

	private int maxPoolSize = 25;
	 private int queueCapacity = 25;
	 
	 private ThreadPoolTaskExecutor threadPool = null;
	 
	 public static CampaignScheduleThreadPool getInstance()
	 {
		 return impl;
	 }
	
	 private CampaignScheduleThreadPool()
	 {
		 initialize();
	 }
	 
	 private void initialize()
	 {
		 threadPool = new ThreadPoolTaskExecutor();
		 threadPool.setCorePoolSize(poolSize);
		 threadPool.setMaxPoolSize(maxPoolSize);
		 threadPool.setQueueCapacity(queueCapacity);
		 threadPool.initialize();
		 
	 }
	 public void shutdown()
	 {
		 threadPool.shutdown();
	 }
	 
	 public void destroy()
	 {
		 threadPool.destroy();
	 } 
	 	 
	 public void execute(final AsyncService service)
	 {
		
		 	threadPool.execute(new Runnable(){
				 
				 public void run()
				 {
					 service.execute();
					 
				 }
				 
			 });
			
			 
	 }
	 
	
	
		
}
