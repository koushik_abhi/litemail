package com.kenscio.litemail.util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

public class PaginationHelper<E> {

    @SuppressWarnings("unchecked")
	public Page<E> fetchPage(
            final JdbcTemplate jt,
            final String sqlCountRows,
            final String sqlFetchRows,
            final Object args[],
            final int pageNo,
            final int pageSize,
            final ParameterizedRowMapper<Object> rowMapper) {
    	final Page<E> page = new Page<E>();
    	try{
        // determine how many rows are available
        final int rowCount = jt.queryForInt(sqlCountRows, args);

        // calculate the number of pages
        int pageCount = rowCount / pageSize;
        if (rowCount > pageSize * pageCount) {
            pageCount++;
        }

        // create the page object
       
        page.setPageNumber(pageNo);
        page.setPagesAvailable(pageCount);

        // fetch a single page of results
        final int startRow = (pageNo - 1) * pageSize;
        jt.query(
                sqlFetchRows,
                args,
                new ResultSetExtractor<Object>() {
                    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                        final List pageItems = page.getPageItems();
                        if(startRow > 0)
                        rs.absolute(startRow);
                        int currentRow = 0;
                        while (rs.next() && currentRow < pageSize) {
                            pageItems.add(rowMapper.mapRow(rs, currentRow));
                            currentRow++;
                        }
                        return page;
                    }
                });
       
    	}catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	return page;
    }

}