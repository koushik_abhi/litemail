package com.kenscio.litemail.util;
public class Rot13EncryptionUtil {

    static private int abyte = 0;
    static private String password = "my GOD";

    public static String doConvert(String in) {

        StringBuffer tempReturn = new StringBuffer();
        for (int i=0; i<in.length(); i++) {

            abyte = in.charAt(i);
            int cap = abyte & 32;
            abyte &= ~cap;
            abyte = ( (abyte >= 'A') && (abyte <= 'Z') ? ((abyte - 'A' + 13) % 26 + 'A') : abyte) | cap;
            tempReturn.append((char)abyte);
        }


        return tempReturn.toString();
        
    }


    public static void main (String args[]) {

        System.out.println("\nInitial password           = " + password);

        String aPassword = doConvert(password);
        System.out.println("Convert initial password   = " + aPassword);

        String bPassword = doConvert(aPassword);
        System.out.println("Perform another conversion = " + bPassword + "\n");
    }

}