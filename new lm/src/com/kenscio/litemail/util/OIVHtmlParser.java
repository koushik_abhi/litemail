/**
 * 
 */
package com.kenscio.litemail.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OIVHtmlParser {
	public OIVHtmlDocument parse(String html){
		//any validation...if required
		OIVHtmlDocument doc=new OIVHtmlDocument(html);
		populateDocument(doc);
		return doc;
	}
	private void populateDocument(OIVHtmlDocument doc){
		
		//parse for links..
	    List linkList=new ArrayList();
	    String html=doc.toString();
	    int idx=0;
	    if(html!=null){
	        Matcher linkMatcher=OIVHtmlDocument.linkPattern.matcher(html);
	    	while(linkMatcher.find())
	    		linkList.add(new OIVHtmlLink(doc,linkMatcher.group(),idx++));
	    }
	    if(!linkList.isEmpty())
		   doc.setOIVHtmlLinks(((OIVHtmlLink[])linkList.toArray(new OIVHtmlLink[linkList.size()])));
	}
}
