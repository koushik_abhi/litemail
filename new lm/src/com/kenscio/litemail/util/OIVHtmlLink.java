/**
 * 
 */
package com.kenscio.litemail.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Biju
 *
 */
public class OIVHtmlLink {

	public static final int URL=1;
	public static final int NAME=2;
	public static final int LINK=3;
  	private OIVHtmlDocument parent;
	private String link="";
	private int idx=-1;
	public OIVHtmlLink(OIVHtmlDocument parent,String link,int idx){
	  this.parent=parent;
	  this.link=(link==null)?"":link;
	  this.idx=idx;
	}
	public String getURL(){
	 int aterm=0;
 	 int hrefStart=0;
 	 int hrefEnd=0;
  	 Matcher hrefMatcher= OIVHtmlDocument.hrefPattern.matcher(link);
     if(hrefMatcher.find()){
       aterm=link.indexOf('>',hrefMatcher.end());
       if(aterm!=-1){
         char lchar=link.charAt(hrefMatcher.end());
         hrefStart=hrefMatcher.end();
         if(lchar=='\''||lchar=='\"')
			 hrefStart+=1;
		  hrefEnd=link.indexOf('\"',hrefStart);
		  if(hrefEnd==-1)hrefEnd=link.indexOf('\'',hrefStart);
		  if(hrefEnd==-1)hrefEnd=link.indexOf(' ',hrefStart);
		  if(hrefEnd>aterm)
			 hrefEnd=aterm;
		  if(hrefEnd!=-1)
			return link.substring(hrefStart,hrefEnd);
       }
     }
 	 return "";
	}
	public String getName(){
	    int aterm=0;
	    Pattern pattern=Pattern.compile("<a",Pattern.CASE_INSENSITIVE|Pattern.UNICODE_CASE);
        Matcher matcher=pattern.matcher(link);
        Pattern epattern=Pattern.compile("</a>",Pattern.CASE_INSENSITIVE|Pattern.UNICODE_CASE);
        Matcher ematcher=epattern.matcher(link);
        if(matcher.find())
  	      aterm=link.indexOf('>',matcher.end());
	      if(aterm!=-1){
   	        if(ematcher.find(aterm+1))
 		      return link.substring(aterm+1,ematcher.start());  
 		}
	   return "";
	}
	public String toString(){
	  return link;	
	}
	public int getIdx(){
	  return idx;
	}
	public boolean setURL(String newURL){
	  return parent.replaceLink(this,newURL,URL);
	}
	public boolean setName(String newName){
	   return parent.replaceLink(this,newName,NAME);
	}
	public boolean setLink(String link){
	   //parent.replaceLink(this,newLink,LINK);
	   this.link=link;
	   return true;	
	}
}
