package com.kenscio.litemail.util;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

public class DAOInitializer {
	
	private static DAOInitializer daoInit;
	private static DataSource dataSource;
	private static JdbcTemplate template=null;
	protected static ApplicationContext actx;
	
	
static
{
	actx=new ClassPathXmlApplicationContext("com/kenscio/litemail/util/SpringDataBaseConfig.xml");
}

public  DAOInitializer()
{
	
}

public static synchronized DAOInitializer getInstance()
{
	if(daoInit==null)
	{
		synchronized(DAOInitializer.class)
		{
			daoInit=new DAOInitializer();
		}
	}
	return daoInit;
	
}

@SuppressWarnings("static-access")
public  void setDataSource(DataSource dataSource) {
	this.dataSource = dataSource;
	template=new JdbcTemplate(dataSource);
}

public static DataSource getDataSource() {
	return dataSource;
}

public static JdbcTemplate getJdbcTemplate() {
	return template;
}


}
