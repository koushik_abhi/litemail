package com.kenscio.litemail.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.PreparedStatementCreator;

import com.dataservice.platform.persistence.impl.DataServiceDaoImpl;

public class CSVImportMySql extends DataServiceDaoImpl  
{

	public boolean addCustomRecordsFromExcel(final String filepath, final long listId, final List<String> headers) {
		
			final StringBuilder str = new StringBuilder("LOAD DATA local INFILE '"+filepath+"' INTO TABLE LITEMAIL_LIST_CUSTOM_DATA_TEST (");
			//str.append(" list_id, ");
			int size = headers.size();
			int count = 0;
			for(String st: headers)
			{
				count++;
				if(count < size)
				{
					str.append(st +",");
				}
				else
				{
					str.append(st);
				}
			}
			str.append(")");
			
			PreparedStatementCreator psc = new PreparedStatementCreator() 
			{
				
				@Override
				public PreparedStatement createPreparedStatement(Connection con)
						throws SQLException {
					
					PreparedStatement ps = con.prepareStatement(str.toString(), ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
					return ps;
				}
			};
			
			this.updateRecordPrepStatement(psc);
			return true;
			
		}
	
	
}
