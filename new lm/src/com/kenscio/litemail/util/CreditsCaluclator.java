package com.kenscio.litemail.util;

import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.service.AccountService;
import com.kenscio.litemail.workflow.service.MailListServiceImpl;

public class CreditsCaluclator {

	
	public Long getReqCredits(Long accountId,String listName)
	{
		Long creditsReq= 0l;
		AccountService service=new AccountService();
		AccountEntity entity=service.getUser(accountId);
		Long costPerMail=entity.getCostPerMail();
		
		MailListServiceImpl impl=new MailListServiceImpl();
		long total=impl.getListRecordCount(accountId, listName);
		if(costPerMail !=null  )
		{
			creditsReq=total*costPerMail;
			return creditsReq;
		}
		return null;
		
	}
}
