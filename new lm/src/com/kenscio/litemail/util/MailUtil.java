package com.kenscio.litemail.util;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.EmailAttachment;

import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.common.DateUtil;
import com.kenscio.litemail.workflow.domain.AccountEntity;
import com.kenscio.litemail.workflow.domain.ActionLogObject;
import com.kenscio.litemail.workflow.domain.MailDTO;
import com.kenscio.litemail.workflow.service.AccountService;
import com.kenscio.litemail.workflow.service.ActionLogServiceImpl;
import com.kenscio.litemail.workflow.service.EmailServiceUtil;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Aug 10, 2006
 * Time: 10:14:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class MailUtil {

    public static void postMail(String recipients[],String subject,String message,String from) throws MessagingException{
        Session session = getSMTPSession();
        // create a message
        Message msg = new MimeMessage(session);

        // set the from and to address
        String validFromAddress = ApplicationProperty.getProperty("support.email");
        InternetAddress addressFrom = null;
        try {
            addressFrom = new InternetAddress(validFromAddress,from);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            addressFrom = new InternetAddress(from);
        }
        msg.setFrom(addressFrom);
        InternetAddress replyTo = new InternetAddress(from);
        InternetAddress[] replyTos = { replyTo };
        msg.setReplyTo(replyTos);

        InternetAddress[] addressTo = new InternetAddress[recipients.length];
        for (int i = 0; i < recipients.length; i++) {
            addressTo[i] = new InternetAddress(recipients[i]);
        }
        msg.setRecipients(Message.RecipientType.TO,addressTo);

        // Optional : You can also set your custom headers in the Email if you Want
        msg.addHeader("MyHeaderName","myHeaderValue");

        // Setting the Subject and Content Type
        msg.setSubject(subject);
        msg.setContent(message,"text/html");
        msg.setSentDate(new Date());
        Transport.send(msg);
    }
    private static Session getSMTPSession(){
        boolean debug = false;
        Properties props = new Properties();
        props.put("mail.smtp.host",ApplicationProperty.getProperty("smtp.sever"));
        Session session = null;
        boolean isAuthRequired = new Boolean(ApplicationProperty.getProperty("relay.auth"));
        if (isAuthRequired) {
            props.put("mail.smtp.auth","true");
            final String relayUserName = ApplicationProperty.getProperty("relay.username");
            final String relayPassword = ApplicationProperty.getProperty("relay.password");
            session = Session.getDefaultInstance(props,new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication(){
                    return new PasswordAuthentication(relayUserName,relayPassword);
                }
            });
        } else {
            session = Session.getDefaultInstance(props);
        }
        session.setDebug(debug);
        return session;
    }

    public static void sendMail(Message message) throws MessagingException{
        Transport.send(message);
    }

    public static MimeMessage createMimeMessage(MailDTO mailDTO){
        System.setProperty("mail.mime.charset","iso-8859-1");
        HtmlEmail email = new HtmlEmail();
      
        email.setHostName(mailDTO.getSmtpServer());
        final String relayUserName = ApplicationProperty.getProperty("relay.username");
        final String relayPassword = ApplicationProperty.getProperty("relay.password");
        email.setAuthentication(relayUserName,relayPassword);
        try {
            email.addTo(mailDTO.getToEmail(),mailDTO.getToName());
            email.setFrom(mailDTO.getFromEmail(),mailDTO.getFromName());
            email.setSubject(mailDTO.getSubject());
            email.addHeader("Content-Transfer-Encoding","8bit");
            email.setHtmlMsg(mailDTO.getHtmlMessage());
            if(mailDTO.getAlternativeTextMessage()!=null){
                email.setTextMsg(mailDTO.getAlternativeTextMessage());
            }
            if (mailDTO.getAttachmentFileName() != null) {
                EmailAttachment attachment = new EmailAttachment();
                attachment.setPath(mailDTO.getAttachmentFilePath());
                attachment.setDisposition(EmailAttachment.ATTACHMENT);
                attachment.setName(mailDTO.getAttachmentFileName());
                email.attach(attachment);
            }
            email.buildMimeMessage();

        } catch (EmailException e) {
            e.printStackTrace(); //To change body of catch statement use File | Settings | File Templates.
        }
        return email.getMimeMessage();
    }
    public static Boolean authenticateSMTPServer(String server,String userid,String password){

        Properties props = new Properties();
        props.put("mail.smtp.host",server);
        Session session = null;
        props.put("mail.smtp.auth","true");
        final String relayUserName = userid;
        final String relayPassword = password;
        session = Session.getInstance(props,new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(relayUserName,relayPassword);
            }
        });
        Transport t = null;
        try {
            t = session.getTransport("smtp");
            t.connect(relayUserName,relayPassword);
            //  t.sendMessage(msg, msg.getAllRecipients());
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                t.close();
            } catch (MessagingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return true;
    }
    
    public static ActionLogObject addListUploadStartActionAndSendNotification(long listId, String filename, String username)
    {
         ActionLogServiceImpl alimpl = new ActionLogServiceImpl();
         ActionLogObject ac = new ActionLogObject();
	     ac.setActionName("upload");
	     ac.setStatus("started");
	     ac.setListId(listId+"");
	     ac.setActionSource(filename+"_"+System.currentTimeMillis());
	     ac.setActionFile(filename);
	     ac.setCreatedBy(username);
	     ac.setUpdatedBy(username);
	     ac.setUserId(username);
	     ac.setPartner("kenscio");
	     Calendar calendar1 = Calendar.getInstance();
	     Date myDate1 = new Date(); // May be your date too.
		 calendar1.setTime(myDate1);
	     ac.setStartTime(calendar1.getTime());
	     ac.setActionSummaryDetails("file reading is done, now processing records ...");
		 alimpl.addActionLog(ac);
		
		 ActionLogObject ac1 = alimpl.getActionLogByActionSource(ac.getActionSource());
		 
		 return ac1;
    }
    
    private static String getPartner(String username) {

    	AccountService ser = new AccountService();
    	AccountEntity ae =  ser.getUser(username);
    	if(ae != null)
    	{
    		return ae.getPartnerName();
    	}
		return username;
	}
	public static ActionLogObject addCampaignSendOuttActionAndSendNotification(String campaignName, String username)
    {
         ActionLogServiceImpl alimpl = new ActionLogServiceImpl();
         ActionLogObject ac = new ActionLogObject();
	     ac.setActionName("campaign sendout");
	     ac.setStatus("started");
	    // ac.setListId(listId);
	     ac.setActionSource(campaignName+"_"+System.currentTimeMillis());
	     ac.setActionFile(campaignName);
	     ac.setCreatedBy(username);
	     ac.setUpdatedBy(username);
	     ac.setUserId(username);
	     ac.setPartner(getPartner(username));
	     Calendar calendar1 = Calendar.getInstance();
	     Date myDate1 = new Date(); // May be your date too.
		 calendar1.setTime(myDate1);
	     ac.setStartTime(calendar1.getTime());
	     ac.setActionSummaryDetails("file reading is done, now processing records ...");
		 alimpl.addActionLog(ac);
		
		 ActionLogObject ac1 = alimpl.getActionLogByActionSource(ac.getActionSource());
		 
		 return ac1;
    }
    
    public static void updateCampaignCompletedActionLogSendEmailNotification(ActionLogObject ac1)
    {
    	     // ac1.setDiscrepancyRecords(invalid.size()+"");
    	ac1.setStatus("completed");
    	Calendar calendar1 = Calendar.getInstance();
		Date myDate11 = new Date(); // May be your date too.
		calendar1.setTime(myDate11);
	    ac1.setEndTime(calendar1.getTime());
	    ActionLogServiceImpl alimpl = new ActionLogServiceImpl();
	    alimpl.updateActionLog(ac1);
	    
	    sendEmailNotification(ac1);
    	
    	
    }
    
    public static void updateListCompletedActionLogSendEmailNotification(ActionLogObject ac1)
    {
    	     // ac1.setDiscrepancyRecords(invalid.size()+"");
	    Calendar calendar1 = Calendar.getInstance();
		Date myDate11 = new Date(); // May be your date too.
		calendar1.setTime(myDate11);
	    ac1.setEndTime(calendar1.getTime());
	    ActionLogServiceImpl alimpl = new ActionLogServiceImpl();
	    alimpl.updateActionLog(ac1);
	    
	    sendEmailNotification(ac1);
    	
    	
    }
    private static void sendEmailNotification(ActionLogObject ac1) {
		
		EmailServiceUtil util = new EmailServiceUtil();
		util.sendEmailNotification(ac1);
	}
    
}
