package com.kenscio.litemail.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;



import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.dao.AttributeDao;
import com.kenscio.litemail.workflow.domain.AttributeEntity;
import com.kenscio.litemail.workflow.service.AttributeService;


public class DataServiceUtilLiteMail {
	
	public static final String LIST_ENHANCE_FIELDS_ANALYTICS_TBL ="LIST_ENHANCE_FIELDS_ANALYTICS_TBL";
	
	public static final String DataEnhanceGroup ="DataEnhanceGroup";
	
	public static final String SHINE_MASTER_TBL ="SHINE_MASTER_TBL";
	public static final String CUSTOM_ATTRIBUTE = "custom_attribute";
	public static final String UPLOAD_TIME = "upload_time";
	public static final String ENHANCE_TIME = "enhance_time";
	public static final String ATTRIBUTE_NAME = "attribute_name";
	public static final String MASTER_VALUE = "master_value";
	public static final String OTHER_VALUE = "other_value";
	public static final String CUSTOM_FIELD = "custom_field";
	public static final String MAPPED_DBATTR_NAME = "mapped_dbattr_name";
	public static final String ACTION_SOURCE = "action_source";
	public static final String ACTION_FILE = "action_file";
	public static final String TOTAL_RECORDS = "total_records";
	public static final String DISCREPANCY_RECORDS = "discrepancy_records";
	public static final String IMPORTED_RECORDS = "imported_records";
	public static final String START_TIME = "start_time";
	public static final String END_TIME = "end_time";
	public static final String DATA_DISCREPANCY_RECORD_TBL = "DATA_DISCREPANCY_RECORD_TBL";
	public static final String DISCREPANCY_ID = "discrepancy_id";
	public static final String LIST_ATTR_ENHANCE_TBL = "LIST_ATTR_ENHANCE_TBL";

	private static String contextPath = "";

	public static final String FIELD_CREDITS = "field_credits";


	public static final String RAPLEAF_DATA_TBL = "RAPLEAF_DATA_TBL";


	public static final String CHILDREN = "children";


	public static final String HOUSE_HOLD_INCOME = "household_income";
	public static final String HIGH_NET_WORTH = "high_net_worth";
	public static final String HOME_OWNER_STATUS = "home_owner_status";
	public static final String HOME_MARKET_VALUE = "home_market_value";
	
	public static final String LENGTH_OF_RESIDENCE = "length_of_residence";
	public static final String BOOKS = "books";
	public static final String BLOGGING = "blogging";
	public static final String BUSINESS = "business";
	public static final String HEALTH_WEALTH = "health_wealth";
	public static final String NEWS = "news";
	
	public static final String AUTOMATIVE = "automative";
	public static final String DISCOUNT_SHOPPER = "discount_shopper";
	public static final String POWER_SHOPPER = "power_shopper";
	public static final String HIGHEND_BRAND_BUYER = "highend_brand_buyer";
	public static final String TRAVEL = "travel";
	public static final String SPROTS = "sports";
	
	public static final String HOME_IMPROVEMENT = "home_improvement";
	public static final String LUXURYGOODS_JEWELS = "luxurygoods_jewels";
	public static final String EDUCATION = "education";


	public static final String JOB_NAME = "job_name";
	public static final String ACTION_DATA_PROVIDER = "action_data_provider";
	public static final String SCHEDULE_START_DATE = "schedule_start_date";
	public static final String SCHEDULE_END_DATE = "schedule_end_date";
	public static final String REPEAT_INTERVAL = "repeat_interval";


	public static final String SCHEDULE_JOBS_TBL = "SCHEDULE_JOBS_TBL";


	public static final String REMOTE_CONNECTION_TBL ="REMOTE_CONNECTION_TBL";
	public static final String FTP_HOST ="ftp_host";
	public static final String FTP_USER_NAME ="ftp_user_name";
	
	public static final String FTP_PASSWORD ="ftp_password";
	public static final String FTP_FILE_LOCATION ="ftp_file_location";
	public static final String DATAPROVIDER_DBURL ="dataprovider_dbUrl";
	public static final String DATAPROVIDER_DBUSERNAME ="dataprovider_dbusername";
	public static final String DATAPROVIDER_DBPASSWORD ="dataprovider_dbpassword";
	public static final String DATAPROVIDER_DBSERVICE ="dataprovider_dbservice";


	public static final String USER_ROLE_TBL = "USER_ROLE_TBL";


	public static final String ERROR = "error";


	public static final String NEXT_FIRETIME = "next_firetime";


	public static final String SUCCESS_NUMBER = "success_number";
	public static final String FAILED_NUMBER = "failed_number";

	public static final String LIST_ENHANCE_SPECIAL_FIELDS = "LIST_ENHANCE_SPECIAL_FIELDS";

	public static final String STD_FIELD_NAME = "STD_FIELD_NAME";

	public static final String STD_FIELD_VALUE = "STD_FIELD_VALUE";

	public static final String FullName = "FullName";

	public static final String Experience = "Experience";

	public static final String LITEMAIL_LIST_STD_DATA = "LITEMAIL_LIST_STD_DATA";

	public static final String LITEMAIL_LIST_CUSTOM_DATA = "LITEMAIL_LIST_CUSTOM_DATA";

	public static final String LITEMAIL_LISTATTRIBUTE_MAP = "LITEMAIL_LISTATTRIBUTE_MAP";
	
	public static String RECORD_ID = "id";
	public static String FAX_NUMBER = "fax_number";
	public static String EMAIL_ID = "email_id";
	public static String ATTR_ID = "attr_id";
	public static String NAME = "name";
	public static String FIRST_NAME ="first_name";
	public static String FIRST_LOGIN ="first_login";
	public static String LAST_NAME="last_name";
	public static String PHONE_NUMBER = "phone_number";
	public static String MOBILE_NUMBER1 = "mobile_number1";
	public static String MOBILE_NUMBER2 = "mobile_number2";
	public static String CITY = "city";
	public static String ADDRESS = "address";
	public static String ADDRESS1 = "address1";
	public static String ADDRESS2 = "address2";
	public static String REGION = "region";
	public static String COUNTRY = "country";
	public static String GENDER = "gender";
	public static String STATUS = "status";
	public static String STATE = "state";
	public static String POSTAL_CODE = "postal_code";
	public static String DATE_OF_BIRTH = "date_of_birth";
	public static String ANNIVERSARY_DATE = "anniversary_date";
	public static String TITLE = "title";
	public static String RESIDENCE_TYPE = "residence_type";
	public static String NO_OF_KIDS = "no_of_kids";
	public static String MARITAL_STATUS = "marital_status";
	public static String VEHICLE_TYPE = "vehicle_type";
	public static String NATURE_OF_WORK = "nature_of_work";
	public static String JOB_TYPE= "job_type";
	public static String PERMANENT_ADDRESS = "permanent_address";
	public static String SALARY = "salary";
	public static String MONTH_OF_BIRTH = "month_of_birth";
	public static String DESCRIPTION ="description";
	public static String COMMENT = "comment";
	public static String CREATED_BY ="created_by";
	public static String UPDATED_BY = "updated_by";
	public static String CREATED_TIME = "created_time";
	public static String CREATED_TIME_STRING = "created_time";
	public static String UPDATED_TIME_STRING = "updated_time";
	public static String UPDATED_TIME = "updated_time";
	public static String OCCUPATION = "occupation";
	public static String AGE  = "age";
	public static String VEHICLE_DETAILS = "vehicle_details";
	public static String SEARCH_SOURCE = "search_source";
	public static String MOTHER_TONGUE = "mother_tongue";
	public static String FIELD_STDUINAME="field_stduiname";
	//public static String CUSTOM_ATTRIBUTE = "custom_attribute";
	
	
	public static String PARTNER_ID = "partner_id";
	public static String PARTNER_NAME = "partner_name";
	public static String TYPE = "type";
	public static String BUSINESS_URL = "business_url";
	public static String BUSINESS_NATURE = "business_nature";
	public static String CUSTOMER_TYPE = "customer_type";
	
	public static String USER_ID = "user_id";
	public static String USER_NAME = "user_name";
	public static String USER_TYPE = "user_type";
	public static String ROLE = "role";
	public static String ROLE_NAME = "role_name";
	public static String PASSWORD = "password";
	public static String MOBILE_NUMBER = "mobile_number";
	
	public static String SEARCH_LOG = "searchlog";
	
	public static String LIST_ID = "list_id";
	
	public static String STD_ATTRIBUTE_NAME = "std_attribute_name";
	public static String STD_ATTRIBUTE_UINAME = "std_attribute_uiname";
	public static String STD_ATTRIBUTE_TYPE = "std_attribute_type";
	public static String STD_ATTRIBUTE_INIT_VALUE = "std_attribute_init_value";
	public static String ACTUAL_ATTR_NAME = "actual_attr_name";
	public static String ACTUAL_ATTR_TYPE = "actual_attr_type";
	public static String ACTUAL_ATTR_INIT_VALUE = "actual_attr_init_value";
	public static String ATTRIBUTE_POSITION = "attribute_position";
	
	public static String TOTAL_EXPERIENCE = "total_experience";
	public static String EXPERIENCE_MONTHS = "experience_months";
	public static String INDUSTRY_SUBFUNCTION = "industry_subfunction";
	public static String COMPANY_NAME = "company_name";
	public static String EDUCATION_INSTITUTION = "education_institution";
	public static String EDUCATION_QUALIFICATION = "education_qualification";
	public static String EDUCATION_SPECIALIZATION = "education_specialization";
	public static String EDUCATION_STREAM = "education_stream";
	
	
	public static String FIELD_NAME = "field_name";
	public static String FIELD_COUNT = "field_count";
	public static String ENHANCE_TYPE = "enhance_type";
	public static String SEARCH_RUN_TIME ="search_run_time";
	public static String FIELDS_FETCHED ="fields_fetched";
	
	public static String JOB_ID = "job_id";
	public static String SCHEDULE_TIME = "schedule_time";
	public static String SCHEDULE_TYPE = "schedule_type";
	
	public static String EMAIL_CONFIG_ID = "email_config_id";
	public static String HOST = "host";
	public static String PORT = "port";
	public static String FROM_ADDRESS = "from_address";
	public static String TO_LIST = "to_list";
	public static String CC_LIST = "cc_list";
	public static String BCC_LIST = "bcc_list";
	public static String SUBJECT = "subject";
	public static String BODYMESSAGE = "bodymessage";
	public static String ACTION_NAME ="action_name";
	public static String ACTION_ID ="action_id";
	public static String ACTION_SUMMARY_DETAILS ="action_summary_details";
	public static String ACTION_LOG_FILE = "action_log_file";
	public static String DISCREPANCY_FILE = "discrepancy_file";
	
	//shine fields
	public static String FirstName = "FirstName";
	public static String Email = "Email";
	public static String email = "email";
	public static String UserId ="UserId";
	public static String LastName = "LastName";
	public static String CellPhone = "Cellphone";
	public static String Gender= "Gender";
	public static String City= "City";
	public static String State= "State";
	public static String VendorId= "VendorId";
	public static String EndVendorId = "EndVendorId";
	public static String Status = "Status";
	public static String RegistrationDate = "RegistrationDate";
	public static String CompletionDate = "CompletionDate";
	public static String LastLoginDate = "LastLoginDate";
	public static String LastUpdatedDate = "LastUpdatedDate";
	public static String LatestAppliedJobDate = "LatestAppliedJobDate";
	public static String TotalExperience ="TotalExperience";
	public static String Experience_Month ="Experience_Month";
	public static String Salary ="Salary";
	public static String JobTitle1 = "JobTitle1";
	public static String SubFunction1 = "SubFunction1";
	
	public static String Function1 = "Function1";
	public static String Industry1 = "Industry1";
	public static String Experience1 = "Experience1";
	public static String CompanyName1 = "CompanyName1";
	public static String Institute1 = "Institute1";
	public static String EducationQualification1 ="EducationQualification1";
	public static String Specialization1 ="Specialization1";
	public static String Stream1="Stream1";
	public static String IsEmailVerified = "IsEmailVerified";
	public static String IsCellPhoneVerified = "IsCellPhoneVerified";
	
	public static String NotificationFrequency ="NotificationFrequency";
	public static String IsReceiveSMS ="IsReceiveSMS";
	
	
	//tables
	public static String MASTER_DATA_TBL = "MASTER_DATA_TBL";
	public static String MASTER_DATA_TBL_HIST = "MASTER_DATA_TBL_HIST";
	public static String PARTNER_DETAILS_TBL = "PARTNER_DETAILS_TBL";
	public static String USER_TBL = "USER_TBL";
	public static String LIST_CONFIG_TBL ="LIST_CONFIG_TBL";
	public static String LIST_ATTR_MAPPING_TBL = "LIST_ATTR_MAPPING_TBL";
	public static String USER_ATTRIBUTES_TBL = "USER_ATTRIBUTES_TBL";
	public static String EMAIL_CONFIG_TBL = "EMAIL_CONFIG_TBL";
	public static String ACTION_LOG_TBL = "ACTION_LOG_TBL";
	public static String STD_ATTRIBUTES_TBL = "STD_ATTRIBUTES_TBL";
	public static String SEARCH_RUN_CONFIG_TBL = "SEARCH_RUN_CONFIG_TBL";
	public static String SEARCH_JOBS_TBL= "SEARCH_JOBS_TBL";
	public static String SEARCH_RESULT_TBL ="SEARCH_RESULT_TBL";
	public static String DATA_DISCREPANCY_TBL = "DATA_DISCREPANCY_TBL";
	public static String LIST_ATTR_BASE_TBL = "LIST_ATTR_BASE_TBL";
	public static String LIST_ATTR_EXTN_TBL = "LIST_ATTR_EXTN_TBL";
	public static String SEARCH_RUN_ATTR = "SEARCH_RUN_ATTR";
	
	public static String MASTER_DATA_CATEGORY_TBL = "MASTER_DATA_CATEGORY_TBL";
	public static String MASTER_DATA_CATEGORY_FIELDMAP_TBL = "MASTER_DATA_CATEGORY_FIELDMAP_TBL";
	public static String MASTER_DATA_CATEGORY_FIELDS_TBL = "MASTER_DATA_CATEGORY_FIELDS_TBL";
	
	
	public static String CATEGORY_NAME = "category_name";
	public static String FIELD_TYPE = "field_type";
	public static String DATABASE_FIELD ="database_field";
	public static String FIELD ="field";
	
	public static String LIST_ENHANCE_ANALYTICS_TBL ="LIST_ENHANCE_ANALYTICS_TBL";
	public static String TRANS_ID ="trans_id";
	public static String SEARCH_ID ="search_id";
	public static String LIST_NAME ="list_name";
	public static String TOTAL_NOOFRECORDS ="total_noOfRecords";
	public static String TOTAL_NOOFFIELDS ="total_noOfFields";
	public static String ACTUAL_NOOFRECORDS ="actual_noOfRecords";
	public static String ACTUAL_NOOFFIELDS ="actual_noOfFields";
	public static String TRANS_TYPE ="trans_type";
	public static String FIELDS_ENHANCED ="fields_enhanced";
	public static String CREDITS ="credits";
	public static String USER_CREDITS_TBL ="USER_CREDITS_TBL";
	public static String DEBITS ="debits";
	public static String BALANCE ="balance";
	public static String PARTNER_FIELD_CREDITS ="PARTNER_FIELD_CREDITS";
	
	
	public static String SEARCH_RUN_ID="search_run_id";
	public static Map<String, String> stdfieldToUIName = new HashMap<String, String>();


	public static String lookup_city ="lookup_city";
	public static String lookup_annualsalary ="lookup_annualsalary";
	public static String lookup_experience ="lookup_experience";
	public static String lookup_experience_months ="lookup_experience_months";
	public static String lookup_functionalarea ="lookup_functionalarea";
	public static String lookup_industry ="lookup_industry";


	public static String lookup_subfunctionalarea ="lookup_subfunctionalarea";

	public static String list_id ="list_id";

	public static String attribute_name ="attribute_name";

	public static String db_attribute_name ="db_attribute_name";
	
	
	public static Map<String, String> getStdfieldToUIName() {
		return stdfieldToUIName;
	}

	public static void setStdfieldToUIName(Map<String, String> stdfieldToUIName) {
		DataServiceUtilLiteMail.stdfieldToUIName = stdfieldToUIName;
	}

	static
	{
		AttributeService imp = new AttributeService();
		//stdfieldToUIName = imp.getAllStdAttributesUIMap();
	}
	
	
	public static java.util.Date convertToUtilDate1(java.sql.Date sqlDate) {
		
		if(sqlDate == null) return null;
		java.util.Date utilDate = new java.util.Date(sqlDate.getTime());
		return utilDate;
		
	}
	public static java.util.Date convertToUtilDate(Timestamp sqlDate) {
		
		if(sqlDate == null) return null;
	
		java.util.Date utilDate = new java.util.Date(sqlDate.getTime());
		return utilDate;
		
	}
	

	public static String convertToUtilDateString(java.sql.Date sqlDate) {
		
		if(sqlDate == null) return null;
		
		java.util.Date utilDate = new java.util.Date(sqlDate.getTime());
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String datef = df.format(utilDate);
		return datef;
		
	}
	
	public static String convertToUtilDateString(Timestamp sqlDate) {
		
		if(sqlDate == null) return null;
		
		java.util.Date utilDate = new java.util.Date(sqlDate.getTime());
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String datef = df.format(utilDate);
		return datef;
		
	}
	
	public static java.sql.Date convertToSqlDate(java.util.Date utilDate) {
		
		if(utilDate == null) return null;
		 
		java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
	    return sqlDate;
	}
	
	public static String convertToDateString(java.util.Date utilDate) {
		
		if(utilDate == null) return null;
		final DateFormat df = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
		String datef = df.format(utilDate);
	    return datef;
	}

	public static String convertToString(List<String> alternativeEmailIds) {
		
		StringBuilder str  = new StringBuilder();
		for(String sr: alternativeEmailIds)
		{
			str.append(sr);
		}
		return str.toString();
	}

	public static void addCustomAttributes(PreparedStatement ps, Map<String, String> customAttributes,
			 int startIndex, int noOfAttr) throws SQLException{
		
		for(int i = 1; i<=noOfAttr; i++)
		{
			ps.setString(startIndex, (String)customAttributes.get("custom_field"+i));
			startIndex++;
		}
		
	}
	
	public static List<String> getAllAvailAtributes(){
		
		List<String> avail = new ArrayList<String>();
		for(int i = 1; i<=40; i++)
		{
			avail.add(CUSTOM_ATTRIBUTE+i);
		}
		return avail;
	}
	
	public static String createUpdateSQLQuery(long listId, Map<String, String> updatemap, String tableName) {
		final StringBuilder query = new StringBuilder();
		
		Set<String> heads = updatemap.keySet();
		int size = heads.size();
		int count = 1;
		query.append( "update "+ tableName +  " set "  );
		for(String st : heads)
		{
			if(count < size)
			{
				query.append(st+"=?,");
			}
			else{
				query.append(st+"=?");
			}
			count++;
		}
		query.append(" where "+DataServiceUtilLiteMail.email+"=? and "+DataServiceUtilLiteMail.list_id+"=?");
		
		return query.toString();
		
	}
	
	public static String createUpdateShineSQLQuery(Map<String, String> updatemap, String tableName) {
		final StringBuilder query = new StringBuilder();
		
		Set<String> heads = updatemap.keySet();
		int size = heads.size();
		int count = 1;
		query.append( "update "+ tableName +  " set "  );
		for(String st : heads)
		{
			if(count < size)
			{
				query.append(st+"=?,");
			}
			else{
				query.append(st+"=?");
			}
			count++;
		}
		query.append(" where "+DataServiceUtilLiteMail.Email+"=?");
		
		return query.toString();
		
	}

	public static String createUpdateSQLQuery(long listId, List<String> keys, String tableName) {
		final StringBuilder query = new StringBuilder();
		
		
		int size = keys.size();
		int count = 1;
		query.append( "update "+ tableName +  " set "  );
		for(String st : keys)
		{
			if(count < size)
			{
				query.append(st+"=?,");
			}
			else{
				query.append(st+"=?");
			}
			count++;
		}
		query.append(" where "+DataServiceUtilLiteMail.email+"=? and "+DataServiceUtilLiteMail.list_id+"=?");
		
		return query.toString();
		
	}
	public static String createSQLQuery(long listId, List<String> colNames, String tableName) {
		final StringBuilder query = new StringBuilder();
		int size = colNames.size();
		int count = 1;
		query.append( "insert into "+ tableName +  " ( "  );
		query.append("list_id ,");
		for(String st : colNames)
		{
			if(count < size)
			{
				query.append(st+",");
			}
			else{
				query.append(st);
			}
			count++;
		}
		query.append(") ");
		
		query.append(" values ( ");
		query.append("? ,");
		count =1;
		for(String st : colNames)
		{
			if(count < size)
			{
				query.append("?, ");
			}
			else{
				query.append("?");
			}
			count++;
		}
		query.append(")");
		
		return query.toString();
	}
	
	public static String createSelectQuery(long listId, String email, List<String> colNames, String tableName) {
		final StringBuilder query = new StringBuilder();
		int size = colNames.size();
		int count = 1;
		query.append( "select ");
		for(String st : colNames)
		{
			if(count < size)
			{
				query.append(st+",");
				System.out.print("colname"+st);
			}
			else{
				query.append(st);
			}
			count++;
		}
		query.append(" from "+ tableName +  " where "  );
		query.append("list_id = "+ listId + " and email = '"+email+"'");
				
		return query.toString();
	}
	
	public static String createSelectQueryList(long listId, List<String> colNames, String tableName) {
		final StringBuilder query = new StringBuilder();
		int size = colNames.size();
		int count = 1;
		query.append( "select ");
		for(String st : colNames)
		{
			if(count < size)
			{
				query.append(st+",");
			}
			else{
				query.append(st);
			}
			count++;
		}
		query.append(" from "+ tableName +  " where "  );
		query.append("list_id = "+ listId );
				
		return query.toString();
	}
	public static String creatSelectUpdateDataQuery(long listId,List<String> colNames,String tableName,String email){
		final StringBuilder queryBuilder=new StringBuilder();
		int size=colNames.size();
		int count=1;
		queryBuilder.append("select");
		for (String st : colNames) {
			if(count<size){
				queryBuilder.append(st+",");
			}
			else{
				queryBuilder.append(st);
			}
			count++;
		}
		queryBuilder.append("from"+tableName+"where");
		queryBuilder.append("list_id="+listId+"and email="+email);
		
		return queryBuilder.toString();
	}
	public static Map<String, String> updateMasterMap(String csval, String emailId, Map<String, String> updatemap)
	{
		
		StringTokenizer str = new StringTokenizer(csval, " ");
		//StringTokenizer strstr = new StringTokenizer(csval, ".");
				
		int tokens = str.countTokens();
		
		if(str.hasMoreTokens())
		{
			
			String st1 = str.nextToken();
			String st = st1.trim();
			//System.out.println("st1 " +st1 +" st " +st + "cs " +csval);
			if(st.equalsIgnoreCase("Mr") || st.equalsIgnoreCase("MR") || st.equalsIgnoreCase("mr") || st.equalsIgnoreCase("Mr.") || st.equalsIgnoreCase("MR.") || st.equalsIgnoreCase("mr."))
			{
				updatemap.put(DataServiceUtilLiteMail.TITLE, "Mr");
				updatemap.put(DataServiceUtilLiteMail.GENDER, "Male");
				
				
			}else if(st.equalsIgnoreCase("Ms") || st.equalsIgnoreCase("MS") || st.equalsIgnoreCase("ms") ||st.equalsIgnoreCase("Ms.") || st.equalsIgnoreCase("MS.") || st.equalsIgnoreCase("ms."))
			{
				updatemap.put(DataServiceUtilLiteMail.TITLE, "Ms");
				updatemap.put(DataServiceUtilLiteMail.GENDER, "Female");
						
				
				
			}else if(st.equalsIgnoreCase("Mrs") || st.equalsIgnoreCase("MRS") || st.equalsIgnoreCase("mrs") || st.equalsIgnoreCase("Mrs.") || st.equalsIgnoreCase("MRS.") || st.equalsIgnoreCase("mrs."))
			{
				
				updatemap.put(DataServiceUtilLiteMail.TITLE, "Mrs");
				updatemap.put(DataServiceUtilLiteMail.GENDER, "Female");
				
			
			}
			else if(st.equalsIgnoreCase("Dr") ||st.equalsIgnoreCase("Dr."))
			{
				updatemap.put(DataServiceUtilLiteMail.TITLE, "Dr");
							
			}else if(st.equalsIgnoreCase("Er") ||st.equalsIgnoreCase("Er."))
			{
				updatemap.put(DataServiceUtilLiteMail.TITLE, "Er");
							
			}
			if(str.hasMoreTokens())
			{
				
				String fn = str.nextToken();
				updatemap.put(DataServiceUtilLiteMail.FIRST_NAME, fn);
								
			}
			if(str.hasMoreTokens())
			{
				String ln = str.nextToken();
				updatemap.put(DataServiceUtilLiteMail.LAST_NAME, ln);
				//System.out.println("cval " + csval + "updatemap " + updatemap);
				
			}
			
			
		}else
		{
			
			return updatemap;
			
		}
	
		return updatemap;
	}
	
	

	public static void setContextPath(String contextPath_) 
	{
		contextPath = contextPath_;
		
	}
	public static String getContextPath() 
	{
		return contextPath;
		
	}
	
	public static boolean validateMailListHeaders(List<String> userHeaders, List<String> dbHeaders)
	{	
				
        List<String> usrHeadrs = new ArrayList<String>();
        try
        {
        	 usrHeadrs.addAll(userHeaders);	         
	         if(usrHeadrs != null && usrHeadrs.size() <=0 )
	         {
	        	 return false;
	         }
	         if(usrHeadrs != null && usrHeadrs.size() ==1 )
	         {
	        	 String str = usrHeadrs.get(0);
	        	 if(str != null && str.toLowerCase().contains("mail"))
	        	 {
	        		 return true;
	        	 }
	         }
	         usrHeadrs.removeAll(dbHeaders);
	         if(usrHeadrs.size() > 0)
	        	 return false;
	         else
	        	 return true;
	         
	         
        } catch (Exception e) {
			
        	System.out.println("error in compare headers with attributes "+ e.getMessage());
        	e.printStackTrace();
		}
		return true;
      
		
	}

	public static String createSelectQueryList(List<String> emailList,
			String tableName) {
		final StringBuilder query = new StringBuilder();
		int size = emailList.size();
		int count = 1;
		query.append( "select email from "+tableName+" where email in (");
		for(String st : emailList)
		{
			if(count < size)
			{
				query.append(" ?, ");
			}
			else{
				query.append(" ? ");
			}
			count++;
		}
		query.append( ")");
		return query.toString();
	}
	

}
