package com.kenscio.litemail.util;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Jisso
 * Date: Jan 8, 2007
 * Time: 3:28:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class DidYouKnowPicker {
   private static String FILE_NAME = "DidYouKnow";

    public static String pick(){
        ResourceBundle bundle = PropertyResourceBundle.getBundle(FILE_NAME);
        Set envBundleKeySet = getKeySet(bundle);
        int totalSize = envBundleKeySet.size();
        Random random = new Random();
        int randomNumber = random.nextInt(totalSize);
        String didYouKnowString = bundle.getString(""+randomNumber);
        return didYouKnowString;
    }

        private static Set getKeySet(ResourceBundle bundle) {
        Set ret = new HashSet();
        Enumeration enume = bundle.getKeys();
        while (enume.hasMoreElements())
        {
            ret.add(enume.nextElement());
        }
        return ret;
    }
}
