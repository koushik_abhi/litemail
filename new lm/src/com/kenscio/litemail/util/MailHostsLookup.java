package com.kenscio.litemail.util;
// Print out a sorted list of mail exchange servers for a network domain name
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.InitialDirContext;
import javax.naming.NamingException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class MailHostsLookup 
{
    public static void main(String args[]) 
    {
        // explain what program does and how to use it 
        //if (args.length != 1)
        {
         //   System.err.println("Print out a sorted list of mail exchange servers ");
        //    System.err.println("    for a network domain name");
        //    System.err.println("USAGE: java MailHostsLookup domainName");
        //    System.exit(-1);
        } 
        try
        {   // print the sorted mail exhchange servers
        	MailHostsLookup test = new MailHostsLookup();
            for (String mailHost: lookupMailHosts("gmail.com"))
            {
                System.out.println("mail" + test.validEmail(mailHost, "testty@gmail.com"));            
            }
        }
        catch (NamingException e)
        {
            System.err.println("ERROR: No DNS record for '" + args[0] + "'");
            System.exit(-2);
        }
     }
    
    private static int hear( BufferedReader in ) throws IOException {
        String line = null;
        int res = 0;

        while ( (line = in.readLine()) != null ) {
            String pfx = line.substring( 0, 3 );
            try {
               res = Integer.parseInt( pfx );
            }
            catch (Exception ex) {
               res = -1;
            }
            if ( line.charAt( 3 ) != '-' ) break;
        }

        return res;
        }

      private static void say( BufferedWriter wr, String text )
         throws IOException {
        wr.write( text + "\r\n" );
        wr.flush();

        return;
        }
    
    public boolean validEmail(String mailHost , String address)
    {
    	
        boolean valid = false;
      
        try {
            int res;
            //
            Socket skt = new Socket( mailHost, 25 );
            BufferedReader rdr = new BufferedReader
               ( new InputStreamReader( skt.getInputStream() ) );
            BufferedWriter wtr = new BufferedWriter
               ( new OutputStreamWriter( skt.getOutputStream() ) );

            res = hear( rdr );
            if ( res != 220 ) throw new Exception( "Invalid header" );
            say( wtr, "EHLO rupeemail.com" );

            res = hear( rdr );
            
            if ( res != 250 ) 
            {
           	 say( wtr, "HELO rupeemail.com" );

                res = hear( rdr );
               
            }
            
            if(res != 250) throw new Exception( "Not ESMTP" );

            // validate the sender address              
            say( wtr, "MAIL FROM: <support@rupeemail.com>" );
            res = hear( rdr );
            if ( res != 250 ) throw new Exception( "Sender rejected" );

            say( wtr, "RCPT TO: <" + address + ">" );
            res = hear( rdr );

            // be polite
            say( wtr, "RSET" ); 
            int rset = hear( rdr );
            System.out.println("rset 1" + rset);
            say( wtr, "QUIT" ); 
            rset = hear( rdr );
            System.out.println("rset 2" + rset);
            if ( res != 250 )
               throw new Exception( "Address is not valid!" );

            valid = true;
            rdr.close();
            wtr.close();
            skt.close();
        }
        catch (Exception ex) {
          // Do nothing but try next host
          ex.printStackTrace();
        }
        finally {
          if ( valid ) return true;
        }
        
    	return valid;
    }

    // returns a String array of mail exchange servers (mail hosts) 
    //     sorted from most preferred to least preferred
    static String[] lookupMailHosts(String domainName) throws NamingException
    {
        // see: RFC 974 - Mail routing and the domain system
        // see: RFC 1034 - Domain names - concepts and facilities
        // see: http://java.sun.com/j2se/1.5.0/docs/guide/jndi/jndi-dns.html
        //    - DNS Service Provider for the Java Naming Directory Interface (JNDI)

        // get the default initial Directory Context
        InitialDirContext iDirC = new InitialDirContext();
        // get the MX records from the default DNS directory service provider
        //    NamingException thrown if no DNS record found for domainName
        Attributes attributes = iDirC.getAttributes("dns:/" + domainName, new String[] {"MX"});
        // attributeMX is an attribute ('list') of the Mail Exchange(MX) Resource Records(RR)
        Attribute attributeMX = attributes.get("MX");

        // if there are no MX RRs then default to domainName (see: RFC 974)
        if (attributeMX == null)
        {
            return (new String[] {domainName});
        }

        // split MX RRs into Preference Values(pvhn[0]) and Host Names(pvhn[1])
        String[][] pvhn = new String[attributeMX.size()][2];
        for (int i = 0; i < attributeMX.size(); i++)
        {
            pvhn[i] = ("" + attributeMX.get(i)).split("\\s+");
        }

        // sort the MX RRs by RR value (lower is preferred)
        Arrays.sort(pvhn, new Comparator<String[]>()
            {
                public int compare(String[] o1, String[] o2)
                {
                    return (Integer.parseInt(o1[0]) - Integer.parseInt(o2[0]));
                }
            });

        // put sorted host names in an array, get rid of any trailing '.' 
        String[] sortedHostNames = new String[pvhn.length];
        for (int i = 0; i < pvhn.length; i++)
        {
            sortedHostNames[i] = pvhn[i][1].endsWith(".") ? 
                pvhn[i][1].substring(0, pvhn[i][1].length() - 1) : pvhn[i][1];
        }
        return sortedHostNames;
    }
}