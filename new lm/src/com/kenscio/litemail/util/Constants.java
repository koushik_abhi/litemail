package com.kenscio.litemail.util;

public class Constants {
  public static final String STAMP_GENERATION_PENDING="PENDING";
  public static final String STAMP_GENERATION_COMPLETE="COMPLETE";
  public static final String STAMP_GENERATION_PROCESSING="PROCESSING";
  public static final String STAMP_GENERATION_ERROR="ERROR";
  public static final String STAMP_GENERATION_CANCELLED="CANCELLED";
	
}
