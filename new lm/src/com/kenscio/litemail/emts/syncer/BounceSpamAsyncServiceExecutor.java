package com.kenscio.litemail.emts.syncer;



import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;





public class BounceSpamAsyncServiceExecutor extends Thread{

	private static final ThreadPoolExecutor pool = new ThreadPoolExecutor(5, 50, 10, TimeUnit.SECONDS, new LinkedBlockingQueue());
	private static final long bounceWaitTime = 10*60*1000; 
	public BounceSpamAsyncServiceExecutor()
	{
		
	}
	
	public void callAsyncServiceForSpam()
	{
		BounceSpamThread thread = new BounceSpamThread("spam");
		pool.execute(thread);
		            
		
	}
	
	private void callAsyncServiceForBounce()
	{
		BounceSpamThread thread = new BounceSpamThread("bounce");
		pool.execute(thread);		            
		
	}
	private void callAsyncServiceForUnsubscribe()
	{
		BounceSpamThread thread = new BounceSpamThread("unsub");
		pool.execute(thread);		            
		
	}
	
	public void run() {
		
		//uploadMissingOpenData();
		while(true)
		{
			
			callAsyncServiceForBounce();
			callAsyncServiceForSpam();
			callAsyncServiceForUnsubscribe();
			try {
				Thread.currentThread().sleep(bounceWaitTime);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			
		}
		
	}


	

}
