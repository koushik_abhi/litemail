package com.kenscio.litemail.emts.syncer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.codehaus.jackson.map.ObjectMapper;


import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.common.ApplicationProperty;
import com.kenscio.litemail.workflow.dao.CampaignDao;
import com.kenscio.litemail.workflow.domain.EmtsResponseBounce;
import com.kenscio.litemail.workflow.domain.EmtsResponseSpam;
import com.kenscio.litemail.workflow.domain.MessageBounce;
import com.kenscio.litemail.workflow.domain.MessageSpam;


public class BounceSpamThread implements  Runnable
{
	private long campaignId = 0;
	private String domainName= "";
	private String clientId="";
	private String apiKey = "";
	private String smtpProvider = "";
	private String catType = "";
	
	public BounceSpamThread(String type )
	{
		this.catType = type;
		
	}
	
	public BounceSpamThread(long campaignId, String domainName, String clientId, String apiKey, String smtpProvider )
	{
		this.setCampaignId(campaignId) ;
		this.setDomainName(domainName) ;
		this.setClientId(clientId);
		this.setApiKey(apiKey);
		this.setSmtpProvider(smtpProvider);
		
	}
	

	public BounceSpamThread(String domainName, String apiKey,String smtpProvider) {
		this.setDomainName(domainName) ;
		this.setApiKey(apiKey);
		this.setSmtpProvider(smtpProvider);
	}

	public void run() {
		
		try
		{
			if(this.catType.equalsIgnoreCase("bounce"))
			{
				
				callAsyncServiceForBounce();
				
			}else if(this.catType.equalsIgnoreCase("spam"))
			{
				
				callAsyncServiceForSpam();
				
			}else if(this.catType.equalsIgnoreCase("unsub"))
			{
				
				callAsyncServiceForUnsub();
				
			}
			
		}catch(Exception e)
		{
			System.out.println("failed to get emts data "+ e.getMessage());
			e.printStackTrace();
		}
			
				
	}	
	
	
	private void callAsyncServiceForUnsub() {
		String url = "ViewMailUnsubscribeApi.htm";
		
	}

	private void callAsyncServiceForSpam() {
		String url = "ViewMailAllSpamsApiPagination.htm";
		boolean runflag = true;
		while(runflag)
		{
			long rec = getSpamMaxId();
			int recId = (int)rec;
			EmtsResponseSpam spams = getEmtsResponseSpam(url, recId, 1000);
			if(spams == null || spams.getItems().size() <= 0)
			{
				runflag = false;
			}
			else
			{
				persistSpams(spams.getItems());
			}
		}
		
	}

	private void persistSpams(List<MessageSpam> items) {
		
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		dao.addBulkSpams(items);
		
	}

	private long getSpamMaxId() {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		long count  = dao.getMaxSpamRecId();
		return count;
	}

	private void callAsyncServiceForBounce() {
		String url = "ViewMailAllBounceApiPagination.htm";
		
		boolean runflag = true;
		while(runflag)
		{
			long rec = getBounceMAxId();
			int recId = (int)rec;
			EmtsResponseBounce resBounce = getEmtsResponseBounce(url, recId, 1000);
			if(resBounce == null || resBounce.getItems().size() <= 0)
			{
				runflag = false;
			}
			else
			{
				persistBounces(resBounce.getItems());
			}
		}
		
		
	}
	
   	
	
	private void persistBounces(List<MessageBounce> items) {
		
				
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		System.out.println(items.get(0).getServiceProvider() + "service provider name in bounce spam theread");
		dao.addBulkBounces(items);
		
	}

	private long getBounceMAxId() {
		CampaignDao  dao = (CampaignDao) AppContext.getFromApplicationContext("campaignDao");
		long count  = dao.getMaxBounceRecId();
		return count;
	}
	
	public static EmtsResponseBounce getEmtsResponseBounce(String viewurl, int recId, int recSize) 
	{
		String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		String instance = ApplicationProperty.getProperty("litemail.instance");
		String url="";
		url=baseUrl+"/"+viewurl+"?pId="+platFormId+"&recId="+recId+"&recSize="+recSize;  
		
		HttpClient client = new DefaultHttpClient();
		EmtsResponseBounce emtsObject = new EmtsResponseBounce() ;
		try{
		 
		  HttpGet request = new HttpGet(url);
		  HttpResponse response = client.execute(request);
		  ObjectMapper wrapper = new ObjectMapper();
		  SimpleDateFormat dateFormat =  new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
		  wrapper.getSerializationConfig().setDateFormat(dateFormat);
		  wrapper.getDeserializationConfig().setDateFormat(dateFormat); 
		  emtsObject = wrapper.readValue(response.getEntity().getContent(), EmtsResponseBounce.class);
		  client.getConnectionManager().shutdown();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			 client.getConnectionManager().shutdown();
		}
		return emtsObject;
		
	}
	
	public static EmtsResponseSpam getEmtsResponseSpam(String viewurl, int recId, int recSize) 
	{
		String baseUrl=ApplicationProperty.getProperty("emts.redirect.url");
		String platFormId = ApplicationProperty.getProperty("emts.platform.id");
		String instance = ApplicationProperty.getProperty("litemail.instance");
		String url="";
		url=baseUrl+"/"+viewurl+"?pId="+platFormId+"&recId="+recId+"&recSize="+recSize;  
		
		HttpClient client = new DefaultHttpClient();
		EmtsResponseSpam emtsObject = new EmtsResponseSpam() ;
		try{
		 
		  HttpGet request = new HttpGet(url);
		  HttpResponse response = client.execute(request);
		  ObjectMapper wrapper = new ObjectMapper();
		  SimpleDateFormat dateFormat =  new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
		  wrapper.getSerializationConfig().setDateFormat(dateFormat);
		  wrapper.getDeserializationConfig().setDateFormat(dateFormat); 
		  emtsObject = wrapper.readValue(response.getEntity().getContent(), EmtsResponseSpam.class);
		  client.getConnectionManager().shutdown();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			 client.getConnectionManager().shutdown();
		}
		return emtsObject;
		
	}
	

	public long getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}


	public String getClientId() {
		return clientId;
	}


	public void setClientId(String clientId) {
		this.clientId = clientId;
	}


	public String getApiKey() {
		return apiKey;
	}


	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}



	public String getSmtpProvider() {
		return smtpProvider;
	}



	public void setSmtpProvider(String smtpProvider) {
		this.smtpProvider = smtpProvider;
	}

	

}
