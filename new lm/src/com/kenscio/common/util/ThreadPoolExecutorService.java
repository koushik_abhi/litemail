package com.kenscio.common.util;

import java.util.Enumeration;
import java.util.Map;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;



/**
 * 
 * @author venu T
 *
 */

public class ThreadPoolExecutorService {
	
	 private int poolSize = 2;
	 
	 public static ThreadPoolExecutorService impl = new ThreadPoolExecutorService();
	 
	 public int getPoolSize() {
		return poolSize;
	}

	public void setPoolSize(int poolSize) {
		this.poolSize = poolSize;
	}

	public int getMaxPoolSize() {
		return maxPoolSize;
	}

	public void setMaxPoolSize(int maxPoolSize) {
		this.maxPoolSize = maxPoolSize;
	}

	public int getQueueCapacity() {
		return queueCapacity;
	}

	public void setQueueCapacity(int queueCapacity) {
		this.queueCapacity = queueCapacity;
	}

	public ThreadPoolTaskExecutor getThreadPool() {
		return threadPool;
	}

	public void setThreadPool(ThreadPoolTaskExecutor threadPool) {
		this.threadPool = threadPool;
	}

	private int maxPoolSize = 50;
	 private int queueCapacity =50;
	 
	 private ThreadPoolTaskExecutor threadPool = null;
	 
	 public static ThreadPoolExecutorService getInstance()
	 {
		 return impl;
	 }
	
	 private ThreadPoolExecutorService()
	 {
		 initialize();
	 }
	 
	 private void initialize()
	 {
		 threadPool = new ThreadPoolTaskExecutor();
		 threadPool.setCorePoolSize(poolSize);
		 threadPool.setMaxPoolSize(maxPoolSize);
		 threadPool.setQueueCapacity(queueCapacity);
		 threadPool.initialize();
		 
	 }
	 public void shutdown()
	 {
		threadPool.shutdown();
	 }
	 
	 public void destroy()
	 {
		 threadPool.destroy();
	 } 
	 	 
	 
	
	
		 
}
