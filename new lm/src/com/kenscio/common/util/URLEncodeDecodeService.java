package com.kenscio.common.util;

import org.apache.commons.codec.binary.Base64;

public class URLEncodeDecodeService {
	
	
	public static String encodeURL(String url, String platformKey)
	{
		
		StringBuilder changedURL = new StringBuilder();
		if(url != null)
		{
			
			int index = url.indexOf("?");
			String params = url.substring(index+1);
			params = params+"&key="+platformKey;
			String actualURL = url.substring(0, index);
			byte[] encodedBytes = Base64.encodeBase64(params.getBytes());
			changedURL.append(actualURL+"?");
			changedURL.append("ecp=");
			changedURL.append(new String(encodedBytes));
			
		}
		return changedURL.toString();
		
	}
	
	public static String encodeParamsString(String paramsString, String platformKey)
	{
		
		StringBuilder changedURL = new StringBuilder();
		if(paramsString != null)
		{			
			
			paramsString = paramsString+"&key="+platformKey;
			byte[] encodedBytes = Base64.encodeBase64(paramsString.getBytes());
			changedURL.append(new String(encodedBytes));
			
		}
		return changedURL.toString();
		
	}
	public static String decodeParamsString(String encodedString)
	{
		//http://$server/hhalerts/unSealedMailOpen.do?cId=$campaignId&name=$toName&eId=$recepientMail
		StringBuilder oriURL = new StringBuilder();
		if(encodedString != null && encodedString.length() > 0)
		{
		
			byte[] decodedBytes = Base64.decodeBase64(encodedString.getBytes());
			oriURL.append(new String(decodedBytes));
			
		}	
		return oriURL.toString();	
		
	}
	
	
	
	public static String encodeParams(String params, String platformKey)
	{
		
		StringBuilder changedURL = new StringBuilder();
		if(params != null)
		{
			
			
			params = params+"&key="+platformKey;
			byte[] encodedBytes = Base64.encodeBase64(params.getBytes());
			changedURL.append("?ecp=");
			changedURL.append(new String(encodedBytes));
			
		}
		return changedURL.toString();
		
	}
	
	public static String decodeParams(String params, String platformKey)
	{
		//http://$server/hhalerts/unSealedMailOpen.do?cId=$campaignId&name=$toName&eId=$recepientMail
		StringBuilder oriURL = new StringBuilder();
		if(params != null && params.indexOf("=") > 0)
		{
						
			int index1 = params.indexOf("=");
			String params1 = params.substring(index1+1);
			byte[] decodedBytes = Base64.decodeBase64(params1.getBytes());
			oriURL.append("?"+new String(decodedBytes));
			
		}else
		{
			byte[] decodedBytes = Base64.decodeBase64(params.getBytes());
			oriURL.append("?"+new String(decodedBytes));
		}		
		return oriURL.toString();	
		
	}
	
	public static String decodeURL(String url, String platformKey)
	{
		//http://$server/hhalerts/unSealedMailOpen.do?cId=$campaignId&name=$toName&eId=$recepientMail
		StringBuilder oriURL = new StringBuilder();
		if(url != null)
		{
			
			int index = url.indexOf("?");
			String params = url.substring(index+1);
			
			int index1 = params.indexOf("=");
			String params1 = params.substring(index1+1);
			
			String actualURL = url.substring(0, index);
			byte[] decodedBytes = Base64.decodeBase64(params1.getBytes());
			oriURL.append(actualURL+"?");
			oriURL.append(new String(decodedBytes));
			
		}
		
		return oriURL.toString();	
		
	}
	
	public static void main(String arg[])
	{
		String url = "http://$server/hhalerts/unsealedMailOpenAction.do?cId=$campaignId&name=$toName&eId=$recepientMail";
		System.out.println("original url before encode " +url);
		String platformKey = "macmis";
		URLEncodeDecodeService u = new URLEncodeDecodeService();
		String enc = u.encodeURL(url, platformKey);
		System.out.println("encode string is " +enc);
		System.out.println("decode string is " + u.decodeURL(enc, platformKey));
		
	}

}
