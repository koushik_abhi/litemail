package com.dataservice.platform.persistence.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class DataServiceMapObject  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -523370732924653422L;
	
	private Map<String, String> dataServiceMap = new HashMap<String, String>();

	public Map<String, String> getDataServiceMap() {
		return dataServiceMap;
	}

	public void setDataServiceMap(Map<String, String> dataServiceMap) {
		this.dataServiceMap = dataServiceMap;
	}
	

}
