package com.dataservice.platform.persistence.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.kenscio.litemail.util.DAOInitializer;
import com.kenscio.litemail.util.Page;
import com.kenscio.litemail.util.PaginationHelper;



public class DataServiceDaoImpl 
{
	private static Logger log = Logger.getLogger(DataServiceDaoImpl.class.getName());
	private static JdbcTemplate jdbcTemplate = null;
	
	static
	{
		try
		{
			jdbcTemplate = DAOInitializer.getJdbcTemplate();
			
		}catch(Exception e)
		{
			log.log(Level.SEVERE, "Datasource is not configured properly "+ e.getMessage())	;	
		}
		
	}
	
	public void setDataSource(DataSource dataSource) 
	{
		jdbcTemplate = new JdbcTemplate(dataSource);
	
	}
	
	public Page<Object> getRecordsOnPagination(final String sqlcountquery, final String sqlrows, final int pageNo, final int pageSize, Object[] args, ParameterizedRowMapper<Object> rowMapper){
        PaginationHelper<Object> ph = new PaginationHelper<Object>();
        
        return ph.fetchPage(
                jdbcTemplate,
                sqlcountquery,
                sqlrows,
                args,
                pageNo,
                pageSize,
                rowMapper
        );

    }
	

	public void executeUpdateRecord(String sql)
	{
		jdbcTemplate.execute(sql);
	}
	
	public int getRecordIdName(String sql)
	{
		return jdbcTemplate.queryForInt(sql);
	}
	public int getRecordCount(String sql)
	{
		return jdbcTemplate.queryForInt(sql);
	}
		
	public void executeUpdateRecordCallBack(String sql, PreparedStatementCallback ps)
	{
		jdbcTemplate.execute(sql, ps);
	}
	

	public void updateRecordPrepStatement(PreparedStatementCreator psc)
	{
		jdbcTemplate.update(psc);
		
	}
			

	public List getRecordsOnSql(String sqlQuery) {
		
		return jdbcTemplate.queryForList(sqlQuery);
	}
	
	public JdbcTemplate getJdbcTemplate()
	{
		return jdbcTemplate;
	}
			
	
	public Map<String, Object> getRecordDataMap(String sqlQuery) {
		
		return jdbcTemplate.queryForMap(sqlQuery);
	}

		
	public List<String> getRecordsEmailOnSqlRowMapperWithPsc(PreparedStatementCreator psc, RowMapper<String> rowMapper) {
		
		return jdbcTemplate.query(psc, rowMapper);
	}
	public List<List<String>> getRecordsEmailandSpecialFields(String sql, RowMapper<List<String>> rowMapper) {
		
		return jdbcTemplate.query(sql, rowMapper);
	}
	
	public List<Map<String,String>> getMapRecordsOnSqlRowMapperWithPsc(PreparedStatementCreator psc, RowMapper<Map<String,String>> rowMapper) {
		
		return jdbcTemplate.query(psc, rowMapper);
	}
	
	public SqlRowSet getRecordsOnSqlRsRowMapperWithPsc(String sql,  String status, int actionId) {
		
		return jdbcTemplate.queryForRowSet(sql, new Object[] {status, actionId});
	}
	
	public SqlRowSet getRecordsOnSqlRsRowMapperWithPsc(String sql,  String userId) {
		
		return jdbcTemplate.queryForRowSet(sql, new Object[] {userId});
	}
	public SqlRowSet getRecordsOnSqlRsRowMapperWithPsc(String sql) {
		
		return jdbcTemplate.queryForRowSet(sql);
	}
	
	public SqlRowSet getRecordsOnSqlRsRowMapperWithPsc(String sql, List<String> userIds) {
		
		return jdbcTemplate.queryForRowSet(sql, new Object[] {userIds.toArray()});
	}
	
	public SqlRowSet getRecordsOnSqlRowSet(String sql) {
		
		return jdbcTemplate.queryForRowSet(sql);
		
	}

	public int[] batchUpdateOnSQL(String[] sqlQuery) {
		
		return jdbcTemplate.batchUpdate(sqlQuery);
		
	}

	public void executeDDL(String sql) {

		jdbcTemplate.execute(sql);
		
	}

	public int[] batchUpdateWithPreparedStament(String sqlQuery,  BatchPreparedStatementSetter bps) {
		
		return jdbcTemplate.batchUpdate(sqlQuery, bps);
	}
	

}
