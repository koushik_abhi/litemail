package com.dataservice.platform.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.*;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;


import au.com.bytecode.opencsv.CSVWriter;

import com.kenscio.litemail.util.DataServiceUtilLiteMail;
import com.kenscio.litemail.workflow.common.AppContext;
import com.kenscio.litemail.workflow.dao.MailListDao;
import com.kenscio.litemail.workflow.domain.EmailData;
import com.kenscio.litemail.workflow.domain.ListDataEntity;
import com.kenscio.litemail.workflow.service.AttributeService;


public class ListDataDaoImpl extends DataServiceDaoImpl {

	public static Logger log = Logger.getLogger(ListDataDaoImpl.class.getName());	
	
	public boolean addCustomRecordsFromExcelNew(final String sql, final long listId, final List<String> colection) {
		
	//	final String sql =DataServiceUtilLiteMail.createSQLQuery(listId, newmaptemp.keySet(),DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA);
		//final Collection<String> colection = newmaptemp.values();
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				int count = 1;
				for(String st : colection)
				{
					count++;
					ps.setString(count, st);
				}
				
				return ps;
			}
		};
		
		this.updateRecordPrepStatement(psc);
		return true;
		
	}
	
	public List<String> getAllBouncedEmails(final String bounceType, final Date from,String sp,String dm) {
		   final String sql  = "select RECIEPIENT_EMAIL  from LITEMAIL_CAMPAIGN_BOUNCE where DOMAIN_NAME='"+dm+"'and SERVICE_PROVIDER='"+sp+"' and BOUNCE_TYPE= ? and BOUNCE_DATE between ? and now()" ;
		   
		   PreparedStatementCreator psc = new PreparedStatementCreator() 
		   {
		    
		    @Override
		    public PreparedStatement createPreparedStatement(Connection con)
		      throws SQLException {
		     
		     PreparedStatement ps = con.prepareStatement(sql);
		     ps.setString(1, bounceType);
		     ps.setTimestamp(2, (Timestamp) from);
		     return ps;
		    }
		   };
		   
		   ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		       public Object extractData(ResultSet rs) throws SQLException {
		        List<String> emailList=new ArrayList<String>();
		          while (rs.next()) {
		            String email = rs.getString("RECIEPIENT_EMAIL");
		            emailList.add(email);
		          }
		     return emailList;
		       }
		   };
		    
		   List<String> emailList=getJdbcTemplate().query(psc, mapExtractor);
		   return emailList;
		       
	}
	
	public List<String> getSpamEmails(final Date campaignStartTime,String sp,String dm) {
		 
		   final String sql  = "select RECIEPIENT_EMAIL  from LITEMAIL_CAMPAIGN_SPAM where SERVICE_PROVIDER='"+sp+"' and DOMAIN_NAME='"+dm+"' and CILCK_DATE between ? and now()" ;
		   PreparedStatementCreator psc = new PreparedStatementCreator() 
		   {
		    
		    @Override
		    public PreparedStatement createPreparedStatement(Connection con)
		      throws SQLException {
		     
		     PreparedStatement ps = con.prepareStatement(sql);
		     ps.setTimestamp(1, (Timestamp) campaignStartTime);
		     return ps;
		    }
		   };
		   
		   ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		       public Object extractData(ResultSet rs) throws SQLException {
		        List<String> emailList=new ArrayList<String>();
		          while (rs.next()) {
		            String email = rs.getString("RECIEPIENT_EMAIL");
		            emailList.add(email);
		          }
		     return emailList;
		       }
		   };
		    
		   List<String> emailList=getJdbcTemplate().query(psc, mapExtractor);
		   return emailList;
		       
		   
		   
	}
	
	public List<String> validEmailIdsinList(final List<String> email)
	{
		final String sql = DataServiceUtilLiteMail.createSelectQueryList( email, "LITEMAIL_LIST_CUSTOM_DATA");
		PreparedStatementCreator ps=new PreparedStatementCreator() {
		
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				  PreparedStatement ps = con.prepareStatement(sql);
				  int count=1;
				    for (String string : email) {
						
				    	ps.setString(count, string);
				    	count++;
					}
				     return ps;
			}
		};
		
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		       public Object extractData(ResultSet rs) throws SQLException {
		        List<String> emailList=new ArrayList<String>();
		          while(rs.next()) {
		            String email = rs.getString("email");
		            emailList.add(email);
		          }
		     return emailList;
		       }
		   };
		    
		   List<String> emailList=getJdbcTemplate().query(ps, mapExtractor);
		   if (emailList !=null && emailList.size() >0) {
			   return emailList;
		}
		   else
		   {
			   return new ArrayList<String>();
		   }
		  
		
	}
	
	public boolean addStdRecordsFromExcelNew(final String sql, final long listId, final List<String> colection) {
		
		//inal Collection<String> colection = newmaptemp.values();
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				int count = 1;
				for(String st : colection)
				{
					count++;
					ps.setString(count, st);
				}
				
				return ps;
			}
		};
		
		this.updateRecordPrepStatement(psc);
		return true;
		
	}
	
		
	public boolean updateCustomRecordsFromExcel( final long listId, final String sql, final List<String> values, final String emailId) {
		
		//final String sql =DataServiceUtilLiteMail.createUpdateSQLQuery(listId, updatemap,DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA);
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				int count = 1;
				//Set<String> keys = updatemap.keySet();
				for(String st : values)
				{
					ps.setString(count, st);
					count++;
				}
				ps.setString(count, emailId);
				count++;
				ps.setLong(count, listId);
				return ps;
			}
		};
		
		this.updateRecordPrepStatement(psc);
		return true;
		
	}

	public boolean updateRecordsOnEmail(final long listId , final Map<String, String> updatemap, final String emailId) {
		
		final String sql =DataServiceUtilLiteMail.createUpdateSQLQuery(listId, updatemap,DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA);
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				int count = 1;
				Set<String> keys = updatemap.keySet();
				for(String st : keys)
				{
					ps.setString(count, updatemap.get(st));
					count++;
				}
				ps.setString(count, emailId);
				ps.setLong(count, listId);
				return ps;
			}
		};
		
		this.updateRecordPrepStatement(psc);
		return true;
		
		
	}
		
	public boolean deleteAllRecods() {
		
		final String sql  = "delete from " +DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA;
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				return ps;
			}
		};
		this.updateRecordPrepStatement(psc);
		return true;
	}
	
	public String isEmailListExist(final String emailId, final long listId) {
		
		final String sql  = "select email  from "+ DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA + " where  list_id= ? and " + DataServiceUtilLiteMail.email + "=?  " ;
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				ps.setString(2, emailId);
				return ps;
			}
		};
		
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
			   public String extractData(ResultSet rs) throws SQLException {
			      if (rs.next()) {
			        String att = rs.getString("email");
			        return att;
			      }
			      return "";
			   }
		};
			
		return this.getJdbcTemplate().query(psc, mapExtractor);
						
	}


	public void deleteCustomRecordsFromExcel(final long listId, final String em) {
		final String sql  = "delete from " +DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA + " where list_id =? and email = ?";
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				ps.setString(2, em);
				return ps;
			}
		};
		this.updateRecordPrepStatement(psc);
		
		
	}
	
	public void deleteCustomRecordsFromExcel(final long listId) {
		final String sql  = "delete from " +DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA + " where list_id =? ";
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				
				return ps;
			}
		};
		this.updateRecordPrepStatement(psc);
		
		
	}
	
	public Map<Long, List<EmailData>> getEmailsIdsbyBatchSpringJDBC(long listId, long fileSeeker, long noOfRecords) {
		List<EmailData> data = getEmailDataForList(listId, fileSeeker, noOfRecords );
		long nextPointer = fileSeeker+data.size();
		Map<Long, List<EmailData>> records = new HashMap<Long, List<EmailData>>();
		records.put(nextPointer, data);
		return records;
	}

	public List<List<String>> getSubDataForList(final long listId, final int pageNumber, final int noOfRecords) {
		
		String sql = "select email, firstname, age, gender, city from  LITEMAIL_LIST_CUSTOM_DATA where list_id="+listId;
		final List<List<String>> emaildata  = new ArrayList<List<String>>();	
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		public Object extractData(ResultSet rs) throws SQLException {
			
			  int count = 0;
			  int absval = (pageNumber-1)*noOfRecords;
			  if(absval> 0)
			  rs.absolute(absval);
		      while (rs.next()) {
		    	List<String> data = new ArrayList<String>();
		    	count++;
		    	String email = rs.getString("email");
		        String firstname = rs.getString("firstname");
		        String age = rs.getString("age");
		        String gender = rs.getString("gender");
		        String city = rs.getString("city");
		        data.add(email);
		        data.add(firstname);
		        data.add(age);
		        data.add(gender);
		        data.add(city);
		       
		        emaildata.add(data);
		        if(count == (int)noOfRecords )
		        	break;
		      }
		      return emaildata;
		   }
		};
		List<List<String>> map = (List<List<String>>) getJdbcTemplate().query(sql.toString(), mapExtractor);
		return map;
		
	}
	
	private List<EmailData> getEmailDataForList(final long listId, final long fileSeeker,
			final long noOfRecords) {
		
		String sql = "select email, firstname from  LITEMAIL_LIST_CUSTOM_DATA where list_id="+listId;
			
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		   public Object extractData(ResultSet rs) throws SQLException {
			  List<EmailData> emaildata  = new ArrayList<EmailData>();
			  int count = 0;
			  if((int)fileSeeker > 0)
			  rs.absolute((int)fileSeeker);
		      while (rs.next()) {
		    	count++;
		    	EmailData ed = new EmailData();
		        String email = rs.getString("email");
		        String firstname = rs.getString("firstname");
		        ed.setListId(listId);
		        ed.setEmailId(email);
		        ed.setName(firstname);
		        emaildata.add(ed);
		        if(count == (int)noOfRecords )
		        	break;
		      }
		      return emaildata;
		   }
		};
		
		List<EmailData> map = (List<EmailData>) getJdbcTemplate().query(sql.toString(), mapExtractor);
		return map;
	}

	public String checkIfStdDataOnly(final Long listId) {
		
		final String sql  = "select email  from "+ DataServiceUtilLiteMail.LITEMAIL_LIST_STD_DATA + " where  list_id= ? limit 1  " ;
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				
				return ps;
			}
		};
		
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
			   public String extractData(ResultSet rs) throws SQLException {
			      if (rs.next()) {
			        String att = rs.getString("email");
			        return att;
			      }
			      return "";
			   }
		};
			
		return this.getJdbcTemplate().query(psc, mapExtractor);
	}
	
	
	/**
	 *
	 * @param writer
	 * @param headers
	 * @param listName
	 * @param accountId
	 * @param accountId2 
	 */

	public void exportListDataToExcel(final CSVWriter writer, List<String> headers,
			String listName, final long listId, long accountId) {
		
		final List<String> attributeMapped = new ArrayList<String>();
		final Map<String, String> customDBToAttrMap = getMappedAttributesMap(listId);
		AttributeService as = new AttributeService();
		List<String> stdAttr = as.getAllStdAttributes();
		final List<String> totalAtrr = new ArrayList<String>();
		final List<String> heads = new ArrayList<String>();
		totalAtrr.addAll(stdAttr);
		totalAtrr.addAll(customDBToAttrMap.keySet());
		heads.addAll(stdAttr);
		heads.addAll(customDBToAttrMap.values());
		String [] array = heads.toArray(new String[heads.size()]);
	    writer.writeNext(array);   
		String sql = DataServiceUtilLiteMail.createSelectQueryList(listId, totalAtrr, "LITEMAIL_LIST_CUSTOM_DATA");
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		public Object extractData(ResultSet rs) throws SQLException {
			while (rs.next()) {
				  final List<String> list = new ArrayList<String>();
				  for(String col : totalAtrr)
				  {
					 list.add(rs.getString(col));
				  }
				  String [] earray = list.toArray(new String[list.size()]);
				  writer.writeNext(earray);   
				  
				  for (int i = 0; i < list.size(); i++) {
						System.out.println(list.get(i));
					}	        
		      }
		      return  new Object();
		   }
		};
		System.out.println("dfdf"+sql);
		this.getJdbcTemplate().query(sql, mapExtractor);
				
	}
	public Map<String, String> getSubscriberCustomDetails(long listId, String email) {
			  final Map<String, String> customDBToAttrMap = getMappedAttributesMap(listId);
			  if (customDBToAttrMap.size()==0) {
				return new HashMap<String, String>();
			}
			  else{
				  
			 
			  final List<String> custromAtrr = new ArrayList<String>();
			  custromAtrr.addAll(customDBToAttrMap.keySet());
			  
			  String sql = DataServiceUtilLiteMail.createSelectQuery(listId, email, custromAtrr, "LITEMAIL_LIST_CUSTOM_DATA");
			  System.out.println("create dynamic1 :"+sql);
			  ResultSetExtractor mapExtractor = new ResultSetExtractor() {
			  public Object extractData(ResultSet rs) throws SQLException {
			     Map<String, String> map  = new HashMap<String, String>();
			     
			     while (rs.next()) {
			      
			      for(String col : custromAtrr)
			      {
			      String value =  rs.getString(col);
			      if(customDBToAttrMap.containsKey(col))
			      {
			       map.put(customDBToAttrMap.get(col), value);
			      }else
			      {
			       map.put(col, value);
			      }
			      }
			                 
			        }
			        return  map;
			     }
			  };
			   
			  Map<String, String> map = ( Map<String, String>) getJdbcTemplate().query(sql, mapExtractor);
			  return map;
			  }
			  
			 }
	private List<String> getMappedAttributes(final long listId) {
		
		final String sql = "select attribute_name from  LITEMAIL_LISTATTRIBUTE_MAP where list_id= ?";
		final List<String> attributeMapped = new ArrayList<String>();
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				
				return ps;
			}
		};			
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		   public Object extractData(ResultSet rs) throws SQLException {
		      while (rs.next()) {
		    	  
		        String attributeName = rs.getString("attribute_name");
		        attributeMapped.add(attributeName);
		     
		      }
		      return new Object();
		      
		   }
		};
			
		this.getJdbcTemplate().query(psc, mapExtractor); 
		return attributeMapped;
		
		
	}
	
	private Map<String, String> getMappedAttributesMap(final long listId) {
		
		final String sql = "select db_attribute_name, attribute_name from  LITEMAIL_LISTATTRIBUTE_MAP where list_id= ?";
		final Map<String, String> attributeMapped = new HashMap<String, String>();
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				
				return ps;
			}
		};			
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		   public Object extractData(ResultSet rs) throws SQLException {
		      while (rs.next()) {
		    	  
		        String db_attribute_name = rs.getString("db_attribute_name");
		        String attribute_name = rs.getString("attribute_name");
		        attributeMapped.put(db_attribute_name, attribute_name);
		     
		      }
		      return new Object();
		      
		   }
		};
			
		this.getJdbcTemplate().query(psc, mapExtractor); 
		
		return attributeMapped;
		
		
	}
	
	private Map<String, String> getAttributesToDBMap(final long listId) {
		
		final String sql = "select db_attribute_name, attribute_name from  LITEMAIL_LISTATTRIBUTE_MAP where list_id= ?";
		final Map<String, String> attributeMapped = new HashMap<String, String>();
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				
				return ps;
			}
		};			
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		   public Object extractData(ResultSet rs) throws SQLException {
		      while (rs.next()) {
		    	  
		        String db_attribute_name = rs.getString("db_attribute_name");
		        String attribute_name = rs.getString("attribute_name");
		        attributeMapped.put(attribute_name, db_attribute_name);
		     
		      }
		      return new Object();
		      
		   }
		};
			
		this.getJdbcTemplate().query(psc, mapExtractor); 
		return attributeMapped;
		
		
	}

	
	public long getValidEmailCountForCampaignList(final Long listId) {
		
		String sql = "select count(email) from  LITEMAIL_LIST_CUSTOM_DATA where list_id= "+ listId;
		int count = this.getJdbcTemplate().queryForInt(sql);
		
		
		return new Long(count);
	}

	public boolean isEmailExistInCampaignList(final long listId, final String email) {
		
		int count = 0;
		String sql  = "select count(email)  from "+ DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA + " where  list_id= ? and email =? " ;
			count = this.getJdbcTemplate().queryForInt(sql);
		
		if(count <= 0)
			return false;
		else 
			return true;
		
	}

	public List getAttributeDataForListGraph(final long listId, final String attribute) {
		final Map<String, String> attrdbmap = getAttributesToDBMap(listId);
		String selectField = "";
		if(attrdbmap.containsKey(attribute))
		{
			selectField = attrdbmap.get(attribute);
		}else
		{
			selectField = attribute;
		}
		final String sql  = "select " +selectField+" a, count("+selectField+") b from LITEMAIL_LIST_CUSTOM_DATA where list_id =?  group by "+ selectField;
		
		final List<ListDataEntity> lds = new ArrayList<ListDataEntity>();
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				
				return ps;
			}
		};			
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		   public Object extractData(ResultSet rs) throws SQLException {
		      while (rs.next()) {
		    	 
		    	 ListDataEntity ld = new ListDataEntity();
		    	 String name = rs.getString("a");
			     int cont = (Integer)rs.getInt("b");
				 ld.setAttributeValue(name);
				 ld.setAttributeCount(cont);
				 lds.add(ld);
		      
		      }
		      return new Object();
		      
		   }
		};
			
		this.getJdbcTemplate().query(psc, mapExtractor); 
		return lds;
		
	}


	public Map<String, String> getAttributesDataMap(String email, long listId) {
		
		final Map<String, String> customDBToAttrMap = getMappedAttributesMap(listId);
		AttributeService as = new AttributeService();
		List<String> stdAttr = as.getAllStdAttributes();
		final List<String> totalAtrr = new ArrayList<String>();
		totalAtrr.addAll(stdAttr);
		totalAtrr.addAll(customDBToAttrMap.keySet());
		
		String sql = DataServiceUtilLiteMail.createSelectQuery(listId, email, totalAtrr, "LITEMAIL_LIST_CUSTOM_DATA");
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		public Object extractData(ResultSet rs) throws SQLException {
			  Map<String, String> map  = new HashMap<String, String>();
			  
			  while (rs.next()) {
				  
				  for(String col : totalAtrr)
				  {
					 String value =  rs.getString(col);
					 if(customDBToAttrMap.containsKey(col))
					 {
						 map.put(customDBToAttrMap.get(col), value);
					 }else
					 {
						 map.put(col, value);
					 }
				  }
		    			        
		      }
		      return  map;
		   }
		};
			
		Map<String, String> map = ( Map<String, String>) getJdbcTemplate().query(sql, mapExtractor);
		return map;
			
		
	}	
	
	
	
	public Map<String, String> getAttributesDataMapCustom(String email, long listId,final List<String> totalColumns, final Map<String, String> customDBToAttrMap)
	{
		
		String sql = DataServiceUtilLiteMail.createSelectQuery(listId, email, totalColumns, "LITEMAIL_LIST_CUSTOM_DATA");
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		public Object extractData(ResultSet rs) throws SQLException {
			  Map<String, String> map  = new HashMap<String, String>();
			  
			  while (rs.next()) {
				  
				  for(String col : totalColumns)
				  {
					 String value =  rs.getString(col);
					 if(customDBToAttrMap.containsKey(col))
					 {
						 map.put(customDBToAttrMap.get(col), value);
					 }else
					 {
						 map.put(col, value);
					 }
				  }
		    			        
		      }
		      return  map;
		   }
		};
			
		Map<String, String> map = ( Map<String, String>) getJdbcTemplate().query(sql, mapExtractor);
		return map;
			

	
	}

	public void deleteSubscriber(final long listId, final String email) 
	{
		final String sql  = "delete from "+ DataServiceUtilLiteMail.LITEMAIL_LIST_CUSTOM_DATA + " where  list_id= ? and email =? " ;
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				ps.setString(2, email);
				return ps;
			}
		};	
		getJdbcTemplate().update(psc);
		
		
	}
	
	public Map<String, String> getSubscriberDetails(final long listId, final String email) {
		
		String sql = "select email, firstname, lastname, age, address, city, salary, gender, dateofbirth, mobilenumber from  LITEMAIL_LIST_CUSTOM_DATA where list_id="+listId + " and email='"+email+"'";
		System.out.println("query >>>>>  " + sql);
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		   public Object extractData(ResultSet rs) throws SQLException {
			final Map<String, String> emaildata  = new HashMap<String, String>();
			 if (rs.next()) 
			 {
		    	String email = rs.getString("email");
		        String firstname = rs.getString("firstname");
		        String lastname = rs.getString("lastname");
		        String age = rs.getString("age");
		        String address = rs.getString("address");
		        String city = rs.getString("city");
		        String salary = rs.getString("salary");
		        String gender = rs.getString("gender");
		        String dateofbirth = rs.getString("dateofbirth");
		        String mobilenumber = rs.getString("mobilenumber");
		        emaildata.put("email", email);
		        emaildata.put("firstname", firstname);
		        emaildata.put("lastname", lastname);
		        emaildata.put("age", age);
		        emaildata.put("address", address);
		        emaildata.put("city", city);
		        emaildata.put("salary", salary);
		        emaildata.put("gender", gender);
		        emaildata.put("dateofbirth", dateofbirth);
		        emaildata.put("mobilenumber", mobilenumber);
		      
		      }
		      return emaildata;
		   }
		};
		Map<String, String> map = ( Map<String, String>) getJdbcTemplate().query(sql, mapExtractor);
		return map;
		
	}

	public void updateSubscriberDetails(final long listId, final String email,
			final Map<String, String> stdAttr) {
		if(stdAttr.containsKey("email"))
			stdAttr.remove("email");
		try{
		final String sql  =  DataServiceUtilLiteMail.createUpdateSQLQuery(listId, stdAttr, "LITEMAIL_LIST_CUSTOM_DATA")  ;
		
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				Set<String> keys = stdAttr.keySet();
				int count  = 1;
				for(String str: keys)
				{
					ps.setString(count, stdAttr.get(str));
					count++;
				}				
			
				ps.setString(count, email);
				count++;
				ps.setLong(count, listId);
				return ps;
			}
		};	
		this.updateRecordPrepStatement(psc);
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}

	public long getListDatCount(long listId) {
		String sql = "select count(email) from  LITEMAIL_LIST_CUSTOM_DATA where list_id= "+ listId;
		int count = this.getJdbcTemplate().queryForInt(sql);
		
		
		return new Long(count);
	}

		

}
