package com.dataservice.platform.persistence.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.*;

import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.kenscio.litemail.util.DataServiceUtilLiteMail;


public class AttributeDataDaoImpl extends DataServiceDaoImpl {

	public static Logger log = Logger.getLogger(AttributeDataDaoImpl.class.getName());	
	
	public void addAttributeMap(final String attrName, final String dbAttr, final long listId) {
		
		final String sql = "insert into " +DataServiceUtilLiteMail.LITEMAIL_LISTATTRIBUTE_MAP+ "("+DataServiceUtilLiteMail.list_id+","+DataServiceUtilLiteMail.attribute_name+","+DataServiceUtilLiteMail.db_attribute_name+") " +
				" values (?,?,?) ";
		
		PreparedStatementCreator psc = new PreparedStatementCreator() 
		{
			
			@Override
			public PreparedStatement createPreparedStatement(Connection con)
					throws SQLException {
				
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, listId);
				ps.setString(2, attrName);
				ps.setString(3, dbAttr);
				return ps;
			}
		};
					
		this.updateRecordPrepStatement(psc);
		
			
	}
	
	

	public Map<String, String> getListAttributesMapped(long listId) {
		
		String sql = "select attribute_name, db_attribute_name from  LITEMAIL_LISTATTRIBUTE_MAP where list_id="+listId;
		        		
		ResultSetExtractor mapExtractor = new ResultSetExtractor() {
		   public Object extractData(ResultSet rs) throws SQLException {
		      Map<String, String> mapOfKeys = new HashMap<String, String>();
		      while (rs.next()) {
		        String att = rs.getString("attribute_name");
		        String dbatt = rs.getString("db_attribute_name");
		       
		        mapOfKeys.put(att, dbatt);
		      }
		      return mapOfKeys;
		   }
		};
		
		Map<String, String> map = (HashMap) getJdbcTemplate().query(sql.toString(), mapExtractor);
		return map;
	}
	
	public List<String> getAttributesOfListId(Long listId1){
		String sql="select attribute_name from LITEMAIL_LISTATTRIBUTE_MAP where list_id="+listId1;
		
		return null;
	}

}
